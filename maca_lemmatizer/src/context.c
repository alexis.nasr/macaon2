#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<unistd.h>
#include<getopt.h>
#include "context.h"
#include "util.h"


void context_set_linguistic_resources_filenames(context *ctx);

void context_free(context *ctx)
{
  if(ctx->program_name) free(ctx->program_name);
  if(ctx->conll_filename) free(ctx->conll_filename);
  if(ctx->fplm_filename) free(ctx->fplm_filename);
  if(ctx->language) free(ctx->language);
  if(ctx->maca_data_path) free(ctx->maca_data_path);
  free(ctx);
}

context *context_new(void)
{
  context *ctx = (context *)memalloc(sizeof(context));

  ctx->help = 0;
  ctx->verbose = 0;
  ctx->debug_mode = 0;
  ctx->program_name = NULL;
  ctx->conll_filename = NULL;
  ctx->fplm_filename = NULL;
  ctx->mcd_filename = NULL;
  ctx->mcd_struct = NULL;
  ctx->language = strdup("fr");
  ctx->maca_data_path = NULL;
  ctx->form_column = -1;
  ctx->pos_column = -1;
  return ctx;
}

void context_general_help_message(context *ctx)
{
    fprintf(stderr, "usage: %s [options]\n", ctx->program_name);
    fprintf(stderr, "Options:\n");
    fprintf(stderr, "\t-h --help             : print this message\n");
    fprintf(stderr, "\t-v --verbose          : activate verbose mode\n");
    fprintf(stderr, "\t-r --hratio   <float> : set the occupation ratio of hash tables (default is 0.5)\n");
}

void context_conll_help_message(context *ctx){
  fprintf(stderr, "\t-i --conll  <file>  : conll file name\n");
}


void context_form_column_help_message(context *ctx){
  fprintf(stderr, "\t-F --form_column  <int>  : column containing form\n");
}

void context_pos_column_help_message(context *ctx){
  fprintf(stderr, "\t-P --pos_column  <int>  : column containing part of speech tag\n");
}

void context_fplm_help_message(context *ctx){
  fprintf(stderr, "\t-f --fplm   <file>  : fplm (form pos lemma morpho) file\n");
}
void context_mcd_help_message(context *ctx){
  fprintf(stderr, "\t-C --mcd   <file> : multi column description file name\n");
}
void context_language_help_message(context *ctx){
  fprintf(stderr, "\t-L --language  : identifier of the language to use\n");
}
void context_maca_data_path_help_message(context *ctx){
  fprintf(stderr, "\t-M --maca_data_path  : path to maca_data directory\n");
}

context *context_read_options(int argc, char *argv[])
{
  int c;
  int option_index = 0;
  context *ctx = context_new();

  ctx->program_name = strdup(argv[0]);

  static struct option long_options[10] =
    {
      {"help",                no_argument,       0, 'h'},
      {"verbose",             no_argument,       0, 'v'},
      {"debug",               no_argument,       0, 'd'},
      {"conll",               required_argument, 0, 'i'},
      {"mcd",                 required_argument, 0, 'C'}, 
      {"language",            required_argument, 0, 'L'},
      {"fplm",                required_argument, 0, 'f'},
      {"form_column",         required_argument, 0, 'F'},
      {"pos_column",          required_argument, 0, 'P'},
      {"maca_data_path",      required_argument, 0, 'D'}
    };
  optind = 0;
  opterr = 0;
  
  while ((c = getopt_long (argc, argv, "hvdi:f:C:L:M:F:D:P:", long_options, &option_index)) != -1){ 
    switch (c)
      {
      case 'd':
	ctx->debug_mode = 1;
	break;
      case 'h':
	ctx->help = 1;
	break;
      case 'v':
	ctx->verbose = 1;
	break;
      case 'F':
	ctx->form_column = atoi(optarg) - 1;
	break;
      case 'P':
	ctx->pos_column = atoi(optarg) - 1;
	break;
      case 'f':
	ctx->fplm_filename = strdup(optarg);
	break;
      case 'i':
	ctx->conll_filename = strdup(optarg);
	break;
      case 'C':
	ctx->mcd_filename = strdup(optarg);
	break;
      case 'L':
	ctx->language = strdup(optarg);
	break;
      case 'D':
	ctx->maca_data_path = strdup(optarg);
	break;
      }
  }

  context_set_linguistic_resources_filenames(ctx);


  if(ctx->mcd_filename)
    ctx->mcd_struct = mcd_read(ctx->mcd_filename, ctx->verbose);


  if((ctx->mcd_filename == NULL) && ((ctx->form_column == -1) || (ctx->pos_column == -1)))
    /* ctx->mcd_struct = mcd_build_conll07(); */
    ctx->mcd_struct = mcd_build_wplgf();

  return ctx;
}

void context_set_linguistic_resources_filenames(context *ctx)
{
  char absolute_path[500];
  char absolute_filename[500];

  absolute_path[0] = '\0';

  if(ctx->maca_data_path)
    strcat(absolute_path, ctx->maca_data_path);
  else {
      char *e = getenv("MACAON_DIR");
      if (e != NULL) {
	  strcat(absolute_path, e);	  
      } else {
	  fprintf(stderr, "ATTENTION: the environment variable MACAON_DIR is not defined\n");
      }
  }

	   
  strcat(absolute_path, "/");
  strcat(absolute_path, ctx->language);
  strcat(absolute_path, "/bin/");

  if(!ctx->fplm_filename){
    strcpy(absolute_filename, absolute_path);
    strcat(absolute_filename, DEFAULT_FPLM_FILENAME);
    ctx->fplm_filename = strdup(absolute_filename);
  }
  
}
