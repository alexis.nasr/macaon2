#ifndef __MACA_LEMMATIZER_CONTEXT__
#define __MACA_LEMMATIZER_CONTEXT__

#include "mcd.h"
#include <stdlib.h>

#define DEFAULT_FPLM_FILENAME "fplm"



typedef struct {
  int help;
  int verbose;
  int debug_mode;
  char *program_name;
  char *conll_filename;
  char *fplm_filename;
  char *language;
  char *maca_data_path;
  char *mcd_filename;
  mcd *mcd_struct;
  int form_column;
  int pos_column;
} context;



context *context_new(void);
void context_free(context *ctx);

context *context_read_options(int argc, char *argv[]);
void context_general_help_message(context *ctx);
void context_conll_help_message(context *ctx);
void context_language_help_message(context *ctx);
void context_fplm_help_message(context *ctx);
void context_maca_data_path_help_message(context *ctx);
void context_mcd_help_message(context *ctx);
void context_form_column_help_message(context *ctx);
void context_pos_column_help_message(context *ctx);

#endif
