#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>

#include"util.h"
#include"hash.h"
#include"mcd.h"
#include"context.h"

void maca_lemmatizer_help_message(context *ctx)
{
  context_general_help_message(ctx);
  fprintf(stderr, "INPUT\n");
  context_conll_help_message(ctx);
  context_mcd_help_message(ctx);
  context_language_help_message(ctx);
  context_maca_data_path_help_message(ctx);
  context_fplm_help_message(ctx);
  context_form_column_help_message(ctx);
  context_pos_column_help_message(ctx);
}


void maca_lemmatizer_check_options(context *ctx){
  if(ctx->help
     /*!ctx->conll_filename*/
     /*     || !ctx->perc_model_filename
     || !ctx->mcd_filename
     || !ctx->vocabs_filename
     || !ctx->features_model_filename*/
     ){
    maca_lemmatizer_help_message(ctx);
    exit(1);
  }
}

char **read_fplm_file(char *fplm_filename, hash *form_pos_ht, int debug_mode)
{
  char form[1000];
  char pos[1000];
  char lemma[1000];  
  char morpho[1000];
  int num = 0;
  char **lemma_array;
  int lemma_array_size = 10000;
  char buffer[10000];
  FILE *f= myfopen(fplm_filename, "r");
  int fields_nb;

  lemma_array = (char **)memalloc(lemma_array_size * sizeof(char *));

  while(fgets(buffer, 10000, f)){
    
    fields_nb = sscanf(buffer, "%[^\t]\t%s\t%[^\t]\t%s\n", form, pos, lemma, morpho);
    /* if(!strcmp(form, "d")) */
    /* fprintf(stderr, "form = %s pos = %s lemma = %s\n", form, pos, lemma);   */
    if(fields_nb != 4){
      if(debug_mode){
	fprintf(stderr, "form = %s pos = %s lemma = %s\n", form, pos, lemma); 
	fprintf(stderr, "incorrect fplm entry, skipping it\n");
      }
      continue;
    }
    strcat(form, "/");
    strcat(form, pos);
    hash_add(form_pos_ht, strdup(form), num);

    if(num >= lemma_array_size){
      lemma_array_size = 2 * (lemma_array_size) + 1;
      lemma_array = (char **)realloc(lemma_array, (lemma_array_size) * sizeof(char *));
    }

    /* if(lemma_array[num] == NULL) */
    lemma_array[num] = strdup(lemma);
    num++;
  }
  /* fprintf(stderr, "%d entries loaded\n", num); */
  return lemma_array;
}
/*
char *to_lower_string(char *s)
{
  unsigned int i;
  for(i=0; i < strlen(s); i++)
    s[i] = tolower(s[i]);
  return s;
}
*/
/*
void print_word(char *input, mcd *mcd_struct, char *lemma)
{
  char *buffer = NULL;
  char *token = NULL;
  int col_nb = 0;
  if(mcd_get_lemma_col(mcd_struct) == -1){
    printf("%s\t%s\n", input, lemma);
  }
  else{
    buffer = strdup(input);
    token = strtok(buffer, "\t");
    col_nb = 0;
    while(token){
      if(col_nb != 0) printf("\t");
      if(col_nb == mcd_get_lemma_col(mcd_struct))
	printf("%s", lemma);
      else
	word_print_col_n(stdout, w->input, col_nb);
      col_nb++;
      token = strtok(NULL, "\t");
    }
    if(col_nb <= mcd_get_lemma_col(mcd_struct))
      printf("\t%s", lemma);
    printf("\n");
    free(buffer);
  }
}

*/


int main(int argc, char *argv[])
{
  hash *form_pos_ht = hash_new(1000000);
  char buffer[10000];
  char *buffer_copy;
  char *form;
  char *pos;
  char *feats;

  char *token;
  int column_nb;
  char form_pos[500];
  char *lemma;
  int index_form_pos;
  char **lemma_array;
  context *ctx;
  int form_column;
  int pos_column;
  int lemma_column;
  int feats_column;
  FILE *f = NULL;

  ctx = context_read_options(argc, argv);
  maca_lemmatizer_check_options(ctx);


  feats_column = ctx->mcd_struct->wf2col[MCD_WF_FEATS];

  
  if(ctx->pos_column != -1)
    pos_column = ctx->pos_column;
  else
    pos_column = ctx->mcd_struct->wf2col[MCD_WF_POS];


  if(ctx->form_column != -1)
    form_column = ctx->form_column;
  else
    form_column = ctx->mcd_struct->wf2col[MCD_WF_FORM];
  
  if(ctx->conll_filename == NULL)
    f = stdin;
  else
    f = myfopen(ctx->conll_filename, "r");

  lemma_column = ctx->mcd_struct->wf2col[MCD_WF_LEMMA];
  
  lemma_array = read_fplm_file(ctx->fplm_filename, form_pos_ht, ctx->debug_mode);
  
  /* look for a valid word */
  buffer_copy = strdup(buffer);
  while(fgets(buffer_copy, 10000, f)){
    if(feof(f)) return 0; /* no more words to read */
    if((buffer_copy[0] == '\n') || (buffer_copy[0] == ' ') || (buffer_copy[0] == '\t')){
      printf("\n");
      continue;
    }
    
    buffer_copy[strlen(buffer_copy)-1] = '\0';
    printf("%s", buffer_copy);
    token = strtok(buffer_copy, "\t");
    column_nb = 0;
    form = NULL;
    pos = NULL;
    lemma = NULL;
    feats = NULL;
    do{
      if(column_nb == lemma_column) /* lemma is present in the input file */
	if(strcmp(token, "_")) /* and it is not an underscore */
	  lemma = strdup(token);
      /* if((column_nb < ctx->mcd_struct->nb_col) && (column_nb == form_column)) */
      if(column_nb == form_column)
	form = strdup(token);
      /* if((column_nb < ctx->mcd_struct->nb_col) && (column_nb == pos_column)) */
      if(column_nb == pos_column){
	pos = strdup(token);
      }
      if(column_nb == feats_column){
	feats = strdup(token);
      }
      column_nb++;
    } while((token = strtok(NULL , "\t")));
    
    if(lemma == NULL){
      strcpy(form_pos, form);
      strcat(form_pos, "/");
      strcat(form_pos, pos);
      index_form_pos = hash_get_val(form_pos_ht, form_pos);
      if(index_form_pos != HASH_INVALID_VAL){
	lemma = lemma_array[index_form_pos];
      }
      else{
	to_lower_string(form_pos);
	index_form_pos = hash_get_val(form_pos_ht, form_pos);
	if(index_form_pos != HASH_INVALID_VAL){
	  lemma = lemma_array[index_form_pos];
	}
	else
	  if(ctx->verbose){
	    fprintf(stderr, "cannot find an entry for %s %s\n", form, pos);
	  }
	lemma = form;
      }
    }
    
    /* print_word(buffer, ctx->mcd_struct, lemma); */

    /*    printf("form = %s pos = %s (%s) feats = %s lemma = %s\n", form, pos, form_pos, feats, lemma); 
	  printf("form = %s pos = %s (%s) feats = %s lemma = %s\n", form, pos, form_pos, feats, lemma); */
    printf("\t%s\n", lemma);
    
    if(pos)free(pos);
    if(form)free(form);
    if(feats)free(feats);
  }
  /* free(buffer_copy); */
  free(lemma_array);
  hash_free(form_pos_ht);

  /*  if(ctx->conll_filename)
      fclose(f);*/
  
  return 0;
}

