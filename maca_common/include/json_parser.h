#ifndef __JSON_PARSER__
#define __JSON_PARSER__

#include "json_tree.h"

#define YYTEXT_MAX 5000
#define EPSILON 0

/* symboles non terminaux */
#define NB_NON_TERMINAUX 6

#define _structure_ 1
#define _list_structure_ 2 
#define _list_structure2_ 3
#define _attr_val_ 4
#define _list_attr_val_ 5
#define _list_attr_val2_ 6
//#define _list_ 2
//#define _object_ 3 

/* symboles terminaux */
#define NB_TERMINAUX 10

#define CROCHET_OUVRANT 1
#define CROCHET_FERMANT 2
#define VIRGULE 3
#define ACCOLADE_OUVRANTE 4
#define ACCOLADE_FERMANTE 5
#define COLON 6
#define STRING 7
#define NUMBER 8
#define CONSTANT 9
#define FIN 10

#define NB_MOTS_CLEFS 3

typedef struct {
  FILE *yyin;
  int uc; /* current token */
  int comment;
  char yytext[YYTEXT_MAX];
  int yyleng;
  /* Compter les lignes pour afficher les messages d'erreur avec numero ligne */
  int nb_ligne;
  int trace_xml;
  int premiers[NB_NON_TERMINAUX+1][NB_TERMINAUX+1];
  int suivants[NB_NON_TERMINAUX+1][NB_TERMINAUX+1];
  int indent_xml;
  int indent_step; // set to 0 for no indentation
  char *tableMotsClefs[NB_MOTS_CLEFS];
  int codeMotClefs[NB_MOTS_CLEFS];
}json_parser_ctx;


json_struct *json_parse(char *filename);
json_struct *json_parse_full(char *filename, int trace_xml);

json_struct *structure(json_parser_ctx *ctx);
json_parser_ctx *json_parser_init(char *filename);
json_parser_ctx *json_parser_init_full(char *filename, int trace_xml);



#endif
