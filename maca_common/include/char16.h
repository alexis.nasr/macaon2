#ifndef __CHAR16__
#define __CHAR16__

typedef unsigned short char16;

int     utf8_strlen(char *utf8_string);
char   *char16toutf8(char16 *char16_string);
int     char16_strlen(char16 *string);
char16 *utf8tochar16(char *utf8_string);

#endif
