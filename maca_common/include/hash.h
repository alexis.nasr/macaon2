#ifndef __HASH__
#define __HASH__

#define HASH_INVALID_VAL -1

typedef struct _cell
{
  char *key;
  int val;
  struct _cell *next; 
} cell;

typedef struct
{
  int size;
  int nbelem;
  cell **array;
} hash;


cell *cell_new(char *key, int val, cell *next);
void  cell_free(cell *c);

hash *hash_new(int size);
void  hash_free(hash *h);
cell *hash_lookup(hash *h, char *key);
int   hash_get_val(hash *h, char *key);
cell *hash_add(hash *h, char *key, int val);
void  hash_stats(hash *h);
void hash_inc_val(hash *h, char *key, int inc);


#endif
