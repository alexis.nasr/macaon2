#ifndef __FORM2POS__
#define __FORM2POS__

#include"hash.h"
#include"dico.h"

typedef struct
{
  int nbelem;
  int pos_nb;
  dico *d_pos;
  dico *d_signature;
  hash *h_form2signature;
} form2pos;


form2pos *form2pos_new(int nbelem, int pos_nb, char *pos_list);
void form2pos_free(form2pos *f2p);
form2pos *form2pos_read(char *filename);
int form2pos_get_signature(form2pos *f2p, char *form);
int form2pos_form_has_pos(form2pos *f2p, char *form, char *pos);
int form2pos_word_is_non_ambiguous(form2pos *f2p, char *form, char **pos);

#endif
