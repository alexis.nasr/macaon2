#ifndef __TRIE__
#define __TRIE__

#include<stdio.h>

typedef struct trans{
  int destination;
  int symbol;
  struct trans *next;
} trie_trans;

typedef struct {
  trie_trans *transitions;
  int is_accept;
  int fail;
} trie_state;

typedef struct {
  trie_state **states;
  int size;
  int states_nb;
} trie;

typedef struct {
  int state;
  int symbol;
} state_symbol;

typedef struct {
  int size;
  state_symbol *array;
  int nbelem;
} trie_path;




trie_state *trie_state_new(trie_trans *transitions, int is_accept);
void trie_state_free(trie_state *state);
trie *trie_new(void);
void trie_free(trie *t);
trie_trans *trie_trans_new(int destination, int symbol, trie_trans *next);
void trie_trans_free_rec(trie_trans *trans);
int trie_add_state(trie *t);
void trie_add_trans(trie *t, int origin, int symbol, int destination);
void trie_add_word(trie *t, int *word, int length);
void trie_print(FILE *f, trie *t);
int trie_lookup(trie *t, int *word, int length);
trie *trie_build_from_collection(char *filename);
int trie_destination_state(trie *t, int origin, int symbol);
#endif
