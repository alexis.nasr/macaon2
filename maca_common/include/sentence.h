#ifndef __SENTENCE__
#define __SENTENCE__

#include"word.h"
#include"util.h"
#include"mcd.h"

typedef struct {
  word **words;
  int length;
  mcd *mcd_struct;
  FILE *f;
} sentence;

sentence *sentence_copy(sentence *s);
sentence *sentence_new(mcd *m, FILE *f);
sentence *sentence_init(mcd *m, FILE *f);

void sentence_print(FILE *f, sentence *s, dico *dico_labels);
sentence *sentence_read(FILE *f, mcd *mcd_struct);
sentence *sentence_read_no_dummy_word(FILE *f, mcd *mcd_struct);
void sentence_add_word(sentence *s, word *w);
void sentence_free(sentence *s);

#endif
