#ifndef __DICO__
#define __DICO__

#include<stdio.h>
#include<stdlib.h>
#include"hash.h"

#define DICO_START_STR "##_DICO_START_##"
#define DICO_END_STR "##_DICO_END_##"

typedef struct
{
  char *name;
  int nbelem;
  int array_size;
  hash *htable;
  char **array;
} dico;

dico *dico_new(char *name, int size);
void  dico_free(dico *d);
int   dico_add(dico *d, char *key);
char *dico_int2string(dico *d, int val);
int   dico_string2int(dico *d, char *string);
void  dico_print(char *filename, dico *d);
void  dico_print_fh(FILE *f, dico *d);
dico *dico_read(char *filename, float ratio);
dico *dico_read_fh(FILE *f, float ratio);
dico *dico_extract_from_corpus(char *filename, int column, char *dico_name);

#endif
