#ifndef __JSON_TREE__
#define __JSON_TREE__

#define JSON_LIST 1
#define JSON_STRING 2
#define JSON_NUMBER 3
#define JSON_CONSTANT 4
#define JSON_AVL 5 /* attribute value list */ 

#define JSON_CONST_TRUE 1
#define JSON_CONST_FALSE 2
#define JSON_CONST_NULL 3

typedef struct _json_struct_ json_struct;
typedef struct _json_struct_list_ json_struct_list;
typedef struct _json_attr_val_ json_attr_val;

struct _json_struct_ {
  int type;
  union {
    json_attr_val *attr_val_list;
    char          *string;
    int            constant;
    float          number;
    json_struct   *first;
  } u;
  struct _json_struct_ *next;
};

/*
typedef struct _json_object_ {
  json_attr_val *list;
} json_object;
*/
/*
struct _json_struct_list_ {
  int          nbelem;
  json_struct *first;
  json_struct *last;
  };*/

struct _json_attr_val_ {
  char                   *attr;
  json_struct            *val;
  struct _json_attr_val_ *next;
};

json_struct *json_new_string(char *string);
json_struct *json_new_number(float number);
json_struct *json_new_constant(int constant);
json_attr_val *json_new_attr_val(char *attr, json_struct *s, json_attr_val *next);
//json_struct *json_new_object(json_attr_val *avl);
json_struct *json_new_avl(json_attr_val *avl);
json_struct *json_new_list(json_struct *s);


void json_print_struct(FILE *f, json_struct *s);
void json_print_avl(FILE *f, json_struct *s);
void json_print_list(FILE *f, json_struct *s);
void json_print_string(FILE *f, json_struct *s);
void json_print_number(FILE *f, json_struct *s);
void json_print_constant(FILE *f, json_struct *s);
void json_free_struct(json_struct *s);


#endif
