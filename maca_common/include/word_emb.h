#ifndef __WORD_EMB__
#define __WORD_EMB__

#include"hash.h"
#include"dico.h"

typedef struct
{
  int dim;
  int nbelem;
  hash *htable;
  float *array;
} word_emb;

word_emb *word_emb_new(int dim, int nbelem);
void      word_emb_free(word_emb *we);

word_emb *word_emb_load(char *filename);
float    *word_emb_get_vector(word_emb *we, char *word);
int       word_emb_get_code(word_emb *we, char *word);
int       word_emb_fill_input_array_dnn(float *input_array, word_emb *we, int code, int first_index);
void      word_emb_print(FILE *f, word_emb *we, int code);

int       word_emb_number_of_lines_in_file(char *filename);
int       word_emb_number_of_columns_in_file(char *filename);

word_emb *word_emb_load_w2v_file(char *filename);
word_emb *word_emb_load_w2v_file_filtered(char *file_name, dico *d);
void      word_emb_print_to_file(word_emb *we, char *filename);


#endif
