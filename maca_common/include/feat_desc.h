#ifndef __FEAT_DESC__
#define __FEAT_DESC__

typedef int (*feat_fct) (void *c);

typedef struct {
  char *name;
  feat_fct fct;
  int type;
  int range;
} simple_feat_desc;

typedef struct {
  int nbelem;
  simple_feat_desc **array;
} feat_desc;


#include "util.h"


feat_desc        *feat_desc_new(void);
void              feat_desc_free(feat_desc *fd);
feat_desc        *feat_desc_add(feat_desc *fd, simple_feat_desc *sfd);
simple_feat_desc *simple_feat_desc_new(char *name, int type, feat_fct fct);
void              simple_feat_desc_free(simple_feat_desc *sfd);

#endif


