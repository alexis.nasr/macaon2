#ifndef __FPLM__
#define __FPLM__

#include<stdio.h>
#include<stdlib.h>
#include"hash.h"

typedef struct
{
  char **lemma_array;
  int    lemma_array_size;
  hash  *form_pos_ht;
  int    nbelem;
} fplm_struct;

fplm_struct *fplm_new();
void         fplm_free(fplm_struct *fplm);
fplm_struct *fplm_load_file(char *fplm_filename, int debug_mode);
char        *fplm_lookup_lemma(fplm_struct *fplm, char *form, char *pos, int verbose);
void         fplm_add(fplm_struct *fplm, char *form, char *pos, char *lemma);
#endif
