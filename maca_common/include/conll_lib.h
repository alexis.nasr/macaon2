/*******************************************************************************
    Copyright (C) 2010 by Alexis Nasr <alexis.nasr@lif.univ-mrs.fr>
                      and Joseph Le Roux <joseph.le.roux@gmail.com>
    conll_lib is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    conll_lib is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with conll_lib. If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#ifndef __CONLL_LIB__
#define __CONLL_LIB__

#include <stdio.h>
#include "hash_str.h"

#define MAX_WORDS_IN_SENTENCE 1000
#define MAX_STR 10000
#define MAX_LINE_LENGTH 50000

#define INCORRECT_SENTENCE_NUM_VALUE -1
#define INCORRECT_PARSE_NUM_VALUE -1
#define INCORRECT_LOGPROB_VALUE 10
#define INCORRECT_ORACLE_VALUE -1
#define INCORRECT_CONF_MEAS -1
#define INCORRECT_LEX_AFF -1

typedef struct w
{
  unsigned id;   /* Token counter, starting at 1 for each new sentence.*/
  char form[MAX_STR];   /* Word form or punctuation symbol.*/
  char lemma[MAX_STR];  /* Lemma or stem (depending on particular data set) of word form,*/
                 /* or an underscore if not available.*/
  char cpostag[MAX_STR];/* Coarse-grained part-of-speech tag, where tagset depends on the language.*/
  char postag[MAX_STR]; /* Fine-grained part-of-speech tag, where the tagset depends on the language,*/
                 /* or identical to the coarse-grained part-of-speech tag if not available.*/
  char feats[MAX_STR];  /* Unordered set of syntactic and/or morphological features (depending on the particular language)*/
                 /*, separated by a vertical bar (|), or an underscore if not available.*/
  int head; /* Head of the current token, which is either a value of ID or zero ('0').*/
  char deprel[MAX_STR];  /* Dependency relation to the HEAD. The set of dependency relations depends on the particular language.*/
                 /* Note that depending on the original treebank annotation, the dependency relation may be meaningful or simply 'ROOT'.*/
  unsigned phead;/* Projective head of current token, which is either a value of ID or zero ('0'), or an underscore if not available. */
                 /* The dependency structure resulting from the PHEAD column is guaranteed to be projective */
                 /* whereas the structures resulting from the HEAD column will be non-projective for some sentences */
  char pdeprel[MAX_STR]; /* Dependency relation to the PHEAD, or an underscore if not available. */
                 /* Note that depending on the original treebank annotation, the dependency relation may be meaningful or simply 'ROOT'.*/
  char language[MAX_STR]; /* Language identifier */

  double score; /* score of the dependency, not in the 2007 conll format */
  double lex_aff; /*  lexical affinity of the dependent and the governor, not in the 2007 conll format */

  struct w * mother;
  struct w * daughters[MAX_WORDS_IN_SENTENCE];
  unsigned daughters_nb;
  double conf_meas;
} conll_word;


typedef struct
{
  conll_word * root;
  conll_word * words[MAX_WORDS_IN_SENTENCE];
  unsigned l; /* sentence length */
  unsigned num; /* sentence number */
} conll_sentence;


conll_sentence *conll_allocate_sentence(void);
void            conll_renumber_sentence(conll_sentence *s);
void            conll_reset_sentence(conll_sentence *s);
void            conll_free_sentence(conll_sentence *s);
int             conll_load_sentence(FILE *f, conll_sentence *s);
void            conll_print_sentence(conll_sentence *s);
void            conll_print_sentence_mcf(conll_sentence *s, int coarse_pos);
void            conll_print_sentence_mcf2(conll_sentence *s, int print_id, int print_form, int print_lemma, int print_cpostag, int print_postag, int print_feats, int print_head, int print_deprel);
void            conll_print_sentence_mcf3(conll_sentence *s, char *columns, int nb_col);
void            conll_compact_sentence(conll_sentence *s);
conll_word     *conll_allocate_word(unsigned id, char *form, char *lemma, char *cpostag, char *postag, char *feats, unsigned head, char *deprel);
conll_word     *conll_copy_word(conll_word *w);
void            conll_add_daughter(conll_word *daughter, conll_word *mother);
void            conll_remove_daughter(conll_sentence *s, int i);
void            conll_remove_word_rec(conll_sentence *s, int i);
void            conll_remove_subtree(conll_sentence *s, int root);
void            conll_add_word(conll_sentence *s, conll_word *w, int pos, conll_word *gov);
void            conll_split_node_in_two(conll_sentence *s, int pos, conll_word *gov, conll_word *dep, int pos_gov, int pos_dep);
void            conll_change_pos(conll_sentence *s, hash_str *h_pos);
void            conll_change_cpos(conll_sentence *s, hash_str *h_cpos);
void            conll_change_fct(conll_sentence *s, hash_str *h_fct);
int             conll_is_num(char *s);
void            conll_renumber_sentence_offset(conll_sentence *s, int offset);
void            conll_compute_relative_index_of_heads(conll_sentence *s);



#endif
