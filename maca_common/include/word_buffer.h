#ifndef __WORD_BUFFER__
#define __WORD_BUFFER__

#include<stdio.h>
#include"word.h"
#include"mcd.h"


#define word_buffer_get_size(wb) (wb)->size
#define word_buffer_get_nbelem(wb) (wb)->nbelem
#define word_buffer_get_lookahead(wb) (wb)->lookahead
#define word_buffer_get_current_index(wb) (wb)->current_index
#define word_buffer_get_input_file(wb) (wb)->input_file
#define word_buffer_get_mcd(wb) (wb)->mcd_struct

/* #define word_buffer_b0(wb) (((wb)->nbelem == 0)? NULL : (wb)->array[(wb)->current_index]) */
#define word_buffer_b0(wb) (((wb)->current_index     >= (wb)->nbelem)? NULL : (wb)->array[(wb)->current_index])
#define word_buffer_b1(wb) (((wb)->current_index + 1 >= (wb)->nbelem)? NULL : (wb)->array[(wb)->current_index + 1])
#define word_buffer_b2(wb) (((wb)->current_index + 2 >= (wb)->nbelem)? NULL : (wb)->array[(wb)->current_index + 2])
#define word_buffer_b3(wb) (((wb)->current_index + 3 >= (wb)->nbelem)? NULL : (wb)->array[(wb)->current_index + 3])
#define word_buffer_bm1(wb) (((wb)->current_index - 1 < 0)? NULL : (wb)->array[(wb)->current_index - 1])
#define word_buffer_bm2(wb) (((wb)->current_index - 2 < 0)? NULL : (wb)->array[(wb)->current_index - 2])
#define word_buffer_bm3(wb) (((wb)->current_index - 3 < 0)? NULL : (wb)->array[(wb)->current_index - 3])
#define word_buffer_bmi(wb,i) (((wb)->current_index - (i) < 0)? NULL : (wb)->array[(wb)->current_index - (i)])

#define word_buffer_nb_elts_right(wb) ((wb)->nbelem - (wb)->current_index - 1)
#define word_buffer_nb_elts_left(wb) ((wb)->current_index)


#define word_buffer_end(wb)      (((wb)->current_index >= (wb)->nbelem)? 1 : 0)
#define word_buffer_is_last(wb)  (((wb)->current_index == (wb)->nbelem - 1)? 1 : 0)
#define word_buffer_is_empty(wb) (((wb)->nbelem == 0)? 1 : 0)

typedef struct {
  int    size;           /* size of the array used to store words */
  int    nbelem;         /* number of words in the buffer */
  int    lookahead;      /* number of words between the current word and the last word of the buffer */
  int    current_index;  /* position of the current word */
  word **array;          /* array to store words */
  FILE  *input_file;     /* file to read the words from */
  mcd   *mcd_struct;     /* mcd describing the format of input_file */
} word_buffer;


word_buffer *word_buffer_new(FILE *input_file, mcd *mcd_struct, int lookahead);
void         word_buffer_free(word_buffer *wb);
int          word_buffer_add(word_buffer *wb, word *w);
void         word_buffer_insert(word_buffer *wb, word *w, int index);
void         word_buffer_rm(word_buffer *wb, int index);
word*        word_buffer_get_word_relative(word_buffer *wb, int dist);
word*        word_buffer_get_word_n(word_buffer *wb, int n);
int          word_buffer_read_next_word(word_buffer *wb);
int          word_buffer_move_right(word_buffer *wb);
int          word_buffer_move_left(word_buffer *wb);
void         word_buffer_print(FILE *f, word_buffer *wb);
void         word_buffer_print_compact(FILE *f, word_buffer *wb);
int          word_buffer_read_sentence(word_buffer *bw);
word_buffer *word_buffer_load_mcf(char *mcf_filename, mcd *mcd_struct);
int          word_buffer_locate_token_with_offset(word_buffer *wb, int offset);

word        *word_buffer_get_rightmost_child_of_s0(word_buffer *wb, word *gov);
word        *word_buffer_get_rightmost_descendent_of_s0(word_buffer *wb, word *root);



/*
int          word_buffer_is_empty(word_buffer *wb);
int          word_buffer_is_last(word_buffer *wb);
int          word_buffer_end(word_buffer *wb);
*/

#endif
