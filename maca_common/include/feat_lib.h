#ifndef __FEAT_LIB__
#define __FEAT_LIB__

#include "feat_desc.h"
#include "dico.h"

typedef struct {
  int                nbelem;
  dico              *d_features;
  simple_feat_desc **array;
} feat_lib;

feat_lib         *feat_lib_new(void);
void              feat_lib_add(feat_lib *fl, int feature_type, char *feature_name, feat_fct feature_fct);
simple_feat_desc *feat_lib_get_simple_feat_desc(feat_lib *fl, char *feat_name);
feat_lib         *feat_lib_build(void);
void              feat_lib_free(feat_lib *fl);
#endif
