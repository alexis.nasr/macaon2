#ifndef __UTIL__
#define __UTIL__

#include<stdlib.h>
#include<stdio.h>

void myfree(void *ptr);
void *memalloc(size_t s);
FILE *myfopen(const char *path, const char *mode);
FILE *myfopen_no_exit(const char *path, const char *mode);
char *to_lower_string(char *s);
#endif
