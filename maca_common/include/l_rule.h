#ifndef __L_RULE__
#define __L_RULE__

char *apply_l_rule(char *form, char *l_rule);
char *compute_l_rule(char *lemma, char *form, int strict);
int l_rule_is_applicable(char *form, char *l_rule);

#endif
