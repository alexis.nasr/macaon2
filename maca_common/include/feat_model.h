#ifndef __FEAT_MODEL__
#define __FEAT_MODEL__

#include "feat_desc.h"
#include "feat_lib.h"

typedef struct {
  char string[2048];
  char *name;
  int nbelem;
  feat_desc **array;
  int dim;
  feat_lib *fl; // stores all simple features 
} feat_model;

/* #include "config.h" */
#include "dico.h"
#include "mcd.h"

void        feat_model_free(feat_model *fm);
feat_model *feat_model_new(char *name);
void        feat_model_print(FILE *f, feat_model *fm);
feat_desc  *feat_model_add(feat_model *fm, feat_desc *fd);
feat_model *feat_model_read(char *filename, feat_lib *fl, int verbose);
void        feat_model_compute_ranges(feat_model *fm, mcd *m, int mvt_nb);
int         feat_model_get_type_feat_n(feat_model *fm, int n);
void        catenate_int(char *string, int val);
#endif
