#ifndef __HASH_STR__
#define __HASH_STR__

#define HASH_STR_INVALID_VAL NULL

typedef struct _hash_str_cell
{
  char *key;
  char *val;
  struct _hash_str_cell *next; 
} hash_str_cell;

typedef struct
{
  int size;
  int nbelem;
  hash_str_cell **array;
} hash_str;


hash_str_cell *hash_str_cell_new(char *key, char *val, hash_str_cell *next);
void hash_str_cell_free(hash_str_cell *c);

hash_str *hash_str_new(int size);
void hash_str_free(hash_str *h);
hash_str_cell *hash_str_lookup(hash_str *h, char *key);
char *hash_str_get_val(hash_str *h, char *key);
void hash_str_add(hash_str *h, char *key, char *val);
void hash_str_stats(hash_str *h);


#endif
