#ifndef __DICO_VEC__
#define __DICO_VEC__
#include "dico.h"




typedef struct {
  int nb;
  dico **t;
  hash *ht;
} dico_vec;

dico_vec *dico_vec_new(void);
void dico_vec_free(dico_vec *dv);
int dico_vec_add(dico_vec *dv, dico *d);
void dico_vec_empty(dico_vec *dv);
void dico_vec_print(char *filename, dico_vec *dv);
dico_vec *dico_vec_read(char *filename, float ratio);
dico *dico_vec_get_dico(dico_vec *dv, char *dico_name);
dico_vec *dico_vec_replace_dico(dico_vec *dv, dico *old_dico, dico *new_dico);

#endif
