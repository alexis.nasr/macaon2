#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include "util.h"


int l_rule_is_applicable(char *form, char *l_rule)
{
  int i,j;
  int sep_index;
  int form_suffix_length;
  int form_length = strlen(form);
  int l_rule_length = strlen(l_rule);

  //  printf("in is_applicable form = %s lrune = %s\n", form, l_rule);
  
  for(sep_index=1; sep_index < l_rule_length; sep_index++)
    if(l_rule[sep_index] == '@')
      break;
  
  form_suffix_length = sep_index - 1;
  
  for(j=1, i=form_length - form_suffix_length; j < sep_index; i++, j++){
    //    printf("l_rule[%d] = %c (%d) form[%d] = %c (%d)\n", j, l_rule[j],l_rule[j], i, form[i], form[i]);
    if((l_rule[j] != '*') && (l_rule[j] != form[i]))
      //  if(l_rule[j] != form[i])
    return 0;
  }
  //  printf("rule can be applied\n");
    return 1;
}

char *apply_l_rule(char *form, char *l_rule)
{
  int i,j;
  int sep_index;
  int lemma_suffix_length;
  int form_suffix_length;
  char *lemma = NULL;
  int form_length = strlen(form);
  int l_rule_length = strlen(l_rule);
  int lemma_length;
  
  for(sep_index=1; sep_index < l_rule_length; sep_index++)
    if(l_rule[sep_index] == '@')
      break;
  
  lemma_suffix_length = l_rule_length - 1 - sep_index;
  form_suffix_length = sep_index - 1;

  lemma_length = form_length + lemma_suffix_length - form_suffix_length;
  if(lemma_length < form_length)
    lemma_length = form_length;
  lemma = (char *) memalloc((lemma_length + 1) * sizeof(char)); 
  strcpy(lemma, form);

  for(j=0, i=form_length - form_suffix_length; j < lemma_suffix_length; i++, j++){
    lemma[i] = l_rule[sep_index + j + 1];
  }
  lemma[i] = 0;
  return lemma;
}
  

char *compute_l_rule(char *lemma, char *form, int strict)
{
  int breakpoint,j,k;
  int lemma_suffix_length;
  int form_suffix_length;
  int lemma_length = strlen(lemma);
  int form_length = strlen(form);
  char *l_rule;
  
  for(breakpoint=0; (breakpoint < lemma_length) && (breakpoint < form_length); breakpoint++)
    if(form[breakpoint] != lemma[breakpoint])
      break;

  lemma_suffix_length = lemma_length - breakpoint;
  form_suffix_length = form_length - breakpoint;

  //  printf("lemma suffix length = %d form suffix length = %d\n", lemma_suffix_length, form_suffix_length);
  l_rule = (char *)memalloc((lemma_suffix_length + form_suffix_length + 3) * sizeof(char));

  j = 0;
  l_rule[j++] = '@'; 
  for(k=0; k < form_suffix_length; k++)
    l_rule[j++] = strict? form[breakpoint + k] : '*';
  l_rule[j++] = '@';
  for(k=0; k < lemma_suffix_length; k++)
    l_rule[j++] = lemma[breakpoint + k];
  l_rule[j] = 0;
  return l_rule;
}

