#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<ctype.h>

void myfree(void *ptr)
{
  if(ptr) free(ptr);
}

void *memalloc(size_t s)
{
  void *p = malloc(s);
  if(p == NULL){
    fprintf(stderr, "memory allocation problem\n");
    exit(1);
  }
  return p;
}

FILE *myfopen(const char *path, const char *mode)
{
  FILE *f = fopen(path, mode);
  if(f == NULL){
    fprintf(stderr, "cannot open file %s\n", path);
    exit(1);
  }
  return f;
}

FILE *myfopen_no_exit(const char *path, const char *mode)
{
  FILE *f = fopen(path, mode);
  if(f == NULL){
    fprintf(stderr, "cannot open file %s\n", path);
  }
  return f;
}

char *to_lower_string(char *s)
{
  size_t i;
  size_t l = strlen(s);
  for(i=0; i < l; i++)
    s[i] = tolower(s[i]);
  return s;
}
