#include<stdio.h>
#include"word_buffer.h"
#include"util.h"

word_buffer *word_buffer_new(FILE *input_file, mcd *mcd_struct, int lookahead)
{
  int i;
  word_buffer *wb = (word_buffer *)memalloc(sizeof(word_buffer));
  wb->input_file = input_file;
  wb->mcd_struct = mcd_struct;
  wb->size = 10;
  wb->nbelem = 0;
  wb->array = (word **)memalloc(wb->size * sizeof(word *));
  wb->current_index = 0;

  /* load lookahead next words */
  wb->lookahead = lookahead;
  for(i=0; i <= lookahead; i++)
    word_buffer_read_next_word(wb);
  
  return wb;
}

void word_buffer_print(FILE *f, word_buffer *wb)
{
  word *w;
  w = word_buffer_bm3(wb);
  if(w){ fprintf(f, "[-3] "); word_print(f, w); fprintf(f, "\n");} 
  w = word_buffer_bm2(wb);
  if(w){ fprintf(f, "[-2] "); word_print(f, w); fprintf(f, "\n");} 
  w = word_buffer_bm1(wb);
  if(w){ fprintf(f, "[-1] "); word_print(f, w); fprintf(f, "\n");} 
  w = word_buffer_b0(wb);
  if(w){ fprintf(f, "[ 0] "); word_print(f, w); fprintf(f, "\n");} 
  w = word_buffer_b1(wb);
  if(w){ fprintf(f, "[ 1] "); word_print(f, w); fprintf(f, "\n");} 
  w = word_buffer_b2(wb);
  if(w){ fprintf(f, "[ 2] "); word_print(f, w); fprintf(f, "\n");} 
  w = word_buffer_b3(wb);
  if(w){ fprintf(f, "[ 3] "); word_print(f, w); fprintf(f, "\n");} 
}

void word_buffer_print_compact(FILE *f, word_buffer *wb)
{
  word *w;
  w = word_buffer_bm3(wb);
  if(w){ fprintf(f, "%d:%s ", word_get_index(w), w->form);}
  w = word_buffer_bm2(wb);
  if(w){ fprintf(f, "%d:%s ", word_get_index(w), w->form);}
  w = word_buffer_bm1(wb);
  if(w){ fprintf(f, "%d:%s ", word_get_index(w), w->form);}
  w = word_buffer_b0(wb);
  if(w){ fprintf(f, "[%d:%s] ", word_get_index(w), w->form);}
  w = word_buffer_b1(wb);
  if(w){ fprintf(f, "%d:%s ", word_get_index(w), w->form);}
  w = word_buffer_b2(wb);
  if(w){ fprintf(f, "%d:%s ", word_get_index(w), w->form);}
  w = word_buffer_b3(wb);
  if(w){ fprintf(f, "%d:%s ", word_get_index(w), w->form);}
  fprintf(f, "\n");
}

void word_buffer_free(word_buffer *wb)
{
  int i;

  for(i=0; i < wb->nbelem; i++){
    if(wb->array[i])
      word_free(wb->array[i]);
  }
  free(wb->array);
  free(wb);
}

/* remove word at position index */
void word_buffer_rm(word_buffer *wb, int index)
{
  int i;
  if((index < 0) || (index >= wb->nbelem)) {
    fprintf(stderr, "cannot remove word %d, index out of range\n", index);
    return;
  }
  /* check if word at index has daughters */
  for(i=0; i < wb->nbelem; i++){
    if(word_get_gov_index(word_buffer_get_word_n(wb, i)) == index){
      fprintf(stderr, "cannot remove word %d, it has at least one daughter", index);
      return;
    }
  }
  /* decrease dependencies length whenever gov and dep are on different sides of index */
  for(int dep_index=0; dep_index < wb->nbelem; dep_index++){
    word *dep = word_buffer_get_word_n(wb, dep_index);
    int gov_index = word_get_gov_index(dep);
    if((dep_index < index && gov_index > index)
       || (dep_index > index && gov_index < index)){
	word_set_gov(dep, word_get_gov(dep) - 1); 
      }
  }
  word_free(wb->array[index]);
  wb->array[index] = NULL;
  for(i=index+1; i < wb->nbelem; i++){
    wb->array[i-1] = wb->array[i];
  }
  wb->nbelem--;
}

/* insert word w at position index */
void word_buffer_insert(word_buffer *wb, word *w, int index)
{
  if(wb->nbelem == wb->size -1){
    wb->size = 2 * (wb->size + 1);
    wb->array = (word **)realloc(wb->array, wb->size * sizeof(word *));
  }

  /* increase dependencies length whenever gov and dep are on different sides of index */
  for(int dep_index=0; dep_index < wb->nbelem; dep_index++){
    word *dep = word_buffer_get_word_n(wb, dep_index);
    int gov_index = word_get_gov_index(dep);
    if((dep_index < index && gov_index >= index)
       || (dep_index >= index && gov_index < index)){
	word_set_gov(dep, word_get_gov(dep) + 1); 
      }
  }
  
  for(int i=wb->nbelem; i >= index; i--){
    wb->array[i] = wb->array[i-1];
  }
  wb->array[index] = w;
  wb->nbelem++;
  
}



int word_buffer_add(word_buffer *wb, word *w)
{
  if(wb->nbelem == wb->size -1){
    wb->size = 2 * (wb->size + 1);
    wb->array = (word **)realloc(wb->array, wb->size * sizeof(word *));
  }
  wb->array[wb->nbelem] = w;
  word_set_index(w, wb->nbelem);
  w->rspan = w->lspan = wb->nbelem;
  wb->nbelem++;
  return wb->nbelem - 1;
}

word *word_buffer_get_word(word_buffer *wb, int offset)
{
  return ((wb->current_index + offset >=0) && (wb->current_index + offset <= wb->nbelem))? wb->array[wb->current_index + offset] : NULL;
}

word *word_buffer_get_word_n(word_buffer *wb, int n)
{
  return ((n >=0) && (n < wb->nbelem))? wb->array[n] : NULL;
}

word_buffer *word_buffer_load_mcf(char *mcf_filename, mcd *mcd_struct)
{
  FILE *f;

  if(mcf_filename == NULL)
    f = stdin;
  else
    f = myfopen(mcf_filename, "r");
  word_buffer *wb = word_buffer_new(f, mcd_struct, 0);
  while(word_buffer_read_next_word(wb)){
    /* printf("load word %d\n", wb->nbelem - 1); */
  }
  //if(mcf_filename != NULL)
  //  fclose(f);
  return wb;
}

int word_buffer_read_next_word(word_buffer *wb)
{
  word *w = word_read(wb->input_file, wb->mcd_struct);
  if(w == NULL) return 0;
  word_buffer_add(wb, w);  
  return 1;
}

int word_buffer_move_right(word_buffer *wb)
{
  if((wb->nbelem - 1 - wb->current_index) <= wb->lookahead)
    word_buffer_read_next_word(wb);
  if(wb->current_index >= wb->nbelem) return 0; 
  wb->current_index++;
  return 1;
}

int word_buffer_move_left(word_buffer *wb)
{
  if(wb->current_index > 0){
    wb->current_index--;
    return 1;
  }
  return 0;
}

int word_buffer_read_sentence(word_buffer *wb)
{
  char buffer[10000];
  word *w = NULL;
  int index = 1;

  while(fgets(buffer, 10000, word_buffer_get_input_file(wb))){
    if((buffer[0] == '\n') || (buffer[0] == ' ') || (buffer[0] == '\t')) continue; /* ignore empty lines */
    if(feof(word_buffer_get_input_file(wb))) break;
    w = word_parse_buffer(buffer, word_buffer_get_mcd(wb));
    word_set_index(w, index);
    index++;
    word_buffer_add(wb, w);
    if(word_is_eos(w, word_buffer_get_mcd(wb))) break;
  }
  return wb->nbelem ; 
}

int word_buffer_locate_token_with_offset(word_buffer *wb, int offset)
{
  int c, first, last, middle;
  word *w_middle;
  first = 0;
  last = wb->nbelem - 1;
  middle = (first+last)/2;
  
  while (first <= last) {
    //        printf("first = %d middle = %d last = %d\n", first, middle, last);
    w_middle = word_buffer_get_word_n(wb, middle);
    //    printf("w middle = %d current offset = %d\n", w_middle, word_get_offset(w_middle));
    if (word_get_offset(w_middle) < offset)
      first = middle + 1;
    else if (word_get_offset(w_middle) == offset) {
      //      printf("%d found at location %d.\n", offset, middle+1);
      break;
    }
    else
      last = middle - 1;
    middle = (first + last)/2;
  }
  if (first > last){
    
    //    printf("Not found! %d is not present in the list.\n", offset);
    return -1;
  }
  return middle;
} 


word *word_buffer_get_rightmost_child_of_s0(word_buffer *wb, word *gov)
{
  word *bm1 = word_buffer_bm1(wb);
  int rightmost_child_index = word_get_index(gov);
  for(int current_index = word_get_index(gov); current_index <= word_get_index(bm1); current_index++)
    if(word_get_gov_index(word_buffer_get_word_n(wb, current_index)) == word_get_index(gov))
      rightmost_child_index = current_index;
  return word_buffer_get_word_n(wb, rightmost_child_index);
}


word *word_buffer_get_rightmost_descendent_of_s0(word_buffer *wb, word *root)
{
  word *rightmost_descendent = root;
  word *rightmost_child = NULL;
  int change = 1;

  while(change){
    change = 0;
    rightmost_child = word_buffer_get_rightmost_child_of_s0(wb, rightmost_descendent);
    if(word_get_index(rightmost_child) > word_get_index(rightmost_descendent)){
      rightmost_descendent = rightmost_child;
      change = 1;
    }
  }

  return rightmost_descendent;
}



/*int word_buffer_end(word_buffer *wb)
{
  return (wb->current_index >= wb->nbelem)? 1 : 0;
}

int word_buffer_is_last(word_buffer *wb)
{
  return (wb->current_index == wb->nbelem - 1)? 1 : 0;
}

int word_buffer_is_empty(word_buffer *wb)
{
  return (wb->nbelem == 0)? 1 : 0;
}
*/
