#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"dico_vec.h"
#include"util.h"


dico_vec *dico_vec_new(void)
{
  dico_vec *dv = (dico_vec *)memalloc(sizeof(dico_vec));
  dv->nb = 0;
  dv->t = NULL;
  dv->ht = hash_new(20);
  return dv;
}


void dico_vec_free(dico_vec *dv)
{
  if(dv){
      if (dv->ht)
	  hash_free(dv->ht);

      if(dv->t)
	  free(dv->t);
      free(dv);
  }
}

dico *dico_vec_get_dico(dico_vec *dv, char *dico_name)
{
  int dico_nb = hash_get_val(dv->ht, dico_name);
  if(dico_nb == HASH_INVALID_VAL) return NULL;
  return dv->t[dico_nb];
}

int dico_vec_add(dico_vec *dv, dico *d)
{
  //  char *dico_name = NULL;
  int dico_nb = dv->nb;
  dv->nb++;
  dv->t = (dico **)realloc(dv->t, dv->nb * sizeof(dico *));
  dv->t[dico_nb] = d;
  //  if(d->name) dico_name = strdup(d->name);

  hash_add(dv->ht, d->name, dico_nb);

  return dv->nb;
}

void dico_vec_empty(dico_vec *dv)
{
  dv->nb = 0;
}


void dico_vec_print(char *filename, dico_vec *dv){
  int i;
  FILE  *f;
  if(filename == NULL){
    f = stdout;
  }
  else{
    f= fopen(filename, "w");
    if(f == NULL){
      fprintf(stderr, "cannot open file %s\n", filename);
      exit(1);
    }
  }
  for(i=0; i < dv->nb; i++){
    dico_print_fh(f, dv->t[i]);
    if(i != dv->nb - 1){
      fprintf(f, DICO_END_STR);
      fprintf(f, "\n");
    }

  }
  fclose(f);
}

dico_vec *dico_vec_replace_dico(dico_vec *dv, dico *old_dico, dico *new_dico)
{
  int dico_nb = hash_get_val(dv->ht, old_dico->name);
  if(dico_nb == HASH_INVALID_VAL) return dv;

  dv->t[dico_nb] = new_dico;
  return dv;
}

dico_vec *dico_vec_read(char *filename, float ratio)
{
  FILE *f;
  dico_vec *dv = dico_vec_new();

  f= fopen(filename, "r");
  if(f == NULL){
    fprintf(stderr, "cannot open file %s\n", filename);
    exit(1);
  }

  while(!feof(f)){
    dico_vec_add(dv, dico_read_fh(f, ratio));
  }
  fclose(f);
  return dv;
}
