#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"sentence.h"
#include"dico.h"

sentence *sentence_new(mcd *m, FILE *f)
{
  sentence *s = (sentence *)memalloc(sizeof(sentence));
  s->mcd_struct = m;
  s->length = 0;
  s->words = NULL;
  s->f = f;
  return s;
}

sentence *sentence_init(mcd *m, FILE *f)
{
  sentence *s = sentence_new(m, f);
  /* sentence_add_word(s, word_create_dummy(m));  */
  return s;
}

sentence *sentence_copy(sentence *s)
{
  int d;
  sentence *copy = sentence_new(s->mcd_struct, s->f);
  for(d=0; d < s->length; d++)
    sentence_add_word(copy, word_copy(s->words[d]));
  return copy;
}

void sentence_print(FILE *f, sentence *s, dico *dico_labels)
{
  int i;


  for(i=0; i < s->length; i++){
    fprintf(f, "%d\t", i);
    word_print(f, s->words[i]);
    fprintf(f, "\n");
  }    
  fprintf(f, "\n");
}

void sentence_add_word(sentence *s, word *w)
{
  s->length++;
  s->words = (word **)realloc(s->words, s->length * sizeof(word *));
  s->words[s->length -1] = w;
  word_set_index(w, s->length -1);
}

void sentence_free(sentence *s)
{
  int i;
  if(s){
    if(s->length){ 
      for(i=0; i < s->length; i++)
	word_free(s->words[i]);
      free(s->words);
    }
    free(s);
  }
}

sentence *sentence_read(FILE *f, mcd *mcd_struct)
{
  sentence *s = sentence_init(mcd_struct, f); 
  char buffer[1000];
  word *w = NULL;
  
  while(fgets(buffer, 1000, f)){
    /* printf("buffer = %s\n", buffer); */
    if(feof(f)) break;
    if((buffer[0] == '\n') || (buffer[0] == ' ')) break; /* end of the sentence indicated by empty line */
    w = word_parse_buffer(buffer, mcd_struct);
    if(w) sentence_add_word(s, w);
    if(word_is_eos(w, mcd_struct)) break;
  }
  
  
  if(s->length == 1){
    sentence_free(s);
    return NULL;
  }
  return s;
}

sentence *sentence_read_no_dummy_word(FILE *f, mcd *mcd_struct)
{
  sentence *s = sentence_new(mcd_struct, f);
  char buffer[1000];
  word *w = NULL;
  
  while(fgets(buffer, 1000, f)){
    if(feof(f)) break;
    if((buffer[0] == '\n') || (buffer[0] == ' ')) break; /* end of the sentence */
    w = word_parse_buffer(buffer, mcd_struct);
    sentence_add_word(s, w);
  }
  
  if(s->length == 1){
    sentence_free(s);
    return NULL;
  }
  return s;
}
