#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"hash_str.h"
#include"util.h"

hash_str_cell *hash_str_cell_new(char *key, char *val, hash_str_cell *next)
{
  hash_str_cell *c = (hash_str_cell *)memalloc(sizeof(hash_str_cell));
  c->val = val;
  c->key = key;
  c->next = next;
  return c;
}

void hash_str_cell_free(hash_str_cell *c)
{
  if(c == NULL) return;
  hash_str_cell_free(c->next);
  free(c->key);
  free(c->val);
  free(c);
}


hash_str *hash_str_new(int size)
{
  int i;
  hash_str *h = (hash_str *)memalloc(sizeof(hash_str));
  h->size = size;
  h->nbelem = 0;
  h->array = (hash_str_cell **)memalloc(size * sizeof(hash_str_cell *));
  for(i=0; i < size; i++)
    h->array[i] = NULL;
  return h;
}

void hash_str_free(hash_str *h)
{
  int i;
  for(i=0; i < h->size; i++)
    hash_str_cell_free(h->array[i]);
  free(h);
}

int hash_str_func(char *key, int size)
{
  int i;
  int l = strlen(key);
  int val = key[0];
  for(i=1; i < l; i++)
    val = val + i *i * abs(key[i]);
  return val % size;
}

hash_str_cell *hash_str_lookup(hash_str *h, char *key)
{
  int index = hash_str_func(key, h->size);
  hash_str_cell *c;
  /* printf("index = %d\n", index); */

  for(c=h->array[index]; c; c = c->next){
    /* printf("dans la boucle index = %d c = %d\n", index, h->array[index]); */
    if(!strcmp(key, c->key))
      return c;
  }
  return NULL;
}

char *hash_str_get_val(hash_str *h, char *key)
{
  int index = hash_str_func(key, h->size);
  hash_str_cell *c;
  for(c=h->array[index]; c; c = c->next)
    if(!strcmp(key, c->key))
      return c->val;
  return HASH_STR_INVALID_VAL;
}

void hash_str_add(hash_str *h, char *key, char *val)
{
  int index;
  /* printf("add couple (%s %s)\n", key, val); */

  if(hash_str_lookup(h, key)) return;
  index = hash_str_func(key, h->size);
  h->array[index] = hash_str_cell_new(key, val, h->array[index]);
  h->nbelem++;
}

int hash_str_cell_nb(hash_str_cell *c)
{
  if(c == NULL) return 0;
  return 1 + hash_str_cell_nb(c->next);
}

void hash_str_stats(hash_str *h)
{
  int max = 0;
  int i,l;
  int *table;
  int nb;

  for(i=0; i < h->size; i++)
    if((l = hash_str_cell_nb(h->array[i])) > max)
    max = l;
  nb = max + 1;
  table = (int *)memalloc(nb * sizeof(int));
  for(i=0; i < nb; i++)
    table[i] = 0;
  for(i=0; i < h->size; i++)
    table[hash_str_cell_nb(h->array[i])]++;
  
  for(i=0; i < nb; i++)
    printf("%d %d\n", i, table[i]);

  
}
