#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include"char16.h"
//typedef unsigned short char16;
#define char_bit1(c) ((c) & 1)
#define char_bit2(c) (((c) & 2) >> 1)
#define char_bit3(c) (((c) & 4) >> 2)
#define char_bit4(c) (((c) & 8) >> 3)
#define char_bit5(c) (((c) & 16) >> 4)
#define char_bit6(c) (((c) & 32) >> 5)
#define char_bit7(c) (((c) & 64) >> 6)
#define char_bit8(c) (((c) & 128) >> 7)

/* return 1 if it is a character that is stored on a single byte, return 2 otherwise */ 
#define utf8_span(c) ((!char_bit8((c)) || (char_bit8(c) && !char_bit7(c)))? 1 : 2)
/*
int utf8_span(char c)
{
  if(!char_bit8(c)) return 1;
  if(char_bit8(c) && !char_bit7(c)) return 1;
  if(char_bit7(c)) return 2;
  if(char_bit6(c)) return 3;
  if(char_bit5(4)) return 4;
  
}
*/
int utf8_strlen(char *utf8_string)
{
  int l = 0;
  while(*utf8_string){
    l += (utf8_span(*utf8_string) == 1) ? 1 : 0;
    utf8_string++;
  }
  return l;
}

int char16_strlen(char16 *string)
{
  int i=0;
  while(string[i]) i++;
  return i;
}

char *char16toutf8(char16 *char16_string)
{
  char16 c;
  int i, j;
  int length_char16 = char16_strlen(char16_string);
  int length_utf8 = 0;
  int hi,lo;
  char *utf8_string;
  
  for(i=0; i < length_char16; i++){
    c = char16_string[i];
    hi = c >> 8;
    if(hi != 0)
      length_utf8 += 2;
    else
      length_utf8 += 1;
  }

  utf8_string = (char *) malloc(length_utf8 * sizeof(char));
  j = 0;
  for(i=0; i < length_char16; i++){
    c = char16_string[i];
    lo = c & 255;
    hi = c >> 8;
    /* printf("c = %d hi = %d lo = %d (%c)\n", c, hi, lo, (char) lo); */
    if(hi != 0)
      utf8_string[j++] = (char)hi + 1; /* something wrong, we should not have to add one */
    utf8_string[j++] = (char)lo;
  }
  utf8_string[j] = 0;
  return utf8_string;
}


char16 *utf8tochar16(char *utf8_string)
{
  int i,j;
  int utf8_length = strlen(utf8_string);
  int char16_length = utf8_strlen(utf8_string);
  char16 *char16_string = (char16*) malloc((char16_length + 1) * sizeof(char16));
  
  for(i=0, j=0; i < utf8_length; i++, j++){
    if(utf8_span(utf8_string[i]) == 1)
      char16_string[j] = (char16)utf8_string[i];
    if(utf8_span(utf8_string[i]) == 2){
      char16_string[j] = utf8_string[i];
      char16_string[j] = char16_string[j] << 8;
      char16_string[j] += utf8_string[++i];
    }
  }
  char16_string[j] = 0;
  return char16_string;
}

/*int main(void)
{
  int i;
  char string[200];
  char *utf8_string;
  char16 *char16_string;
  strcpy(string, "élèmentaire");

  printf("string = %s\n", string);
  printf("length = %d\n", (int)strlen(string));
  printf("utf8 length = %d\n", (int)utf8_strlen(string));
  for(i=0; i < strlen(string); i++){
    printf("%d\t%c\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\tl=%d\n", i, string[i], (int)string[i], char_bit1(string[i]), char_bit2(string[i]), char_bit3(string[i]), char_bit4(string[i]), char_bit5(string[i]), char_bit6(string[i]), char_bit7(string[i]), char_bit8(string[i]), utf8_span(string[i])); 
  }


  char16_string = utf8tochar16(string);
  printf("char16_strlen = %d\n", char16_strlen(char16_string));

  utf8_string = char16toutf8(char16_string);
  printf("string after conversion = %s\n", utf8_string);
  for(i=0; i < strlen(utf8_string); i++){
    printf("%d\t%c\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\tl=%d\n", i, utf8_string[i], (int)utf8_string[i], char_bit1(utf8_string[i]), char_bit2(utf8_string[i]), char_bit3(utf8_string[i]), char_bit4(utf8_string[i]), char_bit5(utf8_string[i]), char_bit6(utf8_string[i]), char_bit7(utf8_string[i]), char_bit8(utf8_string[i]), utf8_span(utf8_string[i])); 
  }

  
}
*/
