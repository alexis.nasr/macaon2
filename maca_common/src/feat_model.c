#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include "feat_model.h"
#include "feat_lib.h"
#include "feat_desc.h"
/* #include "feat_types.h" */
/* #include "config2feat_vec.h" */

/* returns type of the nth feature in fm */
/* if it is a complex feature, or if n is too large, returns -1 */
int feat_model_get_type_feat_n(feat_model *fm, int n)
{
  feat_desc *fd;
  simple_feat_desc *sfd;

  if(n >= fm->nbelem) return -1;
  fd = fm->array[n];
  if(fd->nbelem > 1) return -1;
  sfd = fd->array[0];
  return sfd->type;
}

/* very basic version */
void feat_model_print(FILE *f, feat_model *fm)
{
  int i,j;
  feat_desc *fd;
  simple_feat_desc *sfd;
  if(fm == NULL) return;
  for(i=0; i < fm->nbelem; i++){
    fd = fm->array[i];
    for(j=0; j < fd->nbelem; j++){
      sfd = fd->array[j];
      fprintf(f, "%s ", sfd->name);
    }
    fprintf(f, "\n");
  }
}

void feat_model_free(feat_model *fm)
{
  int i;
  for(i=0; i < fm->nbelem; i++)
    feat_desc_free(fm->array[i]);
  free(fm->array);
  free(fm->name);
  feat_lib_free(fm->fl); 
  free(fm);
}

feat_model *feat_model_read(char *filename, feat_lib *fl, int verbose)
{
  FILE *f = myfopen(filename, "r");
  feat_model *fm = feat_model_new(filename);
  int feature_number = 0;
  char buffer[1000]; /* ugly */
  char *feat_name;
  //feat_lib *fl
  /*fm->fl = feat_lib_build();*/ // must be preserved in feature_model to delete correctly features at the end
  simple_feat_desc *sfd;
  feat_desc *fd;


    fm->fl = fl;

  while(fgets(buffer, 1000, f)){
    if(feof(f)) break;
    if((buffer[0] == '\n') || (buffer[0] == '#')) continue;
    if(verbose) fprintf(stderr, "%d", feature_number + 1);
    fd = feat_desc_new();
    feat_name = strtok(buffer, " \n");
    do{
      if(verbose) fprintf(stderr, "\t%s", feat_name);
      sfd = feat_lib_get_simple_feat_desc(fm->fl, feat_name);
      if(sfd)
	feat_desc_add(fd, sfd);
    }while((feat_name = strtok(NULL, " \n")));
    if(verbose) fprintf(stderr, "\n");
    feat_model_add(fm, fd);
    feature_number++;
  }
  fclose(f);
  return fm;
}

void feat_model_compute_ranges(feat_model *fm, mcd *m, int mvt_nb){}
#if(0)
void feat_model_compute_ranges(feat_model *fm, mcd *m, int mvt_nb)
{
  int i;
  feat_desc *fd;
  simple_feat_desc *sfd;
  int total = 0;
  int column;
  int mcd_representation;

  for(i = 0; i < fm->nbelem; i++){
    fd = fm->array[i];
    if(fd->nbelem > 1){
      /* fprintf(stderr, "cannot compute range of complex features\n"); */
    } 
    else{
      sfd = fd->array[0];
      if((sfd->type <= FEAT_TYPE_INT_16) && (sfd->type >= FEAT_TYPE_INT_1)){
	sfd->range = sfd->type - FEAT_TYPE_INT_0;
      }
      else if(sfd->type == FEAT_TYPE_TRANS){
	sfd->range = mvt_nb;
      }
      else{
	column = m->wf2col[sfd->type];
	mcd_representation = m->representation[column];
	if(mcd_representation == MCD_REPRESENTATION_VOCAB)
	  sfd->range = m->dico_array[column]->nbelem;
	else if(mcd_representation == MCD_REPRESENTATION_EMB)
	  sfd->range = m->word_emb_array[column]->dim;
      }
      total += sfd->range;
    }
  }
  fm->dim = total;
}
#endif
void catenate_int(char *string, int val)
{
  char s[10];
  sprintf(s, "%d", val);
  strcat(string, s);
}


feat_model *feat_model_new(char *name)
{
  feat_model *fm = (feat_model *)memalloc(sizeof(feat_model));
  fm->name = strdup(name);
  fm->nbelem = 0;
  fm->array = NULL;
  fm->dim = 0;
  return fm;
}

feat_desc *feat_model_add(feat_model *fm, feat_desc *fd)
{
  fm->nbelem++;
  fm->array = (feat_desc **)realloc(fm->array, fm->nbelem * sizeof(feat_desc *));
  fm->array[fm->nbelem - 1] = fd;
  return fd;
}
