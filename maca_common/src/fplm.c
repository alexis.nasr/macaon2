#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<getopt.h>
#include<ctype.h>

#include"fplm.h"
#include"dico.h"
#include"util.h"


fplm_struct *fplm_new(void){
  fplm_struct *fplm = (fplm_struct *)memalloc(sizeof(fplm_struct));
  fplm->form_pos_ht = hash_new(1000000);
  fplm->lemma_array = NULL;
  fplm->lemma_array_size = 0;
  fplm->nbelem = 0;
  return fplm;
}

void fplm_free(fplm_struct *fplm){
  hash_free(fplm->form_pos_ht);

  for(int i=0; i< fplm->lemma_array_size; ++i) 
    if(fplm->lemma_array[i])
      free(fplm->lemma_array[i]);
  
  free(fplm->lemma_array);
  free(fplm);
}

void fplm_add(fplm_struct *fplm, char *form, char *pos, char *lemma)
{
  char form_pos[1000];
  
  strcpy(form_pos, form);
  strcat(form_pos, "/");
  strcat(form_pos, pos);

  hash_add(fplm->form_pos_ht, form_pos, fplm->nbelem);

  if(fplm->nbelem >= fplm->lemma_array_size){
    fplm->lemma_array_size = 2 * fplm->lemma_array_size + 1;
    fplm->lemma_array = (char **)realloc(fplm->lemma_array, fplm->lemma_array_size * sizeof(char *));
    // initialize in order to be able to free correctly and the end
    for(int i = fplm->nbelem; i < fplm->lemma_array_size; ++i)
      fplm->lemma_array[i] = NULL;
  }

  /* if(fplm->lemma_array[fplm->nbelem] == NULL) */
  fplm->lemma_array[fplm->nbelem] = strdup(lemma);
  fplm->nbelem++;
}

fplm_struct *fplm_load_file(char *fplm_filename, int debug_mode)
{
  char form[1000];
  char pos[1000];

  char lemma[1000];  
  char morpho[1000];
  char buffer[10000];
  int fields_nb;
  FILE *f= myfopen(fplm_filename, "r");
  fplm_struct *fplm = fplm_new();

  while(fgets(buffer, 10000, f)){
    fields_nb = sscanf(buffer, "%[^\t]\t%s\t%[^\t]\t%s\n", form, pos, lemma, morpho);
    /* if(!strcmp(form, "d")) */
    //     fprintf(stderr, "form = %s pos = %s lemma = %s\n", form, pos, lemma);   
    if(fields_nb != 4){
      if(debug_mode){
	fprintf(stderr, "form = %s pos = %s lemma = %s\n", form, pos, lemma); 
	fprintf(stderr, "incorrect fplm entry, skipping it\n");
      }
      continue;
    }
    fplm_add(fplm, form, pos, lemma);
  }
  /* fprintf(stderr, "%d entries loaded\n", num); */
  fclose(f);
  return fplm;
}

char *fplm_lookup_lemma(fplm_struct *fplm, char *form, char *pos, int verbose)
{
  char form_pos[1000];
  int index_form_pos;

  strcpy(form_pos, form);
  strcat(form_pos, "/");
  strcat(form_pos, pos);
  index_form_pos = hash_get_val(fplm->form_pos_ht, form_pos);

  if(index_form_pos != HASH_INVALID_VAL) /* couple form/pos found in the hash table */
    return fplm->lemma_array[index_form_pos];
  
  strcpy(form_pos, form);
  to_lower_string(form_pos); /* change form to lower case and look it up again */
  strcat(form_pos, "/");
  strcat(form_pos, pos);
  index_form_pos = hash_get_val(fplm->form_pos_ht, form_pos);
  if(index_form_pos != HASH_INVALID_VAL)
    return fplm->lemma_array[index_form_pos];

  /* even in lower case couple form/pos is not found, return the form as lemma */
  if(verbose)
    fprintf(stderr, "cannot find an entry for %s %s\n", form, pos);
  
  return NULL;
  //  return form;
}
