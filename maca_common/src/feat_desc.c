#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include "feat_desc.h"

void simple_feat_desc_free(simple_feat_desc *sfd)
{
    if(sfd->name)
	free(sfd->name);
    free(sfd);
}

void feat_desc_free(feat_desc *fd)
{
  free(fd->array);
  free(fd);
}

simple_feat_desc *simple_feat_desc_new(char *name, int type, feat_fct fct)
{
  simple_feat_desc * sfd = (simple_feat_desc *)memalloc(sizeof(simple_feat_desc));
  sfd->name = strdup(name);
  sfd->type = type;
  sfd->fct = fct;
  return sfd;
}

feat_desc *feat_desc_new(void)
{
  feat_desc * fd = (feat_desc *)memalloc(sizeof(feat_desc));
  fd->nbelem = 0;
  fd->array = NULL;
  return fd;
}

feat_desc *feat_desc_add(feat_desc *fd, simple_feat_desc *sfd)
{
  if(fd == NULL)
    fd = feat_desc_new();
  fd->nbelem++;
  fd->array = (simple_feat_desc **)realloc(fd->array, fd->nbelem * sizeof(simple_feat_desc*));
  fd->array[fd->nbelem - 1] = sfd;
  return fd;
}




