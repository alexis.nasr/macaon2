#include "feat_lib.h"
/* #include "feat_types.h" */
/* #include "feat_fct.h" */

void feat_lib_add(feat_lib *fl, int feature_type, char *feature_name, feat_fct feature_fct)
{
  int feature_nb = dico_add(fl->d_features, feature_name); 
  fl->array = (simple_feat_desc **)realloc(fl->array, (feature_nb + 1) * sizeof(simple_feat_desc *));
  fl->array[feature_nb] = simple_feat_desc_new(feature_name, feature_type, feature_fct);
  fl->nbelem = feature_nb+1;
}

simple_feat_desc *feat_lib_get_simple_feat_desc(feat_lib *fl, char *feat_name)
{
  int index = dico_string2int(fl->d_features, feat_name);
  if(index == -1){
    fprintf(stderr, "features function %s unknown\n", feat_name);
    return NULL;
  }
  return fl->array[index];
}

feat_lib *feat_lib_new(void)
{
  feat_lib *fl = (feat_lib *)memalloc(sizeof(feat_lib));
  fl->nbelem = 0;
  fl->d_features = dico_new(NULL, 100);
  fl->array = NULL;
  return fl;
}

void feat_lib_free(feat_lib *fl) {

    for(int i=0; i < fl->nbelem; ++i) {
	simple_feat_desc_free(fl->array[i]);
    }
    dico_free(fl->d_features);
    free(fl->array);
    free(fl); // ne devrait pas etre en commentaire

}

