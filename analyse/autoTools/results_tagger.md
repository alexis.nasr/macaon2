### Results : 

# 2 classes :
P0=97.4	P1=58.8	
R0=99.3	R1=27.2	
Réduction du taux d'erreur : 2.51%

# 3 classes :
P0=95.1	P1=57.6	P2=64.3	
R0=99.0	R1=24.9	R2=25.1	
Réduction du taux d'erreur : 0.58%

# 4 classes :
P0=92.6	P1=55.5	P2=61.0	P3=63.6	
R0=98.8	R1=23.6	R2=25.2	R3=17.4	
Réduction du taux d'erreur : -6.02%

# 5 classes :
P0=89.8	P1=51.4	P2=55.7	P3=58.1	P4=9.5	
R0=98.7	R1=22.4	R2=23.8	R3=17.5	R4=0.5	
Réduction du taux d'erreur : -9.94%

# 6 classes :
P0=87.0	P1=49.0	P2=51.0	P3=54.1	P4=11.3	P5=9.1	
R0=98.6	R1=21.9	R2=23.5	R3=16.9	R4=0.7	R5=0.3	
Réduction du taux d'erreur : -11.19%
