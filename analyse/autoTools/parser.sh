#!/bin/zsh

cd ~/macaon2/analyse/autoTools/
echo "### Results : (little corpus)" > results_parser.md
for i in {5..7}
do
    echo -e "\n# $i classes :" >> results_parser.md
    cd ~/maca_data2/fr/maca_error_predictor_parser
    make clean ; make NB_CLASSES="$i" ; make install ; ooo
    cd ~/macaon2/analyse/Parser
    ./script.sh "$i" > /dev/null 2> /dev/null
    cat ./data/new_test.ok | head -n 1380 > ./data/test"$i".ok
    cat ./data/test"$i".ok | ../take_parser | ../ref "$i" | ../calcul "$i" >> ~/macaon2/analyse/autoTools/results_parser.md
    cd ~/macaon2/analyse/autoTools/
done
