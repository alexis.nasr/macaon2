#!/bin/zsh

cd ~/macaon2/analyse/autoTools/
echo "### Results : " > results_tagger.md
for i in {2..6}
do
    printf "\rNB Classes : $i  "
    echo -e "\n# $i classes :" >> results_tagger.md 
    cd ~/maca_data2/fr/maca_error_predictor_tagger
    make clean 2> /dev/null > /dev/null; make NB_CLASSES="$i" 2> /dev/null > /dev/null; make install 2> /dev/null > /dev/null
    cd ~/macaon2/analyse/Tagger
    ./script.sh "$i" | tail -n 2 >> ~/macaon2/analyse/autoTools/results_tagger.md 2> /dev/null > /dev/null
    ./taux.sh | tail -n 1 >> ~/macaon2/analyse/autoTools/results_tagger.md 2> /dev/null > /dev/null
    cd ~/macaon2/analyse/autoTools/
done
printf "\n"
