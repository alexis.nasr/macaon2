exception Finish

let n = ref 7
                                 
let recupCourant tab =
  for i = !n-2 downto 0 do
    tab.(i+1) <- tab.(i)
  done;
  tab.(0) <- Scanf.scanf "%b %d\n" (fun re cla -> (re,cla))

exception Find of int
  
let clatru tab =
  try
    for i = !n-1 downto 0 do
      if not (fst tab.(i)) then
        raise (Find i)
    done;
    0
  with
  | Find l -> l
               
let readline tab =
  recupCourant tab;
  Printf.printf "%d %d\n" (clatru tab) (snd tab.(0))
                
let _ =
  (try
     n := int_of_string Sys.argv.(1)
  with
  | _ -> ());
  let tab = Array.make !n (true,0) in
  try
    while true do
      readline tab
    done
  with
  | Finish | End_of_file  -> ()
