#!/bin/zsh

if [ -z $2 ]
then
    name=new_test
else
    name=$2
fi

if [ -z $1 ]
then
    nclass=5
else
    nclass=$1
fi

echo "=> Input : $name.mcf"
cut -f1 ~/maca_data2/fr/data/treebank/"$name".mcf > ./data/"$name".input1
cut -f2 ~/maca_data2/fr/data/treebank/"$name".mcf > ./data/"$name".input2
cut -f3 ~/maca_data2/fr/data/treebank/"$name".mcf > ./data/"$name".input3
cut -f4 ~/maca_data2/fr/data/treebank/"$name".mcf > ./data/"$name".input4
paste ./data/"$name".input1 ./data/"$name".input2 ./data/"$name".input3 ./data/"$name".input4 > ./data/"$name".input

echo "=> Oracle"
cd ~/maca_data2/fr/maca_error_predictor_parser
maca_error_predictor_parser_arc_eager_mcf2cff -m ../bin/maca_trans_parser.model -C maca_error_predictor_parser.mcd  --input ../data/treebank/"$name".mcf --fann maca_error_predictor_parser.fm --feat_model ../bin/maca_trans_parser.fm --vocabs ../bin/maca_trans_parser.vocab -N maca_error_predictor_parser.vocab --cff dev.cff -s 10000000 -S -P ../data/morpho-lexicon/fP -K 2> /dev/null > temp
cd - > /dev/null
mv ~/maca_data2/fr/maca_error_predictor_parser/temp ./data/"$name"_ref.mvt

echo "=> Error predictor"
cat ./data/"$name".input | maca_error_predictor_parser -TK > ./data/"$name"_err.mvt

echo "=> Results : $nclass classes"
#paste ./data/"$name"_ref.mvt ./data/"$name"_err.mvt > ./data/"$name".ok
../decal ./data/"$name"_ref.mvt ./data/"$name"_err.mvt > ./data/"$name".ok
cat ./data/"$name".ok | ../take_parser | ../ref "$nclass" | ../calcul "$nclass"
ooo
