#!/bin/zsh

if [ -z $1 ]
then
    name=new_test
else
    name=$1
fi

echo "=> Input : $name.mcf"
cut -f1 ~/maca_data2/fr/data/treebank/"$name".mcf > ./data/"$name".input1
cut -f2 ~/maca_data2/fr/data/treebank/"$name".mcf > ./data/"$name".input2
cut -f3 ~/maca_data2/fr/data/treebank/"$name".mcf > ./data/"$name".input3
cut -f4 ~/maca_data2/fr/data/treebank/"$name".mcf > ./data/"$name".input4
cat ~/maca_data2/fr/data/treebank/"$name".mcf > ./data/"$name"_ref.output
paste ./data/"$name".input1 ./data/"$name".input2 ./data/"$name".input3 ./data/"$name".input4 > ./data/"$name".input

echo "=> Parser"
cat ./data/"$name".input | maca_trans_parser > ./data/"$name"_tag.output

echo "=> Error predictor"
cat ./data/"$name".input | maca_error_predictor_parser -K > ./data/"$name"_error.output

echo "=> Results : (little corpus)"

cat ./data/"$name"_ref.output | head -n 1380 > ./data/"$name"_refs.output
cat ./data/"$name"_tag.output | head -n 1380 > ./data/"$name"_tags.output
cat ./data/"$name"_error.output | head -n 1380 > ./data/"$name"_errors.output

../analyse_parser ./data/"$name"_refs.output ./data/"$name"_tags.output ./data/"$name"_errors.output $2
ooo
