let rec readline fref ferr =
  let a1,b1 = Scanf.fscanf fref "%s %s\n" (fun a b -> (a,b)) in
  let a2,b2,e = Scanf.fscanf ferr "%s %s %d\n" (fun a b c -> (a,b,c)) in

  if not (a1 = "EOS") || a1=a2 then
    if not (a2 = "EOS") || a1=a2 then
      begin
        Printf.printf "%s %s %s %s %d\n" a1 b1 a2 b2 e;
        readline fref ferr
      end
    else
      begin
      Printf.printf "%s %s %s %s %d\n" a1 b1 a2 b2 e;
      let rec fini () =
        let a,b = Scanf.fscanf fref "%s %s\n" (fun a b -> (a,b)) in
        (match a with
        | "EOS" -> Printf.printf "%s %s _ _ 0\n" a b
        | _ -> Printf.printf "%s %s _ _ 0\n" a b; fini ())
      in fini ()

      end

  else 
    begin
      Printf.printf "%s %s %s %s %d\n" a1 b1 a2 b2 e;
      let rec fini () =
        let a,b,c = Scanf.fscanf ferr "%s %s %d\n" (fun a b c -> (a,b,c)) in
        (match a with
        | "EOS" -> Printf.printf "_ _ %s %s %d\n" a b c
        | _ -> Printf.printf "_ _ %s %s %d\n" a b c; fini ())
      in fini ()
    end
                
let _ =
  let fref = open_in (Sys.argv.(1)) and ferr = open_in (Sys.argv.(2)) in
  try
    while true do
      readline fref ferr
    done
  with
  | End_of_file  -> ()
