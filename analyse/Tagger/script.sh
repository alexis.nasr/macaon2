#!/bin/zsh

if [ -z $2 ]
then
    name=new_test
else
    name=$2
fi

if [ -z $1 ]
then
    nclass=2
else
    nclass=$1
fi


echo "=> Input : $name.mcf"
cut -f1 ~/maca_data2/fr/data/treebank/"$name".mcf > ./data/"$name".input

echo "=> Oracle"
cut -f2 ~/maca_data2/fr/data/treebank/"$name".mcf > ./data/"$name"_ref.output

echo "=> Error predictor"
cat ./data/"$name".input | maca_error_predictor_tagger -T > ./data/pos_"$name".pred

echo "=> Results : $smin/$smax, classes : $nclass"
cat ./data/"$name"_ref.output > ./data/pos_"$name".correct
paste ./data/pos_"$name".pred ./data/pos_"$name".correct > ./data/pos_"$name".ok
cat ./data/pos_"$name".ok | ../take_tagger | ../ref "$nclass" | ../calcul "$nclass"
ooo
