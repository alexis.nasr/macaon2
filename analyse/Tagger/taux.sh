if [ -z $1 ]
then
    name=new_test
else
    name=$1
fi
echo "=> Input : $name.mcf"
cut -f1 ~/maca_data2/fr/data/treebank/"$name".mcf > ./data/"$name".input

echo  "=> Ref"
cut -f2 ~/maca_data2/fr/data/treebank/"$name".mcf > ./data/"$name"_ref.output

echo "=> Tagger"
cat ./data/"$name".input | maca_trans_tagger | cut -f2 > ./data/"$name"_tag.output

echo "=> Error predictor"
cat ./data/"$name".input | maca_error_predictor_tagger | cut -f2 > ./data/"$name"_error.output
echo "=> Results"
paste ./data/"$name"_ref.output ./data/"$name"_tag.output ./data/"$name"_error.output > ./data/"$name".data
cat ./data/"$name".data | ../analyse_tagger
paste data/"$name".input data/"$name"_ref.output ./data/"$name"_tag.output ./data/"$name"_error.output > ./data/"$name".anfm
ooo
