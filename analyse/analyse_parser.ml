open Printf
       
let okTag = ref 0
let okError = ref 0
let pokTag = ref 0
let pokError = ref 0
let diffEj = ref 0
let diffTj = ref 0
let lineDiffTj = ref []
let line = ref 0
                     
let readline firef fitag fierr =
  incr line;
  let nref,fref,pref = Scanf.fscanf firef "%s %s %s %s %d %s %d\n" (fun a b c d e f g -> (e,f,g)) in
  let ntag,ftag,ptag = Scanf.fscanf fitag "%s %s %s %s %d %s %d\n" (fun a b c d e f g -> (e,f,g)) in
  let nerr,ferr,perr = Scanf.fscanf fierr "%s %s %s %s %d %s %d\n" (fun a b c d e f g -> (e,f,g)) in
  
  if fref=ftag then 
    incr okTag
  else
    incr pokTag;
  
  if fref=ferr then
    incr okError
  else
    incr pokError;

  if fref=ferr && not (ftag=fref) then
    incr diffEj
  else if fref=ftag && not (ferr=fref) then
    begin
      incr diffTj;
      lineDiffTj := (!line)::(!lineDiffTj)
    end
         
let tot a b =
  let af = float_of_int !a and bf = float_of_int !b in
  100. *. af /. (af +. bf)
                       
let _ =
  let diff = try (Sys.argv.(4)="diff") with | _ -> false in
  try
    let fref = open_in (Sys.argv.(1)) and ftag = open_in (Sys.argv.(2)) and ferr = open_in (Sys.argv.(3)) in
    while true do
      readline fref ftag ferr
    done
  with
  | End_of_file  ->
     if diff then
       begin
         List.iter (printf "%d " ) (List.rev (!lineDiffTj));
         printf "\n";
       end
     else
       begin
         printf "*******Score*******\n";
         printf "Parser\tvrai = %d\tfaux = %d\tte = %.2f%%\ttr = %.2f%%\n" !okTag !pokTag (tot pokTag okTag) (100. -. tot pokTag okTag);
         printf "Error\tvrai = %d\tfaux = %d\tte = %.2f%%\ttr = %.2f%%\n" !okError !pokError (tot pokError okError) (100. -. tot pokError okError);
         printf "Error fait %d de différent et mieux que Parser\n" !diffEj;
         printf "Parser fait %d de différent et mieux que Error\n" !diffTj;
         printf "Réduction du taux d'erreur : %.2f%%\n" (100.*.((tot pokTag okTag)-.(tot pokError okError))/.(tot pokTag okTag))
       end
         
