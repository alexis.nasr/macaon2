open Printf
       
let okTag = ref 0
let okError = ref 0
let pokTag = ref 0
let pokError = ref 0
let diffEj = ref 0
let diffTj = ref 0
let lineDiffTj = ref []
let line = ref 0
                     
let readline () =
  incr line;
  let reff,tagg,error = Scanf.scanf "%s %s %s\n" (fun a b c -> (a,b,c)) in
  
  if reff=tagg then 
    incr okTag
  else
    incr pokTag;
  
  if reff=error then
    incr okError
  else
    incr pokError;

  if reff=error && not (tagg=reff) then
    incr diffEj
  else if reff=tagg && not (error=reff) then
    begin
      incr diffTj;
      lineDiffTj := (!line)::(!lineDiffTj)
    end
         
let tot a b =
  let af = float_of_int !a and bf = float_of_int !b in
  100. *. af /. (af +. bf)
                       
let _ =
  let diff = try (Sys.argv.(1)="diff") with | _ -> false in
  try    
    while true do
      readline ()
    done
  with
  | End_of_file  ->
     if diff then
       begin
         List.iter (printf "%d " ) (List.rev (!lineDiffTj));
         printf "\n";
       end
     else
       begin
         printf "*******Score*******\n";
         printf "Tagger\tvrai = %d\tfaux = %d\tte = %.2f%%\ttr = %.2f%%\n" !okTag !pokTag (tot pokTag okTag) (100. -. tot pokTag okTag);
         printf "Error\tvrai = %d\tfaux = %d\tte = %.2f%%\ttr = %.2f%%\n" !okError !pokError (tot pokError okError) (100. -. tot pokError okError);
         printf "Error fait %d de différent et mieux que Tag\n" !diffEj;
         printf "Tag fait %d de différent et mieux que Error\n" !diffTj;
         printf "Réduction du taux d'erreur : %.2f%%\n" (100.*.((tot pokTag okTag)-.(tot pokError okError))/.(tot pokTag okTag))
       end
         
