exception Finish
  
let readline () =
  let a,re,cla = Scanf.scanf "%s %s %s %s %d\n" (fun a1 b1 a2 b2 d -> (a1,a1=a2 && b1=b2,d)) in
  if a="" then raise Finish
  else 
    Printf.printf "%b %d\n" re cla

let _ =
  try
    while true do
      readline ()
    done
  with
  | _ -> ()
