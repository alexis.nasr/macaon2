exception Finish
exception Normal

let n = ref 7
        
let readline tab =
  let a,b = Scanf.scanf "%d %d\n" (fun a b -> (a,b)) in
  tab.(a).(0) <- tab.(a).(0) + 1;
  tab.(b).(1) <- tab.(b).(1) + 1;
  if a=b then tab.(a).(2) <- tab.(a).(2) + 1
                    
let rap a b =
  let af = float_of_int a and bf = float_of_int b in
  100. *. (af /. bf)

let pi i tab =
  rap (tab.(i).(2)) (tab.(i).(1))

let ri i tab =
  rap (tab.(i).(2)) (tab.(i).(0))

let printPiRis tab ch =
  let l,f = if ch then "P",pi else "R",ri in
  for i=0 to !n-1 do
    Printf.printf "%s%d=%.1f\t" l i (f i tab)
  done;
  Printf.printf "\n"
           
let _ =
  (try
     n := int_of_string Sys.argv.(1)
  with
  | _ -> ());
  let tab = Array.make_matrix !n 3 0 in 
  try                       
    while true do
      readline tab
    done
  with
  | Finish | End_of_file  -> List.iter (printPiRis tab) [true;false]
                   
