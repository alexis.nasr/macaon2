exception Finish
  
let readline () =
  let a,re,cla = Scanf.scanf "%s %s %d %s\n" (fun a b c d -> (a,b=d,c)) in
  if a="" then raise Finish
  else 
    Printf.printf "%b %d\n" re cla 

let _ =
  try
    while true do
      readline ()
    done
  with
  | _ -> ()
