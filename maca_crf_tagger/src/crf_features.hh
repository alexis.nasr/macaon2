#pragma once
#include <vector>
#include <string>

namespace macaon {
    class FeatureGenerator {
        static void prefixesUtf8(const std::string &word, int n, std::vector<std::string> &output) {
            size_t offset = 0;
            while(offset < word.length() && n > 0) {
                if((unsigned char)word[offset] >> 7 == 1) { // 1xxxxxxx (length of utf8 character)
                    offset++;
                    while(offset < word.length() && (unsigned char)word[offset] >> 6 == 2) { // 10xxxxxx (continuation of character)
                        offset++;
                    }
                } else {
                    offset++;
                }

                output.push_back(word.substr(0, offset));
                n--;
            }
            while(n > 0) {
                output.push_back("__nil__");
                n--;
            }
        }

        static void suffixesUtf8(const std::string &word, int n, std::vector<std::string> &output) {
            std::vector<int> char_starts;
            size_t offset = 0;
            while(offset < word.length()) {
                char_starts.push_back(offset);
                if((unsigned char)word[offset] >> 7 == 1) { // 1xxxxxxx (length of utf8 character)
                    offset++;
                    while(offset < word.length() && (unsigned char)word[offset] >> 6 == 2) { // 10xxxxxx (continuation of character)
                        offset++;
                    }
                } else {
                    offset++;
                }
            }
            for(int i = char_starts.size() - 1; i > 0 && n > 0; i--) {
                //std::cerr << "s=[" << word.substr(offsets[i]) << "]\n";
                output.push_back(word.substr(char_starts[i]));
                n--;
            }
            while(n > 0) {
                output.push_back("__nil__");
                n--;
            }
        }


        static void prefixes(const std::string &word, int n, std::vector<std::string> &output) {
            int length = word.length();
            for(int i = 1; i <= n; i++) {
                if(length >= i) output.push_back(word.substr(0, i));
                else output.push_back("__nil__");
            }
        }
        static void suffixes(const std::string &word, int n, std::vector<std::string> &output) {
            int length = word.length();
            for(int i = 1; i <= n; i++) {
                if(length >= i) output.push_back(word.substr(length - i, i));
                else output.push_back("__nil__");
            }
        }
        static void wordClasses(const std::string &word, std::vector<std::string> &output) {
            bool containsNumber = false;
            bool containsSymbol = false;
            for(int i = 0; i < (int) word.length(); i++) {
                if(!containsNumber && word.at(i) >= '0' && word.at(i) <= '9') containsNumber = true;
                if(!containsSymbol && !((word.at(i) >= '0' && word.at(i) <= '9') || (word.at(i) >= 'a' && word.at(i) <= 'z') || (word.at(i) >= 'A' && word.at(i) <= 'Z'))) containsSymbol = true;
            }
            if(containsNumber) output.push_back("Y");
            else output.push_back("N");
            if(word.length() >= 2 && word.at(0) >= 'A' && word.at(0) <= 'Z' && word.at(1) >= 'a' && word.at(1) <= 'z') output.push_back("Y");
            else output.push_back("N");
            if(containsSymbol) output.push_back("Y");
            else output.push_back("N");
        }
    public:
        static void get_pos_features(const std::string &word, std::vector<std::string> &output, bool utf8=true) {
            output.push_back(word);
            wordClasses(word, output);
            if(utf8) {
                prefixesUtf8(word, 4, output);
                suffixesUtf8(word, 4, output);
            } else {
                prefixes(word, 4, output);
                suffixes(word, 4, output);
            }
        }
        static std::vector<std::string> get_pos_features(const std::string &word, bool utf8=true) {
            std::vector<std::string> output;
            get_pos_features(word, output, utf8);
            return output;
        }
    };
}
