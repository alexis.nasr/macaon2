#include <vector>
#include "crf_decoder.hh"
#include "crf_binlexicon.hh"
#include "crf_features.hh"

namespace macaon {
    class Tagger {
        private:
            macaon::Decoder decoder;
            macaon::BinaryLexicon *lexicon;
        public:
            Tagger(const std::string modelName, const std::string lexiconName = "") : decoder(modelName), lexicon(NULL) {
                if(lexiconName != "") lexicon = new macaon::BinaryLexicon(lexiconName, decoder.getTagset());
            }

            ~Tagger() {
                if(lexicon != NULL) delete lexicon;
            }

            bool ProcessSentence(const std::vector<std::string>& words, std::vector<std::string>& tags) {
                std::vector<std::vector<std::string> > features;
                for(size_t i = 0; i < words.size(); i++) {
                    std::vector<std::string> word_features;
                    macaon::FeatureGenerator::get_pos_features(words[i], word_features);
                    features.push_back(word_features);
                }
                tags.clear();
                decoder.decodeString(features, tags, lexicon);
                return true;
            }
    };
}

extern "C" {
    macaon::Tagger* Tagger_new(const char* modelName, const char* lexiconName);

    void Tagger_free(macaon::Tagger* tagger);

    bool Tagger_ProcessSentence(macaon::Tagger* tagger, int num_words, const char** words, const char** tags);
}
