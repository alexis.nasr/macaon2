#include <stdio.h>
#include <stdlib.h>

const char *byte_to_binary(int x)
{
    static char b[9];
    b[0] = '\0';

    int z;
    for (z = 128; z > 0; z >>= 1)
    {
        strcat(b, ((x & z) == z) ? "1" : "0");
    }

    return b;
}

int main() {
    char word[1024];
    fgets(word, 1024, stdin);
    printf("%s\n", word);
    int offset = 0;
    while(word[offset] != 0) {
        printf("%3d %s [%s]\n", offset, byte_to_binary(word[offset]), &word[offset]);
        if((unsigned char)word[offset] >> 7 == 1) {
            offset++;
            while((unsigned char)word[offset] >> 6 == 2) offset++;
        } else {
            offset++;
        }
    }
    return 0;
}
