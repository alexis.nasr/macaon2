#pragma once

#include <string>
#include <unordered_map>
#include <vector>
#include <fstream>
#include <iostream>

#include "crf_utils.hh"

namespace macaon {
    class Lemmatizer {
        std::unordered_map<std::string, std::string> dictionary;
        public:
            Lemmatizer(const std::string& filename) {
                std::ifstream input(filename);
                if(input) {
                    std::string line;
                    int line_num = 1;
                    while(std::getline(input, line)) {
                        std::vector<std::string> tokens;
                        macaon::Tokenize(line, tokens, "\t", true);
                        if(tokens.size() != 4) {
                            std::cerr << "ERROR: unexpected input in " << filename << ", line " << line_num << ": \"" << line << "\"\n";
                            break;
                        }
                        std::string word = tokens[0];
                        std::string tag = tokens[1];
                        std::string lemma = tokens[2];
                        std::string morpho = tokens[3];
                        dictionary[word + "/" + tag] = lemma;
                        line_num ++;
                    }
                } else {
                    std::cerr << "ERROR: loading " << filename << "\n";
                }
            }
            std::string lemmatize(const std::string& word, const std::string& tag) const {
                std::string key = word + "/" + tag;
                return lemmatize(key);
            }
            std::string lemmatize(const std::string& word_tag) const {
                std::unordered_map<std::string, std::string>::const_iterator found = dictionary.find(word_tag);
                if(found != dictionary.end()) {
                    return found->second;
                }
                return word_tag.substr(0, word_tag.rfind('/'));
            }
    };
}

