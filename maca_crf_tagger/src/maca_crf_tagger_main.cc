/***************************************************************************
    Copyright (C) 2011 by xxx <xxx@lif.univ-mrs.fr>
    This file is part of maca_crf_tagger.

    Maca_crf_tagger is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Maca_crf_tagger is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with maca_crf_tagger. If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

#include "maca_crf_tagger.hh"
#include "crf_decoder.hh"
#include "crf_features.hh"
#include "crf_lexicon.hh"
#include "crf_tclexdet.hh"

void crf_tagger(fst::StdVectorFst &input, maca_crf_tagger_ctx *ctx, bool debug=false)
{
    if(debug) input.Write("debug.crf_tagger.input");
    gfsmStateId start;
    maca_ht_structure * ht = ctx->ms->xml_nodes_ht;
    xmlNodePtr seg;
    char *tokens = NULL;
    std::vector<std::vector<std::string> >features;
    std::vector<int>ilabels;
    fst::StdVectorFst output;
    if(ctx->model_filename == NULL) {
        std::cerr << "ERROR: crf_tagger model file not specified, exiting\n";
        exit(1); // ERROR
    }
    if(ctx->lexicon_filename == NULL) {
        std::cerr << "ERROR: crf_tagger lexicon file not specified, exiting\n";
        exit(1); // ERROR
    }
    fst::SymbolTable inputSymbols("words");
    inputSymbols.AddSymbol("<eps>", 0);

    // extract features
    for(start=0; start < input.NumStates(); start++){
        for(fst::MutableArcIterator<fst::StdVectorFst> aiter(&input, start); !aiter.Done(); aiter.Next()) {
            const fst::StdArc &arc = aiter.Value();
            seg = (xmlNodePtr)maca_ht_index2adr(ht, arc.ilabel);
            tokens = maca_sentence_get_segment_tokens_value(ctx->ms, seg);
            ilabels.push_back(arc.ilabel);
            inputSymbols.AddSymbol(tokens, ilabels.size());
            aiter.SetValue(fst::StdArc(ilabels.size(), arc.olabel, arc.weight, arc.nextstate));
            std::vector<std::string>word_features;
            macaon::FeatureGenerator::get_pos_features(tokens, word_features);
            features.push_back(word_features);
            free(tokens);
        }
    }
    if(debug) input.Write("debug.crf_tagger.features");

    int64 isString = input.Properties(fst::kString, true);
    if(isString & fst::kString && ctx->n == 1) {
        if(ctx->verbose_flag > 0) std::cerr << "INFO: using linear tagger\n";
        // faster pipeline for linear automata
        std::vector<std::string> tags;
        ctx->decoder->decodeString(features, tags, ctx->lexicon);
        output.AddState();
        output.SetStart(0);
        for(int64 state = 0; state < input.NumStates() - 1; state++){
            const fst::StdArc &arc = fst::ArcIterator<fst::StdVectorFst>(input, state).Value();
            output.AddState();
            output.AddArc(state, fst::StdArc(ilabels[arc.ilabel-1], ctx->tag_mapping[ctx->decoder->getTagset()->Find(tags[state])], arc.weight, state + 1));
        }
        output.SetFinal(output.NumStates() - 1, 0);
        input = output;

    } else {
        // add possible tag labels
        input.SetInputSymbols(&inputSymbols);
        ctx->lexicon->AddTags(input);
        if(debug) input.Write("debug.crf_tagger.tags");

        // rescore with CRF
        ctx->decoder->decode(features, input, output, true);
        if(debug) output.Write("debug.crf_tagger.decoded");

        // convert to macaon
        fst::RmEpsilon(&output);

        input = output;
        for(start=0; start < input.NumStates(); start++){
            for(fst::MutableArcIterator<fst::StdVectorFst> aiter(&input, start); !aiter.Done(); aiter.Next()) {
                const fst::StdArc &arc = aiter.Value();
                aiter.SetValue(fst::StdArc(ilabels[arc.ilabel-1], ctx->tag_mapping[arc.olabel], arc.weight, arc.nextstate));
            }
        }
    }
    if(debug) input.Write("debug.crf_tagger.output");
}

void traverse_segments(maca_section *section, maca_crf_tagger_ctx *ctx)
{
    xmlNodePtr segs = section->xml_node_segs;	
    xmlNodePtr seg;
    xmlChar *ulex_id;
    maca_ht_structure * ht = ctx->ms->xml_nodes_ht;
    int n;
    int index;
    char *tokens = NULL;


    for(seg=segs->children, n=0; seg ; seg=seg->next, n++)
    {
        ulex_id = xmlGetProp(seg, BAD_CAST "id");
        index = maca_ht_adr2index(ht, seg);
        tokens = maca_sentence_get_segment_tokens_value(ctx->ms, seg);
        fprintf(stderr, "index = %d n = %d id = %s tokens = %s\n", index, n, ulex_id, tokens);
    }
}

void traverse_automaton(gfsmAutomaton *a, maca_crf_tagger_ctx *ctx)
{
    gfsmStateId i;
    gfsmArcIter ai;
    gfsmArc *t;
    xmlNodePtr n;
    maca_ht_xmlnode *ht = ctx->ms->xml_nodes_ht;
    xmlChar *ulex_id;
    xmlChar *ulex_lex_id;

    for(i=0; i<gfsm_automaton_n_states(a); i++){
        for (gfsm_arciter_open_ptr(&ai,a,gfsm_automaton_find_state(a, i)); gfsm_arciter_ok(&ai); gfsm_arciter_next(&ai)){
            t = gfsm_arciter_arc(&ai);
            n = (xmlNodePtr)maca_ht_index2adr(ht, gfsm_arc_lower(t));
            ulex_id = xmlGetProp(n, BAD_CAST "id");
            ulex_lex_id = xmlGetProp(n, BAD_CAST "lex_id");
            fprintf(stderr,"index = %d ulex id = %s lex_id = %s\n",gfsm_arc_lower(t), ulex_id, ulex_lex_id);
        }
    }
    /* creation de segment et d'une section */
}


maca_section *create_morpho_section(gfsmAutomaton *a, maca_crf_tagger_ctx *ctx)
{
    gfsmStateId i;
    gfsmArc *t;
    gfsmArcIter ai;  
    maca_ht_structure * ht = ctx->ms->xml_nodes_ht;
    char id_pos[500];
    xmlNodePtr posNode = NULL;
    xmlNodePtr lexNode = NULL;
    maca_section * section = NULL;
    GHashTable* segments_created = g_hash_table_new_full(g_str_hash, g_str_equal,free, NULL);
    char * prefix_id;
    char * temp;

    //section = maca_section_create_section(MACA_POSS_SECTION);
    //maca_sentence_add_section(ctx->ms, section);
    section = maca_sentence_new_section(ctx->ms,MACA_MORPHO_SECTION);
    prefix_id = (char*)malloc(sizeof(char)*(strlen(ctx->ms->id_sentence) +3));
    sprintf(prefix_id,"%s_M",ctx->ms->id_sentence);

    for(i=0; i<gfsm_automaton_n_states(a); i++){
        for (gfsm_arciter_open(&ai,a,i); gfsm_arciter_ok(&ai); gfsm_arciter_next(&ai)){
            t = gfsm_arciter_arc(&ai);
            if(gfsm_arc_lower(t) != gfsmEpsilon){
                lexNode = (xmlNodePtr)maca_ht_index2adr(ht,gfsm_arc_lower(t));
                if(lexNode){
                    temp = (char*)xmlGetProp(lexNode, BAD_CAST "id");
                    sprintf(id_pos, "%s_%s",temp, maca_tags_get_str(ctx->cfg, "morpho", "stype", gfsm_arc_upper(t)));
                    free(temp);
                    // printf("key = %s\n", id_pos);
                    if(posNode = (xmlNodePtr)g_hash_table_lookup(segments_created, id_pos)){
                        t->lower = maca_ht_adr2index(ht,posNode);
                        // printf("segment %s already created\n", id_pos);
                    }
                    else{
                        // printf("add segment %s %s (%s)\n", xmlGetProp(lexNode, BAD_CAST "id"), maca_tags_get_str(ctx->cfg, "morpho", "stype", gfsm_arc_upper(t)), id_pos);
                        posNode = maca_sentence_add_segment(ctx->ms, MACA_MORPHO_SECTION, MACA_CAT_TYPE, prefix_id);
                        t->lower = maca_ht_adr2index(ht,posNode);
                        xmlNewProp(posNode, BAD_CAST "stype", BAD_CAST maca_tags_get_str(ctx->cfg, "morpho", "stype", gfsm_arc_upper(t)));
                        maca_segment_add_elt_from_node(posNode, lexNode, 0);
                        g_hash_table_insert(segments_created, strdup(id_pos), posNode);
                    }
                }
            }
        }
    }
    free(prefix_id);
    //  maca_section_add_automaton(section, a);
    maca_sentence_update_xml_automaton(ctx->ms, MACA_MORPHO_SECTION,a);
    // section->xml_node_fsm = xmlAddChild(section->xml_node, fsm2xml(a, ht));

    g_hash_table_destroy(segments_created);
    //  fsm_affiche(a, ht);
    //maca_section_update_xml_automaton(section, ht, a);
    return section;
}



int maca_crf_tagger_ProcessSentence(maca_sentence * ms, maca_crf_tagger_ctx * ctx)
{
    maca_section * prelex_section;
    maca_section * lex_section;
    gfsmAutomaton *lex_automaton;
    fst::StdVectorFst automaton; 

    ctx->ms = ms;

    if(!maca_sentence_is_section_loaded(ctx->ms,MACA_PRELEX_SECTION))
    {
        prelex_section = maca_sentence_load_section_by_type(ctx->ms,MACA_PRELEX_SECTION);
    }
    else prelex_section = maca_sentence_get_section(ctx->ms, MACA_PRELEX_SECTION);
    if(prelex_section == NULL){
        maca_msg(ctx->module, MACA_ERROR);
        fprintf(stderr,"sentence : %s no prelex section\n", ctx->ms->id_sentence);
        return -1;
    }

    if(!maca_sentence_is_section_loaded(ctx->ms,MACA_LEX_SECTION))
    {
        lex_section = maca_sentence_load_section_by_type(ctx->ms,MACA_LEX_SECTION);
    }
    else lex_section = maca_sentence_get_section(ctx->ms, MACA_LEX_SECTION);
    if(lex_section == NULL){
        maca_msg(ctx->module, MACA_ERROR);
        fprintf(stderr,"sentence : %s no lex section\n", ctx->ms->id_sentence);
        return -1;
    }
    lex_automaton = maca_sentence_get_section_automaton(ctx->ms, MACA_LEX_SECTION);
    if(lex_automaton == NULL){
        maca_msg(ctx->module, MACA_ERROR);
        fprintf(stderr,"sentence : %s no lex automaton\n", ctx->ms->id_sentence);
        return -1;
    }
    gfsm2fst(lex_automaton, automaton);
    crf_tagger(automaton, ctx, ctx->verbose_flag > 4);
    if(ctx->n > 0){
        fst::StdVectorFst nbest;
        fst::ShortestPath(automaton, &nbest, ctx->n);
        create_morpho_section(fst2gfsm(nbest), ctx);
    } else if(ctx->n == -1) {
        create_morpho_section(fst2gfsm(automaton), ctx);
    } else if(ctx->n == -2) {
        macaon::DeterminizeTCLex(&automaton);
        create_morpho_section(fst2gfsm(automaton), ctx);
    } else {
        fprintf(stderr, "error: unknown -n value (%d)\n", ctx->n);
        return -1;
    }
    return 1;
}


