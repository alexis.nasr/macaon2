#include "crf_decoder.hh"
#include "crf_binlexicon.hh"

int main(int argc, char** argv) {
    if(argc != 4 && argc != 3) {
        std::cerr << "convert: " << argv[0] << " <crf-model> <lexicon.in> <lexicon.out>\n";
        std::cerr << "test: cat <text-lexicon> | " << argv[0] << " <crf-model> <bin-lexicon>\n";
        return 1;
    }
    if(argc == 4) {
        macaon::Decoder decoder(argv[1]);
        macaon::BinaryLexicon lexicon(argv[2], decoder.getTagset());
        lexicon.Write(argv[3]);
    } else if(argc == 3) {
        macaon::Decoder decoder(argv[1]);
        macaon::BinaryLexicon lexicon(argv[2], decoder.getTagset());
        std::string line;
        int line_num = 0;
        while(std::getline(std::cin, line)) {
            line_num ++;
            std::vector<int64> tags;
            std::vector<std::string> tokens;
            macaon::Tokenize(line, tokens, "\t ");
            if(lexicon.GetTagsForWord(tokens[0], tags) == false) {
                std::cerr << "WARNING: word not found \"" << tokens[0] << "\", using all tags\n";
            }
            if(tags.size() != tokens.size() - 1) {
                std::cerr << "ERROR: wrong number of tags for entry " << line_num << "\n";
                std::cerr << "    TXT: " << line << "\n";
                std::cerr << "    BIN: " << tokens[0];
                for(size_t i = 0; i < tags.size(); i++) {
                    std::cerr << " " << decoder.getTagset()->Find(tags[i]);
                }
                std::cerr << "\n";
            } else {
                for(size_t i = 0; i < tags.size(); i++) {
                    if(decoder.getTagset()->Find(tags[i]) != tokens[i + 1]) {
                        std::cerr << "ERROR: wrong tag \"" << tokens[i + 1] << "\" => \"" << decoder.getTagset()->Find(tags[i]) << "\", entry " << line_num << "\n";
                    }
                }
            }
        }
    }
    return 0;
}

