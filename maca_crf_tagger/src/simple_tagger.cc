#include "simple_tagger.hh"

macaon::Tagger* Tagger_new(const char* modelName, const char* lexiconName) {
    return new macaon::Tagger(modelName, lexiconName ? lexiconName : "");
}

void Tagger_free(macaon::Tagger* tagger) {
    delete tagger;
}

bool Tagger_ProcessSentence(macaon::Tagger* tagger, int num_words, const char** words, const char** tags) {
    std::vector<std::string> word_vector, tag_vector;
    for(int i = 0; i < num_words; i++) {
        word_vector.push_back(words[i]);
    }
    bool result = tagger->ProcessSentence(word_vector, tag_vector);
    for(int i = 0; i < num_words; i++) {
        tags[i] = strdup(tag_vector[i].c_str());
    }
    return result;
}
