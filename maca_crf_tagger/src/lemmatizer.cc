#include "lemmatizer.h"

int main(int argc, char** argv) {
    if(argc != 2) {
        std::cerr << "usage: " << argv[0] << " <fplm-dictionary>\n";
        return 1;
    }
    macaon::Lemmatizer lemmatizer(argv[1]);
    std::string line;
    while(std::getline(std::cin, line)) {
        std::vector<std::string> tokens;
        macaon::Tokenize(line, tokens, " ");
        for(size_t i = 0; i < tokens.size(); i++) {
            if(i > 0) std::cout << " ";
            std::cout << lemmatizer.lemmatize(tokens[i]);
        }
        std::cout << "\n";
    }
}
