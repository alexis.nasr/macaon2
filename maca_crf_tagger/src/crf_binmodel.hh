#pragma once

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include "crf_model.hh"
#include "crf_template.hh"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

#include <limits.h>
#ifdef CHAR_BIT
#if CHAR_BIT != 8
#error CHAR_BIT != 8 not supported
#endif
#endif

namespace macaon {
// disable alignment in MSVC++
#pragma pack(push, 1)
    struct ModelInfo {
        uint32_t magic;
        uint32_t templateLocation;
        uint32_t numTemplates;
        uint32_t labelLocation;
        uint32_t numLabels;
        uint32_t featureLocation;
        uint32_t tableSize;
    } __attribute__((packed));

    struct TableElement {
        uint32_t hashValue;
        uint16_t keySize;
        uint16_t dataSize;
        uint32_t location;
    } __attribute__((packed)); // disable alignment in g++
#define sizeof_TableElement (sizeof(uint32_t) + sizeof(uint16_t) + sizeof(uint16_t) + sizeof(uint32_t))

    struct LabelWeight {
        uint16_t label;
        float weight;
    } __attribute__((packed));
#define sizeof_LabelWeight (sizeof(uint16_t) + sizeof(float))

    struct LabelPairWeight {
        uint16_t previous;
        uint16_t label;
        float weight;
    } __attribute__((packed));
#define sizeof_LabelPairWeight (sizeof(uint16_t) + sizeof(uint16_t) + sizeof(float))
#pragma pack(pop)

    namespace BinaryModelConstants {
        const uint32_t magic = 0x132a0ab5;
    }

    class BinaryModel : public CRFModel {
        private:
            bool isBinary;
            int fd;
            const char* data;
            size_t dataLength;
            const ModelInfo* info;
            const TableElement* table;
            std::vector<double> B_weights; // cache for B template

            // java hash function
            uint32_t Hash(const char *k, size_t length) {
                uint32_t output = 0;
                for(size_t i = 0; i < length; i++) {
                    output = 31 * output + k[i];
                }
                return output;
            }

        public:
            BinaryModel() : CRFModel(), isBinary(false) {}
            BinaryModel(const std::string &filename) : CRFModel(), isBinary(false), fd(-1), data((const char*) MAP_FAILED) { 
                Load(filename);
            }

            ~BinaryModel() {
                if(data != MAP_FAILED) munmap((void*) data, dataLength);
                if(fd != -1) close(fd);
            }

            bool Convert(const std::string& from, const std::string& to) {
                std::cerr << "loading\n";
                CRFModel::Load(from);
                std::cerr << "writing\n";
                return Write(to);
            }

            // trimming is already performed when writing the bin model
            void TrimModel() {
                std::unordered_map<std::string, int> newFeatures;
                for(std::unordered_map<std::string, int>::const_iterator feature = features.begin(); feature != features.end(); feature++) {
                    if(feature->first[0] != 'B') {
                        int numNonNull = 0;
                        for(size_t i = 0; i < labels.size(); i++) {
                            float weight = weights[feature->second + i];
                            if(weight != 0) numNonNull++;
                        }
                        if(numNonNull > 0) {
                            newFeatures[feature->first] = feature->second;
                        }
                    } else {
                        newFeatures[feature->first] = feature->second;
                    }
                }
                std::cerr << "trim: " << features.size() << " -> " << newFeatures.size() << "\n";
                features = newFeatures;
            }

            bool Write(const std::string & filename) {
                FILE* output = fopen(filename.c_str(), "w");
                // magic
                fwrite(&BinaryModelConstants::magic, sizeof(BinaryModelConstants::magic), 1, output); // magic

                // templates
                uint32_t templateLocation = 0;
                uint32_t templateLocationOffset = (uint32_t) ftell(output);
                fwrite(&templateLocation, sizeof(templateLocation), 1, output);
                uint32_t numTemplates = (uint32_t) templates.size();
                fwrite(&numTemplates, sizeof(numTemplates), 1, output);

                // labels
                uint32_t labelLocation = 0;
                uint32_t labelLocationOffset = (uint32_t) ftell(output);
                fwrite(&labelLocation, sizeof(labelLocation), 1, output);
                uint32_t numLabels = (uint32_t) labels.size();
                fwrite(&numLabels, sizeof(numLabels), 1, output);

                // features
                uint32_t featureLocation = 0;
                uint32_t featureLocationOffset = (uint32_t) ftell(output);
                fwrite(&featureLocation, sizeof(featureLocation), 1, output);
                uint32_t tableSize = (uint32_t) features.size() * 3;
                fwrite(&tableSize, sizeof(tableSize), 1, output);

                // create table
                TableElement* table = (TableElement*) malloc(sizeof(TableElement) * tableSize);
                memset(table, 0, sizeof(TableElement) * tableSize);

                // write templates
                templateLocation = (uint32_t) ftell(output);
                for(std::vector<CRFPPTemplate>::const_iterator i = templates.begin(); i != templates.end(); i++) {
                    fprintf(output, "%s\n", i->text.c_str());
                }

                // write labels
                std::vector<std::string> labelVector(labels.size());
                for(std::unordered_map<std::string, int>::const_iterator label = labels.begin(); label != labels.end(); label++) {
                    labelVector[label->second] = label->first;
                }
                labelLocation = (uint32_t) ftell(output);
                for(size_t i = 0; i < labelVector.size(); i++) {
                    fprintf(output, "%s\n", labelVector[i].c_str());
                }

                // write weights
                int num = 0;
                int totalNumCollisions = 0;
                int numUnigram = 0;
                int numBigram = 0;
                for(std::unordered_map<std::string, int>::const_iterator feature = features.begin(); feature != features.end(); feature++) {
                    num++;
                    TableElement element;
                    element.hashValue = Hash(feature->first.c_str(), feature->first.length()) % tableSize;
                    element.keySize = (uint16_t) feature->first.length();
                    element.dataSize = 0;
                    element.location = (uint32_t) ftell(output);
                    fwrite(feature->first.c_str(), element.keySize, 1, output);
                    if(feature->first[0] == 'B') {
                        for(uint16_t label = 0; label < numLabels; label++) {
                            for(uint16_t previous = 0; previous < numLabels; previous++) {
                                float weight = weights[feature->second + label + numLabels * previous];
                                if(weight != 0) {
                                    LabelPairWeight item;
                                    item.previous = previous;
                                    item.label = label;
                                    item.weight = weight;
                                    fwrite(&item, sizeof(item), 1, output);
                                    element.dataSize ++;
                                }
                            }
                        }
                        numBigram++;
                    } else {
                        for(uint16_t label = 0; label < numLabels; label++) {
                            float weight = weights[feature->second + label];
                            if(weight != 0) {
                                LabelWeight item;
                                item.label = label;
                                item.weight = weight;
                                fwrite(&item, sizeof(item), 1, output);
                                element.dataSize ++;
                            }
                        }
                        numUnigram++;
                    }
                    if(element.dataSize > 0) {
                        uint32_t hash = element.hashValue % tableSize;
                        int numCollisions = 0;
                        while(table[hash].location != 0) {
                            numCollisions++;
                            hash = (hash + 1) % tableSize;
                        }
                        totalNumCollisions += numCollisions;
                        //std::cout << element.hashValue << " " << feature->first << "\n";
                        table[hash] = element;
                    }
                }
                std::cerr << "avg collisions: " << 1.0 * totalNumCollisions / (double) features.size() << "\n";
                std::cerr << "sizeof (label+weight) = " << sizeof_LabelWeight << " * " << numUnigram << "\n";
                std::cerr << "sizeof (label+label+weight) = " << sizeof_LabelPairWeight << " * " << numBigram << "\n";
                std::cerr << "sizeof (entry in table) = " << sizeof_TableElement << " * " << tableSize << "\n";

                // write table
                featureLocation = (uint32_t) ftell(output);
                for(uint32_t i = 0; i < tableSize; i++) {
                    fwrite(&table[i], sizeof(table[i]), 1, output);
                }
                free(table);

                // set section locations
                fseek(output, templateLocationOffset, SEEK_SET);
                fwrite(&templateLocation, sizeof(templateLocation), 1, output);

                // set label locations
                fseek(output, labelLocationOffset, SEEK_SET);
                fwrite(&labelLocation, sizeof(labelLocation), 1, output);

                // set feature locations
                fseek(output, featureLocationOffset, SEEK_SET);
                fwrite(&featureLocation, sizeof(featureLocation), 1, output);

                fclose(output);
                return true;
            }

            bool Load(const std::string& filename) {
                isBinary = false;

                struct stat sb;
                fd = open(filename.c_str(), O_RDONLY);
                if(fd == -1) {
                    std::cerr << "ERROR: could not open crf model \"" << filename << "\"\n";
                    return false;
                }
                if (fstat(fd, &sb) == -1) {
                    std::cerr << "ERROR: could not fstat crf model \"" << filename << "\"\n";
                    return false;
                }
                dataLength = sb.st_size;
                data = (const char*) mmap(NULL, dataLength, PROT_READ, MAP_PRIVATE, fd, 0);
                if(data == MAP_FAILED) {
                    perror("mmap");
                    std::cerr << "ERROR: could mmap() crf model \"" << filename << "\"\n";
                    return false;
                }
                name = filename;

                info = (const ModelInfo*) data;

                // read magic
                if(info->magic != BinaryModelConstants::magic) {
                    //std::cerr << "WARNING: binary crf model format not recognized, trying text model\n";
                    return CRFModel::Load(filename);
                }

                size_t lineSize = 0;

                // read templates
                templates.clear();
                const char* line = (const char*) &data[info->templateLocation];
                for(size_t i = 0; i < info->numTemplates; i++) {
                    lineSize = strchr(line, '\n') - line;
                    char content[lineSize + 1];
                    strncpy(content, line, lineSize);
                    content[lineSize] = '\0';
                    //std::cerr << "TEMPLATE[" << content << "]\n";
                    templates.push_back(CRFPPTemplate(content));
                    line += lineSize + 1;
                }

                // read labels
                labels.clear();
                reverseLabels.clear();
                line = (const char*) &data[info->labelLocation];
                for(uint32_t i = 0; i < info->numLabels; i++) {
                    lineSize = strchr(line, '\n') - line;
                    char content[lineSize + 1];
                    strncpy(content, line, lineSize);
                    content[lineSize] = '\0';
                    //std::cerr << "LABEL[" << content << "]\n";
                    labels[std::string(content)] = (int) i;
                    reverseLabels.push_back(std::string(content));
                    line += lineSize + 1;
                }

                // read table
                table = (const TableElement*) &data[info->featureLocation];

                ComputeWindowOffset();
                loaded = true;
                isBinary = true;
                GetWeights("B", B_weights);
                return true;
            }

            bool GetWeights(const std::string& feature, std::vector<double>& output) {
                if(!isBinary) {
                    std::cerr << "ERROR: called GetWeights() on a non binary model\n";
                    return false;
                }
                size_t keySize = feature.length();
                uint32_t hashValue = Hash(feature.c_str(), keySize) % info->tableSize;
                uint32_t offset = 0;
                size_t numLabels = labels.size();
                while(offset < info->tableSize) {
                    uint32_t location = (hashValue + offset) % info->tableSize;
                    const TableElement& element = table[location];
                    if(element.location == 0) return false;
                    if(element.keySize == keySize) {
                        char key[keySize + 1];
                        strncpy(key, &data[element.location], keySize);
                        key[keySize] = '\0';
                        if(std::string(key) == feature) {
                            //std::cerr << "h:" << element.hashValue << " k:" << element.keySize << " d:" << element.dataSize << "\n";
                            if(feature[0] == 'B') {
                                output.assign(numLabels * numLabels, 0);
                                const LabelPairWeight* items = (const LabelPairWeight*) &data[element.location + keySize];
                                for(int i = 0; i < element.dataSize; i++) {
                                    output[items[i].label + numLabels * items[i].previous] = items[i].weight;
                                }
                            } else {
                                output.assign(numLabels, 0);
                                const LabelWeight* items = (const LabelWeight*) &data[element.location + keySize];
                                for(int i = 0; i < element.dataSize; i++) {
                                    output[items[i].label] = items[i].weight;
                                }
                            }
                            return true;
                        }
                    }
                    offset++;
                }
                return false;
            }

            /* note: this function can use bigram templates conditionned on observations */
            double rescore(const std::vector<std::vector<std::string> > &input, const std::vector<int> &context, const std::vector<int> &context_tags) {
                if(!isBinary) {
                    return CRFModel::rescore(input, context, context_tags);
                }
                double output = 0;
                if((int) context.size() != window_length) return 0;
                if(context[window_offset] < 0) return 0;
                const int label = context_tags[window_offset]; //ilabels[input[context[window_offset]][input[context[window_offset]].size() - 1]];
                int previous = -1;
                if(window_length > 1 && context[window_offset - 1] >=0) previous = context_tags[window_offset - 1]; 
                for(std::vector<CRFPPTemplate>::const_iterator i = templates.begin(); i != templates.end(); i++) {
                    std::string feature = i->applyToClique(input, context, window_offset);
                    std::vector<double> feature_weights;
                    if(GetWeights(feature, feature_weights)) {
                        if(i->type == CRFPPTemplate::UNIGRAM) output += feature_weights[label];
                        else if(previous != -1) output += feature_weights[label + labels.size() * previous];
                    }
                }
                return output;
            }

            /* note: this function CANNOT use bigram templates conditionned on observations */
            double transition(int previous, int label) {
                if(!isBinary) return CRFModel::transition(previous, label);
                return B_weights[label + info->numLabels * previous];
            }

            void emissions(const std::vector<std::vector<std::string> > &input, const std::vector<int> &context, std::vector<double>& output) {
                if(!isBinary) {
                    CRFModel::emissions(input, context, output);
                    return;
                }
                output.assign(labels.size(), 0);
                if((int) context.size() != window_length) return;
                if(context[window_offset] == -1) return;
                for(std::vector<CRFPPTemplate>::const_iterator i = templates.begin(); i != templates.end(); i++) {
                    if(i->type == CRFPPTemplate::UNIGRAM) {
                        std::string feature = i->applyToClique(input, context, window_offset);
                        std::vector<double> feature_weights;
                        if(GetWeights(feature, feature_weights)) {
                            for(size_t label = 0; label < labels.size(); label++) 
                                output[label] += feature_weights[label];
                        }
                    }
                }
            }
    };
}
