/***************************************************************************
    Copyright (C) 2011 by xxx <xxx@lif.univ-mrs.fr>
    This file is part of maca_crf_tagger.

    Maca_crf_tagger is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Maca_crf_tagger is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with maca_crf_tagger. If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

#include "maca_crf_tagger.hh"

char * maca_crf_tagger_GetVersion()
{
  return MACA_CRF_TAGGER_VERSION;
}

void maca_crf_tagger_add_stamp(xmlNodePtr node)
{
  add_maca_stamp(node,MACA_CRF_TAGGER_NAME,MACA_CRF_TAGGER_VERSION);
}


