#pragma once
#include <list>
#ifdef __APPLE__
#include "../../../third_party/unordered_map/unordered_map.hpp"
#else
#include <unordered_map>
#endif
#include "crf_binmodel.hh"
#include "crf_utils.hh"
#include "crf_binlexicon.hh"

namespace macaon {

    struct Decoder {

        //CRFModel model;
        BinaryModel model;
        Symbols tagSet;

        Decoder() : tagSet("tagset") { }
        Decoder(const std::string &filename) : tagSet("tagset") { 
            model.Load(filename); 
            tagSet.AddSymbol("<eps>", 0);
            for(std::unordered_map<std::string, int>::const_iterator label = model.labels.begin(); label != model.labels.end(); label++) {
                tagSet.AddSymbol(label->first, label->second + 1);
            }
        }

        Symbols* getTagset() {
            return &tagSet;
        }

        bool IsLoaded() const {
            return model.IsLoaded();
        }

        /* Faster decoder for simple sequences.
         * This function supports an optional lexicon to specify allowed word/tags. And attional option sets the location of the word in the feature vector.
         * */
        void decodeString(const std::vector<std::vector<std::string> > &features, std::vector<std::string> &predictions, const BinaryLexicon* lexicon=NULL, int wordFeatureLocation=0) {
            int length = features.size();
            int numLabels = model.labels.size();

            /*if(lexicon->NumLabels() != numLabels) {
                std::cerr << "ERROR: num label mismatch between model and lexicon\n";
                return;
            }*/

            // store score and backtrack matrices (TODO: size matrices according to possible word/tag assoc)
            std::vector<std::vector<double> > scores(length, std::vector<double>(numLabels, 0.0));
            std::vector<std::vector<int> > backtrack(length, std::vector<int>(numLabels, 0));
            /*double** scores = new double*[length];
            int** backtrack = new int*[length];

            for(int i = 0; i < length; i++) {
				scores[i] = new double[numLabels];
				backtrack[i] = new int[numLabels];
                for(int j = 0; j < numLabels; j++) {
                    backtrack[i][j] = -1;
                    scores[i][j] = 0.0;
                }
            }*/

            // possible tags for each word: use lexicon if provided
            std::vector<std::vector<int64> > wordTags(length);
            std::vector<int64> allTags(numLabels);
            for(int label = 0; label < numLabels; label++) allTags[label] = label + 1; // warning: there is an offset of one for epsilon transitions

            // perform viterbi search for the maximum scoring labeling
            for(int current = 0; current < length; current++) {
                // honor lexicon or allow all tags
                if(lexicon != NULL) lexicon->GetTagsForWord(features[current][wordFeatureLocation], wordTags[current]);
                else wordTags[current] = allTags;

                // create context vector (offset of features for current word)
                std::vector<int> context(model.window_length);
                for(int i = 0; i < model.window_length; i++) 
                    if(current + i - model.window_offset >= 0 && current + i - model.window_offset < length) context[i] = (current + i - model.window_offset);
                    else context[i] = -1;

                // compute emissions and find highest scoring transition pair
                if(current == 0) {
                    // TODO: compute emissions only for valid word/tags pairs
                    std::vector<double> emissions;
                    model.emissions(features, context, emissions);
                    for(int e = 0; e < numLabels; e++) scores[current][e] = emissions[e];
                } else {
                    std::vector<double> emissions;
                    model.emissions(features, context, emissions);
                    for(int e = 0; e < numLabels; e++) scores[current][e] = emissions[e];
                    for(size_t i = 0; i < wordTags[current].size(); i++) {
                        int label = wordTags[current][i] - 1;
                        if(label < 0 || label >= numLabels) {
                            std::cerr << "ERROR: unexpected label (" << label << ") from lexicon, please check that it is compatible with model.\n";
                            return;
                        }
                        double max = 0;
                        int argmax = -1;
                        for(size_t j = 0; j < wordTags[current - 1].size(); j++) {
                            int previous = wordTags[current - 1][j] - 1;
                            if(previous < 0 || previous >= numLabels) {
                                std::cerr << "ERROR: unexpected label (" << previous << ") from lexicon, please check that it is compatible with model.\n";
                                return;
                            }
                            double score = scores[current][label] + scores[current - 1][previous] + model.transition(previous, label);
                            if(argmax == -1 || max < score) {
                                max = score;
                                argmax = previous;
                            }
                        }
                        scores[current][label] = max;
                        backtrack[current][label] = argmax;
                    }
                }
            }
            // find last label
            double max = 0;
            int argmax = -1;
            if(length > 0) {
                for(size_t i = 0; i < wordTags[length - 1].size(); i++) {
                    int label = wordTags[length - 1][i] - 1;
                    if(argmax == -1 || scores[length - 1][label] > max) {
                        max = scores[length - 1][label];
                        argmax = label;
                    }
                }
            }

            // backtrack solution
            int current = length - 1;
            predictions.clear();
            predictions.resize(length);
            while(current >= 0) {
                predictions[current] = model.reverseLabels[argmax];
                argmax = backtrack[current][argmax];
                current --;
            }

			/*for(int i = 0; i < length; i++) {
				delete scores[i];
				delete backtrack[i];
			}
			delete scores;
			delete backtrack;*/
        }
    };
}
