#include <vector>
#include "crf_decoder.hh"
#include "crf_binlexicon.hh"
#include "crf_features.hh"

void tag_sentence(macaon::Decoder& decoder, macaon::BinaryLexicon* lexicon, const std::vector<std::string>& words) {

    std::vector<std::vector<std::string> > features;
    for(size_t i = 0; i < words.size(); i++) {
        std::vector<std::string> word_features;
        macaon::FeatureGenerator::get_pos_features(words[i], word_features);
        features.push_back(word_features);
        /*for(size_t j = 0; j < word_features.size(); j++) std::cout << word_features[j] << " ";
        std::cout << "\n";*/
    }
    std::vector<std::string> tagged;
    decoder.decodeString(features, tagged, lexicon);
    for(size_t i = 0; i < tagged.size(); i++) {
        if(i > 0) std::cout << " ";
        std::cout << words[i] << "/" << tagged[i];
    }
    std::cout << "\n";
}

void usage(const char* argv0) {
    std::cerr << "usage: " << argv0 << " <model> [lexicon]\n";
    exit(1);
}

int main(int argc, char** argv) {
    std::string modelName = "";
    std::string lexiconName = "";

    for(int i = 1; i < argc; i++) {
        std::string arg = argv[i];
        if(arg == "-h" || arg == "--help") {
            usage(argv[0]);
        } else if(modelName == "") {
            modelName = arg;
        } else if(lexiconName =="") {
            lexiconName = arg;
        } else {
            usage(argv[0]);
        }
    }
    if(modelName == "") usage(argv[0]);

    macaon::Decoder decoder(modelName);
    macaon::BinaryLexicon *lexicon = NULL;
    if(lexiconName != "") lexicon = new macaon::BinaryLexicon(lexiconName, decoder.getTagset());

    std::string line;
    while(std::getline(std::cin, line)) {
        std::vector<std::string> words;
        macaon::Tokenize(line, words, " ");
        tag_sentence(decoder, lexicon, words);
    }
    if(lexicon) delete lexicon;
    return 0;
}
