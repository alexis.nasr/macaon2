#pragma once
#include <string>
#include <vector>
#ifdef __APPLE__
#include "../../../third_party/unordered_map/unordered_map.hpp"
#else
#include <unordered_map>
#endif
#include <stdio.h>
#include <errno.h>
#include "crf_template.hh"

namespace macaon {
    class CRFModel {
    protected:
        std::string name;
        std::vector<CRFPPTemplate> templates;
        int version;
        double cost_factor;
        int maxid;
        int xsize;
        std::unordered_map<std::string, int> features;
        std::vector<float> weights;
        bool loaded;
        int bigramWeightLocation;
    public:
        std::unordered_map<std::string, int> labels;
        std::vector<std::string> reverseLabels;
        int window_offset;
        int window_length;
        CRFModel() : loaded(false) {}
        CRFModel(const std::string &filename) : loaded(false) { Load(filename); }

        bool Load(const std::string &filename) {
            name = filename;
            FILE* fp = fopen(filename.c_str(), "r");
            if(!fp) {
                fprintf(stderr, "ERROR: %s, %s\n", filename.c_str(), strerror(errno));
                return false;
            }
            char line[1024];
            int section = 0;
            int header_num = 0;
            int line_num = 0;
            int num_non_null = 0;
            while(NULL != fgets(line, 1024, fp)) {
                line_num ++;
                if(line[0] == '\n') {
                    section ++;
                } else {
                    line[1023] = '\0';
                    line[strlen(line) - 1] = '\0'; // chomp
                    if(section == 0) { // header
                        char* space = line;
                        while(*space != ' ' && *space != '\0') space ++;
                        if(header_num == 0) version = strtol(space + 1, NULL, 10);
                        else if(header_num == 1) cost_factor = strtod(space + 1, NULL);
                        else if(header_num == 2) maxid = strtol(space + 1, NULL, 10);
                        else if(header_num == 3) xsize = strtol(space + 1, NULL, 10);
                        else {
                            fprintf(stderr, "ERROR: unexpected header line %d in %s\n", line_num, filename.c_str());
                            fclose(fp);
                            return false;
                        }
                        header_num ++;
                    } else if (section == 1) { // labels
                        int next_id = labels.size();
                        labels[std::string(line)] = next_id;
                        reverseLabels.push_back(std::string(line));
                    } else if (section == 2) { // templates
                        templates.push_back(CRFPPTemplate(line));
                    } else if (section == 3) { // feature indexes
                        char* space = line;
                        while(*space != ' ' && *space != '\0') space ++;
                        *space = '\0';
                        int index = strtol(line, NULL, 10);
                        features[std::string(space + 1)] = index;
                    } else if (section == 4) { // weights
                        float weight = (float) strtod(line, NULL);
                        if(weight != 0) num_non_null++;
                        weights.push_back(weight);
                    } else {
                        fprintf(stderr, "ERROR: too many sections in %s\n", filename.c_str());
                        fclose(fp);
                        return false;
                    }
                }
            }
            //std::cerr << "weights: " << num_non_null << "/" << weights.size() << "\n";
            fclose(fp);

            ComputeWindowOffset();

            std::unordered_map<std::string, int>::const_iterator found = features.find("B");
            if(found != features.end()) {
                bigramWeightLocation = found->second;
            }
            loaded = true;
            return true;
        }

        void ComputeWindowOffset() {
            int max_template_offset = 0;
            int min_template_offset = 9;
            for(std::vector<CRFPPTemplate>::const_iterator i = templates.begin(); i != templates.end(); i++) {
                if(i->type == CRFPPTemplate::BIGRAM && min_template_offset > -1) min_template_offset = -1; // account for label bigram 
                for(std::vector<TemplateItem>::const_iterator j = i->items.begin(); j != i->items.end(); j++) {
                    if(j->line < min_template_offset) min_template_offset = j->line;
                    if(j->line > max_template_offset) max_template_offset = j->line;
                }
            }
            window_offset = - min_template_offset;
            window_length = max_template_offset - min_template_offset + 1;
        }

        bool IsLoaded() const {
            return loaded;
        }

        /* note: this function can use bigram templates conditionned on observations */
        virtual double rescore(const std::vector<std::vector<std::string> > &input, const std::vector<int> &context, const std::vector<int> &context_tags) {
            double output = 0;
            if((int) context.size() != window_length) return 0;
            //std::cerr << context[window_offset] << std::endl;
            if(context[window_offset] < 0) return 0;
            const int label = context_tags[window_offset]; //ilabels[input[context[window_offset]][input[context[window_offset]].size() - 1]];
            int previous = -1;
            if(window_length > 1 && context[window_offset - 1] >=0) previous = context_tags[window_offset - 1]; //labels[input[context[window_offset - 1]][input[context[window_offset - 1]].size() - 1]];
            for(std::vector<CRFPPTemplate>::const_iterator i = templates.begin(); i != templates.end(); i++) {
                std::string feature = i->applyToClique(input, context, window_offset);
                //std::cerr << "feature: " << feature << std::endl;
                std::unordered_map<std::string, int>::const_iterator found = features.find(feature);
                if(found != features.end()) {
                    if(found->second >= 0 && found->second < (int) weights.size()) {
                        if(i->type == CRFPPTemplate::UNIGRAM) output += weights[found->second + label];
                        else if(previous != -1) output += weights[found->second + label + labels.size() * previous];
                    }
                }
            }
            return output;
        }

        /* note: this function CANNOT use bigram templates conditionned on observations */
        virtual double transition(int previous, int label) {
            if(bigramWeightLocation < 0) return 0;
            return weights[bigramWeightLocation + label + labels.size() * previous];
        }

        virtual void emissions(const std::vector<std::vector<std::string> > &input, const std::vector<int> &context, std::vector<double>& output) {
            output.clear();
            output.resize(labels.size());
            if((int) context.size() != window_length) return;
            if(context[window_offset] == -1) return;
            for(std::vector<CRFPPTemplate>::const_iterator i = templates.begin(); i != templates.end(); i++) {
                std::string feature = i->applyToClique(input, context, window_offset);
                //std::cerr << " " << feature;
                std::unordered_map<std::string, int>::const_iterator found = features.find(feature);
                if(found != features.end()) {
                    if(found->second >= 0 && found->second < (int) weights.size()) {
                        if(i->type == CRFPPTemplate::UNIGRAM) 
                            for(size_t label = 0; label < labels.size(); label++) 
                                output[label] += weights[found->second + label];
                    }
                }
                //else std::cerr << "*";
            }
            //std::cerr << "\n";
        }
    };
}
