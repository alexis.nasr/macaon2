#include <cstdlib>
#include <vector>
#include "crf_decoder.hh"
#include "crf_binlexicon.hh"
#include "crf_features.hh"

/* This is a sample decoder for the crf tagger.
   compile with: 
   g++ -O3 -Wall -o barebones_decoder barebones_decoder.cc 
   
   example usage:
   echo -e "I\nam\nyour\nfather\n\njhon\neats\npotatoes\n" | ./barebones_decoder en/bin/crf_tagger.model.bin en/bin/crf_tagger.wordtag.lexicon
   */

void tag_sentence(macaon::Decoder& decoder, macaon::BinaryLexicon* lexicon, const std::vector<std::vector<std::string> >& lines, int wordField, bool isConll07) {

    std::vector<std::vector<std::string> > features;
    for(size_t i = 0; i < lines.size(); i++) {
        std::vector<std::string> word_features;
        macaon::FeatureGenerator::get_pos_features(lines[i][wordField], word_features);
        features.push_back(word_features);
        //for(size_t j = 0; j < word_features.size(); j++) std::cout << word_features[j] << " ";
        //std::cout << "\n";
    }
    std::vector<std::string> tagged;
    decoder.decodeString(features, tagged, lexicon);
    for(size_t i = 0; i < tagged.size(); i++) {
        if(isConll07) {
            for(size_t j = 0; j < lines[i].size(); j++) {
                if(j != 0) std::cout << "\t";
                if(j == 3 || j == 4) std::cout << tagged[i];
                else std::cout << lines[i][j];
            }
            std::cout << "\n";
        } else {
            for(size_t j = 0; j < lines[i].size(); j++) {
                std::cout << lines[i][j] << "\t";
            }
            std::cout << tagged[i] << "\n";
        }
    }
    std::cout << "\n";
}

void usage(const char* argv0) {
    std::cerr << "usage: " << argv0 << " [--conll07|--column <num>] [<model> [lexicon]|-L <lang> [-D <model-dir>] [--nolexicon]]\n";
    exit(1);
}

int main(int argc, char** argv) {
    bool isConll07 = false; // warning: no verification of conll07 format
    int word_offset = 0;
    std::string modelDir = "";
    std::string modelLang = "";
    char* macaon_dir = getenv("MACAON_DIR");
    if(macaon_dir != NULL) modelDir = macaon_dir;
    std::string modelName = "";
    std::string lexiconName = "";
    bool noLexicon = false;

    for(int i = 1; i < argc; i++) {
        std::string arg = argv[i];
        if(arg == "-h" || arg == "--help") {
            usage(argv[0]);
        } else if(arg == "--conll07") {
            isConll07 = true;
            word_offset = 1;
        } else if(arg == "--column" && i < argc - 1) {
            arg = argv[i + 1];
            word_offset = strtol(arg.c_str(), NULL, 10) - 1;
            i++;
        } else if(arg == "-L") {
            arg = argv[i + 1];
            modelLang = arg;
            i++;
        } else if(arg == "-D") {
            arg = argv[i + 1];
            modelDir = arg;
            i++;
        } else if(arg == "--nolexicon") {
            noLexicon = true;
        } else if(modelName == "") {
            modelName = arg;
        } else if(lexiconName =="") {
            lexiconName = arg;
        } else {
            usage(argv[0]);
        }
    }
    if(modelDir != "" && modelLang != "") {
        modelName = modelDir + "/" + modelLang + "/bin/crf_tagger_model.bin";
        if(!noLexicon) lexiconName = modelDir + "/" + modelLang + "/bin/crf_tagger_wordtag_lexicon.bin";
    }
    if(modelName == "" || word_offset < 0) usage(argv[0]);

    macaon::Decoder decoder(modelName);
    if(!decoder.IsLoaded()) return 1;
    macaon::BinaryLexicon *lexicon = NULL;
    if(lexiconName != "") {
        lexicon = new macaon::BinaryLexicon(lexiconName, decoder.getTagset());
        if(!lexicon->IsLoaded()) return 1;
    }

    std::string line;
    std::vector<std::vector<std::string> > lines;
    while(std::getline(std::cin, line)) {
        if(line == "") {
            tag_sentence(decoder, lexicon, lines, word_offset, isConll07);
            lines.clear();
        } else {
            std::vector<std::string> tokens;
            macaon::Tokenize(line, tokens, "\t");
            lines.push_back(tokens);
        }
    }
    if(!lines.empty()) {
        tag_sentence(decoder, lexicon, lines, word_offset, isConll07);
    }
    if(lexicon) delete lexicon;
    return 0;
}
