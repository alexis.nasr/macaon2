#pragma once

#include <string>
#include <vector>

#define int64 int

namespace macaon {
    class Symbols {
        protected:
            std::string name;
            std::unordered_map<std::string, int> word2int;
            std::unordered_map<int, std::string> int2word;
        public:
            Symbols(std::string _name) : name(_name) {}
            int AddSymbol(const std::string& symbol, int value = -1) {
                if(value == -1) value = word2int.size();
                word2int[symbol] = value;
                int2word[value] = symbol;
                return value;
            }
            int Find(const std::string& word) const {
                std::unordered_map<std::string, int>::const_iterator found = word2int.find(word);
                if(found != word2int.end()) return found->second;
                return -1;
            }
            const std::string Find(const int64 id) const {
                std::unordered_map<int, std::string>::const_iterator found = int2word.find(id);
                if(found != int2word.end()) return found->second;
                return "";
            }
            int NumSymbols() const {
                return word2int.size();
            }
            friend class SymbolsIterator;
    };
    class SymbolsIterator {
            const Symbols& symbols;
            std::unordered_map<std::string, int>::const_iterator iter;
        public:
            SymbolsIterator(const Symbols& _symbols) : symbols(_symbols) { 
                iter = symbols.word2int.begin();
            }
            bool Done() { 
                return iter == symbols.word2int.end();
            }
            void Next() {
                iter++;
            }
            const std::string Symbol() {
                return iter->first;
            }
            int Value() {
                return iter->second;
            }
    };

    // http://www.oopweb.com/CPP/Documents/CPPHOWTO/Volume/C++Programming-HOWTO-7.html
    static void Tokenize(const std::string& str, std::vector<std::string>& tokens, const std::string& delimiters = " ", bool strict = false)
    {
        std::string::size_type lastPos = str.find_first_not_of(delimiters, 0);
        std::string::size_type pos     = str.find_first_of(delimiters, lastPos);
        tokens.clear();
        while (std::string::npos != pos || std::string::npos != lastPos)
        {
            tokens.push_back(str.substr(lastPos, pos - lastPos));
            if(strict) {
                if(pos == std::string::npos) break;
                lastPos = pos + 1;
            } else lastPos = str.find_first_not_of(delimiters, pos);
            pos = str.find_first_of(delimiters, lastPos);
        }
    }

}
