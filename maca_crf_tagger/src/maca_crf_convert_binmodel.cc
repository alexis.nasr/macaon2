#include "crf_binmodel.hh"

int main(int argc, char** argv) {
    if(argc != 3 && argc != 2) {
        std::cerr << "usage: " << argv[0] << " <from> <to> or <binmodel>\n";
        return 1;
    }
    macaon::BinaryModel model;
    if(argc == 3) {
        model.Convert(argv[1], argv[2]);
    } else {
        model.Load(argv[1]);
        std::vector<double> weights;
        model.GetWeights("U18=a/jamais", weights);
        for(size_t i = 0; i < weights.size(); i++) {
            std::cout << weights[i] << " ";
        }
        std::cout << "\n";
        //model.Dump();
    }
    return 0;
}

