#include <stdio.h>

#include "simple_tagger.hh"

int main(int argc, char** argv) {
    int num_words = 6;
    const char* words[] = {"le", "petit", "chat", "boit", "du", "lait"};
    const char* tags[6];
    int i;

    if(argc != 3) {
        fprintf(stderr, "usage: %s <tagger-model> <tagger-lexicon>\n", argv[0]);
        return 1;
    }

    macaon::Tagger* tagger = Tagger_new(argv[1], argv[2]);

    Tagger_ProcessSentence(tagger, num_words, words, tags);

    for(i = 0; i < num_words; i++) {
        printf("%s %s\n", words[i], tags[i]);
    }

    Tagger_free(tagger);

    return 0;
}
