#pragma once

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include "crf_model.hh"
#include "crf_template.hh"
#include "crf_lexicon.hh"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

#include <limits.h>
#ifdef CHAR_BIT
#if CHAR_BIT != 8
#error CHAR_BIT != 8 not supported
#endif
#endif

namespace macaon {
    const uint32_t lexiconMagic = 0xbffe1253;

    class BinaryLexicon : public Lexicon {
        // disable alignment in MSVC++
#pragma pack(push, 1)
        struct ModelInfo {
            uint32_t magic;
            uint32_t dataLocation;
            uint32_t tableSize;
            uint32_t numLabels;
        } __attribute__((packed));

        struct TableElement {
            uint32_t hashValue;
            uint8_t keySize;
            uint8_t dataSize;
            uint32_t location;
        } __attribute__((packed)); // disable alignment in g++
#define lexicon_tag_t uint8_t
#define sizeof_LexiconTableElement (sizeof(uint32_t) + sizeof(uint8_t) + sizeof(uint8_t) + sizeof(uint32_t))

#pragma pack(pop)

        private:
        bool isBinary;
        int fd;
        const char* data;
        size_t dataLength;
        const ModelInfo* info;
        const TableElement* table;

        // copied from https://smhasher.googlecode.com/svn-history/r136/trunk/MurmurHash3.cpp (MIT license)

        static inline uint32_t rotl32 ( uint32_t x, int8_t r ) 
        {
            return (x << r) | (x >> (32 - r));
        }

        static inline uint64_t rotl64 ( uint64_t x, int8_t r )
        {
            return (x << r) | (x >> (64 - r));
        }

#define	ROTL32(x,y)	rotl32(x,y)
#define ROTL64(x,y)	rotl64(x,y)

#define BIG_CONSTANT(x) (x##LLU)
#define	FORCE_INLINE inline

        static FORCE_INLINE uint32_t getblock ( const uint32_t * p, int i )
        {
            return p[i];
        }

        static FORCE_INLINE uint64_t getblock ( const uint64_t * p, int i ) 
        {
            return p[i];
        }

        static FORCE_INLINE uint32_t fmix ( uint32_t h )
        {
            h ^= h >> 16;
            h *= 0x85ebca6b;
            h ^= h >> 13;
            h *= 0xc2b2ae35;
            h ^= h >> 16;

            return h;
        }

        //----------

        static FORCE_INLINE uint64_t fmix ( uint64_t k ) 
        {
            k ^= k >> 33;
            k *= BIG_CONSTANT(0xff51afd7ed558ccd);
            k ^= k >> 33;
            k *= BIG_CONSTANT(0xc4ceb9fe1a85ec53);
            k ^= k >> 33;

            return k;
        }

        static void MurmurHash3_x86_32 ( const void * key, int len, uint32_t seed, void * out ) 
        {
            const uint8_t * data = (const uint8_t*)key;
            const int nblocks = len / 4;

            uint32_t h1 = seed;

            uint32_t c1 = 0xcc9e2d51;
            uint32_t c2 = 0x1b873593;

            //----------
            // body

            const uint32_t * blocks = (const uint32_t *)(data + nblocks*4);

            for(int i = -nblocks; i; i++)
            {
                uint32_t k1 = getblock(blocks,i);

                k1 *= c1;
                k1 = ROTL32(k1,15);
                k1 *= c2;

                h1 ^= k1;
                h1 = ROTL32(h1,13); 
                h1 = h1*5+0xe6546b64;
            }

            //----------
            // tail

            const uint8_t * tail = (const uint8_t*)(data + nblocks*4);

            uint32_t k1 = 0;

            switch(len & 3)
            {
                case 3: k1 ^= tail[2] << 16;
                case 2: k1 ^= tail[1] << 8;
                case 1: k1 ^= tail[0];
                        k1 *= c1; k1 = ROTL32(k1,15); k1 *= c2; h1 ^= k1;
                        //std::cerr << k1 << " " << h1 << " " << seed << "\n";
            };

            //----------
            // finalization

            h1 ^= len;

            h1 = fmix(h1);

            *(uint32_t*)out = h1;
        } 

        static uint32_t Hash(const char *k, size_t length) {
            uint32_t output = 0;
            MurmurHash3_x86_32(k, length, BinaryModelConstants::magic, &output);

            // java hash function
            /*uint32_t output = 0;
            for(size_t i = 0; i < length; i++) {
                output = 31 * output + k[i];
            }*/
            return output;
        }

        public:
        BinaryLexicon() : Lexicon(), isBinary(false) {}
        BinaryLexicon(const std::string &filename, Symbols* _tagSymbols = NULL) : Lexicon(), isBinary(false), fd(-1), data((const char*) MAP_FAILED) { 
            tagSymbols = _tagSymbols;
            Load(filename);
        }

        ~BinaryLexicon() {
            if(data != MAP_FAILED) munmap((void*) data, dataLength);
            if(fd != -1) close(fd);
        }

        bool Convert(const std::string& from, const std::string& to) {
            std::cerr << "loading\n";
            Lexicon::Load(from);
            std::cerr << "writing\n";
            return Write(to);
        }

        bool Write(const std::string & filename) {
            FILE* output = fopen(filename.c_str(), "w");
            // magic
            fwrite(&lexiconMagic, sizeof(lexiconMagic), 1, output); // magic

            // features
            uint32_t dataLocation = 0;
            uint32_t dataLocationOffset = (uint32_t) ftell(output);
            fwrite(&dataLocation, sizeof(dataLocation), 1, output);
            uint32_t tableSize = (uint32_t) wordSymbols.NumSymbols() * 2;
            fwrite(&tableSize, sizeof(tableSize), 1, output);
            uint32_t numLabels = tagsForWord[kUnknownWordTags].size();
            fwrite(&numLabels, sizeof(numLabels), 1, output);

            // create table
            TableElement* table = (TableElement*) malloc(sizeof(TableElement) * tableSize);
            memset(table, 0, sizeof(TableElement) * tableSize);

            // write entries
            int num = 0;
            int totalNumCollisions = 0;
            int numTags = 0;
            int sizeOfKeys = 0;
            for(SymbolsIterator siter(wordSymbols); !siter.Done(); siter.Next()) {
                std::string word = siter.Symbol();
                if(tagsForWordEntry.find(siter.Value()) == tagsForWordEntry.end()) {
                    continue;
                } 
                int64 id = tagsForWordEntry[siter.Value()];

                num++;
                TableElement element;
                element.hashValue = Hash(word.c_str(), word.length()) % tableSize;
                element.keySize = (uint8_t) word.length();
                element.dataSize = tagsForWord[id].size();
                numTags += element.dataSize;
                element.location = (uint32_t) ftell(output);
                fwrite(word.c_str(), element.keySize, 1, output);
                sizeOfKeys += element.keySize;
                for(size_t tag = 0; tag < tagsForWord[id].size(); tag++) {
                    lexicon_tag_t packed = (lexicon_tag_t) tagsForWord[id][tag];
                    fwrite(&packed, sizeof(packed), 1, output);
                }
                if(element.dataSize > 0) {
                    uint32_t hash = element.hashValue % tableSize;
                    int numCollisions = 0;
                    while(table[hash].location != 0) {
                        numCollisions++;
                        hash = (hash + 1) % tableSize;
                    }
                    totalNumCollisions += numCollisions;
                    table[hash] = element;
                }
            }
            std::cerr << "avg collisions: " << 1.0 * totalNumCollisions / (double) wordSymbols.NumSymbols() << "\n";
            std::cerr << "sizeof (keys) = " << sizeOfKeys << "\n";
            std::cerr << "sizeof (tags) = " << sizeof(lexicon_tag_t) << " * " << numTags << "\n";
            std::cerr << "sizeof (entry in table) = " << sizeof_LexiconTableElement << " * " << tableSize << "\n";

            // write table
            dataLocation = (uint32_t) ftell(output);
            for(uint32_t i = 0; i < tableSize; i++) {
                fwrite(&table[i], sizeof(table[i]), 1, output);
            }
            free(table);

            // set feature locations
            fseek(output, dataLocationOffset, SEEK_SET);
            fwrite(&dataLocation, sizeof(dataLocation), 1, output);

            fclose(output);
            return true;
        }

        bool Load(const std::string& filename) {
            isBinary = false;

            struct stat sb;
            fd = open(filename.c_str(), O_RDONLY);
            if(fd == -1) {
                std::cerr << "ERROR: could not open crf lexicon \"" << filename << "\"\n";
                return false;
            }
            if (fstat(fd, &sb) == -1) {
                std::cerr << "ERROR: could not fstat crf lexicon \"" << filename << "\"\n";
                return false;
            }
            dataLength = sb.st_size;
            data = (const char*) mmap(NULL, dataLength, PROT_READ, MAP_PRIVATE, fd, 0);
            if(data == MAP_FAILED) {
                perror("mmap");
                std::cerr << "ERROR: could mmap() crf lexicon \"" << filename << "\"\n";
                return false;
            }

            info = (const ModelInfo*) data;

            // read magic
            if(info->magic != lexiconMagic) {
                bool result = Lexicon::Load(filename);
                if(result == false) {
                    std::cerr << "ERROR: invalid magic or unsupported version in binary crf lexicon. Please reconvert it from text model.\n";
                }
                return result;
            }

            // read table
            table = (const TableElement*) &data[info->dataLocation];

            loaded = true;
            isBinary = true;
            return true;
        }

        bool GetTagsForWord(int64 word, std::vector<int64>& output) const {
            if(!isBinary) {
                return Lexicon::GetTagsForWord(word, output);
            }
            std::cerr << "ERROR: GetTagsForWord() not supported on binary models\n";
            abort();
            return false;
        }

        int NumLabels() const {
            if(!isBinary) return Lexicon::NumLabels();
            return info->numLabels;
        }

        bool GetTagsForWord(const std::string& word, std::vector<int64>& output) const {
            if(!isBinary) {
                return Lexicon::GetTagsForWord(word, output);
                //std::cerr << "ERROR: called GetTagsForWord() on a non binary model\n";
                //return false;
            }
            if(word == "<eps>") {
                output.clear();
                output.push_back(0);
            }
            size_t keySize = word.length();
            uint32_t hashValue = Hash(word.c_str(), keySize); // % info->tableSize;
            uint32_t offset = 0;
            while(offset < info->tableSize) {
                uint32_t location = (hashValue + offset) % info->tableSize;
                const TableElement& element = table[location];
                /*std::cerr << word << " " << location << " " << hashValue << " " << offset << " " << info->tableSize << 
                    "|" << element.hashValue << " " << (int) element.keySize << " " << (int) element.dataSize << " " << element.location << 
                    "\n";*/
                if(element.location == 0) break;
                if(element.keySize == keySize) {
                    char key[keySize + 1];
                    strncpy(key, &data[element.location], keySize);
                    key[keySize] = '\0';
                    if(std::string(key) == word) {
                        //std::cerr << "h:" << element.hashValue << " k:" << element.keySize << " d:" << element.dataSize << "\n";
                        output.clear();
                        const lexicon_tag_t* tags = (const lexicon_tag_t*) &data[element.location + keySize];
                        for(int i = 0; i < element.dataSize; i++) {
                            output.push_back(tags[i]);
                        }
                        return true;
                    }
                }
                offset++;
            }
            // unknown word
            output.clear();
            for(int i = 1; i < (int) info->numLabels + 1; i++) output.push_back(i);
            return false;
        }

    };
}
