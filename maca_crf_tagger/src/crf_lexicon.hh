#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <stdint.h>
#ifdef __APPLE__
#include "../../../third_party/unordered_map/unordered_map.hpp"
#else
#include <unordered_map>
#endif
#include "crf_utils.hh"

namespace macaon {
    const int kEpsilonTags = 0;
    const int kUnknownWordTags = 1;

    class Lexicon {
    protected:
        bool loaded;

        Symbols wordSymbols;
        Symbols* tagSymbols;

        std::vector<std::vector<int64> > tagsForWord;
        std::unordered_map<int64, int> tagsForWordEntry;


    public:
        Lexicon() : loaded(false), wordSymbols("words"), tagSymbols(NULL) {
            wordSymbols.AddSymbol("<eps>", 0);
        }

        Lexicon(const std::string& filename, Symbols* _tagSymbols) : loaded(false), wordSymbols("words"), tagSymbols(_tagSymbols) {
            wordSymbols.AddSymbol("<eps>", 0);
            Load(filename);
        }

        virtual ~Lexicon() {
        }

        bool NumLabels() const {
            return tagSymbols->NumSymbols() - 1; // account for epsilon
        }

        bool Load(const std::string &filename) {
            tagsForWord.push_back(std::vector<int64>()); // keep space for epsilon tags
            tagsForWord.push_back(std::vector<int64>()); // keep space for unk word tags
            loaded = false;
            std::unordered_map<std::string, int> known;
            std::ifstream input(filename.c_str());
            if(!input.is_open()) {
                std::cerr << "ERROR: could not open " << filename << " in Lexicon::Load()" << std::endl;
                return false;
            }
            while(!input.eof()) {
                std::string line;
                std::getline(input, line);
                if(input.eof()) break;
                std::string word;
                std::string::size_type end_of_word = line.find('\t');
                if(end_of_word == std::string::npos) {
                    return false;
                }
                word = line.substr(0, end_of_word);
                int64 wordId = wordSymbols.AddSymbol(word);
                std::string signature = line.substr(end_of_word + 1);
                std::unordered_map<std::string, int>::const_iterator found = known.find(signature);
                if(found == known.end()) {
                    int id = tagsForWord.size();
                    known[signature] = id;
                    tagsForWordEntry[wordId] = id;
                    std::vector<std::string> tokens;
                    Tokenize(signature, tokens, "\t");
                    std::vector<int64> tagset;
                    for(std::vector<std::string>::const_iterator i = tokens.begin(); i != tokens.end(); i++) {
                        int64 tagId = tagSymbols->Find(*i);
                        if(tagId != -1) tagset.push_back(tagId);
                    }
                    tagsForWord.push_back(tagset);
                } else {
                    tagsForWordEntry[wordId] = found->second;
                }
            }
            tagsForWord[kEpsilonTags].push_back(0); // epsilon
            for(SymbolsIterator siter(*tagSymbols); !siter.Done(); siter.Next()) { // unknown word
                if(siter.Value() != 0) tagsForWord[kUnknownWordTags].push_back(siter.Value());
            }
            loaded = true;
            return loaded;
        }

        virtual bool GetTagsForWord(const std::string& word, std::vector<int64>& output) const {
            return GetTagsForWord(wordSymbols.Find(word), output);
        }

        virtual bool GetTagsForWord(int64 word, std::vector<int64>& output) const {
            if(!IsLoaded()) {
                std::cerr << "ERROR: Lexicon::GetTagsForWord(" << wordSymbols.Find(word) << ") called on empty lexicon" << std::endl;
                return false;
            }
            if(word == -1) {
                output = tagsForWord[kUnknownWordTags];
                return true;
            }
            if(word == 0) {
                output = tagsForWord[kEpsilonTags];
                return true;
            }
            std::unordered_map<int64, int>::const_iterator found = tagsForWordEntry.find(word);
            if(found == tagsForWordEntry.end()) {
                output = tagsForWord[kUnknownWordTags];
            } else {
                if(tagsForWord[found->second].size() == 0) {
                    std::cerr << "WARNING: inconsistancy between word/tag lexicon and model, word no " << word << " has no tags => treat as unknown word\n";
                    output = tagsForWord[kUnknownWordTags];
                }
                output = tagsForWord[found->second];
            }
            return true;
        }

        bool IsLoaded() const {
            return loaded;
        }

    };
}
