#ifndef __CONFIG2FEAT_VEC__
#define __CONFIG2FEAT_VEC__

#include"config.h"
#include"feat_vec.h"
#include"dico.h"
#include"dico_vec.h"
#include"word_emb.h"
#include"feat_model.h"

#define feature_types_nb 25

#define LOOKUP_MODE 1
#define ADD_MODE 2

feat_vec *config2feat_vec_fann(feat_model *fm, config *c, feat_vec *fv, int mode);
feat_vec *config2feat_vec_cff(feat_model *fm, config *c, dico *dico_features, feat_vec *fv, int mode);

/* int         feat_model_get_feat_value_fann(feat_model *fm, config *c, int feat_nb); */
/* int         feat_model_get_feat_value_cff(feat_model *fm, config *c, dico *dico_features, int feat_nb, int mode); */


/*
feat_vec *config2feat_vec(config *c, dico *dico_features, feat_vec *fv, int mode);
feat_vec *config2feat_vec_add_features(config *c, dico *dico_features, feat_vec *fv);
dico_vec *build_dico_vec_dnn(context *ctx);
feat_vec *build_size_vec_dnn(context *ctx, int we_dim);
feat_vec *config2feat_vec_dnn(config *c, dico_vec *dv, feat_vec *fv, int mode, word_emb *we);
*/
#endif
