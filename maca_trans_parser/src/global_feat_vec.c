#include"global_feat_vec.h"
#include"util.h"

void global_feat_vec_print(global_feat_vec *gfv)
{
  int i, j;
  
  for(i=0; i < gfv->nbelem; i++){
    fprintf(stdout, "%d : ", i); 
    
    for(j=0; j < gfv->array[i]->fv->nb; j++)
      fprintf(stdout, "%d ", gfv->array[i]->fv->t[j]);
    fprintf(stdout, "\n"); 
  }
}

global_feat_vec *global_feat_vec_new(void)
{
  global_feat_vec *gfv = (global_feat_vec *)memalloc(sizeof(global_feat_vec));
  gfv->nbelem = 0;
  gfv->array = NULL;
  return gfv;
}

void global_feat_vec_add(global_feat_vec *gfv, int pred_mvt, feat_vec *fv)
{
  global_feat_vec_elt *elt =  (global_feat_vec_elt *) memalloc(sizeof(global_feat_vec_elt));
  elt->pred_mvt = pred_mvt;
  /* elt->oracle_mvt = oracle_mvt; */
  elt->fv = fv;

  /* fprintf(stdout, "adding element %d to gfv, nb features = %d\n", gfv->nbelem, fv->nb);  */
  
  gfv->nbelem++;
  gfv->array = (global_feat_vec_elt **) realloc(gfv->array, gfv->nbelem * sizeof(global_feat_vec_elt *));
  gfv->array[gfv->nbelem-1] = elt;
}

global_feat_vec *global_feat_vec_copy(global_feat_vec *gfv)
{
  global_feat_vec *copy = global_feat_vec_new();
  int i;
  for(i=0; i < gfv->nbelem; i++){
    global_feat_vec_add(copy, gfv->array[i]->pred_mvt, feat_vec_copy(gfv->array[i]->fv));    
  }
  return copy;
}

void global_feat_vec_elt_free(global_feat_vec_elt *gfve)
{
  if(gfve->fv)
    feat_vec_free(gfve->fv);
  
}

void global_feat_vec_free(global_feat_vec *gfv)
{
  int i;
  if(gfv){
    for(i=0; i < gfv->nbelem; i++)
      global_feat_vec_elt_free(gfv->array[i]);
    free(gfv->array);
  }
  
}
