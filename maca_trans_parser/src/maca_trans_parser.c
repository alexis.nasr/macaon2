#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<getopt.h>
#include"context.h"
#include"movement_parser.h"
#include"oracle_parser_arc_eager.h"
#include"feat_fct.h"
#include"feature_table.h"
#include"dico.h"
#include"simple_decoder_parser_arc_eager.h"
/*#include"dnn_decoder.h"*/
#include"config2feat_vec.h"

void maca_trans_parser_help_message(context *ctx)
{
  context_general_help_message(ctx);
  /* context_beam_help_message(ctx); */
  /* context_conll_help_message(ctx); */
  context_debug_help_message(ctx);
  fprintf(stderr, "INPUT\n");
  context_input_help_message(ctx);
  context_mcd_help_message(ctx);
  context_model_help_message(ctx);
  context_vocabs_help_message(ctx);
  context_features_model_help_message(ctx);
  context_root_label_help_message(ctx);
}

void maca_trans_parser_check_options(context *ctx){
  if(ctx->help
     /*!ctx->conll_filename*/
     /*     || !ctx->perc_model_filename
     || !ctx->mcd_filename
     || !ctx->vocabs_filename
     || !ctx->features_model_filename*/
     ){
    maca_trans_parser_help_message(ctx);
    exit(1);
  }
}

void set_linguistic_resources_filenames_parser(context *ctx)
{
  char absolute_filename[500];

  if(!ctx->perc_model_filename){
    strcpy(absolute_filename, ctx->maca_data_path);
    strcat(absolute_filename, DEFAULT_MODEL_FILENAME);
    ctx->perc_model_filename = strdup(absolute_filename);
  }

  if(!ctx->vocabs_filename){
    strcpy(absolute_filename, ctx->maca_data_path);
    strcat(absolute_filename, DEFAULT_VOCABS_FILENAME);
    ctx->vocabs_filename = strdup(absolute_filename);
  }

  /*  if(!ctx->mcd_filename){
    strcpy(absolute_filename, ctx->maca_data_path);
    strcat(absolute_filename, DEFAULT_MULTI_COL_DESC_FILENAME);
    ctx->mcd_filename = strdup(absolute_filename);
    }*/

  if(!ctx->features_model_filename){
    strcpy(absolute_filename, ctx->maca_data_path);
    strcat(absolute_filename, DEFAULT_FEATURES_MODEL_FILENAME);
    ctx->features_model_filename = strdup(absolute_filename);
  }

  if(ctx->verbose){
    fprintf(stderr, "perc_model_filename = %s\n", ctx->perc_model_filename);
    fprintf(stderr, "vocabs_filename = %s\n", ctx->vocabs_filename);
    fprintf(stderr, "mcd_filename = %s\n", ctx->mcd_filename);
    fprintf(stderr, "perc_features_model_filename = %s\n", ctx->features_model_filename);
  }
}

int main(int argc, char *argv[])
{
  context *ctx;

  ctx = context_read_options(argc, argv);
  maca_trans_parser_check_options(ctx);

  set_linguistic_resources_filenames_parser(ctx);
  ctx->features_model = feat_model_read(ctx->features_model_filename, feat_lib_build(), ctx->verbose);
  ctx->vocabs = dico_vec_read(ctx->vocabs_filename, ctx->hash_ratio);

  mcd_link_to_dico(ctx->mcd_struct, ctx->vocabs, ctx->verbose);

  ctx->dico_labels = dico_vec_get_dico(ctx->vocabs, (char *)"LABEL");

  if(ctx->dico_labels == NULL){
    fprintf(stderr, "cannot find label names\n");
    return 1;
  }

  ctx->mvt_nb = ctx->dico_labels->nbelem * 2 + 3;

  /* load models */
  
  ctx->d_perceptron_features = dico_vec_get_dico(ctx->vocabs, (char *)"d_perceptron_features");
  
  simple_decoder_parser_arc_eager(ctx);
  
  context_free(ctx);
  return 0;
}

