#ifndef __CONFIG__
#define __CONFIG__

#include<stdio.h>
#include<stdlib.h>

#include"stack.h"
#include"mcd.h"
#include"word_buffer.h"
#include"mvt_stack.h"
#include"feature_table.h"

#define config_get_stack(c) (c)->st
#define config_get_buffer(c) (c)->bf
#define config_get_history(c) (c)->history

typedef struct {
  stack *st;            /* the stack */
  word_buffer *bf;      /* the buffer */
  mvt_stack *history;   /* movement sequence that led to this configuration */
  int mvt_chosen;
  vcode *vcode_array;
} config;

config *config_new(FILE *f, mcd *mcd_struct, int lookahead);
void config_print(FILE *buffer, config *c);
int config_is_terminal(config *c);
void config_free(config *c);
void config_push_mvt(config *c, int type, word *gov, word *dep);
mvt *config_pop_mvt(config *c);

#endif
