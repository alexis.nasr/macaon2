#ifndef __BEAM__
#define __BEAM__
#include "dico.h"
#include "sentence.h"
#include "config.h"
#include "feature_table.h"
#include "context.h"

typedef struct {
  int size;
  int nbelem;
  config **t;
} beam;

typedef struct {
  int config;
  int movement;
  float score;
} triplet_cms;

beam *beam_new(int size);
void beam_free(beam *b);
int beam_add(beam *b, config * c);
void beam_empty(beam *b);
void beam_decoder(FILE *f, mcd *mcd_struct, dico *dico_features, dico *dico_labels, feature_table *ft, feat_model  *fm, int verbose, int root_label, int beam_width, int mvt_nb);
void fill_next_beam(beam *current_beam, beam *next_beam, beam *final_beam, feature_table *ft, feat_model *fm, dico *dico_features, int beam_width, int mode);
void beam_print(FILE *f, beam *b);
config *beam_argmax(beam *b);

#endif
