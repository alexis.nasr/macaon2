#include "word_emb.h"
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char *argv[])
{
  float *table;
  word_emb *we = word_emb_load(argv[1]);
  int i;
  /* word_emb_print_to_file(we, NULL); */

  table = word_emb_get_vector(we, "coopératifs");
  printf("coopératifs ");
  for(i=0; i < we->dim; i++)
    printf(" %f", table[i]);
  printf("\n");

  word_emb_print(stdout, we, word_emb_get_code(we, "coopératifs"));
  return 0;

}
