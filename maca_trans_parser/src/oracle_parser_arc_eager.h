#ifndef __ORACLE_ARC_EAGER__
#define __ORACLE_ARC_EAGER__


#include"config.h"
#include"word_buffer.h"


int oracle_parser_arc_eager(config *c, word_buffer *ref, int root_label);

#endif
