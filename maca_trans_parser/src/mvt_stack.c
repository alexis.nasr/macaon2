#include<stdio.h>
#include<stdlib.h>
#include"mvt_stack.h"
#include"util.h"

void mvt_stack_print(FILE *f, mvt_stack *ms)
{
int i;
 for(i=0; i < mvt_stack_nbelem(ms); i++)
   mvt_print(f, mvt_stack_elt_n(ms, i));
}

void mvt_stack_free(mvt_stack *s)
{
    for(int i=0; i < s->size; ++i) {
	  mvt_free(s->array[i]);
  }
  free(s->array);
  free(s);
}

mvt_stack *mvt_stack_new(void)
{
  mvt_stack *s = (mvt_stack *)memalloc(sizeof(mvt_stack));
  s->size = 0;
  s->array = NULL;
  s->top = 0;
  return s;
}

void mvt_stack_push(mvt_stack *s, mvt *m)
{
  if(s->top == s->size){
    s->size++;
    s->array = (mvt **)realloc(s->array, s->size * sizeof(mvt *));
  }
  s->array[s->top] = m;
  s->top++;
}


