#include<stdio.h>
#include<stdlib.h>
#include"stack.h"
#include"util.h"


/*int stack_height(stack *s)
{
  return(s->top);
  }*/


int stack_is_empty(stack *s)
{
  return(s->top == 0);
}


void stack_free(stack *s)
{
  free(s->array);
  free(s);
}


stack *stack_new(void)
{
  stack *s = (stack *)memalloc(sizeof(stack));
  s->size = 0;
  s->array = NULL;
  s->top = 0;
  return s;
}
/*
stack *stack_static_new(int size)
{
  stack *s = (stack *)memalloc(sizeof(stack));
  s->size = size;
  s->array = (word **)memalloc(size * sizeof(word*)));
  s->top = 0;
  return s;
}
*/

stack *stack_copy(stack *s)
{
  int i;
  stack *c = NULL;
  if(s == NULL) return NULL;
  c = (stack *)memalloc(sizeof(stack));
  c->size = s->size;
  c->top = s->top;
  c->array = (word **)memalloc(c->size * sizeof(word *));
  for(i=0; i < c->size; i++)
    c->array[i] = s->array[i];
  return c;
}


word *stack_pop(stack *s)
{
  if(s->top == 0){
    fprintf(stderr, "cannot pop empty stack\n");
    return NULL;
  }
  else
    return s->array[--s->top];
}

void stack_push(stack *s, word *w)
{
  if(s->top == s->size){
    s->size++;
    s->array = (word **)realloc(s->array, s->size * sizeof(word *));
  }
  s->array[s->top] = w;
  s->top++;
}

word *stack_top(stack *s)
{
  if(s->top == 0){
    /* fprintf(stderr, "empty stack\n"); */
    return NULL;
  }
  else
    return s->array[s->top - 1];
}

void stack_print(FILE *buffer, stack *s)
{
  int i;  
  if(s){
    fprintf(buffer, "[");
    for(i=0; i < stack_height(s); i++)
      fprintf(buffer, " %d", word_get_index(s->array[i]));
    fprintf(buffer, "]");
  }
}

/* keep k upper elements in the stack */

void stack_trim_to_size(stack *s, int k) 
{
  int i, delta;
  
  if(stack_nbelem(s) > k){
    delta = stack_nbelem(s) - k;
    for(i = 0; i < s->top; i++){
      s->array[i] = s->array[i + delta]; 
    }
    s->top = k;
  }
}
