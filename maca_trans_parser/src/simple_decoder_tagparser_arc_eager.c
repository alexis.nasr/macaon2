#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<getopt.h>
#include"context.h"
#include"movement_tagparser_arc_eager.h"
#include"feat_fct.h"
#include"config2feat_vec.h"
#include"feature_table.h"
#include"dico.h"

void add_signature_to_words_in_word_buffer(word_buffer *bf, form2pos *f2p)
{
  int i;
  word *w;

  for(i = word_buffer_get_nbelem(bf) - 1; i >=0  ; i--){
    w = word_buffer_get_word_n(bf, i);
    if(word_get_signature(w) != -1) break;
    w->signature = form2pos_get_signature(f2p, w->form);
  }
}

void print_word_buffer_tagparser(config *c, dico *dico_labels, dico *dico_pos)
{
  int i;
  word *dep;
  char *label;
  char *pos;
  
  for(i=0; i < config_get_buffer(c)->nbelem; i++){
    dep = word_buffer_get_word_n(config_get_buffer(c), i);
    printf("%s\t", word_get_input(dep));
    pos = (word_get_pos(dep) == -1)? NULL : dico_int2string(dico_pos, word_get_pos(dep));
    if(pos != NULL)
      printf("%s\t_\t", pos) ;
    else
      printf("_\t_\t");

    printf("%d\t", word_get_gov(dep));
    label = (word_get_label(dep) == -1)? NULL : dico_int2string(dico_labels, word_get_label(dep));
    if(label != NULL)
      printf("%s\t", label) ;
    else
      printf("_\t");
    if(word_get_sent_seg(dep) == 1)
      printf("1\n") ;
    else
      printf("0\n");
  }
}

void simple_decoder_tagparser_arc_eager(context *ctx)
{
  FILE *f = (ctx->input_filename)? myfopen(ctx->input_filename, "r") : stdin;
  feature_table *ft = feature_table_load(ctx->perc_model_filename, ctx->verbose);
  int root_label;
  int mvt_code;
  int mvt_type;
  int mvt_label;
  float max;
  feat_vec *fv = feat_vec_new(feature_types_nb);
  config *c = NULL;
  int result;
  /* float entropy; */
  /* float delta; */
  int argmax1, argmax2;
  float max1, max2;
  int index;
  
  root_label = dico_string2int(ctx->dico_labels, ctx->root_label);
  if(root_label == -1) root_label = 0;
  
  c = config_new(f, ctx->mcd_struct, 5);
  while(!config_is_terminal(c)){
    if(ctx->f2p)
      add_signature_to_words_in_word_buffer(c->bf, ctx->f2p);

    config2feat_vec_cff(ctx->features_model, c, ctx->d_perceptron_features, fv, LOOKUP_MODE);
    mvt_code = feature_table_argmax(fv, ft, &max);
    mvt_type = movement_tagparser_type(mvt_code);
    mvt_label = movement_tagparser_label(mvt_code);
    
    if(ctx->trace_mode){
      index = word_get_index(word_buffer_b0(config_get_buffer(c)));
      fprintf(stdout, "%d\t", index);

      stack_print(stdout, c->st);
      fprintf(stdout, "\t");
      
      movement_tagparser_print(stdout, mvt_code, ctx->dico_labels, ctx->dico_postags);        
      fprintf(stdout, "\t");
      feature_table_argmax_1_2(fv, ft, &argmax1, &max1, &argmax2, &max2);
      printf("%f\n", max1 - max2);

    }
    
    if(ctx->debug_mode){
      fprintf(stdout, "***********************************\n");
      config_print(stdout, c);      

      vcode *vcode_array = feature_table_get_vcode_array(fv, ft);

      for(int i=0; i < 5; i++){
	printf("%d\t", i);
	movement_tagparser_print(stdout, vcode_array[i].class_code, ctx->dico_labels, ctx->dico_postags);
	printf("\t%.4f\n", vcode_array[i].score);
      }
      free(vcode_array);


#if 0

      entropy = feature_table_entropy(fv, ft);
      /* delta = feature_table_diff_scores(fv, ft); */
      feature_table_argmax_1_2(fv, ft, &argmax1, &max1, &argmax2, &max2);
      movement_tagparser_print(stdout, argmax1, ctx->dico_labels, ctx->dico_postags);         
      printf(":\t%f\n", max1);
      movement_tagparser_print(stdout, argmax2, ctx->dico_labels, ctx->dico_postags);         
      printf(":\t%f\n", max2);
      printf("delta = %f\n", max1 - max2);

      /* delta = feature_table_first_second(fv, ft); */
       /* printf("entropy = %f delta = %f\n", entropy, delta);  */
       printf("entropy = %f\n",entropy); 
      
      /* movement_tagparser_print(stdout, mvt_code, ctx->dico_labels);          */
#endif
    }
    result = 0;
    switch(mvt_type){
    case MVT_TAGPARSER_POSTAG :
      result = movement_tagparser_add_pos(c, mvt_label);
      /*      if(result){
	int code_pos = word_get_pos(word_buffer_b0(config_get_buffer(c)));
	int code_form = word_get_form(word_buffer_b0(config_get_buffer(c)));
	printf("code pos = %d code form = %d\n", code_pos, code_form);
	}*/
      break;
    case MVT_TAGPARSER_LEFT :
      result = movement_tagparser_left_arc(c, mvt_label);
      break;
    case MVT_TAGPARSER_RIGHT:
      result = movement_tagparser_right_arc(c, mvt_label);
      break;
    case MVT_TAGPARSER_REDUCE:
      result = movement_tagparser_reduce(c);
      break;
    case MVT_TAGPARSER_ROOT:
      result = movement_tagparser_root(c, root_label);
      break;
    case MVT_TAGPARSER_EOS:
      result = movement_tagparser_eos(c);
      break;
    case MVT_TAGPARSER_SHIFT:
      result = movement_tagparser_shift(c);
    }

    if(result == 0){
      if(ctx->debug_mode){
	fprintf(stdout, "WARNING : movement cannot be executed doing a SHIFT instead !\n");
      }
      result = movement_tagparser_shift(c);
	if(result == 0){ /* SHIFT failed no more words to read, let's get out of here ! */
	  if(ctx->debug_mode) fprintf(stdout, "WARNING : cannot exectue a SHIFT emptying stack !\n");
	  while(!stack_is_empty(config_get_stack(c)))
	    movement_tagparser_root(c, root_label);
	}
    }
  }
  
  if(!ctx->trace_mode)
    print_word_buffer_tagparser(c, ctx->dico_labels, ctx->dico_postags);
  
  config_free(c); 
  feat_vec_free(fv);
  feature_table_free(ft);
  if(ctx->input_filename)
    fclose(f);
}
