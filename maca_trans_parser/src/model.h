#ifndef __MODEL__
#define __MODEL__
#include"dico.h"
#include"feature_table.h"

typedef struct 
{
  feature_table *ft;
  dico *dico_features;
  dico *dico_classes;
} model;


#endif
