#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"feat_fct.h"
#include"feat_model.h"
#include"config2feat_vec.h"

int get_feat_value_fann(feat_model *fm, config *c, int feat_nb)
{
  feat_desc *fd = fm->array[feat_nb];
  /* fm must be exclusively  composed of simple features */
  return fd->array[0]->fct(c);
}

int get_feat_value_cff(feat_model *fm, config *c, dico *dico_features, int feat_nb, int mode)
{
  feat_desc *fd = fm->array[feat_nb];
  int i;
  int feat_val;

  /*  the name of the feature is built in fm->string and its value in the dictionnary (dico_features) is returned */
  fm->string[0] = '\0';
  for(i=0; i < fd->nbelem; i++){
    strcat(fm->string, fd->array[i]->name);
    strcat(fm->string, "==");
    feat_val = fd->array[i]->fct(c);
    catenate_int(fm->string, feat_val);
  }
 
  if(mode == LOOKUP_MODE){
    if(fm->string){
      //    printf("fmstring = %s\n", fm->string); 
    return dico_string2int(dico_features, fm->string);
    }
  } 
  return dico_add(dico_features, fm->string);
}



feat_vec *config2feat_vec_fann(feat_model *fm, config *c, feat_vec *fv, int mode)
{
  int i;
  feat_vec_empty(fv);
  for(i=0; i < fm->nbelem; i++)
    feat_vec_add(fv, get_feat_value_fann(fm, c, i));
  return fv;
}

feat_vec *config2feat_vec_cff(feat_model *fm, config *c, dico *dico_features, feat_vec *fv, int mode)
{
  int i;
  feat_vec_empty(fv);
  for(i=0; i < fm->nbelem; i++){
    feat_vec_add(fv, get_feat_value_cff(fm, c, dico_features, i, mode));
  }
  return fv;
}


