#include<stdio.h>
#include<stdlib.h>
#include"word_buffer.h"
#include"util.h"

int main(int argc, char *argv[])
{
  mcd *mcd_struct;
  FILE *mcf;
  word_buffer *wb;
  if(argc < 2){
    fprintf(stderr, "usage %s mcf mcd\n", argv[0]);
    exit(1);
  }
  mcd_struct = mcd_read(argv[2], 1);
  mcf = myfopen(argv[1], "r");

  wb = word_buffer_new(mcf, mcd_struct, 0);
  word_buffer_print(stdout, wb);
    printf("\n");
  while(word_buffer_move_right(wb)){
    word_buffer_print(stdout, wb);
    printf("\n");
  }

  printf("=================== CHANGE DIRECTION =====================\n");
  
  while(word_buffer_move_left(wb)){
    word_buffer_print(stdout, wb);
    printf("\n");
  }

  word_buffer_free(wb);
    
}
