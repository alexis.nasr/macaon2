#ifndef __MOVEMENT__
#define __MOVEMENT__

#include"config.h"
#include"feat_vec.h"


#if 0

#define MVT_SHIFT 0
#define MVT_CUT 1
#define MVT_LEFT 2
#define MVT_RIGHT 3
#define movement_left_code(label) (2 * (label) + 2)
#define movement_right_code(label) (2 * (label) + 3)

#endif



#define MVT_SHIFT 0
#define MVT_LEFT 1
#define MVT_RIGHT 2



/* odd movements are left movements */
#define movement_left_code(label) (2 * (label) + 1)
/* even movements are right movements */
#define movement_right_code(label) (2 * (label) + 2)



int movement_type(int mvt);
int movement_label(int mvt);

int movement_left_arc(config *c, int label, float score);
int movement_right_arc(config *c, int label, float score);
int movement_shift(config *c, int stream, float score);
int movement_reduce(config *c, float score);

config *movement_left_arc_dup(config *c, int label, float score, feat_vec *fv);
config *movement_right_arc_dup(config *c, int label, float score, feat_vec *fv);
config *movement_shift_dup(config *c, int stream, float score, feat_vec *fv);
void movement_print(FILE *f, int mvt_code, dico *dico_labels);
  /* int movement_cut(config *c); */


#endif
