#include<stdio.h>
#include<stdlib.h>
#include"mvt_stack.h"
#include"util.h"

int main(int argc, char *argv[])
{
  mvt_stack *ms = mvt_stack_new();
  word *w1 = NULL;
  word *w2 = NULL;
  int i;
  
  for(i=0; i < 10; i++){
    w1 = word_new((char *)"1");
    word_set_index(w1, i + 1);
    w2 = word_new((char *)"1");
    word_set_index(w2, i + 2);
    mvt_stack_push(ms, mvt_new(i, w1, w2));
  }
  mvt_stack_print(stdout, ms);

  printf("\n");

  while(!mvt_stack_is_empty(ms))
    mvt_print(stdout, mvt_stack_pop(ms));

  
}
