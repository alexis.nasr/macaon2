#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<getopt.h>
#include<ctype.h>
#include"movement_parser_arc_eager.h"
#include"oracle_parser_arc_eager.h"
#include"feat_fct.h"
#include"context.h"
#include"feat_vec.h"
#include"dico_vec.h"
#include"word_emb.h"
#include"config2feat_vec.h"
#include"feature_table.h"
#include"dico.h"
#include"mcd.h"


void maca_error_predictor_parser_mcf2cff_help_message(context *ctx)
{
   context_general_help_message(ctx);

  fprintf(stderr, "\t-d --debug                 : debug option\n");
  fprintf(stderr, "INPUT\n");
    fprintf(stderr, "\t-q --classes           : number of error classes \n");
  fprintf(stderr, "\t-i --input      <file> : input is in mcf format (default is new_dev.mcf)\n");
  fprintf(stderr, "\t-F --feat_model <file> : feature model file name\n");
  fprintf(stderr, "\t-f --fann       <file> : feature model error file name\n");
  fprintf(stderr, "\t-m --model      <file> : model filename\n");
  fprintf(stderr, "\t-C --mcd        <file> : model error filename\n");
  fprintf(stderr, "\t-V --vocabs     <file> : vocabularies file\n");
  fprintf(stderr, "\t-N --dnn_model  <file> : vocabularies error file\n");
  
  fprintf(stderr, "OUTPUT\n");
  fprintf(stderr, "\t-x --cff        <file> : CFF format file name (default is stdout)\n");
  
}

void maca_error_predictor_parser_mcf2cff_check_options(context *ctx)
{
  if(!ctx->input_filename
     || ctx->help
     ){
    maca_error_predictor_parser_mcf2cff_help_message(ctx);
    exit(1);
  }
}

int config_is_equal_parser(config *coracle, config *cdetect, int classes)
{
  int cont = 1;
  int classN = 0;

  for (int i=0 ; i<classes-1 ; i++) {
    if (cont && mvt_stack_i(coracle->history,i) && mvt_stack_i(cdetect->history,i)) {
      if (mvt_stack_i(coracle->history,i)->type == MVT_PARSER_EOS){
        return classN;
      }
      if (mvt_stack_i(coracle->history,i)->type != mvt_stack_i(cdetect->history,i)->type) {
        classN = i+1;
      }
    }
    else {
      cont = 0;
    }
  }

  return classN;
  
}


void oracle_movement(int *mvt_code_oracle, char *mvt_type_oracle, int *mvt_label_oracle, config *config_oracle, word_buffer *ref_oracle, int root_label_oracle, context *ctx, int *sentence_nb)
{
  if (!word_buffer_end(ref_oracle) && (*sentence_nb < ctx->sent_nb)) {
          
    *mvt_code_oracle = oracle_parser_arc_eager(config_oracle, ref_oracle, root_label_oracle);
    *mvt_type_oracle = movement_parser_type(*mvt_code_oracle);
    *mvt_label_oracle = movement_parser_label(*mvt_code_oracle);

    if(ctx->debug_mode){
      printf("Oracle : ");
      movement_parser_print(stdout, *mvt_code_oracle, ctx->dico_labels);
      printf("\n");
      config_print(stdout,config_oracle);
    }
    
    if(ctx->force) {
      movement_parser_print(stdout, *mvt_code_oracle, ctx->dico_labels);
      int mvt_code = *mvt_code_oracle;
      if(mvt_code == 0 || mvt_code == 1 || mvt_code == 2 || mvt_code == 3) {//SHIFT REDUCE ROOT OR EOS
        fprintf(stdout," _");
          }
      fprintf(stdout,"\n");
    }
    switch(*mvt_type_oracle){
    case MVT_PARSER_EOS :
      movement_parser_eos(config_oracle);
      (*sentence_nb)++;
      if((*sentence_nb % 100) == 0)
        fprintf(stderr, "\rsentence %d", *sentence_nb);
      break;
    case MVT_PARSER_LEFT :
      movement_parser_left_arc(config_oracle, *mvt_label_oracle);
      break;    
    case MVT_PARSER_RIGHT :
      movement_parser_right_arc(config_oracle, *mvt_label_oracle);
      word_buffer_move_right(ref_oracle);
      break;    
    case MVT_PARSER_REDUCE :
      movement_parser_reduce(config_oracle);
      break;   
    case MVT_PARSER_ROOT :
      movement_parser_root(config_oracle, root_label_oracle);
      break;
    case MVT_PARSER_SHIFT :
      movement_parser_shift(config_oracle);
      word_buffer_move_right(ref_oracle);
      break;
    }
  }
  else {
    //printf("Oracle finishes its job\n");
  }
}
void print_cff(context *ctx, FILE *output_file, config *config_oracle, config *config_decoder, int mvt_code_oracle, int mvt_code_decoder, feat_vec *fv_error)
{
  if(ctx->debug_mode) {
    /*fprintf(stdout,"Stack Oracle : \n");
    mvt_stack_print(stdout, config_oracle->history);
    fprintf(stdout,"Stack Detect : \n");
    mvt_stack_print(stdout, config_decoder->history);*/
  
    fprintf(stdout, "Class : %d\n", (config_is_equal_parser(config_oracle, config_decoder, ctx->nb_classes)));
  }
  if((!ctx->debug_mode || output_file!=stdout) && !ctx->force) {
    fprintf(output_file, "%d", ((config_is_equal_parser(config_oracle, config_decoder, ctx->nb_classes))));
    feat_vec_print(output_file, fv_error);
  }
}


void generate_training_file_error(FILE *output_file, context *ctx)
{
  // oracle
  config *config_oracle;
  int mvt_code_oracle;
  char mvt_type_oracle;
  int mvt_label_oracle;
  int sentence_nb = 0;
  int root_label_oracle = dico_string2int(ctx->dico_labels, (char *) ctx->root_label);
  word_buffer *ref_oracle = word_buffer_load_mcf(ctx->input_filename, ctx->mcd_struct);
  FILE *mcf_file_oracle = myfopen(ctx->input_filename, "r");

  // decoder
  config *config_decoder;
  FILE *mcf_file_decoder = myfopen(ctx->input_filename, "r");
  feature_table *ft = feature_table_load(ctx->perc_model_filename, ctx->verbose);
  int root_label_decoder;
  int mvt_code_decoder;
  int mvt_type_decoder;
  int mvt_label_decoder;
  float max;
  feat_vec *fv_decoder = feat_vec_new(feature_types_nb);
  int result;
  int argmax1, argmax2;
  float max1, max2;


  //error training
  feat_vec *fv_error = feat_vec_new(feature_types_nb);
  
  
  /* create an mcd that corresponds to ctx->mcd_struct, but without gov and label */
  /* the idea is to ignore syntax in the mcf file that will be read */
  /* it is ugly !!! */
  
  mcd *mcd_struct_hyp = mcd_copy(ctx->mcd_struct);
  mcd_remove_wf_column(mcd_struct_hyp, MCD_WF_GOV);
  mcd_remove_wf_column(mcd_struct_hyp, MCD_WF_LABEL);
  mcd_remove_wf_column(mcd_struct_hyp, MCD_WF_SENT_SEG);


  config_oracle = config_new(mcf_file_oracle, mcd_struct_hyp, 5);

  root_label_decoder = dico_string2int(ctx->dico_labels, ctx->root_label);
  if(root_label_decoder == -1) root_label_decoder = 0;

  mcd *mcd_struct_decoder = mcd_copy(mcd_struct_hyp);
  mcd_remove_wf_column(mcd_struct_decoder, MCD_WF_FEATS);
  
  config_decoder = config_new(mcf_file_decoder, mcd_struct_decoder, 5);

  if (ctx->force) {
    while((!word_buffer_end(ref_oracle) && (sentence_nb < ctx->sent_nb))) {
      //oracle    
      oracle_movement(&mvt_code_oracle, &mvt_type_oracle, &mvt_label_oracle, config_oracle, ref_oracle, root_label_oracle, ctx, &sentence_nb);
    }

  }
  
  while(!ctx->force && ((!word_buffer_end(ref_oracle) && (sentence_nb < ctx->sent_nb)) || !config_is_terminal(config_decoder))){

    //oracle   
    oracle_movement(&mvt_code_oracle, &mvt_type_oracle, &mvt_label_oracle, config_oracle, ref_oracle, root_label_oracle, ctx, &sentence_nb);

    // decoder
    config2feat_vec_cff(ctx->features_model, config_decoder, ctx->d_perceptron_features, fv_decoder, LOOKUP_MODE);
          
    mvt_code_decoder = feature_table_argmax(fv_decoder, ft, &max);
    mvt_type_decoder = movement_parser_type(mvt_code_decoder);
    mvt_label_decoder = movement_parser_label(mvt_code_decoder);

    
    /* forced EOS, oracle detect EOS */
          
    if(mvt_type_oracle == MVT_PARSER_EOS) {
      if(ctx->debug_mode) printf("EOS FORCED\n");
      mvt_type_decoder = MVT_PARSER_EOS;
      movement_parser_eos(config_decoder);
      while(movement_parser_reduce(config_decoder)) {
        //oracle_movement(&mvt_code_oracle, &mvt_type_oracle, &mvt_label_oracle, config_oracle, ref_oracle, root_label_oracle, ctx, &sentence_nb);
        //print_cff(ctx, output_file, config_oracle, config_decoder, mvt_code_oracle, mvt_code_decoder, fv_error);
      }

      // WAIT ??
      movement_parser_reduce(config_oracle);
      
      while(movement_parser_root(config_decoder, root_label_decoder)) {
        //oracle_movement(&mvt_code_oracle, &mvt_type_oracle, &mvt_label_oracle, config_oracle, ref_oracle, root_label_oracle, ctx, &sentence_nb);
        //print_cff(ctx, output_file, config_oracle, config_decoder, mvt_code_oracle, mvt_code_decoder, fv_error);
      }
      
      movement_parser_root(config_oracle, root_label_oracle);
      if(ctx->debug_mode) printf("b0? = %d / %d\n", (word_buffer_b0(config_oracle->bf))->index, (word_buffer_b0(config_decoder->bf))->index);

      while((word_buffer_b0(config_decoder->bf))->index < (word_buffer_b0(config_oracle->bf))->index)
        word_buffer_move_right(config_decoder->bf);
      while((word_buffer_b0(config_decoder->bf))->index > (word_buffer_b0(config_oracle->bf))->index)
        word_buffer_move_left(config_decoder->bf);
      
      if(ctx->debug_mode) printf("b0? = %d / %d\n", (word_buffer_b0(config_oracle->bf))->index, (word_buffer_b0(config_decoder->bf))->index);
      continue;
    }

    /* We wait the oracle to do an EOS move*/
    
    if(mvt_type_decoder == MVT_PARSER_EOS && mvt_type_oracle != MVT_PARSER_EOS) {
      feature_table_argmax_1_2(fv_decoder, ft, &argmax1, &max1, &argmax2, &max2);
      mvt_code_decoder = argmax2;
      mvt_type_decoder = movement_parser_type(mvt_code_decoder);
      mvt_label_decoder = movement_parser_label(mvt_code_decoder);
    }

    if(ctx->debug_mode){
      vcode *vcode_array = feature_table_get_vcode_array(fv_decoder, ft);
      for(int i=0; i < 3; i++){
        printf("%d\t", i);
        movement_parser_print(stdout, vcode_array[i].class_code, ctx->dico_labels);
        printf("\t%.4f\n", vcode_array[i].score);
      }
      free(vcode_array);
    }
      

    if(ctx->debug_mode){    
      printf("Decoder : ");
      movement_parser_print(stdout, mvt_code_decoder, ctx->dico_labels);
      printf("\n");
      config_print(stdout,config_decoder);
      if (mvt_code_oracle!=mvt_code_decoder)
        fprintf(stdout, "**************** DIFFERENT CHOICE ***********\n\n");
      else
        fprintf(stdout, "**************** EQUAL CHOICE ***********\n\n");
    }
      
    result = 0;
    switch(mvt_type_decoder){
    case MVT_PARSER_LEFT :
      result = movement_parser_left_arc(config_decoder, mvt_label_decoder);
      break;
    case MVT_PARSER_RIGHT:
      result = movement_parser_right_arc(config_decoder, mvt_label_decoder);
      break;
    case MVT_PARSER_REDUCE:
      result = movement_parser_reduce(config_decoder);
      break;
    case MVT_PARSER_ROOT:
      result = movement_parser_root(config_decoder, root_label_decoder);
      break;
    case MVT_PARSER_EOS:
      result = movement_parser_eos(config_decoder);
      break;
    case MVT_PARSER_SHIFT:
      result = movement_parser_shift(config_decoder);
      break;
    }
            
    if(result == 0){
      if(ctx->debug_mode) fprintf(stdout, "WARNING : movement cannot be executed doing a SHIFT instead !\n");
      result = movement_parser_shift(config_decoder);
      if(result == 0){ /* SHIFT failed no more words to read, let's get out of here ! */
        if(ctx->debug_mode) fprintf(stdout, "WARNING : cannot exectue a SHIFT emptying stack !\n");
        if (!stack_is_empty(config_get_stack(config_decoder))) {
          movement_parser_root(config_decoder, root_label_decoder);
        }
        while(!stack_is_empty(config_get_stack(config_decoder))) {
          oracle_movement(&mvt_code_oracle, &mvt_type_oracle, &mvt_label_oracle, config_oracle, ref_oracle, root_label_oracle, ctx, &sentence_nb);
          movement_parser_root(config_decoder, root_label_decoder);
        }
      }
      else {}
    }
    else {}
    
    //error training
    config2feat_vec_cff(ctx->features_model_error, config_decoder, ctx->d_perceptron_features_error, fv_error, TRAIN_MODE);
    if(!ctx->force)     
      print_cff(ctx, output_file, config_oracle, config_decoder, mvt_code_oracle, mvt_code_decoder, fv_error); 
  }
  
  if(!ctx->force)     
    fprintf(stdout,"\n");
  
  config_free(config_oracle);
  config_free(config_decoder); 
  feat_vec_free(fv_decoder);
  feat_vec_free(fv_error);
  feature_table_free(ft);
  
  fclose(mcf_file_oracle);
  fclose(mcf_file_decoder);
  
}

void error_parser_set_linguistic_resources_filename(context *ctx)
{
  char absolute_filename[500];
  
  if(!ctx->perc_model_filename){
    strcpy(absolute_filename, ctx->maca_data_path);
    strcat(absolute_filename, DEFAULT_MODEL_FILENAME);
    ctx->perc_model_filename = strdup(absolute_filename);
  }

  if(!ctx->vocabs_filename){
    strcpy(absolute_filename, ctx->maca_data_path);
    strcat(absolute_filename, DEFAULT_VOCABS_FILENAME);
    ctx->vocabs_filename = strdup(absolute_filename);
  }

  if(!ctx->input_filename){
    strcpy(absolute_filename, ctx->maca_data_path);
    strcat(absolute_filename, DEFAULT_PATH_RELAT);
    strcat(absolute_filename, DEFAULT_MCF_DEV);
    ctx->input_filename = strdup(absolute_filename);
  }
  
  if(!ctx->features_model_filename){
    strcpy(absolute_filename, ctx->maca_data_path);
    strcat(absolute_filename, DEFAULT_FEATURES_MODEL_FILENAME);
    ctx->features_model_filename = strdup(absolute_filename);
  }

  if(ctx->verbose){
    fprintf(stderr, "perc_model_filename = %s\n", ctx->perc_model_filename);
    fprintf(stderr, "vocabs_filename = %s\n", ctx->vocabs_filename);
    fprintf(stderr, "mcd_filename = %s\n", ctx->mcd_filename);
    fprintf(stderr, "perc_features_model_filename = %s\n", ctx->features_model_filename);
    fprintf(stderr, "f2p_filename = %s\n", ctx->f2p_filename);
    fprintf(stderr, "input_filename = %s\n", ctx->input_filename);
  }
}

int main(int argc, char *argv[])
{
  context *ctx;
  FILE *output_file;
  
  ctx = context_read_options(argc, argv);
  error_parser_set_linguistic_resources_filename(ctx);

  maca_error_predictor_parser_mcf2cff_check_options(ctx);

  ctx->mcd_struct = mcd_build_wpmlgfs();
  
  //error
  ctx->features_model_error = feat_model_read(ctx->fann_filename, feat_lib_build(), ctx->verbose);
  ctx->d_perceptron_features_error = dico_new((char *)"d_perceptron_features_error", 10000000);

  //oracle + decoder
  ctx->features_model = feat_model_read(ctx->features_model_filename, feat_lib_build(), ctx->verbose);
  ctx->vocabs = dico_vec_read(ctx->vocabs_filename, ctx->hash_ratio); 
  mcd_link_to_dico(ctx->mcd_struct, ctx->vocabs, ctx->verbose);
  
  ctx->dico_labels = dico_vec_get_dico(ctx->vocabs, (char *)"LABEL");  
  if(ctx->dico_labels == NULL){
    fprintf(stderr, "cannot find label names\n");
    return 1;
  }
  
  ctx->mvt_nb = ctx->dico_labels->nbelem * 2 + 3;
  ctx->d_perceptron_features = dico_vec_get_dico(ctx->vocabs, (char *)"d_perceptron_features"); 
  
  /* open output file */
  output_file = (ctx->cff_filename) ? myfopen_no_exit(ctx->cff_filename, "w") : stdout;
  
  generate_training_file_error(output_file, ctx);
  
  dico_vec_add(ctx->vocabs, ctx->d_perceptron_features_error);
  dico_vec_print(ctx->dnn_model_filename, ctx->vocabs);
  
  if(ctx->cff_filename)
    fclose(output_file);
  
  context_free(ctx);
  
  return 0;
}

