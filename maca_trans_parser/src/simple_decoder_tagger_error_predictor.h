#ifndef __SIMPLE_DECODER_TAGGER_ERROR_PREDICTOR__
#define __SIMPLE_DECODER_TAGGER_ERROR_PREDICTOR__
void add_signature_to_words_in_word_buffer(word_buffer *bf, form2pos *f2p);
void simple_decoder_tagger_error_predictor(context *ctx, char *perc_error_filename);

#endif
