#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"util.h"
#include"mcd.h"
#include"config.h"
#include"word_buffer.h"
#include"movements.h"

#define LONGUEUR_LIGNE 1000

#define MODE_TAGGER 0
#define MODE_PARSER 1
#define MODE_TAGPARSER 2

void help_message(void)
{
  fprintf(stdout, "help\t print this message\n");
  fprintf(stdout, "verbose\t toggle verbose mode\n");
  fprintf(stdout, "quit\t quit interpreter\n");
  fprintf(stdout, "load_mcd\t load mcd file\n");
  fprintf(stdout, "load_mcf\t load mcf file\n");
  fprintf(stdout, "open_mcf\t open mcf file\n");
  fprintf(stdout, "config_new\t \n");
  fprintf(stdout, "config_print\t print configuration\n");
  fprintf(stdout, "shift\t  perform a shift movement\n");
  fprintf(stdout, "shift_undo\t \n");
  fprintf(stdout, "parser\t switch to parser mode\n");
  fprintf(stdout, "tagger\t switch to tagger mode\n");
  fprintf(stdout, "tagparser\t switch to tagparser mode\n");

}

int main(int argc, char *argv[])
{
  char ligne[LONGUEUR_LIGNE];
  char commande[LONGUEUR_LIGNE], argument[LONGUEUR_LIGNE];
  int n;
  mcd *mcd_struct = NULL;
  char *mcd_filename = NULL;
  char *mcf_filename = NULL;
  FILE *mcf_file = NULL;
  int verbose = 0;
  //word_buffer *wb = NULL;
  config *c = NULL;
  int mode = MODE_PARSER;
  
  while(1){
    printf("> ");

    if(fgets(ligne, LONGUEUR_LIGNE, stdin) == NULL) {
      printf("au revoir !\n");
      exit(1);
    }
    commande[0] = argument[0] = '\0';
    n = sscanf(ligne, "%s %s\n", commande, argument);
     /* printf("ligne = %s n = %d commande = %s argument = %s\n", ligne, n, commande, argument);  */

    if(n == -1) continue;
    if(!strcmp(commande, "quit")){ 
      printf("au revoir !\n");
      exit(1);
    }

    if(!strcmp(commande, "verbose")){ 
      verbose = (verbose == 0) ? 1 : 0;
      printf("verbose = %d\n", verbose);
      continue;
    }

    if(!strcmp(commande, "help")){
      help_message();
      continue;
    }

    /* set mode */
    
    if(!strcmp(commande, "parser")){
      mode = MODE_PARSER;
      if(verbose)
	fprintf(stdout, "mode = parser\n");
      continue;
    }
    
    if(!strcmp(commande, "tagger")){
      mode = MODE_TAGGER;
      if(verbose)
	fprintf(stdout, "mode = tagger\n");
      continue;
    }
    if(!strcmp(commande, "tagparser")){
      mode = MODE_TAGPARSER;
      if(verbose)
	fprintf(stdout, "mode = tagparser\n");
      continue;
    }

    if(!strcmp(commande, "mode")){
      if(mode == MODE_PARSER){fprintf(stdout, "parser\n"); continue;}
      if(mode == MODE_TAGGER){fprintf(stdout, "tagger\n"); continue;}
      if(mode == MODE_TAGPARSER){fprintf(stdout, "tagparser\n"); continue;}
    }

    if(!strcmp(commande, "load_mcd")){
      mcd_filename = strdup(argument);
      mcd_struct = mcd_read(mcd_filename, verbose);
      continue;
    }

    if(!strcmp(commande, "load_mcf")){
      mcf_filename = strdup(argument);
      word_buffer_load_mcf(mcf_filename, mcd_struct);
      continue;
    }

    if(!strcmp(commande, "open_mcf")){
      mcf_filename = strdup(argument);
      mcf_file = myfopen(mcf_filename, "r"); 
      continue;
    }

    if(!strcmp(commande, "config_new")){
      c = config_new(mcf_file, mcd_struct, 5);    
      continue;
    }

    if(!strcmp(commande, "config_print")){
      config_print(stdout, c);
      continue;
    }

    /* movements */

    if(!strcmp(commande, "shift")){
      movement_shift(c, 0);
      config_print(stdout, c);
      continue;
    }

    if(!strcmp(commande, "shift_undo")){
      movement_shift_undo(c);
      config_print(stdout, c);
      continue;
    }

    

    
  }
}
