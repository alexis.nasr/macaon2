#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"util.h"
#include"movement_tagger.h"

int movement_tagger(config *c, int postag)
{
  word_set_pos(word_buffer_b0(c->bf), postag); 
  word_buffer_move_right(c->bf);

  return 1;
}

int forward(config *c, int postag)
{
  word_set_pos(word_buffer_b0(c->bf), postag); 
  word_buffer_move_right(c->bf);
  c->mvt_chosen = 0;
  return 1;
}

int choice_n(config *c, int n)
{
  word_set_pos(word_buffer_bm1(c->bf), c->vcode_array[n].class_code); 
  c->mvt_chosen = n;
  return 1;
}

int backward(config *c)
{
  word_set_pos(word_buffer_b0(c->bf), -1); 
  word_buffer_move_left(c->bf);

  return 1;
}
