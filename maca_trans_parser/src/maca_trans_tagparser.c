#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<getopt.h>
#include"context.h"
#include"feat_fct.h"
#include"feature_table.h"
#include"dico.h"
#include"beam.h"
#include"simple_decoder_tagparser_arc_eager.h"
#include"config2feat_vec.h"

void decode_help_message(context *ctx)
{
  context_general_help_message(ctx);
  /* context_beam_help_message(ctx); */
  /* context_conll_help_message(ctx); */
  fprintf(stderr, "INPUT\n");
  context_input_help_message(ctx);
  context_mcd_help_message(ctx);
  context_model_help_message(ctx);
  context_vocabs_help_message(ctx);
  context_features_model_help_message(ctx);
  context_root_label_help_message(ctx);
}

void decode_check_options(context *ctx){
  if(ctx->help
     /*!ctx->conll_filename*/
     /*     || !ctx->perc_model_filename
     || !ctx->mcd_filename
     || !ctx->vocabs_filename
     || !ctx->features_model_filename*/
     ){
    decode_help_message(ctx);
    exit(1);
  }
}

void set_linguistic_resources_filenames_tagparser(context *ctx)
{
  char absolute_filename[500];

  if(!ctx->perc_model_filename){
    strcpy(absolute_filename, ctx->maca_data_path);
    strcat(absolute_filename, DEFAULT_MODEL_TAGPARSER_FILENAME);
    ctx->perc_model_filename = strdup(absolute_filename);
  }

  if(!ctx->vocabs_filename){
    strcpy(absolute_filename, ctx->maca_data_path);
    strcat(absolute_filename, DEFAULT_VOCABS_TAGPARSER_FILENAME);
    ctx->vocabs_filename = strdup(absolute_filename);
  }

  /*  if(!ctx->mcd_filename){
    strcpy(absolute_filename, ctx->maca_data_path);
    strcat(absolute_filename, DEFAULT_MULTI_COL_DESC_TAGPARSER_FILENAME);
    ctx->mcd_filename = strdup(absolute_filename);
    }*/

  if(!ctx->features_model_filename){
    strcpy(absolute_filename, ctx->maca_data_path);
    strcat(absolute_filename, DEFAULT_FEATURES_MODEL_TAGPARSER_FILENAME);
    ctx->features_model_filename = strdup(absolute_filename);
  }

  if(!ctx->f2p_filename){
    strcpy(absolute_filename, ctx->maca_data_path);
    strcat(absolute_filename, DEFAULT_F2P_FILENAME);
    ctx->f2p_filename = strdup(absolute_filename);
    ctx->f2p = form2pos_read(ctx->f2p_filename);
  }

  
  if(ctx->verbose){
    fprintf(stderr, "perc_model_filename = %s\n", ctx->perc_model_filename);
    fprintf(stderr, "vocabs_filename = %s\n", ctx->vocabs_filename);
    fprintf(stderr, "mcd_filename = %s\n", ctx->mcd_filename);
    fprintf(stderr, "perc_features_model_filename = %s\n", ctx->features_model_filename);
    fprintf(stderr, "f2p_filename = %s\n", ctx->f2p_filename);
  }
}

int main(int argc, char *argv[])
{
  context *ctx;

  ctx = context_read_options(argc, argv);
  decode_check_options(ctx);

  set_linguistic_resources_filenames_tagparser(ctx);
  ctx->features_model = feat_model_read(ctx->features_model_filename, feat_lib_build(), ctx->verbose);
  ctx->vocabs = dico_vec_read(ctx->vocabs_filename, ctx->hash_ratio);

  /* dico_vec_print(NULL, ctx->vocabs); */
  mcd_link_to_dico(ctx->mcd_struct, ctx->vocabs, ctx->verbose);

  ctx->dico_labels = dico_vec_get_dico(ctx->vocabs, (char *)"LABEL");
  ctx->dico_postags = dico_vec_get_dico(ctx->vocabs, (char *)"POS");

  if(ctx->dico_labels == NULL){
    fprintf(stderr, "cannot find label names\n");
    return 1;
  }

  ctx->mvt_nb = ctx->dico_labels->nbelem * 2 + 3;

  /* load models */

  ctx->d_perceptron_features = dico_vec_get_dico(ctx->vocabs, (char *)"d_perceptron_features");

  simple_decoder_tagparser_arc_eager(ctx);
  
  context_free(ctx);
  return 0;
}

