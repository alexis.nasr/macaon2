#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"feat_types.h"

int feat_type_string2int(char *type_name)
{
  if(!strcmp(type_name, "INDEX")) return FEAT_TYPE_INDEX;
  if(!strcmp(type_name, "FORM")) return FEAT_TYPE_FORM;
  if(!strcmp(type_name, "LEMMA")) return FEAT_TYPE_LEMMA;
  if(!strcmp(type_name, "CPOS")) return FEAT_TYPE_CPOS;
  if(!strcmp(type_name, "POS")) return FEAT_TYPE_POS;
  if(!strcmp(type_name, "FEATS")) return FEAT_TYPE_FEATS;
  if(!strcmp(type_name, "LABEL")) return FEAT_TYPE_LABEL;
  if(!strcmp(type_name, "STAG")) return FEAT_TYPE_STAG;
  if(!strcmp(type_name, "INT")) return FEAT_TYPE_INT;
  if(!strcmp(type_name, "GOV")) return FEAT_TYPE_GOV;
  if(!strcmp(type_name, "A")) return FEAT_TYPE_A;
  if(!strcmp(type_name, "B")) return FEAT_TYPE_B;
  if(!strcmp(type_name, "C")) return FEAT_TYPE_C;
  if(!strcmp(type_name, "D")) return FEAT_TYPE_D;
  if(!strcmp(type_name, "E")) return FEAT_TYPE_E;
  if(!strcmp(type_name, "F")) return FEAT_TYPE_F;
  if(!strcmp(type_name, "G")) return FEAT_TYPE_G;
  if(!strcmp(type_name, "H")) return FEAT_TYPE_H;
  if(!strcmp(type_name, "I")) return FEAT_TYPE_I;
  if(!strcmp(type_name, "J")) return FEAT_TYPE_J;
  if(!strcmp(type_name, "K")) return FEAT_TYPE_K;
  if(!strcmp(type_name, "L")) return FEAT_TYPE_L;
  if(!strcmp(type_name, "M")) return FEAT_TYPE_M;
  if(!strcmp(type_name, "N")) return FEAT_TYPE_N;
  if(!strcmp(type_name, "O")) return FEAT_TYPE_O;
  if(!strcmp(type_name, "P")) return FEAT_TYPE_P;
  if(!strcmp(type_name, "Q")) return FEAT_TYPE_Q;
  if(!strcmp(type_name, "R")) return FEAT_TYPE_R;
  if(!strcmp(type_name, "S")) return FEAT_TYPE_S;
  if(!strcmp(type_name, "T")) return FEAT_TYPE_T;
  if(!strcmp(type_name, "U")) return FEAT_TYPE_U;
  if(!strcmp(type_name, "V")) return FEAT_TYPE_V;
  if(!strcmp(type_name, "W")) return FEAT_TYPE_W;
  if(!strcmp(type_name, "X")) return FEAT_TYPE_X;
  if(!strcmp(type_name, "Y")) return FEAT_TYPE_Y;
  if(!strcmp(type_name, "Z")) return FEAT_TYPE_Z;
  return -1;
}
