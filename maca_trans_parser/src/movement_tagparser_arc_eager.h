#ifndef __MOVEMENT_TAGPARSER_ARC_EAGER__
#define __MOVEMENT_TAGPARSER_ARC_EAGER__

#include"config.h"
#include"dico.h"

#define MVT_TAGPARSER_SHIFT 0
#define MVT_TAGPARSER_REDUCE 1
#define MVT_TAGPARSER_ROOT 2
#define MVT_TAGPARSER_EOS 3
#define MVT_TAGPARSER_LEFT 4
#define MVT_TAGPARSER_RIGHT 5
#define MVT_TAGPARSER_POSTAG 6

#define movement_tagparser_postag_code(postag) (3 * (postag) + 4)

/* even movements are left movements (except 0, which is shift and 2 which is root) */
#define movement_tagparser_left_code(label) (3 * (label) + 5)

/* odd movements are right movements  (except 1, which is reduce and 3 which is end_of_sentence) */
#define movement_tagparser_right_code(label) (3 * (label) + 6)

int movement_tagparser_type(int mvt);
int movement_tagparser_label(int mvt);

int movement_tagparser_left_arc(config *c, int label);
int movement_tagparser_right_arc(config *c, int label);
int movement_tagparser_shift(config *c);
int movement_tagparser_reduce(config *c);
int movement_tagparser_root(config *c, int root_code);
int movement_tagparser_eos(config *c);
int movement_tagparser_add_pos(config *c, int postag);
void movement_tagparser_print(FILE *f, int mvt_code, dico *dico_labels, dico *dico_postag);
#endif
