#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<getopt.h>
#include"context.h"
#include"movement_parser.h"
#include"oracle_parser.h"
#include"feat_fct.h"
#include"config2feat_vec.h"
#include"feature_table.h"
#include"dico.h"

void simple_decoder_buffer(context *ctx, FILE *f, feature_table *ft, int root_label)
{
  int mvt_code;
  int mvt_type;
  int mvt_label;
  float max;
  feat_vec *fv = feat_vec_new(feature_types_nb);
  config *c = config_initial(f, ctx->mcd_struct, 0);
  
  /* read a sentence and put it in the buffer */
  while(queue_read_sentence(c->bf, f, ctx->mcd_struct) > 1){
    while(!config_is_terminal(c)){
      config2feat_vec_cff(ctx->features_model, c, ctx->d_perceptron_features, fv, LOOKUP_MODE);
      mvt_code = feature_table_argmax(fv, ft, &max);
      mvt_type = movement_type(mvt_code);
      mvt_label = movement_label(mvt_code);

      if(mvt_type == MVT_LEFT)
	if(movement_left_arc(c, mvt_label, max))
	  continue;
      
      if(mvt_type == MVT_RIGHT)
	if(movement_right_arc(c, mvt_label, max))
	  continue;
      
      movement_shift(c, 0, max);
    }

    /* config_print(stdout, c);    */
    
    config_connect_subtrees(c, root_label); 
    depset_print2(stdout, c->ds, ctx->dico_labels);

    
    /* config_free(c);  */
    c = config_initial(f, ctx->mcd_struct, 0);
  }

  feat_vec_free(fv);
}


void simple_decoder_stream(context *ctx, FILE *f, feature_table *ft, int root_label)
{
  int mvt_code;
  int mvt_type;
  int mvt_label;
  float max;
  feat_vec *fv = feat_vec_new(feature_types_nb);
  config *c = NULL;

  c = config_initial(f, ctx->mcd_struct, 5);
  while(!config_is_terminal(c)){
    /* config_print(stdout, c);   */
    config2feat_vec_cff(ctx->features_model, c, ctx->d_perceptron_features, fv, LOOKUP_MODE);
    /* feat_vec_print(stdout, fv);  */
    mvt_code = feature_table_argmax(fv, ft, &max);
    mvt_type = movement_type(mvt_code);
    mvt_label = movement_label(mvt_code);

    /* printf("code predicted = %d\n", mvt_code); */
    /* movement_print(stdout, mvt_code, ctx->dico_labels); */
    
    /* sentence is complete */
    if((stack_height(c->st)==1)  && (mvt_type == MVT_RIGHT) && (mvt_label == root_label)){       
    /* if((mvt_type == MVT_RIGHT) && (mvt_label == root_label)){       */
      /* if(mvt_label == root_label){        */
      /* printf("sentence complete\n"); */
      /*config_print(stdout, c);  */
      
      /* create the root arc */
      movement_right_arc(c, mvt_label, 0);
      
      /* shift dummy word in stack */
      movement_shift(c, 1, 0);

       /* config_print(stdout, c);    */

      /* config_connect_subtrees(c, root_label);  */
      /* depset_print_new_index(stdout, c->ds, ctx->dico_labels); */

      depset_print2(stdout, c->ds, ctx->dico_labels);
      
      /* pop the dummy word */
      stack_pop(c->st);
      /* remplace it with a fresh one */
      stack_push(c->st, word_create_dummy(ctx->mcd_struct));
      
      /* empty depset */
      depset_free(c->ds);
      c->ds = depset_new();

      /* renumber the words that are left in the buffer */
      c->current_index = queue_renumber_words(c->bf);
      continue;
    }

    if(mvt_type == MVT_LEFT)
      if(movement_left_arc(c, mvt_label, max))
	continue;
    
    if(mvt_type == MVT_RIGHT)
      if(movement_right_arc(c, mvt_label, max))
	continue;
    
    movement_shift(c, 1, max);
  }

  /* config_print(stdout, c);  */
  
  /* config_connect_subtrees(c, root_label);   */

  depset_print2(stdout, c->ds, ctx->dico_labels);
  
  
  /* config_free(c); */
  feat_vec_free(fv);
}


void simple_decoder(context *ctx)
{
  FILE *f = (ctx->input_filename)? myfopen(ctx->input_filename, "r") : stdin;
  feature_table *ft = feature_table_load(ctx->perc_model_filename, ctx->verbose);
  int root_label;

  root_label = dico_string2int(ctx->dico_labels, ctx->root_label);
  if(root_label == -1) root_label = 0;

  if(ctx->stream_mode)
    simple_decoder_stream(ctx, f, ft, root_label);
  else
    simple_decoder_buffer(ctx, f, ft, root_label);

  feature_table_free(ft);
  if(ctx->input_filename)
    fclose(f);

}

