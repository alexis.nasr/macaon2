#ifndef __DNN_DECODER__
#define __DNN_DECODER__
#include "fann.h"
#include "word_emb.h"
#include "feat_model.h"
#include "mcd.h"

void dnn_decoder(FILE *f, mcd *mcd_struct, struct fann *ann, feat_model  *fm, int verbose, int root_label, int stream_mode);

#endif
