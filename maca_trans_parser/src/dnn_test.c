#include <stdio.h>
#include "fann.h"
#include "context.h"
#include "cf_file.h"

void dnn_test_help_message(context *ctx)
{
  context_general_help_message(ctx);
  fprintf(stderr, "INPUT\n");
  context_dnn_model_help_message(ctx);
  context_fann_help_message(ctx);
}

void dnn_test_check_options(context *ctx)
{
  if(!ctx->fann_filename
     || !ctx->dnn_model_filename
     || ctx->help
     ){
    dnn_test_help_message(ctx);
    exit(1);
  }
}

int main(int argc, char *argv[])
{
  fann_type *calc_out;
  unsigned int i;
  int ret = 0;

  struct fann *ann;
  struct fann_train_data *data;
  context *ctx;
  unsigned int j = 0;
  unsigned int argmax, ref;
  double max;
  unsigned int correct = 0;
  unsigned int nb_class;
  ctx = context_read_options(argc, argv);
  dnn_test_check_options(ctx);
  
  printf("Creating network.\n");
  
  ann = fann_create_from_file(ctx->dnn_model_filename);

  if(!ann){
    printf("Error creating ann --- ABORTING.\n");
    return -1;
  }
  nb_class = fann_get_num_output(ann);
  printf("nb class = %d\n", nb_class);


  /*
  fann_print_connections(ann);
  fann_print_parameters(ann);
  */
  printf("Testing network.\n");
  
  data = fann_read_train_from_file(ctx->fann_filename);
    
  for(i = 0; i < fann_length_train_data(data); i++){
    calc_out = fann_run(ann, data->input[i]);
    argmax = 0;
    max = calc_out[argmax];
    for(j=0; j < nb_class; j++){
      if(data->output[i][j] == 1) ref = j;
      if(calc_out[j] > max){
	max = calc_out[j];
	argmax = j;
      }
    }
    /* printf("gold = %d predicted = %d\n", ref, argmax); */
    if(argmax == ref) correct++;
  }
  printf("precision = %f\n", (float) correct /fann_length_train_data(data));


  /*  for(i = 0; i < fann_length_train_data(data); i++){
    fann_reset_MSE(ann);
    calc_out = fann_test(ann, data->input[i], data->output[i]);
    printf("XOR test (%f, %f) -> %f, should be %f, difference=%f\n",
	   data->input[i][0], data->input[i][1], calc_out[0], data->output[i][0],
	   (float) fann_abs(calc_out[0] - data->output[i][0]));
  }
  */
  printf("Cleaning up.\n");
  fann_destroy_train(data);
  fann_destroy(ann);
  
  return ret;
}
