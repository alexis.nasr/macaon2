#include<stdio.h>
#include<stdlib.h>
#include"config.h"
#include"util.h"
#include"dico.h"
#include"feat_types.h"

config *config_new(FILE *f, mcd *mcd_struct, int lookahead)
{
  config *c = (config *)memalloc(sizeof(config));
  c->st = stack_new();
  c->bf = word_buffer_new(f, mcd_struct, lookahead);
  c->history = mvt_stack_new();
  c->mvt_chosen = 0;
  c->vcode_array = NULL;
  return c;
}

void config_free(config *c)
{
  stack_free(c->st);
  word_buffer_free(c->bf);
  mvt_stack_free(c->history);
  free(c);
}

int config_is_terminal(config *c)
{
  return (word_buffer_end(config_get_buffer(c)) &&
	  (stack_is_empty(config_get_stack(c)) ||
	   (stack_top(config_get_stack(c)) == NULL)));

  /*return (word_buffer_end(config_get_buffer(c)) &&
	  (stack_is_empty(config_get_stack(c)) ||
	  (stack_top(config_get_stack(c)) == NULL)));*/
}

void config_push_mvt(config *c, int type, word *gov, word *dep)
{
  mvt_stack_push(config_get_history(c), mvt_new(type, gov, dep)); 
}

mvt *config_pop_mvt(config *c)
{
  return mvt_stack_pop(config_get_history(c));
}

void config_print(FILE *f, config *c)
{
  if(c){
    if(stack_is_empty(c->st))
      fprintf(f, "[ ]");
    else
      stack_print(f, c->st);
    fprintf(f, "\n");
    word_buffer_print_compact(f, c->bf);
  }
}

