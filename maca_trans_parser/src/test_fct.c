#include<stdio.h>
#include<stdlib.h>

typedef int (*fct) (int i);

int f0(int i){return(i + 0);}
int f1(int i){return(i + 1);}
int f2(int i){return(i + 2);}
int f3(int i){return(i + 3);}
int f4(int i){return(i + 4);}
int f5(int i){return(i + 5);}

fct get_fct(fct *tab, int i)
{
  return tab[i];
}


int main(void)
{
  fct *tab;
  int i;

  tab = malloc(6 * sizeof(fct));
  tab[0] = f0;
  tab[1] = f1;
  tab[2] = f2;
  tab[3] = f3;
  tab[4] = f4;
  tab[5] = f5;

  for(i=0; i < 6; i++){
    printf("%d\n", get_fct(tab, i)(0));
  }


}
