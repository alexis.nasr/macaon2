#ifndef __FEAT_TYPES__
#define __FEAT_TYPES__

#define FEAT_TYPE_NB 36

#define FEAT_TYPE_INDEX 0
#define FEAT_TYPE_FORM 1
#define FEAT_TYPE_LEMMA 2
#define FEAT_TYPE_CPOS 3
#define FEAT_TYPE_POS 4
#define FEAT_TYPE_FEATS 5
#define FEAT_TYPE_GOV 6
#define FEAT_TYPE_LABEL 7
#define FEAT_TYPE_STAG 8
#define FEAT_TYPE_A 9
#define FEAT_TYPE_B 10
#define FEAT_TYPE_C 11
#define FEAT_TYPE_D 12
#define FEAT_TYPE_E 13
#define FEAT_TYPE_F 14
#define FEAT_TYPE_G 15
#define FEAT_TYPE_H 16
#define FEAT_TYPE_I 17
#define FEAT_TYPE_J 18
#define FEAT_TYPE_K 10
#define FEAT_TYPE_L 20
#define FEAT_TYPE_M 21
#define FEAT_TYPE_N 22
#define FEAT_TYPE_O 23
#define FEAT_TYPE_P 24
#define FEAT_TYPE_Q 25
#define FEAT_TYPE_R 26
#define FEAT_TYPE_S 27
#define FEAT_TYPE_T 28
#define FEAT_TYPE_U 29
#define FEAT_TYPE_V 30
#define FEAT_TYPE_W 31
#define FEAT_TYPE_X 32
#define FEAT_TYPE_Y 33
#define FEAT_TYPE_Z 34
#define FEAT_TYPE_TRANS 35

/* #define FEAT_TYPE_SENT_SEG 36 */
#define FEAT_TYPE_INT 36

#define FEAT_TYPE_INT_0 37
#define FEAT_TYPE_INT_1 38
#define FEAT_TYPE_INT_2 39
#define FEAT_TYPE_INT_3 40
#define FEAT_TYPE_INT_4 41
#define FEAT_TYPE_INT_5 42
#define FEAT_TYPE_INT_6 43
#define FEAT_TYPE_INT_7 44
#define FEAT_TYPE_INT_8 45
#define FEAT_TYPE_INT_9 46
#define FEAT_TYPE_INT_10 47
#define FEAT_TYPE_INT_11 48
#define FEAT_TYPE_INT_12 49
#define FEAT_TYPE_INT_13 50
#define FEAT_TYPE_INT_14 51
#define FEAT_TYPE_INT_15 52
#define FEAT_TYPE_INT_16 53

int feat_type_string2int(char *type_name);
#endif
