#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<getopt.h>
#include<ctype.h>

#include"context.h"
#include"movement_tagger.h"
#include"feat_fct.h"
#include"config2feat_vec.h"
#include"feature_table.h"
#include"dico.h"
#include"mcd.h"

void add_signature_to_words_in_word_buffer2(word_buffer *bf, form2pos *f2p)
{
  int i;
  word *w;
  char lower_form[100];

  for(i = word_buffer_get_nbelem(bf) - 1; i >=0  ; i--){
    w = word_buffer_get_word_n(bf, i);
    if(word_get_signature(w) != -1) break;
    w->signature = form2pos_get_signature(f2p, w->form);
    if(w->signature == -1){
      strcpy(lower_form, w->form);
      to_lower_string(lower_form);
      w->signature = form2pos_get_signature(f2p, lower_form);
    }
  }
}

void print_word2(word *w, mcd *mcd_struct, dico *dico_pos, int postag)
{
  char *buffer = NULL;
  char *token = NULL;
  int col_nb = 0;
  if(mcd_get_pos_col(mcd_struct) == -1){
    printf("%s\t%s\n", w->input, dico_int2string(dico_pos, postag));
  }
  else{
    buffer = strdup(w->input);
    token = strtok(buffer, "\t");
    col_nb = 0;
    while(token){
      if(col_nb != 0) printf("\t");
      if(col_nb == mcd_get_pos_col(mcd_struct))
	printf("%s", dico_int2string(dico_pos, postag));
      else
	word_print_col_n(stdout, w, col_nb);
      col_nb++;
      token = strtok(NULL, "\t");
    }
    if(col_nb <= mcd_get_pos_col(mcd_struct))
      printf("\t%s", dico_int2string(dico_pos, postag));
    printf("\n");
    free(buffer);
  }
}

void simple_decoder_tagger2(context *ctx)
{
  config *c;
  feat_vec *fv = feat_vec_new(2);
  FILE *f = (ctx->input_filename)? myfopen(ctx->input_filename, "r") : stdin;
  feature_table *ft =  feature_table_load(ctx->perc_model_filename, ctx->verbose);

  int postag;

  feat_model *local_feat_model = feat_model_read("/home/alexis/maca_data2/fr/bin/maca_trans_tagger.fm", ctx->verbose);
  /* dico_vec *local_dico_vec = dico_vec_read("/home/alexis/maca_data2/fr/bin/maca_trans_tagger.vocab", ctx->hash_ratio); */
  ctx->d_perceptron_features = dico_vec_get_dico(ctx->vocabs, (char *)"d_perceptron_features_bt");
  dico *dico_pos = dico_vec_get_dico(ctx->vocabs, (char *)"POS");
  dico *local_perceptron_features = dico_vec_get_dico(ctx->vocabs, (char *)"d_perceptron_features");
  feature_table *local_ft =  feature_table_load("/home/alexis/maca_data2/fr/bin/maca_trans_tagger.model", ctx->verbose);
  
  c = config_new(f, ctx->mcd_struct, 5); 

  while(!config_is_terminal(c)){
    if(ctx->f2p)
      /* add_signature_to_words_in_word_buffer2(c->bf, ctx->f2p, dico_pos); */
      add_signature_to_words_in_word_buffer2(c->bf, ctx->f2p);
    
    /* postag = word_get_pos(word_buffer_b0(c->bf));     */
    if(ctx->debug_mode){
      fprintf(stderr, "***********************************\n");
      config_print(stderr, c);
    }
    
    /* if postag is not specified in input it is predicted */
    /* if(postag == -1){ */
    
    /* apply local model */
    config2feat_vec_cff(local_feat_model, c, local_perceptron_features, fv, LOOKUP_MODE);

    if(c->vcode_array) free(c->vcode_array);
    c->vcode_array = feature_table_get_vcode_array(fv, local_ft);
    
    postag = c->vcode_array[0].class_code;
    
    if(ctx->debug_mode){
      fprintf(stderr, "apply local model\n");
      for(int i=0; i < 5; i++)
	fprintf(stderr, "%d\t%s\t%.4f\n", i, dico_int2string(dico_pos, c->vcode_array[i].class_code), c->vcode_array[i].score);
    }
      
    forward(c, postag);
      
    /* apply global model */
    
    config2feat_vec_cff(ctx->features_model, c, ctx->d_perceptron_features, fv, LOOKUP_MODE);
    vcode *vcode_array = feature_table_get_vcode_array(fv, ft);
    
    if(ctx->debug_mode){
      fprintf(stderr, "apply global model\n");
      for(int i=0; i < 3; i++)
	/* fprintf(stderr, "%d\t%d\t%.4f\n", i, vcode_array[i].class_code, vcode_array[i].score); */
	fprintf(stderr, "%d\t%s\t%.4f\n", i, dico_int2string(dico_pos, vcode_array[i].class_code), vcode_array[i].score);
    }
    
    int choice = vcode_array[0].class_code;
    word_set_pos(word_buffer_bm1(c->bf), choice);
    /*
    if(choice != 0){
      postag = c->vcode_array[choice].class_code;
      choice_n(c, choice);
      }*/
    free(vcode_array);
    /* } */
    /* print_word2(word_buffer_bm1(c->bf), ctx->mcd_struct, dico_pos, postag); */
    print_word2(word_buffer_bm1(c->bf), ctx->mcd_struct, dico_pos, choice);
    
  }
  /* config_print(stdout, c);  */
  config_free(c); 
}

