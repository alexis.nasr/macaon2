#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "fann.h"
#include"util.h"
#include"hash.h"
#include"context.h"
#include"sentence.h"
#include"dico_vec.h"

typedef struct
{
  int line_nb;
  int col_nb;
  int nbelem;
  int *array;
  dico_vec *dv;
} stags_desc;

void print_stag(FILE *f, char *stag, stags_desc *sd)
{
  dico *dico_stag = sd->dv->t[0];
  int   stag_code = dico_string2int(dico_stag, stag);
  int i;

  if(stag_code == -1)
    fprintf(stderr, "stag %s unknown\n", stag);
  else{
    for(i=0; i< dico_stag->nbelem; i++){
      if(i != 0) fprintf(f, " ");
      if(i == stag_code) fprintf(f, "1");
      else  fprintf(f, "0");
    }
  }
}

void print_stag_desc(FILE *f, char *stag, stags_desc *sd)
{
  dico *dico_stag = sd->dv->t[0];
  int   stag_code = dico_string2int(dico_stag, stag);
  int i,j;

  if(stag_code == -1)
    fprintf(stderr, "stag %s unknown\n", stag);
  else{
    for(i=1; i< sd->col_nb; i++){
      for(j=0; j < sd->dv->t[i]->nbelem; j++){
	if(i+ j != 0) fprintf(f, " ");;
	if(j == sd->array[stag_code * sd->col_nb + i])
	  fprintf(f, "1");
	else
	  fprintf(f, "0");
      }
    }
  }
}

stags_desc *stags_desc_new(int line_nb, int col_nb)
{
  stags_desc *sd = memalloc(sizeof(stags_desc));
  int i;

  sd->col_nb = col_nb;
  sd->line_nb = line_nb;
  sd->dv = dico_vec_new(col_nb);
  for(i=0; i < col_nb; i++){
    dico_vec_add(sd->dv, dico_new("X", 5));
  }
  sd->array = malloc(line_nb * col_nb * sizeof(int));
  return sd;
}

int column_number(char *filename)
{
  FILE *f = myfopen(filename, "r");
  char buffer[1000];
  int column_nb;
  char *token;

  fgets(buffer, 1000, f);
  token = strtok(buffer, " "); /* read stag name */
  column_nb = 1;
  while((token = strtok(NULL , " ")))
      column_nb++;
  fclose(f);
  return column_nb;
}

int line_number(char *filename)
{
  FILE *f = myfopen(filename, "r");
  char buffer[1000];
  int line_nb = 0;

  while(fgets(buffer, 1000, f)){
    if(feof(f)) break;
    line_nb++;
  }
  return line_nb;
}

stags_desc *read_stag_file(char *filename)
{
  FILE *f = myfopen(filename, "r");
  char buffer[2000];
  char *token;
  int col_nb = column_number(filename);
  int line_nb = line_number(filename);
  int value;
  int current_line = 0;
  int current_col = 0;
  stags_desc *sd = stags_desc_new(line_nb, col_nb);

  while(fgets(buffer, 2000, f)){
    if(feof(f)) break;
    current_col = 0;
    token = strtok(buffer, " ");
    do{
      if(current_col == col_nb) break;
      value = dico_add(sd->dv->t[current_col], token);
      /* printf("%d %d %s %d\n", current_line, current_col, token, value); */
      sd->array[current_line * col_nb + current_col] = value; 
      current_col++;
    }while((token = strtok(NULL , " ")));
    current_line++;
  }
  return sd;
}

int compute_input_size(stags_desc *sd)
{
  int i;
  int size = 0;

  for(i=1; i< sd->col_nb; i++)
    size += sd->dv->t[i]->nbelem;
  return size;
}

void generate_fann_file(stags_desc *sd, context *ctx)
{
  sentence *s = NULL;
  FILE *f_conll;
  FILE *f_fann;
  int sentence_nb = 0;
  int i;
  char *stag_minus_one;
  char *stag_minus_two;
  char *stag_plus_one;
  char *current_stag;
  char *stag_plus_two;
  char *stag_gov;
  int input_size = compute_input_size(sd);
  int output_size = sd->dv->t[0]->nbelem;
  int example_nb = 0;

  /* open conll file */
  f_conll = myfopen(ctx->conll_filename, "r");

  /* open fann file */
  f_fann = myfopen(ctx->fann_filename, "w");

  fprintf(f_fann, "            %d %d\n", input_size, output_size);
  while((s = sentence_read(f_conll, ctx)) && (sentence_nb < ctx->sent_nb)){
    sentence_nb++;
    fprintf(stderr, "\rsent nb : %d", sentence_nb);
    for(i=0; i < s->length; i++){
      if((i > 2) && (i < s->length - 2)){
	stag_minus_two = s->words[i - 2]->pos;
	stag_minus_one = s->words[i - 1]->pos;
	current_stag = s->words[i]->pos;
	stag_plus_one =  s->words[i + 1]->pos;
	stag_plus_two =  s->words[i + 2]->pos;
	/*	fprintf(stderr, "stag_minus_two = %s\n", stag_minus_two);
	fprintf(stderr, "stag_minus_one = %s\n", stag_minus_one);
	fprintf(stderr, "current_stag = %s\n", current_stag);
	fprintf(stderr, "stag_plus_one  = %s\n", stag_plus_one);
	fprintf(stderr, "stag_plus_two  = %s\n", stag_plus_two);*/

	/* print input */
	
	print_stag_desc(f_fann, stag_minus_two, sd);
	print_stag_desc(f_fann, stag_minus_one, sd);
	print_stag_desc(f_fann, stag_plus_one, sd);
	print_stag_desc(f_fann, stag_plus_two, sd);
	if(s->words[i]->gov != 0){
	  stag_gov = s->words[s->words[i]->gov]->pos;
	  /* printf("stag_gov  = %s\n", stag_gov); */
	  print_stag_desc(f_fann, stag_gov, sd);
	}
	fprintf(f_fann, "\n");

	/* print output */
	print_stag(f_fann, current_stag, sd);
	fprintf(f_fann, "\n");
	example_nb++;
      }
    }
    sentence_free(s);
  }
  fclose(f_conll);
  /* add number of examples in the fann file (ugly !) */
  rewind(f_fann);
  fprintf(f_fann, "%d", example_nb);
  fclose(f_fann);
}

int FANN_API train_callback(struct fann *ann, struct fann_train_data *train,
	unsigned int max_epochs, unsigned int epochs_between_reports, 
			   float desired_error, unsigned int epochs)
{
  fprintf(stderr, "Epochs     %8d. MSE: %.5f. Desired-MSE: %.5f\n", epochs, fann_get_MSE(ann), desired_error);
  return 0;
}

struct fann *train_fann(context *ctx)
{
  int nb_feat;
  int nb_class;
  const unsigned int num_layers = 3;
  unsigned int num_neurons_hidden;
  const float desired_error = (const float) 0;
  unsigned int max_epochs = 200;
  const unsigned int epochs_between_reports = 0;
  struct fann *ann;
  struct fann_train_data *data;

  max_epochs = ctx->iteration_nb;
  num_neurons_hidden = ctx->hidden_neurons_nb;

  data = fann_read_train_from_file(ctx->fann_filename);
  nb_feat = fann_num_input_train_data(data);
  nb_class = fann_num_output_train_data(data);
  
  fprintf(stderr, "Creating network.\n");
  ann = fann_create_standard(num_layers, nb_feat, num_neurons_hidden, nb_class);
  fann_set_activation_steepness_hidden(ann, 1);
  fann_set_activation_steepness_output(ann, 1);
  fann_set_activation_function_hidden(ann, FANN_SIGMOID_SYMMETRIC);
  /* fann_set_activation_function_output(ann, FANN_SIGMOID_SYMMETRIC); */
  fann_set_train_stop_function(ann, FANN_STOPFUNC_BIT);
  fann_set_bit_fail_limit(ann, 0.01f);
  fann_set_training_algorithm(ann, FANN_TRAIN_RPROP);
  
  fprintf(stderr, "Training network.\n");
  
  fann_init_weights(ann, data);

  fann_train_on_data(ann, data, max_epochs, epochs_between_reports, desired_error);
  return ann;
}


void print_embdeddings(struct fann *ann, stags_desc *sd)
{
  struct fann_neuron *output_neuron;
  int output_neuron_num;
  unsigned int idx;
  struct fann_layer *input_layer =  ann->first_layer;
  struct fann_layer *hidden_layer =  ann->first_layer + 1;
  struct fann_layer *output_layer =  ann->first_layer + 2;
  unsigned int input_neurons_nb = (unsigned int)(input_layer->last_neuron - input_layer->first_neuron);
  unsigned int hidden_neurons_nb = (unsigned int)(hidden_layer->last_neuron - hidden_layer->first_neuron);
  /* unsigned int output_neurons_nb = (unsigned int)(output_layer->last_neuron - output_layer->first_neuron); */
  
  /*  printf("number of neurons in input layer = %u\n", input_neurons_nb);
  printf("number of neurons in hidden layer = %u\n", hidden_neurons_nb);
  printf("number of neurons in output layer = %u\n", output_neurons_nb);*/

  /* for each neuron of the output layer */
  for(output_neuron = output_layer->first_neuron; output_neuron != output_layer->last_neuron - 1; output_neuron++){
    output_neuron_num = (int) (output_neuron - ann->first_layer->first_neuron) - (input_neurons_nb + hidden_neurons_nb);
    printf("%s", dico_int2string(sd->dv->t[0], output_neuron_num));
    /* for each connection */

    for (idx = output_neuron->first_con; idx < output_neuron->last_con - 1; idx++){
      printf(" %f", ann->weights[idx]);
    }
    printf("\n");
  }
}

void learn_stag_embeddings_help_message(context *ctx)
{
  context_general_help_message(ctx);
  context_iterations_help_message(ctx);
  context_sent_nb_help_message(ctx);
  context_hidden_neurons_nb_help_message(ctx);
  fprintf(stderr, "INPUT\n");
  context_fann_help_message(ctx);
  context_conll_help_message(ctx);
  context_stag_desc_filename_help_message(ctx);
  fprintf(stderr, "OUTPUT\n");
  context_dnn_model_help_message(ctx);
}

int main(int argc, char *argv[])
{
  context *ctx = NULL;
  stags_desc *sd = NULL;
  struct fann *ann = NULL;
  
  ctx = context_read_options(argc, argv);  
  learn_stag_embeddings_help_message(ctx);

  fprintf(stderr, "read stag file : %s\n",ctx->stag_desc_filename);
  sd = read_stag_file(ctx->stag_desc_filename);

  fprintf(stderr, "generate fann  file\n");
  generate_fann_file(sd, ctx);

  fprintf(stderr, "train dnn\n");
  ann = train_fann(ctx); 
  print_embdeddings(ann, sd);

  return 0;
}
