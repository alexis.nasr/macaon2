#ifndef __MOVEMENT_TAGGER__
#define __MOVEMENT_TAGGER__

#include"config.h"
#include"feat_vec.h"
int movement_tagger(config *c, int postag);


int forward(config *c, int postag);
int next_choice(config *c, int postag);
int backward(config *c);
int choice_n(config *c, int n);



#endif
