#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"oracle_parser.h"
#include"movement_parser.h"

int check_all_dependents_of_word_in_ref_are_in_hyp(config *c, sentence *ref, int word_index)
{
  int dep;
  
  for(dep=0; dep < ref->length; dep++){
    if(word_get_gov_index(ref->words[dep]) == word_index){ /* found a dependent of word in ref */
      /* look for a dependency in hyp such that its dependent is dep */
      if((dep >= c->ds->length)
	 || (c->ds->array[dep].gov == NULL)
	 || (word_get_index(c->ds->array[dep].gov) != word_index)
	 || (c->ds->array[dep].label != word_get_label(ref->words[dep])))
	return 0;
    }
  }
  return 1;
}

int oracle_parser(config *c, sentence *ref)
{
  word *s0; /* word on top of stack */
  word *b0; /* next word in the bufer */
  int s0_index, b0_index;

  if(!stack_is_empty(c->st) && !word_buffer_is_empty(c->bf)){
    s0 = stack_top(c->st);
    s0_index = word_get_index(s0);
    
    b0 = word_buffer_b0(c->bf);
    b0_index = word_get_index(b0);

    /* printf("s0 = %d b0 = %d\n", s0_index, b0_index);  */
    /*printf("dans ref gov de %d = %d\n", s0_index, word_get_gov_index(ref->words[s0_index])); 
      printf("dans ref gov de %d = %d\n", b0_index, word_get_gov_index(ref->words[b0_index])); */
    
    /* LEFT ARC  b0 is the governor and s0 the dependent */
    
    if(word_get_gov_index(ref->words[s0_index]) == b0_index)
      return movement_left_code(word_get_label(ref->words[s0_index])); 
    
    /* RIGHT ARC s0 is the governor and b0 the dependent */
    if((word_get_gov_index(ref->words[b0_index]) == s0_index)
       && check_all_dependents_of_word_in_ref_are_in_hyp(c, ref, b0_index)){
      return movement_right_code(word_get_label(ref->words[b0_index])); 
    }
  }
  /* SHIFT */
  return MVT_SHIFT;
}
