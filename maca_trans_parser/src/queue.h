#ifndef __QUEUE__
#define __QUEUE__

#include<stdio.h>
#include"word.h"
#include"mcd.h"

#define queue_nbelem(q) (q)->nbelem
#define queue_size(q) (q)->size
#define queue_head(q) (q)->head
#define queue_tail(q) (q)->tail

typedef struct {
  int size;
  word **array;
  int head;
  int tail;
  int nbelem;
} queue;

int queue_renumber_words(queue *bf);
int queue_read_sentence(queue *bf, FILE *f, mcd *mcd_struct);
queue *queue_new(void);
queue *queue_copy(queue *q);
void queue_free(queue *q);
int queue_is_empty(queue *q);
void queue_add(queue *q, word *w);
void queue_add_in_front(queue *q, word *w);
word *queue_remove(queue *q);
void queue_print(FILE *f, queue *q);
word *queue_elt_n(queue *q, int n);
void queue_double_size(queue *q);

#endif
