#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<getopt.h>
#include<ctype.h>
#include"movement_tagger.h"
#include"oracle_tagger.h"
#include"feat_fct.h"
#include"context.h"
#include"feat_vec.h"
#include"dico_vec.h"
#include"word_emb.h"
#include"config2feat_vec.h"
#include"feature_table.h"
#include"dico.h"
#include"mcd.h"

void print_word_simple(word *w, mcd *mcd_struct, dico *dico_pos, int postag)
{
  char *buffer = NULL;
  char *token = NULL;
  buffer = strdup(w->input);
  token = strtok(buffer, "\t");
  printf("%s\t%s\n", token ,dico_int2string(dico_pos, postag));
  free(buffer);
}


void add_signature_to_words_in_word_buffer_tagger(word_buffer *bf, form2pos *f2p)
{
  int i;
  word *w;
  char lower_form[1000];
  
  for(i = word_buffer_get_nbelem(bf) - 1; i >=0  ; i--){
    w = word_buffer_get_word_n(bf, i);
    if(word_get_signature(w) != -1) break;
    w->signature = form2pos_get_signature(f2p, w->form);
    if(w->signature == -1){
      if(w->form){
        strcpy(lower_form, w->form);
        to_lower_string(lower_form);
        w->signature = form2pos_get_signature(f2p, lower_form);
      }
    }
  }
}

void maca_error_predictor_help_message(context *ctx)
{
  context_general_help_message(ctx);

  fprintf(stderr, "\t-d --debug                 : debug option\n");
  fprintf(stderr, "INPUT\n");
    fprintf(stderr, "\t-q --classes           : number of error classes \n");
  fprintf(stderr, "\t-i --input      <file> : input is in mcf format (default is new_dev.mcf)\n");
  fprintf(stderr, "\t-F --feat_model <file> : feature model file name\n");
  fprintf(stderr, "\t-f --fann       <file> : feature model error file name\n");
  fprintf(stderr, "\t-m --model      <file> : model filename\n");
  fprintf(stderr, "\t-C --mcd        <file> : model error filename\n");
  fprintf(stderr, "\t-V --vocabs     <file> : vocabularies file\n");
  fprintf(stderr, "\t-N --dnn_model  <file> : vocabularies error file\n");
  
  fprintf(stderr, "OUTPUT\n");
  fprintf(stderr, "\t-x --cff        <file> : CFF format file name (default is stdout)\n");

}

void maca_error_predictor_check_options(context *ctx)
{
  if(ctx->help){
    maca_error_predictor_help_message(ctx);
    exit(1);
  }
}

/* 

   return 0 if no error detected
   return i if an error is detected i words before b0

 */

int config_is_equal_tagger(config *c1, config *c2, int p1, int p2, int nb_classes)
{
  int classN = 0;

   for (int i=1 ; i<nb_classes ; i++) {
     if (bmip(c1,i) != -1 || bmip(c1,i) != -1) { 
      if (bmip(c1,i) != bmip(c2,i))
        classN = i;
    }
    else
      return classN;
  }

  return classN;
}


void generate_error_train(FILE *output_file, context *ctx)
{
  config *config_oracle;
  feat_vec *fv_decoder = feat_vec_new(feature_types_nb);
  FILE *mcf_file_oracle = myfopen(ctx->input_filename, "r");
  int postag_oracle;
  word *b0;
  
  config *config_predicted;
  feature_table *ft = feature_table_load(ctx->perc_model_filename, ctx->verbose);
  feat_vec *fv_error = feat_vec_new(feature_types_nb);
  FILE *mcf_file_predicted = myfopen(ctx->input_filename, "r");
  int postag_predicted;
  float max;
  dico *dico_pos = dico_vec_get_dico(ctx->vocabs, (char *)"POS");


  mcd *mcd_struct_hyp = mcd_copy(ctx->mcd_struct);
  
  config_oracle = config_new(mcf_file_oracle, ctx->mcd_struct, 5); 
  config_predicted = config_new(mcf_file_predicted, mcd_struct_hyp, 5);
  
  while(!config_is_terminal(config_oracle)){
    if(ctx->f2p){
      add_signature_to_words_in_word_buffer_tagger(config_predicted->bf, ctx->f2p);
      add_signature_to_words_in_word_buffer_tagger(config_oracle->bf, ctx->f2p);
    }

    // oracle
    config2feat_vec_cff(ctx->features_model, config_predicted, ctx->d_perceptron_features, fv_decoder, LOOKUP_MODE);
    postag_oracle = oracle_tagger(config_oracle);
    
    if(ctx->debug_mode){
      printf("Oracle    : ");
      print_word_simple(word_buffer_b0(config_oracle->bf), ctx->mcd_struct, dico_pos, postag_oracle);
    }
    
    // predicted
    b0 = word_buffer_b0(config_predicted->bf);
    
    postag_predicted = feature_table_argmax(fv_decoder, ft, &max);
    
    if(ctx->debug_mode){
      printf("Predicted : ");
      print_word_simple(b0, ctx->mcd_struct, dico_pos, postag_predicted);
    }
    
    if(ctx->debug_mode){
      vcode *vcode_array = feature_table_get_vcode_array(fv_decoder, ft);
      for(int i=0; i < 3; i++){
        printf("%d\t", i);
        printf("%s\t%.4f\n", dico_int2string(dico_pos, vcode_array[i].class_code), vcode_array[i].score);
      }
      free(vcode_array);
      
      if (postag_oracle!=postag_predicted)
        fprintf(stdout, "**************** DIFFERENT CHOICE ***********\n\n");

      else
        fprintf(stdout, "**************** EQUAL CHOICE ***********\n\n");

    }
    
    // error training
    config2feat_vec_cff(ctx->features_model_error, config_predicted, ctx->d_perceptron_features_error, fv_error, TRAIN_MODE);

    if(!ctx->debug_mode || output_file!=stdout) {
      fprintf(output_file, "%d", ((config_is_equal_tagger(config_oracle, config_predicted, postag_oracle,postag_predicted, ctx->nb_classes))));
      feat_vec_print(output_file, fv_error);
    }
    
    movement_tagger(config_oracle, postag_oracle);
    movement_tagger(config_predicted, postag_predicted);
  }
  

  feat_vec_free(fv_decoder);
  feat_vec_free(fv_error);
  feature_table_free(ft);
  config_free(config_oracle);
  config_free(config_predicted); 
  
  fclose(mcf_file_oracle);
  fclose(mcf_file_predicted);

}


int main(int argc, char *argv[])
{
  context *ctx;
  FILE *output_file;
  
  ctx = context_read_options(argc, argv);
  
  maca_error_predictor_check_options(ctx);
  
  ctx->f2p = form2pos_read(ctx->f2p_filename);
  ctx->mcd_struct = mcd_read(ctx->mcd_filename, ctx->verbose);

  //tagger
  ctx->vocabs = dico_vec_read(ctx->vocabs_filename, ctx->hash_ratio); 
  mcd_link_to_dico(ctx->mcd_struct, ctx->vocabs, ctx->verbose);

  //error
  ctx->d_perceptron_features_error = dico_new((char *)"d_perceptron_features_error", 10000000);
  ctx->features_model_error = feat_model_read(ctx->fann_filename, feat_lib_build(), ctx->verbose);

  //tagger
  ctx->d_perceptron_features = dico_vec_get_dico(ctx->vocabs, (char *)"d_perceptron_features");
  ctx->features_model = feat_model_read(ctx->features_model_filename, feat_lib_build(), ctx->verbose);
    
  /* open output file */
  if(ctx->cff_filename)
    output_file = myfopen(ctx->cff_filename, "w");
  else
    output_file = stdout;

  generate_error_train(output_file,ctx);
  
  /* add the feature dictionnary to the dico vector */
  dico_vec_add(ctx->vocabs, ctx->d_perceptron_features_error);
  dico_vec_print(ctx->dnn_model_filename, ctx->vocabs);

  if(ctx->cff_filename)
    fclose(output_file);
 
  context_free(ctx);
  
  return 0;
}


