#include <stdio.h>
#include <stdlib.h>

#include "word_emb.h"
#include "dico.h"


int main(int argc, char *argv[])
{
  char *w2v_filename = argv[1];
  char *dico_filename = argv[2];
  dico *d = dico_read(dico_filename, 0.5);
  word_emb *we = word_emb_load_w2v_file_filtered(w2v_filename, d);
  
  word_emb_print_to_file(we, NULL);

  return 0;

}
