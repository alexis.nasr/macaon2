#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"word_buffer.h"
#include"movement_tagparser_arc_eager.h"

int check_all_dependents_of_word_in_ref_are_in_hyp(config *c, word_buffer *ref, int word_index)
{
  int dep;
  int gov_ref;
  int gov_hyp;
  int sentence_change;  

#if 0
  for(dep = word_index - 1; (dep >= 0) && (word_get_sent_seg(word_buffer_get_word_n(ref, dep)) == 0); dep--){
    gov_ref = word_get_gov_index(word_buffer_get_word_n(ref, dep)); 
    if(gov_ref  == word_index){ /* dep is a dependent of word in ref */
      /* check that dep has the same governor in hyp */
      gov_hyp = word_get_gov_index(word_buffer_get_word_n(config_get_buffer(c), dep));
      if(gov_hyp != gov_ref) return 0;
    }
  }

  for(dep = word_index + 1; ((dep < word_buffer_get_nbelem(ref)) && (word_get_sent_seg(word_buffer_get_word_n(ref, dep)) == 0)); dep++){
    gov_ref = word_get_gov_index(word_buffer_get_word_n(ref, dep)); 
    if(gov_ref  == word_index){ /* dep is a dependent of word in ref */
      /* check that dep has the same governor in hyp */
      gov_hyp = word_get_gov_index(word_buffer_get_word_n(config_get_buffer(c), dep));
      if(gov_hyp != gov_ref) return 0;
    }
  }
#endif

#if 1
  for(dep = word_index - 1; (dep >= 0) && (word_get_sent_seg(word_buffer_get_word_n(ref, dep)) == 0); dep--){
    gov_ref = word_get_gov_index(word_buffer_get_word_n(ref, dep)); 
    if(gov_ref  == word_index){ /* dep is a dependent of word in ref */
      /* check that dep has the same governor in hyp */
      gov_hyp = word_get_gov_index(word_buffer_get_word_n(config_get_buffer(c), dep));
      if(gov_hyp != gov_ref) return 0;
    }
  }

  sentence_change = 0;
  for(dep = word_index + 1; (dep < word_buffer_get_nbelem(ref)) && (sentence_change == 0); dep++){
    if(word_get_sent_seg(word_buffer_get_word_n(ref, dep)) == 1)
      sentence_change = 1;
    gov_ref = word_get_gov_index(word_buffer_get_word_n(ref, dep)); 
    if(gov_ref  == word_index){ /* dep is a dependent of word in ref */
      /* look for a dependency in hyp such that its dependent is dep */
      gov_hyp = word_get_gov_index(word_buffer_get_word_n(config_get_buffer(c), dep));
      if(gov_hyp != gov_ref) return 0;
    }
  }

#endif
  
  return 1;
}

int oracle_tagparser_arc_eager(config *c, word_buffer *ref, int root_label)
{
  word *s0; /* word on top of stack */
  word *b0; /* next word in the bufer */
  int s0_index, b0_index;
  int s0_gov_index, b0_gov_index;
  int s0_label;
  /* int s0_label_in_hyp; */

  b0 = word_buffer_b0(config_get_buffer(c));
  b0_index = word_get_index(b0);
  b0_gov_index = word_get_gov_index(word_buffer_get_word_n(ref, b0_index));

  
  /* give a pos to b0  if it does not have one */
  if(word_get_pos(b0) == -1){
    /* word_set_pos(b0, word_get_pos(word_buffer_get_word_n(ref, b0_index))); */
    /* return movement_tagparser_postag(word_get_pos(b0)); */

    return movement_tagparser_postag_code(word_get_pos(word_buffer_get_word_n(ref, b0_index)));
  }
  
  
  /* if(!stack_is_empty(config_get_stack(c)) && !word_buffer_is_empty(config_get_buffer(c))){ */
  if(!stack_is_empty(config_get_stack(c))){
    
    
    s0 = stack_top(config_get_stack(c));
    s0_index = word_get_index(s0);
    s0_gov_index = word_get_gov_index(word_buffer_get_word_n(ref, s0_index));
    s0_label = word_get_label(word_buffer_get_word_n(ref, s0_index));
    /* s0_label_in_hyp = word_get_label(word_buffer_get_word_n(config_get_buffer(c), s0_index)); */
    
    /*    printf("s0_index = %d b0_index = %d\n", s0_index, b0_index);  
	  printf("dans ref gov de s0 (%d) = %d\n", s0_index, s0_gov_index);
	  printf("dans ref gov de b0 (%d) = %d\n", b0_index, b0_gov_index);*/
    
    
    
    
    /* s0 is the root of the sentence */
    if((s0_label == root_label)
       && check_all_dependents_of_word_in_ref_are_in_hyp(c, ref, s0_index)
       ){
      return MVT_TAGPARSER_ROOT;
    }

    /* word on the top of the stack is an end of sentence marker */
      if((word_get_sent_seg(word_buffer_get_word_n(ref, s0_index)) == 1)
       && (word_get_sent_seg(word_buffer_get_word_n(config_get_buffer(c), s0_index)) != 1)){
      return MVT_TAGPARSER_EOS;
      }

    /* LEFT ARC  b0 is the governor and s0 the dependent */
    if(s0_gov_index == b0_index){
      return movement_tagparser_left_code(word_get_label(word_buffer_get_word_n(ref, s0_index)));
    }
    
    /* RIGHT ARC s0 is the governor and b0 the dependent */
    if(b0_gov_index == s0_index){
      return movement_tagparser_right_code(word_get_label(word_buffer_get_word_n(ref, b0_index))); 
    }
    /* REDUCE */
    if((stack_nbelem(config_get_stack(c)) > 1) 
       && check_all_dependents_of_word_in_ref_are_in_hyp(c, ref, s0_index)     /* word on top must have all its dependents */
       && (word_get_gov(stack_top(config_get_stack(c))) != WORD_INVALID_GOV)){ /* word on top of the stack has a governor */
      return MVT_TAGPARSER_REDUCE;
    }
  }

  /* SHIFT */
  return MVT_TAGPARSER_SHIFT;

}
