#include "word_emb.h"
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char *argv[])
{

  printf("argc = %d\n", argc);
  printf("argv1 = %s\n", argv[1]);

  word_emb *we = word_emb_load_w2v_file(argv[1]);
  word_emb_print_to_file(we, NULL);

  return 0;

}
