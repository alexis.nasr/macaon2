#ifndef __MOVEMENTS_
#define __MOVEMENTS_

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"config.h"

int movement_eos(config *c, int movement_code);
int movement_eos_undo(config *c);
int movement_left_arc(config *c, int movement_code, int label);
int movement_left_arc_undo(config *c);
int movement_right_arc(config *c, int movement_code, int label);
int movement_right_arc_undo(config *c);
int movement_ignore(config *c, int movement_code);
int movement_shift(config *c, int movement_code);
int movement_shift_undo(config *c);
int movement_reduce(config *c, int movement_code);
int movement_reduce_undo(config *c);
int movement_root(config *c, int movement_code, int root_code);
int movement_root_undo(config *c);
int movement_undo(config *c);
int movement_add_pos(config *c, int movement_code, int pos);
int movement_add_pos_undo(config *c);

#endif
