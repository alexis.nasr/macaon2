#ifndef __ORACLE__
#define __ORACLE__


#include<stdio.h>
#include<stdlib.h>
#include"config.h"
#include"sentence.h"


int oracle_parser(config *c, sentence *ref);

#endif
