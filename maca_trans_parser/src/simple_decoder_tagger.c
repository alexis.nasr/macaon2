#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<getopt.h>
#include<ctype.h>

#include"context.h"
#include"movement_tagger.h"
#include"feat_fct.h"
#include"config2feat_vec.h"
#include"feature_table.h"
#include"dico.h"
#include"mcd.h"

#if 1
void add_signature_to_words_in_word_buffer(word_buffer *bf, form2pos *f2p)
{
  int i;
  word *w;
  char lower_form[1000];

  for(i = word_buffer_get_nbelem(bf) - 1; i >=0  ; i--){
    w = word_buffer_get_word_n(bf, i);
    if(word_get_signature(w) != -1) break;
    w->signature = form2pos_get_signature(f2p, w->form);
    if(w->signature == -1){
      strcpy(lower_form, w->form);
      to_lower_string(lower_form);
      w->signature = form2pos_get_signature(f2p, lower_form);
    }
  }
}
#endif

#if 0
void add_signature_to_words_in_word_buffer(word_buffer *bf, form2pos *f2p, dico *dico_pos)
{
  int i;
  word *w;
  int signature;
  char *pos;
  for(i = word_buffer_get_nbelem(bf) - 1; i >=0  ; i--){
    w = word_buffer_get_word_n(bf, i);
    if(word_get_signature(w) != -1) break;
    signature = form2pos_get_signature(f2p, w->form);
    w->signature = signature;
    if(form2pos_word_is_non_ambiguous(f2p, w->form, &pos)){
      //      printf("%s non ambigu code = %d \n", pos, dico_string2int(dico_pos, pos)); 
      word_set_pos(w, dico_string2int(dico_pos, pos));
      
    }
  }
}
#endif

void print_word(word *w, mcd *mcd_struct, dico *dico_pos, int postag)
{
  char *buffer = NULL;
  char *token = NULL;
  int col_nb = 0;
  if(mcd_get_pos_col(mcd_struct) == -1){
    printf("%s\t%s\n", w->input, dico_int2string(dico_pos, postag));
  }
  else{
    buffer = strdup(w->input);
    token = strtok(buffer, "\t");
    col_nb = 0;
    while(token){
      if(col_nb != 0) printf("\t");
      if(col_nb == mcd_get_pos_col(mcd_struct))
	printf("%s", dico_int2string(dico_pos, postag));
      else
	word_print_col_n(stdout, w, col_nb);
      col_nb++;
      token = strtok(NULL, "\t");
    }
    if(col_nb <= mcd_get_pos_col(mcd_struct))
      printf("\t%s", dico_int2string(dico_pos, postag));
    printf("\n");
    free(buffer);
  }
}

#if 1
void simple_decoder_tagger(context *ctx)
{
  config *c;
  feat_vec *fv = feat_vec_new(feature_types_nb);
  FILE *f = (ctx->input_filename)? myfopen(ctx->input_filename, "r") : stdin;
  feature_table *ft =  feature_table_load(ctx->perc_model_filename, ctx->verbose);
  int postag;
  float max;
  word *b0;
  dico *dico_pos = dico_vec_get_dico(ctx->vocabs, (char *)"POS");

  c = config_new(f, ctx->mcd_struct, 5); 

  while(!config_is_terminal(c)){
    if(ctx->f2p)
      /* add_signature_to_words_in_word_buffer(c->bf, ctx->f2p, dico_pos); */
      add_signature_to_words_in_word_buffer(c->bf, ctx->f2p); 

    b0 = word_buffer_b0(c->bf);
    postag = -1;//word_get_pos(b0);

    if(ctx->debug_mode){
      fprintf(stderr, "***********************************\n");
      config_print(stderr, c);
    }
    
    /* if postag is not specified in input it is predicted */
    if(postag == -1){
      /* config_print(stdout, c); */
      config2feat_vec_cff(ctx->features_model, c, ctx->d_perceptron_features, fv, LOOKUP_MODE);
      
      /* feat_vec_print(stdout, fv); */
      postag = feature_table_argmax(fv, ft, &max);
      /* printf("postag = %d\n", postag); */

      if(ctx->debug_mode){
	vcode *vcode_array = feature_table_get_vcode_array(fv, ft);
	for(int i=0; i < 3; i++){
	  fprintf(stderr, "%d\t", i);
	  fprintf(stderr, "%s\t%.4f\n", dico_int2string(dico_pos, vcode_array[i].class_code), vcode_array[i].score);
	}
	free(vcode_array);
      }
    }

    print_word(b0, ctx->mcd_struct, dico_pos, postag);
    
    movement_tagger(c, postag);

  }
  /* config_print(stdout, c);  */
  feat_vec_free(fv);
  feature_table_free(ft);
  config_free(c); 
  if (ctx->input_filename) fclose(f);
}
#endif

#if 0

void simple_decoder_tagger(context *ctx)
{
  config *c;
  feat_vec *fv = feat_vec_new(feature_types_nb);
  FILE *f = (ctx->input_filename)? myfopen(ctx->input_filename, "r") : stdin;
  feature_table *ft =  feature_table_load(ctx->perc_model_filename, ctx->verbose);
  int postag;
  float max;
  dico *dico_pos = dico_vec_get_dico(ctx->vocabs, (char *)"POS");
  
  c = config_new(f, ctx->mcd_struct, 5); 
  
  while(!config_is_terminal(c)){
    if(ctx->f2p)
      /* add_signature_to_words_in_word_buffer(c->bf, ctx->f2p, dico_pos); */
      add_signature_to_words_in_word_buffer(c->bf, ctx->f2p); 
    
    if(ctx->debug_mode){
      fprintf(stderr, "***********************************\n");
	fprintf(stderr, "b0 lex = %d\n", word_get_form(word_buffer_b0(config_get_buffer(c))));
      config_print(stderr, c);
    }
    
    /* config_print(stdout, c); */
    config2feat_vec_cff(ctx->features_model, c, ctx->d_perceptron_features, fv, LOOKUP_MODE);
    
    /* feat_vec_print(stdout, fv); */
    postag = feature_table_argmax(fv, ft, &max);
    /* printf("postag = %d\n", postag); */
    
    if(ctx->debug_mode){
      vcode *vcode_array = feature_table_get_vcode_array(fv, ft);
      for(int i=0; i < 3; i++){
	fprintf(stderr, "%d\t", i);
	fprintf(stderr, "%s\t%.4f\n", dico_int2string(dico_pos, vcode_array[i].class_code), vcode_array[i].score);
      }
      free(vcode_array);
    }
    
    word_set_pos(word_buffer_b0(config_get_buffer(c)), postag); 
  
    if((word_buffer_b0(config_get_buffer(c)))->index > 0){
      /* word_buffer_move_left(config_get_buffer(c)); */
      word_buffer_move_left(config_get_buffer(c));
      int postag_old = word_get_pos(word_buffer_b0(config_get_buffer(c)));

      config2feat_vec_cff(ctx->features_model, c, ctx->d_perceptron_features, fv, LOOKUP_MODE);
      int postag_new = feature_table_argmax(fv, ft, &max);

      if(ctx->debug_mode){
	fprintf(stderr, "***********************************\n");
	fprintf(stderr, "b1p = %s\n", dico_int2string(dico_pos, b1p(c)));
	fprintf(stderr, "bm1p = %s\n", dico_int2string(dico_pos, bm1p(c)));
	fprintf(stderr, "b0 index = %d\n", word_get_index(word_buffer_b0(config_get_buffer(c))));
	fprintf(stderr, "b0 lex = %d\n", word_get_form(word_buffer_b0(config_get_buffer(c))));
	config_print(stderr, c);
	vcode *vcode_array = feature_table_get_vcode_array(fv, ft);
	for(int i=0; i < 3; i++){
	  fprintf(stderr, "%d\t", i);
	  fprintf(stderr, "%s\t%.4f\n", dico_int2string(dico_pos, vcode_array[i].class_code), vcode_array[i].score);
	}
	free(vcode_array);
      }

      if(postag_new != postag_old)
	fprintf(stderr, "postag old = %s postag_new = %s\n", dico_int2string(dico_pos, postag_old), dico_int2string(dico_pos, postag_new));
      
      word_set_pos(word_buffer_b0(c->bf), postag_new); 
      print_word(word_buffer_b0(c->bf), ctx->mcd_struct, dico_pos, postag_new);
      word_buffer_move_right(config_get_buffer(c));
    }
    word_buffer_move_right(config_get_buffer(c));
  }
  config_free(c);
}
#endif
