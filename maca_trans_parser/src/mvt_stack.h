#ifndef __MVT_STACK__
#define __MVT_STACK__

#include"mvt.h"

typedef struct {
  int size;
  mvt **array;
  int top;
} mvt_stack;


#define mvt_stack_nbelem(s)   ((s)->top) 
#define mvt_stack_elt_n(s, n) ((s)->array[(s)->top - (n) - 1])
#define mvt_stack_pop(s)      (((s)->top == 0)? NULL : (s)->array[--((s)->top)])
#define mvt_stack_is_empty(s) (((s)->top == 0) ? 1 : 0)
#define mvt_stack_top(s)      (((s)->top > 0)? (s)->array[(s)->top - 1] : NULL)
#define mvt_stack_0(s)        (((s)->top > 0)? (s)->array[(s)->top - 1] : NULL)
#define mvt_stack_1(s)        (((s)->top > 1)? (s)->array[(s)->top - 2] : NULL)
#define mvt_stack_2(s)        (((s)->top > 2)? (s)->array[(s)->top - 3] : NULL)
#define mvt_stack_3(s)        (((s)->top > 3)? (s)->array[(s)->top - 4] : NULL)
#define mvt_stack_4(s)        (((s)->top > 4)? (s)->array[(s)->top - 5] : NULL)
#define mvt_stack_5(s)        (((s)->top > 5)? (s)->array[(s)->top - 6] : NULL)
#define mvt_stack_i(s,i)      (((s)->top > (i)) ? (s)->array[(s)->top - (i+1)] : NULL)


mvt_stack *mvt_stack_new(void);
void       mvt_stack_push(mvt_stack *ms, mvt *m);
void       mvt_stack_print(FILE *f, mvt_stack *ms);
void       mvt_stack_free(mvt_stack *ms);

#endif
