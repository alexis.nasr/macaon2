#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<getopt.h>
#include"movement_parser_arc_eager.h"
#include"oracle_parser_arc_eager.h"
#include"feat_fct.h"
#include"context.h"
#include"feat_vec.h"
#include"dico_vec.h"
#include"word_emb.h"
#include"config2feat_vec.h"

void maca_trans_parser_mcf2cff_help_message(context *ctx)
{
  context_general_help_message(ctx);
  context_mode_help_message(ctx);
  context_sent_nb_help_message(ctx);

  fprintf(stderr, "INPUT\n");
  context_conll_help_message(ctx);
  fprintf(stderr, "IN TEST MODE\n");
  context_vocabs_help_message(ctx);

  fprintf(stderr, "OUTPUT\n");
  context_cff_help_message(ctx);
  fprintf(stderr, "IN TRAIN MODE\n");
  context_vocabs_help_message(ctx);

}

void maca_trans_parser_mcf2cff_check_options(context *ctx)
{
  if(!ctx->input_filename
     || ctx->help
     /* || !ctx->mcd_filename */
     /* || !(ctx->cff_filename || ctx->fann_filename) */
     ){
    maca_trans_parser_mcf2cff_help_message(ctx);
    exit(1);
  }
}

void generate_training_file(FILE *output_file, context *ctx)
{
  config *c;
  int mvt_code;
  char mvt_type;
  int mvt_label;
  feat_vec *fv = feat_vec_new(feature_types_nb);
  int sentence_nb = 0;
  int root_label = dico_string2int(ctx->dico_labels, (char *) ctx->root_label);
  word_buffer *ref = word_buffer_load_mcf(ctx->input_filename, ctx->mcd_struct);
  FILE *mcf_file = myfopen(ctx->input_filename, "r");
  
  /* create an mcd that corresponds to ctx->mcd_struct, but without gov and label */
  /* the idea is to ignore syntax in the mcf file that will be read */
  /* it is ugly !!! */
  
  mcd *mcd_struct_hyp = mcd_copy(ctx->mcd_struct);
  mcd_remove_wf_column(mcd_struct_hyp, MCD_WF_GOV);
  mcd_remove_wf_column(mcd_struct_hyp, MCD_WF_LABEL);
  mcd_remove_wf_column(mcd_struct_hyp, MCD_WF_SENT_SEG);
  
  c = config_new(mcf_file, mcd_struct_hyp, 5);
  
  while(!word_buffer_end(ref) && (sentence_nb < ctx->sent_nb)){
    mvt_code = oracle_parser_arc_eager(c, ref, root_label);
    mvt_type = movement_parser_type(mvt_code);
    mvt_label = movement_parser_label(mvt_code);
    
    if(ctx->debug_mode){
      config_print(stdout,c);           
      movement_parser_print(stdout, mvt_code, ctx->dico_labels);        
    }

    if(ctx->trace_mode){
      fprintf(output_file, "%d\t", word_get_index(word_buffer_b0(config_get_buffer(c))));
      stack_print(output_file, c->st);
      fprintf(output_file, "\t");
      
      movement_parser_print(output_file, mvt_code, ctx->dico_labels);        
      fprintf(output_file, "\t1\n");
    }
    else{
      fprintf(output_file, "%d", mvt_code);
      config2feat_vec_cff(ctx->features_model, c, ctx->d_perceptron_features, fv, ctx->mode);
      feat_vec_print(output_file, fv);
    }

    switch(mvt_type){
    case MVT_PARSER_EOS :
      movement_parser_eos(c);
      /* if(word_buffer_is_last(ref)) */
      break;
    case MVT_PARSER_LEFT :
      movement_parser_left_arc(c, mvt_label);
      break;    
    case MVT_PARSER_RIGHT :
      movement_parser_right_arc(c, mvt_label);
      word_buffer_move_right(ref);
      break;    
    case MVT_PARSER_REDUCE :
      movement_parser_reduce(c);
      break;   
    case MVT_PARSER_ROOT :
      sentence_nb++;
      if((sentence_nb % 100) == 0)
	fprintf(stderr, "\rsentence %d", sentence_nb);
      movement_parser_root(c, root_label);
      break;
    case MVT_PARSER_SHIFT :
      movement_parser_shift(c);
      word_buffer_move_right(ref);
      break;
    }
  }
  fprintf(stderr, "\n");
}

int main(int argc, char *argv[])
{
  context *ctx;
  FILE *output_file;
  
  ctx = context_read_options(argc, argv);
  maca_trans_parser_mcf2cff_check_options(ctx);

  ctx->features_model = feat_model_read(ctx->features_model_filename, feat_lib_build(), ctx->verbose);
  
  if(ctx->mode == TRAIN_MODE){
    mcd_extract_dico_from_corpus(ctx->mcd_struct, ctx->input_filename);
    ctx->vocabs = mcd_build_dico_vec(ctx->mcd_struct);
  }
  else if(ctx->mode == TEST_MODE){
    ctx->vocabs = dico_vec_read(ctx->vocabs_filename, ctx->hash_ratio);
    mcd_link_to_dico(ctx->mcd_struct, ctx->vocabs, ctx->verbose);
  }

  ctx->dico_labels = dico_vec_get_dico(ctx->vocabs, (char *)"LABEL");
  
  if(ctx->dico_labels == NULL){
    fprintf(stderr, "cannot find label names\n");
    return 1;
  }
  ctx->mvt_nb = ctx->dico_labels->nbelem * 2 + 3;
    
  feat_model_compute_ranges(ctx->features_model, ctx->mcd_struct, ctx->mvt_nb);

  
  /* in train mode create feature dictionnary for perceptron */
  if(ctx->mode == TRAIN_MODE)
    ctx->d_perceptron_features = dico_new((char *)"d_perceptron_features", 10000000);
  
  /* in test mode read feature dictionnary for perceptron */
  if(ctx->mode == TEST_MODE)
    ctx->d_perceptron_features = dico_vec_get_dico(ctx->vocabs, (char *)"d_perceptron_features");
  
  /* add the feature dictionnary to the dico vector */
  dico_vec_add(ctx->vocabs, ctx->d_perceptron_features);
  
  /* open output file */
  output_file = (ctx->cff_filename) ? myfopen_no_exit(ctx->cff_filename, "w") : stdout;
  
  generate_training_file(output_file, ctx);

  if(ctx->mode == TRAIN_MODE)
    dico_vec_print(ctx->vocabs_filename, ctx->vocabs);
  
  if(ctx->cff_filename)
    fclose(output_file);
  context_free(ctx);
  return 0;
}

