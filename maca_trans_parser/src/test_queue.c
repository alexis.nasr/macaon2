#include<stdio.h>
#include"queue.h"

int main(void)
{
  int i;
  queue *q = queue_new(5);

  printf("add 0\n");
  queue_add(q, 0);
  queue_print(stdout, q); 

  printf("add 1\n");

  queue_add(q, 1);
  queue_print(stdout, q); 

  printf("add 2\n");
  queue_add(q, 2);
  queue_print(stdout, q); 

  printf("add in front -1\n");
  queue_add_in_front(q, -1);
  queue_print(stdout, q); 

  for(i=0; i <=5; i++)
    printf("elt %d  = %d\n", i, queue_elt_n(q,i));



  printf("remove %d\n", queue_remove(q));
  queue_print(stdout, q); 
  printf("remove %d\n", queue_remove(q));
  queue_print(stdout, q); 
  printf("remove %d\n", queue_remove(q));
  queue_print(stdout, q); 

  printf("add 4\n");
  queue_add(q, 4);
  queue_print(stdout, q); 

  printf("add 5\n");
  queue_add(q, 5);
  queue_print(stdout, q); 

  printf("add 6\n");
  queue_add(q, 6);
  queue_print(stdout, q); 

  printf("add 7\n");
  queue_add(q, 7);
  queue_print(stdout, q); 


  for(i=0; i <=5; i++)
    printf("elt %d  = %d\n", i, queue_elt_n(q,i));
 



}
