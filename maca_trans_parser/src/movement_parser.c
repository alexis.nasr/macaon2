#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"util.h"
#include"movement_parser.h"

void movement_print(FILE *f, int mvt_code, dico *dico_labels){
  int mvt_type = movement_type(mvt_code);
  int mvt_label = movement_label(mvt_code);
  char *label;
  if(mvt_type == MVT_SHIFT) fprintf(f, "SHIFT");
  else{
    if(mvt_type == MVT_RIGHT) fprintf(f, "RIGHT");
    else fprintf(f, "LEFT");
    label = dico_int2string(dico_labels, mvt_label);
    fprintf(f, " %s", label);
  }
  fprintf(f, "\n");
}

int movement_type(int mvt)
{
  if(mvt == 0) return MVT_SHIFT;     /* 0  is the code of shift */
  if(mvt % 2 == 0) return MVT_RIGHT;  /* even movements are right movements */
  return MVT_LEFT;                  /* odd movements are left movements */
}

int movement_label(int mvt)
{
  if(mvt == 0) return -1;     /* 0  is the code of shift */ 
  if(mvt % 2 == 0)            /* even codes correspond to right movements */
    return (mvt - 2) / 2;
  return (mvt - 1) / 2;       /* odd codes correspond to left movements */
}

int movement_left_arc(config *c, int label, float score)
{
  if(stack_is_empty(c->st)) return 0;
  if(word_buffer_is_empty(c->bf)) return 0;
  if(word_get_index(stack_top(c->st)) == 0) return 0; /* the dummy word cannot be a dependent */
  
  /* create a new dependency */
  word_set_gov(stack_top(c->st), word_get_index(word_buffer_b0(c->bf)));
  word_set_label(stack_top(c->st), label);

  /* depset_add(c->ds, word_buffer_b0(c->bf), label, stack_top(c->st)); */
  stack_pop(c->st);
  
  config_add_mvt(c, movement_left_code(label));
  return 1;
}

int movement_right_arc(config *c, int label, float score)
{
 /* printf("RA "); */
  if(stack_is_empty(c->st)) return 0;
  if(word_buffer_is_empty(c->bf)) return 0;


  /* create a new dependency */
  word_set_gov(word_buffer_b0(c->bf), word_get_index(stack_top(c->st)));
  word_set_label(word_buffer_b0(c->bf), label);
  
  /* depset_add(c->ds, stack_top(c->st), label, word_buffer_b0(c->bf));  */
  stack_push(c->st, word_buffer_b0(c->bf));
  word_buffer_move_right(c->bf);
  
  config_add_mvt(c, movement_right_code(label));
  return 1;
}


int movement_shift(config *c, int stream, float score)
{
  if(word_buffer_is_empty(c->bf)) return 0;
  /* printf("SH\n"); */

  stack_push(c->st, word_buffer_b0(c->bf));
  word_buffer_move_right(c->bf);
  printf("JE FAIS UN TEST ICI :\n");
  config_add_mvt(c, MVT_SHIFT);
  printf("TEST TERMINÉ :\n\n");
  return 1;
}

int movement_reduce(config *c, float score)
{
  if(stack_is_empty(c->st)) return 0;
  if(word_get_gov(stack_top(c->st)) == -1) return 0; /* word on top of stack does not have a governor */
  stack_pop(c->st);
  return 1;
}

#if 0

config *movement_left_arc_dup(config *c, int label, float score, feat_vec *fv)
{
  config *copy = NULL;
  if(stack_is_empty(c->st)) return NULL;
  if(word_buffer_is_empty(c->bf)) return NULL;
  if(word_get_index(stack_top(c->st)) == 0) return NULL;

  copy = config_copy(c);
  depset_add(copy->ds, word_buffer_b0(copy->bf), label, stack_top(copy->st));

  stack_pop(copy->st);
  copy->score = score;
  config_add_mvt(copy, movement_left_code(label));

  if(fv) global_feat_vec_add(copy->gfv, movement_left_code(label), feat_vec_copy(fv));
  
  return copy;
}

config *movement_shift_dup(config *c, int stream, float score, feat_vec *fv)
{
  config *copy = NULL;
  /* fprintf(stderr, "SHIFT\n"); */
  if(queue_is_empty(c->bf)){return NULL;}
  copy = config_copy(c);
  stack_push(copy->st, queue_remove(copy->bf));
  copy->score = score;
  config_add_mvt(copy, MVT_SHIFT);

  /* in stream mode, read a new word and add it to the buffer */
  if(stream)
     config_add_next_word_to_buffer(c); 

  if(fv) global_feat_vec_add(copy->gfv, MVT_SHIFT, feat_vec_copy(fv));

  return copy;
}

config *movement_right_arc_dup(config *c, int label, float score, feat_vec *fv)
{
  config *copy = NULL;
  /* fprintf(stderr, "RIGHT ARC\n"); */
  if(stack_is_empty(c->st)) return NULL;
  if(queue_is_empty(c->bf)) return NULL;

  copy = config_copy(c);
  depset_add(copy->ds, stack_top(copy->st), label, queue_elt_n(copy->bf, 0)); 
  queue_remove(copy->bf);
  queue_add_in_front(copy->bf, stack_pop(copy->st));
  copy->score = score;
  config_add_mvt(copy, movement_right_code(label));
  if(fv) global_feat_vec_add(copy->gfv, movement_right_code(label), feat_vec_copy(fv));
  return copy;
}
#endif
