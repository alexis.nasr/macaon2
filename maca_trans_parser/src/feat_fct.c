#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"word_buffer.h"
#include"stack.h"
#include"feat_fct.h"
#include"feat_types.h"
#include"config.h"


/* word features */

/* words in the stack */
int s0g(void *c) {return (word_get_gov(stack_s0(config_get_stack((config *) c))) == WORD_INVALID_GOV) ? 0 : 1;}
int s0sf(void *c) {return word_get_label(stack_s0(config_get_stack((config *) c)));}

int s0seg(void *c) {return word_get_sent_seg(stack_s0(config_get_stack((config *) c)));}
int s0f(void *c) {return word_get_form(stack_s0(config_get_stack((config *) c)));}
int s0l(void *c) {return word_get_lemma(stack_s0(config_get_stack((config *) c)));}
int s0c(void *c) {return word_get_cpos(stack_s0(config_get_stack((config *) c)));}
int s0p(void *c) {return word_get_pos(stack_s0(config_get_stack((config *) c)));}
int s0m(void *c) {return word_get_feats(stack_s0(config_get_stack((config *) c)));}
int s0s(void *c) {return word_get_stag(stack_s0(config_get_stack((config *) c)));}
int s0A(void *c) {return word_get_A(stack_s0(config_get_stack((config *) c)));}
int s0B(void *c) {return word_get_B(stack_s0(config_get_stack((config *) c)));}
int s0C(void *c) {return word_get_C(stack_s0(config_get_stack((config *) c)));}
int s0D(void *c) {return word_get_D(stack_s0(config_get_stack((config *) c)));}
int s0E(void *c) {return word_get_E(stack_s0(config_get_stack((config *) c)));}
int s0F(void *c) {return word_get_F(stack_s0(config_get_stack((config *) c)));}
int s0G(void *c) {return word_get_G(stack_s0(config_get_stack((config *) c)));}
int s0H(void *c) {return word_get_H(stack_s0(config_get_stack((config *) c)));}
int s0I(void *c) {return word_get_I(stack_s0(config_get_stack((config *) c)));}
int s0J(void *c) {return word_get_J(stack_s0(config_get_stack((config *) c)));}
int s0K(void *c) {return word_get_K(stack_s0(config_get_stack((config *) c)));}
int s0L(void *c) {return word_get_L(stack_s0(config_get_stack((config *) c)));}
int s0M(void *c) {return word_get_M(stack_s0(config_get_stack((config *) c)));}
int s0N(void *c) {return word_get_N(stack_s0(config_get_stack((config *) c)));}
int s0O(void *c) {return word_get_O(stack_s0(config_get_stack((config *) c)));}
int s0P(void *c) {return word_get_P(stack_s0(config_get_stack((config *) c)));}
int s0Q(void *c) {return word_get_Q(stack_s0(config_get_stack((config *) c)));}
int s0R(void *c) {return word_get_R(stack_s0(config_get_stack((config *) c)));}
int s0S(void *c) {return word_get_S(stack_s0(config_get_stack((config *) c)));}
int s0T(void *c) {return word_get_T(stack_s0(config_get_stack((config *) c)));}
int s0U(void *c) {return word_get_U(stack_s0(config_get_stack((config *) c)));}
int s0V(void *c) {return word_get_V(stack_s0(config_get_stack((config *) c)));}
int s0W(void *c) {return word_get_W(stack_s0(config_get_stack((config *) c)));}
int s0X(void *c) {return word_get_X(stack_s0(config_get_stack((config *) c)));}
int s0Y(void *c) {return word_get_Y(stack_s0(config_get_stack((config *) c)));}
int s0Z(void *c) {return word_get_Z(stack_s0(config_get_stack((config *) c)));}

int s0U1(void *c) {return word_get_U1(stack_s0(config_get_stack((config *) c)));}
int s0sgn(void *c) {return word_get_signature(stack_s0(config_get_stack((config *) c)));}

int s1g(void *c)  {return (word_get_gov(stack_s1(config_get_stack((config *) c))) == WORD_INVALID_GOV) ? 0 : 1;}
int s1sf(void *c) {return word_get_label(stack_s1(config_get_stack((config *) c)));}

int s1f(void *c) {return word_get_form(stack_s1(config_get_stack((config *) c)));}
int s1l(void *c) {return word_get_lemma(stack_s1(config_get_stack((config *) c)));}
int s1c(void *c) {return word_get_cpos(stack_s1(config_get_stack((config *) c)));}
int s1p(void *c) {return word_get_pos(stack_s1(config_get_stack((config *) c)));}
int s1m(void *c) {return word_get_feats(stack_s1(config_get_stack((config *) c)));}
int s1s(void *c) {return word_get_stag(stack_s1(config_get_stack((config *) c)));}
int s1A(void *c) {return word_get_A(stack_s1(config_get_stack((config *) c)));}
int s1B(void *c) {return word_get_B(stack_s1(config_get_stack((config *) c)));}
int s1C(void *c) {return word_get_C(stack_s1(config_get_stack((config *) c)));}
int s1D(void *c) {return word_get_D(stack_s1(config_get_stack((config *) c)));}
int s1E(void *c) {return word_get_E(stack_s1(config_get_stack((config *) c)));}
int s1F(void *c) {return word_get_F(stack_s1(config_get_stack((config *) c)));}
int s1G(void *c) {return word_get_G(stack_s1(config_get_stack((config *) c)));}
int s1H(void *c) {return word_get_H(stack_s1(config_get_stack((config *) c)));}
int s1I(void *c) {return word_get_I(stack_s1(config_get_stack((config *) c)));}
int s1J(void *c) {return word_get_J(stack_s1(config_get_stack((config *) c)));}
int s1K(void *c) {return word_get_K(stack_s1(config_get_stack((config *) c)));}
int s1L(void *c) {return word_get_L(stack_s1(config_get_stack((config *) c)));}
int s1M(void *c) {return word_get_M(stack_s1(config_get_stack((config *) c)));}
int s1N(void *c) {return word_get_N(stack_s1(config_get_stack((config *) c)));}
int s1O(void *c) {return word_get_O(stack_s1(config_get_stack((config *) c)));}
int s1P(void *c) {return word_get_P(stack_s1(config_get_stack((config *) c)));}
int s1Q(void *c) {return word_get_Q(stack_s1(config_get_stack((config *) c)));}
int s1R(void *c) {return word_get_R(stack_s1(config_get_stack((config *) c)));}
int s1S(void *c) {return word_get_S(stack_s1(config_get_stack((config *) c)));}
int s1T(void *c) {return word_get_T(stack_s1(config_get_stack((config *) c)));}
int s1U(void *c) {return word_get_U(stack_s1(config_get_stack((config *) c)));}
int s1V(void *c) {return word_get_V(stack_s1(config_get_stack((config *) c)));}
int s1W(void *c) {return word_get_W(stack_s1(config_get_stack((config *) c)));}
int s1X(void *c) {return word_get_X(stack_s1(config_get_stack((config *) c)));}
int s1Y(void *c) {return word_get_Y(stack_s1(config_get_stack((config *) c)));}
int s1Z(void *c) {return word_get_Z(stack_s1(config_get_stack((config *) c)));}

int s2g(void *c)  {return (word_get_gov(stack_s2(config_get_stack((config *) c))) == WORD_INVALID_GOV) ? 0 : 1;}
int s2sf(void *c) {return word_get_label(stack_s2(config_get_stack((config *) c)));}

int s2f(void *c) {return word_get_form(stack_s2(config_get_stack((config *) c)));}
int s2l(void *c) {return word_get_lemma(stack_s2(config_get_stack((config *) c)));}
int s2c(void *c) {return word_get_cpos(stack_s2(config_get_stack((config *) c)));}
int s2p(void *c) {return word_get_pos(stack_s2(config_get_stack((config *) c)));}
int s2m(void *c) {return word_get_feats(stack_s2(config_get_stack((config *) c)));}
int s2s(void *c) {return word_get_stag(stack_s2(config_get_stack((config *) c)));}
int s2A(void *c) {return word_get_A(stack_s2(config_get_stack((config *) c)));}
int s2B(void *c) {return word_get_B(stack_s2(config_get_stack((config *) c)));}
int s2C(void *c) {return word_get_C(stack_s2(config_get_stack((config *) c)));}
int s2D(void *c) {return word_get_D(stack_s2(config_get_stack((config *) c)));}
int s2E(void *c) {return word_get_E(stack_s2(config_get_stack((config *) c)));}
int s2F(void *c) {return word_get_F(stack_s2(config_get_stack((config *) c)));}
int s2G(void *c) {return word_get_G(stack_s2(config_get_stack((config *) c)));}
int s2H(void *c) {return word_get_H(stack_s2(config_get_stack((config *) c)));}
int s2I(void *c) {return word_get_I(stack_s2(config_get_stack((config *) c)));}
int s2J(void *c) {return word_get_J(stack_s2(config_get_stack((config *) c)));}
int s2K(void *c) {return word_get_K(stack_s2(config_get_stack((config *) c)));}
int s2L(void *c) {return word_get_L(stack_s2(config_get_stack((config *) c)));}
int s2M(void *c) {return word_get_M(stack_s2(config_get_stack((config *) c)));}
int s2N(void *c) {return word_get_N(stack_s2(config_get_stack((config *) c)));}
int s2O(void *c) {return word_get_O(stack_s2(config_get_stack((config *) c)));}
int s2P(void *c) {return word_get_P(stack_s2(config_get_stack((config *) c)));}
int s2Q(void *c) {return word_get_Q(stack_s2(config_get_stack((config *) c)));}
int s2R(void *c) {return word_get_R(stack_s2(config_get_stack((config *) c)));}
int s2S(void *c) {return word_get_S(stack_s2(config_get_stack((config *) c)));}
int s2T(void *c) {return word_get_T(stack_s2(config_get_stack((config *) c)));}
int s2U(void *c) {return word_get_U(stack_s2(config_get_stack((config *) c)));}
int s2V(void *c) {return word_get_V(stack_s2(config_get_stack((config *) c)));}
int s2W(void *c) {return word_get_W(stack_s2(config_get_stack((config *) c)));}
int s2X(void *c) {return word_get_X(stack_s2(config_get_stack((config *) c)));}
int s2Y(void *c) {return word_get_Y(stack_s2(config_get_stack((config *) c)));}
int s2Z(void *c) {return word_get_Z(stack_s2(config_get_stack((config *) c)));}

int s3f(void *c) {return word_get_form(stack_s3(config_get_stack((config *) c)));}
int s3l(void *c) {return word_get_lemma(stack_s3(config_get_stack((config *) c)));}
int s3c(void *c) {return word_get_cpos(stack_s3(config_get_stack((config *) c)));}
int s3p(void *c) {return word_get_pos(stack_s3(config_get_stack((config *) c)));}
int s3m(void *c) {return word_get_feats(stack_s3(config_get_stack((config *) c)));}
int s3s(void *c) {return word_get_stag(stack_s3(config_get_stack((config *) c)));}
int s3A(void *c) {return word_get_A(stack_s3(config_get_stack((config *) c)));}
int s3B(void *c) {return word_get_B(stack_s3(config_get_stack((config *) c)));}
int s3C(void *c) {return word_get_C(stack_s3(config_get_stack((config *) c)));}
int s3D(void *c) {return word_get_D(stack_s3(config_get_stack((config *) c)));}
int s3E(void *c) {return word_get_E(stack_s3(config_get_stack((config *) c)));}
int s3F(void *c) {return word_get_F(stack_s3(config_get_stack((config *) c)));}
int s3G(void *c) {return word_get_G(stack_s3(config_get_stack((config *) c)));}
int s3H(void *c) {return word_get_H(stack_s3(config_get_stack((config *) c)));}
int s3I(void *c) {return word_get_I(stack_s3(config_get_stack((config *) c)));}
int s3J(void *c) {return word_get_J(stack_s3(config_get_stack((config *) c)));}
int s3K(void *c) {return word_get_K(stack_s3(config_get_stack((config *) c)));}
int s3L(void *c) {return word_get_L(stack_s3(config_get_stack((config *) c)));}
int s3M(void *c) {return word_get_M(stack_s3(config_get_stack((config *) c)));}
int s3N(void *c) {return word_get_N(stack_s3(config_get_stack((config *) c)));}
int s3O(void *c) {return word_get_O(stack_s3(config_get_stack((config *) c)));}
int s3P(void *c) {return word_get_P(stack_s3(config_get_stack((config *) c)));}
int s3Q(void *c) {return word_get_Q(stack_s3(config_get_stack((config *) c)));}
int s3R(void *c) {return word_get_R(stack_s3(config_get_stack((config *) c)));}
int s3S(void *c) {return word_get_S(stack_s3(config_get_stack((config *) c)));}
int s3T(void *c) {return word_get_T(stack_s3(config_get_stack((config *) c)));}
int s3U(void *c) {return word_get_U(stack_s3(config_get_stack((config *) c)));}
int s3V(void *c) {return word_get_V(stack_s3(config_get_stack((config *) c)));}
int s3W(void *c) {return word_get_W(stack_s3(config_get_stack((config *) c)));}
int s3X(void *c) {return word_get_X(stack_s3(config_get_stack((config *) c)));}
int s3Y(void *c) {return word_get_Y(stack_s3(config_get_stack((config *) c)));}
int s3Z(void *c) {return word_get_Z(stack_s3(config_get_stack((config *) c)));}

/* words in the buffer */

int b0s1(void *c){return word_get_s1(word_buffer_b0(config_get_buffer((config *) c)));}
int b0s2(void *c){return word_get_s2(word_buffer_b0(config_get_buffer((config *) c)));}
int b0s3(void *c){return word_get_s3(word_buffer_b0(config_get_buffer((config *) c)));}
int b0s4(void *c){return word_get_s4(word_buffer_b0(config_get_buffer((config *) c)));}
int b0s5(void *c){return word_get_s5(word_buffer_b0(config_get_buffer((config *) c)));}
int b0s6(void *c){return word_get_s6(word_buffer_b0(config_get_buffer((config *) c)));}

int b0p1(void *c){return word_get_p1(word_buffer_b0(config_get_buffer((config *) c)));}
int b0p2(void *c){return word_get_p2(word_buffer_b0(config_get_buffer((config *) c)));}
int b0p3(void *c){return word_get_p3(word_buffer_b0(config_get_buffer((config *) c)));}
int b0p4(void *c){return word_get_p4(word_buffer_b0(config_get_buffer((config *) c)));}
int b0p5(void *c){return word_get_p5(word_buffer_b0(config_get_buffer((config *) c)));}
int b0p6(void *c){return word_get_p6(word_buffer_b0(config_get_buffer((config *) c)));}

int b0g(void *c) {return (word_get_gov(word_buffer_b0(config_get_buffer((config *) c))) == WORD_INVALID_GOV) ? 0 : 1;}
int b0sf(void *c) {return word_get_label(word_buffer_b0(config_get_buffer((config *) c)));}


int b0len(void *c) {
  int len = 0;
  word *w = word_buffer_b0(config_get_buffer((config *) c));
  if(w && w->form)
    len = strlen(w->form);
  return (len > 7)? 7 : len;
}



int b0f(void *c) {return word_get_form(word_buffer_b0(config_get_buffer((config *) c)));}
int b0l(void *c) {return word_get_lemma(word_buffer_b0(config_get_buffer((config *) c)));}
int b0c(void *c) {return word_get_cpos(word_buffer_b0(config_get_buffer((config *) c)));}
int b0p(void *c) {return word_get_pos(word_buffer_b0(config_get_buffer((config *) c)));}
int b0m(void *c) {return word_get_feats(word_buffer_b0(config_get_buffer((config *) c)));}
int b0s(void *c) {return word_get_stag(word_buffer_b0(config_get_buffer((config *) c)));}
int b0A(void *c) {return word_get_A(word_buffer_b0(config_get_buffer((config *) c)));}
int b0B(void *c) {return word_get_B(word_buffer_b0(config_get_buffer((config *) c)));}
int b0C(void *c) {return word_get_C(word_buffer_b0(config_get_buffer((config *) c)));}
int b0D(void *c) {return word_get_D(word_buffer_b0(config_get_buffer((config *) c)));}
int b0E(void *c) {return word_get_E(word_buffer_b0(config_get_buffer((config *) c)));}
int b0F(void *c) {return word_get_F(word_buffer_b0(config_get_buffer((config *) c)));}
int b0G(void *c) {return word_get_G(word_buffer_b0(config_get_buffer((config *) c)));}
int b0H(void *c) {return word_get_H(word_buffer_b0(config_get_buffer((config *) c)));}
int b0I(void *c) {return word_get_I(word_buffer_b0(config_get_buffer((config *) c)));}
int b0J(void *c) {return word_get_J(word_buffer_b0(config_get_buffer((config *) c)));}
int b0K(void *c) {return word_get_K(word_buffer_b0(config_get_buffer((config *) c)));}
int b0L(void *c) {return word_get_L(word_buffer_b0(config_get_buffer((config *) c)));}
int b0M(void *c) {return word_get_M(word_buffer_b0(config_get_buffer((config *) c)));}
int b0N(void *c) {return word_get_N(word_buffer_b0(config_get_buffer((config *) c)));}
int b0O(void *c) {return word_get_O(word_buffer_b0(config_get_buffer((config *) c)));}
int b0P(void *c) {return word_get_P(word_buffer_b0(config_get_buffer((config *) c)));}
int b0Q(void *c) {return word_get_Q(word_buffer_b0(config_get_buffer((config *) c)));}
int b0R(void *c) {return word_get_R(word_buffer_b0(config_get_buffer((config *) c)));}
int b0S(void *c) {return word_get_S(word_buffer_b0(config_get_buffer((config *) c)));}
int b0T(void *c) {return word_get_T(word_buffer_b0(config_get_buffer((config *) c)));}
int b0U(void *c) {return word_get_U(word_buffer_b0(config_get_buffer((config *) c)));}
int b0V(void *c) {return word_get_V(word_buffer_b0(config_get_buffer((config *) c)));}
int b0W(void *c) {return word_get_W(word_buffer_b0(config_get_buffer((config *) c)));}
int b0X(void *c) {return word_get_X(word_buffer_b0(config_get_buffer((config *) c)));}
int b0Y(void *c) {return word_get_Y(word_buffer_b0(config_get_buffer((config *) c)));}
int b0Z(void *c) {return word_get_Z(word_buffer_b0(config_get_buffer((config *) c)));}

int b0U1(void *c) {return word_get_U1(word_buffer_b0(config_get_buffer((config *) c)));}
int b0sgn(void *c) {return word_get_signature(word_buffer_b0(config_get_buffer((config *) c)));}

int b1f(void *c) {return word_get_form(word_buffer_b1(config_get_buffer((config *) c)));}
int b1l(void *c) {return word_get_lemma(word_buffer_b1(config_get_buffer((config *) c)));}
int b1c(void *c) {return word_get_cpos(word_buffer_b1(config_get_buffer((config *) c)));}
int b1p(void *c) {return word_get_pos(word_buffer_b1(config_get_buffer((config *) c)));}
int b1m(void *c) {return word_get_feats(word_buffer_b1(config_get_buffer((config *) c)));}
int b1s(void *c) {return word_get_stag(word_buffer_b1(config_get_buffer((config *) c)));}
int b1A(void *c) {return word_get_A(word_buffer_b1(config_get_buffer((config *) c)));}
int b1B(void *c) {return word_get_B(word_buffer_b1(config_get_buffer((config *) c)));}
int b1C(void *c) {return word_get_C(word_buffer_b1(config_get_buffer((config *) c)));}
int b1D(void *c) {return word_get_D(word_buffer_b1(config_get_buffer((config *) c)));}
int b1E(void *c) {return word_get_E(word_buffer_b1(config_get_buffer((config *) c)));}
int b1F(void *c) {return word_get_F(word_buffer_b1(config_get_buffer((config *) c)));}
int b1G(void *c) {return word_get_G(word_buffer_b1(config_get_buffer((config *) c)));}
int b1H(void *c) {return word_get_H(word_buffer_b1(config_get_buffer((config *) c)));}
int b1I(void *c) {return word_get_I(word_buffer_b1(config_get_buffer((config *) c)));}
int b1J(void *c) {return word_get_J(word_buffer_b1(config_get_buffer((config *) c)));}
int b1K(void *c) {return word_get_K(word_buffer_b1(config_get_buffer((config *) c)));}
int b1L(void *c) {return word_get_L(word_buffer_b1(config_get_buffer((config *) c)));}
int b1M(void *c) {return word_get_M(word_buffer_b1(config_get_buffer((config *) c)));}
int b1N(void *c) {return word_get_N(word_buffer_b1(config_get_buffer((config *) c)));}
int b1O(void *c) {return word_get_O(word_buffer_b1(config_get_buffer((config *) c)));}
int b1P(void *c) {return word_get_P(word_buffer_b1(config_get_buffer((config *) c)));}
int b1Q(void *c) {return word_get_Q(word_buffer_b1(config_get_buffer((config *) c)));}
int b1R(void *c) {return word_get_R(word_buffer_b1(config_get_buffer((config *) c)));}
int b1S(void *c) {return word_get_S(word_buffer_b1(config_get_buffer((config *) c)));}
int b1T(void *c) {return word_get_T(word_buffer_b1(config_get_buffer((config *) c)));}
int b1U(void *c) {return word_get_U(word_buffer_b1(config_get_buffer((config *) c)));}
int b1V(void *c) {return word_get_V(word_buffer_b1(config_get_buffer((config *) c)));}
int b1W(void *c) {return word_get_W(word_buffer_b1(config_get_buffer((config *) c)));}
int b1X(void *c) {return word_get_X(word_buffer_b1(config_get_buffer((config *) c)));}
int b1Y(void *c) {return word_get_Y(word_buffer_b1(config_get_buffer((config *) c)));}
int b1Z(void *c) {return word_get_Z(word_buffer_b1(config_get_buffer((config *) c)));}

int b1U1(void *c) {return word_get_U1(word_buffer_b1(config_get_buffer((config *) c)));}
int b1sgn(void *c) {return word_get_signature(word_buffer_b1(config_get_buffer((config *) c)));}

int b2f(void *c) {return word_get_form(word_buffer_b2(config_get_buffer((config *) c)));}
int b2l(void *c) {return word_get_lemma(word_buffer_b2(config_get_buffer((config *) c)));}
int b2c(void *c) {return word_get_cpos(word_buffer_b2(config_get_buffer((config *) c)));}
int b2p(void *c) {return word_get_pos(word_buffer_b2(config_get_buffer((config *) c)));}
int b2m(void *c) {return word_get_feats(word_buffer_b2(config_get_buffer((config *) c)));}
int b2s(void *c) {return word_get_stag(word_buffer_b2(config_get_buffer((config *) c)));}
int b2A(void *c) {return word_get_A(word_buffer_b2(config_get_buffer((config *) c)));}
int b2B(void *c) {return word_get_B(word_buffer_b2(config_get_buffer((config *) c)));}
int b2C(void *c) {return word_get_C(word_buffer_b2(config_get_buffer((config *) c)));}
int b2D(void *c) {return word_get_D(word_buffer_b2(config_get_buffer((config *) c)));}
int b2E(void *c) {return word_get_E(word_buffer_b2(config_get_buffer((config *) c)));}
int b2F(void *c) {return word_get_F(word_buffer_b2(config_get_buffer((config *) c)));}
int b2G(void *c) {return word_get_G(word_buffer_b2(config_get_buffer((config *) c)));}
int b2H(void *c) {return word_get_H(word_buffer_b2(config_get_buffer((config *) c)));}
int b2I(void *c) {return word_get_I(word_buffer_b2(config_get_buffer((config *) c)));}
int b2J(void *c) {return word_get_J(word_buffer_b2(config_get_buffer((config *) c)));}
int b2K(void *c) {return word_get_K(word_buffer_b2(config_get_buffer((config *) c)));}
int b2L(void *c) {return word_get_L(word_buffer_b2(config_get_buffer((config *) c)));}
int b2M(void *c) {return word_get_M(word_buffer_b2(config_get_buffer((config *) c)));}
int b2N(void *c) {return word_get_N(word_buffer_b2(config_get_buffer((config *) c)));}
int b2O(void *c) {return word_get_O(word_buffer_b2(config_get_buffer((config *) c)));}
int b2P(void *c) {return word_get_P(word_buffer_b2(config_get_buffer((config *) c)));}
int b2Q(void *c) {return word_get_Q(word_buffer_b2(config_get_buffer((config *) c)));}
int b2R(void *c) {return word_get_R(word_buffer_b2(config_get_buffer((config *) c)));}
int b2S(void *c) {return word_get_S(word_buffer_b2(config_get_buffer((config *) c)));}
int b2T(void *c) {return word_get_T(word_buffer_b2(config_get_buffer((config *) c)));}
int b2U(void *c) {return word_get_U(word_buffer_b2(config_get_buffer((config *) c)));}
int b2V(void *c) {return word_get_V(word_buffer_b2(config_get_buffer((config *) c)));}
int b2W(void *c) {return word_get_W(word_buffer_b2(config_get_buffer((config *) c)));}
int b2X(void *c) {return word_get_X(word_buffer_b2(config_get_buffer((config *) c)));}
int b2Y(void *c) {return word_get_Y(word_buffer_b2(config_get_buffer((config *) c)));}
int b2Z(void *c) {return word_get_Z(word_buffer_b2(config_get_buffer((config *) c)));}

int b2U1(void *c) {return word_get_U1(word_buffer_b2(config_get_buffer((config *) c)));}
int b2sgn(void *c) {return word_get_signature(word_buffer_b2(config_get_buffer((config *) c)));}



int b3f(void *c) {return word_get_form(word_buffer_b3(config_get_buffer((config *) c)));}
int b3l(void *c) {return word_get_lemma(word_buffer_b3(config_get_buffer((config *) c)));}
int b3c(void *c) {return word_get_cpos(word_buffer_b3(config_get_buffer((config *) c)));}
int b3p(void *c) {return word_get_pos(word_buffer_b3(config_get_buffer((config *) c)));}
int b3m(void *c) {return word_get_feats(word_buffer_b3(config_get_buffer((config *) c)));}
int b3s(void *c) {return word_get_stag(word_buffer_b3(config_get_buffer((config *) c)));}
int b3A(void *c) {return word_get_A(word_buffer_b3(config_get_buffer((config *) c)));}
int b3B(void *c) {return word_get_B(word_buffer_b3(config_get_buffer((config *) c)));}
int b3C(void *c) {return word_get_C(word_buffer_b3(config_get_buffer((config *) c)));}
int b3D(void *c) {return word_get_D(word_buffer_b3(config_get_buffer((config *) c)));}
int b3E(void *c) {return word_get_E(word_buffer_b3(config_get_buffer((config *) c)));}
int b3F(void *c) {return word_get_F(word_buffer_b3(config_get_buffer((config *) c)));}
int b3G(void *c) {return word_get_G(word_buffer_b3(config_get_buffer((config *) c)));}
int b3H(void *c) {return word_get_H(word_buffer_b3(config_get_buffer((config *) c)));}
int b3I(void *c) {return word_get_I(word_buffer_b3(config_get_buffer((config *) c)));}
int b3J(void *c) {return word_get_J(word_buffer_b3(config_get_buffer((config *) c)));}
int b3K(void *c) {return word_get_K(word_buffer_b3(config_get_buffer((config *) c)));}
int b3L(void *c) {return word_get_L(word_buffer_b3(config_get_buffer((config *) c)));}
int b3M(void *c) {return word_get_M(word_buffer_b3(config_get_buffer((config *) c)));}
int b3N(void *c) {return word_get_N(word_buffer_b3(config_get_buffer((config *) c)));}
int b3O(void *c) {return word_get_O(word_buffer_b3(config_get_buffer((config *) c)));}
int b3P(void *c) {return word_get_P(word_buffer_b3(config_get_buffer((config *) c)));}
int b3Q(void *c) {return word_get_Q(word_buffer_b3(config_get_buffer((config *) c)));}
int b3R(void *c) {return word_get_R(word_buffer_b3(config_get_buffer((config *) c)));}
int b3S(void *c) {return word_get_S(word_buffer_b3(config_get_buffer((config *) c)));}
int b3T(void *c) {return word_get_T(word_buffer_b3(config_get_buffer((config *) c)));}
int b3U(void *c) {return word_get_U(word_buffer_b3(config_get_buffer((config *) c)));}
int b3V(void *c) {return word_get_V(word_buffer_b3(config_get_buffer((config *) c)));}
int b3W(void *c) {return word_get_W(word_buffer_b3(config_get_buffer((config *) c)));}
int b3X(void *c) {return word_get_X(word_buffer_b3(config_get_buffer((config *) c)));}
int b3Y(void *c) {return word_get_Y(word_buffer_b3(config_get_buffer((config *) c)));}
int b3Z(void *c) {return word_get_Z(word_buffer_b3(config_get_buffer((config *) c)));}

int bm1f(void *c) {return word_get_form(word_buffer_bm1(config_get_buffer((config *) c)));}
int bm1l(void *c) {return word_get_lemma(word_buffer_bm1(config_get_buffer((config *) c)));}
int bm1c(void *c) {return word_get_cpos(word_buffer_bm1(config_get_buffer((config *) c)));}
int bm1p(void *c) {return word_get_pos(word_buffer_bm1(config_get_buffer((config *) c)));}

int bmip(void *c, int i) {return word_get_pos(word_buffer_bmi(config_get_buffer((config *) c),i));}

int bm1m(void *c) {return word_get_feats(word_buffer_bm1(config_get_buffer((config *) c)));}
int bm1s(void *c) {return word_get_stag(word_buffer_bm1(config_get_buffer((config *) c)));}
int bm1A(void *c) {return word_get_A(word_buffer_bm1(config_get_buffer((config *) c)));}
int bm1B(void *c) {return word_get_B(word_buffer_bm1(config_get_buffer((config *) c)));}
int bm1C(void *c) {return word_get_C(word_buffer_bm1(config_get_buffer((config *) c)));}
int bm1D(void *c) {return word_get_D(word_buffer_bm1(config_get_buffer((config *) c)));}
int bm1E(void *c) {return word_get_E(word_buffer_bm1(config_get_buffer((config *) c)));}
int bm1F(void *c) {return word_get_F(word_buffer_bm1(config_get_buffer((config *) c)));}
int bm1G(void *c) {return word_get_G(word_buffer_bm1(config_get_buffer((config *) c)));}
int bm1H(void *c) {return word_get_H(word_buffer_bm1(config_get_buffer((config *) c)));}
int bm1I(void *c) {return word_get_I(word_buffer_bm1(config_get_buffer((config *) c)));}
int bm1J(void *c) {return word_get_J(word_buffer_bm1(config_get_buffer((config *) c)));}
int bm1K(void *c) {return word_get_K(word_buffer_bm1(config_get_buffer((config *) c)));}
int bm1L(void *c) {return word_get_L(word_buffer_bm1(config_get_buffer((config *) c)));}
int bm1M(void *c) {return word_get_M(word_buffer_bm1(config_get_buffer((config *) c)));}
int bm1N(void *c) {return word_get_N(word_buffer_bm1(config_get_buffer((config *) c)));}
int bm1O(void *c) {return word_get_O(word_buffer_bm1(config_get_buffer((config *) c)));}
int bm1P(void *c) {return word_get_P(word_buffer_bm1(config_get_buffer((config *) c)));}
int bm1Q(void *c) {return word_get_Q(word_buffer_bm1(config_get_buffer((config *) c)));}
int bm1R(void *c) {return word_get_R(word_buffer_bm1(config_get_buffer((config *) c)));}
int bm1S(void *c) {return word_get_S(word_buffer_bm1(config_get_buffer((config *) c)));}
int bm1T(void *c) {return word_get_T(word_buffer_bm1(config_get_buffer((config *) c)));}
int bm1U(void *c) {return word_get_U(word_buffer_bm1(config_get_buffer((config *) c)));}
int bm1V(void *c) {return word_get_V(word_buffer_bm1(config_get_buffer((config *) c)));}
int bm1W(void *c) {return word_get_W(word_buffer_bm1(config_get_buffer((config *) c)));}
int bm1X(void *c) {return word_get_X(word_buffer_bm1(config_get_buffer((config *) c)));}
int bm1Y(void *c) {return word_get_Y(word_buffer_bm1(config_get_buffer((config *) c)));}
int bm1Z(void *c) {return word_get_Z(word_buffer_bm1(config_get_buffer((config *) c)));}

int bm1U1(void *c) {return word_get_U1(word_buffer_bm1(config_get_buffer((config *) c)));}
int bm1sgn(void *c) {return word_get_signature(word_buffer_bm1(config_get_buffer((config *) c)));}

int bm2f(void *c) {return word_get_form(word_buffer_bm2(config_get_buffer((config *) c)));}
int bm2l(void *c) {return word_get_lemma(word_buffer_bm2(config_get_buffer((config *) c)));}
int bm2c(void *c) {return word_get_cpos(word_buffer_bm2(config_get_buffer((config *) c)));}
int bm2p(void *c) {return word_get_pos(word_buffer_bm2(config_get_buffer((config *) c)));}
int bm2m(void *c) {return word_get_feats(word_buffer_bm2(config_get_buffer((config *) c)));}
int bm2s(void *c) {return word_get_stag(word_buffer_bm2(config_get_buffer((config *) c)));}
int bm2A(void *c) {return word_get_A(word_buffer_bm2(config_get_buffer((config *) c)));}
int bm2B(void *c) {return word_get_B(word_buffer_bm2(config_get_buffer((config *) c)));}
int bm2C(void *c) {return word_get_C(word_buffer_bm2(config_get_buffer((config *) c)));}
int bm2D(void *c) {return word_get_D(word_buffer_bm2(config_get_buffer((config *) c)));}
int bm2E(void *c) {return word_get_E(word_buffer_bm2(config_get_buffer((config *) c)));}
int bm2F(void *c) {return word_get_F(word_buffer_bm2(config_get_buffer((config *) c)));}
int bm2G(void *c) {return word_get_G(word_buffer_bm2(config_get_buffer((config *) c)));}
int bm2H(void *c) {return word_get_H(word_buffer_bm2(config_get_buffer((config *) c)));}
int bm2I(void *c) {return word_get_I(word_buffer_bm2(config_get_buffer((config *) c)));}
int bm2J(void *c) {return word_get_J(word_buffer_bm2(config_get_buffer((config *) c)));}
int bm2K(void *c) {return word_get_K(word_buffer_bm2(config_get_buffer((config *) c)));}
int bm2L(void *c) {return word_get_L(word_buffer_bm2(config_get_buffer((config *) c)));}
int bm2M(void *c) {return word_get_M(word_buffer_bm2(config_get_buffer((config *) c)));}
int bm2N(void *c) {return word_get_N(word_buffer_bm2(config_get_buffer((config *) c)));}
int bm2O(void *c) {return word_get_O(word_buffer_bm2(config_get_buffer((config *) c)));}
int bm2P(void *c) {return word_get_P(word_buffer_bm2(config_get_buffer((config *) c)));}
int bm2Q(void *c) {return word_get_Q(word_buffer_bm2(config_get_buffer((config *) c)));}
int bm2R(void *c) {return word_get_R(word_buffer_bm2(config_get_buffer((config *) c)));}
int bm2S(void *c) {return word_get_S(word_buffer_bm2(config_get_buffer((config *) c)));}
int bm2T(void *c) {return word_get_T(word_buffer_bm2(config_get_buffer((config *) c)));}
int bm2U(void *c) {return word_get_U(word_buffer_bm2(config_get_buffer((config *) c)));}
int bm2V(void *c) {return word_get_V(word_buffer_bm2(config_get_buffer((config *) c)));}
int bm2W(void *c) {return word_get_W(word_buffer_bm2(config_get_buffer((config *) c)));}
int bm2X(void *c) {return word_get_X(word_buffer_bm2(config_get_buffer((config *) c)));}
int bm2Y(void *c) {return word_get_Y(word_buffer_bm2(config_get_buffer((config *) c)));}
int bm2Z(void *c) {return word_get_Z(word_buffer_bm2(config_get_buffer((config *) c)));}

int bm3f(void *c) {return word_get_form(word_buffer_bm3(config_get_buffer((config *) c)));}
int bm3l(void *c) {return word_get_lemma(word_buffer_bm3(config_get_buffer((config *) c)));}
int bm3c(void *c) {return word_get_cpos(word_buffer_bm3(config_get_buffer((config *) c)));}
int bm3p(void *c) {return word_get_pos(word_buffer_bm3(config_get_buffer((config *) c)));}
int bm3m(void *c) {return word_get_feats(word_buffer_bm3(config_get_buffer((config *) c)));}
int bm3s(void *c) {return word_get_stag(word_buffer_bm3(config_get_buffer((config *) c)));}
int bm3A(void *c) {return word_get_A(word_buffer_bm3(config_get_buffer((config *) c)));}
int bm3B(void *c) {return word_get_B(word_buffer_bm3(config_get_buffer((config *) c)));}
int bm3C(void *c) {return word_get_C(word_buffer_bm3(config_get_buffer((config *) c)));}
int bm3D(void *c) {return word_get_D(word_buffer_bm3(config_get_buffer((config *) c)));}
int bm3E(void *c) {return word_get_E(word_buffer_bm3(config_get_buffer((config *) c)));}
int bm3F(void *c) {return word_get_F(word_buffer_bm3(config_get_buffer((config *) c)));}
int bm3G(void *c) {return word_get_G(word_buffer_bm3(config_get_buffer((config *) c)));}
int bm3H(void *c) {return word_get_H(word_buffer_bm3(config_get_buffer((config *) c)));}
int bm3I(void *c) {return word_get_I(word_buffer_bm3(config_get_buffer((config *) c)));}
int bm3J(void *c) {return word_get_J(word_buffer_bm3(config_get_buffer((config *) c)));}
int bm3K(void *c) {return word_get_K(word_buffer_bm3(config_get_buffer((config *) c)));}
int bm3L(void *c) {return word_get_L(word_buffer_bm3(config_get_buffer((config *) c)));}
int bm3M(void *c) {return word_get_M(word_buffer_bm3(config_get_buffer((config *) c)));}
int bm3N(void *c) {return word_get_N(word_buffer_bm3(config_get_buffer((config *) c)));}
int bm3O(void *c) {return word_get_O(word_buffer_bm3(config_get_buffer((config *) c)));}
int bm3P(void *c) {return word_get_P(word_buffer_bm3(config_get_buffer((config *) c)));}
int bm3Q(void *c) {return word_get_Q(word_buffer_bm3(config_get_buffer((config *) c)));}
int bm3R(void *c) {return word_get_R(word_buffer_bm3(config_get_buffer((config *) c)));}
int bm3S(void *c) {return word_get_S(word_buffer_bm3(config_get_buffer((config *) c)));}
int bm3T(void *c) {return word_get_T(word_buffer_bm3(config_get_buffer((config *) c)));}
int bm3U(void *c) {return word_get_U(word_buffer_bm3(config_get_buffer((config *) c)));}
int bm3V(void *c) {return word_get_V(word_buffer_bm3(config_get_buffer((config *) c)));}
int bm3W(void *c) {return word_get_W(word_buffer_bm3(config_get_buffer((config *) c)));}
int bm3X(void *c) {return word_get_X(word_buffer_bm3(config_get_buffer((config *) c)));}
int bm3Y(void *c) {return word_get_Y(word_buffer_bm3(config_get_buffer((config *) c)));}
int bm3Z(void *c) {return word_get_Z(word_buffer_bm3(config_get_buffer((config *) c)));}

/* structural features */

int ldep_s0r(void *input){
  config *c = (config *)input;
  word *gov = stack_s0(config_get_stack((config *) c));
  int i;
  word *dep;
  int dist;
  
  if(gov){
    for(i=word_get_index(gov) - 1; i > 0  ; i--){
      dep = word_buffer_get_word_n(config_get_buffer((config *) c), i);
      if(word_get_sent_seg(dep) == 1) return -1;
      dist = word_get_index(gov) - i;
      if(word_get_gov(dep) == dist){
	return word_get_label(dep);
      }
    }
  }
  return -1;
}

int ldep_s0p(void *input){
  config *c = (config *)input;
  word *gov = stack_s0(config_get_stack((config *) c));
  int i;
  word *dep;
  int dist;
  
  if(gov){
    for(i=word_get_index(gov) - 1; i > 0  ; i--){
      dep = word_buffer_get_word_n(config_get_buffer((config *) c), i);
      if(word_get_sent_seg(dep) == 1) return -1;
      dist = word_get_index(gov) - i;
      if(word_get_gov(dep) == dist){
	return word_get_pos(dep);
      }
    }
  }
  return -1;
}

int ldep_s1r(void *input){
  config *c = (config *)input;
  word *gov = stack_s1(config_get_stack((config *) c));
  int i;
  word *dep;
  int dist;
  
  if(gov){
    for(i=word_get_index(gov) - 1; i > 0  ; i--){
      dep = word_buffer_get_word_n(config_get_buffer((config *) c), i);
      if(word_get_sent_seg(dep) == 1) return -1;
      dist = word_get_index(gov) - i;
      if(word_get_gov(dep) == dist){
	return word_get_label(dep);
      }
    }
  }
  return -1;
}

int ldep_s1p(void *input){
  config *c = (config *)input;
  word *gov = stack_s1(config_get_stack((config *) c));
  int i;
  word *dep;
  int dist;
  
  if(gov){
    for(i=word_get_index(gov) - 1; i > 0  ; i--){
      dep = word_buffer_get_word_n(config_get_buffer((config *) c), i);
      if(word_get_sent_seg(dep) == 1) return -1;
      dist = word_get_index(gov) - i;
      if(word_get_gov(dep) == dist){
	return word_get_pos(dep);
      }
    }
  }
  return -1;
}

int ldep_b0r(void *input){
  config *c = (config *)input;
  word *gov = word_buffer_b0(config_get_buffer((config *) c));
  int i;
  word *dep;
  int dist;
  
  if(gov){
    for(i=word_get_index(gov) - 1; i > 0  ; i--){
      dep = word_buffer_get_word_n(config_get_buffer((config *) c), i);
      if(word_get_sent_seg(dep) == 1) return -1;
      dist = word_get_index(gov) - i;
      if(word_get_gov(dep) == dist){
	return word_get_label(dep);
      }
    }
  }
  return -1;
}

int ldep_b0p(void *input){
  config *c = (config *)input;
  word *gov = word_buffer_b0(config_get_buffer((config *) c));
  int i;
  word *dep;
  int dist;
  
  if(gov){
    for(i=word_get_index(gov) - 1; i > 0  ; i--){
      dep = word_buffer_get_word_n(config_get_buffer((config *) c), i);
      if(word_get_sent_seg(dep) == 1) return -1;
      dist = word_get_index(gov) - i;
      if(word_get_gov(dep) == dist){
	return word_get_pos(dep);
      }
    }
  }
  return -1;
}

int rdep_s0r(void *input){
  config *c = (config *)input;
  word *gov = stack_s0(config_get_stack((config *) c));
  int i;
  word *dep;
  int dist;
  
  if(gov){
    for(i=word_get_index(gov) + 1; i < word_buffer_get_nbelem(config_get_buffer((config *) c))  ; i++){
      dep = word_buffer_get_word_n(config_get_buffer((config *) c), i);
      if(i >= word_get_index(word_buffer_b0(config_get_buffer((config *) c)))) return -1;
      dist = i - word_get_index(gov);
      if(word_get_gov(dep) == - dist){
	return word_get_label(dep);
      }
    }
  }
  return -1;
}

int rdep_s0p(void *input){
  config *c = (config *)input;
  word *gov = stack_s0(config_get_stack((config *) c));
  int i;
  word *dep;
  int dist;
  
  if(gov){
    for(i=word_get_index(gov) + 1; i < word_buffer_get_nbelem(config_get_buffer((config *) c))  ; i++){
      dep = word_buffer_get_word_n(config_get_buffer((config *) c), i);
      if(i >= word_get_index(word_buffer_b0(config_get_buffer((config *) c)))) return -1;
      dist = i - word_get_index(gov);
      if(word_get_gov(dep) == - dist){
	return word_get_pos(dep);
      }
    }
  }
  return -1;
}

int rdep_s1p(void *input){
  config *c = (config *)input;
  word *gov = stack_s1(config_get_stack((config *) c));
  int i;
  word *dep;
  int dist;
  
  if(gov){
    for(i=word_get_index(gov) + 1; i < word_buffer_get_nbelem(config_get_buffer((config *) c))  ; i++){
      dep = word_buffer_get_word_n(config_get_buffer((config *) c), i);
      if(i >= word_get_index(word_buffer_b0(config_get_buffer((config *) c)))) return -1;
      dist = i - word_get_index(gov);
      if(word_get_gov(dep) == - dist){
	return word_get_pos(dep);
      }
    }
  }
  return -1;
}

int rdep_s1r(void *input){
  config *c = (config *)input;
  word *gov = stack_s1(config_get_stack((config *) c));
  int i;
  word *dep;
  int dist;
  
  if(gov){
    for(i=word_get_index(gov) + 1; i < word_buffer_get_nbelem(config_get_buffer((config *) c))  ; i++){
      dep = word_buffer_get_word_n(config_get_buffer((config *) c), i);
      if(i >= word_get_index(word_buffer_b0(config_get_buffer((config *) c)))) return -1;
      dist = i - word_get_index(gov);
      if(word_get_gov(dep) == - dist){
	return word_get_label(dep);
      }
    }
  }
  return -1;
}

int rdep_b0r(void *input){
  config *c = (config *)input;
  word *gov = word_buffer_b0(config_get_buffer((config *) c));
  int i;
  word *dep;
  int dist;
  
  if(gov){
    for(i=word_get_index(gov) + 1; i < word_buffer_get_nbelem(config_get_buffer((config *) c))  ; i++){
      dep = word_buffer_get_word_n(config_get_buffer((config *) c), i);
      if(i >= word_get_index(word_buffer_b0(config_get_buffer((config *) c)))) return -1;
      dist = i - word_get_index(gov);
      if(word_get_gov(dep) == - dist){
	return word_get_label(dep);
      }
    }
  }
  return -1;
}

int rdep_b0p(void *input){
  config *c = (config *)input;
  word *gov = word_buffer_b0(config_get_buffer((config *) c));
  int i;
  word *dep;
  int dist;
  
  if(gov){
    for(i=word_get_index(gov) + 1; i < word_buffer_get_nbelem(config_get_buffer((config *) c))  ; i++){
      dep = word_buffer_get_word_n(config_get_buffer((config *) c), i);
      if(i >= word_get_index(word_buffer_b0(config_get_buffer((config *) c)))) return -1;
      dist = i - word_get_index(gov);
      if(word_get_gov(dep) == - dist){
	return word_get_pos(dep);
      }
    }
  }
  return -1;
}


int ndep_b0(void *input){
  config *c = (config *)input;
  word *gov = word_buffer_b0(config_get_buffer((config *) c));
  int i;
  int n = 0;
  word *dep;
  int dist;
  
  if(gov){
    for(i=word_get_index(gov) - 1; i > 0  ; i--){
      dep = word_buffer_get_word_n(config_get_buffer((config *) c), i);
      if(word_get_sent_seg(dep) == 1) break;
      dist = word_get_index(gov) - i;
      if(word_get_gov(dep) == dist)
	n++;
    }
    for(i=word_get_index(gov) + 1; i < word_buffer_get_nbelem(config_get_buffer((config *) c))  ; i++){
      dep = word_buffer_get_word_n(config_get_buffer((config *) c), i);
      if(i >= word_get_index(word_buffer_b0(config_get_buffer((config *) c)))) break;
      dist = i - word_get_index(gov);
      if(word_get_gov(dep) == - dist)
	n++;
    }
  }
  return (n > 6)? 6 : n;
}

int ndep_s0(void *input){
  config *c = (config *)input;
  word *gov = stack_s0(config_get_stack((config *) c));
  int i;
  int n = 0;
  word *dep;
  int dist;
  
  if(gov){
    for(i=word_get_index(gov) - 1; i > 0  ; i--){
      dep = word_buffer_get_word_n(config_get_buffer((config *) c), i);
      if(word_get_sent_seg(dep) == 1) break;
      dist = word_get_index(gov) - i;
      if(word_get_gov(dep) == dist)
	n++;
    }
    for(i=word_get_index(gov) + 1; i < word_buffer_get_nbelem(config_get_buffer((config *) c))  ; i++){
      dep = word_buffer_get_word_n(config_get_buffer((config *) c), i);
      if(i >= word_get_index(word_buffer_b0(config_get_buffer((config *) c)))) break;
      dist = i - word_get_index(gov);
      if(word_get_gov(dep) == - dist)
	n++;
    }
  }
  return (n > 6)? 6 : n;
}


/* distance features */

int dist_s0_b0(void *input){
  config *c = (config *)input;
  int dist;
  
  if(stack_is_empty(config_get_stack((config *) c)) || word_buffer_is_empty(config_get_buffer((config *) c)))
    return 0;

  dist =  word_get_index(word_buffer_b0(config_get_buffer((config *) c))) - word_get_index(stack_top(config_get_stack((config *) c)));

   return (abs(dist) > 6)? 6 : dist; 
}

/* configurational features */
 /* stack height */
int sh(void *input)
{
config *c = (config *)input;

   return (config_get_stack((config *) c)->top > 7)? 7 : config_get_stack((config *) c)->top; 
  /* return (stack_nbelem(config_get_stack((config *) c)) > 0)? 1 : 0; */
}
 /* buffer size */
int bh(void *input)
{
config *c = (config *)input;

  return (config_get_buffer((config *) c)->size > 7)? 7 : config_get_buffer((config *) c)->size;
}

#if 0
/* depset size */
int dh(void *c) 
{
 return (c->ds->length > 7)? 7 : c->ds->length; 
  /* return c->ds->length; */
}
#endif
/* previous transition */
int t1(void *c) 
{
  mvt *m = mvt_stack_0(config_get_history((config *) c));
  return (m == NULL)? -1 : mvt_get_type(m); 
}
/* previous transition */
int t2(void *c) 
{
  mvt *m = mvt_stack_1(config_get_history((config *) c));
  return (m == NULL)? -1 : mvt_get_type(m); 
}
/* previous transition */
int t3(void *c) 
{
  mvt *m = mvt_stack_2(config_get_history((config *) c));
  return (m == NULL)? -1 : mvt_get_type(m); 
}
/* previous transition */
int t4(void *c) 
{
  mvt *m = mvt_stack_3(config_get_history((config *) c));
  return (m == NULL)? -1 : mvt_get_type(m); 
}



int mvt0(void *input)
{
  config *c = (config *)input;
  if(c->vcode_array == NULL) return -1;
  return c->vcode_array[0].class_code;
}

int mvt1(void *input)
{
  config *c = (config *)input;
  if(c->vcode_array == NULL) return -1;
  return c->vcode_array[1].class_code;
}

int delta1(void *input)
{
  config *c = (config *)input;
  if(c->vcode_array == NULL) return -1;
  int delta = (int) (c->vcode_array[0].score - c->vcode_array[1].score); 
  return (delta >= 10)? 10: delta;
}

int mvt2(void *input)
{
  config *c = (config *)input;
  if(c->vcode_array == NULL) return -1;
  return c->vcode_array[2].class_code;
}

int delta2(void *input)
{
  config *c = (config *)input;
  if(c->vcode_array == NULL) return -1;
  int delta = (int) (c->vcode_array[0].score - c->vcode_array[2].score);
  return (delta >= 10)? 10: delta;
}

int mvt3(void *input)
{
  config *c = (config *)input;
  if(c->vcode_array == NULL) return -1;
  return c->vcode_array[3].class_code;
}

int delta3(void *input)
{
  config *c = (config *)input;
  if(c->vcode_array == NULL) return -1;
  int delta = (int) (c->vcode_array[0].score - c->vcode_array[3].score);
  return (delta >= 10)? 10: delta;
}

feat_lib *feat_lib_build(void)
{
  feat_lib *fl = feat_lib_new();
  
  feat_lib_add(fl, FEAT_TYPE_LABEL,  (char *)"s0sf", s0sf);
  feat_lib_add(fl, FEAT_TYPE_INT,    (char *)"s0g", s0g);

  feat_lib_add(fl, FEAT_TYPE_INT,  (char *)"s0seg", s0seg);
  feat_lib_add(fl, FEAT_TYPE_FORM,  (char *)"s0f", s0f);
  feat_lib_add(fl, FEAT_TYPE_LEMMA, (char *)"s0l", s0l);
  feat_lib_add(fl, FEAT_TYPE_CPOS,  (char *)"s0c", s0c);
  feat_lib_add(fl, FEAT_TYPE_POS,   (char *)"s0p", s0p);
  feat_lib_add(fl, FEAT_TYPE_FEATS, (char *)"s0m", s0m);
  feat_lib_add(fl, FEAT_TYPE_STAG , (char *)"s0s", s0s);
  /* feat_lib_add(fl, FEAT_TYPE_LABEL, (char *)"s0r", s0r); */
  feat_lib_add(fl, FEAT_TYPE_A,     (char *)"s0A", s0A);
  feat_lib_add(fl, FEAT_TYPE_B,     (char *)"s0B", s0B);
  feat_lib_add(fl, FEAT_TYPE_C,     (char *)"s0C", s0C);
  feat_lib_add(fl, FEAT_TYPE_D,     (char *)"s0D", s0D);
  feat_lib_add(fl, FEAT_TYPE_E,     (char *)"s0E", s0E);
  feat_lib_add(fl, FEAT_TYPE_F,     (char *)"s0F", s0F);
  feat_lib_add(fl, FEAT_TYPE_G,     (char *)"s0G", s0G);
  feat_lib_add(fl, FEAT_TYPE_H,     (char *)"s0H", s0H);
  feat_lib_add(fl, FEAT_TYPE_I,     (char *)"s0I", s0I);
  feat_lib_add(fl, FEAT_TYPE_J,     (char *)"s0J", s0J);
  feat_lib_add(fl, FEAT_TYPE_K,     (char *)"s0K", s0K);
  feat_lib_add(fl, FEAT_TYPE_L,     (char *)"s0L", s0L);
  feat_lib_add(fl, FEAT_TYPE_M,     (char *)"s0M", s0M);
  feat_lib_add(fl, FEAT_TYPE_N,     (char *)"s0N", s0N);
  feat_lib_add(fl, FEAT_TYPE_O,     (char *)"s0O", s0O);
  feat_lib_add(fl, FEAT_TYPE_P,     (char *)"s0P", s0P);
  feat_lib_add(fl, FEAT_TYPE_Q,     (char *)"s0Q", s0Q);
  feat_lib_add(fl, FEAT_TYPE_R,     (char *)"s0R", s0R);
  feat_lib_add(fl, FEAT_TYPE_S,     (char *)"s0S", s0S);
  feat_lib_add(fl, FEAT_TYPE_T,     (char *)"s0T", s0T);
  feat_lib_add(fl, FEAT_TYPE_U,     (char *)"s0U", s0U);
  feat_lib_add(fl, FEAT_TYPE_V,     (char *)"s0V", s0V);
  feat_lib_add(fl, FEAT_TYPE_W,     (char *)"s0W", s0W);
  feat_lib_add(fl, FEAT_TYPE_X,     (char *)"s0X", s0X);
  feat_lib_add(fl, FEAT_TYPE_Y,     (char *)"s0Y", s0Y);
  feat_lib_add(fl, FEAT_TYPE_Y,     (char *)"s0Z", s0Z);
  /* feat_lib_add(fl, FEAT_TYPE_INT_3, (char *)"s0U1", s0U1); */
  feat_lib_add(fl, FEAT_TYPE_INT, (char *)"s0U1", s0U1);
  feat_lib_add(fl, FEAT_TYPE_INT,   (char *)"s0sgn", s0sgn);
  
  feat_lib_add(fl, FEAT_TYPE_FORM,  (char *)"s1g", s1g);
  feat_lib_add(fl, FEAT_TYPE_FORM,  (char *)"s1sf", s1sf);

  feat_lib_add(fl, FEAT_TYPE_FORM,  (char *)"s1f", s1f);
  feat_lib_add(fl, FEAT_TYPE_LEMMA, (char *)"s1l", s1l);
  feat_lib_add(fl, FEAT_TYPE_CPOS,  (char *)"s1c", s1c);
  feat_lib_add(fl, FEAT_TYPE_POS,   (char *)"s1p", s1p);
  feat_lib_add(fl, FEAT_TYPE_FEATS, (char *)"s1m", s1m);
  feat_lib_add(fl, FEAT_TYPE_STAG,  (char *)"s1s", s1s);
  /* feat_lib_add(fl, FEAT_TYPE_LABEL, (char *)"s1r", s1r); */
  feat_lib_add(fl, FEAT_TYPE_A,     (char *)"s1A", s1A);
  feat_lib_add(fl, FEAT_TYPE_B,     (char *)"s1B", s1B);
  feat_lib_add(fl, FEAT_TYPE_C,     (char *)"s1C", s1C);
  feat_lib_add(fl, FEAT_TYPE_D,     (char *)"s1D", s1D);
  feat_lib_add(fl, FEAT_TYPE_E,     (char *)"s1E", s1E);
  feat_lib_add(fl, FEAT_TYPE_F,     (char *)"s1F", s1F);
  feat_lib_add(fl, FEAT_TYPE_G,     (char *)"s1G", s1G);
  feat_lib_add(fl, FEAT_TYPE_H,     (char *)"s1H", s1H);
  feat_lib_add(fl, FEAT_TYPE_I,     (char *)"s1I", s1I);
  feat_lib_add(fl, FEAT_TYPE_J,     (char *)"s1J", s1J);
  feat_lib_add(fl, FEAT_TYPE_K,     (char *)"s1K", s1K);
  feat_lib_add(fl, FEAT_TYPE_L,     (char *)"s1L", s1L);
  feat_lib_add(fl, FEAT_TYPE_M,     (char *)"s1M", s1M);
  feat_lib_add(fl, FEAT_TYPE_N,     (char *)"s1N", s1N);
  feat_lib_add(fl, FEAT_TYPE_O,     (char *)"s1O", s1O);
  feat_lib_add(fl, FEAT_TYPE_P,     (char *)"s1P", s1P);
  feat_lib_add(fl, FEAT_TYPE_Q,     (char *)"s1Q", s1Q);
  feat_lib_add(fl, FEAT_TYPE_R,     (char *)"s1R", s1R);
  feat_lib_add(fl, FEAT_TYPE_S,     (char *)"s1S", s1S);
  feat_lib_add(fl, FEAT_TYPE_T,     (char *)"s1T", s1T);
  feat_lib_add(fl, FEAT_TYPE_U,     (char *)"s1U", s1U);
  feat_lib_add(fl, FEAT_TYPE_V,     (char *)"s1V", s1V);
  feat_lib_add(fl, FEAT_TYPE_W,     (char *)"s1W", s1W);
  feat_lib_add(fl, FEAT_TYPE_X,     (char *)"s1X", s1X);
  feat_lib_add(fl, FEAT_TYPE_Y,     (char *)"s1Y", s1Y);
  feat_lib_add(fl, FEAT_TYPE_Z,     (char *)"s1Z", s1Z);
  
  feat_lib_add(fl, FEAT_TYPE_LABEL,  (char *)"s2sf", s2sf);
  feat_lib_add(fl, FEAT_TYPE_INT,    (char *)"s2g", s2g);
  
  feat_lib_add(fl, FEAT_TYPE_FORM,  (char *)"s2f", s2f);
  feat_lib_add(fl, FEAT_TYPE_LEMMA, (char *)"s2l", s2l);
  feat_lib_add(fl, FEAT_TYPE_CPOS,  (char *)"s2c", s2c);
  feat_lib_add(fl, FEAT_TYPE_POS,   (char *)"s2p", s2p);
  feat_lib_add(fl, FEAT_TYPE_FEATS, (char *)"s2m", s2m);
  feat_lib_add(fl, FEAT_TYPE_STAG,  (char *)"s2s", s2s);
  /* feat_lib_add(fl, FEAT_TYPE_LABEL, (char *)"s2r", s2r); */
  feat_lib_add(fl, FEAT_TYPE_A,     (char *)"s2A", s2A);
  feat_lib_add(fl, FEAT_TYPE_B,     (char *)"s2B", s2B);
  feat_lib_add(fl, FEAT_TYPE_C,     (char *)"s2C", s2C);
  feat_lib_add(fl, FEAT_TYPE_D,     (char *)"s2D", s2D);
  feat_lib_add(fl, FEAT_TYPE_E,     (char *)"s2E", s2E);
  feat_lib_add(fl, FEAT_TYPE_F,     (char *)"s2F", s2F);
  feat_lib_add(fl, FEAT_TYPE_G,     (char *)"s2G", s2G);
  feat_lib_add(fl, FEAT_TYPE_H,     (char *)"s2H", s2H);
  feat_lib_add(fl, FEAT_TYPE_I,     (char *)"s2I", s2I);
  feat_lib_add(fl, FEAT_TYPE_J,     (char *)"s2J", s2J);
  feat_lib_add(fl, FEAT_TYPE_K,     (char *)"s2K", s2K);
  feat_lib_add(fl, FEAT_TYPE_L,     (char *)"s2L", s2L);
  feat_lib_add(fl, FEAT_TYPE_M,     (char *)"s2M", s2M);
  feat_lib_add(fl, FEAT_TYPE_N,     (char *)"s2N", s2N);
  feat_lib_add(fl, FEAT_TYPE_O,     (char *)"s2O", s2O);
  feat_lib_add(fl, FEAT_TYPE_P,     (char *)"s2P", s2P);
  feat_lib_add(fl, FEAT_TYPE_Q,     (char *)"s2Q", s2Q);
  feat_lib_add(fl, FEAT_TYPE_R,     (char *)"s2R", s2R);
  feat_lib_add(fl, FEAT_TYPE_S,     (char *)"s2S", s2S);
  feat_lib_add(fl, FEAT_TYPE_T,     (char *)"s2T", s2T);
  feat_lib_add(fl, FEAT_TYPE_U,     (char *)"s2U", s2U);
  feat_lib_add(fl, FEAT_TYPE_V,     (char *)"s2V", s2V);
  feat_lib_add(fl, FEAT_TYPE_W,     (char *)"s2W", s2W);
  feat_lib_add(fl, FEAT_TYPE_X,     (char *)"s2X", s2X);
  feat_lib_add(fl, FEAT_TYPE_Y,     (char *)"s2Y", s2Y);
  feat_lib_add(fl, FEAT_TYPE_Y,     (char *)"s2Z", s2Z);
  
  feat_lib_add(fl, FEAT_TYPE_FORM,  (char *)"s3f", s3f);
  feat_lib_add(fl, FEAT_TYPE_LEMMA, (char *)"s3l", s3l);
  feat_lib_add(fl, FEAT_TYPE_CPOS,  (char *)"s3c", s3c);
  feat_lib_add(fl, FEAT_TYPE_POS,   (char *)"s3p", s3p);
  feat_lib_add(fl, FEAT_TYPE_FEATS, (char *)"s3m", s3m);
  feat_lib_add(fl, FEAT_TYPE_STAG,  (char *)"s3s", s3s);
  /* feat_lib_add(fl, FEAT_TYPE_LABEL, (char *)"s3r", s3r); */
  feat_lib_add(fl, FEAT_TYPE_A,     (char *)"s3A", s3A);
  feat_lib_add(fl, FEAT_TYPE_B,     (char *)"s3B", s3B);
  feat_lib_add(fl, FEAT_TYPE_C,     (char *)"s3C", s3C);
  feat_lib_add(fl, FEAT_TYPE_D,     (char *)"s3D", s3D);
  feat_lib_add(fl, FEAT_TYPE_E,     (char *)"s3E", s3E);
  feat_lib_add(fl, FEAT_TYPE_F,     (char *)"s3F", s3F);
  feat_lib_add(fl, FEAT_TYPE_G,     (char *)"s3G", s3G);
  feat_lib_add(fl, FEAT_TYPE_H,     (char *)"s3H", s3H);
  feat_lib_add(fl, FEAT_TYPE_I,     (char *)"s3I", s3I);
  feat_lib_add(fl, FEAT_TYPE_J,     (char *)"s3J", s3J);
  feat_lib_add(fl, FEAT_TYPE_K,     (char *)"s3K", s3K);
  feat_lib_add(fl, FEAT_TYPE_L,     (char *)"s3L", s3L);
  feat_lib_add(fl, FEAT_TYPE_M,     (char *)"s3M", s3M);
  feat_lib_add(fl, FEAT_TYPE_N,     (char *)"s3N", s3N);
  feat_lib_add(fl, FEAT_TYPE_O,     (char *)"s3O", s3O);
  feat_lib_add(fl, FEAT_TYPE_P,     (char *)"s3P", s3P);
  feat_lib_add(fl, FEAT_TYPE_Q,     (char *)"s3Q", s3Q);
  feat_lib_add(fl, FEAT_TYPE_R,     (char *)"s3R", s3R);
  feat_lib_add(fl, FEAT_TYPE_S,     (char *)"s3S", s3S);
  feat_lib_add(fl, FEAT_TYPE_T,     (char *)"s3T", s3T);
  feat_lib_add(fl, FEAT_TYPE_U,     (char *)"s3U", s3U);
  feat_lib_add(fl, FEAT_TYPE_V,     (char *)"s3V", s3V);
  feat_lib_add(fl, FEAT_TYPE_W,     (char *)"s3W", s3W);
  feat_lib_add(fl, FEAT_TYPE_X,     (char *)"s3X", s3X);
  feat_lib_add(fl, FEAT_TYPE_Y,     (char *)"s3Y", s3Y);
  feat_lib_add(fl, FEAT_TYPE_Y,     (char *)"s3Z", s3Z);

  feat_lib_add(fl, FEAT_TYPE_FORM,  (char *)"b0g", b0g);
  feat_lib_add(fl, FEAT_TYPE_FORM,  (char *)"b0sf", b0sf);

  feat_lib_add(fl, FEAT_TYPE_INT,  (char *)"b0len", b0len);

  feat_lib_add(fl, FEAT_TYPE_FORM,  (char *)"b0f", b0f);
  feat_lib_add(fl, FEAT_TYPE_LEMMA, (char *)"b0l", b0l);
  feat_lib_add(fl, FEAT_TYPE_CPOS,  (char *)"b0c", b0c);
  feat_lib_add(fl, FEAT_TYPE_POS,   (char *)"b0p", b0p);
  feat_lib_add(fl, FEAT_TYPE_FEATS, (char *)"b0m", b0m);
  feat_lib_add(fl, FEAT_TYPE_STAG,  (char *)"b0s", b0s);
  /* feat_lib_add(fl, FEAT_TYPE_LABEL, (char *)"b0r", b0r); */
  feat_lib_add(fl, FEAT_TYPE_A,     (char *)"b0A", b0A);
  feat_lib_add(fl, FEAT_TYPE_B,     (char *)"b0B", b0B);
  feat_lib_add(fl, FEAT_TYPE_C,     (char *)"b0C", b0C);
  feat_lib_add(fl, FEAT_TYPE_D,     (char *)"b0D", b0D);
  feat_lib_add(fl, FEAT_TYPE_E,     (char *)"b0E", b0E);
  feat_lib_add(fl, FEAT_TYPE_F,     (char *)"b0F", b0F);
  feat_lib_add(fl, FEAT_TYPE_G,     (char *)"b0G", b0G);
  feat_lib_add(fl, FEAT_TYPE_H,     (char *)"b0H", b0H);
  feat_lib_add(fl, FEAT_TYPE_I,     (char *)"b0I", b0I);
  feat_lib_add(fl, FEAT_TYPE_J,     (char *)"b0J", b0J);
  feat_lib_add(fl, FEAT_TYPE_K,     (char *)"b0K", b0K);
  feat_lib_add(fl, FEAT_TYPE_L,     (char *)"b0L", b0L);
  feat_lib_add(fl, FEAT_TYPE_M,     (char *)"b0M", b0M);
  feat_lib_add(fl, FEAT_TYPE_N,     (char *)"b0N", b0N);
  feat_lib_add(fl, FEAT_TYPE_O,     (char *)"b0O", b0O);
  feat_lib_add(fl, FEAT_TYPE_P,     (char *)"b0P", b0P);
  feat_lib_add(fl, FEAT_TYPE_Q,     (char *)"b0Q", b0Q);
  feat_lib_add(fl, FEAT_TYPE_R,     (char *)"b0R", b0R);
  feat_lib_add(fl, FEAT_TYPE_S,     (char *)"b0S", b0S);
  feat_lib_add(fl, FEAT_TYPE_T,     (char *)"b0T", b0T);
  feat_lib_add(fl, FEAT_TYPE_U,     (char *)"b0U", b0U);
  feat_lib_add(fl, FEAT_TYPE_V,     (char *)"b0V", b0V);
  feat_lib_add(fl, FEAT_TYPE_W,     (char *)"b0W", b0W);
  feat_lib_add(fl, FEAT_TYPE_X,     (char *)"b0X", b0X);
  feat_lib_add(fl, FEAT_TYPE_Y,     (char *)"b0Y", b0Y);
  feat_lib_add(fl, FEAT_TYPE_Y,     (char *)"b0Z", b0Z);
  /* feat_lib_add(fl, FEAT_TYPE_INT_3, (char *)"b0U1", b0U1); */
  feat_lib_add(fl, FEAT_TYPE_INT, (char *)"b0U1", b0U1);
  feat_lib_add(fl, FEAT_TYPE_INT,   (char *)"b0sgn", b0sgn);

  feat_lib_add(fl, FEAT_TYPE_INT,   (char *)"b0s1", b0s1);
  feat_lib_add(fl, FEAT_TYPE_INT,   (char *)"b0s2", b0s2);
  feat_lib_add(fl, FEAT_TYPE_INT,   (char *)"b0s3", b0s3);
  feat_lib_add(fl, FEAT_TYPE_INT,   (char *)"b0s4", b0s4);
  feat_lib_add(fl, FEAT_TYPE_INT,   (char *)"b0s5", b0s5);
  feat_lib_add(fl, FEAT_TYPE_INT,   (char *)"b0s6", b0s6);

  feat_lib_add(fl, FEAT_TYPE_INT,   (char *)"b0p1", b0p1);
  feat_lib_add(fl, FEAT_TYPE_INT,   (char *)"b0p2", b0p2);
  feat_lib_add(fl, FEAT_TYPE_INT,   (char *)"b0p3", b0p3);
  feat_lib_add(fl, FEAT_TYPE_INT,   (char *)"b0p4", b0p4);
  feat_lib_add(fl, FEAT_TYPE_INT,   (char *)"b0p5", b0p5);
  feat_lib_add(fl, FEAT_TYPE_INT,   (char *)"b0p6", b0p6);

  
  feat_lib_add(fl, FEAT_TYPE_FORM,  (char *)"bm1f", bm1f);
  feat_lib_add(fl, FEAT_TYPE_LEMMA, (char *)"bm1l", bm1l);
  feat_lib_add(fl, FEAT_TYPE_CPOS,  (char *)"bm1c", bm1c);
  feat_lib_add(fl, FEAT_TYPE_POS,   (char *)"bm1p", bm1p);
  feat_lib_add(fl, FEAT_TYPE_FEATS, (char *)"bm1m", bm1m);
  feat_lib_add(fl, FEAT_TYPE_STAG,  (char *)"bm1s", bm1s);
  /* feat_lib_add(fl, FEAT_TYPE_LABEL, (char *)"bm1r", bm1r); */
  feat_lib_add(fl, FEAT_TYPE_A,     (char *)"bm1A", bm1A);
  feat_lib_add(fl, FEAT_TYPE_B,     (char *)"bm1B", bm1B);
  feat_lib_add(fl, FEAT_TYPE_C,     (char *)"bm1C", bm1C);
  feat_lib_add(fl, FEAT_TYPE_D,     (char *)"bm1D", bm1D);
  feat_lib_add(fl, FEAT_TYPE_E,     (char *)"bm1E", bm1E);
  feat_lib_add(fl, FEAT_TYPE_F,     (char *)"bm1F", bm1F);
  feat_lib_add(fl, FEAT_TYPE_G,     (char *)"bm1G", bm1G);
  feat_lib_add(fl, FEAT_TYPE_H,     (char *)"bm1H", bm1H);
  feat_lib_add(fl, FEAT_TYPE_I,     (char *)"bm1I", bm1I);
  feat_lib_add(fl, FEAT_TYPE_J,     (char *)"bm1J", bm1J);
  feat_lib_add(fl, FEAT_TYPE_K,     (char *)"bm1K", bm1K);
  feat_lib_add(fl, FEAT_TYPE_L,     (char *)"bm1L", bm1L);
  feat_lib_add(fl, FEAT_TYPE_M,     (char *)"bm1M", bm1M);
  feat_lib_add(fl, FEAT_TYPE_N,     (char *)"bm1N", bm1N);
  feat_lib_add(fl, FEAT_TYPE_O,     (char *)"bm1O", bm1O);
  feat_lib_add(fl, FEAT_TYPE_P,     (char *)"bm1P", bm1P);
  feat_lib_add(fl, FEAT_TYPE_Q,     (char *)"bm1Q", bm1Q);
  feat_lib_add(fl, FEAT_TYPE_R,     (char *)"bm1R", bm1R);
  feat_lib_add(fl, FEAT_TYPE_S,     (char *)"bm1S", bm1S);
  feat_lib_add(fl, FEAT_TYPE_T,     (char *)"bm1T", bm1T);
  feat_lib_add(fl, FEAT_TYPE_U,     (char *)"bm1U", bm1U);
  feat_lib_add(fl, FEAT_TYPE_V,     (char *)"bm1V", bm1V);
  feat_lib_add(fl, FEAT_TYPE_W,     (char *)"bm1W", bm1W);
  feat_lib_add(fl, FEAT_TYPE_X,     (char *)"bm1X", bm1X);
  feat_lib_add(fl, FEAT_TYPE_Y,     (char *)"bm1Y", bm1Y);
  feat_lib_add(fl, FEAT_TYPE_Y,     (char *)"bm1Z", bm1Z);
  /* feat_lib_add(fl, FEAT_TYPE_INT_3,     (char *)"bm1U1", bm1U1); */
  feat_lib_add(fl, FEAT_TYPE_INT,     (char *)"bm1U1", bm1U1);
  feat_lib_add(fl, FEAT_TYPE_INT,     (char *)"bm1sgn", bm1sgn);

  feat_lib_add(fl, FEAT_TYPE_FORM,  (char *)"bm2f", bm2f);
  feat_lib_add(fl, FEAT_TYPE_LEMMA, (char *)"bm2l", bm2l);
  feat_lib_add(fl, FEAT_TYPE_CPOS,  (char *)"bm2c", bm2c);
  feat_lib_add(fl, FEAT_TYPE_POS,   (char *)"bm2p", bm2p);
  feat_lib_add(fl, FEAT_TYPE_FEATS, (char *)"bm2m", bm2m);
  feat_lib_add(fl, FEAT_TYPE_STAG,  (char *)"bm2s", bm2s);
  /* feat_lib_add(fl, FEAT_TYPE_LABEL, (char *)"bm2r", bm2r); */
  feat_lib_add(fl, FEAT_TYPE_A,     (char *)"bm2A", bm2A);
  feat_lib_add(fl, FEAT_TYPE_B,     (char *)"bm2B", bm2B);
  feat_lib_add(fl, FEAT_TYPE_C,     (char *)"bm2C", bm2C);
  feat_lib_add(fl, FEAT_TYPE_D,     (char *)"bm2D", bm2D);
  feat_lib_add(fl, FEAT_TYPE_E,     (char *)"bm2E", bm2E);
  feat_lib_add(fl, FEAT_TYPE_F,     (char *)"bm2F", bm2F);
  feat_lib_add(fl, FEAT_TYPE_G,     (char *)"bm2G", bm2G);
  feat_lib_add(fl, FEAT_TYPE_H,     (char *)"bm2H", bm2H);
  feat_lib_add(fl, FEAT_TYPE_I,     (char *)"bm2I", bm2I);
  feat_lib_add(fl, FEAT_TYPE_J,     (char *)"bm2J", bm2J);
  feat_lib_add(fl, FEAT_TYPE_K,     (char *)"bm2K", bm2K);
  feat_lib_add(fl, FEAT_TYPE_L,     (char *)"bm2L", bm2L);
  feat_lib_add(fl, FEAT_TYPE_M,     (char *)"bm2M", bm2M);
  feat_lib_add(fl, FEAT_TYPE_N,     (char *)"bm2N", bm2N);
  feat_lib_add(fl, FEAT_TYPE_O,     (char *)"bm2O", bm2O);
  feat_lib_add(fl, FEAT_TYPE_P,     (char *)"bm2P", bm2P);
  feat_lib_add(fl, FEAT_TYPE_Q,     (char *)"bm2Q", bm2Q);
  feat_lib_add(fl, FEAT_TYPE_R,     (char *)"bm2R", bm2R);
  feat_lib_add(fl, FEAT_TYPE_S,     (char *)"bm2S", bm2S);
  feat_lib_add(fl, FEAT_TYPE_T,     (char *)"bm2T", bm2T);
  feat_lib_add(fl, FEAT_TYPE_U,     (char *)"bm2U", bm2U);
  feat_lib_add(fl, FEAT_TYPE_V,     (char *)"bm2V", bm2V);
  feat_lib_add(fl, FEAT_TYPE_W,     (char *)"bm2W", bm2W);
  feat_lib_add(fl, FEAT_TYPE_X,     (char *)"bm2X", bm2X);
  feat_lib_add(fl, FEAT_TYPE_Y,     (char *)"bm2Y", bm2Y);
  feat_lib_add(fl, FEAT_TYPE_Y,     (char *)"bm2Z", bm2Z);
  /* feat_lib_add(fl, FEAT_TYPE_INT_3,     (char *)"bm2U1", bm2U1); */
  /* feat_lib_add(fl, FEAT_TYPE_INT,     (char *)"bm2sgn", bm2sgn); */

  feat_lib_add(fl, FEAT_TYPE_FORM,  (char *)"bm3f", bm3f);
  feat_lib_add(fl, FEAT_TYPE_LEMMA, (char *)"bm3l", bm3l);
  feat_lib_add(fl, FEAT_TYPE_CPOS,  (char *)"bm3c", bm3c);
  feat_lib_add(fl, FEAT_TYPE_POS,   (char *)"bm3p", bm3p);
  feat_lib_add(fl, FEAT_TYPE_FEATS, (char *)"bm3m", bm3m);
  feat_lib_add(fl, FEAT_TYPE_STAG,  (char *)"bm3s", bm3s);
  /* feat_lib_add(fl, FEAT_TYPE_LABEL, (char *)"bm3r", bm3r); */
  feat_lib_add(fl, FEAT_TYPE_A,     (char *)"bm3A", bm3A);
  feat_lib_add(fl, FEAT_TYPE_B,     (char *)"bm3B", bm3B);
  feat_lib_add(fl, FEAT_TYPE_C,     (char *)"bm3C", bm3C);
  feat_lib_add(fl, FEAT_TYPE_D,     (char *)"bm3D", bm3D);
  feat_lib_add(fl, FEAT_TYPE_E,     (char *)"bm3E", bm3E);
  feat_lib_add(fl, FEAT_TYPE_F,     (char *)"bm3F", bm3F);
  feat_lib_add(fl, FEAT_TYPE_G,     (char *)"bm3G", bm3G);
  feat_lib_add(fl, FEAT_TYPE_H,     (char *)"bm3H", bm3H);
  feat_lib_add(fl, FEAT_TYPE_I,     (char *)"bm3I", bm3I);
  feat_lib_add(fl, FEAT_TYPE_J,     (char *)"bm3J", bm3J);
  feat_lib_add(fl, FEAT_TYPE_K,     (char *)"bm3K", bm3K);
  feat_lib_add(fl, FEAT_TYPE_L,     (char *)"bm3L", bm3L);
  feat_lib_add(fl, FEAT_TYPE_M,     (char *)"bm3M", bm3M);
  feat_lib_add(fl, FEAT_TYPE_N,     (char *)"bm3N", bm3N);
  feat_lib_add(fl, FEAT_TYPE_O,     (char *)"bm3O", bm3O);
  feat_lib_add(fl, FEAT_TYPE_P,     (char *)"bm3P", bm3P);
  feat_lib_add(fl, FEAT_TYPE_Q,     (char *)"bm3Q", bm3Q);
  feat_lib_add(fl, FEAT_TYPE_R,     (char *)"bm3R", bm3R);
  feat_lib_add(fl, FEAT_TYPE_S,     (char *)"bm3S", bm3S);
  feat_lib_add(fl, FEAT_TYPE_T,     (char *)"bm3T", bm3T);
  feat_lib_add(fl, FEAT_TYPE_U,     (char *)"bm3U", bm3U);
  feat_lib_add(fl, FEAT_TYPE_V,     (char *)"bm3V", bm3V);
  feat_lib_add(fl, FEAT_TYPE_W,     (char *)"bm3W", bm3W);
  feat_lib_add(fl, FEAT_TYPE_X,     (char *)"bm3X", bm3X);
  feat_lib_add(fl, FEAT_TYPE_Y,     (char *)"bm3Y", bm3Y);
  feat_lib_add(fl, FEAT_TYPE_Y,     (char *)"bm3Z", bm3Z);
  /* feat_lib_add(fl, FEAT_TYPE_INT_3,     (char *)"bm3U1", bm3U1); */
  /* feat_lib_add(fl, FEAT_TYPE_INT,     (char *)"bm3sgn", bm3sgn); */
  
  feat_lib_add(fl, FEAT_TYPE_FORM,  (char *)"b1f", b1f);
  feat_lib_add(fl, FEAT_TYPE_LEMMA, (char *)"b1l", b1l);
  feat_lib_add(fl, FEAT_TYPE_CPOS,  (char *)"b1c", b1c);
  feat_lib_add(fl, FEAT_TYPE_POS,   (char *)"b1p", b1p);
  feat_lib_add(fl, FEAT_TYPE_FEATS, (char *)"b1m", b1m);
  feat_lib_add(fl, FEAT_TYPE_STAG,  (char *)"b1s", b1s);
  /* feat_lib_add(fl, FEAT_TYPE_LABEL, (char *)"b1r", b1r); */
  feat_lib_add(fl, FEAT_TYPE_A,     (char *)"b1A", b1A);
  feat_lib_add(fl, FEAT_TYPE_B,     (char *)"b1B", b1B);
  feat_lib_add(fl, FEAT_TYPE_C,     (char *)"b1C", b1C);
  feat_lib_add(fl, FEAT_TYPE_D,     (char *)"b1D", b1D);
  feat_lib_add(fl, FEAT_TYPE_E,     (char *)"b1E", b1E);
  feat_lib_add(fl, FEAT_TYPE_F,     (char *)"b1F", b1F);
  feat_lib_add(fl, FEAT_TYPE_G,     (char *)"b1G", b1G);
  feat_lib_add(fl, FEAT_TYPE_H,     (char *)"b1H", b1H);
  feat_lib_add(fl, FEAT_TYPE_I,     (char *)"b1I", b1I);
  feat_lib_add(fl, FEAT_TYPE_J,     (char *)"b1J", b1J);
  feat_lib_add(fl, FEAT_TYPE_K,     (char *)"b1K", b1K);
  feat_lib_add(fl, FEAT_TYPE_L,     (char *)"b1L", b1L);
  feat_lib_add(fl, FEAT_TYPE_M,     (char *)"b1M", b1M);
  feat_lib_add(fl, FEAT_TYPE_N,     (char *)"b1N", b1N);
  feat_lib_add(fl, FEAT_TYPE_O,     (char *)"b1O", b1O);
  feat_lib_add(fl, FEAT_TYPE_P,     (char *)"b1P", b1P);
  feat_lib_add(fl, FEAT_TYPE_Q,     (char *)"b1Q", b1Q);
  feat_lib_add(fl, FEAT_TYPE_R,     (char *)"b1R", b1R);
  feat_lib_add(fl, FEAT_TYPE_S,     (char *)"b1S", b1S);
  feat_lib_add(fl, FEAT_TYPE_T,     (char *)"b1T", b1T);
  feat_lib_add(fl, FEAT_TYPE_U,     (char *)"b1U", b1U);
  feat_lib_add(fl, FEAT_TYPE_V,     (char *)"b1V", b1V);
  feat_lib_add(fl, FEAT_TYPE_W,     (char *)"b1W", b1W);
  feat_lib_add(fl, FEAT_TYPE_X,     (char *)"b1X", b1X);
  feat_lib_add(fl, FEAT_TYPE_Y,     (char *)"b1Y", b1Y);
  feat_lib_add(fl, FEAT_TYPE_Y,     (char *)"b1Z", b1Z);
  /* feat_lib_add(fl, FEAT_TYPE_INT_3, (char *)"b1U1", b1U1); */
  feat_lib_add(fl, FEAT_TYPE_INT, (char *)"b1U1", b1U1);
  feat_lib_add(fl, FEAT_TYPE_INT,   (char *)"b1sgn", b1sgn);
  
  feat_lib_add(fl, FEAT_TYPE_FORM,  (char *)"b2f", b2f);
  feat_lib_add(fl, FEAT_TYPE_LEMMA, (char *)"b2l", b2l);
  feat_lib_add(fl, FEAT_TYPE_CPOS,  (char *)"b2c", b2c);
  feat_lib_add(fl, FEAT_TYPE_POS,   (char *)"b2p", b2p);
  feat_lib_add(fl, FEAT_TYPE_FEATS, (char *)"b2m", b2m);
  feat_lib_add(fl, FEAT_TYPE_STAG,  (char *)"b2s", b2s);
  /* feat_lib_add(fl, FEAT_TYPE_LABEL, (char *)"b2r", b2r); */
  feat_lib_add(fl, FEAT_TYPE_A,     (char *)"b2A", b2A);
  feat_lib_add(fl, FEAT_TYPE_B,     (char *)"b2B", b2B);
  feat_lib_add(fl, FEAT_TYPE_C,     (char *)"b2C", b2C);
  feat_lib_add(fl, FEAT_TYPE_D,     (char *)"b2D", b2D);
  feat_lib_add(fl, FEAT_TYPE_E,     (char *)"b2E", b2E);
  feat_lib_add(fl, FEAT_TYPE_F,     (char *)"b2F", b2F);
  feat_lib_add(fl, FEAT_TYPE_G,     (char *)"b2G", b2G);
  feat_lib_add(fl, FEAT_TYPE_H,     (char *)"b2H", b2H);
  feat_lib_add(fl, FEAT_TYPE_I,     (char *)"b2I", b2I);
  feat_lib_add(fl, FEAT_TYPE_J,     (char *)"b2J", b2J);
  feat_lib_add(fl, FEAT_TYPE_K,     (char *)"b2K", b2K);
  feat_lib_add(fl, FEAT_TYPE_L,     (char *)"b2L", b2L);
  feat_lib_add(fl, FEAT_TYPE_M,     (char *)"b2M", b2M);
  feat_lib_add(fl, FEAT_TYPE_N,     (char *)"b2N", b2N);
  feat_lib_add(fl, FEAT_TYPE_O,     (char *)"b2O", b2O);
  feat_lib_add(fl, FEAT_TYPE_P,     (char *)"b2P", b2P);
  feat_lib_add(fl, FEAT_TYPE_Q,     (char *)"b2Q", b2Q);
  feat_lib_add(fl, FEAT_TYPE_R,     (char *)"b2R", b2R);
  feat_lib_add(fl, FEAT_TYPE_S,     (char *)"b2S", b2S);
  feat_lib_add(fl, FEAT_TYPE_T,     (char *)"b2T", b2T);
  feat_lib_add(fl, FEAT_TYPE_U,     (char *)"b2U", b2U);
  feat_lib_add(fl, FEAT_TYPE_V,     (char *)"b2V", b2V);
  feat_lib_add(fl, FEAT_TYPE_W,     (char *)"b2W", b2W);
  feat_lib_add(fl, FEAT_TYPE_X,     (char *)"b2X", b2X);
  feat_lib_add(fl, FEAT_TYPE_Y,     (char *)"b2Y", b2Y);
  feat_lib_add(fl, FEAT_TYPE_Y,     (char *)"b2Z", b2Z);

  feat_lib_add(fl, FEAT_TYPE_Y,     (char *)"b2U1", b2U1);
  feat_lib_add(fl, FEAT_TYPE_Y,     (char *)"b2sgn", b2sgn);

  feat_lib_add(fl, FEAT_TYPE_FORM,  (char *)"b3f", b3f);
  feat_lib_add(fl, FEAT_TYPE_LEMMA, (char *)"b3l", b3l);
  feat_lib_add(fl, FEAT_TYPE_CPOS,  (char *)"b3c", b3c);
  feat_lib_add(fl, FEAT_TYPE_POS,   (char *)"b3p", b3p);
  feat_lib_add(fl, FEAT_TYPE_FEATS, (char *)"b3m", b3m);
  feat_lib_add(fl, FEAT_TYPE_STAG,  (char *)"b3s", b3s);
  /* feat_lib_add(fl, FEAT_TYPE_LABEL, (char *)"b3r", b3r); */
  feat_lib_add(fl, FEAT_TYPE_A,     (char *)"b3A", b3A);
  feat_lib_add(fl, FEAT_TYPE_B,     (char *)"b3B", b3B);
  feat_lib_add(fl, FEAT_TYPE_C,     (char *)"b3C", b3C);
  feat_lib_add(fl, FEAT_TYPE_D,     (char *)"b3D", b3D);
  feat_lib_add(fl, FEAT_TYPE_E,     (char *)"b3E", b3E);
  feat_lib_add(fl, FEAT_TYPE_F,     (char *)"b3F", b3F);
  feat_lib_add(fl, FEAT_TYPE_G,     (char *)"b3G", b3G);
  feat_lib_add(fl, FEAT_TYPE_H,     (char *)"b3H", b3H);
  feat_lib_add(fl, FEAT_TYPE_I,     (char *)"b3I", b3I);
  feat_lib_add(fl, FEAT_TYPE_J,     (char *)"b3J", b3J);
  feat_lib_add(fl, FEAT_TYPE_K,     (char *)"b3K", b3K);
  feat_lib_add(fl, FEAT_TYPE_L,     (char *)"b3L", b3L);
  feat_lib_add(fl, FEAT_TYPE_M,     (char *)"b3M", b3M);
  feat_lib_add(fl, FEAT_TYPE_N,     (char *)"b3N", b3N);
  feat_lib_add(fl, FEAT_TYPE_O,     (char *)"b3O", b3O);
  feat_lib_add(fl, FEAT_TYPE_P,     (char *)"b3P", b3P);
  feat_lib_add(fl, FEAT_TYPE_Q,     (char *)"b3Q", b3Q);
  feat_lib_add(fl, FEAT_TYPE_R,     (char *)"b3R", b3R);
  feat_lib_add(fl, FEAT_TYPE_S,     (char *)"b3S", b3S);
  feat_lib_add(fl, FEAT_TYPE_T,     (char *)"b3T", b3T);
  feat_lib_add(fl, FEAT_TYPE_U,     (char *)"b3U", b3U);
  feat_lib_add(fl, FEAT_TYPE_V,     (char *)"b3V", b3V);
  feat_lib_add(fl, FEAT_TYPE_W,     (char *)"b3W", b3W);
  feat_lib_add(fl, FEAT_TYPE_X,     (char *)"b3X", b3X);
  feat_lib_add(fl, FEAT_TYPE_Y,     (char *)"b3Y", b3Y);
  feat_lib_add(fl, FEAT_TYPE_Y,     (char *)"b3Z", b3Z);

  /* feat_lib_add(fl, FEAT_TYPE_LEMMA, (char *)"gs0l", gs0l); */
  /* feat_lib_add(fl, FEAT_TYPE_POS,   (char *)"gs0p", gs0p); */
  
  feat_lib_add(fl, FEAT_TYPE_LABEL, (char *)"ldep_s0r", ldep_s0r);
  feat_lib_add(fl, FEAT_TYPE_LABEL, (char *)"rdep_s0r", rdep_s0r);
  feat_lib_add(fl, FEAT_TYPE_LABEL, (char *)"ldep_s0p", ldep_s0p);
  feat_lib_add(fl, FEAT_TYPE_LABEL, (char *)"rdep_s0p", rdep_s0p);
  feat_lib_add(fl, FEAT_TYPE_LABEL, (char *)"ldep_s1r", ldep_s1r);
  feat_lib_add(fl, FEAT_TYPE_LABEL, (char *)"rdep_s1r", rdep_s1r);
  feat_lib_add(fl, FEAT_TYPE_LABEL, (char *)"ldep_s1p", ldep_s1p);
  feat_lib_add(fl, FEAT_TYPE_LABEL, (char *)"rdep_s1p", rdep_s1p);
  feat_lib_add(fl, FEAT_TYPE_LABEL, (char *)"ldep_b0r", ldep_b0r);
  feat_lib_add(fl, FEAT_TYPE_LABEL, (char *)"rdep_b0r", rdep_b0r);
  feat_lib_add(fl, FEAT_TYPE_LABEL, (char *)"ldep_b0p", ldep_b0p);
  feat_lib_add(fl, FEAT_TYPE_LABEL, (char *)"rdep_b0p", rdep_b0p);

  /* feat_lib_add(fl, FEAT_TYPE_INT_7, (char *)"ndep_b0", ndep_b0); */
  /* feat_lib_add(fl, FEAT_TYPE_INT_7, (char *)"ndep_s0", ndep_s0); */
  
  feat_lib_add(fl, FEAT_TYPE_INT, (char *)"ndep_b0", ndep_b0);
  feat_lib_add(fl, FEAT_TYPE_INT, (char *)"ndep_s0", ndep_s0);
  
  /* distance features */
  /* feat_lib_add(fl, FEAT_TYPE_INT_7, (char *)"dist_s0_b0", dist_s0_b0); */
  feat_lib_add(fl, FEAT_TYPE_INT, (char *)"dist_s0_b0", dist_s0_b0);

/* configurational features */

  /* feat_lib_add(fl, FEAT_TYPE_INT_8, (char *)"sh", sh); */
  /* feat_lib_add(fl, FEAT_TYPE_INT_8, (char *)"bh", bh); */

  feat_lib_add(fl, FEAT_TYPE_INT, (char *)"sh", sh);
  feat_lib_add(fl, FEAT_TYPE_INT, (char *)"bh", bh);

  /* feat_lib_add(fl, FEAT_TYPE_INT_8, (char *)"dh", dh); */

  /* feat_lib_add(fl, FEAT_TYPE_TRANS, (char *)"t1", t1); */
  /* feat_lib_add(fl, FEAT_TYPE_TRANS, (char *)"t2", t2); */
  /* feat_lib_add(fl, FEAT_TYPE_TRANS, (char *)"t3", t3); */
  /* feat_lib_add(fl, FEAT_TYPE_TRANS, (char *)"t4", t4); */

  feat_lib_add(fl, FEAT_TYPE_INT, (char *)"t1", t1);
  feat_lib_add(fl, FEAT_TYPE_INT, (char *)"t2", t2);
  feat_lib_add(fl, FEAT_TYPE_INT, (char *)"t3", t3);
  feat_lib_add(fl, FEAT_TYPE_INT, (char *)"t4", t4);

  feat_lib_add(fl, FEAT_TYPE_INT,   (char *)"mvt0", mvt0);

  feat_lib_add(fl, FEAT_TYPE_INT,   (char *)"mvt1", mvt1);
  feat_lib_add(fl, FEAT_TYPE_INT,   (char *)"delta1", delta1);

  feat_lib_add(fl, FEAT_TYPE_INT,   (char *)"mvt2", mvt2);
  feat_lib_add(fl, FEAT_TYPE_INT,   (char *)"delta2", delta2);

  feat_lib_add(fl, FEAT_TYPE_INT,   (char *)"mvt3", mvt3);
  feat_lib_add(fl, FEAT_TYPE_INT,   (char *)"delta3", delta3);

  return fl;
}

