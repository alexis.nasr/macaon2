#include<stdio.h>
#include"queue.h"
#include"util.h"

int queue_renumber_words(queue *bf)
{
  int i;
  int index = 1;
  for(i=0; i < bf->nbelem; i++){
    word_set_index(queue_elt_n(bf, i), index++);
  }
  return index;
}

int queue_read_sentence(queue *bf, FILE *f, mcd *mcd_struct)
{
  char buffer[10000];
  word *w = NULL;
  int index = 1;

  while(fgets(buffer, 10000, f)){
    if(feof(f)) break;
    /* fprintf(stderr, "%s", buffer);   */
    if((buffer[0] == '\n') || (buffer[0] == ' ') || (buffer[0] == '\t')) break; /* end of the sentence */
    w = word_parse_buffer(buffer, mcd_struct);
    word_set_index(w, index);
    index++;
    queue_add(bf, w);
    if(word_is_eos(w, mcd_struct)) break;
  }
  /* return bf->nbelem - 1; */ /* because of the dummy word */
  return bf->nbelem ; 
}

word *queue_elt_n(queue *q, int n)
{
  return (n >= q->nbelem) ? NULL : q->array[(q->head + n) % q->size];
}

void queue_print(FILE *f, queue *q)
{
  int i;
  fprintf(f, "(");
  if(q->tail >= q->head)
    for(i=q->head; i < q->tail; i++)
      fprintf(f, "%d ", word_get_index(q->array[i]));
  else{
    for(i=q->head; i < q->size; i++)
      fprintf(f, "%d ", word_get_index(q->array[i]));
    for(i=0; i < q->tail; i++)
      fprintf(f, "%d ", word_get_index(q->array[i]));
  }
  fprintf(f, ")\n");
}

queue *queue_new_full(int size)
{
  queue *q = (queue *)memalloc(sizeof(queue));
  q->size = size;
  q->array = (word **)memalloc(size * sizeof(word *));
  q->nbelem = q->head = q->tail = 0;

  return q;
}

queue *queue_new(void)
{
  return queue_new_full(10);
}

queue *queue_copy(queue *q)
{
  int i;
  queue *copy = queue_new_full(q->size);
  for(i=0; i < copy->size; i++)
    copy->array[i] = q->array[i];
  copy->head = q->head;
  copy->tail = q->tail;
  copy->nbelem = q->nbelem;
  return copy;
}

void queue_free(queue *q)
{
  if(q == NULL) return;
  free(q->array);
  free(q);
}

int queue_is_empty(queue *q)
{
  return (q->nbelem == 0);
}

void queue_add_in_front(queue *q, word *w)
{
  if(q->head == 0)
    q->head = q->size - 1;
  else
    q->head --;
  q->array[q->head] = w;
  q->nbelem++;
  if(q->tail == q->head){
    /* fprintf(stderr, "queue full ! increasing size\n"); */
    queue_double_size(q);
  }
}

void queue_double_size(queue *q)
{
  int i;
  queue *q2 = queue_new_full(q->size * 2);
  for(i=0; i < q->nbelem; i++)
    queue_add(q2, queue_elt_n(q, i));

  if(q->array) free(q->array);
  q->size = q2->size;
  q->array = q2->array;
  q->head = q2->head;
  q->tail = q2->tail;
  q->nbelem = q2->nbelem;

  free(q2);
} 

void queue_add(queue *q, word *w)
{
  q->array[q->tail] = w;
  if(q->tail == q->size-1)
    q->tail = 0;
  else
    q->tail++;
  q->nbelem++;
  
  if(q->tail == q->head){
    /* fprintf(stderr, "queue full ! increasing size\n"); */
    queue_double_size(q);
  }
}

word *queue_remove(queue *q)
{
  word *w;
  if(q->head == q->tail){
    fprintf(stderr, "queue empty !\n");
    return NULL;
  }
  w = q->array[q->head];
  if(q->head == q->size-1)
    q->head = 0;
  else
    q->head++;

  q->nbelem--;
  return w;
}
