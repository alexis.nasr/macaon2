#ifndef __MOVEMENT_PARSER_ARC_EAGER__
#define __MOVEMENT_PARSER_ARC_EAGER__

#include"config.h"
#include"dico.h"

#define MVT_PARSER_SHIFT 0
#define MVT_PARSER_REDUCE 1
#define MVT_PARSER_ROOT 2
#define MVT_PARSER_EOS 3
#define MVT_PARSER_LEFT 4
#define MVT_PARSER_RIGHT 5

/* even movements are left movements (except 0, which is shift and 2 which is root) */
#define movement_parser_left_code(label) (2 * (label) + 4)

/* odd movements are right movements  (except 1, which is reduce and 3 which is end_of_sentence) */
#define movement_parser_right_code(label) (2 * (label) + 5)

int movement_parser_type(int mvt);
int movement_parser_label(int mvt);

int movement_parser_left_arc(config *c, int label);
int movement_parser_left_arc_undo(config *c);
int movement_parser_right_arc(config *c, int label);
int movement_parser_right_arc_undo(config *c);
int movement_parser_shift(config *c);
int movement_parser_shift_undo(config *c);
int movement_parser_reduce(config *c);
int movement_parser_reduce_undo(config *c);
int movement_parser_root(config *c, int root_code);
int movement_parser_root_undo(config *c);
int movement_parser_eos(config *c);
int movement_parser_eos_undo(config *c);
int movement_parser_undo(config *c);
void movement_parser_print(FILE *f, int mvt_code, dico *dico_labels);

void movement_parser_sprint(char *f, int mvt_code, dico *dico_labels);

#endif
