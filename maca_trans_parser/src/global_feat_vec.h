#ifndef __GLOBAL_FEAT_VEC__
#define __GLOBAL_FEAT_VEC__

#include"feat_vec.h"

typedef struct {
  int pred_mvt;
  /* int oracle_mvt;*/
  feat_vec *fv;
} global_feat_vec_elt;


typedef struct {
  int nbelem;
  global_feat_vec_elt **array;
} global_feat_vec;


global_feat_vec *global_feat_vec_new(void);
void global_feat_vec_add(global_feat_vec *gfv, int pred_mvt, feat_vec *fv);
global_feat_vec *global_feat_vec_copy(global_feat_vec *gfv);
void global_feat_vec_free(global_feat_vec *gfv);
void global_feat_vec_print(global_feat_vec *gfv);


#endif
