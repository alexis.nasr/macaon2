#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<getopt.h>
#include"movement_tagger.h"
#include"oracle_tagger.h"
#include"feat_fct.h"
#include"feature_table.h"
#include"context.h"
#include"feat_vec.h"
#include"dico_vec.h"
#include"word_emb.h"
#include"config2feat_vec.h"

void add_signature_to_words_in_word_buffer(word_buffer *bf, form2pos *f2p)
{
  int i;
  word *w;
  char lower_form[100];

  for(i = word_buffer_get_nbelem(bf) - 1; i >=0  ; i--){
    w = word_buffer_get_word_n(bf, i);
    if(word_get_signature(w) != -1) break;
    w->signature = form2pos_get_signature(f2p, w->form);
    if(w->signature == -1){
      strcpy(lower_form, w->form);
      to_lower_string(lower_form);
      w->signature = form2pos_get_signature(f2p, lower_form);
    }
  }
}

void maca_trans_parser_mcf2cff_help_message(context *ctx)
{
  context_general_help_message(ctx);
  context_mode_help_message(ctx);
  context_sent_nb_help_message(ctx);
  context_mcd_help_message(ctx);

  fprintf(stderr, "INPUT\n");
  context_conll_help_message(ctx);
  fprintf(stderr, "IN TEST MODE\n");
  context_vocabs_help_message(ctx);

  fprintf(stderr, "OUTPUT\n");
  context_cff_help_message(ctx);
  fprintf(stderr, "IN TRAIN MODE\n");
  context_vocabs_help_message(ctx);


}

void maca_trans_parser_mcf2cff_check_options(context *ctx)
{
  if(!ctx->input_filename
     || ctx->help
     /* || !ctx->mcd_filename */
     || !(ctx->cff_filename || ctx->fann_filename)
     ){
    maca_trans_parser_mcf2cff_help_message(ctx);
    exit(1);
  }
}

void generate_training_file(FILE *output_file, context *ctx)
{  
  config *config_oracle;
  feat_vec *fv = feat_vec_new(feature_types_nb); 
  FILE *conll_file = myfopen(ctx->input_filename, "r");
  FILE *conll_file_predicted = myfopen(ctx->input_filename, "r");
  int postag_oracle;
  /* dico *dico_pos_oracle = dico_vec_get_dico(ctx->vocabs, (char *)"POS"); */

  feat_model *local_feat_model = feat_model_read("/home/alexis/maca_data2/fr/bin/maca_trans_tagger.fm", ctx->verbose);
  dico_vec *local_dico_vec = dico_vec_read("/home/alexis/maca_data2/fr/bin/maca_trans_tagger.vocab", ctx->hash_ratio);
  /* dico *dico_pos_local = dico_vec_get_dico(local_dico_vec, (char *)"POS"); */
  feature_table *local_ft =  feature_table_load("/home/alexis/maca_data2/fr/bin/maca_trans_tagger.model", ctx->verbose);
  dico *local_perceptron_features = dico_vec_get_dico(local_dico_vec, (char *)"d_perceptron_features");
  config *config_predicted;
  int postag_predicted;
  /* int i; */
  /* char *postag_oracle_string;   */
  /* char *postag_predicted_string;   */
  
  config_predicted = config_new(conll_file_predicted, ctx->mcd_struct, 5);
  config_oracle = config_new(conll_file, ctx->mcd_struct, 5);
  
  while(!config_is_terminal(config_oracle)){ 
    if(ctx->f2p){
      add_signature_to_words_in_word_buffer(config_oracle->bf, ctx->f2p);
      add_signature_to_words_in_word_buffer(config_predicted->bf, ctx->f2p);
    }
    
    postag_oracle = word_get_pos(word_buffer_b0(config_get_buffer(config_oracle)));
    /* postag_oracle_string = dico_int2string(dico_pos_oracle, postag_oracle); */
    
    config2feat_vec_cff(local_feat_model, config_predicted, local_perceptron_features, fv, ctx->mode);
    
    if(config_predicted->vcode_array)
      free(config_predicted->vcode_array);
    config_predicted->vcode_array = feature_table_get_vcode_array(fv, local_ft);
    
    postag_predicted = config_predicted->vcode_array[0].class_code;
    /* postag_predicted_string = dico_int2string(dico_pos_local, postag_predicted); */
    
    if(ctx->debug_mode){
      /* if(strcmp(postag_oracle_string, postag_predicted_string)){ */
      if(postag_oracle != postag_predicted){
	fprintf(stdout, "**************** DIFFERENTS ***********\n");
	fprintf(stdout, "%s\n", word_get_input(word_buffer_b0(config_get_buffer(config_oracle))));
      }
    }
    
    
    forward(config_predicted, postag_predicted);
    forward(config_oracle, postag_oracle);

    fprintf(output_file, "%d", postag_oracle);
    config2feat_vec_cff(ctx->features_model, config_predicted, ctx->d_perceptron_features, fv, ctx->mode);
    feat_vec_print(output_file, fv);
    word_set_pos(word_buffer_bm1(config_predicted->bf), postag_oracle); 
  }
}
#if 0
/* if(!strcmp(postag_oracle_string, postag_predicted_string)){ */
    if(postag_oracle == postag_predicted){
      fprintf(output_file, "0");
      config2feat_vec_cff(ctx->features_model, config_predicted, ctx->d_perceptron_features, fv, ctx->mode);
      feat_vec_print(output_file, fv);
      if(ctx->debug_mode){
        printf("CHOOSE 0\n"); 
      }
    }
    
    if(postag_oracle == postag_predicted)
      printf("CORRECT %d\n", delta2(config_predicted));
    else
      printf("WRONG %d\n", delta2(config_predicted));
    
    int choice = 1;
    /* while(strcmp(postag_oracle_string, postag_predicted_string) && (choice < 10)){ */
    while((postag_oracle != postag_predicted) && (choice < 10)){
      if(ctx->debug_mode){
	if(choice == 1){
	  fprintf(stdout, "%d postag oracle = %s postag predicted = %s\n",
		  word_buffer_get_current_index(config_get_buffer(config_oracle)),
		  dico_int2string(dico_pos_oracle, postag_oracle),
		  dico_int2string(dico_pos_local, postag_predicted));
	  
	  for(i=0; i < 5; i++)
	    fprintf(stdout, "%d\t%s\t%.4f\t%.4f\n", i,
		    dico_int2string(dico_pos_local, config_predicted->vcode_array[i].class_code),
		    config_predicted->vcode_array[i].score,
		    config_predicted->vcode_array[i].score - config_predicted->vcode_array[0].score);
	}
      /* fprintf(stdout, "CHOICE %d\n", choice); */
      }    
      postag_predicted = config_predicted->vcode_array[choice].class_code;
      /* postag_predicted_string = dico_int2string(dico_pos_local, postag_predicted); */
      
      /* if(!strcmp(postag_predicted_string, postag_oracle_string)){ */
      if(postag_predicted == postag_oracle){
	if(ctx->debug_mode){
	  printf("CHOOSE %d\n", choice);
	}
	fprintf(output_file, "%d", choice);
	config2feat_vec_cff(ctx->features_model, config_predicted, ctx->d_perceptron_features, fv, ctx->mode);
	feat_vec_print(output_file, fv);
	choice_n(config_predicted, choice);
      }
      choice++;
    }
  }
}
#endif
int main(int argc, char *argv[])
{
  context *ctx;
  FILE *output_file;
  
  ctx = context_read_options(argc, argv);
  maca_trans_parser_mcf2cff_check_options(ctx);
  
  ctx->features_model = feat_model_read(ctx->features_model_filename, ctx->verbose);

  /*  if(ctx->mode == TRAIN_MODE){
    mcd_extract_dico_from_corpus(ctx->mcd_struct, ctx->input_filename);
    ctx->vocabs = mcd_build_dico_vec(ctx->mcd_struct);
  }
  else if(ctx->mode == TEST_MODE){*/
    
  ctx->vocabs = dico_vec_read(ctx->vocabs_filename, ctx->hash_ratio);
  mcd_link_to_dico(ctx->mcd_struct, ctx->vocabs, ctx->verbose);
    
  /* } */
    
  feat_model_compute_ranges(ctx->features_model, ctx->mcd_struct, ctx->mvt_nb);
  
  /* in train mode create feature dictionnary for perceptron */
  if(ctx->mode == TRAIN_MODE)
    ctx->d_perceptron_features = dico_new((char *)"d_perceptron_features_bt", 10000000);
  
  /* in test mode read feature dictionnary for perceptron */
  if(ctx->mode == TEST_MODE)
    ctx->d_perceptron_features = dico_vec_get_dico(ctx->vocabs, (char *)"d_perceptron_features_bt");
  
  /* add the feature dictionnary to the dico vector */
  dico_vec_add(ctx->vocabs, ctx->d_perceptron_features);
  
  /* open output file */
  if(ctx->cff_filename)
    output_file = myfopen(ctx->cff_filename, "w");
  else
    output_file = stdout;
  
  generate_training_file(output_file, ctx);
    
  /* if(ctx->mode == TRAIN_MODE){ */
    /* dico_print(ctx->perceptron_features_filename, ctx->d_perceptron_features); */
    dico_vec_print(ctx->vocabs_filename, ctx->vocabs);
    
  /* } */
  
  if(ctx->cff_filename)
    fclose(output_file);
  context_free(ctx);
  return 0;
}

