#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<getopt.h>
#include<ctype.h>

#include"context.h"
#include"movement_chunker.h"
#include"feat_fct.h"
#include"config2feat_vec.h"
#include"feature_table.h"
#include"dico.h"
#include"mcd.h"

void print_word(word *w, mcd *mcd_struct, dico *dico_bio, int tag)
{
  char *buffer = NULL;
  char *token = NULL;
  int col_nb = 0;
  if(mcd_get_a_col(mcd_struct) == -1){
    printf("%s\t%s\n", w->input, dico_int2string(dico_bio, tag));
  }
  else{
    buffer = strdup(w->input);
    token = strtok(buffer, "\t");
    col_nb = 0;
    while(token){
      if(col_nb != 0) printf("\t");
      if(col_nb == mcd_get_a_col(mcd_struct))
	printf("%s", dico_int2string(dico_bio, tag));
      else
	word_print_col_n(stdout, w, col_nb);
      col_nb++;
      token = strtok(NULL, "\t");
    }
    if(col_nb <= mcd_get_a_col(mcd_struct))
      printf("\t%s", dico_int2string(dico_bio, tag));
    printf("\n");
    free(buffer);
  }
}

void simple_decoder_chunker(context *ctx)
{
  config *c;
  feat_vec *fv = feat_vec_new(feature_types_nb);
  FILE *f = (ctx->input_filename)? myfopen(ctx->input_filename, "r") : stdin;
  feature_table *ft =  feature_table_load(ctx->perc_model_filename, ctx->verbose);
  int tag;
  float max;
  word *b0;
  dico *dico_pos = dico_vec_get_dico(ctx->vocabs, (char *)"A");

  c = config_new(f, ctx->mcd_struct, 5); 

  while(!config_is_terminal(c)){
    b0 = word_buffer_b0(c->bf);
    tag = -1;//word_get_pos(b0);

    if(ctx->debug_mode){
      fprintf(stderr, "***********************************\n");
      config_print(stderr, c);
    }
    
    /* if tag is not specified in input it is predicted */
    if(tag == -1){
      /* config_print(stdout, c); */
      config2feat_vec_cff(ctx->features_model, c, ctx->d_perceptron_features, fv, LOOKUP_MODE);
      
      /* feat_vec_print(stdout, fv); */
      tag = feature_table_argmax(fv, ft, &max);
      /* printf("tag = %d\n", tag); */

      if(ctx->debug_mode){
	vcode *vcode_array = feature_table_get_vcode_array(fv, ft);
	for(int i=0; i < 3; i++){
	  fprintf(stderr, "%d\t", i);
	  fprintf(stderr, "%s\t%.4f\n", dico_int2string(dico_pos, vcode_array[i].class_code), vcode_array[i].score);
	}
	free(vcode_array);
      }
    }

    print_word(b0, ctx->mcd_struct, dico_pos, tag);
    
    movement_chunker(c, tag);

  }
  /* config_print(stdout, c);  */
  feat_vec_free(fv);
  feature_table_free(ft);
  config_free(c); 
  if (ctx->input_filename) fclose(f);
}

