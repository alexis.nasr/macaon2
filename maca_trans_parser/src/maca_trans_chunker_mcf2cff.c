#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<getopt.h>
#include"movement_chunker.h"
#include"oracle_chunker.h"
#include"feat_fct.h"
#include"context.h"
#include"feat_vec.h"
#include"dico_vec.h"
#include"word_emb.h"
#include"config2feat_vec.h"

void maca_trans_chunker_mcf2cff_help_message(context *ctx)
{
  context_general_help_message(ctx);
  context_mode_help_message(ctx);
  context_sent_nb_help_message(ctx);
  context_mcd_help_message(ctx);

  fprintf(stderr, "INPUT\n");
  context_conll_help_message(ctx);
  fprintf(stderr, "IN TEST MODE\n");
  context_vocabs_help_message(ctx);

  fprintf(stderr, "OUTPUT\n");
  context_cff_help_message(ctx);
  fprintf(stderr, "IN TRAIN MODE\n");
  context_vocabs_help_message(ctx);
}

void maca_trans_chunker_mcf2cff_check_options(context *ctx)
{
  if(!ctx->input_filename
     || ctx->help
     /* || !ctx->mcd_filename */
     || !(ctx->cff_filename || ctx->fann_filename)
     ){
    maca_trans_chunker_mcf2cff_help_message(ctx);
    exit(1);
  }
}

void generate_training_file(FILE *output_file, context *ctx)
{  
  config *c;
  feat_vec *fv = feat_vec_new(feature_types_nb);
  FILE *conll_file = myfopen(ctx->input_filename, "r");
  int tag;
  
  c = config_new(conll_file, ctx->mcd_struct, 5);

  while(!config_is_terminal(c)){ 
    config2feat_vec_cff(ctx->features_model, c, ctx->d_perceptron_features, fv, ctx->mode);
    tag = oracle_chunker(c);
    
    fprintf(output_file, "%d", tag);
    feat_vec_print(output_file, fv);
    movement_chunker(c, tag);
  }
}

int main(int argc, char *argv[])
{
  context *ctx;
  FILE *output_file;
  
  ctx = context_read_options(argc, argv);
  maca_trans_chunker_mcf2cff_check_options(ctx);
  
  ctx->features_model = feat_model_read(ctx->features_model_filename, feat_lib_build(), ctx->verbose);


  if(ctx->mode == TRAIN_MODE){
    mcd_extract_dico_from_corpus(ctx->mcd_struct, ctx->input_filename);
    ctx->vocabs = mcd_build_dico_vec(ctx->mcd_struct);
  }
  else if(ctx->mode == TEST_MODE){
    ctx->vocabs = dico_vec_read(ctx->vocabs_filename, ctx->hash_ratio);
    mcd_link_to_dico(ctx->mcd_struct, ctx->vocabs, ctx->verbose);
  }
    
  feat_model_compute_ranges(ctx->features_model, ctx->mcd_struct, ctx->mvt_nb);
  
  /* in train mode create feature dictionnary for perceptron */
  if(ctx->mode == TRAIN_MODE)
    ctx->d_perceptron_features = dico_new((char *)"d_perceptron_features", 10000000);
  
  /* in test mode read feature dictionnary for perceptron */
  if(ctx->mode == TEST_MODE)
    ctx->d_perceptron_features = dico_vec_get_dico(ctx->vocabs, (char *)"d_perceptron_features");
  
  /* add the feature dictionnary to the dico vector */
  dico_vec_add(ctx->vocabs, ctx->d_perceptron_features);
  
  /* open output file */
  if(ctx->cff_filename)
    output_file = myfopen(ctx->cff_filename, "w");
  else
    output_file = stdout;
  
  generate_training_file(output_file, ctx);
    
  if(ctx->mode == TRAIN_MODE){
    /* dico_print(ctx->perceptron_features_filename, ctx->d_perceptron_features); */
    dico_vec_print(ctx->vocabs_filename, ctx->vocabs);
    
  }
  
  if(ctx->cff_filename)
    fclose(output_file);
  context_free(ctx);
  return 0;
}

