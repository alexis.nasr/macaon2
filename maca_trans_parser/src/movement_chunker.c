#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"util.h"
#include"movement_chunker.h"

int movement_chunker(config *c, int postag)
{
  word_set_pos(word_buffer_b0(c->bf), postag); 
  word_buffer_move_right(c->bf);

  return 1;
}
