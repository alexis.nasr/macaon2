#ifndef __SIMPLE_DECODER_TAGGER__
#define __SIMPLE_DECODER_TAGGER__
#include "context.h"

void add_signature_to_words_in_word_buffer(word_buffer *bf, form2pos *f2p);
void simple_decoder_tagger(context *ctx);

#endif
