#pragma once

//#define USE_CBLAS
#include <cstdio>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <iostream>

#ifdef USE_CBLAS
extern "C" {
#include <cblas.h>
}
#endif

#define error(...) fprintf(stderr, __VA_ARGS__);

typedef enum {
    BOTH, ROWS, COLS
} Axis;

template <class T>
class MatrixRow {
    private:
        T* values;
        int cols;
    public:
        MatrixRow(T* _values, int _cols) : values(_values), cols(_cols) { }
        T& operator[](int y) {
            return *(values + y);
        }
        MatrixRow& operator=(const MatrixRow& other) {
            assert(cols == other.cols);
            memcpy(values, other.values, cols * sizeof(T));
            return *this;
        }
        MatrixRow slice(int offset, int length) const {
            assert(offset + length <= cols);
            return MatrixRow(values + offset, length);
        }

};

template <class T>
class Matrix {
    public:
        int rows;
        int cols;
    private:
        T* values;
    public:
        Matrix() : rows(0), cols(0), values(NULL) { }
        Matrix(int _rows, int _cols) : rows(_rows), cols(_cols) {
            if(rows * cols > 0) {
                values = new T[rows * cols];
            } else {
                values = NULL;
            }
        }
        ~Matrix() {
            if(values != NULL) delete[] values;
        }
        Matrix<T>(const Matrix<T>& other) : rows(other.rows), cols(other.cols) {
            if(rows * cols > 0) {
                values = new T[rows * cols];
                memcpy(values, other.values, sizeof(T) * rows * cols);
            } else {
                values = NULL;
            }
        }

        void info(const char* name) const {
            printf("%s: %dx%d\n", name, rows, cols);
        }

        void print(const char* name = NULL, const char* format = "%9f") const {
            if(name != NULL) info(name);
            for(int i = 0; i < rows; i++) {
                for(int j = 0; j < cols; j++) {
                    printf(format, at(i, j));
                    printf(" ");
                }
                printf("\n");
            }
        }

        static Matrix zeros(int rows, int cols) {
            Matrix result(rows, cols);
            bzero(result.values, sizeof(T) * rows * cols);
            return result;
        }

        static Matrix ones(int rows, int cols) {
            Matrix result(rows, cols);
            for(int i = 0; i < rows * cols; i++) result.values[i] = 1;
            return result;
        }

        static Matrix rand(int rows, int cols) {
            Matrix result(rows, cols);
            for(int i = 0; i < rows * cols; i++) result.values[i] = (2.0f * std::rand()) / RAND_MAX - 1.0f;
            return result;
        }

        static Matrix zeros_like(const Matrix<T>& other) {
            return zeros(other.rows, other.cols);
        }

        static Matrix ones_like(const Matrix<T>& other) {
            return ones(other.rows, other.cols);
        }

        Matrix<T> broadcast(const Matrix<T>& other) const {
            if(rows == other.rows && cols == other.cols) return *this;
            assert(other.cols % cols == 0 && other.rows % rows == 0);
            Matrix result(other.rows, other.cols);
            for(int i = 0; i < other.rows; i++) {
                for(int j = 0; j < other.cols; j++) {
                    result.at(i, j) = at(i % rows, j % cols);
                }
            }
            return result;
        }

        Matrix<T> slice(int start_row, int end_row) const {
            return slice(start_row, end_row, 0, cols);
        }
        Matrix<T> slice(int start_row, int end_row, int start_col, int end_col) const {
            Matrix<T> result(end_row - start_row, end_col - start_col);
            for(int i = 0; i < end_row - start_row; i++) {
                result[i] = (*this)[i + start_row].slice(start_col, end_col);
            }
            return result;
        }

        /*Matrix<T>& operator=(const T& value) {
            for(int i = 0; i < rows * cols; i++) values[i] = value;
        }*/

        const Matrix<T>& operator=(const Matrix<T>& other) {
            if(cols != other.cols || rows != other.rows) {
                delete[] values;
                cols = other.cols;
                rows = other.rows;
                values = new T[cols * rows];
            }
            memcpy(values, other.values, sizeof(T) * rows * cols);
            return *this;
        }

        MatrixRow<T> operator[](int x) {
            return MatrixRow<T>(values + x * cols, cols);
        }
        const MatrixRow<T> operator[](int x) const {
            return MatrixRow<T>(values + x * cols, cols);
        }
        T& at(int x, int y) {
            return values[x * cols + y];
        }
        const T& at(int x, int y) const {
            return values[x * cols + y];
        }
        // matrix-scalar operations
        Matrix<T> operator-(T a) const {
            assert(a.rows == rows && a.cols == cols);
            Matrix<T> result(rows, cols);
            for(int i = 0; i < rows * cols; i++) result.values[i] = values[i] - a;
            return result;
        }
        Matrix<T> operator+(T a) const {
            Matrix<T> result(rows, cols);
            for(int i = 0; i < rows * cols; i++) result.values[i] = a + values[i];
            return result;
        }
        Matrix<T> operator*(T a) const {
            assert(a.rows == rows && a.cols == cols);
            Matrix<T> result(rows, cols);
            for(int i = 0; i < rows * cols; i++) result.values[i] = a * values[i];
            return result;
        }
        Matrix<T> operator/(T a) const {
            assert(a.rows == rows && a.cols == cols);
            Matrix<T> result(rows, cols);
            for(int i = 0; i < rows * cols; i++) result.values[i] = values[i] / a;
            return result;
        }
        // matrix-matrix operations
        Matrix<T> operator-(const Matrix<T>& other) const {
            Matrix<T> a = other.broadcast(*this);
            Matrix<T> result(rows, cols);
            for(int i = 0; i < rows * cols; i++) result.values[i] = values[i] - a.values[i];
            return result;
        }
        Matrix<T> operator+(const Matrix<T>& other) const {
            Matrix<T> a = other.broadcast(*this);
            Matrix<T> result(rows, cols);
            for(int i = 0; i < rows * cols; i++) result.values[i] = a.values[i] + values[i];
            return result;
        }
        Matrix<T> operator*(const Matrix<T>& other) const {
            Matrix<T> a = other.broadcast(*this);
            Matrix<T> result(rows, cols);
            for(int i = 0; i < rows * cols; i++) result.values[i] = a.values[i] * values[i];
            return result;
        }
        Matrix<T> operator/(const Matrix<T>& other) const {
            Matrix<T> a = other.broadcast(*this);
            Matrix<T> result(rows, cols);
            for(int i = 0; i < rows * cols; i++) result.values[i] = values[i] / a.values[i];
            return result;
        }

        Matrix<T> dot(const Matrix<T>& a) const {
            assert(a.rows == cols);
            Matrix<T> result = zeros(rows, a.cols);
#ifdef USE_CBLAS
            cblas_sgemm(  CblasRowMajor, CblasNoTrans, CblasNoTrans,   rows, a.cols, cols, 1,     values, cols, a.values, a.cols, 1,    result.values, result.cols);
#else
            for(int i = 0; i < rows; i++)
                for(int j = 0; j < a.cols; j++)
                    for(int k = 0; k < cols; k++)
                        result.at(i, j) += at(i, k) * a.at(k, j);
                        //result.values[i * result.cols + j] += values[i * cols + k] * a.values[k * a.cols + j];
#endif
            return result;
        }
        Matrix<T> transpose() const {
            Matrix<T> result(cols, rows);
            for(int i = 0; i < cols; i++)
                for(int j = 0; j < rows; j++)
                    result.values[i * result.rows + j] = values[j * cols + i];
            return result;
        }

        Matrix<T> sum(const Axis axis=BOTH) const {
            if(axis == BOTH) {
                Matrix<T> result = zeros(1, 1);
                for(int i = 0; i < rows * cols; i++) result[0][0] += values[i];
                return result;
            } else if(axis == ROWS) {
                Matrix<T> result = zeros(1, cols);
                for(int i = 0; i < rows; i++)
                    for(int j = 0; j < cols; j++)
                        result.values[j] += values[i * cols + j];
                return result;
            } else if(axis == COLS) {
                Matrix<T> result = zeros(rows, 1);
                for(int i = 0; i < rows; i++)
                    for(int j = 0; j < cols; j++)
                        result.values[i] += values[i * cols + j];
                return result;
            }
            std::cerr << "ERROR: unsupported axis for Matrix::sum\n";
            exit(1);
            return zeros(0, 0);
        }
        Matrix<T> max(const Axis axis=BOTH) const {
            if(axis == BOTH) {
                Matrix<T> result = zeros(1, 1);
                for(int i = 0; i < rows * cols; i++) if(i == 0 || result.values[0] < values[i]) result.values[0] = values[i];
                return result;
            } else if(axis == ROWS) {
                Matrix<T> result = zeros(1, cols);
                for(int i = 0; i < rows; i++)
                    for(int j = 0; j < cols; j++)
                        if(j == 0 || result.values[j] < values[i * cols + j]) result.values[j] = values[i * cols + j];
                return result;
            } else if(axis == COLS) {
                Matrix<T> result = zeros(rows, 1);
                for(int i = 0; i < rows; i++)
                    for(int j = 0; j < cols; j++)
                        if(i == 0 || result.values[i] < values[i * cols + j]) result.values[i] = values[i * cols + j];
                return result;
            }
            std::cerr << "ERROR: unsupported axis for Matrix::max\n";
            exit(1);
            return zeros(0, 0);
        }
        Matrix<T> min(const Axis axis=BOTH) const {
            if(axis == BOTH) {
                Matrix<T> result = zeros(1, 1);
                for(int i = 0; i < rows * cols; i++) if(i == 0 || result.values[0] > values[i]) result.values[0] = values[i];
                return result;
            } else if(axis == ROWS) {
                Matrix<T> result = zeros(1, cols);
                for(int i = 0; i < rows; i++)
                    for(int j = 0; j < cols; j++)
                        if(j == 0 || result.values[j] > values[i * cols + j]) result.values[j] = values[i * cols + j];
                return result;
            } else if(axis == COLS) {
                Matrix<T> result = zeros(rows, 1);
                for(int i = 0; i < rows; i++)
                    for(int j = 0; j < cols; j++)
                        if(i == 0 || result.values[i] > values[i * cols + j]) result.values[i] = values[i * cols + j];
                return result;
            }
            std::cerr << "ERROR: unsupported axis for Matrix::min\n";
            exit(1);
            return zeros(0, 0);
        }
        Matrix<T> argmax(const Axis axis=COLS) const {
            if(axis == ROWS) {
                Matrix<T> result = zeros(1, cols);
                Matrix<T> max = zeros(1, cols);
                for(int i = 0; i < rows; i++) {
                    for(int j = 0; j < cols; j++) {
                        if(i == 0 || max.at(0, j) < at(i, j)) {
                            max.at(0, j) = at(i, j);
                            result.at(0, j) = i;
                        }
                    }
                }
                return result;
            } else if(axis == COLS) {
                Matrix<T> result = zeros(rows, 1);
                for(int i = 0; i < rows; i++) {
                    double max = 0;
                    for(int j = 0; j < cols; j++) {
                        if(j == 0 || max < at(i, j)) {
                            max = at(i, j);
                            result.at(i, 0) = j;
                        }
                    }
                }
                return result;
            }
            std::cerr << "ERROR: unsupported axis for Matrix::argmax\n";
            return zeros(0, 0);
        }

        void load(FILE* fp) {
            if(fread(&rows, sizeof(int), 1, fp) != 1) error("loading number of rows from fp\n");
            if(fread(&cols, sizeof(int), 1, fp) != 1) error("loading number of cols from fp\n");
            values = new T[rows * cols];
            if(fread(values, sizeof(T), rows * cols, fp) != (size_t) (rows * cols)) {
                error("loading %dx%d matrix from fp\n", rows, cols);
            }
        }

        void save(FILE* fp) {
            fwrite(&rows, sizeof(int), 1, fp);
            fwrite(&cols, sizeof(int), 1, fp);
            if(fwrite(values, sizeof(T), rows * cols, fp) != (size_t) (rows * cols)) {
                error("saving %dx%d matrix to fp\n", rows, cols);
            }
        }

        Matrix<T> apply(T (*function)(T)) const {
            Matrix<T> output(rows, cols);
            for(int i = 0; i < rows * cols; i++) output.values[i] = function(values[i]);
            return output;
        }

        class Function {
            public:
            static T sigmoid(const T a) { return std::tanh(a * 0.5) * 0.5 + 0.5; }
            static T hard_sigmoid(const T a) { T tmp = a * 0.2 + 0.5; return tmp < 0 ? 0 : tmp > 1 ? 1 : tmp; }
            static T identity(const T a) { return a; }
            static T tanh(const T a) { return std::tanh(a); }
            static T exp(const T a) { return std::exp(a); }
            static T log(const T a) { return std::log(a); }
            static T relu(const T a) { return a > 0 ? a : 0; }
        };

        static Matrix<T> sigmoid(const Matrix<T>& x) { return x.apply(Function::sigmoid); }
        static Matrix<T> hard_sigmoid(const Matrix<T>& x) { return x.apply(Function::hard_sigmoid); }
        static Matrix<T> identity(const Matrix<T>& x) { return x.apply(Function::identity); }
        static Matrix<T> tanh(const Matrix<T>& x) { return x.apply(Function::tanh); }
        static Matrix<T> exp(const Matrix<T>& x) { return x.apply(Function::exp); }
        static Matrix<T> log(const Matrix<T>& x) { return x.apply(Function::log); }
        static Matrix<T> relu(const Matrix<T>& x) { return x.apply(Function::relu); }
        static Matrix<T> softmax(const Matrix<T>& x) {
            Matrix<T> r = Matrix::exp(x - x.max(COLS));
            return r / r.sum(COLS);
        }
        /*Matrix<T> operator=(Matrix<float> x) {
            Matrix<T> result(x.rows, x.cols);
            for(int i = 0; i < x.rows; i++)
                for(int j = 0; j < y.cols; j++)
                    result.at(i, j) = (T) x.at(i, j);
            return result;
        }*/
};

