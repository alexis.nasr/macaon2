#ifndef __MVT__
#define __MVT__

#include<stdio.h>
#include<stdlib.h>
#include"word.h"

#define mvt_get_type(m) ((m)->type)
#define mvt_set_type(m,v) ((m)->type = (v))

#define mvt_get_gov(m) ((m)->gov)
#define mvt_set_gov(m,v) ((m)->gov = (v))

#define mvt_get_dep(m) ((m)->dep)
#define mvt_set_dep(m,v) ((m)->dep = (v))

typedef struct {
  int type; /* type of the movement */
  word* gov;  /* first argument of the movement (governor if it is a dependency creation movement) */
  word* dep;  /* second argument of the movement (dependent if it is a dependency creation movement) */
} mvt;

mvt *mvt_new(int type, word* gov, word* dep);
void mvt_free(mvt *m);
void mvt_print(FILE *f, mvt *m);

#endif
