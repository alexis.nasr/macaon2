#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<unistd.h>
#include<getopt.h>
#include "feat_types.h"
#include "context.h"
#include "util.h"

void context_free(context *ctx)
{
  if(ctx->program_name)            free(ctx->program_name);
  if(ctx->input_filename)          free(ctx->input_filename);
  if(ctx->perc_model_filename)     free(ctx->perc_model_filename);
  if(ctx->dnn_model_filename)      free(ctx->dnn_model_filename);
  if(ctx->cff_filename)            free(ctx->cff_filename);
  if(ctx->fann_filename)           free(ctx->fann_filename);
  if(ctx->mcd_filename)            free(ctx->mcd_filename);
  if(ctx->stag_desc_filename)      free(ctx->stag_desc_filename);
  if(ctx->features_model_filename) free(ctx->features_model_filename);
  if(ctx->f2p_filename)            free(ctx->f2p_filename);
  if(ctx->maca_data_path)          free(ctx->maca_data_path);
  if(ctx->language)                free(ctx->language);
  if(ctx->root_label)              free(ctx->root_label);
  if(ctx->vocabs_filename)         free(ctx->vocabs_filename);
  if(ctx->fplm_filename)           free(ctx->fplm_filename);
  if(ctx->json_filename)           free(ctx->json_filename);

  if (ctx->mcd_struct)
    mcd_free(ctx->mcd_struct);
    
  if (ctx->vocabs) 
    dico_vec_free(ctx->vocabs);
    
  if(ctx->d_perceptron_features)
    dico_free(ctx->d_perceptron_features);
  
  if(ctx->d_perceptron_features_error)
    dico_free(ctx->d_perceptron_features_error);
  /*
    if(ctx->mcd_struct)
    mcd_free(ctx->mcd_struct);
  */
  if(ctx->features_model)
    feat_model_free(ctx->features_model);

  if(ctx->features_model_error)
    feat_model_free(ctx->features_model_error);

  if(ctx->f2p)
    form2pos_free(ctx->f2p);

  free(ctx);
}

context *context_new(void)
{
  context *ctx = (context *)memalloc(sizeof(context));

  ctx->smin = 1;
  ctx->smax = 1;
  ctx->force = 0;
  ctx->nb_classes = 6; 
  ctx->verbose = 0;
  ctx->program_name = NULL;
  ctx->input_filename = NULL;
  ctx->perc_model_filename = NULL;
  ctx->dnn_model_filename = NULL;
  ctx->cff_filename = NULL;
  ctx->fann_filename = NULL;
  ctx->stag_desc_filename = NULL;
  ctx->mcd_filename = NULL;
  ctx->features_model_filename = NULL;
  ctx->vocabs_filename = NULL;  
  ctx->f2p_filename = NULL;  
  ctx->fplm_filename = NULL;

  ctx->maca_data_path = NULL;  
  ctx->language = strdup("fr");

  ctx->root_label = strdup("root");
  ctx->d_perceptron_features = NULL;
  ctx->d_perceptron_features_error = NULL;
  ctx->mcd_struct = NULL;
  ctx->features_model = NULL;
  ctx->features_model_error = NULL;
  ctx->vocabs = NULL;
  ctx->dico_labels = NULL;
  ctx->dico_postags = NULL;
  ctx->f2p = NULL;

  ctx->iteration_nb = 4;
  ctx->debug_mode = 0;
  ctx->feature_cutoff = 0;
  ctx->help = 0;
  ctx->hash_ratio = 0.5;
  ctx->mode = TRAIN_MODE;
  ctx->beam_width = 1;
  ctx->sent_nb = 1000000;
  ctx->hidden_neurons_nb = 100;
  ctx->stream_mode = 1;

  ctx->conll = 0;
  ctx->ifpls = 1;

  ctx->trace_mode = 0;


  ctx->json_filename = NULL;
  ctx->dnn_model_filename = NULL;
  ctx->l_rules_filename = NULL;
  
  return ctx;
}

void context_general_help_message(context *ctx)
{
  fprintf(stderr, "usage: %s [options]\n", ctx->program_name);
  fprintf(stderr, "Options:\n");
  fprintf(stderr, "\t-h --help                 : print this message\n");
  fprintf(stderr, "\t-v --verbose              : activate verbose mode\n");
  fprintf(stderr, "\t-r --hratio    <float>    : set the occupation ratio of hash tables (default is 0.5)\n");
  fprintf(stderr, "\t-D --maca_data_path <str> : path to the maca_data directory\n");
  fprintf(stderr, "\t-L --language    <str>    : identifier of the language to use (default is fr)\n");
}

void context_model_help_message(context *ctx){
  fprintf(stderr, "\t-m --model      <file> : model file name\n");
}
void context_input_help_message(context *ctx){
  fprintf(stderr, "\t-i --input      <file> : input file name (default is stdin)\n");
}
void context_iterations_help_message(context *ctx){
  fprintf(stderr, "\t-n --iter        <int> : number of iterations  (default is 4)\n");
}
void context_cff_help_message(context *ctx){
  fprintf(stderr, "\t-x --cff        <file> : CFF format file name\n");
}
void context_fann_help_message(context *ctx){
  fprintf(stderr, "\t-f --fann      <file>  : FANN format file name\n");
}
void context_conll_help_message(context *ctx){
  fprintf(stderr, "\t-c --conll             : input is in conll07 format\n");
}
void context_cutoff_help_message(context *ctx){
  fprintf(stderr, "\t-u --cutoff      <int> : cutoff value\n");
}
void context_mode_help_message(context *ctx){
  fprintf(stderr, "\t-M --mode              : TEST|TRAIN\n");
}
void context_beam_help_message(context *ctx){
  fprintf(stderr, "\t-b --beam        <int> : beam width (default is 1)\n");
}
void context_sent_nb_help_message(context *ctx){
  fprintf(stderr, "\t-s --sent_nb     <int> : number of sentences to process (default is 1000000)\n");
}
void context_mcd_help_message(context *ctx){
  fprintf(stderr, "\t-C --mcd        <file> : multi column description file name\n");
}
void context_features_model_help_message(context *ctx){
  fprintf(stderr, "\t-F --feat_model <file> : feature model file name\n");
}
void context_stream_help_message(context *ctx){
  fprintf(stderr, "\t-S --stream            : steam mode\n");
}
void context_vocabs_help_message(context *ctx){
  fprintf(stderr, "\t-V --vocabs     <file> : vocabularies file\n");
}
void context_language_help_message(context *ctx){
  fprintf(stderr, "\t-L --language          : identifier of the language to use\n");
}
void context_maca_data_path_help_message(context *ctx){
  fprintf(stderr, "\t-D --maca_data_path    : path to maca_data directory\n");
}
void context_root_label_help_message(context *ctx){
  fprintf(stderr, "\t-R --root_label  <str> : name of the root label (default is \"root\")\n");
}
void context_f2p_filename_help_message(context *ctx){
  fprintf(stderr, "\t-P --f2p        <file> : form to pos (f2p) filename\n");
}
void context_trace_mode_help_message(context *ctx){
  fprintf(stderr, "\t-T --traces            : activate trace mode (default is false)\n");
}
void context_debug_help_message(context *ctx){
  fprintf(stderr, "\t-d --debug            : activate debug mode (default is false)\n");
}
void context_json_help_message(context *ctx){
  fprintf(stderr, "\t-J --json             : json description of keras model\n");
}
void context_dnn_model_help_message(context *ctx){
  fprintf(stderr, "\t-N --dnn_model        : weight file for dnn\n");
}



context *context_read_options(int argc, char *argv[])
{
  int c;
  int option_index = 0;
  context *ctx = context_new();

  ctx->program_name = strdup(argv[0]);

  static struct option long_options[30] =
    {
      {"help",                no_argument,       0, 'h'},
      {"force",               no_argument,       0, 'K'},
      {"verbose",             no_argument,       0, 'v'},
      {"debug",               no_argument,       0, 'd'},
      {"conll",               no_argument,       0, 'c'},
      {"classes",             required_argument, 0, 'q'},
      {"smin",                required_argument, 0, 'A'},
      {"smax",                required_argument, 0, 'B'}, 
      {"model",               required_argument, 0, 'm'}, 
      {"input",               required_argument, 0, 'i'},
      {"iter",                required_argument, 0, 'n'},
      {"cff",                 required_argument, 0, 'x'},
      {"cutoff",              required_argument, 0, 'u'},
      {"hratio",              required_argument, 0, 'r'},
      {"mode",                required_argument, 0, 'M'},
      {"beam",                required_argument, 0, 'b'},
      {"fann",                required_argument, 0, 'f'},
      {"sent_nb",             required_argument, 0, 's'},
      {"mcd",                 required_argument, 0, 'C'}, 
      {"feat_model",          required_argument, 0, 'F'}, 
      {"vocabs",              required_argument, 0, 'V'}, 
      {"language",            required_argument, 0, 'L'},
      {"maca_data_path",      required_argument, 0, 'D'},
      {"root_label",          required_argument, 0, 'R'},
      {"f2p",                 required_argument, 0, 'P'},
      {"traces",              required_argument, 0, 'T'},
      {"json",                required_argument, 0, 'J'},
      {"dnn_model",           required_argument, 0, 'N'},
      {"l_rules",             required_argument, 0, 'l'},
      {"fplm",                required_argument, 0, 'w'}
    };
  optind = 0;
  opterr = 0;
  
  
  while ((c = getopt_long (argc, argv, "hKvdcSTm:i:A:B:n:x:q:u:r:M:b:f:s:C:F:V:L:D:R:P:J:N:w:l:", long_options, &option_index)) != -1){ 
    switch (c)
      {
      case 'A': 
        ctx->smin = atoi(optarg);
        break;
      case 'B': 
        ctx->smax = atoi(optarg);
        break;
      case 'q': 
        ctx->nb_classes = atoi(optarg);
        break;
      case 'h':
        ctx->help = 1;
        break;
      case 'K' :
        ctx->force = 1;
        break;
      case 'v':
        ctx->verbose = 1;
        break;
      case 'd':
        ctx->debug_mode = 1;
        break;
      case 'c':
        ctx->conll = 1;
        break;
      case 'T':
        ctx->trace_mode = 1;
        break;
      case 'm':
        ctx->perc_model_filename = strdup(optarg);
        break;
      case 'i':
        ctx->input_filename = strdup(optarg);
        break;
      case 'n':
        ctx->iteration_nb = atoi(optarg);
        break;
      case 'x':
        ctx->cff_filename = strdup(optarg);
        break;
      case 'w':
        ctx->fplm_filename = strdup(optarg);
        break;
      case 'u':
        ctx->feature_cutoff = atoi(optarg);
        break;
      case 'r':
        ctx->hash_ratio = atof(optarg);
        break;
      case 'M':
        ctx->mode = (!strcmp(optarg, "TEST"))? TEST_MODE : TRAIN_MODE;
        break;
      case 'b':
        ctx->beam_width = atoi(optarg);
        break;
      case 'f':
        ctx->fann_filename = strdup(optarg);
        break;
      case 'l':
        ctx->l_rules_filename = strdup(optarg);
        break;
      case 's':
        ctx->sent_nb = atoi(optarg);
        break;
      case 'C':
        ctx->mcd_filename = strdup(optarg);
        break;
      case 'F':
        ctx->features_model_filename = strdup(optarg);
        break;
      case 'V':
        ctx->vocabs_filename = strdup(optarg);
        break;
      case 'L':
        if (ctx->language) free(ctx->language); // libérer le default (strdup("fr") )
        ctx->language = strdup(optarg);
        break;
      case 'D':
        ctx->maca_data_path = strdup(optarg);
        break;
      case 'R':
        if (ctx->root_label) free(ctx->root_label); // libérer le default (strdup("root") )
        ctx->root_label = strdup(optarg);
        break;
      case 'P':
        ctx->f2p_filename = strdup(optarg);
        if(!strcmp(ctx->f2p_filename, "_") || !strcmp(ctx->f2p_filename, "NULL"))
          ctx->f2p = NULL;
        else
          ctx->f2p = form2pos_read(ctx->f2p_filename);
        break;
      case 'N':
        ctx->dnn_model_filename = strdup(optarg);
        break;
      case 'J':
        ctx->json_filename = strdup(optarg);
        break;
      }
  }

  if(ctx->mcd_filename)
    ctx->mcd_struct = mcd_read(ctx->mcd_filename, ctx->verbose);
  else
    ctx->mcd_struct = mcd_build_wpmlgfs();
  /* ctx->mcd_struct = mcd_build_wplgfs(); */


  /* initialize maca_data_path field */
  
  char absolute_path[500];
  absolute_path[0] = '\0';
  if(ctx->maca_data_path){
    strcpy(absolute_path, ctx->maca_data_path);
    free(ctx->maca_data_path);
  }
  else {
    char *e = getenv("MACAON_DIR");
    if (e != NULL) {
	  strcpy(absolute_path, e);	  
    } else {
	  fprintf(stderr, "WARNING: the environment variable MACAON_DIR is not defined\n");
    }
  }
  strcat(absolute_path, "/");
  strcat(absolute_path, ctx->language);
  strcat(absolute_path, "/bin/");
  ctx->maca_data_path = strdup(absolute_path);

  
  return ctx;
}


