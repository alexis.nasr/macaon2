#ifndef __CONTEXT__
#define __CONTEXT__

#define TEST_MODE 1
#define TRAIN_MODE 2

#define DEFAULT_MULTI_COL_DESC_FILENAME "maca_trans_parser.mcd" 
#define DEFAULT_FEATURES_MODEL_FILENAME "maca_trans_parser.fm" 
#define DEFAULT_VOCABS_FILENAME "maca_trans_parser.vocab" 
#define DEFAULT_MODEL_FILENAME  "maca_trans_parser.model" 

#define DEFAULT_MULTI_COL_DESC_TAGGER_FILENAME "maca_trans_tagger.mcd" 
#define DEFAULT_FEATURES_MODEL_TAGGER_FILENAME "maca_trans_tagger.fm" 
#define DEFAULT_VOCABS_TAGGER_FILENAME "maca_trans_tagger.vocab" 
#define DEFAULT_MODEL_TAGGER_FILENAME  "maca_trans_tagger.model"

#define DEFAULT_MULTI_COL_DESC_CHUNKER_FILENAME "maca_trans_chunker.mcd" 
#define DEFAULT_FEATURES_MODEL_CHUNKER_FILENAME "maca_trans_chunker.fm" 
#define DEFAULT_VOCABS_CHUNKER_FILENAME "maca_trans_chunker.vocab" 
#define DEFAULT_MODEL_CHUNKER_FILENAME  "maca_trans_chunker.model"

#define DEFAULT_MULTI_COL_DESC_TAGGER_ERROR_PREDICTOR_FILENAME "maca_error_predictor_tagger.mcd" 
#define DEFAULT_FEATURES_MODEL_TAGGER_ERROR_PREDICTOR_FILENAME "maca_error_predictor_tagger.fm" 
#define DEFAULT_VOCABS_TAGGER_ERROR_PREDICTOR_FILENAME "maca_error_predictor_tagger.vocab" 
#define DEFAULT_MODEL_TAGGER_ERROR_PREDICTOR_FILENAME  "maca_error_predictor_tagger.model"

#define DEFAULT_MULTI_COL_DESC_PARSER_ERROR_PREDICTOR_FILENAME "maca_error_predictor_parser.mcd" 
#define DEFAULT_FEATURES_MODEL_PARSER_ERROR_PREDICTOR_FILENAME "maca_error_predictor_parser.fm" 
#define DEFAULT_VOCABS_PARSER_ERROR_PREDICTOR_FILENAME "maca_error_predictor_parser.vocab" 
#define DEFAULT_MODEL_PARSER_ERROR_PREDICTOR_FILENAME  "maca_error_predictor_parser.model"

#define DEFAULT_MULTI_COL_DESC_LEMMATIZER_FILENAME "maca_trans_lemmatizer.mcd" 
#define DEFAULT_FEATURES_MODEL_LEMMATIZER_FILENAME "maca_trans_lemmatizer.fm" 
#define DEFAULT_VOCABS_LEMMATIZER_FILENAME "maca_trans_lemmatizer.vocab" 
#define DEFAULT_MODEL_LEMMATIZER_FILENAME  "maca_trans_lemmatizer.model" 
#define DEFAULT_RULES_LEMMATIZER_FILENAME  "maca_trans_lemmatizer_rules.txt" 
#define DEFAULT_EXCEPTIONS_LEMMATIZER_FILENAME  "maca_trans_lemmatizer_exceptions.fplm" 

#define DEFAULT_MULTI_COL_DESC_MORPHO_FILENAME "maca_trans_morpho.mcd" 
#define DEFAULT_FEATURES_MODEL_MORPHO_FILENAME "maca_trans_morpho.fm" 
#define DEFAULT_VOCABS_MORPHO_FILENAME "maca_trans_morpho.vocab" 
#define DEFAULT_MODEL_MORPHO_FILENAME  "maca_trans_morpho.model" 

#define DEFAULT_MULTI_COL_DESC_TAGPARSER_FILENAME "maca_trans_tagparser.mcd" 
#define DEFAULT_FEATURES_MODEL_TAGPARSER_FILENAME "maca_trans_tagparser.fm" 
#define DEFAULT_VOCABS_TAGPARSER_FILENAME "maca_trans_tagparser.vocab" 
#define DEFAULT_MODEL_TAGPARSER_FILENAME  "maca_trans_tagparser.model" 

#define DEFAULT_MULTI_COL_DESC_PARSER_NN_FILENAME "maca_trans_parser_nn.mcd" 
#define DEFAULT_FEATURES_MODEL_PARSER_NN_FILENAME "maca_trans_parser_nn.fm" 
#define DEFAULT_VOCABS_PARSER_NN_FILENAME "maca_trans_parser_nn.vocab" 
#define DEFAULT_MODEL_PARSER_NN_FILENAME  "maca_trans_parser_nn.weights" 
#define DEFAULT_JSON_PARSER_NN_FILENAME  "maca_trans_parser_nn.json" 

#define DEFAULT_PATH_RELAT "../data/treebank/"
#define DEFAULT_MCF_DEV "dev.mcf"
#define DEFAULT_MCF_TRAIN "train.mcf"
#define DEFAULT_MCF_TEST "test.mcf"

#define DEFAULT_F2P_FILENAME "fP" 
#define DEFAULT_FPLM_FILENAME "fplm" 

#include "dico_vec.h"
#include "feat_model.h"
#include "mcd.h"
#include "stdlib.h"
#include "form2pos.h"

typedef struct {
  int help;
  int force;
  int smin;
  int smax;
  int nb_classes;
  char *program_name;
  char *input_filename;
  char *perc_model_filename;
  char *cff_filename;
  char *fann_filename;
  char *stag_desc_filename;
  char *f2p_filename;
  char *fplm_filename;
  int hidden_neurons_nb;
  int iteration_nb;
  int debug_mode;
  int verbose;
  int feature_cutoff;
  int mode;
  int sent_nb;
  float hash_ratio;
  int beam_width;
  int mvt_nb;
  dico_vec *vocabs;
  char *vocabs_filename;
  int perceptron;
  char *mcd_filename;
  mcd *mcd_struct;
  char *features_model_filename;
  feat_model *features_model;
  feat_model *features_model_error;
  int stream_mode;
  dico *d_perceptron_features;
  dico *d_perceptron_features_error;
  dico *dico_labels;
  dico *dico_postags;
  char *maca_data_path;
  char *language;
  char *root_label;
  form2pos *f2p;
  int conll;
  int ifpls;
  int trace_mode;

  char *json_filename;
  char *dnn_model_filename;
  char *l_rules_filename;

} context;

context *context_new(void);
void context_free(context *ctx);
context *context_read_options(int argc, char *argv[]);
void context_general_help_message(context *ctx);
void context_model_help_message(context *ctx);
void context_iterations_help_message(context *ctx);
void context_cff_help_message(context *ctx);
void context_fann_help_message(context *ctx);
void context_d_features_help_message(context *ctx);
void context_d_classes_help_message(context *ctx);
void context_conll_help_message(context *ctx);
void context_cutoff_help_message(context *ctx);
void context_mode_help_message(context *ctx);
void context_beam_help_message(context *ctx);
void context_sent_nb_help_message(context *ctx);
void context_dnn_model_help_message(context *ctx);
void context_hidden_neurons_nb_help_message(context *ctx);
void context_stag_desc_filename_help_message(context *ctx);
void context_input_filename_help_message(context *ctx);
void context_mcd_help_message(context *ctx);
void context_features_model_help_message(context *ctx);
void context_vocabs_help_message(context *ctx);

void context_language_help_message(context *ctx);
void context_maca_data_path_help_message(context *ctx);

void context_f2p_filename_help_message(context *ctx);

void context_conll_help_message(context *ctx);
void context_ifpls_help_message(context *ctx);
void context_input_help_message(context *ctx);
void context_root_label_help_message(context *ctx);

void context_debug_help_message(context *ctx);
void context_json_help_message(context *ctx);
void context_dnn_model_help_message(context *ctx);

#endif
