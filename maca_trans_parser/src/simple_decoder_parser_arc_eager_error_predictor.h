#ifndef __SIMPLE_DECODER_PARSER_ARC_EAGER_ERROR_PREDICTOR__
#define __SIMPLE_DECODER_PARSER_ARC_EAGER_ERROR_PREDICTOR__
#include"context.h"

void simple_decoder_parser_arc_eager_error_predictor(context *ctx, char *perc_error_filename);

#endif
