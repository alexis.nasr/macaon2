#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<getopt.h>
#include"context.h"
#include"movement_parser_arc_eager.h"
#include"feat_fct.h"
#include"config2feat_vec.h"
#include"feature_table.h"
#include"dico.h"
#define N_BACK 3

void print_word_buffer(config *c, dico *dico_labels, mcd *mcd_struct)
{
  int i;
  word *w;
  char *label;
  char *buffer = NULL;
  char *token = NULL;
  int col_nb = 0;

  
  for(i=0; i < config_get_buffer(c)->nbelem; i++){
    w = word_buffer_get_word_n(config_get_buffer(c), i);

    if((mcd_get_gov_col(mcd_struct) == -1)
       && (mcd_get_label_col(mcd_struct) == -1)
       && (mcd_get_sent_seg_col(mcd_struct) == -1)){
      printf("%s\t", word_get_input(w));
      printf("%d\t", word_get_gov(w));
      label = (word_get_label(w) == -1)? NULL : dico_int2string(dico_labels, word_get_label(w));
      if(label != NULL)
        printf("%s\t", label) ;
      else
        printf("_\t");
      if(word_get_sent_seg(w) == 1)
        printf("1\n") ;
      else
        printf("0\n");
    }
    else{
      buffer = strdup(w->input);
      token = strtok(buffer, "\t");
      col_nb = 0;
      while(token){
        if(col_nb != 0) printf("\t");
        if(col_nb == mcd_get_gov_col(mcd_struct)){
          printf("%d", word_get_gov(w));
        }
        else
          if(col_nb == mcd_get_label_col(mcd_struct)){
            label = (word_get_label(w) == -1)? NULL : dico_int2string(dico_labels, word_get_label(w));
            if(label != NULL)
              printf("%s", label) ;
            else
              printf("_");
          }
          else
            if(col_nb == mcd_get_sent_seg_col(mcd_struct)){
              if(word_get_sent_seg(w) == 1)
                printf("1") ;
              else
                printf("0");
            }
            else{
              word_print_col_n(stdout, w, col_nb);
            }
        col_nb++;
        token = strtok(NULL, "\t");
      }
      if((col_nb <= mcd_get_gov_col(mcd_struct)) || (mcd_get_gov_col(mcd_struct) == -1)){
        printf("\t%d", word_get_gov(w));
      }
      if((col_nb <= mcd_get_label_col(mcd_struct)) || (mcd_get_label_col(mcd_struct) == -1)){
        label = (word_get_label(w) == -1)? NULL : dico_int2string(dico_labels, word_get_label(w));
        if(label != NULL)
          printf("\t%s", label) ;
        else
          printf("\t_");
      }
      if((col_nb <= mcd_get_sent_seg_col(mcd_struct)) || (mcd_get_sent_seg_col(mcd_struct) == -1)){
        if(word_get_sent_seg(w) == 1)
          printf("\t1") ;
        else
          printf("\t0");
      }
      printf("\n");
      free(buffer);
    }
  }
}

void sprint_movement(context *ctx, int mvt_code, int err_detect, char *st)
{
  if(ctx->trace_mode){
    movement_parser_sprint(st, mvt_code, ctx->dico_labels);
    if(mvt_code == 0 || mvt_code == 1 || mvt_code == 2 || mvt_code == 3) {//SHIFT REDUCE ROOT OR EOS
      sprintf(st," _");
    }
    sprintf(st, "\t%d\n", err_detect);
  }
  
}

void print_movement(context *ctx, int mvt_code, int err_detect)
{
  if(ctx->trace_mode){
    movement_parser_print(stdout, mvt_code, ctx->dico_labels);
    if(mvt_code == 0 || mvt_code == 1 || mvt_code == 2 || mvt_code == 3) {//SHIFT REDUCE ROOT OR EOS
      printf(" _");
    }
    printf("\t%d\n", err_detect);
  }
  
}

void simple_decoder_parser_arc_eager_error_predictor(context *ctx, char *perc_error_filename)
{
  config *c = NULL;
  FILE *f = (ctx->input_filename)? myfopen(ctx->input_filename, "r") : stdin;
  feature_table *ft = feature_table_load(ctx->perc_model_filename, ctx->verbose);
  feat_vec *fv = feat_vec_new(feature_types_nb);
  
  int root_label;
  int mvt_code;
  int mvt_type;
  int mvt_label;
  float max;
  int result;
  int argmax1, argmax2;
  float max1, max2;
  int index;

  int error_detect = 0;
  int err_mvt_code = 0;
  feature_table *ft_error = feature_table_load(perc_error_filename, ctx->verbose);
  float max_err;
  feat_vec *fv_error = feat_vec_new(feature_types_nb);
  int no_back_sentence = 0;

  root_label = dico_string2int(ctx->dico_labels, ctx->root_label);
  if(root_label == -1) root_label = 0;
  
  c = config_new(f, ctx->mcd_struct, 5);
  while(!config_is_terminal(c)){

    if(0) {}
    /* normal behaviour, ask classifier what is the next movement to do and do it */
    
    config2feat_vec_cff(ctx->features_model, c, ctx->d_perceptron_features, fv, LOOKUP_MODE);
    mvt_code = feature_table_argmax(fv, ft, &max);
      
    if(ctx->debug_mode){
      fprintf(stdout, "***********************************\n");
      config_print(stdout, c);
    }
      
    if(ctx->debug_mode){
      fprintf(stdout, " ***Parser choice***\n");
      vcode *vcode_array = feature_table_get_vcode_array(fv, ft);
      for(int i=0; i < 3; i++){
        printf("   %d\t", i);
        movement_parser_print(stdout, vcode_array[i].class_code, ctx->dico_labels);
        printf("\t%.4f\n", vcode_array[i].score);
      }
      free(vcode_array);
    }

    mvt_type = movement_parser_type(mvt_code);
    mvt_label = movement_parser_label(mvt_code);

    
    config2feat_vec_cff(ctx->features_model_error, c, ctx->d_perceptron_features_error, fv_error, LOOKUP_MODE);
    error_detect = feature_table_argmax(fv_error, ft_error, &max_err);

    vcode *vcode_array_err = feature_table_get_vcode_array(fv_error, ft_error);
    if(ctx->debug_mode){
      fprintf(stdout, " ***Error detection***\n");
      for(int i=0; i < 3; i++){
        fprintf(stdout, "   %d\t", i);
        fprintf(stdout, "%d\t%.4f\n", vcode_array_err[i].class_code, vcode_array_err[i].score);
      }
    }
      
    if (mvt_stack_top(config_get_history(c)) && mvt_get_type(mvt_stack_top(config_get_history(c))) == MVT_PARSER_EOS) {
      no_back_sentence = 0; //  a new sentence        
      if(ctx->debug_mode)
        fprintf(stdout,"A new chance to backtrack \n");
    }
      
    // If there is an error :
    float scoreError = vcode_array_err[0].score;
    free(vcode_array_err);
      
    int do_backtrack;
      
    int smin = 0;//50
      
    do_backtrack = error_detect > 0 && word_get_pos((word_buffer_bm1(c->bf))) != 13 && word_get_pos((word_buffer_b1(c->bf))) != 13 && word_get_pos((word_buffer_b0(c->bf))) != 13 && scoreError >= smin && mvt_stack_0(c->history) && ctx->force;
    // && c->bf->current_index < c->bf->nbelem -1 its to avoid problems with EOS, PPT :-> get_pos(b0) == ponct;
       
    for(int i=1 ; i <= error_detect+1 ; i++) {
      do_backtrack = do_backtrack && word_get_pos(word_buffer_bmi(c->bf,i)) != 13;
    }
    do_backtrack = do_backtrack && word_get_pos(word_buffer_b0(c->bf)) != 13; 
    //no_back_sentence = 0;
      
    if(do_backtrack && !ctx->trace_mode) {
      if(no_back_sentence) {}
      else {

        //no_back_sentence = 1;
        for (int ba = 1; ba < error_detect; ba++) {
          movement_parser_undo(c);
        }
        err_mvt_code = mvt_get_type(mvt_stack_top(config_get_history(c))); // issue because, sometimes a shift is comming instead
        movement_parser_undo(c);
          
        config2feat_vec_cff(ctx->features_model, c, ctx->d_perceptron_features, fv, LOOKUP_MODE);
        vcode *vcode_array = feature_table_get_vcode_array(fv, ft);

        int debug_choice = -1;
          
        for(int i=0; i < N_BACK /*ft->classes_nb-1*/; i++){
          if (err_mvt_code == vcode_array[i].class_code) {
            mvt_code = vcode_array[i+1].class_code;
            debug_choice = i+1;
            break;
          }
        }
        if (debug_choice == -1) {
          debug_choice = 1;
          no_back_sentence = 1;
          mvt_code = vcode_array[0].class_code;
          if(ctx->debug_mode){
            fprintf(stdout, "ERROR PREDICTOR, NO CHOICE LEFT, take the first choice \n");
          }
        }
        if(ctx->debug_mode){
          fprintf(stdout, "***********************************\n");
          fprintf(stdout, "         Correction :\n");
          config_print(stdout, c);
          vcode *vcode_arraye = feature_table_get_vcode_array(fv, ft);
          for(int i=debug_choice-1; i < debug_choice+2; i++){//postag_err+3; i++){
            fprintf(stdout, "%d\t", i);
            movement_parser_print(stdout, vcode_array[i].class_code, ctx->dico_labels);
            printf("\t%.4f\n", vcode_array[i].score);
          }
          free(vcode_arraye);
        }                  
                  
        mvt_type = movement_parser_type(mvt_code);
        mvt_label = movement_parser_label(mvt_code);

        if(ctx->debug_mode){
          fprintf(stdout,"Old : ");
          movement_parser_print(stdout, err_mvt_code, ctx->dico_labels);
          fprintf(stdout, ", New : ");
          movement_parser_print(stdout, mvt_code, ctx->dico_labels);
          fprintf(stdout, "\n");
        }
      
      }
    }
      
    // normal case :  
    result = 0;
    switch(mvt_type){
    case MVT_PARSER_LEFT :
      result = movement_parser_left_arc(c, mvt_label);
      break;
    case MVT_PARSER_RIGHT:
      result = movement_parser_right_arc(c, mvt_label);
      break;
    case MVT_PARSER_REDUCE:
      result = movement_parser_reduce(c);
      break;
    case MVT_PARSER_ROOT:
      result = movement_parser_root(c, root_label);
      break;
    case MVT_PARSER_EOS:
      result = movement_parser_eos(c);
      break;
    case MVT_PARSER_SHIFT:
      result = movement_parser_shift(c);
    }
      
    if(result == 0){
      if(ctx->debug_mode) fprintf(stdout, "WARNING : movement cannot be executed doing a SHIFT instead !\n");
      result = movement_parser_shift(c);
      if(result == 0){ /* SHIFT failed no more words to read, let's get out of here ! */
        if(ctx->debug_mode) fprintf(stdout, "WARNING : cannot exectue a SHIFT emptying stack !\n");
        while(!stack_is_empty(config_get_stack(c))) {
          movement_parser_root(c, root_label);
          if (ctx->trace_mode) {
            print_movement(ctx, MVT_PARSER_ROOT, 0);
          }
        }
      }
      else {
        if (ctx->trace_mode) {
          print_movement(ctx, MVT_PARSER_SHIFT, error_detect);// * do_backtrack);
        }
      }
    }
     
    else {
        
      if (ctx->trace_mode) {
        print_movement(ctx, mvt_code, error_detect);// * do_backtrack);
      }
    }
    
  }
  
  if(!ctx->trace_mode)
    print_word_buffer(c, ctx->dico_labels, ctx->mcd_struct);
  
  config_free(c); 
  feat_vec_free(fv);
  feature_table_free(ft);
  if(ctx->input_filename)
    fclose(f);
}
