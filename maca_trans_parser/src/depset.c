#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"depset.h"
#include"util.h"

depset *depset_new(void)
{
  depset *d = (depset *)memalloc(sizeof(depset));
  d->length = 1;
  d->array = (dependency *)memalloc(d->length * sizeof(dependency));
  d->array[0].gov = NULL;
  d->array[0].dep = NULL;
  d->array[0].label = -1;
  return d;
}

void depset_free(depset *d)
{
  /* int i;  */
  
  /*  for(i=0; i < d->length; i++){
    word_free(d->array[i].dep);
    }*/
  free(d->array);
  free(d);
}

depset *depset_copy(depset *d)
{
  int i;
  if(d == NULL) return NULL;
  depset *copy = depset_new();

  for(i=0; i < d->length; i++){
    depset_add(copy, d->array[i].gov, d->array[i].label, d->array[i].dep);
  }
  return copy;
}

void depset_add(depset *d, word *gov, int label, word *dep)
{
  int i;
  int new_length;

  if(gov == NULL || dep == NULL) return;
  word *max = (word_get_index(gov) > word_get_index(dep)) ? gov : dep;

  if(word_get_index(max) >= d->length){
    new_length = word_get_index(max) + 1;
    d->array = (dependency *)realloc(d->array, new_length * sizeof(dependency));
    for(i=d->length; i < new_length; i++){
      d->array[i].gov = NULL;
      d->array[i].dep = NULL;
      d->array[i].label = -1;
    }
    d->length = new_length;
  }
  d->array[word_get_index(dep)].gov = gov;
  d->array[word_get_index(dep)].dep = dep;
  d->array[word_get_index(dep)].label = label;
}

void depset_print(FILE *f, depset *d)
{
  int i;
  for(i=0; i < d->length; i++){
    if((d->array[i].gov) && (d->array[i].dep))
      fprintf(f, "(%d, %d, %d) ", word_get_index(d->array[i].dep), d->array[i].label, word_get_index(d->array[i].gov));
  }  
  fprintf(f, "\n");
}

void depset_print2(FILE *f, depset *d, dico *dico_labels)
{
  int i;
  int distance;
  char *label;
  for(i=1; i < d->length; i++){
    if((d->array[i].gov) && (d->array[i].dep)){
	distance = word_get_index(d->array[i].gov) - word_get_index(d->array[i].dep);
	/* fprintf(f, "%s\t%d\t%s\n", d->array[i].dep->input, distance, dico_int2string(dico_labels, d->array[i].label)); */

	label = dico_int2string(dico_labels, d->array[i].label);
	fprintf(f, "%s\t%d\t%s\t", d->array[i].dep->input, distance, label);
	if(!strcmp(label, "eos"))
	  fprintf(f, "1\n");
	else
	  fprintf(f, "0\n");
	    

	
    }  
  }
}
/*
void depset_print3(FILE *f, depset *d, dico *dico_labels)
{
  int i;
  int root_code = dico_string2int(dico_labels, "root");
  int distance;

  for(i=1; i < d->length; i++){
    if((d->array[i].gov) && (d->array[i].dep)){
      if(d->array[i].label == root_code)
      fprintf(f, "%d\t%s\t%d\t%s\n", word_get_index(d->array[i].dep), d->array[i].dep->input, 0, dico_int2string(dico_labels, d->array[i].label));
      else{
	distance = word_get_index(d->array[i].gov) - word_get_index(d->array[i].dep);
	fprintf(f, "%d\t%s\t%d\t%s\n", word_get_index(d->array[i].dep), d->array[i].dep->input, distance, dico_int2string(dico_labels, d->array[i].label));
      }
    }
  }  
}
*/

char *skip_index(char *buffer)
{
  int i;
  for(i=0; i < strlen(buffer); i++){
    if(buffer[i] < '0' || buffer[i] > '9' || buffer[i] == ' ' || buffer[i] == '\t')
      return &buffer[i];
  }
  return NULL;
}

void depset_print_new_index(FILE *f, depset *d, dico *dico_labels)
{
  int i;

  for(i=1; i < d->length; i++){
    if((d->array[i].gov) && (d->array[i].dep)){
       /* fprintf(f, "%d\t", word_get_index(d->array[i].dep)); */
       fprintf(f, "%d\t", word_get_index(d->array[i].dep));
       fprintf(f, "%s\t%d\t%s\n", skip_index(d->array[i].dep->input), word_get_index(d->array[i].gov), dico_int2string(dico_labels, d->array[i].label));
    }
  }  
  fprintf(f, "\n");
}

/*check that all dependents of d1 are in d2 */
int depset_compare(depset *d1, depset *d2)
{
  int i,j;

  /* fprintf(stdout, "d2 = "); */
  /* depset_print(stdout, d2); */

  if(d1->length != d2->length){      fprintf(stdout, "fail\n"); return 0;} 
  for(i=0; i < d1->length; i++){
    for(j=0; j < d2->length; j++){
      if((word_get_index(d1->array[i].gov) == word_get_index(d2->array[j].gov))
	 && (word_get_index(d1->array[i].dep) == word_get_index(d2->array[j].dep))
	 && (d1->array[i].label == d2->array[j].label)) break;
    }
    if(j == d2->length){
      fprintf(stdout, "fail\n");
      return 0;
    }
  }
  fprintf(stdout, "success\n");
  return 1;
}

