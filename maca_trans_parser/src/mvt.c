#include<stdio.h>
#include<stdlib.h>
#include"util.h"
#include"mvt.h"

mvt *mvt_new(int type, word *gov, word *dep)
{
  mvt *m = (mvt *)memalloc(sizeof(mvt));
  m->type = type;
  m->gov = gov;
  m->dep = dep;
  return m;
}

void mvt_free(mvt *m)
{
  if(m)
    free(m);
}

void mvt_print(FILE *f, mvt *m)
{
  if(m)
    printf("type = %d gov = %d dep = %d\n", mvt_get_type(m), word_get_index(mvt_get_gov(m)), word_get_index(mvt_get_dep(m)));
  
}
