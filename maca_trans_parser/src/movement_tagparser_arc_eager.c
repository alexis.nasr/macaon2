#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"util.h"
#include"movements.h"
#include"movement_tagparser_arc_eager.h"

void movement_tagparser_print(FILE *f, int mvt_code, dico *dico_labels, dico *dico_postag)
{
  
  int mvt_type = movement_tagparser_type(mvt_code);
  int mvt_label = movement_tagparser_label(mvt_code);
  char *label;

  if(mvt_type == MVT_TAGPARSER_SHIFT)  {fprintf(f, "SHIFT"); return;}
  if(mvt_type == MVT_TAGPARSER_REDUCE) {fprintf(f, "REDUCE"); return;}
  if(mvt_type == MVT_TAGPARSER_ROOT)   {fprintf(f, "ROOT"); return;}
  if(mvt_type == MVT_TAGPARSER_EOS)    {fprintf(f, "EOS"); return;}
  if(mvt_type == MVT_TAGPARSER_POSTAG){
    fprintf(f, "POSTAG");
    label = dico_int2string(dico_postag, mvt_label);
    fprintf(f, " %s", label);
    return;
  }
  if(mvt_type == MVT_TAGPARSER_RIGHT)   fprintf(f, "RIGHT");
  else fprintf(f, "LEFT");
  label = dico_int2string(dico_labels, mvt_label);
  fprintf(f, " %s", label);
}

int movement_tagparser_type(int mvt)
{
  if(mvt == MVT_TAGPARSER_SHIFT)     return MVT_TAGPARSER_SHIFT;  /* 0 */
  if(mvt == MVT_TAGPARSER_REDUCE)    return MVT_TAGPARSER_REDUCE; /* 1 */
  if(mvt == MVT_TAGPARSER_ROOT)      return MVT_TAGPARSER_ROOT;   /* 2 */
  if(mvt == MVT_TAGPARSER_EOS)       return MVT_TAGPARSER_EOS;    /* 3 */
  if(mvt % 3 == 0)                   return MVT_TAGPARSER_RIGHT;  /* 4, 7, 10 ... */
  if(mvt % 3 == 1)                   return MVT_TAGPARSER_POSTAG; /* 5, 8, 11 ... */
  /*if(mvt % 3 == 2)*/               return MVT_TAGPARSER_LEFT;   /* 6, 9, 12 ... */
}

int movement_tagparser_label(int mvt)
{
  if(mvt == MVT_TAGPARSER_SHIFT) return -1;  
  if(mvt == MVT_TAGPARSER_REDUCE) return -1; 
  if(mvt == MVT_TAGPARSER_ROOT) return -1;   
  if(mvt == MVT_TAGPARSER_EOS) return -1;    
  if(mvt % 3 == 1) /* pos movement */
    return (mvt - 4) / 3;
  if(mvt % 3 == 2) /* left movement */
    return (mvt - 5) / 3;
  /* if(mvt % 3 == 0)*/ /* right movement */
    return (mvt - 6) / 3;
}

int movement_tagparser_add_pos(config *c, int pos)
{
  return movement_add_pos(c, movement_tagparser_postag_code(pos), pos); 
}

int movement_tagparser_add_pos_undo(config *c)
{
  return movement_add_pos_undo(c);
}

int movement_tagparser_eos(config *c)
{
  return movement_eos(c, MVT_TAGPARSER_EOS);
}

int movement_tagparser_eos_undo(config *c)
{
  return movement_eos_undo(c);
}

int movement_tagparser_left_arc(config *c, int label)
{
  return movement_left_arc(c, movement_tagparser_left_code(label), label);
}

int movement_tagparser_left_arc_undo(config *c)
{
  return movement_left_arc_undo(c);
}

int movement_tagparser_right_arc(config *c, int label)
{
  return movement_right_arc(c, movement_tagparser_right_code(label), label);
}

int movement_tagparser_right_arc_undo(config *c)
{
  return movement_right_arc_undo(c);
}

int movement_tagparser_shift(config *c)
{
  return movement_shift(c, MVT_TAGPARSER_SHIFT);
}

int movement_tagparser_shift_undo(config *c)
{
  return movement_shift_undo(c);
}

int movement_tagparser_reduce(config *c)
{
  return movement_reduce(c, MVT_TAGPARSER_REDUCE);
}

int movement_tagparser_reduce_undo(config *c)
{
  return movement_reduce_undo(c);
}

int movement_tagparser_root(config *c, int root_code)
{
  return movement_root(c, MVT_TAGPARSER_ROOT, root_code);
}

int movement_tagparser_root_undo(config *c)
{
  return movement_root_undo(c);
}

int movement_tagparser_undo(config *c)
{
  int result;
  int mvt_type = mvt_get_type(mvt_stack_top(config_get_history(c)));
  switch(mvt_type){
  case MVT_TAGPARSER_POSTAG :
    result = movement_tagparser_left_arc_undo(c);
    break;
  case MVT_TAGPARSER_LEFT :
    result = movement_tagparser_left_arc_undo(c);
    break;
  case MVT_TAGPARSER_RIGHT:
    result = movement_tagparser_right_arc_undo(c);
    break;
  case MVT_TAGPARSER_REDUCE:
    result = movement_tagparser_reduce_undo(c);
    break;
  case MVT_TAGPARSER_ROOT:
    result = movement_tagparser_root_undo(c);
    break;
  case MVT_TAGPARSER_EOS:
    result = movement_tagparser_eos_undo(c);
    break;
  case MVT_TAGPARSER_SHIFT:
    result = movement_tagparser_shift_undo(c);
    }
  return result;
}
