#ifndef __DEPSET__
#define __DEPSET__

#include "word.h"

typedef struct {
  word *gov;
  word *dep;
  int label;
} dependency;

typedef struct {
  int length;
  dependency *array;
} depset;

int depset_compare(depset *d1, depset *d2);

word *depset_get_root(depset *ds);
depset *depset_new(void);
void depset_free(depset *d);
depset *depset_copy(depset *d);
void depset_init(depset *d);
void depset_add(depset *d, word *gov, int label, word *dep);
void depset_print(FILE *f, depset *d);
void depset_print2(FILE *f, depset *d, dico *dico_labels);
void depset_print3(FILE *f, depset *d, dico *dico_labels);
void depset_print_new_index(FILE *f, depset *d, dico *dico_labels);


#endif
