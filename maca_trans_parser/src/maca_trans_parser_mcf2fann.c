#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<getopt.h>
#include"movement_parser.h"
#include"oracle_parser.h"
#include"feat_fct.h"
#include"context.h"
#include"feat_types.h"
#include"feat_vec.h"
#include"dico_vec.h"
#include"word_emb.h"
#include"config2feat_vec.h"


void feat_vec_print_dnn(FILE *f, feat_vec *fv, feat_model *fm, mcd *m); 

void transform_treebank_help_message(context *ctx)
{
  context_general_help_message(ctx);
  context_mode_help_message(ctx);
  context_sent_nb_help_message(ctx);

  fprintf(stderr, "INPUT\n");
  context_conll_help_message(ctx);
  context_mcd_help_message(ctx);
  context_features_model_help_message(ctx);
  context_input_help_message(ctx);
 fprintf(stderr, "IN TEST MODE\n");
  context_vocabs_help_message(ctx);

  fprintf(stderr, "OUTPUT\n");
  context_cff_help_message(ctx);
  context_fann_help_message(ctx);
  fprintf(stderr, "IN TRAIN MODE\n");
  context_vocabs_help_message(ctx);

}

void transform_treebank_check_options(context *ctx)
{
  if( ctx->help
     || !ctx->input_filename  
          /* || !ctx->mcd_filename */
     /* || !(ctx->cff_filename || ctx->fann_filename) */
     ){
    transform_treebank_help_message(ctx);
    exit(1);
  }
}

void print_mvt_fann(FILE *f, int mvt_nb, int mvt_code)
{
  int i;
  if(mvt_code == 0)
    fprintf(f, "1");
  else
    fprintf(f, "0");
  for(i=1; i < mvt_nb; i++){
    if(i == mvt_code)
      fprintf(f, " 1");
    else
      fprintf(f, " 0");
  }
  fprintf(f, "\n");
}

int generate_training_file_buffer(FILE *output_file, context *ctx)
{  
  config *c;
  int mvt_code;
  char mvt_type;
  int mvt_label;
  feat_vec *fv = feat_vec_new(feature_types_nb);
  sentence *ref = NULL;
  int nb_trans = 0;
  int sentence_nb = 0;
  FILE *conll_file = myfopen(ctx->input_filename, "r");
  FILE *conll_file_ref = myfopen(ctx->input_filename, "r");

  c = config_initial(conll_file, ctx->mcd_struct, 0);

  while((ref = sentence_read(conll_file_ref, ctx->mcd_struct)) && (sentence_nb < ctx->sent_nb)){ 
      /* sentence_print(stdout, ref, NULL);    */
    word_buffer_read_sentence(c->bf);
    while(!config_is_terminal(c)){
      /* config_print(stdout,c);     */
      config2feat_vec_fann(ctx->features_model, c, fv, ctx->mode);
      mvt_code = oracle_parser(c, ref);
      nb_trans++;

      feat_vec_print_dnn(output_file, fv, ctx->features_model, ctx->mcd_struct);
      print_mvt_fann(output_file, ctx->mvt_nb, mvt_code);
      fprintf(output_file, "\n\n");

      mvt_type = movement_type(mvt_code);
      mvt_label = movement_label(mvt_code);
      if(mvt_type == MVT_LEFT){
	movement_left_arc(c, mvt_label, 0);
	continue;
      }
      if(mvt_type == MVT_RIGHT){
	movement_right_arc(c, mvt_label, 0);
	continue;
      }
      if(mvt_type == MVT_SHIFT){
	movement_shift(c, 0, 0);
	continue;
      }
    }
    config_free(c); 
    c = config_initial(conll_file, ctx->mcd_struct, 0);
    sentence_nb++;
  }
  return nb_trans;
}

int main(int argc, char *argv[])
{
  context *ctx;
  int input_size, output_size;
  int nb_trans = 0;
  FILE *output_file;
  
  ctx = context_read_options(argc, argv);
  transform_treebank_check_options(ctx);

  ctx->features_model = feat_model_read(ctx->features_model_filename, ctx->verbose);
  
  if(ctx->mode == TRAIN_MODE){
    mcd_extract_dico_from_corpus(ctx->mcd_struct, ctx->input_filename);
    ctx->vocabs = mcd_build_dico_vec(ctx->mcd_struct);
  }
  else if(ctx->mode == TEST_MODE){
    ctx->vocabs = dico_vec_read(ctx->vocabs_filename, ctx->hash_ratio);
    mcd_link_to_dico(ctx->mcd_struct, ctx->vocabs, ctx->verbose);
  }
  
  ctx->dico_labels = dico_vec_get_dico(ctx->vocabs, (char *)"LABEL");
  if(ctx->dico_labels == NULL){
    fprintf(stderr, "cannot find label names\n");
    return 1;
  }
  ctx->mvt_nb = ctx->dico_labels->nbelem * 2 + 1;
    
  feat_model_compute_ranges(ctx->features_model, ctx->mcd_struct, ctx->mvt_nb);

  input_size = ctx->features_model->dim;
  output_size = ctx->mvt_nb;

  fprintf(stderr, "input size = %d\n", input_size); 
  fprintf(stderr, "output size = %d\n", output_size);
    
  /* open output file */
  output_file = myfopen(ctx->fann_filename, "w");
  /* spaces are here to leave room to write number of lines in the file when it will be known */
  fprintf(output_file, "                   %d %d\n", input_size, output_size); 
  
  nb_trans = generate_training_file_buffer(output_file, ctx);
  
  rewind(output_file);
  fprintf(output_file, "%d", nb_trans);
  fclose(output_file);

  if(ctx->mode == TRAIN_MODE)
    dico_vec_print(ctx->vocabs_filename, ctx->vocabs);

  context_free(ctx);
  return 0;
}




void feat_vec_print_dnn(FILE *f, feat_vec *fv, feat_model *fm, mcd *m)
{
  int i,j;
  feat_desc *fd;
  simple_feat_desc *sfd;
  int mcd_representation;
  int column;

  for(i=0; i < fm->nbelem; i++){
    fd = fm->array[i];
    sfd = fd->array[0];
    
    if((sfd->type <= FEAT_TYPE_INT_16) && (sfd->type >= FEAT_TYPE_INT_1)){
      int range = sfd->type - FEAT_TYPE_INT_0;
      for(j=0; j < range; j++){
	/* if(i + j != 0) fprintf(f, " "); */
	if(j == fv->t[i])
	  fprintf(f, "1 ");
	else
	  fprintf(f, "0 ");
      }
    }
    else{
      column = m->wf2col[sfd->type];
      mcd_representation = m->representation[column];
      if(mcd_representation == MCD_REPRESENTATION_EMB){
	/* if(i != 0) fprintf(f, " "); */
	word_emb_print(f, m->word_emb_array[column], fv->t[i]);
      }
      else if(mcd_representation == MCD_REPRESENTATION_VOCAB){
	for(j=0; j < m->dico_array[column]->nbelem; j++){
	  /* if(i + j != 0) fprintf(f, " "); */
	  if(j == fv->t[i])
	    fprintf(f, "1 ");
	  else
	    fprintf(f, "0 ");
	}
      }
    }
  fprintf(f, "\n");

  }
  fprintf(f, "\n");
}

/* void feat_vec_fill_input_array_dnn(fann_type *input_array, feat_vec *fv, feat_model *fm, mcd *m) */
void feat_vec_fill_input_array_dnn(float *input_array, feat_vec *fv, feat_model *fm, mcd *m)
{
  int i,j;
  feat_desc *fd;
  simple_feat_desc *sfd;
  int mcd_representation;
  int current_index = 0;
  int column;

  for(i=0; i < fm->nbelem; i++){
    fd = fm->array[i];
    sfd = fd->array[0];

    if((sfd->type <= FEAT_TYPE_INT_16) && (sfd->type >= FEAT_TYPE_INT_1)){
      int range = sfd->type - FEAT_TYPE_INT_0;
      for(j=0; j < range; j++){
	if(j == fv->t[i])
	  input_array[current_index++] = 1;
	else
	  input_array[current_index++] = 0;
      }
    }
    else{
      column = m->wf2col[sfd->type];
      mcd_representation = m->representation[column];
      if(mcd_representation == MCD_REPRESENTATION_EMB){
	current_index =  word_emb_fill_input_array_dnn(input_array, m->word_emb_array[column], fv->t[i], current_index);
      }
      else if(mcd_representation == MCD_REPRESENTATION_VOCAB){
	for(j=0; j < m->dico_array[column]->nbelem; j++){
	  if(j == fv->t[i])
	    input_array[current_index++] = 1;
	  else
	    input_array[current_index++] = 0;
	}
      }
    }
 
  }
}


