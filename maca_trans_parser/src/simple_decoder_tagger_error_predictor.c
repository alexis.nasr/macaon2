#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<getopt.h>
#include<ctype.h>

#include"context.h"
#include"movement_tagger.h"
#include"feat_fct.h"
#include"config2feat_vec.h"
#include"feature_table.h"
#include"dico.h"
#include"mcd.h"

void add_signature_to_words_in_word_buffer(word_buffer *bf, form2pos *f2p)
{
  int i;
  word *w;
  char lower_form[1000];

  for(i = word_buffer_get_nbelem(bf) - 1; i >=0  ; i--){
    w = word_buffer_get_word_n(bf, i);
    if(word_get_signature(w) != -1) break;
    w->signature = form2pos_get_signature(f2p, w->form);
    if(w->signature == -1){
      strcpy(lower_form, w->form);
      to_lower_string(lower_form);
      w->signature = form2pos_get_signature(f2p, lower_form);
    }
  }
}


void string_word_print_col_n(FILE *f, word *w, int n, char *str)
{
  int i;
  int col = 0;
  char *buffer = w->input;
  if(buffer == NULL) return;
  int l= strlen(buffer);
  for(i=0; i < l; i++){
    if(buffer[i] == '\t') {
      col++;
      continue;
    }
    if(col == n)
      sprintf(str + strlen(str), "%c", buffer[i]);
  }
}

void string_print_word(word *w, mcd *mcd_struct, dico *dico_pos, int postag, char **strF)
{
  char *buffer = NULL;
  char *token = NULL;
  int col_nb = 0;
  char str[1000] = "";

  if(mcd_get_pos_col(mcd_struct) == -1){
    sprintf(str + strlen(str),"%s\t%s\n", w->input, dico_int2string(dico_pos, postag));
  }
  else{
    buffer = strdup(w->input);
    token = strtok(buffer, "\t");
    col_nb = 0;
    while(token){
      if(col_nb != 0) sprintf(str + strlen(str),"\t");
      if(col_nb == mcd_get_pos_col(mcd_struct)) {
        sprintf(str + strlen(str),"%s", dico_int2string(dico_pos, postag));
      }
      else 
        string_word_print_col_n(stdout, w, col_nb,str);
      
      col_nb++;
      token = strtok(NULL, "\t");
    }
    if(col_nb <= mcd_get_pos_col(mcd_struct))
      sprintf(str + strlen(str),"\t%s", dico_int2string(dico_pos, postag));
    
    free(buffer);
    
  }
  *strF = (char*)malloc(sizeof(char)*strlen(str) + 10);
  strcpy(*strF,str);
}

void simple_decoder_tagger_error_predictor(context *ctx, char *perc_error_filename)
{
  config *c;
  feat_vec *fv = feat_vec_new(feature_types_nb);
  feat_vec *fv_error = feat_vec_new(feature_types_nb);
  
  FILE *f = (ctx->input_filename)? myfopen(ctx->input_filename, "r") : stdin;
  
  feature_table *ft = feature_table_load(ctx->perc_model_filename, ctx->verbose);   
  feature_table *ft_error = feature_table_load(perc_error_filename, ctx->verbose);
  
  int postag;
  int postag_err;
  int error_detect;
  float max;
  float max_err;
  word *b0;
  dico *dico_pos = dico_vec_get_dico(ctx->vocabs, (char *)"POS");
  char *impr[300000]; // it's for printing
  int nb = 0;

  int no_more_back = -1;
  
  c = config_new(f, ctx->mcd_struct, 5); 

  while(!config_is_terminal(c)){
    if(ctx->f2p)
      add_signature_to_words_in_word_buffer(c->bf, ctx->f2p); 

    b0 = word_buffer_b0(c->bf);

    if(ctx->debug_mode){
      fprintf(stdout, "***********************************\n");
      config_print(stdout, c);
    }
    
    config2feat_vec_cff(ctx->features_model, c, ctx->d_perceptron_features, fv, LOOKUP_MODE);       
    postag = feature_table_argmax(fv, ft, &max);

    if(ctx->debug_mode){
      fprintf(stdout, " ***Tagger choice***\n");
      vcode *vcode_array = feature_table_get_vcode_array(fv, ft);
      for(int i=0; i < 3; i++){
        fprintf(stdout, "   %d\t", i);
        fprintf(stdout, "   %s\t%.4f\n", dico_int2string(dico_pos, vcode_array[i].class_code), vcode_array[i].score);
      }
      free(vcode_array);
    }
    
    word_set_pos(word_buffer_b0(c->bf), postag);
    
    config2feat_vec_cff(ctx->features_model_error, c, ctx->d_perceptron_features_error, fv_error, LOOKUP_MODE);
          
    string_print_word(b0, ctx->mcd_struct, dico_pos, postag, &impr[nb]);
    error_detect = feature_table_argmax(fv_error, ft_error, &max_err);
    // char ✔,✖
    if(ctx->debug_mode) {
      switch (error_detect) {
      case 0 : // No errors detected 
        sprintf(impr[nb]+strlen(impr[nb]),"\t✔\t_");
        break;

      default :
        sprintf(impr[nb]+strlen(impr[nb]),"\t✖\t-%d",error_detect);
        break;
        
      }
    }
    
    else if(ctx->trace_mode)
        sprintf(impr[nb]+strlen(impr[nb]),"\t%d",error_detect);
    
    sprintf(impr[nb]+strlen(impr[nb]),"\n");
    nb +=1;
    
    vcode *vcode_array_err = feature_table_get_vcode_array(fv_error, ft_error);
    
    if(ctx->debug_mode){
      fprintf(stdout, " ***Error detection***\n");
      for(int i=0; i < 2; i++){
        fprintf(stdout, "   %d\t", i);
        fprintf(stdout, "   %d\t%.4f\n", vcode_array_err[i].class_code, vcode_array_err[i].score);
      }
    }
    
    float scoreError = vcode_array_err[0].score;
    free(vcode_array_err);

    int score_min = 0;

    int do_backtrack = error_detect > 0 && scoreError >= score_min && !ctx->trace_mode;

    int n_back = error_detect;

    if (no_more_back >= word_get_index(word_buffer_b0(c->bf)) - n_back) // it's to avoid looping problems
      do_backtrack = 0;
    
    if (do_backtrack) {
      nb -= 1;
      for (int i=0; i < n_back; i++) { // backtrack here
        backward(c);
        nb -= 1;
      }

      if(ctx->f2p)
        add_signature_to_words_in_word_buffer(c->bf, ctx->f2p);
          
      postag_err = word_get_pos(word_buffer_b0(c->bf));

      postag = postag_err;
      config2feat_vec_cff(ctx->features_model, c, ctx->d_perceptron_features, fv, LOOKUP_MODE);

      vcode *vcode_array = feature_table_get_vcode_array(fv, ft);
      
      int debug_choice = -1;
      for(int i=0; i < 3 ; i++) { // Looks only the 3 first move
        if (postag_err == vcode_array[i].class_code) {
          postag = vcode_array[i+1].class_code;
          debug_choice = i+1;
          break;
        }
      }

      free(vcode_array);
      
      if(ctx->debug_mode){
        fprintf(stdout, "***********************************\n");
        config_print(stdout, c);
      }

      if(debug_choice == -1) {
        postag = feature_table_argmax(fv, ft, &max);
        if(ctx->debug_mode){
          fprintf(stdout, "ERROR PREDICTOR, NO CHOICE LEFT, take the first choice : %s\n", dico_int2string(dico_pos, postag));
        }
        no_more_back = word_get_index(word_buffer_b0(c->bf)); // it blocks backtracking for this word (and before this word)
      }

      else {
        if(ctx->debug_mode){
          vcode *vcode_arraye = feature_table_get_vcode_array(fv, ft);
          for(int i=debug_choice-1; i < debug_choice+2; i++){
            fprintf(stdout, "%d\t", i);
            fprintf(stdout, "%s\t%.4f\n", dico_int2string(dico_pos, vcode_arraye[i].class_code), vcode_arraye[i].score);
          }
          free(vcode_arraye);
          fprintf(stdout, "Old pos : %s, New : %s\n", dico_int2string(dico_pos, postag_err), dico_int2string(dico_pos, postag));
        }          
      }
      
      word_set_pos(word_buffer_b0(c->bf), postag);
      string_print_word(word_buffer_b0(c->bf), ctx->mcd_struct, dico_pos, postag,&impr[nb]);
      if(ctx->debug_mode)
        sprintf(impr[nb]+strlen(impr[nb]),"\t✐\t_\n");
      else
        sprintf(impr[nb]+strlen(impr[nb]),"\n");
      nb += 1;
    }
    
    word_buffer_move_right(c->bf);
}
  for (int i = 0; i < nb; i++) {
    printf("%s",impr[i]);
  }
  feat_vec_free(fv_error);
  feat_vec_free(fv);
  feature_table_free(ft_error);
  feature_table_free(ft);
  config_free(c); 
  if (ctx->input_filename) fclose(f);
}
