#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct {
  int index;
  char *stack;
  char *movement;
  float score;
} configuration;

typedef struct {
  int size;           /* size of the array used to store words */
  int nbelem;         /* number of words in the buffer */
  configuration **array;       /* array to store configurations */
} trace;


int configuration_equal(configuration *c1, configuration *c2)
{
  if(c1->index != c2->index) return 0;
  if(strcmp(c1->stack, c2->stack)) return 0;
  return 1;
}


configuration *configuration_new(int index, char *stack, char *movement, float score)
{
  configuration *c = (configuration *)malloc(sizeof(configuration));
  if(c == NULL) return NULL;
  c->index = index;
  c->stack = stack;
  c->movement = movement;
  c->score = score;
  return c;
}

void configuration_print(FILE *f, configuration *c)
{
  fprintf(f, "%d\t%s\t%s\t%f\n", c->index, c->stack, c->movement, c->score);
}


trace *trace_new()
{
  trace *t = (trace *)malloc(sizeof(trace));
  t->size = 10;
  t->nbelem = 0;
  t->array = (configuration **)malloc(t->size * sizeof(configuration *));
  
  return t;
}


int trace_add(trace *t, configuration *c)
{
  if(t->nbelem == t->size -1){
    t->size = 2 * (t->size + 1);
    t->array = (configuration **)realloc(t->array, t->size * sizeof(configuration *));
  }
  t->array[t->nbelem] = c;

  t->nbelem++;
  return t->nbelem - 1;
}

void trace_print(FILE *f, trace *t)
{
  int i;
  for(i=0; i < t->nbelem; i++)
    configuration_print(f, t->array[i]);
}



trace *trace_load(char *trace_filename)
{
  FILE *f;
  int index;
  float score;
  char stack[10000];
  char movement[100];
  char buffer[20000];
  if(trace_filename == NULL)
    f = stdin;
  else
    f = fopen(trace_filename, "r");
  if(f == NULL){
    fprintf(stderr, "cannot open file %s aborting\n", trace_filename);
    exit(1);
  }
    
  
  trace *t = trace_new();
  /* while(!feof(f)){ */
  while(fgets(buffer, 20000, f)){
    int r = sscanf(buffer, "%d\t%[^\t]\t%[^\t]\t%f\n", &index, stack, movement, &score);
    if(r == 4){
      /* printf("index = %d stack = %s movement = %s score = %f\n", index, stack, movement, score); */
      trace_add(t, configuration_new(index, strdup(stack), strdup(movement), score));
    }
  }
  
  if(trace_filename != NULL)
    fclose(f);
  return t;
}


void trace_compare(trace *ref, trace *hyp)
{
  int index_hyp = 0;
  int index_ref = 0;
  configuration *c_ref, *c_hyp;

  while(1){
    c_ref = ref->array[index_ref];
    c_hyp = hyp->array[index_hyp];
    if(!c_hyp || !c_ref) break;
       printf("REF ");
    configuration_print(stdout, c_ref);
    printf("HYP ");
    configuration_print(stdout, c_hyp);
    
    if(configuration_equal(c_ref, c_hyp)){
      fprintf(stdout, "EQUAL\n"); 
      if(strcmp(c_ref->movement, c_hyp->movement)){
	/* fprintf(stdout, "BAAD\t%s\t%s\t%f\n", c_ref->movement, c_hyp->movement, c_hyp->score); */
	/* fprintf(stdout, "BAAD\t%s\t%f\n", c_hyp->movement, c_hyp->score); */
      }
      else{
	/* fprintf(stdout, "GOOD\t%s\t%s\t%f\n", c_ref->movement, c_hyp->movement, c_hyp->score); */
	/* fprintf(stdout, "GOOD\t%s\t%f\n", c_hyp->movement, c_hyp->score); */
      }
      index_hyp++;
      index_ref++;
    }
    else{
       fprintf(stdout, "DIFFERENT\n"); 
      if(c_ref->index > c_hyp->index)
	index_hyp++;
      else if(c_ref->index < c_hyp->index)
	index_ref++;
      else{
      index_hyp++;
      index_ref++;
      
      }
      
    }
  }
}


int main(int arc, char *argv[])
{
  char *ref_filename = argv[1];
  char *hyp_filename = argv[2];

  fprintf(stderr, "loading file %s\n", ref_filename);
  trace *t_ref = trace_load(ref_filename);
  fprintf(stderr, "loading file %s\n", hyp_filename);
  trace *t_hyp = trace_load(hyp_filename);

 /* trace_print(stdout, t_ref);  */

  
  trace_compare(t_ref, t_hyp);

}
