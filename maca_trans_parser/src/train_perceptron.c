#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<getopt.h>
#include"movement_parser.h"
#include"oracle_parser.h"
#include"feat_fct.h"
#include"context.h"
#include"feat_vec.h"
#include"feature_table.h"
#include"dico_vec.h"
#include"beam.h"
#include"word_emb.h"
#include"config2feat_vec.h"

feature_table *train_perceptron(context *ctx);
feature_table *train_perceptron_early_update(context *ctx);
int perceptron_update(int class_ref, feat_vec *fv, feature_table *ft, feature_table *ft_sum, int counter);


void train_perceptron_help_message(context *ctx)
{
  context_general_help_message(ctx);
  context_mode_help_message(ctx);
  context_sent_nb_help_message(ctx);
  context_iterations_help_message(ctx);

  fprintf(stderr, "INPUT\n");
  context_mcd_help_message(ctx);
  context_conll_help_message(ctx);
  context_features_model_help_message(ctx);

  fprintf(stderr, "OUTPUT\n");
  context_model_help_message(ctx);
  context_vocabs_help_message(ctx);

}

void train_perceptron_check_options(context *ctx)
{
  if(!ctx->input_filename
     || ctx->help
     /* || !ctx->mcd_filename */
     || !ctx->features_model_filename
     || !ctx->perc_model_filename
     || !ctx->vocabs_filename
     ){
    train_perceptron_help_message(ctx);
    exit(1);
  }
}

int main(int argc, char *argv[])
{
  feature_table *ft = NULL;
  context *ctx;
  
  ctx = context_read_options(argc, argv);
  train_perceptron_check_options(ctx);
  
  mcd_extract_dico_from_corpus(ctx->mcd_struct, ctx->input_filename);

  ctx->vocabs = mcd_build_dico_vec(ctx->mcd_struct);
  ctx->dico_labels = dico_vec_get_dico(ctx->vocabs, (char *)"LABEL");
  
  if(ctx->dico_labels == NULL){
    fprintf(stderr, "cannot find label names\n");
    return 1;
  }
  ctx->mvt_nb = ctx->dico_labels->nbelem * 2 + 1;

  ctx->d_perceptron_features = dico_new((char *)"d_perceptron_features", 10000000);

  /* add the feature dictionnary to the dico vector */
  dico_vec_add(ctx->vocabs, ctx->d_perceptron_features);
  
  ft = train_perceptron_early_update(ctx);


  printf("nb features = %d\n", ctx->d_perceptron_features->nbelem);
  ft->features_nb = ctx->d_perceptron_features->nbelem;
  feature_table_dump(ctx->perc_model_filename, ft);
  dico_vec_print(ctx->vocabs_filename, ctx->vocabs);
  feature_table_free(ft);
  context_free(ctx);
  return 0;
}

feature_table *train_perceptron(context *ctx)
{  
  config *config_oracle;
  int mvt_oracle_code;
  char mvt_oracle_type;
  int mvt_oracle_label;
  int i,j;
  /* config *config_pred; */
  /* int mvt_pred_code; */
  feat_vec *fv = feat_vec_new(feature_types_nb);
  sentence *ref = NULL;
  int sentence_nb = 0;
  FILE *conll_file = NULL;
  FILE *conll_file_ref = NULL;
  feature_table *ft_sum = feature_table_new(1000000, ctx->mvt_nb);
  feature_table *ft = feature_table_new(1000000, ctx->mvt_nb);
  int counter = 0;
  int epoch;
  
  for(epoch = 0; epoch < ctx->iteration_nb; epoch++){
    fprintf(stderr, "[%d]", epoch + 1);
    conll_file = myfopen(ctx->input_filename, "r");
    conll_file_ref = myfopen(ctx->input_filename, "r");
    
    config_oracle = config_initial(conll_file, ctx->mcd_struct, 0);
    /* config_pred = config_initial(conll_file, ctx->mcd_struct, 0); */
    sentence_nb = 0;
    while((ref = sentence_read(conll_file_ref, ctx->mcd_struct)) && (sentence_nb < ctx->sent_nb)){ 
      queue_read_sentence(config_oracle->bf, conll_file, ctx->mcd_struct);
      while(!config_is_terminal(config_oracle)){
	/* config_print(stdout,c);   */
	
	config2feat_vec_cff(ctx->features_model, config_oracle, ctx->d_perceptron_features, fv, ctx->mode);
	
	mvt_oracle_code = oracle_parser(config_oracle, ref);
	mvt_oracle_type = movement_type(mvt_oracle_code);
	mvt_oracle_label = movement_label(mvt_oracle_code);
	
	/* mvt_pred_code = perceptron_update(mvt_oracle_code, fv, ft, ft_sum, counter++); */
	perceptron_update(mvt_oracle_code, fv, ft, ft_sum, counter++);

      if(mvt_oracle_type == MVT_LEFT){
	/* printf("LEFT\n"); */
	movement_left_arc(config_oracle, mvt_oracle_label, 0);
	continue;
      }
      if(mvt_oracle_type == MVT_RIGHT){
	/* printf("RIGHT\n"); */
	movement_right_arc(config_oracle, mvt_oracle_label, 0);
	continue;
      }
      if(mvt_oracle_type == MVT_SHIFT){
	/* printf("SHIFT\n"); */
	movement_shift(config_oracle, 0, 0);
	continue;
      }
      }
      config_free(config_oracle); 
      config_oracle = config_initial(conll_file, ctx->mcd_struct, 0);
      sentence_nb++;
      /* sentence_free(ref);  */
      
      /* if((sentence_nb % 1000) == 0){fprintf(stdout, ".");fflush(stdout);} */
    }
    fclose(conll_file);
    fclose(conll_file_ref);
  }
  for(i=0; i< ft->features_nb; i++)
    for(j=0; j< ft->classes_nb; j++)
      ft->table[i][j] -= 1/(float)counter * ft_sum->table[i][j];
  feature_table_free(ft_sum);
  
  return ft;
}

int look_for_config_in_beam(config *c, beam *b)
{
  int i;
   /* fprintf(stdout, "\ngold\n"); */
   /* fprintf(stdout, "mvt seq "); */
    
    /* fprintf(stdout, "dep set "); */
    /* depset_print(stdout, c->ds); */


  for(i=0; i < b->nbelem; i++){
    /* config_print(stdout, b->t[i]); */

   
    if(config_equal(c, b->t[i]))
    return i;

    /*    if(config_equal2(b->t[i], c))
	  return i;*/
  }

  /*  for(i=0; i < c->mvt_array_nbelem; i++)
    fprintf(stdout, "%d ", c->mvt_array[i]);
    fprintf(stdout, "\n");*/
  return -1;
}

void perceptron_update_global(global_feat_vec *gfv, config *config_oracle, feature_table *ft,  feature_table *ft_sum, int counter)
{
  int i;
  int j;
  feat_vec *fv = NULL;

 /* fprintf(stdout, "gfv nbelem = %d\n", gfv->nbelem); */

   /* global_feat_vec_print(gfv);  */

  if(gfv){
    for(i=0; i < gfv->nbelem; i++){
      printf("classe predite = %d classe oracle = %d\n", gfv->array[i]->pred_mvt, config_oracle->mvt_array[i]);
       if(gfv->array[i]->pred_mvt != config_oracle->mvt_array[i]){ 
	 printf("mise à jour\n");
	fv = gfv->array[i]->fv;
	for(j=0; j < fv->nb; j++){
	  if(fv->t[j] != -1){
	    /* printf("%d update feat : %d\n", i, fv->t[j]); */
	    ft->table[fv->t[j]][gfv->array[i]->pred_mvt]--;
	    ft->table[fv->t[j]][config_oracle->mvt_array[i]]++;
	    
	    /* for averaged perceptron */
	    ft_sum->table[fv->t[j]][gfv->array[i]->pred_mvt] -= counter;
	    ft_sum->table[fv->t[j]][config_oracle->mvt_array[i]] += counter;
	  }
	}
      } 
    }
  }
}

feature_table *train_perceptron_early_update(context *ctx)
{  
  config *config_oracle;
  int mvt_oracle_code;
  char mvt_oracle_type;
  int mvt_oracle_label;
  int i,j;
  config *config_pred;
  /* int mvt_pred_code; */
  /* feat_vec *fv = feat_vec_new(feature_types_nb); */
  sentence *ref = NULL;
  int sentence_nb = 0;
  FILE *conll_file = NULL;
  FILE *conll_file2 = NULL;
  FILE *conll_file_ref = NULL;
  feature_table *ft_sum = feature_table_new(1000000, ctx->mvt_nb);
  feature_table *ft = feature_table_new(1000000, ctx->mvt_nb);
  int counter = 0;
  int epoch;
  /* int count_ok = 0; */
  /* int count_ko = 0; */
  beam *current_beam= beam_new(ctx->beam_width);
  beam *next_beam= beam_new(ctx->beam_width);
  beam *final_beam= beam_new(ctx->beam_width);
  beam *tmp_beam= NULL;
  int oracle_rank;
  /* config *argmax; */

  for(epoch = 0; epoch < ctx->iteration_nb; epoch++){
    fprintf(stderr, "[%d]", epoch + 1);
    conll_file = myfopen(ctx->input_filename, "r");
    conll_file2 = myfopen(ctx->input_filename, "r");
    conll_file_ref = myfopen(ctx->input_filename, "r");
    
    config_oracle = config_initial(conll_file, ctx->mcd_struct, 0);
    config_pred = config_initial(conll_file2, ctx->mcd_struct, 0);
    
    sentence_nb = 0;
    while((ref = sentence_read(conll_file_ref, ctx->mcd_struct)) && (sentence_nb < ctx->sent_nb)){ 
      queue_read_sentence(config_oracle->bf, conll_file, ctx->mcd_struct);
      queue_read_sentence(config_pred->bf, conll_file2, ctx->mcd_struct);
      beam_empty(final_beam);
      beam_empty(current_beam);
      beam_add(current_beam, config_pred);

      /* fprintf(stdout, "\nNEW SENT\n"); */
      
      while(!config_is_terminal(config_oracle)){
	/*---------------------------------------*/
	/* early update : if the oracle configuration is not in the beam, update weights and go to next sentence */
	oracle_rank = look_for_config_in_beam(config_oracle, current_beam);
	if(oracle_rank == -1){
	  /*	  printf("\nBEAM\n");
		  beam_print(stdout, current_beam);*/
	  perceptron_update_global(current_beam->t[0]->gfv, config_oracle, ft, ft_sum, counter++);
	  break;
	}

	/*---------------------------------------*/
	/* update beam and swap current and next */

	/*		fprintf(stdout, "CURRENT BEAM\n");
			beam_print(stdout, current_beam);*/

	fill_next_beam(current_beam, next_beam, final_beam, ft, ctx->features_model, ctx->d_perceptron_features, ctx->beam_width, TRAIN_MODE);

	/*	fprintf(stdout, "NEXT BEAM\n");
		beam_print(stdout, next_beam);*/

	beam_empty(current_beam);
	tmp_beam = current_beam;
	current_beam = next_beam;
	next_beam = tmp_beam;

	/*---------------------------------------*/
	/* compute new oracle transition */
	mvt_oracle_code = oracle_parser(config_oracle, ref);
	mvt_oracle_type = movement_type(mvt_oracle_code);
	mvt_oracle_label = movement_label(mvt_oracle_code);
	
	/*---------------------------------------*/
	/* compute new oracle configuration */
	if(mvt_oracle_type == MVT_LEFT){
	  movement_left_arc(config_oracle, mvt_oracle_label, 0);
	  continue;
	}
	if(mvt_oracle_type == MVT_RIGHT){
	  movement_right_arc(config_oracle, mvt_oracle_label, 0);
	  continue;
	}
	if(mvt_oracle_type == MVT_SHIFT){
	  movement_shift(config_oracle, 0, 0);
	  continue;
	}
      }


      /*            fprintf(stdout, "FINAL BEAM\n");
	      beam_print(stdout, final_beam);

      argmax = beam_argmax(final_beam);
      if(argmax)
      perceptron_update_global(argmax->gfv, config_oracle, ft, ft_sum, counter++);*/


      config_pred = config_initial(conll_file2, ctx->mcd_struct, 0);
      config_free(config_oracle); 
      config_oracle = config_initial(conll_file, ctx->mcd_struct, 0);
      sentence_nb++;
      sentence_free(ref);  
      
      /* if((sentence_nb % 1000) == 0){fprintf(stdout, ".");fflush(stdout);} */
    }
    fclose(conll_file);
    fclose(conll_file2);
    fclose(conll_file_ref);
  }

  for(i=0; i< ft->features_nb; i++)
    for(j=0; j< ft->classes_nb; j++)
      ft->table[i][j] -= 1/(float)counter * ft_sum->table[i][j];

  
  beam_free(current_beam);
  beam_free(next_beam);
  beam_free(final_beam);
  
  return ft;
}


int perceptron_update(int class_ref, feat_vec *fv, feature_table *ft, feature_table *ft_sum, int counter)
{
  /* int classes_nb = ft->classes_nb; */
  /* int cla; */
  int argmax;
  int feat;
  float max;

  argmax = feature_table_argmax(fv, ft, &max);

  if(argmax != class_ref){
    /* fprintf(stderr, "[%d][%d] wrong\n", epoch, i);   */
    for(feat=0; feat < fv->nb; feat++){
      if(fv->t[feat] != -1){
	ft->table[fv->t[feat]][argmax]--;
	ft->table[fv->t[feat]][class_ref]++;

	/* for averaged perceptron */
	ft_sum->table[fv->t[feat]][argmax] -= counter;
	ft_sum->table[fv->t[feat]][class_ref] += counter;
      }
    }
  }
  else{
    /* fprintf(stderr, "[%d][%d] right\n", epoch, i);   */
  }
  
  /*  feature_table_free(ft_sum);*/
  return argmax;
}
