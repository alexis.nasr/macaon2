#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<getopt.h>
#include"movement.h"
#include"oracle.h"
#include"feat_fct.h"
#include"context.h"
#include"feat_vec.h"
#include"dico_vec.h"
#include"corpus.h"
#include"word_emb.h"
#include"config2feat_vec.h"

void transform_treebank_help_message(context *ctx)
{
  context_general_help_message(ctx);
  context_mode_help_message(ctx);
  context_sent_nb_help_message(ctx);

  fprintf(stderr, "INPUT\n");
  context_conll_help_message(ctx);
  fprintf(stderr, "IN TEST MODE\n");
  context_alphabet_help_message(ctx);

  fprintf(stderr, "OUTPUT\n");
  context_cff_help_message(ctx);
  context_fann_help_message(ctx);
  fprintf(stderr, "IN TRAIN MODE\n");
  context_alphabet_help_message(ctx);

}

void transform_treebank_check_options(context *ctx)
{
  if(!ctx->conll_filename
     || ctx->help
     /* || !ctx->mcd_filename */
     || !(ctx->cff_filename || ctx->fann_filename)
     ){
    transform_treebank_help_message(ctx);
    exit(1);
  }
}

void print_mvt_fann(FILE *f, int mvt_nb, int mvt_code)
{
  int i;
  if(mvt_code == 0)
    fprintf(f, "1");
  else
    fprintf(f, "0");
  for(i=1; i < mvt_nb; i++){
    if(i == mvt_code)
      fprintf(f, " 1");
    else
      fprintf(f, " 0");
  }
  fprintf(f, "\n");
}

int generate_training_file_stream(FILE *output_file, context *ctx)
{  
  config *c;
  int mvt_code;
  char mvt_type;
  int mvt_label;
  feat_vec *fv = feat_vec_new(feature_types_nb);
  sentence *ref = NULL;
  int nb_trans = 0;
  int sentence_nb = 0;
  int root_label = dico_string2int(ctx->mcd_struct->dico_array[FEAT_TYPE_LABEL], ctx->root_label);
  FILE *conll_file = myfopen(ctx->conll_filename, "r");
  FILE *conll_file_ref = myfopen(ctx->conll_filename, "r");
  word *b0;
  int b0_index;
  word *s0;
  int s0_index;
  int premier_mot_consomme = 0;

  c = config_initial(conll_file, ctx->mcd_struct, 10, 5);

  while((ref = sentence_read(conll_file_ref , ctx->mcd_struct)) && (sentence_nb < ctx->sent_nb)){ 
  /* while((s = sentence_read_next_word(conll_file , s, ctx->mcd_struct)) && (sentence_nb < ctx->sent_nb)){ */
  /* while(!config_is_terminal(c) && (sentence_nb < ctx->sent_nb)){ */
    /* sentence_print(stdout, ref, NULL);  */

    
    while(1){
        /* config_print(stdout,c);      */
      
      if(ctx->fann_filename)
	config2feat_vec_fann(ctx->features_model, c, fv, ctx->mode);
      else /*if(ctx->cff_filename)*/
	config2feat_vec_cff(ctx->features_model, c, ctx->d_perceptron_features, fv, ctx->mode);
      
      mvt_code = oracle(c, ref);
      nb_trans++;
      
      mvt_type = movement_type(mvt_code);
      mvt_label = movement_label(mvt_code);

      /* printf("mvt type = %d mvt label = %d\n", mvt_type, mvt_label); */
      
      if(ctx->cff_filename){
	fprintf(output_file, "%d", mvt_code);
	feat_vec_print(output_file, fv);
      }
      else if(ctx->fann_filename){
	/* feat_vec_print_dnn(output_file, fv, ctx->features_model, ctx->mcd_struct); */
	print_mvt_fann(output_file, ctx->mvt_nb, mvt_code);
	fprintf(output_file, "\n\n");
      }

    s0 = stack_top(c->st);
    if(s0)s0_index = word_get_index(s0);
    
    b0 = queue_elt_n(c->bf, 0);
    if(b0)b0_index = word_get_index(b0);
    

    if(queue_is_empty(c->bf)) break;


    if((mvt_type == MVT_RIGHT) && (mvt_label == root_label)){       /* sentence is complete */
      /*      printf("sentence complete config : ");
	      config_print(stdout,c);   */
      
      /* printf("sentence is complete\n"); */
      /* create the root arc */
      movement_right_arc(c, mvt_label, 0);
      
      /* shift dummy word in stack */
      movement_shift(c, 1, 0);
      /* config_print(stdout,c);    */
      
      /* empty depset */
      depset_free(c->ds);
      c->ds = depset_new();
      sentence_nb++;
      break;
    }

    if(mvt_type == MVT_LEFT){
	/* printf("LEFT\n"); */
	movement_left_arc(c, mvt_label, 0);
	if(s0_index == 1) premier_mot_consomme = 1;
	continue;
      }
      if(mvt_type == MVT_RIGHT){
	/* printf("RIGHT\n"); */
	movement_right_arc(c, mvt_label, 0);
	if(b0_index == 1) premier_mot_consomme = 1;
	continue;
      }
      if(mvt_type == MVT_SHIFT){
	/* printf("SHIFT\n"); */
	movement_shift(c, 1, 0);
	continue;
      }
    }
  } 
  return nb_trans;
}

int generate_training_file_buffer(FILE *output_file, context *ctx)
{  
  config *c;
  int mvt_code;
  char mvt_type;
  int mvt_label;
  feat_vec *fv = feat_vec_new(feature_types_nb);
  sentence *ref = NULL;
  int nb_trans = 0;
  int sentence_nb = 0;
  FILE *conll_file = myfopen(ctx->conll_filename, "r");
  FILE *conll_file_ref = myfopen(ctx->conll_filename, "r");

  c = config_initial(conll_file, ctx->mcd_struct, 1000, 0);

  while((ref = sentence_read(conll_file_ref, ctx->mcd_struct)) && (sentence_nb < ctx->sent_nb)){ 
      /* sentence_print(stdout, ref, NULL);    */
    queue_read_sentence(c->bf, conll_file, ctx->mcd_struct);
    while(!config_is_terminal(c)){
      /* config_print(stdout,c);     */
      
      if(ctx->fann_filename)
	config2feat_vec_fann(ctx->features_model, c, fv, ctx->mode);
      else /*if(ctx->cff_filename)*/
	config2feat_vec_cff(ctx->features_model, c, ctx->d_perceptron_features, fv, ctx->mode);
      
      mvt_code = oracle(c, ref);
      nb_trans++;
      
      mvt_type = movement_type(mvt_code);
      mvt_label = movement_label(mvt_code);

      /* printf("mvt type = %d mvt label = %d\n", mvt_type, mvt_label); */
      
      if(ctx->cff_filename){
	fprintf(output_file, "%d", mvt_code);
	feat_vec_print(output_file, fv);
      }
      else if(ctx->fann_filename){
	feat_vec_print_dnn(output_file, fv, ctx->features_model, ctx->mcd_struct);
	print_mvt_fann(output_file, ctx->mvt_nb, mvt_code);
	fprintf(output_file, "\n\n");
      }
      if(mvt_type == MVT_LEFT){
	/* printf("LEFT\n"); */
	movement_left_arc(c, mvt_label, 0);
	continue;
      }
      if(mvt_type == MVT_RIGHT){
	/* printf("RIGHT\n"); */
	movement_right_arc(c, mvt_label, 0);
	continue;
      }
      if(mvt_type == MVT_SHIFT){
	/* printf("SHIFT\n"); */
	movement_shift(c, 0, 0);
	continue;
      }
    }
    config_free(c); 
    c = config_initial(conll_file, ctx->mcd_struct, 1000, 0);
    sentence_nb++;
  }
  return nb_trans;
}

int main(int argc, char *argv[])
{
  context *ctx;
  int input_size, output_size;
  int nb_trans = 0;
  FILE *output_file;
  
  ctx = context_read_options(argc, argv);
  transform_treebank_check_options(ctx);
  
  if(ctx->mode == TRAIN_MODE){
    mcd_extract_dico_from_corpus(ctx->mcd_struct, ctx->conll_filename);
    ctx->vocabs = mcd_build_dico_vec(ctx->mcd_struct);
  }
  else if(ctx->mode == TEST_MODE){
    ctx->vocabs = dico_vec_read(ctx->vocabs_filename, ctx->hash_ratio);
    mcd_link_to_dico(ctx->mcd_struct, ctx->vocabs);
  }
  
  ctx->dico_labels = dico_vec_get_dico(ctx->vocabs, (char *)"LABEL");
  if(ctx->dico_labels == NULL){
    fprintf(stderr, "cannot find label names\n");
    return 1;
  }
  ctx->mvt_nb = ctx->dico_labels->nbelem * 2 + 1;
    
  feat_model_compute_ranges(ctx->features_model, ctx->mcd_struct, ctx->mvt_nb);



  /* perceptron mode */
  if(ctx->cff_filename){

    /* in train mode create feature dictionnary for perceptron */
    if(ctx->mode == TRAIN_MODE)
      ctx->d_perceptron_features = dico_new((char *)"d_perceptron_features", 10000000);

    /* in test mode read feature dictionnary for perceptron */
    if(ctx->mode == TEST_MODE)
    ctx->d_perceptron_features = dico_vec_get_dico(ctx->vocabs, (char *)"d_perceptron_features");
    /* ctx->d_perceptron_features = dico_read(ctx->perceptron_features_filename, 0.5); */

    /* add the feature dictionnary to the dico vector */
    dico_vec_add(ctx->vocabs, ctx->d_perceptron_features);


    /* open output file */
    output_file = myfopen(ctx->cff_filename, "w");
  }
  
  /* dnn mode */
  if(ctx->fann_filename){
    input_size = ctx->features_model->dim;
    output_size = ctx->mvt_nb;

    fprintf(stderr, "input size = %d\n", input_size); 
    fprintf(stderr, "output size = %d\n", output_size);
    
    /* open output file */
    output_file = myfopen(ctx->fann_filename, "w");
    /* spaces are here to leave room to write number of lines in the file when it will be known */
    fprintf(output_file, "                   %d %d\n", input_size, output_size); 
  }
  
  /* open conll file */
  /* s = sentence_init(ctx->mcd_struct, conll_file); */
  /* ref = sentence_init(ctx->mcd_struct, conll_file_ref); */


  if(ctx->stream_mode)
    nb_trans = generate_training_file_stream(output_file, ctx);
  else
    nb_trans = generate_training_file_buffer(output_file, ctx);
  
  if(ctx->fann_filename){
    rewind(output_file);
    fprintf(output_file, "%d", nb_trans);
  }
  
  if((ctx->cff_filename) && (ctx->mode == TRAIN_MODE)){
    /* dico_print(ctx->perceptron_features_filename, ctx->d_perceptron_features); */
    dico_vec_print(ctx->vocabs_filename, ctx->vocabs);

  }
  
  fclose(output_file);
  context_free(ctx);
  return 0;
}


      
      /*    if(mvt_type == MVT_CUT){
	    sentence_free(s);
	    s = NULL;
	    sentence_free(ref);
	    r = NULL;
	    movement_cut(c, 0);
	    sentence_nb++;
	    c = config_initial(s);
	    continue;
	    }*/
      /* config_print(stdout, c); printf("\n"); */
      /* printf("\n"); */
