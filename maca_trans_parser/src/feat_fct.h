#ifndef __FEATURES__
#define __FEATURES__

#include "feat_lib.h"
/* typedef int (*feat_fct) (config *c); */
/* typedef int (*feat_fct) (void *c); */

feat_lib *feat_lib_build(void);

/* word features */
int s0g(void *input);
int s0sf(void *input);

int s0f(void *input);
int s0l(void *input);
int s0c(void *input);
int s0p(void *input);
int s0m(void *input);
int s0s(void *input);
int s0A(void *input);
int s0B(void *input);
int s0C(void *input);
int s0D(void *input);
int s0E(void *input);
int s0F(void *input);
int s0G(void *input);
int s0H(void *input);
int s0I(void *input);
int s0J(void *input);
int s0K(void *input);
int s0L(void *input);
int s0M(void *input);
int s0N(void *input);
int s0O(void *input);
int s0P(void *input);
int s0Q(void *input);
int s0R(void *input);
int s0S(void *input);
int s0T(void *input);
int s0U(void *input);
int s0V(void *input);
int s0W(void *input);
int s0X(void *input);
int s0Y(void *input);
int s0Z(void *input);

int s0U1(void *input);
int s0sgn(void *input);

int s0r(void *input);

int s1g(void *input);
int s1sf(void *input);


int s1f(void *input);
int s1l(void *input);
int s1c(void *input);
int s1p(void *input);
int s1m(void *input);
int s1s(void *input);
int s1A(void *input);
int s1B(void *input);
int s1C(void *input);
int s1D(void *input);
int s1E(void *input);
int s1F(void *input);
int s1G(void *input);
int s1H(void *input);
int s1I(void *input);
int s1J(void *input);
int s1K(void *input);
int s1L(void *input);
int s1M(void *input);
int s1N(void *input);
int s1O(void *input);
int s1P(void *input);
int s1Q(void *input);
int s1R(void *input);
int s1S(void *input);
int s1T(void *input);
int s1U(void *input);
int s1V(void *input);
int s1W(void *input);
int s1X(void *input);
int s1Y(void *input);
int s1Z(void *input);

int s1r(void *input);

int s2g(void *input);
int s2sf(void *input);

int s2f(void *input);
int s2l(void *input);
int s2c(void *input);
int s2p(void *input);
int s2m(void *input);
int s2s(void *input);
int s2A(void *input);
int s2B(void *input);
int s2C(void *input);
int s2D(void *input);
int s2E(void *input);
int s2F(void *input);
int s2G(void *input);
int s2H(void *input);
int s2I(void *input);
int s2J(void *input);
int s2K(void *input);
int s2L(void *input);
int s2M(void *input);
int s2N(void *input);
int s2O(void *input);
int s2P(void *input);
int s2Q(void *input);
int s2R(void *input);
int s2S(void *input);
int s2T(void *input);
int s2U(void *input);
int s2V(void *input);
int s2W(void *input);
int s2X(void *input);
int s2Y(void *input);
int s2Z(void *input);

int s2r(void *input);

int s3f(void *input);
int s3l(void *input);
int s3c(void *input);
int s3p(void *input);
int s3m(void *input);
int s3s(void *input);
int s3A(void *input);
int s3B(void *input);
int s3C(void *input);
int s3D(void *input);
int s3E(void *input);
int s3F(void *input);
int s3G(void *input);
int s3H(void *input);
int s3I(void *input);
int s3J(void *input);
int s3K(void *input);
int s3L(void *input);
int s3M(void *input);
int s3N(void *input);
int s3O(void *input);
int s3P(void *input);
int s3Q(void *input);
int s3R(void *input);
int s3S(void *input);
int s3T(void *input);
int s3U(void *input);
int s3V(void *input);
int s3W(void *input);
int s3X(void *input);
int s3Y(void *input);
int s3Z(void *input);
int s3r(void *input);

int b0g(void *input);
int b0sf(void *input);
int b0len(void *input);


int b0f(void *input);
int b0l(void *input);
int b0c(void *input);
int b0p(void *input);
int b0m(void *input);
int b0s(void *input);
int b0A(void *input);
int b0B(void *input);
int b0C(void *input);
int b0D(void *input);
int b0E(void *input);
int b0F(void *input);
int b0G(void *input);
int b0H(void *input);
int b0I(void *input);
int b0J(void *input);
int b0K(void *input);
int b0L(void *input);
int b0M(void *input);
int b0N(void *input);
int b0O(void *input);
int b0P(void *input);
int b0Q(void *input);
int b0R(void *input);
int b0S(void *input);
int b0T(void *input);
int b0U(void *input);
int b0V(void *input);
int b0W(void *input);
int b0X(void *input);
int b0Y(void *input);
int b0Z(void *input);
int b0r(void *input);

int b0U1(void *input);
int b0sgn(void *input);

int b1f(void *input);
int b1l(void *input);
int b1c(void *input);
int b1p(void *input);
int b1m(void *input);
int b1s(void *input);
int b1A(void *input);
int b1B(void *input);
int b1C(void *input);
int b1D(void *input);
int b1E(void *input);
int b1F(void *input);
int b1G(void *input);
int b1H(void *input);
int b1I(void *input);
int b1J(void *input);
int b1K(void *input);
int b1L(void *input);
int b1M(void *input);
int b1N(void *input);
int b1O(void *input);
int b1P(void *input);
int b1Q(void *input);
int b1R(void *input);
int b1S(void *input);
int b1T(void *input);
int b1U(void *input);
int b1V(void *input);
int b1W(void *input);
int b1X(void *input);
int b1Y(void *input);
int b1Z(void *input);

int b1U1(void *input);
int b1sgn(void *input);

int b1r(void *input);



int b2f(void *input);
int b2l(void *input);
int b2c(void *input);
int b2p(void *input);
int b2m(void *input);
int b2s(void *input);
int b2A(void *input);
int b2B(void *input);
int b2C(void *input);
int b2D(void *input);
int b2E(void *input);
int b2F(void *input);
int b2G(void *input);
int b2H(void *input);
int b2I(void *input);
int b2J(void *input);
int b2K(void *input);
int b2L(void *input);
int b2M(void *input);
int b2N(void *input);
int b2O(void *input);
int b2P(void *input);
int b2Q(void *input);
int b2R(void *input);
int b2S(void *input);
int b2T(void *input);
int b2U(void *input);
int b2V(void *input);
int b2W(void *input);
int b2X(void *input);
int b2Y(void *input);
int b2Z(void *input);

int b2r(void *input);

int b2U1(void *input);
int b2sgn(void *input);


int b3f(void *input);
int b3l(void *input);
int b3c(void *input);
int b3p(void *input);
int b3m(void *input);
int b3s(void *input);
int b3A(void *input);
int b3B(void *input);
int b3C(void *input);
int b3D(void *input);
int b3E(void *input);
int b3F(void *input);
int b3G(void *input);
int b3H(void *input);
int b3I(void *input);
int b3J(void *input);
int b3K(void *input);
int b3L(void *input);
int b3M(void *input);
int b3N(void *input);
int b3O(void *input);
int b3P(void *input);
int b3Q(void *input);
int b3R(void *input);
int b3S(void *input);
int b3T(void *input);
int b3U(void *input);
int b3V(void *input);
int b3W(void *input);
int b3X(void *input);
int b3Y(void *input);
int b3Z(void *input);

int b3r(void *input);

int bmip(void *c, int i);

int bm1f(void *input);
int bm1l(void *input);
int bm1c(void *input);
int bm1p(void *input);
int bm1m(void *input);
int bm1s(void *input);
int bm1A(void *input);
int bm1B(void *input);
int bm1C(void *input);
int bm1D(void *input);
int bm1E(void *input);
int bm1F(void *input);
int bm1G(void *input);
int bm1H(void *input);
int bm1I(void *input);
int bm1J(void *input);
int bm1K(void *input);
int bm1L(void *input);
int bm1M(void *input);
int bm1N(void *input);
int bm1O(void *input);
int bm1P(void *input);
int bm1Q(void *input);
int bm1R(void *input);
int bm1S(void *input);
int bm1T(void *input);
int bm1U(void *input);
int bm1V(void *input);
int bm1W(void *input);
int bm1X(void *input);
int bm1Y(void *input);
int bm1Z(void *input);

int bm1U1(void *input);
int bm1sgn(void *input);

int bm2f(void *input);
int bm2l(void *input);
int bm2c(void *input);
int bm2p(void *input);
int bm2m(void *input);
int bm2s(void *input);
int bm2A(void *input);
int bm2B(void *input);
int bm2C(void *input);
int bm2D(void *input);
int bm2E(void *input);
int bm2F(void *input);
int bm2G(void *input);
int bm2H(void *input);
int bm2I(void *input);
int bm2J(void *input);
int bm2K(void *input);
int bm2L(void *input);
int bm2M(void *input);
int bm2N(void *input);
int bm2O(void *input);
int bm2P(void *input);
int bm2Q(void *input);
int bm2R(void *input);
int bm2S(void *input);
int bm2T(void *input);
int bm2U(void *input);
int bm2V(void *input);
int bm2W(void *input);
int bm2X(void *input);
int bm2Y(void *input);
int bm2Z(void *input);

int bm3f(void *input);
int bm3l(void *input);
int bm3c(void *input);
int bm3p(void *input);
int bm3m(void *input);
int bm3s(void *input);
int bm3A(void *input);
int bm3B(void *input);
int bm3C(void *input);
int bm3D(void *input);
int bm3E(void *input);
int bm3F(void *input);
int bm3G(void *input);
int bm3H(void *input);
int bm3I(void *input);
int bm3J(void *input);
int bm3K(void *input);
int bm3L(void *input);
int bm3M(void *input);
int bm3N(void *input);
int bm3O(void *input);
int bm3P(void *input);
int bm3Q(void *input);
int bm3R(void *input);
int bm3S(void *input);
int bm3T(void *input);
int bm3U(void *input);
int bm3V(void *input);
int bm3W(void *input);
int bm3X(void *input);
int bm3Y(void *input);
int bm3Z(void *input);

/* structural features */

int gs0l(void *input);
int gs0p(void *input);

int ldep_s0r(void *input);
int rdep_s0r(void *input);
int ldep_s0p(void *input);
int rdep_s0p(void *input);
int ldep_s1r(void *input);
int rdep_s1r(void *input);
int ldep_s1p(void *input);
int rdep_s1p(void *input);
int ndep_b0(void *input);
int ndep_s0(void *input);
int ldep_b0r(void *input);
int rdep_b0r(void *input);
int ldep_b0p(void *input);
int rdep_b0p(void *input);

/* distance features */

int dist_s0_b0(void *input);

/* configurational features */

int sh(void *input);
int bh(void *input);
/* int dh(void *input); */

int t1(void *input);
int t2(void *input);
int t3(void *input);
int t4(void *input);

int mvt0(void *input);
int mvt1(void *input);
int delta1(void *input);
int mvt2(void *input);
int delta2(void *input);
int mvt3(void *input);
int delta3(void *input);

#endif
