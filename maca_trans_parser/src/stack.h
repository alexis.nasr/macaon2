#ifndef __STACK__
#define __STACK__

#include<stdio.h>
#include"word.h"

#define stack_height(s) (s)->top 
#define stack_nbelem(s) (s)->top 
#define stack_elt_n(s, n) (s)->array[(s)->top - (n) - 1]

#define stack_s0(s) ((s)->top > 0)? (s)->array[(s)->top - 1] : NULL
#define stack_s1(s) ((s)->top > 1)? (s)->array[(s)->top - 2] : NULL
#define stack_s2(s) ((s)->top > 2)? (s)->array[(s)->top - 3] : NULL
#define stack_s3(s) ((s)->top > 3)? (s)->array[(s)->top - 4] : NULL
#define stack_s4(s) ((s)->top > 4)? (s)->array[(s)->top - 5] : NULL
#define stack_s5(s) ((s)->top > 5)? (s)->array[(s)->top - 6] : NULL


typedef struct {
  int size;
  word **array;
  int top;
} stack;

stack *stack_new(void);
stack *stack_copy(stack *s);
word *stack_pop(stack *s);
void stack_push(stack *s, word *w);
word *stack_top(stack *s);
void stack_print(FILE *buffer, stack *s);
void stack_free(stack *s);
int stack_is_empty(stack *s);
/* int stack_height(stack *s); */
void stack_trim_to_size(stack *s, int k);
#endif
