#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"util.h"
#include"movement_parser_arc_eager.h"
#include"movements.h"

void movement_parser_print(FILE *f, int mvt_code, dico *dico_labels){
  int mvt_type = movement_parser_type(mvt_code);
  int mvt_label = movement_parser_label(mvt_code);
  char *label;
  if(mvt_type == MVT_PARSER_SHIFT)  {fprintf(f, "SHIFT"); return;}
  if(mvt_type == MVT_PARSER_REDUCE) {fprintf(f, "REDUCE"); return;}
  if(mvt_type == MVT_PARSER_ROOT)   {fprintf(f, "ROOT"); return;}
  if(mvt_type == MVT_PARSER_EOS)    {fprintf(f, "EOS"); return;}
  if(mvt_type == MVT_PARSER_RIGHT)   fprintf(f, "RIGHT");
  else fprintf(f, "LEFT");
  label = dico_int2string(dico_labels, mvt_label);
  fprintf(f, " %s", label);
}

void movement_parser_sprint(char *f, int mvt_code, dico *dico_labels){
  int mvt_type = movement_parser_type(mvt_code);
  int mvt_label = movement_parser_label(mvt_code);
  char *label;
  if(mvt_type == MVT_PARSER_SHIFT)  {sprintf(f, "SHIFT"); return;}
  if(mvt_type == MVT_PARSER_REDUCE) {sprintf(f, "REDUCE"); return;}
  if(mvt_type == MVT_PARSER_ROOT)   {sprintf(f, "ROOT"); return;}
  if(mvt_type == MVT_PARSER_EOS)    {sprintf(f, "EOS"); return;}
  if(mvt_type == MVT_PARSER_RIGHT)   sprintf(f, "RIGHT");
  else sprintf(f, "LEFT");
  label = dico_int2string(dico_labels, mvt_label);
  sprintf(f, " %s", label);
}


int movement_parser_type(int mvt)
{
  if(mvt == MVT_PARSER_SHIFT)     return MVT_PARSER_SHIFT;  
  if(mvt == MVT_PARSER_REDUCE)    return MVT_PARSER_REDUCE; 
  if(mvt == MVT_PARSER_ROOT)      return MVT_PARSER_ROOT;   
  if(mvt == MVT_PARSER_EOS)       return MVT_PARSER_EOS;    
  if(mvt % 2 == 0)                return MVT_PARSER_LEFT; /* even movements are left movements */
  return MVT_PARSER_RIGHT;                              /* odd movements are right movements */
}

int movement_parser_label(int mvt)
{
  if(mvt == MVT_PARSER_SHIFT) return -1;  
  if(mvt == MVT_PARSER_REDUCE) return -1; 
  if(mvt == MVT_PARSER_ROOT) return -1;   
  if(mvt == MVT_PARSER_EOS) return -1;    
  if(mvt % 2 == 0)            /* even codes correspond to left movements */
    return mvt / 2 - 2;
  return (mvt - 1) / 2 - 2;   /* odd codes correspond to right movements */
}

int movement_parser_eos(config *c)
{
  return movement_eos(c, MVT_PARSER_EOS);
}

int movement_parser_eos_undo(config *c)
{
  return movement_eos_undo(c);
}

int movement_parser_left_arc(config *c, int label)
{
  return movement_left_arc(c, movement_parser_left_code(label), label);
}

int movement_parser_left_arc_undo(config *c)
{
  return movement_left_arc_undo(c);
}

int movement_parser_right_arc(config *c, int label)
{
  return movement_right_arc(c, movement_parser_right_code(label), label);
}

int movement_parser_right_arc_undo(config *c)
{
  return movement_right_arc_undo(c);
}

int movement_parser_shift(config *c)
{
  return movement_shift(c, MVT_PARSER_SHIFT);
}

int movement_parser_shift_undo(config *c)
{
  return movement_shift_undo(c);
}

int movement_parser_reduce(config *c)
{
  return movement_reduce(c, MVT_PARSER_REDUCE);
}

int movement_parser_reduce_undo(config *c)
{
  return movement_reduce_undo(c);
}

int movement_parser_root(config *c, int root_code)
{
  return movement_root(c, MVT_PARSER_ROOT, root_code);
}

int movement_parser_root_undo(config *c)
{
  return movement_root_undo(c);
}

int movement_parser_undo(config *c)
{
  int result;
  int mvt_type = movement_parser_type(mvt_get_type(mvt_stack_top(config_get_history(c))));
  switch(mvt_type){
  case MVT_PARSER_LEFT :
    result = movement_parser_left_arc_undo(c);
    break;
  case MVT_PARSER_RIGHT:
    result = movement_parser_right_arc_undo(c);
    break;
  case MVT_PARSER_REDUCE:
    result = movement_parser_reduce_undo(c);
    break;
  case MVT_PARSER_ROOT:
    result = movement_parser_root_undo(c);
    break;
  case MVT_PARSER_EOS:
    result = movement_parser_eos_undo(c);
    break;
  case MVT_PARSER_SHIFT:
    result = movement_parser_shift_undo(c);
    break;
  default :
    printf("type pas reconnu :/ \n");
    }
  return result;
}
