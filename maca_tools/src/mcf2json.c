#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<getopt.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include"mcd.h"
#include"util.h"
#include"word_buffer.h"

typedef struct {
  int help;
  int verbose;
  int debug_mode;
  char *program_name;
  char *conll_filename;
  char *mcf_filename;
  char *mcd_filename;
  mcd *mcd_struct;
  char *root_dir;
} context;

void mcf2json_context_free(context *ctx)
{
  if(ctx){
    if(ctx->program_name)
      free(ctx->program_name);
    if(ctx->conll_filename)
      free(ctx->conll_filename);
    if(ctx->mcf_filename)
      free(ctx->mcf_filename);
    if(ctx->mcd_filename)
      free(ctx->mcd_filename);
    if(ctx->mcd_struct)
      mcd_free(ctx->mcd_struct);
    if(ctx->root_dir)
      free(ctx->root_dir);
    free(ctx);
  }
}

context *mcf2json_context_new(void)
{
  context *ctx = (context *)memalloc(sizeof(context));

  ctx->help = 0;
  ctx->verbose = 0;
  ctx->debug_mode = 0;
  ctx->program_name = NULL;
  ctx->conll_filename = NULL;
  ctx->mcf_filename = NULL;
  ctx->mcd_filename = NULL;
  ctx->mcd_struct = NULL;
  ctx->root_dir = NULL;
  return ctx;
}

void mcf2json_context_general_help_message(context *ctx)
{
  fprintf(stderr, "usage: %s [options]\n", ctx->program_name);
  fprintf(stderr, "Options:\n");
  fprintf(stderr, "\t-h --help             : print this message\n");
  fprintf(stderr, "\t-v --verbose          : activate verbose mode\n");
  fprintf(stderr, "\t-C --mcd              : mcd filename\n");
  fprintf(stderr, "\t-i --mcf              : mcf filename (read from stdin if absent)\n");
  fprintf(stderr, "\t-r --root             : root directory of the json files\n");
}

void mcf2json_check_options(context *ctx){
  if(ctx->help){
    mcf2json_context_general_help_message(ctx);
    exit(1);
  }
}

context *mcf2json_context_read_options(int argc, char *argv[])
{
  int c;
  int option_index = 0;
  context *ctx = mcf2json_context_new();

  ctx->program_name = strdup(argv[0]);

  static struct option long_options[6] =
    {
      {"help",                no_argument,       0, 'h'},
      {"verbose",             no_argument,       0, 'v'},
      {"debug",               no_argument,       0, 'd'},
      {"mcd",                 required_argument, 0, 'C'}, 
      {"mcf",                 required_argument, 0, 'i'},
      {"root",                required_argument, 0, 'r'},
    };
  optind = 0;
  opterr = 0;
  
  while ((c = getopt_long (argc, argv, "hvdC:i:r:", long_options, &option_index)) != -1){ 
    switch (c)
      {
      case 'd':
	ctx->debug_mode = 1;
	break;
      case 'h':
	ctx->help = 1;
	break;
      case 'v':
	ctx->verbose = 1;
	break;
      case 'i':
	ctx->mcf_filename = strdup(optarg);
	break;
      case 'C':
	ctx->mcd_filename = strdup(optarg);
	break;
      case 'r':
	ctx->root_dir = strdup(optarg);
	break;
      }
  }

  if(ctx->mcd_filename){
    ctx->mcd_struct = mcd_read(ctx->mcd_filename, ctx->verbose);
  }
  else{
    ctx->mcd_struct = mcd_build_wpmlgfs();
  }
  return ctx;
}

void print_footer(FILE *output_file)
{
  fprintf(output_file, "]\n");
  fprintf(output_file, "}\n");
}


void print_header(FILE *output_file, mcd *mcd_struct, char *filename)
{
  int pos_col =  mcd_get_pos_col(mcd_struct);
  int label_col =  mcd_get_label_col(mcd_struct);
  dico *dico_pos = mcd_struct->dico_array[pos_col];
  dico *dico_label = mcd_struct->dico_array[label_col];
  int i;

  //  printf("dico label : %d dico = %d\n", label_col, dico_label);
  
  fprintf(output_file, "{\n");
  fprintf(output_file, "\"header\":{\n");
  fprintf(output_file, "\"id\": \"\",\n");
  fprintf(output_file, "\"timestamp\": \"\",\n");
  fprintf(output_file, "\"filename\": \"%s\",\n", filename);

  fprintf(output_file, "\"labels_segment\": [");
  for(i=0; i < dico_pos->nbelem; i++){
    if(i != 0) fprintf(output_file, ", ");
    fprintf(output_file, "\"%s\"", dico_pos->array[i]); 
  }
  fprintf(output_file, "],\n");

  fprintf(output_file, "\"labels_link\": [");
  for(i=0; i < dico_label->nbelem; i++){
    if(i != 0) fprintf(output_file, ", ");
     fprintf(output_file, "\"%s\"", dico_label->array[i]); 
  }
  fprintf(output_file, "]\n");
  
  fprintf(output_file, "},\n");
  
  fprintf(output_file, "\"annotation\":{\n");
  fprintf(output_file, "\"name\": \"\",\n");
  fprintf(output_file, "\"time_start\": \"\",\n");
  fprintf(output_file, "\"time_end\": \"\"\n");
  fprintf(output_file, "},\n");
  fprintf(output_file, "\"documents\": [");

}

void print_link(FILE *output_file, word_buffer *wb, int index_first_word, int index)
{
  int gov_col =   mcd_get_gov_col(word_buffer_get_mcd(wb));
  int label_col = mcd_get_label_col(word_buffer_get_mcd(wb));
  int index_col =  mcd_get_index_col(word_buffer_get_mcd(wb));

  int dep_span_status_col =  mcd_get_dep_span_status_col(word_buffer_get_mcd(wb));
  int dep_label_status_col =  mcd_get_dep_label_status_col(word_buffer_get_mcd(wb));


  word *w = word_buffer_get_word_n(wb, index);
  
  fprintf(output_file, "{");
  fprintf(output_file, "\"orig\": %d, ", index - index_first_word);
  fprintf(output_file, "\"dest\":");
  if(gov_col){
    if((word_get_gov(w) == 0) || ((word_get_gov(w) + index) < 0))
      fprintf(output_file, "-1");
    else{
      word *gov = word_buffer_get_word_n(wb, word_get_gov(w) + index);
      fprintf(output_file, "%d", word_get_gov(w) + index - index_first_word);
    }
  }
  else
    fprintf(output_file, "_");
  fprintf(output_file, ", ");
  
  
  fprintf(output_file, "\"label\": \"");
  if(label_col != -1)
    word_print_col_n(output_file, w, label_col);
  else
    fprintf(output_file, "_");
  fprintf(output_file, "\", ");
  
  /* ajoute le 04/05/2021 par Alexis pour Orfeo */
  
  fprintf(output_file, "\"status_link\": \"");
  if(dep_span_status_col != -1)
    word_print_col_n(output_file, w, dep_span_status_col);
  else
    fprintf(output_file, "_");
  fprintf(output_file, "\", ");

  fprintf(output_file, "\"status_lab\": \"");
  
  if(dep_label_status_col != -1)
    word_print_col_n(output_file, w, dep_label_status_col);
  else
    fprintf(output_file, "_");
  fprintf(output_file, "\", ");
  /* ------------------------------------------- */
  fprintf(output_file, "\"timestamp\": \"\", ");
  fprintf(output_file, "\"author\": \"\", ");
  fprintf(output_file, "\"target\": \"\"");
  fprintf(output_file, "}");
  
}


void print_links(FILE *output_file, word_buffer *wb, int index_first_word, int index_last_word)
{
  int index;
  int first_link = 1;
  
  fprintf(output_file, "\"links\": [");
  for(index = index_first_word; index <= index_last_word; index++){
    if(first_link == 1) first_link = 0; else fprintf(output_file, ",");
    fprintf(output_file, "\n");
    print_link(output_file, wb, index_first_word, index);
  }
  fprintf(output_file," ]");
}


void print_segment(FILE *output_file, word_buffer *wb, int index_first_word, int index)
{
  int pos_col =  mcd_get_pos_col(word_buffer_get_mcd(wb));
  int word_span_status_col = mcd_get_word_span_status_col(word_buffer_get_mcd(wb));
  int pos_label_status_col = mcd_get_pos_label_status_col(word_buffer_get_mcd(wb));
  word *w = word_buffer_get_word_n(wb, index);

  fprintf(output_file, "{ ");
  fprintf(output_file, "\"start\": %d, ", index - index_first_word);
  fprintf(output_file, "\"end\": %d, ", index - index_first_word);
  fprintf(output_file, "\"label\": \"");
  
  if(pos_col != -1)
    word_print_col_n(output_file, w, pos_col);
  else
    fprintf(output_file, "_");
  fprintf(output_file, "\", ");
  
  fprintf(output_file, "\"status_seg\": \"");
  if(word_span_status_col != -1)
    word_print_col_n(output_file, w, word_span_status_col);
  else
    fprintf(output_file, "_");
  fprintf(output_file, "\", ");
  
  fprintf(output_file, "\"status_lab\": \"");
  if(pos_label_status_col != -1)
    word_print_col_n(output_file, w, pos_label_status_col);
  else
    fprintf(output_file, "_");
  fprintf(output_file, "\", ");


  fprintf(output_file, "\"timestamp\": \"\", ");
  fprintf(output_file, "\"author\": \"\", ");
  fprintf(output_file, "\"target\": \"\", ");
  fprintf(output_file, "\"priority\": \"\"");
  fprintf(output_file, " }");
}

void print_segments(FILE *output_file, word_buffer *wb, int index_first_word, int index_last_word)
{
  int index;
  int first_segment = 1;

  fprintf(output_file, "\"segments\": [");
  for(index = index_first_word; index <= index_last_word; index++){
    if(first_segment == 1) first_segment = 0; else fprintf(output_file, ",");
    fprintf(output_file, "\n");
    print_segment(output_file, wb, index_first_word, index);
  }
  fprintf(output_file," ],\n");
}

void print_token(FILE *output_file, word_buffer *wb, int index)
{
  int form_col =  mcd_get_form_col(word_buffer_get_mcd(wb));
  int length_col =  mcd_get_length_col(word_buffer_get_mcd(wb));
  word *w = word_buffer_get_word_n(wb, index);
  char token[5000];
  int length_token, i;

  fprintf(output_file, "{ ");
  fprintf(output_file, "\"id\": %d, ", word_get_index(w));
  fprintf(output_file, "\"word\": \"");
  if(form_col != -1){
    word_sprint_col_n(token, w, form_col);
    length_token = strlen(token);
    for(i=0; i < length_token; i++){
      if(token[i] == '"')
	fprintf(output_file, "&quot");
      else
	fprintf(output_file, "%c", token[i]);
    }
   }
  else
    fprintf(output_file, "_");
  fprintf(output_file, "\", ");
  
  fprintf(output_file, "\"bold\": 0, ");
  fprintf(output_file, "\"newline\": 0 ");
  fprintf(output_file, "}");
}


void print_tokens(FILE *output_file, word_buffer *wb, int index_first_word, int index_last_word)
{
  int index;
  int first_token = 1;

  fprintf(output_file, "\"tokens\": [");
  for(index = index_first_word; index <= index_last_word; index++){
    if(first_token == 1) first_token = 0; else fprintf(output_file, ",");
    fprintf(output_file, "\n");
    print_token(output_file, wb, index);
  }
  fprintf(output_file," ],\n");
}

void mcf_print_sentence(FILE *output_file, word_buffer *wb, int index_first_word, int index_last_word)
{
  int index;
  word *w = NULL;
  
  for(index = index_first_word; index <= index_last_word; index++){
    w = word_buffer_get_word_n(wb, index);
    fprintf(output_file, "%s\n", w->input);
  }
}


void print_sentence(FILE *output_file, int sentence_nb, word_buffer *wb, int index_first_word, int index_last_word)
{
  fprintf(output_file, "{\n");
  fprintf(output_file, "\"id\": \"s_%d\",\n", sentence_nb);
  print_tokens(output_file, wb, index_first_word, index_last_word);
  print_segments(output_file, wb, index_first_word, index_last_word);
  print_links(output_file, wb, index_first_word, index_last_word);
  fprintf(output_file, "}\n");
}

int main(int argc, char *argv[])
{
  FILE *json_output_file = NULL;
  FILE *mcf_output_file = NULL;
  context *ctx = mcf2json_context_read_options(argc, argv);
  word_buffer *wb = NULL;
  word *w = NULL;
  int first_sentence = 1;
  int new_sentence = 1;
  int index_first_word;
  int index_last_word;
  int sentence_nb = 0;
  char current_directory[1000];
  char current_file[1000];
  char previous_directory[1000];
  char previous_file[1000];
  char json_output_filename_for_header[1000];
  char *root_directory = NULL;
  char json_output_filename[1000];
  char mcf_output_filename[1000];
  char destination_dir[1000];
  struct stat st = {0};
  
  mcf2json_check_options(ctx);

  mcd_extract_dico_from_corpus(ctx->mcd_struct, ctx->mcf_filename);
  wb = word_buffer_load_mcf(ctx->mcf_filename, ctx->mcd_struct);

  if(ctx->root_dir){
    if(stat(ctx->root_dir, &st) == -1) {
      mkdir(ctx->root_dir, 0700);
      fprintf(stderr, "creating directory %s\n", ctx->root_dir);
    }
    do{
      w = word_buffer_b0(wb);
      if(w == NULL) break;
      word_sprint_col_n(current_directory, w, ctx->mcd_struct->wf2col[MCD_WF_DIRECTORY]);
      word_sprint_col_n(current_file, w, ctx->mcd_struct->wf2col[MCD_WF_FILE]);
            
      if(strcmp(current_directory, previous_directory)){
	strcpy(destination_dir, ctx->root_dir);
	strcat(destination_dir, "/");
	strcat(destination_dir, current_directory);
	if (stat(destination_dir, &st) == -1) {
	  mkdir(destination_dir, 0700);
	  fprintf(stderr, "creating directory %s\n", destination_dir);
	}
      }
      if(strcmp(current_file, previous_file)){
	strcpy(json_output_filename, destination_dir);
	strcat(json_output_filename, "/");
	strcat(json_output_filename, current_file);
	strcat(json_output_filename, ".json");
	fprintf(stderr, "creating file %s\n", json_output_filename);

	strcpy(mcf_output_filename, destination_dir);
	strcat(mcf_output_filename, "/");
	strcat(mcf_output_filename, current_file);
	strcat(mcf_output_filename, ".mcf");
	fprintf(stderr, "creating file %s\n", mcf_output_filename);

	
	if(json_output_file){
	  print_footer(json_output_file);
	  fclose(json_output_file);
	  fclose(mcf_output_file);
	}
	mcf_output_file = myfopen_no_exit(mcf_output_filename, "w");
	json_output_file = myfopen_no_exit(json_output_filename, "w");
	strcpy(json_output_filename_for_header, current_directory);
	strcat(json_output_filename_for_header, "/");
	strcat(json_output_filename_for_header, current_file);
	strcat(json_output_filename_for_header, ".json");
	print_header(json_output_file, ctx->mcd_struct, json_output_filename_for_header);
	first_sentence = 1;
      }
      if(new_sentence){
	new_sentence = 0;
	sentence_nb++;
	index_first_word = word_buffer_get_current_index(wb);
      }
      if(word_get_sent_seg(w)){
	index_last_word = word_buffer_get_current_index(wb);
	new_sentence = 1;
	
	if(first_sentence == 1)
	  first_sentence = 0;
	else
	  fprintf(json_output_file, ",");
	fprintf(json_output_file, "\n");
	print_sentence(json_output_file, sentence_nb, wb, index_first_word, index_last_word);
	mcf_print_sentence(mcf_output_file, wb, index_first_word, index_last_word);
      }
      strcpy(previous_file, current_file);
      strcpy(previous_directory, current_directory);
    } while(word_buffer_move_right(wb));
    print_footer(json_output_file);
    fclose(json_output_file);
    fclose(mcf_output_file);
  }

  else{ //ctx->root_dir is NULL dump everything to stdout
      json_output_file = stdout;
      print_header(json_output_file, ctx->mcd_struct, "");
      do{
	w = word_buffer_b0(wb);
	if(new_sentence){
	  new_sentence = 0;
	  sentence_nb++;
	  index_first_word = word_buffer_get_current_index(wb);
	}
	if(word_get_sent_seg(w)){
	  index_last_word = word_buffer_get_current_index(wb);
	  new_sentence = 1;
	  
	  if(first_sentence == 1)
	    first_sentence = 0;
	  else
	    fprintf(json_output_file, ",");
	  fprintf(json_output_file, "\n");
	  print_sentence(json_output_file, sentence_nb, wb, index_first_word, index_last_word);
	}
      } while(word_buffer_move_right(wb));
      print_footer(json_output_file);
  }
  
  mcf2json_context_free(ctx);
  return 0;
}
