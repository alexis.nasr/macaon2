#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<getopt.h>

#include"mcd.h"
#include"util.h"
#include"word_buffer.h"
#include"json_parser.h"

typedef struct {
  int help;
  int verbose;
  int debug_mode;
  char *program_name;
  char *conll_filename;
  char *mcf_filename;
  char *mcd_filename;
  mcd *mcd_struct;
  char *json_filename;
} context;

void json2mcf_context_free(context *ctx)
{
  if(ctx){
    if(ctx->program_name)
      free(ctx->program_name);
    if(ctx->conll_filename)
      free(ctx->conll_filename);
    if(ctx->mcf_filename)
      free(ctx->mcf_filename);
    if(ctx->mcd_filename)
      free(ctx->mcd_filename);
    if(ctx->mcd_struct)
      mcd_free(ctx->mcd_struct);
    free(ctx);
  }
}

context *json2mcf_context_new(void)
{
  context *ctx = (context *)memalloc(sizeof(context));

  ctx->help = 0;
  ctx->verbose = 0;
  ctx->debug_mode = 0;
  ctx->program_name = NULL;
  ctx->conll_filename = NULL;
  ctx->mcf_filename = NULL;
  ctx->mcd_filename = NULL;
  ctx->mcd_struct = NULL;
  ctx->json_filename = NULL;
  return ctx;
}

void json2mcf_context_general_help_message(context *ctx)
{
  fprintf(stderr, "usage: %s [options]\n", ctx->program_name);
  fprintf(stderr, "Options:\n");
  fprintf(stderr, "\t-h --help             : print this message\n");
  fprintf(stderr, "\t-v --verbose          : activate verbose mode\n");
  fprintf(stderr, "\t-C --mcd              : mcd filename\n");
  fprintf(stderr, "\t-i --mcf              : mcf filename (read from stdin if absent)\n");
  fprintf(stderr, "\t-j --json             : json filename\n");
}

void json2mcf_check_options(context *ctx){
  if(ctx->help){
    json2mcf_context_general_help_message(ctx);
    exit(1);
  }
}

context *json2mcf_context_read_options(int argc, char *argv[])
{
  int c;
  int option_index = 0;
  context *ctx = json2mcf_context_new();

  ctx->program_name = strdup(argv[0]);

  static struct option long_options[7] =
    {
      {"help",                no_argument,       0, 'h'},
      {"verbose",             no_argument,       0, 'v'},
      {"debug",               no_argument,       0, 'd'},
      {"conll",               required_argument, 0, 'o'},
      {"mcd",                 required_argument, 0, 'C'}, 
      {"mcf",                 required_argument, 0, 'i'},
      {"json",                 required_argument, 0, 'j'},
    };
  optind = 0;
  opterr = 0;
  
  while ((c = getopt_long (argc, argv, "hvdo:C:i:j:", long_options, &option_index)) != -1){ 
    switch (c)
      {
      case 'd':
	ctx->debug_mode = 1;
	break;
      case 'h':
	ctx->help = 1;
	break;
      case 'v':
	ctx->verbose = 1;
	break;
      case 'o':
	ctx->conll_filename = strdup(optarg);
	break;
      case 'i':
	ctx->mcf_filename = strdup(optarg);
	break;
      case 'C':
	ctx->mcd_filename = strdup(optarg);
	break;
      case 'j':
	ctx->json_filename = strdup(optarg);
	break;
      }
  }

  if(ctx->mcd_filename){
    ctx->mcd_struct = mcd_read(ctx->mcd_filename, ctx->verbose);
  }
  else{
    ctx->mcd_struct = mcd_build_wpmlgfs();
  }

  return ctx;
}

void json2mcf_print_word_buffer(FILE *f, word_buffer *wb)
{
  int i;
  word *w;
  int col_nb = 0;
  mcd *mcd_struct = word_buffer_get_mcd(wb);
  char *string;
  int wf;
  
  for(i=0; i < wb->nbelem; i++){
    w = word_buffer_get_word_n(wb, i);
    for(col_nb=0; col_nb < mcd_struct->nb_col;col_nb++){
      wf = mcd_struct->wf[col_nb];
      if(col_nb > 0) {fprintf(f, "\t");}
      if(mcd_struct->representation[col_nb] == MCD_REPRESENTATION_INT){
	fprintf(f, "%d", w->wf_array[wf]);
      }
      if(mcd_struct->representation[col_nb] == MCD_REPRESENTATION_VOCAB){
	string = mcd_get_str(mcd_struct, w->wf_array[wf], col_nb);
	//	string = dico_int2string(mcd_struct->dico_array[col_nb], w->wf_array[wf]);
	if(string)
	  fprintf(f, "%s", string);
	else
	  fprintf(f, "_");
      }
    }
    fprintf(f, "\n");
  }
}
      
/*    if((mcd_get_gov_col(mcd_struct) == -1)
       && (mcd_get_label_col(mcd_struct) == -1)
       && (mcd_get_sent_seg_col(mcd_struct) == -1)){
      printf("%s\t", word_get_input(w));
      printf("%d\t", word_get_gov(w));
      label = (word_get_label(w) == -1)? NULL : dico_int2string(dico_labels, word_get_label(w));
      if(label != NULL)
	printf("%s\t", label) ;
      else
	printf("_\t");
      if(word_get_sent_seg(w) == 1)
	printf("1\n") ;
      else
	printf("0\n");
    }
    else{
      buffer = strdup(w->input);
      token = strtok(buffer, "\t");
      col_nb = 0;
      while(token){
	if(col_nb != 0) printf("\t");
	if(col_nb == mcd_get_gov_col(mcd_struct)){
	  printf("%d", word_get_gov(w));
	}
	else
	  if(col_nb == mcd_get_label_col(mcd_struct)){
	    label = (word_get_label(w) == -1)? NULL : dico_int2string(dico_labels, word_get_label(w));
	    if(label != NULL)
	      printf("%s", label) ;
	    else
	      printf("_");
	  }
	  else
	    if(col_nb == mcd_get_sent_seg_col(mcd_struct)){
	      if(word_get_sent_seg(w) == 1)
		printf("1") ;
	      else
		printf("0");
	    }
	    else{
	      word_print_col_n(stdout, w, col_nb);
	    }
	col_nb++;
	token = strtok(NULL, "\t");
      }
      if((col_nb <= mcd_get_gov_col(mcd_struct)) || (mcd_get_gov_col(mcd_struct) == -1)){
	printf("\t%d", word_get_gov(w));
      }
      if((col_nb <= mcd_get_label_col(mcd_struct)) || (mcd_get_label_col(mcd_struct) == -1)){
	label = (word_get_label(w) == -1)? NULL : dico_int2string(dico_labels, word_get_label(w));
	if(label != NULL)
	  printf("\t%s", label) ;
	else
	  printf("\t_");
      }
      if((col_nb <= mcd_get_sent_seg_col(mcd_struct)) || (mcd_get_sent_seg_col(mcd_struct) == -1)){
	if(word_get_sent_seg(w) == 1)
	  printf("\t1") ;
	else
	  printf("\t0");
      }
      printf("\n");
      free(buffer);
    }
  }
}

*/

void update_segment(word_buffer *wb, int start, int end, char *label, char *status_seg, char *status_lab, int offset)
{
  int index;
  word *w;
  int label_code = -1;
  dico *d;
  mcd *mcd_struct = NULL;

  mcd_struct = word_buffer_get_mcd(wb); 
  d = mcd_struct->dico_array[mcd_get_pos_col(mcd_struct)];

  w = word_buffer_get_word_n(wb, offset + start);
  if(w == NULL){
    fprintf(stderr, "WARNING cannot access segment %d\n", offset + start);
    return;
  }  
  if(status_lab && !strcmp(status_lab, "G")){
    fprintf(stderr, "updating label of segment [%d-%d] with \"%s\"\n", start, end, label);

    /* -------------------------------------*/
    /* added by alexis 21/07/18 for datcha */
    //    word_set_F(w, 1);
    /* -------------------------------------*/
    if(d)
      label_code = dico_string2int(d, label);
    if(label_code == -1)
      fprintf(stderr, "label %s unknown\n", label);
    else
      word_set_pos(w, label_code);
  }
  

  /* -------------------------------------*/
  /* added by alexis 04/05/2021 for orfeo */
  if(status_seg){
    dico *d1 = mcd_struct->dico_array[mcd_get_word_span_status_col(mcd_struct)];
    if(d1){
      label_code = dico_string2int(d1, status_seg);
      if(label_code == -1)
	fprintf(stderr, "label %s unknown\n", label);
      else
	word_set_word_span_status(w, label_code);
      
    }
  }
  if(status_lab){
      dico *d1 = mcd_struct->dico_array[mcd_get_pos_label_status_col(mcd_struct)];
      if(d1){
	label_code = dico_string2int(d1, status_lab);
	if(label_code == -1)
	  fprintf(stderr, "label %s unknown\n", label);
	else
	word_set_pos_label_status(w, label_code);
	
      }
  }
  /* -------------------------------------*/
}

void process_segment(json_attr_val *avl, word_buffer *wb, int offset)
{
  int start, end;
  char *label, *status_seg, *status_lab;
  json_attr_val *av;

  for(av = avl; av != NULL; av = av->next){
    //      printf("attr = %s\n", av->attr);
    if(!strcmp(av->attr, "start")){start = (int)(av->val->u.number); continue;}
    if(!strcmp(av->attr, "end")){end = (int)(av->val->u.number); continue;}
    if(!strcmp(av->attr, "label")){label = av->val->u.string; continue;}
    if(!strcmp(av->attr, "status_seg")){status_seg = av->val->u.string; continue;}
    if(!strcmp(av->attr, "status_lab")){status_lab = av->val->u.string; continue;}
  }
  update_segment(wb, start, end, label, status_seg, status_lab, offset);
  //   printf("segment : start = %d end = %d label = %s status_seg = %s status_lab = %s\n", start, end, label, status_seg, status_lab);

}

void process_segments(json_struct *segments, word_buffer *wb, int offset)
{
  json_struct *segment;
  //  printf("process_segments\n");
  for(segment = segments->u.first; segment != NULL; segment = segment->next){
    process_segment(segment->u.attr_val_list, wb, offset);
  }
}

// {"orig": 1, "dest":2, "label": "suj", "status_link": "", "status_lab": "", "timestamp": "", "author": "", "target": ""},

void update_link(word_buffer *wb,  int orig, int dest, char *label, char *status_link, char *status_lab, int offset)
{

  int index;
  word *w = NULL;
  int label_code = -1;
  dico *d;
  mcd *mcd_struct = NULL;

  mcd_struct = word_buffer_get_mcd(wb); 
  d = mcd_struct->dico_array[mcd_get_label_col(mcd_struct)];

  w = word_buffer_get_word_n(wb, offset + orig);
  
  if(w == NULL){
    fprintf(stderr, "WARNING cannot access segment %d\n", offset + orig);
    return;
  }
  
  if(status_lab && !strcmp(status_lab, "G")){
    fprintf(stderr, "updating label of link %d -> %d with \"%s\"\n", orig, dest, label);
      if(d)
	label_code = dico_string2int(d, label);
      if(label_code == -1)
	fprintf(stderr, "WARNING : label %s unknown\n", label);
      //    else
      word_set_label(w, label_code);
  }
  
  if(status_link && !strcmp(status_link, "G")){
    fprintf(stderr, "updating governor of token %d with %d\n", orig, dest);
      if(dest == -1) /* -1 is for root */
	word_set_gov(w, 0);
      else
	word_set_gov(w, dest - orig);
  }
  /* -------------------------------------*/
  /* added by alexis 04/05/2021 for orfeo */
  if(status_link){
    dico *d1 = mcd_struct->dico_array[mcd_get_dep_span_status_col(mcd_struct)];
    if(d1){
      label_code = dico_string2int(d1, status_link);
      if(label_code == -1)
	fprintf(stderr, "label %s unknown\n", status_link);
      else
	word_set_dep_span_status(w, label_code);
      
    }
  }
  if(status_lab){
      dico *d1 = mcd_struct->dico_array[mcd_get_dep_label_status_col(mcd_struct)];
      if(d1){
	label_code = dico_string2int(d1, status_lab);
	if(label_code == -1)
	  fprintf(stderr, "label %s unknown\n", status_lab);
	else
	word_set_dep_label_status(w, label_code);
	
      }
  }
  /* -------------------------------------*/

}




void process_link(json_attr_val *avl, word_buffer *wb, int offset)
{
  int orig, dest;
  char *label = NULL;
  char *status_link = NULL;
  char *status_lab = NULL;
  char *status_label = NULL;
  json_attr_val *av;

  for(av = avl; av != NULL; av = av->next){
    //      printf("attr = %s\n", av->attr);
    if(!strcmp(av->attr, "orig")){orig = (int)(av->val->u.number); continue;}
    if(!strcmp(av->attr, "dest")){dest = (int)(av->val->u.number); continue;}
    if(!strcmp(av->attr, "label")){label = av->val->u.string; continue;}
    if(!strcmp(av->attr, "status_link")){status_link = av->val->u.string; continue;}
    if(!strcmp(av->attr, "status_label")){status_label = av->val->u.string; continue;}
    if(!strcmp(av->attr, "status_lab")){status_lab = av->val->u.string; continue;}
  }
  //  fprintf(stderr, "link : orig = %d dest = %d label = %s status_link = %s status_lab = %s\n", orig, dest, label, status_link, status_lab);

  if(status_label == NULL)
    update_link(wb, orig, dest, label, status_link, status_lab, offset);
  else
    update_link(wb, orig, dest, label, status_link, status_label, offset);

}

void process_links(json_struct *links, word_buffer *wb, int offset)
{
  json_struct *link;
  //  printf("process_links\n");
  for(link = links->u.first; link != NULL; link = link->next){
    process_link(link->u.attr_val_list, wb, offset);
  }

}

int get_id_of_first_token_in_document(json_struct *document)
{
  json_attr_val *avl = NULL;
  json_struct *tokens, *token;
  json_attr_val *avl2 = NULL;
  for(avl = document->u.attr_val_list; avl != NULL; avl = avl->next){
    if(!strcmp(avl->attr, (char *)"tokens")){
      tokens = avl->val;
      if(tokens){
	token = tokens->u.first;
	if(token){
	  for(avl2 = token->u.attr_val_list; avl2 != NULL; avl2 = avl2->next){
	    if(!strcmp(avl2->attr, (char *)"id"))
	      return (int)avl2->val->u.number;
	  }
	}
      }
    }
  }
  return -1;
}


int string_equal(char *s_json, char *s_mcf)
{
  if(!strcmp(s_json, "&quot") && !strcmp(s_mcf, "\"")) return 1;
  if(!strcmp(s_json, "&quot;&quot") && !strcmp(s_mcf, "\";\"")) return 1;
  if(!strcmp(s_json, s_mcf)) return 1;
  return 0;
}

//{"id":337,"word":"Bonjour","bold":0,"newline":0}

void check_token(json_attr_val *avl, word_buffer *wb, int offset)
{
  int id;
  char *form_json = NULL;
  char *form_mcf = NULL;
  char *status = NULL;
  json_attr_val *av;
  word *w = NULL;
  for(av = avl; av != NULL; av = av->next){
    //      printf("attr = %s\n", av->attr);
    if(!strcmp(av->attr, "id")){id = (int)(av->val->u.number); continue;}
    if(!strcmp(av->attr, "word")){form_json = av->val->u.string; continue;}
    if(!strcmp(av->attr, "status")){status = av->val->u.string; continue;}

  }
  /* ajouté le 24 juillet 2020 par Alexis */
  if(status != NULL && !strcmp(status, "inserted")){
    int form_column = wb->mcd_struct->wf2col[MCD_WF_FORM];
    fprintf(stderr, "inserting token at position %d\n", id);
    word *w = word_new(NULL);
    //    word_set_form(w, form_json);
    int code = dico_add(wb->mcd_struct->dico_array[form_column], form_json);
    w->wf_array[MCD_WF_FORM] = dico_add(wb->mcd_struct->dico_array[form_column], form_json);
    w->form = strdup(form_json);
    word_buffer_insert(wb, w, id);
  }

  /* ajouté le 24 juillet 2020 par Alexis */
  if(status != NULL && !strcmp(status, "deleted")){
    fprintf(stderr, "deleting token at position %d\n", id);
    word_buffer_rm(wb, id);
  }

  /* ajouté le 24 juillet 2020 par Alexis */
  if(status != NULL && !strcmp(status, "modified")){
    fprintf(stderr, "modifying token at position %d\n", id);
    int form_column = wb->mcd_struct->wf2col[MCD_WF_FORM];
    int code = dico_add(wb->mcd_struct->dico_array[form_column], form_json);
    word *w = word_buffer_get_word_n(wb, id);
    w->wf_array[MCD_WF_FORM] = dico_add(wb->mcd_struct->dico_array[form_column], form_json);
    w->form = strdup(form_json);
  }
  
  w = word_buffer_get_word_n(wb, id);
  if(w == NULL){
    fprintf(stderr, "attention, w vaut nul, id = %d form = %s", id, form_json);
    exit(1);
  }
  form_mcf = w->form;
  fprintf(stderr, "id : %d \t json : %s \t mcf : %s\n", id, form_json, form_mcf);
  if(!string_equal(form_json, form_mcf)){
    fprintf(stderr, "ERROR, tokens do not correspond in json and mcf files\n");
    exit(1);
  }
}

void check_tokens(json_struct *tokens, word_buffer *wb, int offset)
{
  json_struct *token;
  //  printf("process_tokens\n");
  for(token = tokens->u.first; token != NULL; token = token->next){
    check_token(token->u.attr_val_list, wb, offset);
  }
}



void process_document(json_struct *document, word_buffer *wb)
{
  json_attr_val *avl = NULL;
  int offset = get_id_of_first_token_in_document(document);
  fprintf(stderr, "process_document, offset = %d\n", offset);
  for(avl = document->u.attr_val_list; avl != NULL; avl = avl->next){
    //    if(!strcmp(avl->attr, (char *)"id")) printf("id = %s\n", avl->val->u.string);
    if(!strcmp(avl->attr, (char *)"tokens")) check_tokens(avl->val, wb, offset);
    if(!strcmp(avl->attr, (char *)"segments")) process_segments(avl->val, wb, offset);
    if(!strcmp(avl->attr, (char *)"links")) process_links(avl->val, wb, offset);
  }
}

void process_documents(json_struct *documents, word_buffer *wb)
{
  json_struct *document;
  //  printf("process_documents\n");
  for(document = documents->u.first; document != NULL; document = document->next){
    process_document(document, wb);
  }
}

int main(int argc, char *argv[])
{
  FILE *output_file;
  context *ctx = json2mcf_context_read_options(argc, argv);
  word_buffer *wb = NULL;
  word *w = NULL;
  int first_sentence = 1;
  int new_sentence = 1;
  int index_first_word;
  int index_last_word;
  int sentence_nb = 0;
  json_struct *root = NULL;
  json_struct *document = NULL;
  json_attr_val *avl = NULL;

  json2mcf_check_options(ctx);
  mcd_extract_dico_from_corpus(ctx->mcd_struct, ctx->mcf_filename);


  /*---------------------------------------------------*/  
  /* ajouté par Alexis le 04/05/2021 pour prendre en compte la notion de statut dans les fichiers mcf*/
  /* add (eventually) missing labels to status dictionnaries */
  
  dico *d;
  d = ctx->mcd_struct->dico_array[mcd_get_word_span_status_col(ctx->mcd_struct)];
  if(d){
    dico_add(d, "_");
    dico_add(d, "G");
    dico_add(d, "C");
  }

  d = ctx->mcd_struct->dico_array[mcd_get_pos_label_status_col(ctx->mcd_struct)];
  if(d){
    dico_add(d, "_");
    dico_add(d, "G");
    dico_add(d, "C");
  }

  d = ctx->mcd_struct->dico_array[mcd_get_dep_span_status_col(ctx->mcd_struct)];
  if(d){
    dico_add(d, "_");
    dico_add(d, "G");
    dico_add(d, "C");
  }

  d = ctx->mcd_struct->dico_array[mcd_get_dep_label_status_col(ctx->mcd_struct)];
  if(d){
    dico_add(d, "_");
    dico_add(d, "G");
    dico_add(d, "C");
  }

  /*---------------------------------------------------*/  
  
  wb = word_buffer_load_mcf(ctx->mcf_filename, ctx->mcd_struct);
  
  root = json_parse_full(ctx->json_filename, 0);
  
  if(root->type != JSON_AVL){
    fprintf(stderr, "erreur le json doit être un objet\n");
    exit(1);
  }
  for(avl = root->u.attr_val_list; avl != NULL; avl = avl->next){
    //    printf("section %s\n", avl->attr);
    if(!strcmp(avl->attr, (char *)"documents")){
      process_documents(avl->val, wb);
    }
  }
  
  //json_print_struct(stdout, root);
  json2mcf_print_word_buffer(stdout, wb);
  json_free_struct(root);
  json2mcf_context_free(ctx);
  return 0;
}
