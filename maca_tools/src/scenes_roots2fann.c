#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"dico.h"

typedef struct _image{
  char *name;
  int image_id;
  int f1, f2, f3, f4, f5, f6; 
} image;

typedef struct _scene{
  int images_nb;
  image **images_array;
} scene;


scene *new_scene(int images_nb)
{
  scene *sc =(scene *) malloc(sizeof(scene));
  sc->images_nb = images_nb;
  sc->images_array = (image **)malloc(images_nb * sizeof(image *));
  return sc;
}

image *new_image(char *name, int image_id, int f1 , int f2 , int f3 , int f4 , int f5 , int f6)
{
  image *im = (image *)malloc(sizeof(image));
  im->name = strdup(name);
  im->image_id = image_id;
  im->f1 = f1;
  im->f2 = f2;
  im->f3 = f3;
  im->f4 = f4;
  im->f5 = f5;
  im->f6 = f6;
  return im;
}

scene **load_scenes(char *filename, dico *d_images)
{
  char buffer[1000];
  char image_name[100];
  FILE *f = NULL;
  int line_nb = 1;
  int scenes_nb;
  int images_nb;
  int xx;
  int f1,f2,f3,f4,f5,f6;
  int scene_num, image_num;
  scene *sc;
  scene **scenes_array;
  int image_id;
  f = fopen(filename, "r");
  if(f == NULL){
    fprintf(stderr, "cannot open file %s\n", filename);
    exit(1);
  }

  fgets(buffer, 1000, f);
  buffer[strlen(buffer) - 1] = '\0';

  // line 1
  sscanf(buffer, "%d", &scenes_nb);
  scenes_array = (scene **)malloc(scenes_nb * sizeof(scene *));
  for(scene_num=0; scene_num < scenes_nb; scene_num++){
    fgets(buffer, 1000, f);
    buffer[strlen(buffer) - 1] = '\0';
    sscanf(buffer, "%d\t%d", &xx, &images_nb);
    scenes_array[scene_num] = sc = new_scene(images_nb);
    for(image_num = 0; image_num < images_nb; image_num++){
      fgets(buffer, 1000, f);
      buffer[strlen(buffer) - 1] = '\0';
      //s_3s.png	0	3	467	24	2	1
      sscanf(buffer, "%s\t%d\t%d\t%d\t%d\t%d\t%d", image_name, &f1, &f2, &f3, &f4, &f5, &f6);
      image_name[strlen(image_name) - 4]=0;
      image_id = dico_add(d_images, image_name);
      //      printf("%s\n", buffer);
      //printf("image name = %s\n", image_name);
      //printf("f1 = %d f2 = %d f3 = %d f4 = %d f5 = %d f6 = %d\n", f1, f2, f3, f4, f5, f6);
      sc->images_array[image_num] = new_image(image_name, image_id, f1, f2, f3, f4, f5, f6);
    }
  }
  fclose(f);
  return scenes_array;
}

void print_image(image *im)
{
  /*  printf("\t%s.1=%d", im->name, im->f1);
  printf("\t%s.2=%d", im->name, im->f2);
  printf("\t%s.3=%d", im->name, im->f3);
  printf("\t%s.4=%d", im->name, im->f4);
  printf("\t%s.5=%d", im->name, im->f5);
  printf("\t%s.6=%d", im->name, im->f6);*/
  printf("\t%d\t%d\t%d\t%d\t%d\t%d", im->f1, im->f2, (int)(im->f3 / 10), (int)(im->f4 / 10), im->f5, im->f6);
  
}

void print_scene(scene *sc, dico *d_images)
{
  int image_num;
  int i;
  for(i=0; i < d_images->nbelem; i++){
    for(image_num=0; image_num < sc->images_nb; image_num++){
      if(i == sc->images_array[image_num]->image_id){
	//	printf("i = %d\n", i);
		print_image(sc->images_array[image_num]);
	break;
      }
    }
    if(image_num == sc->images_nb){
            printf("\t-1\t-1\t-1\t-1\t-1\t-1");
    }
  }
  printf("\n");
}

void load_root_files(char *filename, scene **scenes_array, dico *d_images, dico *d_roots)
{
  char buffer[1000];
  char predicate[100];
  char scenes_nb[100];
  FILE *f = fopen(filename, "r");
  int i;
  int predicate_id;
  if(f == NULL){
    fprintf(stderr, "cannot open file %s\n", filename);
    exit(1);
  }

  while(fgets(buffer, 1000, f)){
    if(feof(f)) 
      break;
    buffer[strlen(buffer) - 1] = '\0';
    sscanf(buffer, "%s\t%s", scenes_nb, predicate);
    predicate_id = dico_add(d_roots, predicate);
    for(i=0; i < strlen(scenes_nb); i++)
      if(scenes_nb[i] == '_'){
	scenes_nb[i] = 0;
	break;
      }
    printf("%d", predicate_id);
    print_scene(scenes_array[atoi(scenes_nb)], d_images);
  }
  fclose(f);
}


int main(int argc, char *argv[])
{
  char *filename = argv[1];
  scene **scenes_array;
  dico *d_images = dico_new("images", 1000);
  dico *d_roots = dico_new("roots", 1000);
  scenes_array = load_scenes(filename, d_images);
  filename = argv[2];
  load_root_files(filename, scenes_array, d_images, d_roots);
  dico_print((char *)"roots.dico", d_roots);  

}
