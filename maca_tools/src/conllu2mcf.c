#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<strings.h>
#include<math.h>
#include<getopt.h>
#include"conll_lib.h" 
#include"hash_str.h" 

#define NB_COL 7

typedef struct options
{
  FILE * fd_parses;                    // parser output
  int verbose_level;
  int snum;
  char *filename;
  char columns[NB_COL];
} options;

/*---------------------------------------------------------------------------------*/

options op;

void print_options(options *op)
{
  fprintf(stderr, "file name = %s\n", op->filename);
  fprintf(stderr, "verbose level = %d\n", op->verbose_level);
  fprintf(stderr, "maximum number of sentences to process = %d\n", op->snum);
}

void reset_options(options * op)
{
  int i;
  op->filename = NULL;
  op->fd_parses = stdin;
  op->verbose_level = 0;
  op->snum = 100000000;
  for(i=0; i < NB_COL; i++)
    op->columns[i] = '0';
}

/*---------------------------------------------------------------------------------*/
void  print_help_message(char *program_name)
{
  fprintf(stderr, "%s usage: %s [options]\n", program_name, program_name);
  fprintf(stderr, "OPTIONS :\n");
  fprintf(stderr, "      -f <file>     : hypothesis conll file\n");
  fprintf(stderr, "      -n <int>      : process n sentences (default is 100 000 000)\n");
  fprintf(stderr, "      -v 1|2|3      : verbosity level\n");
  fprintf(stderr, "      -h            : print this message\n");

  fprintf(stderr, "      -1            : content of column 1 in the mcf file produced\n");
  fprintf(stderr, "      -2            : content of column 2 in the mcf file produced\n");
  fprintf(stderr, "      -3            : content of column 3 in the mcf file produced\n");
  fprintf(stderr, "      -4            : content of column 4 in the mcf file produced\n");
  fprintf(stderr, "      -5            : content of column 5 in the mcf file produced\n");
  fprintf(stderr, "      -6            : content of column 6 in the mcf file produced\n");
  fprintf(stderr, "      -7            : content of column 7 in the mcf file produced\n");
  fprintf(stderr, "                    : values of options -1 to -7 must be one of\n");
  fprintf(stderr, "                    : I for id\n");
  fprintf(stderr, "                    : W for form\n");
  fprintf(stderr, "                    : L for lemma\n");
  fprintf(stderr, "                    : C for coarse part of speech\n");
  fprintf(stderr, "                    : P for part of speech\n");
  fprintf(stderr, "                    : F for features\n");
  fprintf(stderr, "                    : H for head\n");
  fprintf(stderr, "                    : D for deprel\n");
  fprintf(stderr, "                    : G for language\n");

}




/*---------------------------------------------------------------------------------*/

void parse_options(int argc, char *argv[], options * op)
{
  char c;

  reset_options(op);
  /*
  if(argc ==1){
    print_help_message(argv[0]);
    exit(1);
    }*/
  
  while ((c = getopt (argc, argv, "hIWLCPFHDf:n:v:1:2:3:4:5:6:7:8:9:")) != -1)
    switch (c)
      {
      case 'h':
	print_help_message(argv[0]);
	exit(0);
      case 'f':
	op->filename = strdup(optarg);
	if((op->fd_parses = fopen(op->filename, "r")) == NULL){
	  fprintf(stderr, "cannot open hypothesis file %s : aborting\n", op->filename);
	  exit(1);
	}
	break;
      case '1':
	op->columns[0] = optarg[0];
	break;
      case '2':
	op->columns[1] = optarg[0];
	break;
      case '3':
	op->columns[2] = optarg[0];
	break;
      case '4':
	op->columns[3] = optarg[0];
	break;
      case '5':
	op->columns[4] = optarg[0];
	break;
      case '6':
	op->columns[5] = optarg[0];
	break;
      case '7':
	op->columns[6] = optarg[0];
	break;
      case 'n':
	op->snum = atoi(optarg);
	break;
      case 'v':
	op->verbose_level = atoi(optarg);
	break;
      }
  
  /*  if (op->fd_parses == NULL){
    fprintf(stderr, "error : cannot open parse file: aborting\n");
    exit(1);
    }*/
}

/*---------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------*/
int main(int argc, char *argv[])
{
  conll_sentence *s = conll_allocate_sentence();
  int snum = 0;
  int res;
  parse_options(argc, argv, &op);
  
  print_options(&op); 
  
  
  for(res = conll_load_sentence(op.fd_parses, s); res && (snum < op.snum); res = conll_load_sentence(op.fd_parses, s)){
    s->num = snum;
    snum++;
    conll_compute_relative_index_of_heads(s);
    conll_print_sentence_mcf3(s, op.columns, NB_COL);
  }
  if(op.filename)
    fclose(op.fd_parses);
  conll_free_sentence(s);
  return 0;
}
