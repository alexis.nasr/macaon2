#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<getopt.h>

#include"mcd.h"
#include"util.h"
#include"word_buffer.h"

typedef struct {
  int help;
  int verbose;
  int debug_mode;
  char *program_name;
  char *conll_filename;
  char *mcf_filename;
  char *mcd_filename;
  mcd *mcd_struct;
} context;

void context_free(context *ctx)
{
  if(ctx){
    if(ctx->program_name)
      free(ctx->program_name);
    if(ctx->conll_filename)
      free(ctx->conll_filename);
    if(ctx->mcf_filename)
      free(ctx->mcf_filename);
    if(ctx->mcd_filename)
      free(ctx->mcd_filename);
    if(ctx->mcd_struct)
      mcd_free(ctx->mcd_struct);
    free(ctx);
  }
}

context *context_new(void)
{
  context *ctx = (context *)memalloc(sizeof(context));

  ctx->help = 0;
  ctx->verbose = 0;
  ctx->debug_mode = 0;
  ctx->program_name = NULL;
  ctx->conll_filename = NULL;
  ctx->mcf_filename = NULL;
  ctx->mcd_filename = NULL;
  ctx->mcd_struct = NULL;
  return ctx;
}

void context_general_help_message(context *ctx)
{
  fprintf(stderr, "usage: %s [options]\n", ctx->program_name);
  fprintf(stderr, "Options:\n");
  fprintf(stderr, "\t-h --help             : print this message\n");
  fprintf(stderr, "\t-v --verbose          : activate verbose mode\n");
  fprintf(stderr, "\t-C --mcd              : mcd filename\n");
  fprintf(stderr, "\t-i --mcf              : mcf filename (read from stdin if absent)\n");
  fprintf(stderr, "\t-o --conll            : conll filename (write to stdout if absent)\n");
}

void mcf2conll_check_options(context *ctx){
  if(ctx->help){
    context_general_help_message(ctx);
    exit(1);
  }
}

context *context_read_options(int argc, char *argv[])
{
  int c;
  int option_index = 0;
  context *ctx = context_new();

  ctx->program_name = strdup(argv[0]);

  static struct option long_options[6] =
    {
      {"help",                no_argument,       0, 'h'},
      {"verbose",             no_argument,       0, 'v'},
      {"debug",               no_argument,       0, 'd'},
      {"conll",               required_argument, 0, 'o'},
      {"mcd",                 required_argument, 0, 'C'}, 
      {"mcf",                 required_argument, 0, 'i'},
    };
  optind = 0;
  opterr = 0;
  
  while ((c = getopt_long (argc, argv, "hvdo:C:i:", long_options, &option_index)) != -1){ 
    switch (c)
      {
      case 'd':
	ctx->debug_mode = 1;
	break;
      case 'h':
	ctx->help = 1;
	break;
      case 'v':
	ctx->verbose = 1;
	break;
      case 'o':
	ctx->conll_filename = strdup(optarg);
	break;
      case 'i':
	ctx->mcf_filename = strdup(optarg);
	break;
      case 'C':
	ctx->mcd_filename = strdup(optarg);
	break;
      }
  }

  if(ctx->mcd_filename){
    ctx->mcd_struct = mcd_read(ctx->mcd_filename, ctx->verbose);
  }
  else{
    ctx->mcd_struct = mcd_build_wpmlgfs();
  }

  return ctx;
}


word *get_rightmost_child(word_buffer *wb, word *gov)
{
  word *bm1 = word_buffer_bm1(wb);
  int rightmost_child_index = word_get_index(gov);
  for(int current_index = word_get_index(gov); current_index < wb->nbelem; current_index++)
    if(word_get_gov_index(word_buffer_get_word_n(wb, current_index)) == word_get_index(gov))
      rightmost_child_index = current_index;
  return word_buffer_get_word_n(wb, rightmost_child_index);
}


word *get_rightmost_descendent(word_buffer *wb, word *root)
{
  word *rightmost_descendent = root;
  word *rightmost_child = NULL;
  int change = 1;

  while(change){
    change = 0;
    rightmost_child = get_rightmost_child(wb, rightmost_descendent);
    if(word_get_index(rightmost_child) > word_get_index(rightmost_descendent)){
      rightmost_descendent = rightmost_child;
      change = 1;
    }
  }

  return rightmost_descendent;
}



int main(int argc, char *argv[])
{
  FILE *output_file;
  context *ctx = context_read_options(argc, argv);
  mcf2conll_check_options(ctx);


  word_buffer *wb = word_buffer_load_mcf(ctx->mcf_filename, ctx->mcd_struct);
  word *w = NULL;
  int form_col =  mcd_get_form_col(ctx->mcd_struct);
  int pos_col =   mcd_get_pos_col(ctx->mcd_struct);
  int cpos_col =  mcd_get_cpos_col(ctx->mcd_struct);
  int lemma_col = mcd_get_lemma_col(ctx->mcd_struct);
  int gov_col =   mcd_get_gov_col(ctx->mcd_struct);
  int label_col = mcd_get_label_col(ctx->mcd_struct);
  int feats_col = mcd_get_feats_col(ctx->mcd_struct);
  int sent_seg_col = mcd_get_sent_seg_col(ctx->mcd_struct);
  int index = 1;
  char label[100];
  word *rightmost_descendent = NULL;
  
  output_file = (ctx->conll_filename)? myfopen_no_exit(ctx->conll_filename, "w"): stdout;
  

  do{
    w = word_buffer_b0(wb);
    if(w){
      fprintf(output_file, "%d\t", index);
      
      if(form_col != -1)
	word_print_col_n(output_file, w, form_col);
      else
	fprintf(output_file, "_");
      fprintf(output_file, "\t");
      
      if(lemma_col != -1)
	word_print_col_n(output_file, w, lemma_col);
      else
	fprintf(output_file, "_");
      fprintf(output_file, "\t");
      
      if(cpos_col != -1)
	word_print_col_n(output_file, w, cpos_col);
      else
	fprintf(output_file, "_");
      fprintf(output_file, "\t");
      
      if(pos_col != -1)
	word_print_col_n(output_file, w, pos_col);
      else
	fprintf(output_file, "_");
      fprintf(output_file, "\t");
      
      if(feats_col != -1)
	word_print_col_n(output_file, w, feats_col);
      else
	fprintf(output_file, "_");
      fprintf(output_file, "\t");
      
      if(gov_col){
	if((word_get_gov(w) == 0) || ((word_get_gov(w) + index) < 0))
	  fprintf(output_file, "0\t");
	else
	  fprintf(output_file, "%d\t", word_get_gov(w) + index);
      }
      else
	fprintf(output_file, "_\t");
    
      if(label_col != -1){
	word_print_col_n(output_file, w, label_col);
	word_sprint_col_n(label, w, label_col);
	if(!strcmp(label, "root")){
	  
	  rightmost_descendent = get_rightmost_descendent(wb, w);
	  //	  printf("index rightmost desc = %d form = %s\n", rightmost_descendent->index, rightmost_descendent->form);
	  if(rightmost_descendent){
	    word_set_sent_seg(rightmost_descendent, 1);
	  }
	}
      }
      else
	fprintf(output_file, "_");
      fprintf(output_file, "\t");
      
      /* fprintf(output_file, "\t_\t\n"); */
      fprintf(output_file, "_\t_\n");
      if((sent_seg_col) && (word_get_sent_seg(w))){
	fprintf(output_file, "\n");
	index = 0;
      }
      
      index ++;
    }
  } while(word_buffer_move_right(wb));


  if (wb->input_file != stdin)
    fclose(wb->input_file);
    
  word_buffer_free(wb);

  if(ctx->conll_filename)
    fclose(output_file);
  context_free(ctx);
  return 0;
}
