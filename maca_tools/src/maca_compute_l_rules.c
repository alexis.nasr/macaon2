#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<getopt.h>

#include"l_rule.h"
#include"util.h"
#include"dico.h"

typedef struct {
  int help;
  int verbose;
  int debug_mode;
  int strict_mode;
  int threshold;
  char *program_name;
  char *fplm_filename;
  char *l_rules_filename;
  char *exceptions_filename;
} context;

void context_free(context *ctx)
{
  if(ctx){
    if(ctx->program_name)
      free(ctx->program_name);
    if(ctx->fplm_filename)
      free(ctx->fplm_filename);
    free(ctx);
  }
}

context *context_new(void)
{
  context *ctx = (context *)memalloc(sizeof(context));

  ctx->help = 0;
  ctx->verbose = 0;
  ctx->debug_mode = 0;
  ctx->threshold = 100;
  ctx->strict_mode = 0;
  ctx->program_name = NULL;
  ctx->fplm_filename = NULL;
  ctx->l_rules_filename = NULL;
  ctx->exceptions_filename = NULL;
  return ctx;
}

void help_message(context *ctx)
{
  fprintf(stderr, "usage: %s [options]\n", ctx->program_name);
  fprintf(stderr, "Options:\n");
  fprintf(stderr, "\t-h --help                   : print this message\n");
  fprintf(stderr, "\t-v --verbose                : activate verbose mode\n");
  fprintf(stderr, "\t-f --fplm       <filename>  : fplm filename\n");
  fprintf(stderr, "\t-s --strict                 : generate strict l_rules\n");
  fprintf(stderr, "\t-t --threshold  <int>       : threshold\n");
  fprintf(stderr, "\t-r --l_rules    <filename>  : file to stock l_rules\n");
  fprintf(stderr, "\t-e --exceptions <filename>  : exceptions filename (fplm format)\n");
}

void check_options(context *ctx){
  if((ctx->help)
     || !ctx->fplm_filename
     || !ctx->l_rules_filename
     || !ctx->exceptions_filename
     )
    {
    help_message(ctx);
    exit(1);
  }
}

context *context_read_options(int argc, char *argv[])
{
  int c;
  int option_index = 0;
  context *ctx = context_new();

  ctx->program_name = strdup(argv[0]);

  static struct option long_options[8] =
    {
      {"help",                no_argument,       0, 'h'},
      {"verbose",             no_argument,       0, 'v'},
      {"debug",               no_argument,       0, 'd'},
      {"strict",              no_argument,       0, 's'},
      {"fplm",                required_argument, 0, 'f'},
      {"threshold",           required_argument, 0, 't'},
      {"l_rules",             required_argument, 0, 'r'},
      {"exceptions",          required_argument, 0, 'e'},
    };
  optind = 0;
  opterr = 0;
  
  while ((c = getopt_long (argc, argv, "hvdsf:t:r:e:", long_options, &option_index)) != -1){ 
    switch (c)
      {
      case 'h':
	ctx->help = 1;
	break;
      case 'v':
	ctx->verbose = 1;
	break;
      case 'd':
	ctx->debug_mode = 1;
	break;
      case 's':
	ctx->strict_mode = 1;
	break;
      case 'f':
	ctx->fplm_filename = strdup(optarg);
	break;
      case 'r':
	ctx->l_rules_filename = strdup(optarg);
	break;
      case 'e':
	ctx->exceptions_filename = strdup(optarg);
	break;
      case 't':
	ctx->threshold = atoi(optarg);
	break;
      }
  }
  return ctx;
}

   

int main(int argc, char *argv[])
{
  context *ctx = context_read_options(argc, argv);
  int i;
  cell *c;
  char form[100];
  char pos[100];
  char lemma[100];
  char morpho[100];
  FILE *F_fplm = NULL;
  FILE *F_exceptions = NULL;
  char *l_rule;
  char buffer[1000];
  dico *d_rules = dico_new((char *)"d_rules", 100);
  hash *h_rules = hash_new(10000);
  check_options(ctx);

  F_fplm = myfopen(ctx->fplm_filename, "r");
  
  while(fgets(buffer, 1000, F_fplm)){
    if(feof(F_fplm)) 
      break;
    /* printf("%s", buffer); */
    buffer[strlen(buffer) - 1] = '\0';
    sscanf(buffer, "%[^\t]\t%[^\t]\t%[^\t]\t%[^\n]\n", form, pos, lemma, morpho);

    l_rule = compute_l_rule(lemma, form, ctx->strict_mode);
    //    char *new_lemma = apply_l_rule(form, l_rule);

    //    printf("%s\t%s\t%s=%s\t%s\t%s\n", form, pos, lemma, new_lemma, morpho, l_rule);
    //    printf("%s\t%s\t%s\t%s\t%s\n", form, pos, lemma, morpho, l_rule);
    hash_inc_val(h_rules, l_rule, 1);

    //free(new_lemma);
    free(l_rule);
  }
  fclose(F_fplm);

  for(i=0; i < h_rules->size; i++){
    for(c = h_rules->array[i]; c; c = c->next)
      if(c->val >= ctx->threshold){
	dico_add(d_rules, c->key);
      }
	
  }

  F_fplm = myfopen(ctx->fplm_filename, "r");
  F_exceptions = myfopen(ctx->exceptions_filename, "w");
  while(fgets(buffer, 1000, F_fplm)){
    if(feof(F_fplm)) 
      break;
    /* printf("%s", buffer); */
    buffer[strlen(buffer) - 1] = '\0';
    sscanf(buffer, "%[^\t]\t%[^\t]\t%[^\t]\t%[^\n]\n", form, pos, lemma, morpho);
    
    l_rule = compute_l_rule(lemma, form, ctx->strict_mode);
    //    if((dico_string2int(d_rules, l_rule) == -1) && (strcmp(form, lemma)))
    if((dico_string2int(d_rules, l_rule) == -1))
            fprintf(F_exceptions, "%s\t%s\t%s\t%s\t%s\n", form, pos, lemma, morpho, l_rule);
      //      fprintf(F_exceptions, "%s\t%s\t%s\t%s\n", form, pos, lemma, morpho);
    free(l_rule);
  } 
  fclose(F_fplm);
  fclose(F_exceptions);
  
  dico_print(ctx->l_rules_filename, d_rules);
  
}
