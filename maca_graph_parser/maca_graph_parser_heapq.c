/*******************************************************************************
    Copyright (C) 2013 by Alexis Nasr <alexis.nasr@lif.univ-mrs.fr>
                          Mathieu Morey <mathieu.morey@lif.univ-mrs.fr>
                          Frederic Bechet <frederic.bechet@lif.univ-mrs.fr>
    This file is part of maca_graph_parser.

    maca_graph_parser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    maca_graph_parser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with maca_graph_parser. If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#include "maca_graph_parser_heapq.h"


void heap_swap(heap *h, const int a, const int b){
  // swap values
  float tmp_value = h->values[a];
  h->values[a] = h->values[b];
  h->values[b] = tmp_value;
  // swap objects
  void *tmp_object = h->objects[a];
  h->objects[a] = h->objects[b];
  h->objects[b] = tmp_object;
}

/**
 * n-max
 */
/* nmax: private functions */
void heap_heapify_min(heap *h, int index){
  const int left = index << 1;
  const int right = left + 1;
  int largest = index;
  if(left < h->num && h->values[left] < h->values[index]) largest = left;
  if(right < h->num && h->values[right] < h->values[largest]) largest = right;
  if(largest != index){
    heap_swap(h, index, largest);
    heap_heapify_min(h, largest);
  }
}

void heap_build_min(heap *h){
  int index = (h->num - 1) >> 1;
  while(index > 0){
    heap_heapify_min(h, index);
    index >>= 1;
  }
  heap_heapify_min(h, index);
}

void heap_extract_min(heap *h){
  h->values[0] = h->values[h->num - 1];
  h->objects[0] = h->objects[h->num -1];
  h->num--;
  heap_heapify_min(h, 0);
}

/* nmax: public functions */
void heap_sort_nmax(heap *h){
  const int saved_num = h->num;
  int index = h->num - 1;
  while(index > 0){
    heap_swap(h, 0, index);
    h->num--;
    heap_heapify_min(h, 0);
    index--;
  }
  h->num = saved_num;
}

void heap_insert_nmax(heap *h, float value, void *object){
  if(h->num >= h->max_size){
    if(value < h->values[0]) return;
    heap_extract_min(h);
  }
  int index = h->num;
  h->values[index] = value;
  h->objects[index] = object;
  h->num += 1;
  int parent = index >> 1;
  while(index > 0 && h->values[parent] > h->values[index]){
    heap_swap(h, index, parent);
    index = parent;
    parent >>= 1;
  }
}

/**
 * n-min
 */
/* nmin: private functions */
void heap_heapify_max(heap *h, int index){
  const int left = index << 1;
  const int right = left + 1;
  int largest = index;
  if(left < h->num && h->values[left] > h->values[index]) largest = left;
  if(right < h->num && h->values[right] > h->values[largest]) largest = right;
  if(largest != index) {
    heap_swap(h, index, largest);
    heap_heapify_max(h, largest);
  }
}

void heap_build_max(heap *h){
  int index = (h->num - 1) >> 1;
  while(index > 0){
    heap_heapify_max(h, index);
    index >>= 1;
  }
  heap_heapify_max(h, index);
}

void heap_extract_max(heap *h){
  h->values[0] = h->values[h->num - 1];
  h->objects[0] = h->objects[h->num - 1];
  h->num--;
  heap_heapify_max(h, 0);
}

/* nmin: public functions */
void heap_sort_nmin(heap *h){
  const int saved_num = h->num;
  int index = h->num - 1;
  while(index > 0){
    heap_swap(h, 0, index);
    h->num--;
    heap_heapify_max(h, 0);
    index--;
  }
  h->num = saved_num;
}

void heap_insert_nmin(heap *h, float value, void *object){
  if(h->num >= h->max_size){
    if(value > h->values[0]) return;
    heap_extract_max(h);
  }
  int index = h->num;
  h->values[index] = value;
  h->objects[index] = object;
  h->num += 1;
  int parent = index >> 1;
  while(index > 0 && h->values[parent] < h->values[index]){
    heap_swap(h, index, parent);
    index = parent;
    parent >>= 1;
  }
}

/**
 * Common functions to nmin and nmax.
 */
heap *heap_create(const int max_size, const float dft_value){
  int i;
  heap *h = malloc(sizeof(heap));
  h->num = 0;
  h->max_size = max_size;
  h->dft_value = dft_value;
  h->values = malloc(max_size * sizeof(float));
  h->objects = malloc(max_size * sizeof(void *));
  /* init */
  for (i=0; i<max_size; i++){
    h->values[i] = dft_value;
    h->objects[i] = NULL;
  }
  return h;
}

void heap_destroy(heap* h){
  int i;
  /* reset */
  for (i=0; i<h->max_size; i++){
    h->values[i] = 0;
    h->objects[i] = NULL;
  }
  h->num = 0;
  h->max_size = 0;
  h->dft_value = 0;
  free(h->values);
  free(h->objects);
  free(h);
}

void *heap_get(heap *h, const int index){
  return h->objects[index];
}

float heap_get_value(heap *h, const int index){
  return h->values[index];
}

int heap_size(heap *h){
  return h->num;
}

void heap_clear(heap *h){
  h->num = 0;
}
