#include "maca_graph_parser_feature_counter_array.h"

#ifdef __cplusplus
extern "C"{
#endif



/* basics */
maca_graph_parser_model *maca_graph_parser_model_allocate(maca_graph_parser_ctx *ctx);
void maca_graph_parser_model_init(maca_graph_parser_ctx *ctx, maca_graph_parser_model *model);
void maca_graph_parser_model_free(maca_graph_parser_model *model);
/* persistence */
maca_graph_parser_model *maca_graph_parser_model_load(maca_graph_parser_ctx *ctx, char *model_file_name);
void maca_graph_parser_model_dump(maca_graph_parser_ctx *ctx, maca_graph_parser_model *model, char *file_name, int produce_hash_model);
/* scoring */
float score_feat_vector(feat_vector *fv, maca_graph_parser_model *model);
float score_feature_counter_array(feature_counter_array *a, maca_graph_parser_model *model);
#ifdef __cplusplus
}
#endif
