#include "maca_mcf.hh"

// int main(int argc, char **argv) {
//   // std::vector<std::shared_ptr<macaon::McfValue>> array;
  
//   // std::shared_ptr<macaon::McfValueInt> iValue(new macaon::McfValueInt(42));
//   // //std::shared_ptr<macaon::McfValue> value = iValue;
//   // array.push_back(std::shared_ptr<macaon::McfValue>(iValue));
  
//   // std::cout << "array[0]: " << array[0].get()->toString() << std::endl;
  

//   // std::list<std::unique_ptr<macaon::McfValue>> list;
//   // std::unique_ptr<macaon::McfValue> value(new macaon::McfValueInt(42));
//   // list.push_back(std::move(value));
  
//   // std::cout << list.front().get()->toString() << std::endl;

//   // macaon::McfSentence sentence(3);
  
//   // macaon::McfWord &word = sentence.addWord();
//   // word.addValueString(0,"BAR");
//   // word.addValueString(0,"FOO");
//   // word.addValueFloat(2,42.4);

//   // float f = word.getFirstFloatValue(2);
  
//   // std::cout << "f: " << f << "\n";

//   // word.print();
//   // std::cout << "\n";

//   if (argc - 1 != 2) {
//     std::cerr << "Usage: " << argv[0] << " ftb alpha\n";
//     return 1;
//   }
//   std::string ftb(argv[1]);
//   std::string alpha(argv[2]);
//   // std::ifstream input(ftb);
//   macaon::Mcf mcf(ftb, alpha);

//   mcf.readHeader();
//   int formId = mcf.input("FORM");
//   int lemmaId = mcf.input("LEMMA");
//   int posId = mcf.input("POSTAG");
//   int deprelId = mcf.output("OUT_DEPREL", "LABELS");
  

//   std::cout << "Form id: " << formId << ", lemma id: " << lemmaId << "\n";
//   std::cout << "out_deprel id: " << deprelId << "\n";

//   std::cout << "form's alpha: " << mcf.getColumnInfo(formId).alphabet->getName() << "\n";
//   std::cout << "lemma's alpha: " << mcf.getColumnInfo(lemmaId).alphabet->getName() << "\n";
//   std::cout << "pos's alpha: " << mcf.getColumnInfo(posId).alphabet->getName() << "\n";
//   std::cout << "deprel's alpha: " << mcf.getColumnInfo(deprelId).alphabet->getName() << "\n";

//   // mcf.getColumnInfo(posId).alphabet->lock();

//   mcf.printHeader(stdout);

//   //std::shared_ptr<macaon::McfSentences> sents = mcf.readSentences();

//   //sents->print();

//   //input.close();

//   return 0;
// }

bool to_bool(std::string str) {
  if (str == "0" || str == "False" || str == "false")
    return false;
  if (str == "1" || str == "True" || str == "true")
    return true;
  
  throw std::invalid_argument("Can't convert given string to bool");
}

namespace macaon {

  McfWord::McfWord(const McfWord &other): id(other.id), word(other.word.size()) {
    for (std::size_t i = 0; i < other.word.size(); i++) {
      const std::vector<std::unique_ptr<McfValue>> &vec = other.word[i];
      for (const std::unique_ptr<McfValue> &ptrOther : vec) {
  	std::unique_ptr<McfValue> ptr(ptrOther.get()->clone());
	word[i].push_back(std::move(ptr));
      }
    }
  }

  McfWord &McfWord::operator =(const McfWord &other) {
    id = other.id;
    std::vector<std::vector<std::unique_ptr<McfValue>>>(other.word.size()).swap(word);
    for (std::size_t i = 0; i < other.word.size(); i++) {
      const std::vector<std::unique_ptr<McfValue>> &vec = other.word[i];
      for (const std::unique_ptr<McfValue> &ptrOther : vec) {
  	std::unique_ptr<McfValue> ptr(ptrOther.get()->clone());
	word[i].push_back(std::move(ptr));
      }
    }
    return *this;
  }

  int McfWord::getId() {
    return id;
  }

  void McfWord::addValueInt(int column, int value) {
    std::unique_ptr<McfValue> ptr(new McfValueInt(value));
    word.at(column).push_back(std::move(ptr));
  }

  void McfWord::addValueFloat(int column, float value) {
    std::unique_ptr<McfValue> ptr(new McfValueFloat(value));
    word.at(column).push_back(std::move(ptr));
  }
    
  void McfWord::addValueString(int column, std::string value) {
    std::unique_ptr<McfValue> ptr(new McfValueString(value));
    word.at(column).push_back(std::move(ptr));
  }

  void McfWord::addValueBool(int column, bool value) {
    std::unique_ptr<McfValue> ptr(new McfValueBool(value));
    word.at(column).push_back(std::move(ptr));
  }

  void McfWord::addValueAlphabet(int column, std::string symbol, std::shared_ptr<Alphabet> a) {
    std::unique_ptr<McfValue> ptr(new McfValueAlphabet(symbol, a));
    word.at(column).push_back(std::move(ptr));
  }

  std::list<int> McfWord::getIntValues(int column) {
    std::vector<std::unique_ptr<McfValue>> &mcfValues = word.at(column);
    std::list<int> values;

    for (std::unique_ptr<McfValue> &mcfVal : mcfValues) {
      McfValueInt *ptrVal = dynamic_cast<McfValueInt*>(mcfVal.get());
      values.push_back(ptrVal->i);
    }

    return values;
  }

  std::list<float> McfWord::getFloatValues(int column) {
    std::vector<std::unique_ptr<McfValue>> &mcfValues = word.at(column);
    std::list<float> values;

    for (std::unique_ptr<McfValue> &mcfVal : mcfValues) {
      McfValueFloat *ptrVal = dynamic_cast<McfValueFloat*>(mcfVal.get());
      values.push_back(ptrVal->f);
    }

    return values;
  }

  std::list<std::string> McfWord::getStringValues(int column) {
    std::vector<std::unique_ptr<McfValue>> &mcfValues = word.at(column);
    std::list<std::string> values;

    for (std::unique_ptr<McfValue> &mcfVal : mcfValues) {
      McfValueString *ptrVal = dynamic_cast<McfValueString*>(mcfVal.get());
      values.push_back(ptrVal->s);
    }

    return values;
  }

  std::list<bool> McfWord::getBoolValues(int column) {
    std::vector<std::unique_ptr<McfValue>> &mcfValues = word.at(column);
    std::list<bool> values;

    for (std::unique_ptr<McfValue> &mcfVal : mcfValues) {
      McfValueBool *ptrVal = dynamic_cast<McfValueBool*>(mcfVal.get());
      values.push_back(ptrVal->b);
    }

    return values;
  }

  int McfWord::getNbValues(int column) {
    return word.at(column).size();
  }

  int &McfWord::getIntValue(int column, int index) {
    std::unique_ptr<McfValue> &mcfValue = word.at(column).at(index);
    return dynamic_cast<McfValueInt*>(mcfValue.get())->i;
  }

  float &McfWord::getFloatValue(int column, int index) {
    std::unique_ptr<McfValue> &mcfValue = word.at(column).at(index);
    return dynamic_cast<McfValueFloat*>(mcfValue.get())->f;
  }

  std::string &McfWord::getStringValue(int column, int index) {
    std::unique_ptr<McfValue> &mcfValue = word.at(column).at(index);
    return dynamic_cast<McfValueString*>(mcfValue.get())->s;
  }

  bool &McfWord::getBoolValue(int column, int index) {
    std::unique_ptr<McfValue> &mcfValue = word.at(column).at(index);
    return dynamic_cast<McfValueBool*>(mcfValue.get())->b;
  }

  void McfWord::clearValues(int column) {
    word.at(column).clear();
  }

  void McfWord::print(std::ostream &output) {
    bool firstColumn = true;
    for (std::vector<std::unique_ptr<McfValue>> &v : word) {
      if (!firstColumn)
  	output << "\t";
      if (v.empty()) {
  	output << "_";
      } else {
  	bool firstInList = true;
  	for (std::unique_ptr<McfValue> &ptr : v) {
  	  if (!firstInList)
  	    output << "|";
  	  output << ptr.get()->toString();
  	  firstInList = false;
  	}
      }
      firstColumn = false;
    }
    output << "\n";
  }

  void McfWord::print(FILE *output) {
    bool firstColumn = true;
    for (std::vector<std::unique_ptr<McfValue>> &v : word) {
      if (!firstColumn)
  	fprintf(output,"\t");
      if (v.empty()) {
  	fprintf(output,"_");
      } else {
  	bool firstInList = true;
  	for (std::unique_ptr<McfValue> &ptr : v) {
  	  if (!firstInList)
  	    fprintf(output,"|");
  	  fprintf(output,"%s",ptr.get()->toString().c_str());
  	  firstInList = false;
  	}
      }
      firstColumn = false;
    }
    fprintf(output,"\n");
  }

  McfSentence::McfSentence(int nbColumns): nbColumns(nbColumns), length(0) {
    addWord();
  }

  McfWord &McfSentence::addWord() {
    sentence.push_back(McfWord(length, nbColumns));
    length++;
    return sentence.back();
  }

  McfWord &McfSentence::getWord(int i) {
    return sentence.at(i);
  }

  void McfSentence::clear() {
    sentence.clear();
    addWord();
    length = 1;
  }

  void McfSentence::print(std::ostream &output) {
    for (std::size_t i = 1; i < sentence.size(); i++) {
      sentence[i].print(output);
    }
    output << "\n";
  }

  void McfSentence::print(FILE *output) {
    for (std::size_t i = 1; i < sentence.size(); i++) {
      sentence[i].print(output);
    }
    fprintf(output,"\n");
  }

  McfSentences::McfSentences(): nbSentences(0) {}

  void McfSentences::addSentence(std::shared_ptr<McfSentence> s) {
    sentences.push_back(s);
  }

  std::shared_ptr<McfSentence> McfSentences::getSentence(int index) {
    return sentences.at(index);
  }

  std::shared_ptr<McfSentence> McfSentences::operator[](int index) {
    return sentences[index];
  }

  int McfSentences::size() {
    return nbSentences;
  }

  void McfSentences::print(std::ostream &output) {
    for (std::shared_ptr<McfSentence> &s : sentences) {
      s->print(output);
    }
  }

  void McfSentences::print(FILE *output) {
    for (std::shared_ptr<McfSentence> &s : sentences) {
      s->print(output);
    }
  }

  McfColumn::McfColumn(std::string name, std::string typeName, int columnIdInFile):
    name(name), typeName(typeName), alphabet(nullptr), columnIdInFile(columnIdInFile) {}

  void McfColumn::setAlphabet(std::shared_ptr<Alphabet> a) {
    alphabet = a;
  }

  Mcf::Mcf(std::istream &sinput): sinput(sinput), internalFile(false), parsedHeader(false) {
    readHeader();
  }

  Mcf::Mcf(std::istream &sinput, std::string alphabetFilename): 
    sinput(sinput), internalFile(false), parsedHeader(false), alphaArray(alphabetFilename) {
    readHeader();
  }

  Mcf::Mcf(std::istream &sinput, AlphabetArray &array):
    sinput(sinput), internalFile(false), parsedHeader(false), alphaArray(array) {
    readHeader();
  }

  Mcf::Mcf(std::string filename): 
    sinput(*(new std::ifstream(filename))), internalFile(true), parsedHeader(false) {
    readHeader();
  }

  Mcf::Mcf(std::string filename, std::string alphabetFilename): 
    sinput(*(new std::ifstream(filename))), internalFile(true), parsedHeader(false), alphaArray(alphabetFilename) {
    readHeader();
  }

  Mcf::Mcf(std::string filename, AlphabetArray &array):
    sinput(*(new std::ifstream(filename))), internalFile(true), parsedHeader(false), alphaArray(array) {
    readHeader();
  }

  Mcf::~Mcf() {
    if(internalFile)
      delete &sinput;
  }

  void Mcf::readHeader() {
    if (parsedHeader) {
      throw std::runtime_error("Already read header once!");
    }
    
    std::string line;
    char firstChar = sinput.get();
    if (firstChar == '#') {
      if (!std::getline(sinput, line)) {
	throw std::runtime_error("Couldn't read header (EOF) !");
      }
    } else { /* conll07 compatibility */
      sinput.unget();
      line = "ID@INT\tFORM@WORDS\tLEMMA@WORDS\tCPOSTAG@STRING\tPOSTAG@POS\tFEATS@MORPHO\t"
	     "HEAD@INT\tDEPREL@LABELS\tPHEAD@STRING\tPDEPREL@STRING";
    }

    std::istringstream iss(line);
    std::string token;
    int column = 0;
    while (std::getline(iss, token, '\t')) { 
      std::string name;
      std::string typeName;
      std::size_t pos = token.find('@');
      if (pos == std::string::npos) {
	name = token;
	typeName = MCF_COLUMN_TYPE_STRING;
      } else {
	name = token.substr(0, pos);
	typeName = token.substr(pos+1);
      }
      
      columns.push_back(McfColumn(name, typeName, column));
      col2Index[name] = column;
      column++;
    }

    nbInputColumns = column;
    parsedHeader = true;
  }

  int Mcf::input(std::string columnName) {
    std::map<std::string,int>::iterator it = col2Index.find(columnName);
    if (it == col2Index.end()) {
      throw std::runtime_error("Can't register a non-existant column!");
    }
    McfColumn &column = columns[it->second];
    std::string &columnType = column.typeName;
    if (columnType != MCF_COLUMN_TYPE_STRING &&
	columnType != MCF_COLUMN_TYPE_INTEGER && 
	columnType != MCF_COLUMN_TYPE_FLOAT &&
	columnType != MCF_COLUMN_TYPE_BOOL) { // We have an alphabet then
      if (alphaArray.has(columnType)) {
	column.setAlphabet(alphaArray[columnType]);
      } else {
	std::shared_ptr<Alphabet> a(new Alphabet(columnType));
	alphaArray.addAlphabet(a);
	column.setAlphabet(a);
      }
    }
    
    registeredColumns[columnName] = it->second;

    return it->second;
  }

  int Mcf::output(std::string columnName, std::string columnType) {
    int columnId = columns.size();
    
    std::map<std::string,int>::iterator it = col2Index.find(columnName);
    if (it != col2Index.end())
      throw std::runtime_error("Column has already been registered!");
    
    columns.push_back(McfColumn(columnName, columnType, -1));
    col2Index[columnName] = columnId;
    registeredColumns[columnName] = columnId;
    
    if (columnType != MCF_COLUMN_TYPE_STRING &&
	columnType != MCF_COLUMN_TYPE_INTEGER && 
	columnType != MCF_COLUMN_TYPE_FLOAT &&
	columnType != MCF_COLUMN_TYPE_BOOL) { // We have an alphabet then
      if (alphaArray.has(columnType)) {
	columns[columnId].setAlphabet(alphaArray[columnType]);
      } else {
	std::shared_ptr<Alphabet> a(new Alphabet(columnType));
	alphaArray.addAlphabet(a);
	columns[columnId].setAlphabet(a);
      }
    }
    
    return columnId;
  }

  McfColumn &Mcf::getColumnInfo(int id) {
    return columns.at(id);
  }

  McfColumn &Mcf::getColumnInfo(std::string name) {
    std::map<std::string,int>::iterator it = col2Index.find(name);
    if (it == col2Index.end())
      throw std::invalid_argument("Column does not exist!");
    return columns[it->second];
  }

  bool Mcf::readLine(std::string &line, McfSentence &sentence, bool sentenceValid) {
    std::istringstream iss(line);
    std::string token;
    int columnId = 0;
    McfWord &word = sentence.addWord();
    while (getline(iss, token, '\t')) {
      if (columnId >= nbInputColumns) {
	sentenceValid = false;
	std::cerr << "Warning: Line has too many columns!\n";
	break;
      }
      
      if (token != "_") {
	std::istringstream subiss(token);
	std::string subtoken;
	McfColumn &column = columns[columnId];
	while (getline(subiss, subtoken, '|')) {
	  if (column.typeName == MCF_COLUMN_TYPE_STRING) {
	    word.addValueString(columnId, subtoken);
	  } else if (column.typeName == MCF_COLUMN_TYPE_INTEGER) {
	    word.addValueInt(columnId, std::stoi(subtoken));
	  } else if (column.typeName == MCF_COLUMN_TYPE_FLOAT) {
	    word.addValueFloat(columnId, std::stof(subtoken));
	  } else if (column.typeName == MCF_COLUMN_TYPE_BOOL) {
	    word.addValueBool(columnId, to_bool(subtoken));
	  } else {
	    if (column.alphabet) {
	      try {
		word.addValueAlphabet(columnId, subtoken, column.alphabet);
	      } catch (std::exception &e) {
		sentenceValid = false;
		std::cerr << "Warning: Couldn't add '" << subtoken 
			  << "' to alphabet (maybe locked?)!\n";
	      }
	    } else {
	      word.addValueString(columnId, subtoken);
	    }
	  }
	}
      }
      columnId++;
    }
    if (columnId != nbInputColumns) {
      sentenceValid = false;
      std::cerr << "Warning: Line has too few columns!\n";
    }

    return sentenceValid;
  }

  std::shared_ptr<McfSentence> Mcf::readNextSentence() {
    bool sentenceValid = true;
    std::string line;
    
    if (sinput.eof()) {
      return nullptr;
    }

    std::shared_ptr<McfSentence> sentence(new McfSentence(columns.size()));

    while (getline(sinput, line)) {
      if (line.empty()) {
	if (sentence->length > 1) {
	  if (sentenceValid) {
	    break;
	  } else {
	    sentence->clear();
	    sentenceValid = true;
	    std::cerr << "Warning: Skipping invalid sentence...\n";
	  }
	}
	continue;
      }

      sentenceValid = readLine(line, *sentence, sentenceValid);
    }

    if (!sentenceValid) {
      return nullptr;
    }

    return sentence;
  }


  std::shared_ptr<McfSentences> Mcf::readSentences() {
    std::shared_ptr<McfSentences> sentences(new McfSentences());
    
    std::shared_ptr<McfSentence> sentence;
    while ((sentence = readNextSentence())) {
      sentences->addSentence(sentence);
    }

    return sentences;
  }

  void Mcf::printHeader(std::ostream &output) {
    output << "#";
    bool first = true;
    for (McfColumn &column : columns) {
      if (!first)
	output << "\t";
      output << column.name << "@" << column.typeName;
      first = false;
    }
    output << "\n";
  }

  void Mcf::printHeader(FILE* output) {
    fprintf(output, "#");
    bool first = true;
    for (McfColumn &column : columns) {
      if (!first)
	fprintf(output, "\t");
      fprintf(output, "%s@%s", column.name.c_str(), column.typeName.c_str());
      first = false;
    }
    fprintf(output, "\n");
  }

  void Mcf::dumpAlphabets(std::ostream &output) {
    alphaArray.dump(output);
  }

  void Mcf::dumpAlphabets(std::string filename) {
    std::ofstream output(filename);
    if (!output.is_open())
      throw std::runtime_error("couldn't open alphabets file!");
    dumpAlphabets(output);
    output.close();
  }
}

