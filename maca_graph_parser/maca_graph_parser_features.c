/***************************************************************************
    Copyright (C) 2012 by Alexis Nasr <alexis.nasr@lif.univ-mrs.fr>
                          Ghasem Mirroshandel <ghasem.mirroshandel@lif.univ-mrs.fr>
    This file is part of maca_graph_parser.

    maca_graph_parser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    maca_graph_parser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with maca_graph_parser. If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

#include "maca_graph_parser_features.h"

#include <math.h>
#include <stdint.h>

#define maca_graph_parser_extract_bits(f,begin,end) (((f) << (63 - (end))) >> (63 + (begin) - (end)))

/*maca_graph_parser_extract_bits(feature_t f, int begin, int end)
{
  return ((f << (63 - end)) >> (63 + begin - end));
  }*/

int maca_graph_parser_get_feature_type(feature_t feat, maca_graph_parser_templ_library *tl)
{
  return (int) maca_graph_parser_extract_bits(feat, tl->type_start, tl->type_end);
}

templ *maca_graph_parser_get_templ(feature_t feat, maca_graph_parser_templ_library *tl)
{
  return tl->type2templ[maca_graph_parser_get_feature_type(feat, tl)];
}

int maca_graph_parser_get_feature_label(feature_t feat, maca_graph_parser_templ_library *tl)
{
  int type = maca_graph_parser_get_feature_type(feat, tl);
  return (int) maca_graph_parser_extract_bits(feat, tl->type2templ[type]->label_start, tl->type2templ[type]->label_end);
}

int maca_graph_parser_get_feature_direction(feature_t feat, maca_graph_parser_templ_library *tl)
{
  int type = maca_graph_parser_get_feature_type(feat, tl);
  return (int) maca_graph_parser_extract_bits(feat, tl->type2templ[type]->direction_start, tl->type2templ[type]->direction_end);
}

feature_t maca_graph_parser_get_feature_hash_key(feature_t feat, maca_graph_parser_templ_library *tl)
{
  int type = maca_graph_parser_get_feature_type(feat, tl);
  return maca_graph_parser_extract_bits(feat, tl->type2templ[type]->hash_key_start, tl->type2templ[type]->hash_key_end);
}

void maca_graph_parser_decompose_feature(feature_t feat, int *direction, int *label, feature_t *hash_key, maca_graph_parser_templ_library *tl)
{
  int type = maca_graph_parser_get_feature_type(feat, tl);
  *direction = (int) maca_graph_parser_extract_bits(feat, tl->type2templ[type]->direction_start, tl->type2templ[type]->direction_end);
  *label = (int) maca_graph_parser_extract_bits(feat, tl->type2templ[type]->label_start, tl->type2templ[type]->label_end);
  *hash_key =  maca_graph_parser_extract_bits(feat, tl->type2templ[type]->hash_key_start, tl->type2templ[type]->hash_key_end);
}

int maca_graph_parser_compute_bits_number(int v)
{
  if (v == 0) return 0;
  return (int)(log(v) / log(2)) + 1; 
}

void maca_graph_parser_print_feature_bin(FILE *f, feature_t feat)
{
  int i;
  for(i=63; i >=0; i--)
    fprintf(f, "%d", (int)((feat & ((feature_t)1 << i))>>i));
}

void maca_graph_parser_decode_feature(feature_t f, templ *t)
{
  int field;
  for(field=0; field < t->field_nb; field++)
    t->value[field] = maca_graph_parser_extract_bits(f, t->start[field], t->end[field]);
}

void maca_graph_parser_print_templ_field(FILE *f, templ *t, int field, maca_graph_parser_ctx *ctx)
{
  char pos_str[128];
  char fct_str[128];
  char word_str[128];
  char subcat_str[128];
  char type = t->type[field];
  int value = t->value[field];

  if(type == 's'){
    fprintf(f, "\t%d", value);
    }
  if(type == 't'){
    fprintf(f, "\t%d", value);
    }
  if(type == 'd'){
    if(value == ra)
      fprintf(f, "\t->");
    else
      fprintf(f, "\t<-");
    }
  if(type == 'f'){
    if(maca_alphabet_get_symbol(ctx->labels_alphabet, value, fct_str, sizeof(fct_str)))
      fprintf(f, "\t%s", fct_str);
    else
      fprintf(f, "\t(null)");
    }
  if(type == 'p'){
    if(maca_alphabet_get_symbol(ctx->pos_alphabet, value, pos_str, sizeof(pos_str)))
      fprintf(f, "\t%s", pos_str);
    else
      fprintf(f, "\t(null)");
  }
  if(type == 'w'){    
    if (maca_alphabet_get_symbol(ctx->words_alphabet, value, word_str, sizeof(word_str)))
      fprintf(f, "\t%s", word_str);
    else
      fprintf(f, "\t(null)");
  }

  if(type == 'l'){    
    if (maca_alphabet_get_symbol(ctx->synt_feats_alphabet, value, subcat_str, sizeof(subcat_str)))
      fprintf(f, "\t%s", subcat_str);
    else
      fprintf(f, "\t(null)");
  }

}

void maca_graph_parser_print_templ(FILE *f, templ *t, maca_graph_parser_ctx *ctx)
{
  int field;

  for(field=0; field < t->field_nb; field++)
    if(t->length[field]) maca_graph_parser_print_templ_field(f, t, field, ctx);
}

void maca_graph_parser_print_feature(FILE *f, maca_graph_parser_ctx *ctx, feature_t feat)
{
  templ *t = maca_graph_parser_get_templ(feat, ctx->e);
  maca_graph_parser_decode_feature(feat, t);
  maca_graph_parser_print_templ(f, t, ctx);
}

void maca_graph_parser_templ_free(templ *t)
{
  if(t) free(t);
}

templ *maca_graph_parser_templ_allocator(int v0, int v1, int v2, int v3, int v4, int v5, int v6, int v7, int v8, int v9, 
		       char t0, char t1, char t2, char t3, char t4, char t5, char t6, char t7, char t8, char t9,
		       int label_position, int direction_position, int hash_key_start, int hash_key_end)
{
  int start = 0;
  int end;
  /* int l; */
  templ *t = malloc(sizeof(templ));
  if(t == NULL){
    fprintf(stderr, "memory allocation error\n");
    exit(1);
  }
  t->field_nb = 10;

  start = 0;
  t->length[0] = maca_graph_parser_compute_bits_number(v0);
  end = start + t->length[0] - 1;
  t->start[0] = start;
  t->end[0] = end;
  t->type[0] = t0;

  start = end + 1;
  t->length[1] = maca_graph_parser_compute_bits_number(v1);
  end = start + t->length[1] - 1;
  t->start[1] = start;
  t->end[1] = end;
  t->type[1] = t1;

  start = end + 1;
  t->length[2] = maca_graph_parser_compute_bits_number(v2);
  end = start + t->length[2] - 1;
  t->start[2] = start;
  t->end[2] = end;
  t->type[2] = t2;

  start = end + 1;
  t->length[3] = maca_graph_parser_compute_bits_number(v3);
  end = start + t->length[3] - 1;
  t->start[3] = start;
  t->end[3] = end;
  t->type[3] = t3;

  start = end + 1;
  t->length[4] = maca_graph_parser_compute_bits_number(v4);
  end = start + t->length[4] - 1;
  t->start[4] = start;
  t->end[4] = end;
  t->type[4] = t4;

  start = end + 1;
  t->length[5] = maca_graph_parser_compute_bits_number(v5);
  end = start + t->length[5] - 1;
  t->start[5] = start;
  t->end[5] = end;
  t->type[5] = t5;

  start = end + 1;
  t->length[6] = maca_graph_parser_compute_bits_number(v6);
  end = start + t->length[6] - 1;
  t->start[6] = start;
  t->end[6] = end;
  t->type[6] = t6;

  start = end + 1;
  t->length[7] = maca_graph_parser_compute_bits_number(v7);
  end = start + t->length[7] - 1;
  t->start[7] = start;
  t->end[7] = end;
  t->type[7] = t7;

  start = end + 1;
  t->length[8] = maca_graph_parser_compute_bits_number(v8);
  end = start + t->length[8] - 1;
  t->start[8] = start;
  t->end[8] = end;
  t->type[8] = t8;

  start = end + 1;
  t->length[9] = maca_graph_parser_compute_bits_number(v9);
  end = start + t->length[9] - 1;
  t->start[9] = start;
  t->end[9] = end;
  t->type[9] = t9;

  t->label_start = t->start[label_position];
  t->label_end = t->end[label_position];

  t->direction_start = t->start[direction_position];
  t->direction_end = t->end[direction_position];

  t->hash_key_start = t->start[hash_key_start];
  t->hash_key_end = t->end[hash_key_end];

  return t;
}

void maca_graph_parser_templ_library_free(maca_graph_parser_templ_library *tl)
{
  if(tl){
    maca_graph_parser_templ_free(tl->tfdp);
    maca_graph_parser_templ_free(tl->tfdw);
    maca_graph_parser_templ_free(tl->tfdwp);
    maca_graph_parser_templ_free(tl->tfdpp);
    maca_graph_parser_templ_free(tl->tfdww);
    maca_graph_parser_templ_free(tl->tfdwpp);
    maca_graph_parser_templ_free(tl->tfdwwp);
    maca_graph_parser_templ_free(tl->tfdppp);
    maca_graph_parser_templ_free(tl->tfdpppp);
    maca_graph_parser_templ_free(tl->tfdwpwp);
    maca_graph_parser_templ_free(tl->tdppp);
    maca_graph_parser_templ_free(tl->tfddpp);
    maca_graph_parser_templ_free(tl->tfddppp);
    maca_graph_parser_templ_free(tl->tfddww);
    maca_graph_parser_templ_free(tl->tfddwp);
    maca_graph_parser_templ_free(tl->tfdsppp);
    maca_graph_parser_templ_free(tl->tfdspp);
    maca_graph_parser_templ_free(tl->tfdsww);
    maca_graph_parser_templ_free(tl->tfdswp);
    maca_graph_parser_templ_free(tl->tflpp);
    maca_graph_parser_templ_free(tl->tflppp);
    maca_graph_parser_templ_free(tl->tflpwp);
    free(tl);
  }
}

maca_graph_parser_templ_library *maca_graph_parser_templ_library_allocator(maca_graph_parser_ctx *ctx){
  /**
   * Allocate a templ library.
   *
   * FIXME: write a proper maca_graph_parser_templ_library_free() and use it !
   */

  maca_graph_parser_templ_library *e = malloc(sizeof(maca_graph_parser_templ_library));
  if(e == NULL){
    fprintf(stderr, "memory allocation error\n");
    exit(1);
  }

  e->s_pos =  ctx->pos_nb;
  /* e->s_word = maca_alphabet_size(ctx->alphabet); */
  e->s_word = ctx->words_nb;
  e->s_rel = ctx->labels_nb;
  e->s_synt_feat = ctx->synt_feats_nb;

  e->s_type = 255;
  e->s_dir = 2;
  e->s_dist = 10;
  e->s_feat = 0;
  e->s_child = 0;

  e->type_start = 0;
  e->type_end = maca_graph_parser_compute_bits_number(e->s_type) - 1;

  e->tfdp    = maca_graph_parser_templ_allocator(e->s_type, e->s_rel, e->s_dir, e->s_pos,  0 ,        0,         0,        0, 0, 0, 't', 'f', 'd', 'p', '0', '0', '0', '0', '0', '0', 1, 2, 3, 3);
  e->tfdw    = maca_graph_parser_templ_allocator(e->s_type, e->s_rel, e->s_dir, e->s_word, 0 ,        0,         0,        0, 0, 0, 't', 'f', 'd', 'w', '0', '0', '0', '0', '0', '0', 1, 2, 3, 3);
  e->tfdwp   = maca_graph_parser_templ_allocator(e->s_type, e->s_rel, e->s_dir, e->s_word, e->s_pos,  0,         0,        0, 0, 0, 't', 'f', 'd', 'w', 'p', '0', '0', '0', '0', '0', 1, 2, 3, 4);
  e->tfdpp   = maca_graph_parser_templ_allocator(e->s_type, e->s_rel, e->s_dir, e->s_pos,  e->s_pos,  0,         0,        0, 0, 0, 't', 'f', 'd', 'p', 'p', '0', '0', '0', '0', '0', 1, 2, 3, 4);
  e->tfdww   = maca_graph_parser_templ_allocator(e->s_type, e->s_rel, e->s_dir, e->s_word, e->s_word, 0,         0,        0, 0, 0, 't', 'f', 'd', 'w', 'w', '0', '0', '0', '0', '0', 1, 2, 3, 4);
  e->tfdwpp  = maca_graph_parser_templ_allocator(e->s_type, e->s_rel, e->s_dir, e->s_word, e->s_pos,  e->s_pos,  0,        0, 0, 0, 't', 'f', 'd', 'w', 'p', 'p', '0', '0', '0', '0', 1, 2, 3, 5);
  e->tfdwwp  = maca_graph_parser_templ_allocator(e->s_type, e->s_rel, e->s_dir, e->s_word, e->s_word, e->s_pos,  0,        0, 0, 0, 't', 'f', 'd', 'w', 'w', 'p', '0', '0', '0', '0', 1, 2, 3, 5);
  e->tfdppp  = maca_graph_parser_templ_allocator(e->s_type, e->s_rel, e->s_dir, e->s_pos,  e->s_pos,  e->s_pos,  0,        0, 0, 0, 't', 'f', 'd', 'p', 'p', 'p', '0', '0', '0', '0', 1, 2, 3, 5);
  e->tfdpppp = maca_graph_parser_templ_allocator(e->s_type, e->s_rel, e->s_dir, e->s_pos,  e->s_pos,  e->s_pos,  e->s_pos, 0, 0, 0, 't', 'f', 'd', 'p', 'p', 'p', 'p', '0', '0', '0', 1, 2, 3, 6);
  e->tfdwpwp = maca_graph_parser_templ_allocator(e->s_type, e->s_rel, e->s_dir, e->s_word, e->s_pos,  e->s_word, e->s_pos, 0, 0, 0, 't', 'f', 'd', 'w', 'p', 'w', 'p', '0', '0', '0', 1, 2, 3, 6);
  e->tfddpp  = maca_graph_parser_templ_allocator(e->s_type, e->s_rel, e->s_dir, e->s_dir,  e->s_pos,  e->s_pos,  0,        0, 0, 0, 't', 'f', 'd', 'd', 'p', 'p', '0', '0', '0', '0', 1, 2, 3, 5);
  e->tfddppp = maca_graph_parser_templ_allocator(e->s_type, e->s_rel, e->s_dir, e->s_dir,  e->s_pos,  e->s_pos,  e->s_pos, 0, 0, 0, 't', 'f', 'd', 'd', 'p', 'p', 'p', '0', '0', '0', 1, 2, 3, 6);
  e->tfddww  = maca_graph_parser_templ_allocator(e->s_type, e->s_rel, e->s_dir, e->s_dir,  e->s_word, e->s_word, 0,        0, 0, 0, 't', 'f', 'd', 'd', 'w', 'w', '0', '0', '0', '0', 1, 2, 3, 5);
  e->tfddwp  = maca_graph_parser_templ_allocator(e->s_type, e->s_rel, e->s_dir, e->s_dir,  e->s_word, e->s_pos,  0,        0, 0, 0, 't', 'f', 'd', 'd', 'w', 'p', '0', '0', '0', '0', 1, 2, 3, 5);
  e->tfdsppp = maca_graph_parser_templ_allocator(e->s_type, e->s_rel, e->s_dir, e->s_dist, e->s_pos,  e->s_pos,  e->s_pos, 0, 0, 0, 't', 'f', 'd', 's', 'p', 'p', 'p', '0', '0', '0', 1, 2, 3, 6);
  e->tfdspp  = maca_graph_parser_templ_allocator(e->s_type, e->s_rel, e->s_dir, e->s_dist, e->s_pos,  e->s_pos,  0,        0, 0, 0, 't', 'f', 'd', 's', 'p', 'p', '0', '0', '0', '0', 1, 2, 3, 5);
  e->tfdsww  = maca_graph_parser_templ_allocator(e->s_type, e->s_rel, e->s_dir, e->s_dist, e->s_word, e->s_word, 0,        0, 0, 0, 't', 'f', 'd', 's', 'w', 'w', '0', '0', '0', '0', 1, 2, 3, 5);
  e->tfdswp  = maca_graph_parser_templ_allocator(e->s_type, e->s_rel, e->s_dir, e->s_dist, e->s_word, e->s_pos,  0,        0, 0, 0, 't', 'f', 'd', 's', 'w', 'p', '0', '0', '0', '0', 1, 2, 3, 5);

  e->tdppp   = maca_graph_parser_templ_allocator(e->s_type, e->s_dir, e->s_pos, e->s_pos,  e->s_pos,  0,         0,        0, 0, 0, 't', 'd', 'p', 'p', 'p', '0', '0', '0', '0', '0', -1, 1, 2, 4);

  /* subcat features */
  e->tflpp   = maca_graph_parser_templ_allocator(e->s_type, e->s_rel, e->s_synt_feat, e->s_pos,  e->s_pos,  0,         0,        0, 0, 0, 't', 'f', 'l', 'p', 'p', '0', '0', '0', '0', '0', 1, -1, 2, 4); 
  e->tflppp   = maca_graph_parser_templ_allocator(e->s_type, e->s_rel, e->s_synt_feat, e->s_pos,  e->s_pos,  e->s_pos,         0,        0, 0, 0, 't', 'f', 'l', 'p', 'p', 'p', '0', '0', '0', '0', 1, -1, 2, 5); 
  e->tflpwp   = maca_graph_parser_templ_allocator(e->s_type, e->s_rel, e->s_synt_feat, e->s_pos,  e->s_word,  e->s_pos,         0,        0, 0, 0, 't', 'f', 'l', 'p', 'w', 'p', '0', '0', '0', '0', 1, -1, 2, 5); 


  /* basic features */
  e->type2templ[_f39] = e->tdppp;

  /* first order features with form */
  e->type2templ[_f1]  = e->tfdwp;   /* (hits, X, VBZ) -SBJ-> (X,   X, X)  */
  e->type2templ[_f2]  = e->tfdw;    /* (hits, X, X)   -SBJ-> (X,   X, X)  */
  e->type2templ[_f3]  = e->tfdp;    /* (X,    X, VBZ) -SBJ-> (X,   X, X)  */
  e->type2templ[_f4]  = e->tfdwp;   /* (X,    X, X)   -SBJ-> (boy, X, NN) */
  e->type2templ[_f5]  = e->tfdw;    /* (X,    X, X)   -SBJ-> (boy, X, X)  */
  e->type2templ[_f6]  = e->tfdp;    /* (X,    X, X)   -SBJ-> (X,   X, NN) */
  e->type2templ[_f7]  = e->tfdwpwp; /* (hits, X, VBZ) -SBJ-> (boy, X, NN) */
  e->type2templ[_f8]  = e->tfdwpp;  /* (X,    X, VBZ) -SBJ-> (boy, X, NN) */
  e->type2templ[_f9]  = e->tfdwwp;  /* (hits, X, X)   -SBJ-> (boy, X, NN) */
  e->type2templ[_f10] = e->tfdwpp;  /* (hits, X, VBZ) -SBJ-> (X,   X, NN) */
  e->type2templ[_f11] = e->tfdwwp;  /* (hits, X, VBZ) -SBJ-> (boy, X, XX) */
  e->type2templ[_f12] = e->tfdww;   /* (hits, X, X)   -SBJ-> (boy, X, X)  */
  e->type2templ[_f13] = e->tfdpp;   /* (X,    X, VBZ) -SBJ-> (X,   X, NN) */
 
  /* first order features with lemma */
  e->type2templ[_f1l]  = e->tfdwp;  /* (X, hit, VBZ)  -SBJ-> (X, X,   X)  */
  e->type2templ[_f2l]  = e->tfdw;   /* (X, hit, X)    -SBJ-> (X, X,   X)  */
  e->type2templ[_f3l]  = e->tfdp;    /* (X,    X, VBZ) -SBJ-> (X,   X, X)  */
  e->type2templ[_f4l]  = e->tfdwp;  /* (X, X,   X)    -SBJ-> (X, boy, NN) */
  e->type2templ[_f5l]  = e->tfdw;   /* (X, X,   X)    -SBJ-> (X, boy, XX) */
  e->type2templ[_f6l]  = e->tfdp;    /* (X,    X, X)   -SBJ-> (X,   X, NN) */
  e->type2templ[_f7l]  = e->tfdwpwp;/* (X, hit, VBZ)  -SBJ-> (X, boy, NN) */
  e->type2templ[_f8l]  = e->tfdwpp; /* (X, X,   VBZ)  -SBJ-> (X, boy, NN) */
  e->type2templ[_f9l]  = e->tfdwwp; /* (X, hit, X)    -SBJ-> (X, boy, NN) */
  e->type2templ[_f10l] = e->tfdwpp; /* (X, hit, VBZ)  -SBJ-> (X, X,   NN) */
  e->type2templ[_f11l] = e->tfdwwp; /* (X, hit, VBZ)  -SBJ-> (X, boy, XX) */
  e->type2templ[_f12l] = e->tfdww;  /* (X, hit, X)    -SBJ-> (X, boy, X)  */
  e->type2templ[_f13l] = e->tfdpp;   /* (X,    X, VBZ) -SBJ-> (X,   X, NN) */
 
  /* linear features */
  e->type2templ[_f14] = e->tfdppp;
  e->type2templ[_f15] = e->tfdppp;
  e->type2templ[_f16] = e->tfdppp;
  e->type2templ[_f17] = e->tfdpppp;
  e->type2templ[_f18] = e->tfdpppp;
  e->type2templ[_f19] = e->tfdpppp;
  e->type2templ[_f20] = e->tfdpppp;

  /* grandchildren features with pos only */
  e->type2templ[_f21] = e->tfddppp;/* (X, X, NN) -NMOD-> (X, X, TO) -X-> (X, X, NN) */  
  e->type2templ[_f22] = e->tfddpp; /* (X, X, NN) -NMOD-> (X, X, X ) -X-> (X, X, NN) */ 
  e->type2templ[_f23] = e->tfddpp; /* (X, X, X ) -NMOD-> (X, X, TO) -X-> (X, X, NN) */  

  /* grandchildren features with pos and forms */
  e->type2templ[_f24] = e->tfddww; /* (key, X, X)  -NMOD-> (X,  X, X ) -X-> (heaven, X, X) */
  e->type2templ[_f25] = e->tfddww; /* (X,   X, X)  -NMOD-> (to, X, X ) -X-> (heaven, X, X) */ 
  e->type2templ[_f26] = e->tfddwp; /* (X,   X, NN) -NMOD-> (X,  X, X ) -X-> (heaven, X, X) */  
  e->type2templ[_f27] = e->tfddwp; /* (X,   X, X)  -NMOD-> (X,  X, TO) -X-> (heaven, X, X) */  
  e->type2templ[_f28] = e->tfddwp; /* (key, X, X)  -NMOD-> (X,  X, X ) -X-> (X,      X, NN) */  
  e->type2templ[_f29] = e->tfddwp; /* (X,   X, X)  -NMOD-> (to, X, X ) -X-> (X,      X, NN) */ 

  /* grandchildren features with pos and lemmas */
  e->type2templ[_f24l] = e->tfddww; /* (X, key, X)  -NMOD-> (X, X,  X)  -X-> (X, heaven, X) */ 
  e->type2templ[_f25l] = e->tfddww; /* (X, X,   X)  -NMOD-> (X, to, X)  -X-> (X, heaven, X) */
  e->type2templ[_f26l] = e->tfddwp; /* (X, X,   NN) -NMOD-> (X, X,  X)  -X-> (X, heaven, X) */ 
  e->type2templ[_f27l] = e->tfddwp; /* (X, X,   X)  -NMOD-> (X, X,  TO) -X-> (X, heaven, X) */
  e->type2templ[_f28l] = e->tfddwp; /* (X, key, X)  -NMOD-> (X, X,  X)  -X-> (X, X,      NN)*/  
  e->type2templ[_f29l] = e->tfddwp; /* (X, X,   X)  -NMOD-> (X, to, X)  -X-> (X, X,      NN)*/ 

  /* grandchildren features with neighboring pos */
  e->type2templ[_f42] = e->tfdppp;
  e->type2templ[_f43] = e->tfdppp;
  e->type2templ[_f44] = e->tfdppp;
  e->type2templ[_f45] = e->tfdppp;
  e->type2templ[_f46] = e->tfdpppp;
  e->type2templ[_f47] = e->tfdpppp;
  e->type2templ[_f48] = e->tfdpppp;
  e->type2templ[_f49] = e->tfdpppp;
  e->type2templ[_f50] = e->tfdppp;
  e->type2templ[_f51] = e->tfdppp;
  e->type2templ[_f52] = e->tfdppp;
  e->type2templ[_f53] = e->tfdppp;
  e->type2templ[_f54] = e->tfdpppp;
  e->type2templ[_f55] = e->tfdpppp;
  e->type2templ[_f56] = e->tfdpppp;
  e->type2templ[_f57] = e->tfdpppp;

  /* sibling features with pos only */
  e->type2templ[_f30] = e->tfdsppp; /* (X, X, NNP) <- SBJ - (X, X, VB) - X -> (X, X, NN) */
  e->type2templ[_f31] = e->tfdspp;  /* (X, X, X  ) <- SBJ - (X, X, VB) - X -> (X, X, NN) */
  e->type2templ[_f32] = e->tfdspp;  /* (X, X, NNP) <- SBJ - (X, X, X ) - X -> (X, X, NN) */

  /* sibling features with pos and forms */
  e->type2templ[_f33] = e->tfdsww; /* (X,    X, X  ) <- SBJ - (ate, X, X ) - X -> (apple, X, X) */
  e->type2templ[_f34] = e->tfdsww; /* (John, X, X  ) <- SBJ - (X,   X, X ) - X -> (apple, X, X) */
  e->type2templ[_f35] = e->tfdswp; /* (X,    X, X  ) <- SBJ - (X,   X, VB) - X -> (apple, X, X) */
  e->type2templ[_f36] = e->tfdswp; /* (X,    X, NNP) <- SBJ - (X,   X, X ) - X -> (apple, X, X) */
  e->type2templ[_f37] = e->tfdswp; /* (X,    X, X  ) <- SBJ - (ate, X, X ) - X -> (X,     X, P) */
  e->type2templ[_f38] = e->tfdswp; /* (John, X, X  ) <- SBJ - (X,   X, X ) - X -> (X,     X, P) */

  /* sibling features with pos and lemmas */
  e->type2templ[_f33l] = e->tfdsww; /* (X, X,    X  ) <- SBJ - (X, eat, X ) - X -> (X, apple, X ) */
  e->type2templ[_f34l] = e->tfdsww; /* (X, John, X  ) <- SBJ - (X, X,   X ) - X -> (X, apple, X ) */
  e->type2templ[_f35l] = e->tfdswp; /* (X, X,    X  ) <- SBJ - (X, X,   VB) - X -> (X, apple, X ) */
  e->type2templ[_f36l] = e->tfdswp; /* (X, X,    NNP) <- SBJ - (X, X,   X ) - X -> (X, apple, X ) */
  e->type2templ[_f37l] = e->tfdswp; /* (X, X,    X  ) <- SBJ - (X, eat, X ) - X -> (X, X,     NN) */
  e->type2templ[_f38l] = e->tfdswp; /* (X, John, X  ) <- SBJ - (X, X,   X ) - X -> (X, X,     NN) */

  /* sibling features with neighboring pos */
  e->type2templ[_f58] = e->tfdppp;  /* (X,      X,    X     ) <-L- (X,      govP, X     ) -X-> (X,      sblP, sblPp1) */
  e->type2templ[_f59] = e->tfdppp;  /* (X,      X,    X     ) <-L- (X,      govP, X     ) -X-> (sblPm1, sblP, X     ) */
  e->type2templ[_f60] = e->tfdppp;  /* (X,      X,    X     ) <-L- (X,      govP, govPp1) -X-> (X,      sblP, X     ) */
  e->type2templ[_f61] = e->tfdppp;  /* (X,      X,    X     ) <-L- (govPm1, govP, X     ) -X-> (X,      sblP, X     ) */
  e->type2templ[_f62] = e->tfdpppp; /* (X,      X,    X     ) <-L- (govPm1, govP, X     ) -X-> (X,      sblP, sblPp1) */
  e->type2templ[_f63] = e->tfdpppp; /* (X,      X,    X     ) <-L- (govPm1, govP, X     ) -X-> (sblPm1, sblP, X     ) */
  e->type2templ[_f64] = e->tfdpppp; /* (X,      X,    X     ) <-L- (X,      govP, govPp1) -X-> (X,      sblP, sblPp1) */
  e->type2templ[_f65] = e->tfdpppp; /* (X,      X,    X     ) <-L- (X,      govP, govPp1) -X-> (sblPm1, sblP, X     ) */
  e->type2templ[_f66] = e->tfdppp;  /* (X,      depP, X     ) <-L- (X,      X,    X     ) -X-> (X,      sblP, sblPp1) */
  e->type2templ[_f67] = e->tfdppp;  /* (X,      depP, X     ) <-L- (X,      X,    X     ) -X-> (sblPm1, sblP, X     ) */
  e->type2templ[_f68] = e->tfdppp;  /* (X,      depP, depPp1) <-L- (X,      X,    X     ) -X-> (X,      sblP, X     ) */
  e->type2templ[_f69] = e->tfdppp;  /* (depPm1, depP, X     ) <-L- (X,      X,    X     ) -X-> (X,      sblP, X     ) */
  e->type2templ[_f70] = e->tfdpppp; /* (depPm1, depP, X     ) <-L- (X,      X,    X     ) -X-> (X,      sblP, sblPp1) */
  e->type2templ[_f71] = e->tfdpppp; /* (depPm1, depP, X     ) <-L- (X,      X,    X     ) -X-> (sblPm1, sblP, X     ) */
  e->type2templ[_f72] = e->tfdpppp; /* (X,      depP, depPp1) <-L- (X,      X,    X     ) -X-> (X,      sblP, sblPp1) */
  e->type2templ[_f73] = e->tfdpppp; /* (X,      depP, depPp1) <-L- (X,      X,    X     ) -X-> (sblPm1, sblP, X     ) */

  /* subcat templates */

  e->type2templ[_f77] = e->tflpp; /* (X, X, VBZ, SCAT) -SBJ-> (X,   X, N)  */
  e->type2templ[_f78] = e->tflppp; /* (X, X, VBZ, SCAT) -AOBJ-> (X,   X, PRE) -COMP-> (X,   X, N)  */
  e->type2templ[_f79] = e->tflpwp; /* (X, X, VBZ, SCAT) -AOBJ-> (X,   X, PRE) -COMP-> (X,   X, N)  */

  return e;
}

feature_t maca_graph_parser_encode_feature_2(templ *d) {
  feature_t l = (feature_t) d->value[0];
  int shift = d->length[0];

  /* if (d->value[0] < 0 || d->value[1] < 0) return -1; */
  l |= (feature_t)d->value[1] << shift;
  shift +=d->length[1];
  /* d->shift=shift; */
  
  return l;
}

feature_t maca_graph_parser_encode_feature_3(templ *d) {
  feature_t l = (feature_t) d->value[0];
  int shift = d->length[0];
  
  /* if (d->value[0] < 0 || d->value[1] < 0 || d->value[2] < 0) return -1; */

  l |= (feature_t)d->value[1] << shift;
  shift +=d->length[1];
  l |= (feature_t)d->value[2] << shift;
  /* d->shift=shift + d->length[2]; */
  
  return l;
}


feature_t maca_graph_parser_encode_feature_4(templ *d) {
  feature_t l = (feature_t) d->value[0];
  int shift =d->length[0];
  
  /* if (d->value[0] < 0  ||  d->value[1] < 0  ||  d->value[2] < 0  ||  d->value[3] < 0) return -1; */
  
  l |= (feature_t)d->value[1] << shift;
  shift += d->length[1];
  l |= (feature_t)d->value[2] << shift;
  shift += d->length[2];
  l |= (feature_t)d->value[3] << shift;
  /* d->shift = shift + d->length[3]; */
  
  return l;
}


feature_t maca_graph_parser_encode_feature_5(templ *d) {
  feature_t l = (feature_t) d->value[0];
  int shift =d->length[0];
  
  /* if (d->value[0] < 0 || d->value[1] < 0 || d->value[2] < 0 || d->value[3] < 0 || d->value[4] < 0) return -1; */
  
  l |= (feature_t)d->value[1] << shift;
  shift +=d->length[1];
  l |= (feature_t)d->value[2] << shift;
  shift +=d->length[2];
  l |= (feature_t)d->value[3] << shift;
  shift +=d->length[3];	
  l |= (feature_t)d->value[4] << shift;
  /* d->shift = shift + d->length[4]; */
		
  return l;
}

feature_t maca_graph_parser_encode_feature_6(templ *d) {
  feature_t l = (feature_t) d->value[0];
  int shift =d->length[0];
  
  /* if (d->value[0] < 0 || d->value[1] < 0 || d->value[2] < 0 || d->value[3] < 0 || d->value[4] < 0 || d->value[5] < 0) return -1; */
  
  l |= (feature_t)d->value[1] << shift;
  shift +=d->length[1];
  l |= (feature_t)d->value[2] << shift;
  shift +=d->length[2];
  l |= (feature_t)d->value[3] << shift;
  shift +=d->length[3];	
  l |= (feature_t)d->value[4] << shift;
  shift +=d->length[4];
  l |= (feature_t)d->value[5] << shift;
  /* d->shift =shift+d->length[5]; */
  
  return l;
}

feature_t maca_graph_parser_encode_feature_7(templ *d) {
  feature_t l = (feature_t) d->value[0];
  int shift = d->length[0];
  
  /* if (d->value[0] < 0 || d->value[1] < 0 || d->value[2] < 0 || d->value[3] < 0 || d->value[4] < 0 || d->value[5] < 0 || d->value[6] < 0) return -1; */
  
  l |= (feature_t)d->value[1] << shift;
  shift += d->length[1];
  l |= (feature_t)d->value[2] << shift;
  shift += d->length[2];
  l |= (feature_t)d->value[3] << shift;
  shift += d->length[3];	
  l |= (feature_t)d->value[4] << shift;
  shift += d->length[4];
  l |= (feature_t)d->value[5] << shift;
  shift += d->length[5];
  l |= (feature_t)d->value[6] << shift;
  /* d->shift = shift + d->length[6]; */
  
  return l;
}


feature_t maca_graph_parser_encode_feature_8(templ *d) {
  feature_t l = (feature_t) d->value[0];
  int shift =d->length[0];
  
  /* if (d->value[0] < 0 || d->value[1] < 0 || d->value[2] < 0 || d->value[3] < 0 || d->value[4] < 0 || d->value[5] < 0 || d->value[6] < 0 || d->value[7] < 0) return -1; */
  
  l |= (feature_t)d->value[1] << shift;
  shift +=d->length[1];
  l |= (feature_t)d->value[2] << shift;
  shift +=d->length[2];
  l |= (feature_t)d->value[3] << shift;
  shift +=d->length[3];	
  l |= (feature_t)d->value[4] << shift;
  shift +=d->length[4];
  l |= (feature_t)d->value[5] << shift;
  shift +=d->length[5];
  l |= (feature_t)d->value[6] << shift;
  shift +=d->length[6];
  l |= (feature_t)d->value[7] << shift;
  /* d->shift =shift+d->length[7]; */
  
  return l;
}


feat_vector *basic(maca_graph_parser_sentence *s, maca_graph_parser_ctx *ctx, int gov, int dep, feat_vector *fv){
  /**
   * Compute the vector of basic features for a dependency.
   *
   * Fills and returns fv if not NULL, a new feat_vector otherwise.
   */


  if(fv == NULL){
    fv = allocate_feat_vector(BasicFeatNb);
  } else {
    /* reset fv */
    fv->elt_nb = 0;
  }

  int dir = (gov < dep) ? ra : la;
  int end = (gov >= dep ? gov : dep);
  int begin = (gov >= dep ? dep : gov) + 1;
  maca_graph_parser_templ_library *e = ctx->e;

  e->tdppp->value[0] = _f39;
  e->tdppp->value[1] = dir;
  e->tdppp->value[2] = s->pos[gov];
  e->tdppp->value[3] = s->pos[dep];

  int i;
  for (i = begin; i < end; i++) {
    e->tdppp->value[4] = s->pos[i];
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_5(e->tdppp);
  }
  return fv;
}

feat_vector *first(maca_graph_parser_sentence *s, maca_graph_parser_ctx *ctx, int gov, int dep, int label, feat_vector *fv){
  /**
   * Compute the vector of first order features for a dependency.
   *
   * Fills and returns fv if not NULL, a new feat_vector otherwise.
   */

  if(fv == NULL){
    fv = allocate_feat_vector(FirstOrderFeatNb);
  } else {
    /* reset fv */
    fv->elt_nb = 0;
  }

  maca_graph_parser_templ_library *e = ctx->e;

  int dir = (gov < dep) ? ra : la;
  /* gov and dep */
  int govF = s->words[gov];
  int depF = s->words[dep];
  int govL = s->lemmas[gov];
  int depL = s->lemmas[dep];
  int govP = s->pos[gov];
  int depP = s->pos[dep];
  /* gov+-1, dep+-1 */
  int govm1P = (gov == 0) ? ctx->pos_start : s->pos[gov-1];
  int depm1P = (dep == 0) ? ctx->pos_start : s->pos[dep-1];
  int govp1P = (gov == (s->l-1)) ? ctx->pos_end : s->pos[gov+1];
  int depp1P = (dep == (s->l-1)) ? ctx->pos_end : s->pos[dep+1];

  int subcat_feats_nb = s->synt_feats_nb[gov];
  int *subcat_feats_array = s->synt_feats_array[gov];
  int i;

  /* fprintf(stderr, "extract first order features : gov : (%d,%s,%s) dep :(%d,%s,%s)\n", gov,  */

  /* (hits, hit, VBZ) -SBJ-> (boy, boy, NN) */
  if(ctx->use_full_forms){
    /* (hits, X, VBZ) -SBJ-> (X, X, X) */
    e->tfdwp->value[0] = _f1;
    e->tfdwp->value[1] = label;
    e->tfdwp->value[2] = dir;
    e->tfdwp->value[3] = govF;
    e->tfdwp->value[4] = govP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_5(e->tfdwp);

    /* MATE: 2 */
    /* (hits, X, X) -SBJ-> (X, X, X) */
    e->tfdw->value[0] = _f2;
    e->tfdw->value[1] = label;
    e->tfdw->value[2] = dir;
    e->tfdw->value[3] = govF;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_4(e->tfdw);

    /* MATE: 7 */
    /* (X, X, VBZ) -SBJ-> (X, X, X) */
    e->tfdp->value[0] = _f3;
    e->tfdp->value[1] = label;
    e->tfdp->value[2] = dir;
    e->tfdp->value[3] = govP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_4(e->tfdp);

    /* (X, X, X) -SBJ-> (boy, X, NN) */
    e->tfdwp->value[0] = _f4;
    //  e->tfdwp->value[1] = label;
    //  e->tfdwp->value[2] = dir;
    e->tfdwp->value[3] = depF;
    e->tfdwp->value[4] = depP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_5(e->tfdwp);

    /* different from the 2010 paper ; guess it is a typo in the paper */
    /* MATE: 4 */
    /* (X, X, X) -SBJ-> (boy, X, X) */
    e->tfdw->value[0] = _f5; 
    //  e->tfdw->value[1] = label;
    //  e->tfdw->value[2] = dir;
    e->tfdw->value[3] = depF;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_4(e->tfdw);

    /* MATE: 6 */
    /* (X, X, X) -SBJ-> (X, X, NN) */
    e->tfdp->value[0] = _f6;
    // e->tfdp->value[1] = label;
    // e->tfdp->value[2] = dir;
    e->tfdp->value[3] = depP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_4(e->tfdp);

    /* (hits, X, VBZ) -SBJ-> (boy, X, NN) */
    e->tfdwpwp->value[0] = _f7;
    e->tfdwpwp->value[1] = label;
    e->tfdwpwp->value[2] = dir;
    e->tfdwpwp->value[3] = govF;
    e->tfdwpwp->value[4] = govP;
    e->tfdwpwp->value[5] = depF;
    e->tfdwpwp->value[6] = depP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_7(e->tfdwpwp);

    /* (X, X, VBZ) -SBJ-> (boy, X, NN) */
    e->tfdwpp->value[0] = _f8;
    e->tfdwpp->value[1] = label;
    e->tfdwpp->value[2] = dir;
    e->tfdwpp->value[3] = depF;
    e->tfdwpp->value[4] = depP; 
    e->tfdwpp->value[5] = govP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdwpp);

    /* (hits, X, X) -SBJ-> (boy, X, NN) */
    e->tfdwwp->value[0] = _f9;
    e->tfdwwp->value[1] = label;
    e->tfdwwp->value[2] = dir;
    e->tfdwwp->value[3] = govF;
    e->tfdwwp->value[4] = depF;
    e->tfdwwp->value[5] = depP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdwwp);

    /* conform with paper */
    /* (hits, X, VBZ) -SBJ-> (boy, X, X) */
    e->tfdwpp->value[0] = _f10; 
    // e->tfdwpp->value[1] = label;
    // e->tfdwpp->value[2] = dir; 
    e->tfdwpp->value[3] = govF; 
    e->tfdwpp->value[4] = govP; 
    e->tfdwpp->value[5] = depP; 
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdwpp);

    /* (hits, X, VBZ) -SBJ-> (X, X, NN) */
    e->tfdwwp->value[0] = _f11;
    //  e->tfdwwp->value[1] = label;
    //  e->tfdwwp->value[2] = dir;
    e->tfdwwp->value[3] = govF;
    e->tfdwwp->value[4] = depF;
    e->tfdwwp->value[5] = govP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdwwp);

    /* MATE: 5 */
    /* (hits, X, X) -SBJ-> (boy, X, X) */
    e->tfdww->value[0] = _f12;
    e->tfdww->value[1] = label;
    e->tfdww->value[2] = dir;
    e->tfdww->value[3] = govF;
    e->tfdww->value[4] = depF;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_5(e->tfdww);

    /* MATE: 8 */
    /* (X, X, VBZ) -SBJ-> (X, X, NN) */
    e->tfdpp->value[0] = _f13; 
    e->tfdpp->value[1] = label;
    e->tfdpp->value[2] = dir; 
    e->tfdpp->value[3] = govP; 
    e->tfdpp->value[4] = depP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_5(e->tfdpp);
  }

  // lemmas
  if(ctx->use_lemmas){
    /* _f1l = _f77 */
    /* (X, hit, VBZ) -SBJ-> (X, X, X) */
    e->tfdwp->value[0] = _f1l;
    //  e->tfdwp->value[1] = label;
    //  e->tfdwp->value[2] = dir;
    e->tfdwp->value[3] = govL;
    e->tfdwp->value[4] = govP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_5(e->tfdwp);

    /* _f2l = _f78 */
    /* (X, hit, X) -SBJ-> (X, X, X) */
    e->tfdw->value[0] = _f2l;
    e->tfdw->value[1] = label;
    e->tfdw->value[2] = dir;
    e->tfdw->value[3] = govL;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_4(e->tfdw);

    /* _f3l = _f79 */
    /* (X, X, VBZ) -SBJ-> (X, X, X) */
    e->tfdp->value[0] = _f3l;
    e->tfdp->value[1] = label;
    e->tfdp->value[2] = dir;
    e->tfdp->value[3] = govP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_4(e->tfdp);

    /* _f4l = _f80 */
    /* (X, X, X) -SBJ-> (X, boy, NN) */
    e->tfdwp->value[0] = _f4l;
    //  e->tfdwp->value[1] = label;
    //  e->tfdwp->value[2] = dir;
    e->tfdwp->value[3] = depL;
    e->tfdwp->value[4] = depP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_5(e->tfdwp);

    /* _f5l = _f81 */
    /* (X, X,X) -SBJ-> (X, boy, XX) */
    e->tfdw->value[0] = _f5l;
    //  e->tfdw->value[1] = label;
    //  e->tfdw->value[2] = dir;
    e->tfdw->value[3] = depL;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_4(e->tfdw);

    /* _f6l = _f82 */
    /* (X, X, X) -SBJ-> (X, X, NN) */
    e->tfdp->value[0] = _f6l;
    // e->tfdp->value[1] = label;
    // e->tfdp->value[2] = dir;
    e->tfdp->value[3] = depP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_4(e->tfdp);
  
    /* _f7l = _f83 */
    /* (X, hit, VBZ) -SBJ-> (X, boy, NN) */
    e->tfdwpwp->value[0] = _f7l;
    e->tfdwpwp->value[1] = label;
    e->tfdwpwp->value[2] = dir;
    e->tfdwpwp->value[3] = govL;
    e->tfdwpwp->value[4] = govP;
    e->tfdwpwp->value[5] = depL;
    e->tfdwpwp->value[6] = depP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_7(e->tfdwpwp);

    /* _f8l = _f84 */
    /* (X, X, VBZ) -SBJ-> (X, boy, NN) */
    e->tfdwpp->value[0] = _f8l;
    e->tfdwpp->value[1] = label;
    e->tfdwpp->value[2] = dir;
    e->tfdwpp->value[3] = depL;
    e->tfdwpp->value[4] = depP; 
    e->tfdwpp->value[5] = govP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdwpp);

    /* _f9l = _f85 */
    /* (X, hit, X) -SBJ-> (X, boy, NN) */
    e->tfdwwp->value[0] = _f9l;
    e->tfdwwp->value[1] = label;
    e->tfdwwp->value[2] = dir;
    e->tfdwwp->value[3] = govL;
    e->tfdwwp->value[4] = depL;
    e->tfdwwp->value[5] = depP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdwwp);

    /* _f10l = _f86 */
    /* (X, hit, VBZ) -SBJ-> (X, X, NN) */
    e->tfdwpp->value[0] = _f10l; 
    // e->tfdwpp->value[1] = label;
    // e->tfdwpp->value[2] = dir; 
    e->tfdwpp->value[3] = govL; 
    e->tfdwpp->value[4] = govP; 
    e->tfdwpp->value[5] = depP; 
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdwpp);

    /* _f11l = _f87 */
    /* (X, hit, VBZ) -SBJ-> (X, boy, XX) */
    e->tfdwwp->value[0] = _f11l;
    //  e->tfdwwp->value[1] = label;
    //  e->tfdwwp->value[2] = dir;
    e->tfdwwp->value[3] = govL;
    e->tfdwwp->value[4] = depL;
    e->tfdwwp->value[5] = govP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdwwp);

    /* _f12l = _f88 */
    /* (X, hit, X) -SBJ-> (X, boy, X) */
    e->tfdww->value[0] = _f12l;
    e->tfdww->value[1] = label;
    e->tfdww->value[2] = dir;
    e->tfdww->value[3] = govL;
    e->tfdww->value[4] = depL;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_5(e->tfdww);

    /* _f13l = _f89 */
    /* (X, X, VBZ) -SBJ-> (X, X, NN) */
    e->tfdpp->value[0] = _f13l; 
    e->tfdpp->value[1] = label;
    e->tfdpp->value[2] = dir; 
    e->tfdpp->value[3] = govP; 
    e->tfdpp->value[4] = depP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_5(e->tfdpp);
  }

  /* MATE: special values for cases like (govm1 == dep) */
  /* linear features */
  if(1){
    e->tfdppp->value[0] = _f14; 
    e->tfdppp->value[1] = label; 
    e->tfdppp->value[2] = dir;
    e->tfdppp->value[3] = govP;
    e->tfdppp->value[4] = govp1P;
    e->tfdppp->value[5] = depP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdppp);

    e->tfdppp->value[0] = _f15;
    //  e->tfdppp->value[1] = label;
    //  e->tfdppp->value[2] = dir;
    // e->tfdppp->value[3] = govP;
    e->tfdppp->value[4] = depm1P;
    // e->tfdppp->value[5] = depP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdppp);

    e->tfdppp->value[0] = _f16;
    //  e->tfdppp->value[1] = label;
    //  e->tfdppp->value[2] = dir;
    // e->tfdppp->value[3] = govP;
    e->tfdppp->value[4] = depP;
    e->tfdppp->value[5] = depp1P;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdppp);

    /* MATE: 10 */
    e->tfdpppp->value[0] = _f17;
    e->tfdpppp->value[1] = label;
    e->tfdpppp->value[2] = dir;
    e->tfdpppp->value[3] = govP;
    e->tfdpppp->value[4] = govp1P;
    e->tfdpppp->value[5] = depm1P;
    e->tfdpppp->value[6] = depP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_7(e->tfdpppp);

    /* MATE: 11 */
    e->tfdpppp->value[0] = _f18;
    //  e->tfdpppp->value[1] = label;
    //  e->tfdpppp->value[2] = dir;
    e->tfdpppp->value[3] = govm1P;
    e->tfdpppp->value[4] = govP;
    // e->tfdpppp->value[5] = depm1P;
    // e->tfdpppp->value[6] = depP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_7(e->tfdpppp);

    /* MATE: 9 */
    e->tfdpppp->value[0] = _f19;
    //  e->tfdpppp->value[1] = label;
    //  e->tfdpppp->value[2] = dir;
    e->tfdpppp->value[3] = govP;
    e->tfdpppp->value[4] = govp1P;
    e->tfdpppp->value[5] = depP;
    e->tfdpppp->value[6] = depp1P;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_7(e->tfdpppp);

    /* MATE: 12 */
    e->tfdpppp->value[0] = _f20;
    //  e->tfdpppp->value[1] = label;
    //  e->tfdpppp->value[2] = dir;
    e->tfdpppp->value[3] = govm1P;
    e->tfdpppp->value[4] = govP;
    // e->tfdpppp->value[5] = depP;
    // e->tfdpppp->value[6] = depp1P;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_7(e->tfdpppp);
  }

  /* subcat features */
  if(ctx->subcat_features){
   for(i=0; i < subcat_feats_nb; i++){
     e->tflpp->value[0] = _f77;
     e->tflpp->value[1] = label;
     e->tflpp->value[2] = subcat_feats_array[i];
     e->tflpp->value[3] = govP;
     e->tflpp->value[4] = depP;
     fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_5(e->tflpp);
   }
  }





  /* _f41: morphological features (fields FEAT and PFEAT of the CONLL format) */
  /*  if (feats == null)
      return;

      if (_f41 > 0) {
      short[] featsP = feats[gov], featsD = feats[dep];
      dlf->value[0] = _f41;
      dlf->value[1] = label;
      dlf->value[2] = govP;
      dlf->value[3] = depP;
      extractFeat(f, dir, featsP, featsD);
      }
  */

  return fv;
}


feat_vector *grandchildren(maca_graph_parser_sentence *s, maca_graph_parser_ctx *ctx, int gov, int dep, int gdep, int label, feat_vector *fv){
  /**
   *
   */

  if(fv == NULL){
    fv = allocate_feat_vector(GrandchildrenFeatNb);
  } else {
    /* reset */
    fv->elt_nb = 0;
  }

  maca_graph_parser_templ_library *e = ctx->e;

  int dir = (gov < dep) ? ra : la;
  /* gov and dep */
  int govF = s->words[gov];
  int depF = s->words[dep];
  int govL = s->lemmas[gov];
  int depL = s->lemmas[dep];
  int govP = s->pos[gov];
  int depP = s->pos[dep];
  /* gov and dep +- 1 */
  int govPm1 = (gov == 0) ? ctx->pos_start : s->pos[gov - 1]; // parent-pos-minus1
  int depPm1 = (dep == 0) ? ctx->pos_start : s->pos[dep - 1]; // child-pos-minus1
  int govPp1 = (gov == s->l - 1) ? ctx->pos_end : s->pos[gov + 1];
  int depPp1 = (dep == s->l - 1) ? ctx->pos_end : s->pos[dep + 1];
  /* gdep == -1 in case the grandchild does not exist */
  int dir_gdep = (dep < gdep) ? ra : la;
  int gdepP, gdepF, gdepL, gdepPm1, gdepPp1;

  int subcat_feats_nb = s->synt_feats_nb[gov];
  int *subcat_feats_array = s->synt_feats_array[gov];
  int i;


  if(gdep == -1){
    gdepP = ctx->pos_start;
    gdepF = ctx->w_start;
    gdepL = ctx->w_start;
    gdepPm1 = ctx->pos_start;
    gdepPp1 = ctx->pos_end;
  } else {
    gdepP = s->pos[gdep];
    gdepF = s->words[gdep];
    gdepL = s->lemmas[gdep];
    gdepPm1 = (gdep == 0) ? ctx->pos_start : s->pos[gdep - 1];
    gdepPp1 = (gdep == s->l - 1) ? ctx->pos_end : s->pos[gdep + 1];
  }

  /* (key, key, NN) -NMOD-> (to, to, TO) -PMOD-> (heaven, heaven, NN) */  

  if(1){
    /* (X, X, NN) -NMOD-> (X, X, TO) -X-> (X, X, NN) */  
    e->tfddppp->value[0] = _f21;
    e->tfddppp->value[1] = label;
    e->tfddppp->value[2] = dir;
    e->tfddppp->value[3] = dir_gdep;
    e->tfddppp->value[4] = govP;
    e->tfddppp->value[5] = depP;
    e->tfddppp->value[6] = gdepP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_7(e->tfddppp); 

    /* (X, X, NN) -NMOD-> (X, X, X) -X-> (X, X, NN) */  
    e->tfddpp->value[0] = _f22;
    e->tfddpp->value[1] = label;
    e->tfddpp->value[2] = dir;
    e->tfddpp->value[3] = dir_gdep;
    e->tfddpp->value[4] = govP;
    e->tfddpp->value[5] = gdepP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfddpp); 

    /* (X, X, X) -NMOD-> (X, X, TO) -X-> (X, X, NN) */  
    e->tfddpp->value[0] = _f23;
    //    e->tfddpp->value[1] = label;
    //    e->tfddpp->value[2] = dir;
    //    e->tfddpp->value[3] = dir_gdep;
    e->tfddpp->value[4] = depP;
    e->tfddpp->value[5] = gdepP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfddpp); 
  }
  
  if(ctx->use_full_forms){
    /* (key, X, X) -NMOD-> (X, X, X) -X-> (heaven, X, X) */  
    e->tfddww->value[0] = _f24;
    e->tfddww->value[1] = label;
    e->tfddww->value[2] = dir;
    e->tfddww->value[3] = dir_gdep;
    e->tfddww->value[4] = govF;
    e->tfddww->value[5] = gdepF;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfddww);
    
    /* (X, X, X) -NMOD-> (to, X, X) -X-> (heaven, X, X) */  
    e->tfddww->value[0] = _f25;
    //    e->tfddww->value[1] = label;
    //    e->tfddww->value[2] = dir;
    //    e->tfddww->value[3] = dir_gdep;
    e->tfddww->value[4] = depF;
    // e->tfddww->value[5] = gdepF;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfddww);
    
    /* (X, X, NN) -NMOD-> (X, X, X) -X-> (heaven, X, X) */  
    e->tfddwp->value[0] = _f26;
    e->tfddwp->value[1] = label;
    e->tfddwp->value[2] = dir;
    e->tfddwp->value[3] = dir_gdep;
    e->tfddwp->value[4] = gdepF;
    e->tfddwp->value[5] = govP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfddwp);
    
    /* (X, X, X) -NMOD-> (X, X, TO) -X-> (heaven, X, X) */  
    e->tfddwp->value[0] = _f27;
    //    e->tfddwp->value[1] = label;
    //    e->tfddwp->value[2] = dir;
    //    e->tfddwp->value[3] = dir_gdep;
    // e->tfddwp->value[4] = gdepF;
    e->tfddwp->value[5] = depP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfddwp);
    
    /* (key, X, X) -NMOD-> (X, X, X) -X-> (X, X, NN) */  
    e->tfddwp->value[0] = _f28;
    //    e->tfddwp->value[1] = label;
    //    e->tfddwp->value[2] = dir;
    //    e->tfddwp->value[3] = dir_gdep;
    e->tfddwp->value[4] = govF;
    e->tfddwp->value[5] = gdepP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfddwp);
    
    /* (X, X, X) -NMOD-> (to, X, X) -X-> (X, X, NN) */  
    e->tfddwp->value[0] = _f29;
    //    e->tfddwp->value[1] = label;
    //    e->tfddwp->value[2] = dir;
    //    e->tfddwp->value[3] = dir_gdep;
    e->tfddwp->value[4] = depF;
    //    e->tfddwp->value[5] = gdepP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfddwp);
  }
  
  if(ctx->use_lemmas){
    /* _f23l = _f92 */
    /* feature in paper not in macaon */

    /* _f24l = _f91 */
    /* (X, key, X) -NMOD-> (X, X, X) -X-> (X, heaven, X) */      
    e->tfddww->value[0] = _f24l;
    //    e->tfddww->value[1] = label;
    //    e->tfddww->value[2] = dir;
    //    e->tfddww->value[3] = dir_gdep;
    e->tfddww->value[4] = govL;
    e->tfddww->value[5] = gdepL;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfddww);

    /* _f25l = ??? */
    /* feature in macaon not in paper */
    /* (X, X, X) -NMOD-> (X, to, X) -X-> (X, heaven, X) */  
    e->tfddww->value[0] = _f25l;
    //    e->tfddww->value[1] = label;
    //    e->tfddww->value[2] = dir;
    //    e->tfddww->value[3] = dir_gdep;
    e->tfddww->value[4] = depL;
    e->tfddww->value[5] = gdepL;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfddww);
    
    /* _f26l = _f93 */
    /* (X, X, NN) -NMOD-> (X, X, X) -X-> (X, heaven, X) */  
    e->tfddwp->value[0] = _f26l;
    //    e->tfddwp->value[1] = label;
    //    e->tfddwp->value[2] = dir;
    //    e->tfddwp->value[3] = dir_gdep;
    e->tfddwp->value[4] = gdepL;
    e->tfddwp->value[5] = govP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfddwp);
    
    /* _f27l = _f94 */
    /* (X, X, X) -NMOD-> (X, X, TO) -X-> (X, heaven, X) */  
    e->tfddwp->value[0] = _f27l;
    //    e->tfddwp->value[1] = label;
    //    e->tfddwp->value[2] = dir;
    //    e->tfddwp->value[3] = dir_gdep;
    //    e->tfddwp->value[4] = gdepL;
    e->tfddwp->value[5] = depP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfddwp);
    
    /* _f28l = _f95 */
    /* (X, key, X) -NMOD-> (X, X, X) -X-> (X, X, NN) */  
    e->tfddwp->value[0] = _f28l;
    //    e->tfddwp->value[1] = label;
    //    e->tfddwp->value[2] = dir;
    //    e->tfddwp->value[3] = dir_gdep;
    e->tfddwp->value[4] = govL;
    e->tfddwp->value[5] = gdepP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfddwp);
    
    /* _f29l = _f96 */
    /* (X, X, X) -NMOD-> (X, to, X) -X-> (X, X, NN) */  
    e->tfddwp->value[0] = _f29l;
    //    e->tfddwp->value[1] = label;
    //    e->tfddwp->value[2] = dir;
    //    e->tfddwp->value[3] = dir_gdep;
    e->tfddwp->value[4] = depL;
    //    e->tfddwp->value[5] = gdepP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfddwp);
  }

  // linear features
  /* (X, X, X) -> (X, depP, X) -> (X, gdepP, gdepPp1) */
  e->tfdppp->value[0] = _f42;
  e->tfdppp->value[1] = label;
  e->tfdppp->value[2] = dir;
  e->tfdppp->value[3] = gdepP;
  e->tfdppp->value[4] = gdepPp1;
  e->tfdppp->value[5] = depP;
  fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdppp); 

  /* (X, X, X) -> (X, depP, X) -> (gdepPm1, gdepP, X) */
  e->tfdppp->value[0] = _f43;
  //  e->tfdppp->value[1] = label;
  //  e->tfdppp->value[2] = dir;
  // e->tfdppp->value[3] = gdepP;
  e->tfdppp->value[4] = gdepPm1;
  // e->tfdppp->value[5] = depP;
  fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdppp); 
  
  /* (X, X, X) -> (X, depP, depPp1) -> (X, gdepP, X) */
  e->tfdppp->value[0] = _f44;
  //  e->tfdppp->value[1] = label;
  //  e->tfdppp->value[2] = dir;
  // e->tfdppp->value[3] = gdepP;
  e->tfdppp->value[4] = depPp1;
  // e->tfdppp->value[5] = depP;
  fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdppp); 
    
  /* (X, X, X) -> (depPm1, depP, X) -> (X, gdepP, X) */
  e->tfdppp->value[0] = _f45;
  //  e->tfdppp->value[1] = label;
  //  e->tfdppp->value[2] = dir;
  // e->tfdppp->value[3] = gdepP;
  e->tfdppp->value[4] = depPm1;
  // e->tfdppp->value[5] = depP;
  fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdppp); 
  
  /* (X, X, X) -> (depPm1, depP, X) -> (X, gdepP, gdepPp1) */
  e->tfdpppp->value[0] = _f46;
  e->tfdpppp->value[1] = label;
  e->tfdpppp->value[2] = dir;
  e->tfdpppp->value[3] = gdepP;
  e->tfdpppp->value[4] = gdepPp1;
  e->tfdpppp->value[5] = depP;
  e->tfdpppp->value[6] = depPm1;
  fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_7(e->tfdpppp); 
  
  /* (X, X, X) -> (depPm1, depP, X) -> (gdepPm1, gdepP, X) */
  e->tfdpppp->value[0] = _f47;
  //  e->tfdpppp->value[1] = label;
  //  e->tfdpppp->value[2] = dir;
  e->tfdpppp->value[3] = gdepPm1;
  e->tfdpppp->value[4] = gdepP;
  // e->tfdpppp->value[5] = depP;
  // e->tfdpppp->value[6] = depPm1;
  fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_7(e->tfdpppp); 
  
  /* (X, X, X) -> (X, depP, depPp1) -> (X, gdepP, gdepPp1) */
  e->tfdpppp->value[0] = _f48;
  //  e->tfdpppp->value[1] = label;
  //  e->tfdpppp->value[2] = dir;
  e->tfdpppp->value[3] = gdepPp1;
  // e->tfdpppp->value[4] = gdepP;
  // e->tfdpppp->value[5] = depP;
  e->tfdpppp->value[6] = depPp1;
  fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_7(e->tfdpppp); 
  
  /* (X, X, X) -> (X, depP, depPp1) -> (gdepPm1, gdepP, X) */
  e->tfdpppp->value[0] = _f49;
  //  e->tfdpppp->value[1] = label;
  //  e->tfdpppp->value[2] = dir;
  e->tfdpppp->value[3] = gdepPm1;
  // e->tfdpppp->value[4] = gdepP;
  // e->tfdpppp->value[5] = depP;
  // e->tfdpppp->value[6] = depPp1;
  fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_7(e->tfdpppp); 
  
  /* (X, govP, X) -> (X, X, X) -> (X, gdepP, gdepPp1) */
  e->tfdppp->value[0] = _f50;
  //  e->tfdppp->value[1] = label;
  //  e->tfdppp->value[2] = dir;
  e->tfdppp->value[3] = gdepPp1;
  e->tfdppp->value[4] = gdepP;
  e->tfdppp->value[5] = govP;
  fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdppp); 
  
  /* (X, govP, X) -> (X, X, X) -> (gdepPm1, gdepP, X) */
  e->tfdppp->value[0] = _f51;
  //  e->tfdppp->value[1] = label;
  //  e->tfdppp->value[2] = dir;
  e->tfdppp->value[3] = gdepPm1;
  // e->tfdppp->value[4] = gdepP;
  // e->tfdppp->value[5] = govP;
  fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdppp); 
  
  /* (X, govP, govPp1) -> (X,X,X) -> (X, gdepP, X) */
  e->tfdppp->value[0] = _f52;
  //  e->tfdppp->value[1] = label;
  //  e->tfdppp->value[2] = dir;
  e->tfdppp->value[3] = govPp1;
  // e->tfdppp->value[4] = gdepP;
  // e->tfdppp->value[5] = govP;
  fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdppp); 
  
  /* (govPm1, govP, X) -> (X, X, X) -> (X, gdepP, X) */
  e->tfdppp->value[0] = _f53;
  //  e->tfdppp->value[1] = label;
  //  e->tfdppp->value[2] = dir;
  e->tfdppp->value[3] = govPm1;
  // e->tfdppp->value[4] = gdepP;
  // e->tfdppp->value[5] = govP;
  fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdppp); 

  /* (govPm1, govP, X) -> (X, X, X) -> (X, gdepP, gdepPp1) */
  e->tfdpppp->value[0] = _f54;
  //  e->tfdpppp->value[1] = label;
  //  e->tfdpppp->value[2] = dir;
  e->tfdpppp->value[3] = govPm1;
  e->tfdpppp->value[4] = gdepPp1;
  e->tfdpppp->value[5] = gdepP;
  e->tfdpppp->value[6] = govP;
  fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_7(e->tfdpppp); 

  // I think that I introduced a bug:
  // e->dl1->value[0]= _f55; e->dl1->value[2]=gdepPm1; e->dl1->value[3]=gdepP;//e->dl1->value[4]=depPm1;e->dl1->value[5]=cldP;

  /* (govPm1, govP, X) -> (X, X, X) -> (gdepPm1, gdepP, X) */
  e->tfdpppp->value[0] = _f55;
  //  e->tfdpppp->value[1] = label;
  //  e->tfdpppp->value[2] = dir;
  // e->tfdpppp->value[3] = govPm1;
  e->tfdpppp->value[4] = gdepPm1;
  // e->tfdpppp->value[5] = gdepP;
  // e->tfdpppp->value[6] = govP;
  fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_7(e->tfdpppp); 

  /* (X, govP, govPp1) -> (X, X, X) -> (X, gdepP, gdepPp1) */
  e->tfdpppp->value[0] = _f56;
  //  e->tfdpppp->value[1] = label;
  //  e->tfdpppp->value[2] = dir;
  e->tfdpppp->value[3] = govPp1;
  e->tfdpppp->value[4] = gdepPp1;
  // e->tfdpppp->value[5] = gdepP;
  // e->tfdpppp->value[6] = govP;
  fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_7(e->tfdpppp); 

  // I think that I introduced a bug:
  // e->dl1->value[0]= _f57; e->dl1->value[2]=gdepPm1; e->dl1->value[3]=gdepP; e->dl1->value[4]=govP;
  // //e->dl1->value[5]=depPp1;

  /* (X, govP, govPp1) -> (X, X, X) -> (gdepPm1, gdepP, X) */
  e->tfdpppp->value[0] = _f57;
  //  e->tfdpppp->value[1] = label;
  //  e->tfdpppp->value[2] = dir;
  // e->tfdpppp->value[3] = govPp1;
  e->tfdpppp->value[4] = gdepPm1;
  // e->tfdpppp->value[5] = gdepP;
  // e->tfdpppp->value[6] = govP;
  fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_7(e->tfdpppp); 


  /* subcat features */
  if(ctx->subcat_features){
   for(i=0; i < subcat_feats_nb; i++){
     e->tflppp->value[0] = _f78;
     e->tflppp->value[1] = label;
     e->tflppp->value[2] = subcat_feats_array[i];
     e->tflppp->value[3] = govP;
     e->tflppp->value[4] = depP;
     e->tflppp->value[5] = gdepP;
     fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tflppp);

     e->tflpwp->value[0] = _f79;
     e->tflpwp->value[1] = label;
     e->tflpwp->value[2] = subcat_feats_array[i];
     e->tflpwp->value[3] = govP;
     e->tflpwp->value[4] = depL;
     e->tflpwp->value[5] = gdepP;
     fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tflpwp);
   }
  }


  /* _f74: morphological features (fields FEAT and PFEAT of the CONLL format) */
  /*
  if (feats == null)
    return;

  short[] featsP = feats[dep];
  short[] featsD = gdep != -1 ? feats[gdep] : null;

  dlf.v0 = _f74;
  dlf.v1 = label;
  dlf.v2 = gdepP;
  dlf.v3 = depP;
  extractFeat(f, dir, featsP, featsD);
  */
  return fv;
}

feat_vector *sibling(maca_graph_parser_sentence *s, maca_graph_parser_ctx *ctx, int gov, int dep, int sblng, int label, feat_vector *fv){
  /**
   *
   */

  if(fv == NULL){
    fv = allocate_feat_vector(SiblingFeatNb);
  } else {
    /* reset */
    fv->elt_nb = 0;
  }

  maca_graph_parser_templ_library *e = ctx->e;

  int dir = (gov < dep) ? ra : la;
  /* gov and dep */
  int govF = s->words[gov];
  int govL = s->lemmas[gov];
  int govP = s->pos[gov];
  int depF = s->words[dep];
  int depL = s->lemmas[dep];
  int depP = s->pos[dep];
  /* gov and dep +- 1 */
  int govPm1 = (gov == 0) ? ctx->pos_start : s->pos[gov - 1]; // parent-pos-minus1
  int depPm1 = (dep == 0) ? ctx->pos_start : s->pos[dep - 1]; // child-pos-minus1
  int govPp1 = (gov == s->l - 1) ? ctx->pos_end : s->pos[gov + 1];
  int depPp1 = (dep == s->l - 1) ? ctx->pos_end : s->pos[dep + 1];
  /* sblng == -1 in case dep has no sibling */
  int sblP, sblF, sblL, sblPm1, sblPp1;
  if(sblng == -1){
    sblP = ctx->pos_start;
    sblF = ctx->w_start;
    sblL = ctx->w_start;
    sblPm1 = ctx->pos_start;
    sblPp1 = ctx->pos_end;
  } else {
    sblP = s->pos[sblng];
    sblF = s->words[sblng];
    sblL = s->lemmas[sblng];
    sblPm1 = (sblng == 0) ? ctx->pos_start : s->pos[sblng - 1];
    sblPp1 = (sblng == s->l - 1) ? ctx->pos_end : s->pos[sblng + 1];
  }
  /* distance */
  int real_dist = abs(gov - dep);
  int dist;
  if (real_dist > 10)
    dist = d10;
  else if (real_dist > 5)
    dist = d5;
  else if (real_dist == 5)
    dist = d4;
  else if (real_dist == 4)
    dist = d3;
  else if (real_dist == 3)
    dist = d2;
  else if (real_dist == 2)
    dist = d1;
  else
    dist = di0;

  /* common values to all features */
  e->tfdsppp->value[1] = label;
  e->tfdspp->value[1] = label;
  e->tfdsww->value[1] = label;
  e->tfdswp->value[1] = label;
  e->tfdppp->value[1] = label;
  e->tfdpppp->value[1] = label;

  e->tfdsppp->value[2] = dir;
  e->tfdspp->value[2] = dir;
  e->tfdsww->value[2] = dir;
  e->tfdswp->value[2] = dir;
  e->tfdppp->value[2] = dir;
  e->tfdpppp->value[2] = dir;

  e->tfdsppp->value[3] = dist;
  e->tfdspp->value[3] = dist;
  e->tfdsww->value[3] = dist;
  e->tfdswp->value[3] = dist;

  e->tfdppp->value[3] = sblP;
  e->tfdpppp->value[3] = sblP;


  /*        dep                       gov                       sbl        */
  /* (John, John, NNP) <- SBJ - (ate, eat, VB) - OBJ -> (apple, apple, NN) */

  /* (X, X, NNP) <- SBJ - (X, X, VB) - X -> (X, X, NN) */
  e->tfdsppp->value[0] = _f30;
  e->tfdsppp->value[4] = govP;
  e->tfdsppp->value[5] = depP;
  e->tfdsppp->value[6] = sblP;
  fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_7(e->tfdsppp);

  /* (X, X, X) <- SBJ - (X, X, VB) - X -> (X, X, NN) */
  e->tfdspp->value[0] = _f31;
  e->tfdspp->value[4] = govP;
  e->tfdspp->value[5] = sblP;
  fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdspp);

  /* (X, X, NNP) <- SBJ - (X, X, X) - X -> (X, X, NN) */
  e->tfdspp->value[0] = _f32;
  e->tfdspp->value[4] = depP;
  e->tfdspp->value[5] = sblP;
  fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdspp);

  if(ctx->use_full_forms){
    /* (X, X, X) <- SBJ - (ate, X, X) - X -> (apple, X, X) */
    e->tfdsww->value[0] = _f33;
    e->tfdsww->value[4] = govF;
    e->tfdsww->value[5] = sblF;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdsww);

    /* (John, X, X) <- SBJ - (X, X, X) - X -> (apple, X, X) */
    e->tfdsww->value[0] = _f34;
    e->tfdsww->value[4] = depF;
    e->tfdsww->value[5] = sblF;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdsww);

    /* (X, X, X) <- SBJ - (X, X, VB) - X -> (apple, X, X) */
    e->tfdswp->value[0] = _f35;
    e->tfdswp->value[4] = sblF;
    e->tfdswp->value[5] = govP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdswp);

    /* (X, X, NNP) <- SBJ - (X, X, X) - X -> (apple, X, X) */
    e->tfdswp->value[0] = _f36;
    e->tfdswp->value[4] = sblF;
    e->tfdswp->value[5] = depP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdswp);

    /* (X, X, X) <- SBJ - (ate, X, X) - X -> (X, X, P) */
    e->tfdswp->value[0] = _f37;
    e->tfdswp->value[4] = govF;
    e->tfdswp->value[5] = sblP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdswp);

    /* (John, X, X) <- SBJ - (X, X, X) - X -> (X, X, P) */
    e->tfdswp->value[0] = _f38;
    e->tfdswp->value[4] = depF;
    e->tfdswp->value[5] = sblP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdswp);
  }

  // sibling word only could be tried
  // lemmas
  if(ctx->use_lemmas){
    /* f[97..102] */
    
    /* (X, X, X) <- SBJ - (X, eat, X) - X -> (X, apple, X) */
    e->tfdsww->value[0] = _f33l;
    e->tfdsww->value[4] = govL;
    e->tfdsww->value[5] = sblL;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdsww);

    /* (X, John, X) <- SBJ - (X, X, X) - X -> (X, apple, X) */
    e->tfdsww->value[0] = _f34l;
    e->tfdsww->value[4] = depL;
    e->tfdsww->value[5] = sblL;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdsww);

    /* (X, X, X) <- SBJ - (X, X, VB) - X -> (X, apple, X) */
    e->tfdswp->value[0] = _f35l;
    e->tfdswp->value[4] = sblL;
    e->tfdswp->value[5] = govP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdswp);

    /* (X, X, NNP) <- SBJ - (X, X, X) - X -> (X, apple, X) */
    e->tfdswp->value[0] = _f36l;
    e->tfdswp->value[4] = sblL;
    e->tfdswp->value[5] = depP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdswp);

    /* (X, X, X) <- SBJ - (X, eat, X) - X -> (X, X, NN) */
    e->tfdswp->value[0] = _f37l;
    e->tfdswp->value[4] = govL;
    e->tfdswp->value[5] = sblP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdswp);

    /* (X, John, X) <- SBJ - (X, X, X) - X -> (X, X, NN) */
    e->tfdswp->value[0] = _f38l;
    e->tfdswp->value[4] = depL;
    e->tfdswp->value[5] = sblP;
    fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdswp);
  }

  /* linear features */
  /* (depPm1, depP, depPp1) <-L- (govPm1, govP, govPp1) -X-> (sblPm1, sblP, sblPp1) */

  /* (X, X, X) <-L- (X, govP, X) -X-> (X, sblP, sblPp1) */
  e->tfdppp->value[0] = _f58;
  e->tfdppp->value[4] = sblPp1;
  e->tfdppp->value[5] = govP;
  fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdppp);

  /* (X, X, X) <-L- (X, govP, X) -X-> (sblPm1, sblP, X) */
  e->tfdppp->value[0] = _f59;
  e->tfdppp->value[4] = sblPm1;
  // e->tfdppp->value[5] = govP;
  fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdppp);

  /* (X, X, X) <-L- (X, govP, govPp1) -X-> (X, sblP, X) */
  e->tfdppp->value[0] = _f60;
  e->tfdppp->value[4] = govPp1;
  // e->tfdppp->value[5] = govP;
  fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdppp);

  /* (X, X, X) <-L- (govPm1, govP, X) -X-> (X, sblP, X) */
  e->tfdppp->value[0] = _f61;
  e->tfdppp->value[4] = govPm1;
  // e->tfdppp->value[5] = govP;
  fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdppp);

  /* different from paper: no govP in paper */
  /* (X, X, X) <-L- (govPm1, govP, X) -X-> (X, sblP, sblPp1) */
  e->tfdpppp->value[0] = _f62;
  e->tfdpppp->value[4] = sblPp1;
  e->tfdpppp->value[5] = govPm1;
  e->tfdpppp->value[6] = govP;
  fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_7(e->tfdpppp);

  /* different from paper: no govP in paper */
  /* (X, X, X) <-L- (govPm1, govP, X) -X-> (sblPm1, sblP, X) */
  e->tfdpppp->value[0] = _f63;
  e->tfdpppp->value[4] = sblPm1;
  e->tfdpppp->value[5] = govPm1;
  e->tfdpppp->value[6] = govP;
  fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_7(e->tfdpppp);

  /* different from paper: no govPp1 in paper */
  /* (X, X, X) <-L- (X, govP, govPp1) -X-> (X, sblP, sblPp1) */
  e->tfdpppp->value[0] = _f64;
  e->tfdpppp->value[4] = sblPp1;
  e->tfdpppp->value[5] = govPp1;
  e->tfdpppp->value[6] = govP;
  fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_7(e->tfdpppp);

  /* both in paper and here  */
  /* (X, X, X) <-L- (X, govP, govPp1) -X-> (sblPm1, sblP, X) */
  e->tfdpppp->value[0] = _f65;
  e->tfdpppp->value[4] = sblPm1;
  e->tfdpppp->value[5] = govPp1;
  e->tfdpppp->value[6] = govP;
  fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_7(e->tfdpppp);

  /* (X, depP, X) <-L- (X, X, X) -X-> (X, sblP, sblPp1) */
  e->tfdppp->value[0] = _f66;
  e->tfdppp->value[4] = sblPp1;
  e->tfdppp->value[5] = depP;
  fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdppp);

  /* (X, depP, X) <-L- (X, X, X) -X-> (sblPm1, sblP, X) */
  e->tfdppp->value[0] = _f67;
  e->tfdppp->value[4] = sblPm1;
  e->tfdppp->value[5] = depP;
  fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdppp);

  /* different from paper: no label in paper */
  /* (X, depP, depPp1) <-L- (X, X, X) -X-> (X, sblP, X) */
  e->tfdppp->value[0] = _f68;
  e->tfdppp->value[4] = depPp1;
  e->tfdppp->value[5] = depP;
  fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdppp);

  /* different from paper: no label in paper */
  /* (depPm1, depP, X) <-L- (X, X, X) -X-> (X, sblP, X) */
  e->tfdppp->value[0] = _f69;
  e->tfdppp->value[4] = depPm1;
  e->tfdppp->value[5] = depP;
  fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_6(e->tfdppp);

  /* different from paper: no label in paper */
  /* (depPm1, depP, X) <-L- (X, X, X) -X-> (X, sblP, sblPp1) */
  e->tfdpppp->value[0] = _f70;
  e->tfdpppp->value[4] = sblPp1;
  e->tfdpppp->value[5] = depP;
  e->tfdpppp->value[6] = depPm1;
  fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_7(e->tfdpppp);

  /* different from paper: no label in paper */
  /* (depPm1, depP, X) <-L- (X, X, X) -X-> (sblPm1, sblP, X) */
  e->tfdpppp->value[0] = _f71;
  e->tfdpppp->value[4] = sblPm1;
  e->tfdpppp->value[5] = depP;
  e->tfdpppp->value[6] = depPm1;
  fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_7(e->tfdpppp);

  /* different from paper: no label in paper */
  /* (X, depP, depPp1) <-L- (X, X, X) -X-> (X, sblP, sblPp1) */
  e->tfdpppp->value[0] = _f72;
  e->tfdpppp->value[4] = sblPp1;
  e->tfdpppp->value[5] = depP;
  e->tfdpppp->value[6] = depPp1;
  fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_7(e->tfdpppp);

  /* different from paper: no label in paper */
  /* (X, depP, depPp1) <-L- (X, X, X) -X-> (sblPm1, sblP, X) */
  e->tfdpppp->value[0] = _f73;
  e->tfdpppp->value[4] = sblPm1;
  e->tfdpppp->value[5] = depP;
  e->tfdpppp->value[6] = depPp1;
  fv->array[(fv->elt_nb)++] = maca_graph_parser_encode_feature_7(e->tfdpppp);

  /* _f75, _f76: morphological features (fields FEAT and PFEAT of the CONLL format) */
  /*
    if (feats == null)
    return;

    feature_t l;
    
    short[] featsP = feats[dep];
    short[] featsSbl = sblng != -1 ? feats[sblng] : null;

    dlf.v0 = _f75;
    dlf.v1 = label;
    dlf.v2 = sblP;
    dlf.v3 = depP;
    extractFeat(f, dir, featsP, featsSbl);

    featsP = feats[gov];
    featsSbl = sblng != -1 ? feats[sblng] : null;

    dlf.v0 = _f76;
    dlf.v1 = label;
    dlf.v2 = govP;
    dlf.v3 = sblP;
    if (featsP != null && featsSbl != null) {
    for (short i1 = 0; i1 < featsP.length; i1++) {
    for (short i2 = 0; i2 < featsSbl.length; i2++) {
    dlf.v4 = featsP[i1];
    dlf.v5 = featsSbl[i2];
    l = maca_graph_parser_encode_feature_6(dlf);
    l = dlf.maca_graph_parser_encode_feature_s(e->s_dir, gov < sblng ? 1 : 2, l);
    fv->array[(fv->elt_nb)++] = l2i(l);
    }
    }
    } else if (featsP == null && featsSbl != null) {

    for (short i2 = 0; i2 < featsSbl.length; i2++) {
    dlf.v4 = nofeat;
    dlf.v5 = featsSbl[i2];
    l = maca_graph_parser_encode_feature_6(dlf);
    l = dlf.maca_graph_parser_encode_feature_s(e->s_dir, dir, l);
    fv->array[(fv->elt_nb)++] = l2i(l);
    }

    } else if (featsP != null && featsSbl == null) {

    for (short i1 = 0; i1 < featsP.length; i1++) {
    dlf.v4 = featsP[i1];
    dlf.v5 = nofeat;
    l = maca_graph_parser_encode_feature_6(dlf);
    l = dlf.maca_graph_parser_encode_feature_s(e->s_dir, dir, l);
    fv->array[(fv->elt_nb)++] = l2i(l);
    }
    }*/

  return fv;

}






