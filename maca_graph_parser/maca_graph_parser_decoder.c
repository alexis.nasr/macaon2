/*******************************************************************************
    Copyright (C) 2012 by Alexis Nasr <alexis.nasr@lif.univ-mrs.fr>
                          Ghasem Mirroshandel <ghasem.mirroshandel@lif.univ-mrs.fr>
    This file is part of maca_graph_parser.

    maca_graph_parser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    maca_graph_parser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with maca_graph_parser. If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "maca_graph_parser.h"
#include"maca_graph_parser_feature_table.h"
#include"maca_graph_parser_decoder.h"
#include"maca_graph_parser_hyperdecoder.h"



void maca_graph_parser_decoder_cleanup(maca_graph_parser_ctx *ctx,  maca_graph_parser_sentence *s)
{
  if(ctx->k){
    maca_graph_parser_hyperdecoder_cleanup(ctx, s);
  } else {
    if(ctx->order == 1){
      maca_graph_parser_decoder1_cleanup(ctx, s);
    } else {
      maca_graph_parser_decoder2_cleanup(ctx, s);
    }
  }
}
  
/*-------------------------------------------------------------------------------------------*/

void maca_graph_parser_decoder_parse(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s){
  /**
   * Parse sentence s.
   */

  /* extract features */
  /*if(ctx->store_in_feature_table){
    if(ctx->verbose_flag > 1){
      maca_msg(ctx->module, MACA_MESSAGE);
      if(ctx->ms)
	fprintf(stderr, "computing features for sentence %s\n", ctx->ms->id_sentence);
      else
	fprintf(stderr, "computing features for sentence\n");
    }
    maca_graph_parser_feature_table_fill(ctx, s, ctx->feature_table);
    
    if(ctx->verbose_flag > 1){
      maca_msg(ctx->module, MACA_MESSAGE);
      fprintf(stderr, "done\n");
    }
  }*/

  /* parse */
  /*if(ctx->verbose_flag > 1){
    maca_msg(ctx->module, MACA_MESSAGE);
    if(ctx->ms)
      fprintf(stderr, "parsing sentence %s\n", ctx->ms->id_sentence);
    else
      fprintf(stderr, "parsing sentence\n");
  }*/


  /* extract features */
  maca_graph_parser_feature_table_fill(ctx, s, ctx->feature_table);
  
  maca_graph_parser_decoder_init(ctx, s);
  if(ctx->k){
    maca_graph_parser_hyperdecoder_parse(ctx, s, ctx->feature_table);
  } else {
    if(ctx->order == 1){
      maca_graph_parser_decoder1_parse(ctx, s, ctx->feature_table);
    } else {
      maca_graph_parser_decoder2_parse(ctx, s, ctx->feature_table);
    }
  }

  maca_graph_parser_decoder_cleanup(ctx, s);
  
  if(ctx->verbose_flag > 1){
    maca_msg(ctx->module, MACA_MESSAGE);
    fprintf(stderr, "done\n");
  }

}

/*-------------------------------------------------------------------------------------------*/

void maca_graph_parser_decoder_init(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s)
{
  if(ctx->k){
    maca_graph_parser_hyperdecoder_init(ctx, s);
  } else {
    if(ctx->order == 1){
      maca_graph_parser_decoder1_init(ctx, s);
    } else {
      maca_graph_parser_decoder2_init(ctx, s);
    }
  }
}

/*-------------------------------------------------------------------------------------------*/

Open *alloc_open(float score,
		 int start,
		 int end,
		 int label,
		 int dir,
		 Closed *left,
		 Closed *right)
{
  Open *o = malloc(sizeof(Open));
  if(o == NULL){
    fprintf(stderr, "memory allocation problem\n");
    exit(1);
  }
  o->score = score;
  o->start = start;
  o->end = end;
  o->label = label;
  o->dir = dir;
  o->left = left;
  o->right = right;
  return o;
}

/*-------------------------------------------------------------------------------------------*/

Closed *alloc_closed(  float score,
		       int start,
		       int end,
		       int breakpoint,
		       int dir,
		       Closed *d,
		       Open *u)
{
  Closed *c = malloc(sizeof(Closed));
  if(c == NULL){
    fprintf(stderr, "memory allocation problem\n");
    exit(1);
  }
  c->score = score;
  c->start = start;
  c->end = end;
  c->breakpoint = breakpoint;
  c->dir = dir;
  c->d = d;
  c->u = u;
  return c;
}

/*-------------------------------------------------------------------------------------------*/

void create_closed(Closed *c, maca_graph_parser_sentence *s)
{
  /*
  if(c->dir == la){
    printf("create_closed [%d;(%d);<%d>] from closed [%d;<%d>] and open [%d;<%d>]\n",
	   c->start, c->breakpoint, c->end,
	   (c->d)? c->d->start : -1, (c->d)? c->d->end : -1,
	   (c->u)? c->u->start : -1, (c->u)? c->u->end : -1);
  } else {
    printf("create_closed [<%d>;(%d);%d] from open [<%d>;%d] and closed [<%d>;%d]\n",
	   c->start, c->breakpoint, c->end,
	   (c->u)? c->u->start : -1, (c->u)? c->u->end : -1,
	   (c->d)? c->d->start : -1, (c->d)? c->d->end : -1);
  }
  */
  if(c->d) create_closed(c->d, s);
  if(c->u) create_open(c->u, s);
}

/*-------------------------------------------------------------------------------------------*/

void create_open(Open *o, maca_graph_parser_sentence *s)
{
  /*
  if (o->dir == la){
    printf("create_open [%d;<%d>] from left closed [<%d>;%d] and right closed [%d;<%d>]\n",
	   o->start, o->end,
	   (o->left)? o->left->start : -1, (o->left)? o->left->end : -1,
	   (o->right)? o->right->start : -1, (o->right)? o->right->end : -1);
  } else {
    printf("create_open [<%d>;%d] from left closed [<%d;%d>] and right closed [<%d;%d>]\n",
	   o->start, o->end,
	   (o->left)? o->left->start : -1, (o->left)? o->left->end : -1,
	   (o->right)? o->right->start : -1, (o->right)? o->right->end : -1);
  }
  */
  
  /* 1-best gov and label */
  if (o->dir == la) {
    s->gov[o->start] = o->end;
    s->label[o->start] = o->label;
  } 
  else {
    s->gov[o->end] = o->start;
    s->label[o->end] = o->label;
  }
  if (o->left) create_closed(o->left, s);
  if (o->right) create_closed(o->right, s);
}

/*-------------------------------------------------------------------------------------------*/

