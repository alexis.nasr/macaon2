/***************************************************************************
    Copyright (C) 2012 by Alexis Nasr <alexis.nasr@lif.univ-mrs.fr>
                          Ghasem Mirroshandel <ghasem.mirroshandel@lif.univ-mrs.fr>
    This file is part of maca_graph_parser.

    maca_graph_parser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    maca_graph_parser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with maca_graph_parser. If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

#ifndef __MACA_GRAPH_PARSER_DEP_COUNT_TABLE__
#define __MACA_GRAPH_PARSER_DEP_COUNT_TABLE__

#include "maca_graph_parser.h"

#ifdef __cplusplus
extern "C"{
#endif


maca_graph_parser_dep_count_table maca_graph_parser_dep_count_table_allocate(int pos_nb, int synt_labels_nb);
void maca_graph_parser_dep_count_table_free(int pos_nb, int synt_labels_nb, maca_graph_parser_dep_count_table t);
maca_graph_parser_dep_count_table maca_graph_parser_dep_count_table_read(maca_graph_parser_ctx *ctx, char *filename);
void maca_graph_parser_dep_count_table_print(maca_graph_parser_ctx * ctx, char *filename);
void maca_graph_parser_dep_count_table_update(maca_graph_parser_ctx * ctx, maca_graph_parser_sentence *s);
int maca_graph_parser_dep_count_table_compute_length_class(int gov, int dep);
#ifdef __cplusplus
}
#endif


#endif
