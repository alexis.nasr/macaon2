#include "maca_graph_parser_feature_counter.h"


typedef struct {
  feature_counter *basic_feature_counter;
  feature_counter *first_feature_counter;
  feature_counter *grandchildren_feature_counter;
  feature_counter *sibling_feature_counter;
  feature_counter *array[4];
  int size;
  int nb_words; /* nb of words of the sentence (extremely ad-hoc field) */
} feature_counter_array;

#ifdef __cplusplus
extern "C"{
#endif


feature_counter_array *allocate_feature_counter_array(maca_graph_parser_ctx *ctx, int nb_words);
void free_feature_counter_array(feature_counter_array *a);

int feature_counter_array_squared_norm(feature_counter_array *a);
feature_counter_array *feature_counter_array_difference(maca_graph_parser_ctx *ctx, feature_counter_array *a, feature_counter_array *b);
feature_counter_array *extract_features_from_parse_fca(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s, feature_counter_array *a);

  #ifdef __cplusplus
}
#endif
