#include "maca_mcf.hh"
#include "maca_mcf_wrapper.h"
#include <cstring>

extern "C" {
  int maca_mcf_word_get_id(maca_mcf_word *word) {
    return word->getId();
  }

  void maca_mcf_word_add_value_int(maca_mcf_word *word, int column, int value) {
    word->addValueInt(column, value);
  }

  void maca_mcf_word_add_value_float(maca_mcf_word *word, int column, float value) {
    word->addValueFloat(column, value);
  }

  void maca_mcf_word_add_value_string(maca_mcf_word *word, int column, char* value) {
    word->addValueString(column, value);
  }

  void maca_mcf_word_add_value_bool(maca_mcf_word *word, int column, char value) {
    word->addValueBool(column, value);
  }

  char maca_mcf_word_add_value_alphabet(maca_mcf_word *word, int column, char *symbol,
					maca_alphabet *alphabet) {
    std::shared_ptr<macaon::Alphabet> ptr = PtrPool<macaon::Alphabet>::getInstance().get(alphabet);
    try {
      word->addValueAlphabet(column, symbol, ptr);
      return 1;
    } catch (std::exception &e) {
      return 0;
    }
  }

  int maca_mcf_word_get_nb_values(maca_mcf_word *word, int column) {
    return word->getNbValues(column);
  }

  int maca_mcf_word_get_value_int(maca_mcf_word *word, int column, int index) {
    return word->getIntValue(column, index);
  }

  float maca_mcf_word_get_value_float(maca_mcf_word *word, int column, int index) {
    return word->getFloatValue(column, index);
  }

  void maca_mcf_word_get_value_string(maca_mcf_word *word, int column, int index,
				      char *str, int size) {
    strncpy(str, word->getStringValue(column,index).c_str(), size);
  }

  char maca_mcf_word_get_value_bool(maca_mcf_word *word, int column, int index) {
    return word->getBoolValue(column, index);
  }

  void maca_mcf_word_clear_values(maca_mcf_word *word, int column) {
    word->clearValues(column);
  }

  void maca_mcf_word_print(maca_mcf_word *word, FILE *out) {
    word->print(out);
  }

  maca_mcf_sentence *maca_mcf_sentence_new(int nb_columns) {
    std::shared_ptr<macaon::McfSentence> sent(new macaon::McfSentence(nb_columns));
    PtrPool<macaon::McfSentence>::getInstance().accept(sent);

    return sent.get();
  }

  void maca_mcf_sentence_delete(maca_mcf_sentence *sentence) {
    PtrPool<macaon::McfSentence>::getInstance().release(sentence);
  }

  void maca_mcf_sentence_release(maca_mcf_sentence *sentence) {
    maca_mcf_sentence_delete(sentence);
  }

  maca_mcf_word *maca_mcf_sentence_add_word(maca_mcf_sentence *sentence) {
    return &(sentence->addWord());
  }

  maca_mcf_word *maca_mcf_sentence_get_word(maca_mcf_sentence *sentence, int index) {
    return &(sentence->getWord(index));
  }

  void maca_mcf_sentence_clear(maca_mcf_sentence *sentence) {
    sentence->clear();
  }

  int maca_mcf_sentence_get_length(maca_mcf_sentence *sentence) {
    return sentence->length;
  }
  
  void maca_mcf_sentence_print(maca_mcf_sentence *sentence, FILE *out) {
    sentence->print(out);
  }

  maca_mcf_sentences *maca_mcf_sentences_new() {
    std::shared_ptr<macaon::McfSentences> sents(new macaon::McfSentences());
    PtrPool<macaon::McfSentences>::getInstance().accept(sents);
    
    return sents.get();
  }

  void maca_mcf_sentences_delete(maca_mcf_sentences *sentences) {
    PtrPool<macaon::McfSentences>::getInstance().release(sentences);
  }

  void maca_mcf_sentences_release(maca_mcf_sentences *sentences) {
    maca_mcf_sentences_release(sentences);
  }

  void maca_mcf_sentences_add_sentence(maca_mcf_sentences *sentences, maca_mcf_sentence *sentence) {
    std::shared_ptr<macaon::McfSentence> ptr = 
      PtrPool<macaon::McfSentence>::getInstance().get(sentence);
    sentences->addSentence(ptr);
  }

  maca_mcf_sentence *maca_mcf_sentences_get_sentence(maca_mcf_sentences *sentences, int index) {
    std::shared_ptr<macaon::McfSentence> ptr = sentences->getSentence(index);
    
    return PtrPool<macaon::McfSentence>::getInstance().accept(ptr);
  }
  
  int maca_mcf_sentences_size(maca_mcf_sentences *sentences) {
    return sentences->size();
  }

  void maca_mcf_sentences_print(maca_mcf_sentences *sentences, FILE *out) {
    sentences->print(out);
  }

  void maca_mcf_column_get_name(maca_mcf_column *column, char *name, int size) {
    strncpy(name, column->name.c_str(), size);
  }

  void maca_mcf_column_get_type(maca_mcf_column *column, char *type, int size) {
    strncpy(type, column->typeName.c_str(), size);
  }

  maca_alphabet *maca_mcf_column_get_alphabet(maca_mcf_column *column) {
    return PtrPool<macaon::Alphabet>::getInstance().accept(column->alphabet);
  }
  
  void maca_mcf_column_set_alphabet(maca_mcf_column *column, maca_alphabet *alphabet) {
    std::shared_ptr<macaon::Alphabet> ptr = PtrPool<macaon::Alphabet>::getInstance().get(alphabet);
    column->setAlphabet(ptr);
  }

  maca_mcf *maca_mcf_new(char *input_filename) {
    std::shared_ptr<macaon::Mcf> format(new macaon::Mcf(input_filename));
    PtrPool<macaon::Mcf>::getInstance().accept(format);
    
    return format.get();
  }

  maca_mcf *maca_mcf_new_with_alphabets(char *input_filename, char *alphabet_filename) {
    std::shared_ptr<macaon::Mcf> format(new macaon::Mcf(input_filename, alphabet_filename));
    PtrPool<macaon::Mcf>::getInstance().accept(format);
    
    return format.get();
  }

  maca_mcf *maca_mcf_new_with_alphabet_array(char *input_filename, maca_alphabet_array *array) {
    std::shared_ptr<macaon::Mcf> format(new macaon::Mcf(input_filename, *array));
    PtrPool<macaon::Mcf>::getInstance().accept(format);

    return format.get();
  }

  void maca_mcf_delete(maca_mcf *format) {
    PtrPool<macaon::Mcf>::getInstance().release(format);
  }
  
  int maca_mcf_input(maca_mcf *format, char *column_name) {
    try {
      return format->input(column_name);
    } catch (std::exception &e) {
      return -1;
    }
  }

  int maca_mcf_output(maca_mcf *format, char *column_name, char *column_type) {
    try {
      return format->output(column_name, column_type);
    } catch (std::exception &e) {
      return -1;
    }
  }
  
  maca_mcf_column *maca_mcf_get_column_info(maca_mcf *format, int id) {
    try {
      return &(format->getColumnInfo(id));
    } catch (std::exception &e) {
      return NULL;
    }
  }

  maca_mcf_column *maca_mcf_get_column_info_by_name(maca_mcf *format, char *name) {
    try {
      return &(format->getColumnInfo(name));
    } catch (std::exception &e) {
      return NULL;
    }
  }
  
  maca_mcf_sentence *maca_mcf_read_next_sentence(maca_mcf *format) {
    std::shared_ptr<macaon::McfSentence> ptr = format->readNextSentence();
    return PtrPool<macaon::McfSentence>::getInstance().accept(ptr);
  }

  maca_mcf_sentences *maca_mcf_read_sentences(maca_mcf *format) {
    std::shared_ptr<macaon::McfSentences> ptr = format->readSentences();
    return PtrPool<macaon::McfSentences>::getInstance().accept(ptr);
  }

  void maca_mcf_print_header(maca_mcf *format, FILE *out) {
    format->printHeader(out);
  }

  char maca_mcf_dump_alphabets(maca_mcf *format, char *output_filename) {
    try {
      format->dumpAlphabets(output_filename);
      return 1;
    } catch (std::exception &e) {
      return 0;
    }
  }
}
