/*******************************************************************************
    Copyright (C) 2012 by Alexis Nasr <alexis.nasr@lif.univ-mrs.fr>
                          Ghasem Mirroshandel <ghasem.mirroshandel@lif.univ-mrs.fr>
    This file is part of maca_graph_parser.

    maca_graph_parser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    maca_graph_parser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with maca_graph_parser. If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#include<stdlib.h>
#include <string.h>
#include"maca_graph_parser_feature_table.h"
#include"maca_graph_parser_features.h"
#include"maca_graph_parser.h"
#include"maca_graph_parser_feature_vector.h"
#include"maca_graph_parser_model.h"
#include "maca_graph_parser_dep_count_table.h"


/* compute on the fly the score of a basic configuration */

feat_vector *maca_graph_parser_basic_score(int gov, int dep, maca_graph_parser_ctx *ctx, feat_vector *fv, float *score) 
{
  fv = basic(ctx->s, ctx, gov, dep, fv);
  *score = score_feat_vector(fv, ctx->model);
   return fv;
}

/* compute on the fly the score of a first order configuration */

feat_vector *maca_graph_parser_first_score(int gov, int dep, int label, maca_graph_parser_ctx *ctx, feat_vector *fv, float *score) 
{
  fv = first(ctx->s, ctx, gov, dep, label, fv);
  *score = score_feat_vector(fv, ctx->model);
  return fv;
}

/* compute on the fly the score of a grandchildren configuration */

feat_vector *maca_graph_parser_grandchildren_score(int gov, int dep, int gdep, int label, maca_graph_parser_ctx *ctx, feat_vector *fv, float *score) 
{
  fv = grandchildren(ctx->s, ctx, gov, dep, gdep, label, fv);
  *score = score_feat_vector(fv, ctx->model);
  return fv;
}

/* compute on the fly the score of a sibling configuration */

feat_vector *maca_graph_parser_sibling_score(int gov, int dep, int sib, int label, maca_graph_parser_ctx *ctx, feat_vector *fv, float *score) 
{
  fv = sibling(ctx->s, ctx, gov, dep, sib, label, fv);
  *score = score_feat_vector(fv, ctx->model);
  return fv;
}


void maca_graph_parser_feature_table_fill(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s, maca_graph_parser_feature_table *d){

  feat_vector *fv_basic = NULL;
  feat_vector *fv_first = NULL;
  feat_vector *fv_grandchildren = NULL;
  feat_vector *fv_sibling = NULL;

  /* int labels_nb = ctx->labels_nb;  */
  /* default edge label: joker */
  int dft_label = ctx->fct_joker;

  int w1,w2,w3,label,dir,gov,dep;
  float score;
  char s_na[3] = "NA";

  int length_class; /* length class of the dependency beween gov and dep */

  if(ctx->verbose_flag > 2){
    maca_msg(ctx->module, MACA_MESSAGE);
    fprintf(stderr, "computing first order features\n");
  }


  for(w1 = 0; w1 < s->l; w1++){
     for(w2 = w1+1; w2 < s->l; w2++){
       for(dir = 0; dir < 2; dir++){
	 gov = (dir == ra) ? w1 : w2;
	 dep = (dir == ra) ? w2 : w1;
	 length_class = maca_graph_parser_dep_count_table_compute_length_class(gov, dep);
	 /* basic */
	 if(ctx->basic_features){ 
	   fv_basic = basic(s, ctx, gov, dep, fv_basic);
	   score = score_feat_vector(fv_basic, ctx->model);
	   d->pl[w1][w2][dir] = score;
	   
	   if(ctx->verbose_flag > 3){
	     char bgov[128];
	     char bdep[128];
	     maca_alphabet_get_symbol(ctx->words_alphabet, s->words[gov], bgov, sizeof(bgov));
	     maca_alphabet_get_symbol(ctx->words_alphabet, s->words[dep], bdep, sizeof(bdep));
	     maca_msg(ctx->module, MACA_MESSAGE);
	     fprintf(stderr, "basic (%s, %s) ", bgov, bdep);
	     fprintf(stderr, "%d features, score = %f\n", fv_basic->elt_nb, score);
	   }
	 } /* end basic */

	 for(label = 0; label < ctx->labels_nb; label++){
	   /* printf("pos w1 = %d w2 = %d pos w2 = %d label = %d \n", s->pos[w1], w2, s->pos[w2], label ); */
	   if((ctx->dep_count_table[s->pos[gov]][s->pos[dep]][label][length_class][dir] >= ctx->min_dep_count) ||
	       (label == dft_label)){
	     /* first */
	     if(ctx->first_features){ 
	       fv_first = first(s, ctx, gov, dep, label, fv_first);
	       score = score_feat_vector(fv_first, ctx->model);
	       d->lab[w1][w2][label][dir] = score;
	       
	       if(ctx->verbose_flag > 3){
		 char bw1[128];
		 char bw2[128];
		 char bpos1[128];
		 char bpos2[128];
		 char blabel[128];
		 char blem1[128];
		 char blem2[128];
		 maca_alphabet_get_symbol(ctx->words_alphabet, s->words[w1], bw1, sizeof(bw1));
		 if (s->lemmas[w1] != -1) {
		   maca_alphabet_get_symbol(ctx->words_alphabet, s->lemmas[w1], blem1, sizeof(blem1));
		 } else {
		   strcpy(blem1, s_na);
		 }
		 maca_alphabet_get_symbol(ctx->pos_alphabet, s->pos[w1], bpos1, sizeof(bpos1));
		 maca_alphabet_get_symbol(ctx->labels_alphabet, label, blabel, sizeof(blabel));
		 maca_alphabet_get_symbol(ctx->words_alphabet, s->words[w2], bw2, sizeof(bw2));
		 if (s->lemmas[w2] != -1) {
		   maca_alphabet_get_symbol(ctx->words_alphabet, s->lemmas[w2], blem2, sizeof(blem2));
		 } else {
		   strcpy(blem2, s_na);
		 }
		 maca_alphabet_get_symbol(ctx->pos_alphabet, s->pos[w2], bpos2, sizeof(bpos2));
		 maca_msg(ctx->module, MACA_MESSAGE);
		 fprintf(stderr, "first (%s,%s,%s) %s-%s-%s, (%s,%s,%s) ", 
			 bw1, blem1, bpos1,
			 (dir == ra) ? "" : "<", blabel, (dir == ra) ? ">" : "",
			 bw2, blem2, bpos2);
		 fprintf(stderr, "%d features, score = %f\n", fv_first->elt_nb, score);
	       }
	     } /* end first */
	   } /* min_dep_count pruning */
	 } /* end label */

       } /* end dir */
     } /* end for w2 */
  } /* end for w1 */


  //GHASEM: in generating grandchildren information, we need w3 to be inside and outside of the
  //range between w1 and w2!!
  //for sibling the situation won't be changed, and w3 will be just inside the range!!
  if(ctx->verbose_flag > 2){
    maca_msg(ctx->module, MACA_MESSAGE);
    fprintf(stderr, "computing second order features\n");
  }

  
  if(ctx->sibling_features || ctx->grandchildren_features){
    for(w1 = 0; w1 < s->l; w1++){
      for(w2 = w1+1; w2 < s->l; w2++){

	length_class = maca_graph_parser_dep_count_table_compute_length_class(w1, w2);

	for(label = 0; label < ctx->labels_nb; label++){
	  /* ra */
	  if((ctx->dep_count_table[s->pos[w1]][s->pos[w2]][label][length_class][ra] >= ctx->min_dep_count) ||
	     (label == dft_label)){
	    /* for projectivity to be enforced, w3 cannot be before w1 */
	    for(w3 = w1; w3 < s->l; w3++){ //GHASEM    OLD -->  for(w3 = w1; w3 <= w2; w3++){
	      
	      /* grandchildren */
	      if(ctx->grandchildren_features){
		if ((w3 != w1) && (w3 != w2))
		  fv_grandchildren = grandchildren(s, ctx, w1, w2, w3, label, fv_grandchildren);
		else
		  fv_grandchildren = grandchildren(s, ctx, w1, w2, -1, label, fv_grandchildren);
		score = score_feat_vector(fv_grandchildren, ctx->model);
		d->gra[w1][w2][w3][ra][label] = score;
		
		if(ctx->verbose_flag > 3){
		  char bw1[128];
		  char bw2[128];
		  char bw3[128];
		  char blabel[128];
		  maca_alphabet_get_symbol(ctx->words_alphabet, s->words[w1], bw1, sizeof(bw1));
		  maca_alphabet_get_symbol(ctx->words_alphabet, s->words[w2], bw2, sizeof(bw2));
		  maca_alphabet_get_symbol(ctx->words_alphabet, s->words[w3], bw2, sizeof(bw3));
		  maca_alphabet_get_symbol(ctx->words_alphabet, label, blabel, sizeof(blabel));
		  maca_msg(ctx->module, MACA_MESSAGE);
		  fprintf(stderr, "grand children ([%s], %s, %s, %s) ", bw1, blabel, bw2, bw3);
		  fprintf(stderr, "%d features, score = %f\n", fv_grandchildren->elt_nb, score);
		}
	      }
	      /* end grandchildren */

	      /* siblings */
	      //		  if((w1 <= w3) && (w3 <= w2)){//GHASEM : here I just checked if w3 is between w1 and w2 or not?
	      if((ctx->sibling_features) && (w3 <= w2)){
		if ((w3 != w1) && (w3 != w2))
		  fv_sibling = sibling(s, ctx, w1, w2, w3, label, fv_sibling);
		else
		  fv_sibling = sibling(s, ctx, w1, w2, -1, label, fv_sibling);
		score = score_feat_vector(fv_sibling, ctx->model);
		d->sib[w1][w2][w3][ra][label] = score;
		
		if(ctx->verbose_flag > 3){
		  char bw1[128];
		  char bw2[128];
		  char bw3[128];
		  char blabel[128];
		  maca_alphabet_get_symbol(ctx->words_alphabet, s->words[w1], bw1, sizeof(bw1));
		  maca_alphabet_get_symbol(ctx->words_alphabet, s->words[w2], bw2, sizeof(bw2));
		  maca_alphabet_get_symbol(ctx->words_alphabet, s->words[w3], bw2, sizeof(bw3));
		  maca_alphabet_get_symbol(ctx->words_alphabet, label, blabel, sizeof(blabel));
		  maca_msg(ctx->module, MACA_MESSAGE);
		  fprintf(stderr, "sibling ([%s], %s, %s, %s) ", bw1, blabel, bw2, bw3);
		  fprintf(stderr, "%d features, score = %f\n", fv_sibling->elt_nb, score);
		}
	      }
	      /* end siblings */
	    } /* end for w3 */
	  } /* end ra */

	  /* la */
	  if((ctx->dep_count_table[s->pos[w2]][s->pos[w1]][label][length_class][la] >= ctx->min_dep_count) ||
	     (label == dft_label)){
	    /* for projectivity to be enforced, w3 cannot be after w2 */
	    for(w3 = 0; w3 <= w2; w3++){ //GHASEM    OLD -->  for(w3 = w1; w3 <= w2; w3++){
	      /* grandchildren */
	      if(ctx->grandchildren_features){ //  && (w3 < w2)
		if ((w3 != w1) && (w3 != w2))
		  fv_grandchildren = grandchildren(s, ctx, w2, w1, w3, label, fv_grandchildren);
		else
		  fv_grandchildren = grandchildren(s, ctx, w2, w1, -1, label, fv_grandchildren);
		score = score_feat_vector(fv_grandchildren, ctx->model);
		d->gra[w1][w2][w3][la][label] = score;
		
		if(ctx->verbose_flag > 3){
		  char bw1[128];
		  char bw2[128];
		  char bw3[128];
		  char blabel[128];
		  maca_alphabet_get_symbol(ctx->words_alphabet, s->words[w1], bw1, sizeof(bw1));
		  maca_alphabet_get_symbol(ctx->words_alphabet, s->words[w2], bw2, sizeof(bw2));
		  maca_alphabet_get_symbol(ctx->words_alphabet, s->words[w3], bw2, sizeof(bw3));
		  maca_alphabet_get_symbol(ctx->words_alphabet, label, blabel, sizeof(blabel));
		  maca_msg(ctx->module, MACA_MESSAGE);
		  fprintf(stderr, "grand children (%s, %s, [%s], %s) ", bw1, blabel, bw2, bw3);
		  fprintf(stderr, "%d features, score = %f\n", fv_grandchildren->elt_nb, score);
		}
	      }
	      /* end grandchildren */
	      
	      /* siblings */
	      // GHASEM : here I just checked if w3 is between w1 and w2 or not?
	      if((ctx->sibling_features) && (w3 >= w1)){
		if ((w3 != w1) && (w3 != w2))
		  fv_sibling = sibling(s, ctx, w2, w1, w3, label, fv_sibling);
		else
		  fv_sibling = sibling(s, ctx, w2, w1, -1, label, fv_sibling);
		score = score_feat_vector(fv_sibling, ctx->model);
		d->sib[w1][w2][w3][la][label] = score;
		
		if(ctx->verbose_flag > 3){
		  char bw1[128];
		  char bw2[128];
		  char bw3[128];
		  char blabel[128];
		  maca_alphabet_get_symbol(ctx->words_alphabet, s->words[w1], bw1, sizeof(bw1));
		  maca_alphabet_get_symbol(ctx->words_alphabet, s->words[w2], bw2, sizeof(bw2));
		  maca_alphabet_get_symbol(ctx->words_alphabet, s->words[w3], bw2, sizeof(bw3));
		  maca_alphabet_get_symbol(ctx->words_alphabet, label, blabel, sizeof(blabel));
		  maca_msg(ctx->module, MACA_MESSAGE);
		  fprintf(stderr, "sibling (%s, %s, [%s], %s) ", bw1, blabel, bw2, bw3);
		  fprintf(stderr, "%d features, score = %f\n", fv_sibling->elt_nb, score);
		}
	      }
	      /* end siblings */

	    } /* end for w3 */
	  } /* end la */
	} /* end for label */
      } /* end for w2 */
    } /* end for w1 */
  } /* end second order features */

  /* house cleaning */
  if(fv_basic){
    free_feat_vector(fv_basic);
    fv_basic = NULL;
  }
  if(fv_first){
    free_feat_vector(fv_first);
    fv_first = NULL;
  }
  if(fv_grandchildren){
    free_feat_vector(fv_grandchildren);
    fv_grandchildren = NULL;
  }
  if(fv_sibling){
    free_feat_vector(fv_sibling);
    fv_sibling = NULL;
  }

}



/*-------------------------------------------------------------------------------------------*/


void maca_graph_parser_feature_table_free(maca_graph_parser_ctx *ctx)
{
  /* int i,j,k,l; */
  maca_graph_parser_feature_table *d = ctx->feature_table;
  /* int length = d->len; */
  /* int types = d->typesLen; */

  if(ctx->basic_features){
    free(d->pl[0][0]);
    free(d->pl[0]);
    free(d->pl);
  }
  
  if(ctx->first_features){
    free(d->lab[0][0][0]);
    free(d->lab[0][0]);
    free(d->lab[0]);
    free(d->lab);
  }

  if(ctx->grandchildren_features){
    free(d->gra[0][0][0][0]);
    free(d->gra[0][0][0]);
    free(d->gra[0][0]);
    free(d->gra[0]);
    free(d->gra);
  }

  if(ctx->sibling_features){
    free(d->sib[0][0][0][0]);
    free(d->sib[0][0][0]);
    free(d->sib[0][0]);
    free(d->sib[0]);
    free(d->sib);
  }

  free(d);
}

/*-------------------------------------------------------------------------------------------*/

void maca_graph_parser_feature_table_allocator(maca_graph_parser_ctx *ctx)
{
  maca_graph_parser_feature_table *d = malloc(sizeof(maca_graph_parser_feature_table));
  if(d == NULL){
    fprintf(stderr, "memory allocation error\n");
    exit(1);
  }

  int types = d->typesLen = ctx->labels_nb;
  int length = d->len = ctx->max_sent_length;

  int i,j,k,l;
  d->pl = NULL;
  d->lab = NULL;
  d->sib = NULL;
  d->gra = NULL;

  if(ctx->basic_features){
    d->pl = malloc((size_t) length * sizeof(float **));
    d->pl[0] = malloc((size_t) (length * length) * sizeof(float *));
    d->pl[0][0] = malloc((size_t) (length * length * 2) * sizeof(float));
    for(i=0; i<length; i++){
      d->pl[i] = d->pl[0] + (i * length);
      for(j=0; j<length; j++){
	d->pl[i][j] = d->pl[0][0] + ((i * length) + j) * 2;
      }
    }
  }

  if(ctx->first_features){
    /* [start][end][label][dir] */
    d->lab = malloc((size_t) length * sizeof(float ***));
    d->lab[0] = malloc((size_t) (length * length) * sizeof(float **));
    d->lab[0][0] = malloc((size_t) (length * length * types * sizeof(float *)));
    d->lab[0][0][0] = malloc((size_t) (length * length * types * 2 * sizeof(float)));
    for(i=0; i<length; i++){
      d->lab[i] = d->lab[0] + (i * length); 
      for(j=0; j<length; j++){
	d->lab[i][j] = d->lab[0][0] + ((i * length) + j) * types;
	for(k=0; k<types; k++){
	  d->lab[i][j][k] = d->lab[0][0][0] + (((i * length) + j) * types + k) * 2;
	}
      }
    }
  }

  if(ctx->sibling_features){
    /* [gov][dep][sib][dir][label] */
    d->sib = malloc((size_t) length * sizeof(float ****));
    d->sib[0] = malloc((size_t) (length * length) * sizeof(float ***));
    d->sib[0][0] = malloc((size_t) (length * length * length) * sizeof(float **));
    d->sib[0][0][0] = malloc((size_t) (length * length * length * 2) * sizeof(float *));
    d->sib[0][0][0][0] = malloc((size_t) (length * length * length * 2 * types) * sizeof(float));
    for(i=0; i<length; i++){
      d->sib[i] = d->sib[0] + (i * length);
      for(j=0; j<length; j++){
	/* for(j=i+1; j<length; j++){ */ /* MM: start at i+1 because siblings are used in open only and open cannot have span 0 */
	d->sib[i][j] = d->sib[0][0] + (i * length + j) * length;
	for(k=0; k<length; k++){
	/* for(k=i; k<=j; k++){ */ /* MM: sib in [i..j] */
	  d->sib[i][j][k] = d->sib[0][0][0] + ((i * length + j) * length + k) * 2;
	  for(l=0; l<2; l++){
	    d->sib[i][j][k][l] = d->sib[0][0][0][0] + (((i * length + j) * length + k) * 2 + l) * types;
	  }
	}
      }
    }
  }

  if(ctx->grandchildren_features){
    /* [gov][dep][gra][dir][label] */
    d->gra = malloc((size_t) length * sizeof(float ****));
    d->gra[0] = malloc((size_t) (length * length) * sizeof(float ***));
    d->gra[0][0] = malloc((size_t) (length * length * length) * sizeof(float **));
    d->gra[0][0][0] = malloc((size_t) (length * length * length * 2) * sizeof(float *));
    d->gra[0][0][0][0] = malloc((size_t) (length * length * length * 2 * types) * sizeof(float));
    for(i=0; i<length; i++){
      d->gra[i] = d->gra[0] + (i * length);
      for(j=0; j<length; j++){
	/* for(j=i; j<length; j++){ */ /* MM: start at i because grandchildren are used in (open and) closed and closed can have span 0 */
	d->gra[i][j] = d->gra[0][0] + (i * length + j) * length;
	for(k=0; k<length; k++){
	  d->gra[i][j][k] = d->gra[0][0][0] + ((i * length + j) * length + k) * 2;
	  for(l=0; l<2; l++){
	    d->gra[i][j][k][l] = d->gra[0][0][0][0] + (((i * length + j) * length + k) * 2 + l) * types;
	  }
	}
      }
    }
  }

  ctx->feature_table = d;
}

/*-------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------*/
