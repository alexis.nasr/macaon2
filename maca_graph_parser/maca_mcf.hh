#ifndef __MACA_MCF_H__
#define __MACA_MCF_H__

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <list>
#include <memory>
#include <cstdio>
#include "maca_alphabet.hh"

#define MCF_COLUMN_TYPE_STRING     "STRING"
#define MCF_COLUMN_TYPE_INTEGER    "INT"
#define MCF_COLUMN_TYPE_FLOAT      "FLOAT"
#define MCF_COLUMN_TYPE_BOOL       "BOOL"

namespace macaon {
  class McfValue {
  public:
    virtual std::string toString() = 0;
    virtual McfValue *clone() const = 0;
  };

  template <class Derived>
  class McfValueDerivationHelper : public McfValue {
  public:
    virtual McfValue* clone() const {
      return new Derived(static_cast<const Derived&>(*this)); // call the copy ctor.
    }
  };

  class McfValueInt: public McfValueDerivationHelper<McfValueInt> {
  public:
    int i;
    
    McfValueInt(int i): i(i) {}

    std::string toString() {
      return std::to_string(i);
    }
  };

  class McfValueFloat: public McfValueDerivationHelper<McfValueFloat> {
  public:
    float f;
    
    McfValueFloat(float f): f(f) {}
    
    std::string toString() {
      std::ostringstream ss; /* We have to do that because std::to_string makes us */
      ss << f;               /* lose precision */
      return ss.str();
    }
  };

  class McfValueString: public McfValueDerivationHelper<McfValueString> {
  public:
    std::string s;
    
    McfValueString(std::string s): s(s) {}
    
    std::string toString() {
      return s;
    }
  };

  class McfValueBool: public McfValueDerivationHelper<McfValueBool> {
  public:
    bool b;
    
    McfValueBool(bool b) : b(b) {}

    std::string toString() {
      if (b)
	return "1";
      return "0";
    }
  };

  class McfValueAlphabet: public McfValueInt {
  private:
    std::shared_ptr<Alphabet> alphabet;
    
  public:
    McfValueAlphabet(std::string symbol, std::shared_ptr<Alphabet> alphabet): 
      McfValueInt(-1), alphabet(alphabet) {
      
      try{
      i = alphabet->addSymbol(symbol);
      } catch (std::exception e){
	throw std::runtime_error("Can't create alphabet value!");
      }
      
    }

    std::string toString() {
      return alphabet->getSymbol(i);
    }

    virtual McfValue* clone() const {
      return new McfValueAlphabet(static_cast<const McfValueAlphabet&>(*this));
    }
  };
  

  class McfWord {
  private:
    int id;			/**< Index of the word in its sentence */
    std::vector<std::vector<std::unique_ptr<McfValue>>> word; 
    
  public:
    /** 
     * Constructor of McfWord.
     * 
     * @param id the index of the word in its sentence.
     * @param nbColumns the number of columns the word has.
     */
    McfWord(int id, int nbColumns) : id(id), word(nbColumns) {}

    /** 
     * Copy constructor of McfWord.
     * 
     * @param other the other McfWord to copy from.
     */
    McfWord(const McfWord &other);

    /** 
     * Assignement operator of McfWord.
     * 
     * @param other the other McfWord to assign from.
     * 
     * @return the McfWord to assign.
     */
    McfWord &operator =(const McfWord &other);

    /** 
     * Gets the index of the word in its sentence.
     * 
     * 
     * @return the index;
     */
    int getId();

    /** 
     * Adds an integer value to the word at the specified column.
     * 
     * @param column the id of the column.
     * @param value the integer value to add.
     */
    void addValueInt(int column, int value);

    /** 
     * Adds a float value to the word at the specified column.
     * 
     * @param column the id of the column.
     * @param value the float value to add.
     */
    void addValueFloat(int column, float value);

    /** 
     * Adds a string value to the word at the specified column.
     * 
     * @param column the id of the column.
     * @param value the string value to add.
     */    
    void addValueString(int column, std::string value);

    /** 
     * Adds a boolean value to the word at the specified column.
     * 
     * @param column the id of the column.
     * @param value the boolean value to add.
     */
    void addValueBool(int column, bool value);

    /** 
     * Adds a symbol of an alphabet to the word at the specified column.
     * 
     * @param column the id of the column.
     * @param symbol the symbol value to add.
     * @param a the alphabet to use to convert the symbol into an integer.
     *
     * @note once stored in the word, these values act as integers.
     *
     * @see getIntValue()
     * @see getIntValues()
     */
    void addValueAlphabet(int column, std::string symbol, std::shared_ptr<Alphabet> a);

    /** 
     * Gets an integer list of the stored values from the specified column.
     * 
     * @param column the id of the column.
     * 
     * @return a list of integer values.
     *
     * @note only use this method if stored values are integers!
     * @note if performance is a concern, prefer the get*Value() alternatives.
     *
     * @see getIntValue()
     */
    std::list<int> getIntValues(int column);

    /** 
     * Gets a float list of the stored values from the specified column.
     * 
     * @param column the id of the column.
     * 
     * @return a list of float values.
     *
     * @note only use this method if stored values are floats!
     * @note if performance is a concern, prefer the get*Value() alternatives.
     *
     * @see getFloatValue()
     */
    std::list<float> getFloatValues(int column);

    /** 
     * Gets a string list of the stored values from the specified column.
     * 
     * @param column the id of the column.
     * 
     * @return a list of string values.
     *
     * @note only use this method if stored values are strings!
     * @note if performance is a concern, prefer the get*Value() alternatives.
     *
     * @see getStringValue()
     */
    std::list<std::string> getStringValues(int column);

    /** 
     * Gets a boolean list of the stored values from the specified column.
     * 
     * @param column the id of the column.
     * 
     * @return a list of boolean values.
     *
     * @note only use this method if stored values are booleans!
     * @note if performance is a concern, prefer the get*Value() alternatives.
     *
     * @see getBoolValue()
     */
    std::list<bool> getBoolValues(int column);

    /** 
     * Gets the number of values stored in the specified column.
     * 
     * @param column the id of the column.
     * 
     * @return the number of stored values.
     *
     * @note it's recommended to use this method before calling any get*Value() method.
     */
    int getNbValues(int column);

    /** 
     * Get an integer value from the specified column and index.
     * 
     * @param column the id of the column.
     * @param index the index of the value in the column.
     * 
     * @return an integer value.
     *
     * @see getNbValues()
     */
    int &getIntValue(int column, int index);

    /** 
     * Get a float value from the specified column and index.
     * 
     * @param column the id of the column.
     * @param index the index of the value in the column.
     * 
     * @return a float value.
     *
     * @see getNbValues()
     */
    float &getFloatValue(int column, int index);

    /** 
     * Get a string value from the specified column and index.
     * 
     * @param column the id of the column.
     * @param index the index of the value in the column.
     * 
     * @return a string value.
     *
     * @see getNbValues()
     */
    std::string &getStringValue(int column, int index);

    /** 
     * Get a bool value from the specified column and index.
     * 
     * @param column the id of the column.
     * @param index the index of the value in the column.
     * 
     * @return a bool value.
     *
     * @see getNbValues()
     */
    bool &getBoolValue(int column, int index);

    /** 
     * Clears all the values in the specified column.
     * 
     * @param column the id of the column to clear.
     */
    void clearValues(int column);
    
    /** 
     * Prints the word into the specified output stream.
     * 
     * @param output the output stream.
     */
    void print(std::ostream &output=std::cout);

    /** 
     * Prints the word into the specified output file.
     * 
     * @param output the output file.
     */
    void print(FILE* output);
  };

  class McfSentence {
  private:
    int nbColumns; //!< Number of columns in the file
    std::vector<McfWord> sentence; 

  public:
    int length; //!< Length of the sentence (starts at 1 because of a fake root)
    
    /** 
     * Constructor of McfSentence.
     * 
     * @param nbColumns the number of columns in the sentence.
     */
    McfSentence(int nbColumns);

    /** 
     * Adds a new word to the sentence.
     * 
     * @return a reference to the added McfWord.
     */
    McfWord &addWord();

    /** 
     * Gets a word of the sentence.
     * 
     * @param i the index of the word in the sentence.
     * 
     * @return a referencen to a McfWord.
     */
    McfWord &getWord(int i);
    
    /** 
     * Clears the sentence.
     * 
     */
    void clear();

    /** 
     * Prints the sentence into the specified output stream.
     * 
     * @param output the output stream.
     */
    void print(std::ostream &output=std::cout);

    /** 
     * Prints the sentence into the specified output file.
     * 
     * @param output the output file.
     */
    void print(FILE* output);
  };

  class McfSentences {
  private:
    int nbSentences;
    std::vector<std::shared_ptr<McfSentence>> sentences; 
    
  public:
    /** 
     * Constructor of McfSentences.
     * 
     */
    McfSentences();
    
    /** 
     * Adds a sentence.
     * 
     * @param s the sentence to add.
     */
    void addSentence(std::shared_ptr<McfSentence> s);

    /** 
     * Gets a sentence from its index.
     * 
     * @param index the index of the sentence.
     * 
     * @return the sentence.
     */
    std::shared_ptr<McfSentence> getSentence(int index);

    /** 
     * Same as getSentenc()
     */
    std::shared_ptr<McfSentence> operator[](int index);

    /** 
     * Gets the number of sentences.
     * 
     * @return the number of sentences.
     */
    int size();
    
    /** 
     * Prints the sentences into the specified output stream.
     * 
     * @param output the output stream.
     */
    void print(std::ostream &output=std::cout);

    /** 
     * Prints the sentences into the specified output file.
     * 
     * @param output the output file.
     */
    void print(FILE* output);
  };

  class McfColumn {
  public:
    std::string name;		/**< the name of the column */
    std::string typeName;	/**< the type of the column */
    std::shared_ptr<Alphabet> alphabet;	/**< the alphabet used by the column (if the type is an alphabet) */
    int columnIdInFile;		/**< the position of the column in the file */

    /** 
     * Constructor of McfColumn.
     * 
     * @param name the name of the column.
     * @param typeName the type of the column.
     * @param columnIdInFile the position of the column in the file.
     */
    McfColumn(std::string name, std::string typeName, int columnIdInFile);
    
    /** 
     * Sets the alphabet of the column.
     * 
     * @param a the alphabet.
     */
    void setAlphabet(std::shared_ptr<Alphabet> a);
  };

  class Mcf {
  public:
    /** 
     * Constructor of Mcf.
     * It also reads the header of the input.
     * This constructor won't load any alphabets from a file.
     * 
     * @param input the input mcf stream.
     */
    Mcf(std::istream &input=std::cin);

    /** 
     * Constructor of Mcf.
     * It also reads the header of the input.
     * This constructor will load alphabets from the specified file.
     * 
     * @param input the input mcf stream.
     * @param alphabetFilename the name of the file which contains alphabets.
     */
    Mcf(std::istream &input, std::string alphabetFilename);

    /** 
     * Constructor of Mcf.
     * It also reads the header of the input.
     * This constructor will use an already existing alphabet array.
     * 
     * @param input the input mcf stream.
     * @param array the alphabet array to use.
     */
    Mcf(std::istream &input, AlphabetArray &array);

    /** 
     * Constructor of Mcf.
     * It also reads the header of the input.
     * This constructor won't load any alphabets from a file.
     * 
     * @param filename the name of the mcf file.
     */
    Mcf(std::string filename);

    /** 
     * Constructor of Mcf.
     * It also reads the header of the input.
     * This constructor will load alphabets from the specified file. 
     *
     * @param filename the name of the mcf file.
     * @param alphabetFilename the name of the file which contains alphabets.
     */
    Mcf(std::string filename, std::string alphabetFilename);

    /** 
     * Constructor of Mcf.
     * It also reads the header of the input.
     * This constructor will use an already existing alphabet array.
     * 
     * @param filename the name of the mcf file.
     * @param array the alphabet array to use.
     */
    Mcf(std::string filename, AlphabetArray &array);

    /** 
     * Destructor of Mcf.
     * 
     */
    ~Mcf();
    
    /** 
     * Notifies that you are going to use a column.
     * 
     * @param columnName the name of the column.
     * 
     * @return the id of the column.
     */
    int input(std::string columnName);

    /** 
     * Creates a new output column.
     * 
     * @param columnName the name of the new column.
     * @param columnType the type of the new column.
     * 
     * @return the id of the new column.
     *
     * @note if type is an alphabet, it will try to use already existing alphabets
     *       if an alphabet with the same name exists. If not, it will create a new alphabet.
     */
    int output(std::string columnName, std::string columnType=MCF_COLUMN_TYPE_STRING);

    /** 
     * Gets information about a column.
     * 
     * @param id the id of the column.
     * 
     * @return a McfColumn which contains information on the column.
     */
    McfColumn &getColumnInfo(int id);

    /** 
     * Gets information about a column.
     * 
     * @param name the name of the column.
     * 
     * @return a McfColumn which contains information on the column.
     */
    McfColumn &getColumnInfo(std::string name);
    
    /** 
     * Reads the next sentence in the stream.
     * 
     * @return the read sentence, or nullptr if none were read.
     */
    std::shared_ptr<McfSentence> readNextSentence();

    /** 
     * Reads all the sentences of the stream.
     * 
     * @return a McfSentences which contains all the sentences.
     */
    std::shared_ptr<McfSentences> readSentences();

    /** 
     * Prints the header into the specified output stream.
     * 
     * @param output the output stream.
     */
    void printHeader(std::ostream &output=std::cout);

    /** 
     * Prints the header into the specified output file.
     * 
     * @param output the output file.
     */
    void printHeader(FILE *output);

    /** 
     * Dumps the alphabets into the output stream.
     * 
     * @param output the output stream.
     */
    void dumpAlphabets(std::ostream &output);

    /** 
     * Dumps the alphabets into the specified file.
     * 
     * @param filename the name of the file.
     */
    void dumpAlphabets(std::string filename);

  private:
    std::istream &sinput;
    bool internalFile;
    int nbInputColumns;
    std::vector<McfColumn> columns;
    std::map<std::string,int> col2Index;
    std::map<std::string,int> registeredColumns;
    bool parsedHeader;
    AlphabetArray alphaArray;

    bool readLine(std::string &line, McfSentence &sentence, bool sentenceValid);
        /** 
     * Reads the header of the mcf file.
     * 
     */
    void readHeader();
  };
}
#endif /* MACA_MCF_H */
