/*******************************************************************************
    Copyright (C) 2012 by Alexis Nasr <alexis.nasr@lif.univ-mrs.fr>
                          Ghasem Mirroshandel <ghasem.mirroshandel@lif.univ-mrs.fr>
                          Jeremy Auguste <jeremy.auguste@etu.univ-amu.fr>
    This file is part of maca_graph_parser.

    maca_tagger is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    maca_tagger is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with maca_tagger. If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#ifndef __MACA_ALPHABET_WRAPPER_H__
#define __MACA_ALPHABET_WRAPPER_H__

#ifdef __cplusplus
extern "C" {
  namespace macaon {}
  using namespace macaon;
#else
  #define MACA_ALPHABET_INVALID_CODE -1
#endif
  
  typedef struct Alphabet maca_alphabet;

  /** 
   * Creates a new maca_alphabet.
   * They allow you to store multiple symbols and get an integer representation
   * for each of them.
   * 
   * @param name   the name of the alphabet.
   * @param loaded true if alphabet is loaded from a file, false otherwise.
   * 
   * @return a pointer to a new maca_alphabet.
   *
   * @see maca_alphabet_delete()
   */
  maca_alphabet* maca_alphabet_new(const char *name, char loaded);

  /** 
   * Frees a maca_alphabet.
   * 
   * @param a the alphabet we are freeing.
   *
   * @see maca_alphabet_new()
   */
  void maca_alphabet_delete(maca_alphabet *a);

  /** 
   * Releases a maca_alphabet.
   * 
   * @param a the alphabet we are releasing.
   */
  void maca_alphabet_release(maca_alphabet *a);
  
  /** 
   * Gets the name of an alphabet.
   * 
   * @param a    the alphabet to get the name from.
   * @param name the buffer to store the name into.
   * @param size the size of the buffer.
   */
  void maca_alphabet_get_name(maca_alphabet *a, char *name, int size);
  
  /** 
   * Adds a symbol to the alphabet.
   * You can only add symbols if the alphabet is unlocked.
   * 
   * @param a      the alphabet in which we're adding a symbol.
   * @param symbol the symbol to add.
   * 
   * @return the integer representation of the symbol, -1 if error.
   *
   * @see maca_alphabet_lock()
   * @see maca_alphabet_unlock()
   */
  int maca_alphabet_add_symbol(maca_alphabet *a, const char *symbol);

  /** 
   * Gets the integer representation of a symbol.
   * 
   * @param a      the alphabet in which we are looking for a symbol.
   * @param symbol the symbol we are looking for.
   * 
   * @return the integer representation of the symbol, -1 if not found.
   */
  int maca_alphabet_get_code(maca_alphabet *a, const char *symbol);

  /** 
   * Get a symbol from its integer representation.
   * 
   * @param a    the alphabet in which the symbol is stored.
   * @param code the integer representation of the symbol.
   * @param name a buffer to store the symbol into.
   * @param size the size of the buffer.
   * 
   * @return 1 if everything went fine, 0 otherwise (invalid code, etc.)
   */
  char maca_alphabet_get_symbol(maca_alphabet *a, int code, char *name, int size);
  
  /** 
   * Gets the size of the alphabet.
   * 
   * @param a the alphabet we are getting the size from.
   * 
   * @return the size (number of symbols) of the alphabet.
   */
  int maca_alphabet_size(maca_alphabet *a);

  /** 
   * Locks the alphabet.
   * This means that you won't be able to add any new symbols into the alphabet.
   * 
   * @param a the alphabet to lock.
   */
  void maca_alphabet_lock(maca_alphabet *a);

  /** 
   * Unlocks the alphabet.
   * 
   * @param a the alphabet to unlock.
   */
  void maca_alphabet_unlock(maca_alphabet *a);

  /** 
   * Returns if the alphabet has been loaded from a file or not.
   * 
   * @param a the alphabet we are checking.
   * 
   * @return 1 if the alphabet has been loaded from a file, 0 otherwise.
   */
  char maca_alphabet_is_loaded(maca_alphabet *a);


  typedef struct AlphabetArray maca_alphabet_array;
  

  /** 
   * Creates a new maca_alphabet_array.
   * They allow you to store multiple alphabets.
   * It can load alphabets from files and also dump its content in a file.
   * 
   * @return a pointer to a new maca_alphabet_array.
   *
   * @see maca_alphabet_array_new_from_file()
   * @see maca_alphabet_array_delete()
   */
  maca_alphabet_array *maca_alphabet_array_new();

  /** 
   * Creates a new maca_alphabet_array, and loads alphabets from the specified
   * file.
   * 
   * @param filename the name of the file which contains alphabets.
   * 
   * @return a pointer to a new maca_alphabet_array, or NULL if we couldn't open the file.
   *
   * @see maca_alphabet_array_new()
   * @see maca_alphabet_array_delete()
   */
  maca_alphabet_array *maca_alphabet_array_new_from_file(const char *filename);

  /** 
   * Frees the alphabet array.
   * 
   * @param array the alphabet array we are freeing.
   */
  void maca_alphabet_array_delete(maca_alphabet_array *array);

  /** 
   * Adds an alphabet to the array.
   * 
   * @param array the alphabet array in which we're adding an alphabet.
   * @param a     the alphabet we're adding.
   *
   * @return 1 if successful, 0 otherwise (name already in array).
   */
  char maca_alphabet_array_add_alphabet(maca_alphabet_array *array, maca_alphabet *a);

  /** 
   * Removes an alphabet from the array.
   * 
   * @param array         the array we are removing an alphabet from.
   * @param name          the name of the alphabet we are removing.
   * @param free_alphabet 1 if we also want to free the alphabet, 0 otherwise.
   * 
   * @return a pointer to the removed alphabet or NULL if it was freed.
   *
   * @note if you kept a pointer to the alphabet somewhere else, even if you free it here,
   *       you'll need to release your other pointer too.
   */
  maca_alphabet *maca_alphabet_array_remove_alphabet(maca_alphabet_array *array, const char *name, 
						     char free_alphabet);

  /** 
   * Gets an alphabet from the array.
   * 
   * @param array the array we are getting an alphabet from.
   * @param name  the name of the alphabet to look for.
   * 
   * @return a pointer to an alphabet if it exists, NULL otherwise.
   *
   * @note you must release the pointer once you are finished with it!
   *
   * @see maca_alphabet_release()
   */
  maca_alphabet *maca_alphabet_array_get_alphabet(maca_alphabet_array *array, const char *name);

  /** 
   * Gets the size of the array.
   * 
   * @param array the array we are getting the size from.
   * 
   * @return the size of the array.
   */
  int maca_alphabet_array_size(maca_alphabet_array *array);

  /** 
   * Loads from a file alphabets and stores them in the array.
   * 
   * @param array    the array to store the alphabets in.
   * @param filename the file to load the alphabets from.
   *
   * @return 1 if successful, 0 if we couldn't open the file.
   */
  char maca_alphabet_array_load(maca_alphabet_array *array, const char *filename);

  /** 
   * Dumps into a file all the alphabets in the array.
   * 
   * @param array    the array to dump from.
   * @param filename the file to dump into.
   *
   * @return 1 if successful, 0 if we couldn't open the file.
   */
  char maca_alphabet_array_dump(maca_alphabet_array *array, const char *filename);
  
#ifdef __cplusplus
}
#endif
#endif
