/***************************************************************************
    Copyright (C) 2012 by Alexis Nasr <alexis.nasr@lif.univ-mrs.fr>
                          Ghasem Mirroshandel <ghasem.mirroshandel@lif.univ-mrs.fr>
    This file is part of maca_graph_parser.

    maca_graph_parser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    maca_graph_parser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with maca_graph_parser. If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


#include <string.h>
#include <getopt.h>

#include "maca_common.h"
#include "maca_graph_parser.h"
#include "maca_graph_parser_model.h"
#include "maca_graph_parser_sentence.h"
#include "maca_graph_parser_dep_count_table.h"
#include "maca_graph_parser_decoder.h"
#include "maca_graph_parser_features.h"
#include "maca_graph_parser_feature_table.h"

/*
char * maca_graph_parser_get_model_filename(char * cfg, int order);
char * maca_graph_parser_get_alphabet_filename(char * cfg, int order);
char * maca_graph_parser_get_dep_count_filename(char * cfg, int order);
*/

void maca_graph_parser_print_ctx(maca_graph_parser_ctx *ctx)
{
  fprintf(stderr, "module =                  %s\n", ctx->module);
  fprintf(stderr, "cfg =                     %s\n", ctx->cfg);
  fprintf(stderr, "verbose_flag =            %d\n", ctx->verbose_flag); 
  fprintf(stderr, "mcf file name =           %s\n", ctx->mcf_file_name);
  fprintf(stderr, "dep count file name =     %s\n", ctx->dep_count_table_file_name);
  fprintf(stderr, "model file name =         %s\n", ctx->model_file_name);
  fprintf(stderr, "alphabet file name =      %s\n", ctx->alphabet_file_name);
  fprintf(stderr, "sent nb =                 %d\n", ctx->sent_nb);
  fprintf(stderr, "I =                       %d\n", ctx->I);
  fprintf(stderr, "H =                       %d\n", ctx->H);
  fprintf(stderr, "order =                   %d\n", ctx->order);
  fprintf(stderr, "synt labels nb =          %d\n", ctx->labels_nb);
  fprintf(stderr, "pos nb =                  %d\n", ctx->pos_nb);
  fprintf(stderr, "min dep count =           %d\n", ctx->min_dep_count);
  fprintf(stderr, "maximum sentence length = %d\n", ctx->max_sent_length);
  fprintf(stderr, "algorithm =               %d\n", ctx->algorithm);
  fprintf(stderr, "use lemmas =              %d\n", ctx->use_lemmas);
  fprintf(stderr, "use full forms =          %d\n", ctx->use_full_forms);
  fprintf(stderr, "basic features =          %d\n", ctx->basic_features);
  fprintf(stderr, "first order features =    %d\n", ctx->first_features);
  fprintf(stderr, "grandchildren_features =  %d\n", ctx->grandchildren_features);
  fprintf(stderr, "sibling features =        %d\n", ctx->sibling_features);
  fprintf(stderr, "subcat features =         %d\n", ctx->subcat_features);
  fprintf(stderr, "produce hash model =      %d\n", ctx->produce_hash_model); 
  fprintf(stderr, "hash fill rate =          %f\n", ctx->hash_fill_rate);
  fprintf(stderr, "k-best =                  %d\n", ctx->k);
  if(ctx->mode == DECODE_MODE)
    fprintf(stderr, "mode =                    decode\n");
  else if(ctx->mode == TRAIN_MODE)
    fprintf(stderr, "mode =                    train\n");
  else if(ctx->mode == EVAL_MODE)
    fprintf(stderr, "mode =                    eval\n");
  fprintf(stderr, "out file name =           %s\n", ctx->file_name_out);
}

maca_graph_parser_ctx * maca_graph_parser_InitCTX()
{
  maca_graph_parser_ctx * ctx = (maca_graph_parser_ctx *)calloc(sizeof(maca_graph_parser_ctx), 1);

    ctx->cfg=MACA_DEFAULT_CFG;
    ctx->verbose_flag = maca_verbose;
    ctx->module = MACA_GRAPH_PARSER_NAME;
    ctx->model_file_name = NULL;
    ctx->model2_file_name = NULL;
    ctx->mcf_file_name = NULL;
    ctx->e = NULL;
    ctx->sent_nb = 1000000;
    ctx->alphabet_file_name = NULL;
    ctx->I = 10;
    ctx->H = 50000000;
    ctx->order = 1;
    ctx->dep_count_table = NULL;
    ctx->dep_count_table_file_name = NULL;
    ctx->feature_table = NULL;
    ctx->min_dep_count = 0;
    ctx->max_sent_length = MACA_MAX_LENGTH_SENTENCE;
    ctx->algorithm = PERCEPTRON_TRAINING;
    ctx->use_lemmas = 0;
    ctx->use_full_forms = 0;
    ctx->basic_features = 1;
    ctx->first_features = 1;
    ctx->grandchildren_features = 0;
    ctx->sibling_features = 0;
    ctx->subcat_features = 0;
/*     ctx->hash_model = 1; */
    ctx->produce_hash_model = 1;
    ctx->print_ctx = 0;
    ctx->hash_fill_rate = 0.25;
    ctx->k = 0;
    ctx->file_name_out = NULL;
    ctx->file_out = stdout;
    ctx->mode = DECODE_MODE;
    ctx->s = NULL;
    ctx->store_in_feature_table = 1 ;

    ctx->words_alphabet = NULL;
    ctx->pos_alphabet = NULL;
    ctx->labels_alphabet = NULL;
    ctx->morpho_alphabet = NULL;
    ctx->synt_feats_alphabet = NULL;

    ctx->words_nb = 0;
    ctx->pos_nb = 0;
    ctx->labels_nb = 0;
    ctx->morpho_nb = 0;
    ctx->synt_feats_nb = 0;

    ctx->mcf_index_id = -1;
    ctx->mcf_form_id = -1;
    ctx->mcf_lemma_id = -1;
    ctx->mcf_cpostag_id = -1;
    ctx->mcf_postag_id = -1;
    ctx->mcf_feats_id = -1;
    ctx->mcf_deprel_id = -1;
    ctx->mcf_head_id = -1;
    ctx->mcf_pdeprel_id = -1;
    ctx->mcf_phead_id = -1;
    ctx->mcf_subcat_id = -1;

    return ctx;
}

maca_graph_parser_ctx * maca_graph_parser_LoadCTX(int argc, char ** argv) {
  /**
   * Load the execution context.
   */

  /* init with default values */
  maca_graph_parser_ctx * ctx = maca_graph_parser_InitCTX();
  /* get cfg from maca_common_ctx */
  ctx->cfg = NULL;

  /* parse command line options */
  static struct option long_options[] =
  {
        /* These options set a flag. */
        /*{"verbose", no_argument,       &verbose_flag, 1},
        {"brief",   no_argument,       &verbose_flag, 0},*/
        /* These options don't set a flag.
         We distinguish them by their indices. */
        {"verbose",     required_argument,       0, 'v'},
	{"model",     required_argument,       0, 'm'}, 
	{"conll",     required_argument,       0, 'c'}, 
        {"help",  no_argument, 0, 'h'},
        {"version",    no_argument, 0, 'V'},
        {"sentence-number",    no_argument, 0, 'n'},
        {0, 0, 0, 0}
  };
  int option_index = 0;

  optind = 0;
  opterr = 0;

  int c;
  while ((c = getopt_long (argc, (char**)argv, "hVv:c:m:n:a:I:H:r:C:d:M:p:D:l:LWBFGSUtxk:z:q:",long_options, &option_index)) != -1){ 
    switch (c)
      {
      case 'h':
	maca_graph_parser_PrintHelpMessage(argv[0]);
	exit(0);
      case 'V':
	fprintf(stderr, "%s version : %s\n", argv[0], MACA_GRAPH_PARSER_VERSION);
	exit(0);
      case 'B':
	ctx->basic_features = 1;
	break;
      case 'U':
	ctx->subcat_features = 1;
	break;
      case 'F':
	ctx->first_features = 1;
	break;
      case 'G':
	ctx->grandchildren_features = 1;
	break;
      case 'S':
	ctx->sibling_features = 1;
	ctx->order = 2;
	break;
      case 'L':
	ctx->use_lemmas = 1;
	break;
      case 'W':
	ctx->use_full_forms = 1;
	break;
      case 'p':
	if(!strcmp(optarg, "perceptron")) ctx->algorithm = PERCEPTRON_TRAINING;
	else if(!strcmp(optarg, "mira")) ctx->algorithm = MIRA_TRAINING;
	else if(!strcmp(optarg, "adagrad")) ctx->algorithm = ADAGRAD_TRAINING;
	else {
	  fprintf(stderr, "ERROR: Unsupported training algorithm \"%s\".\n", optarg);
	  exit(1);
	}
	break;
      case 't':
/* 	ctx->hash_model = 0; */
	ctx->produce_hash_model = 0;
	break;
      case 'v':
	ctx->verbose_flag = atoi(optarg);
	break;
      case 'a':
	ctx->alphabet_file_name = strdup(optarg);
	break;
      case 'm':
	ctx->model_file_name = strdup(optarg);
	break;
      case 'q':
	ctx->model2_file_name = strdup(optarg);
	break;
      case 'C':
	ctx->cfg = strdup(optarg);
	break;
      case 'M':
	if(optarg[0] == 't') ctx->mode = TRAIN_MODE;
	else if(optarg[0] == 'd') ctx->mode = DECODE_MODE;
	break;
      case 'c':
	ctx->mcf_file_name = strdup(optarg);
	break;
      case 'z':
	ctx->file_name_out = strdup(optarg);
	ctx->file_out = fopen(ctx->file_name_out, "w");
	if(ctx->file_out == NULL){
	  maca_msg(ctx->module, MACA_ERROR);
	  fprintf(stderr, "cannot open out file %s aborting\n", ctx->file_name_out);
 	  exit(1); 
	}
	break;
      case 'd':
	ctx->dep_count_table_file_name = strdup(optarg);
	break;
      case 'l':
	ctx->max_sent_length = atoi(optarg) + 1; /* add one for the fake root */
	if(ctx->max_sent_length > MACA_MAX_LENGTH_SENTENCE){
	  fprintf(stderr, "cannot set max sentence length to %d", ctx->max_sent_length);
	  fprintf(stderr, ", it is higher than the maximal hard-coded value of %d", MACA_MAX_LENGTH_SENTENCE);
	  fprintf(stderr, ", aborting\n");
	  exit(1);
	}
	break;
      case 'D':
	ctx->min_dep_count = atoi(optarg);
	break;
      case 'n':
	ctx->sent_nb = atoi(optarg);
	break;
      case 'r':
	ctx->hash_fill_rate = atof(optarg);
	break;
      case 'I':
	ctx->I = atoi(optarg);
	break;
      case 'H':
	ctx->H = atoi(optarg);
	break;
      case 'x':
	ctx->print_ctx = 1;
	break;
      case 'k':
	ctx->k = atoi(optarg);
	break;
/*       default : optind++; break; */
	/*       default : break; */
      }
  }

  /* check for presence of mandatory options */

  /* alphabet */
  /* file name from command line or cfg */
  if(ctx->alphabet_file_name == NULL){
    ctx->alphabet_file_name = maca_graph_parser_get_alphabet_filename(ctx->cfg, ctx->order);
  }
  if(ctx->alphabet_file_name == NULL){
    maca_msg(ctx->module, MACA_ERROR);
    fprintf(stderr, "you must specify an alphabet file with option -a\n");
    exit(1);
  }

  /* dep_count_table */
  /* file name from command line or cfg */
  if(ctx->dep_count_table_file_name == NULL){
    ctx->dep_count_table_file_name = maca_graph_parser_get_dep_count_filename(ctx->cfg, ctx->order);
  }
  if(ctx->dep_count_table_file_name == NULL){
    maca_msg(ctx->module, MACA_ERROR);
    fprintf(stderr, "you must specify an dep count table file with option -d\n");
    exit(1);
  }

  /* model */
  /* file name from command line or cfg */
  if(ctx->model_file_name == NULL){
    ctx->model_file_name = maca_graph_parser_get_model_filename(ctx->cfg, ctx->order);
  }
  if(ctx->model_file_name == NULL){
    maca_msg(ctx->module, MACA_ERROR);
    fprintf(stderr, "you must specify a name for the model file with option -m\n");
    exit(1);
  }


  /* training corpus required */
  if((ctx->mode == TRAIN_MODE) && (ctx->mcf_file_name == NULL)){
    maca_msg(ctx->module, MACA_ERROR);
    fprintf(stderr, "you must specify a name for the training corpus with option -c\n");
    exit(1);
  }
  


  return ctx;
}


void maca_graph_parser_init(maca_graph_parser_ctx * ctx)
{
  /* lexicon */
  /* used only in maca_graph_parser_sentence, at the moment */
  /* filename in cfg */
  
  /* tagset */
  /* filename in cfg */

  /*  if(!maca_tags_loaded()){
    maca_tags_load_bin_cfg(ctx->cfg);
    }*/


  /* ctx->labels_nb =  maca_tags_count(ctx->cfg, "morpho", "fct"); */
  /*  if(ctx->labels_nb > NB_LABELS){
    fprintf(stderr, "the given tagset contains %d syntactic labels, which exceeds the hard-coded limitation of %d, aborting\n", ctx->labels_nb, NB_LABELS);
    } */
  /* ctx->pos_nb = maca_tags_count(ctx->cfg, "morpho", "stype"); */


  /* initialize or load alphabets */


  if(ctx->pos_start == -1){
    fprintf(stderr, "error: POS-tag __START__ not found in the alphabet file.\n");
    exit(1);
  }
  if(ctx->pos_end == -1){
    fprintf(stderr, "error: POS-tag __END__ not found in the alphabet file.\n");
    exit(1);
  }
  if(ctx->fct_joker == -1){
    fprintf(stderr, "error: function __JOKER__ not found in the alphabet file.\n");
    exit(1);
  }


}


void maca_graph_parser_free_all(maca_graph_parser_ctx *ctx)
{
  maca_graph_parser_dep_count_table_free(ctx->pos_nb, ctx->labels_nb, ctx->dep_count_table);
  if(ctx->words_alphabet) maca_alphabet_release(ctx->words_alphabet);
  if(ctx->labels_alphabet) maca_alphabet_release(ctx->labels_alphabet);
  if(ctx->pos_alphabet) maca_alphabet_release(ctx->pos_alphabet);
  if(ctx->morpho_alphabet) maca_alphabet_release(ctx->morpho_alphabet);


  maca_graph_parser_model_free(ctx->model);
  maca_graph_parser_model_free(ctx->model2);
  if(ctx->feature_table) maca_graph_parser_feature_table_free(ctx);
  if(ctx->model_file_name) free(ctx->model_file_name);
  if(ctx->model2_file_name) free(ctx->model2_file_name);
  if(ctx->alphabet_file_name) free(ctx->alphabet_file_name);
  if(ctx->dep_count_table_file_name) free(ctx->dep_count_table_file_name);
  if(ctx->s) maca_graph_parser_free_sentence(ctx->s);
  if(ctx->mcf_file_name) free(ctx->mcf_file_name);
  if(ctx->file_name_out) free(ctx->file_name_out);
  if(ctx->file_out) fclose(ctx->file_out);
  //if(ctx->cfg) free(ctx->cfg);
  if(ctx->e) maca_graph_parser_templ_library_free(ctx->e);
  //  maca_lex_free();
  /* maca_sentence_clean_section(ctx->ms,MACA_GRAPH_PARSER_SECTION); */
}



void  maca_graph_parser_PrintHelpMessage()
{
  fprintf(stderr, "%s usage: %s [options]\n", MACA_GRAPH_PARSER_NAME, MACA_GRAPH_PARSER_NAME);
  fprintf(stderr, "OPTIONS :\n");
  
  fprintf(stderr, "general options\n");
  fprintf(stderr, "      -h            : print this message\n");
  fprintf(stderr, "      -V            : show version\n");
  fprintf(stderr, "      -M t|d        : t for train mode\n");
  fprintf(stderr, "                    : d for decode mode\n");
  fprintf(stderr, "      -v 1|2|3      : verbosity level\n");
  fprintf(stderr, "      -c file name  : conll file (mandatory in training mode)\n");
  fprintf(stderr, "      -m file name  : model file\n");
  fprintf(stderr, "      -a file name  : alphabet file\n");
  fprintf(stderr, "      -d file name  : dep count table file name\n");
  fprintf(stderr, "      -n int        : number of sentences to process\n");
  fprintf(stderr, "      -C language   : language\n");
  fprintf(stderr, "      -l int        : maximum sentence length\n");
  fprintf(stderr, "      -x            : print context\n");

  fprintf(stderr, "\ndecoding options\n");
  fprintf(stderr, "      -k int        : num of k-best (works for first order only)\n");
  fprintf(stderr, "      -z file name  : output file\n");
  fprintf(stderr, "      -r float      : hash fill rate\n");
  fprintf(stderr, "      -S            : use second order model\n");

  fprintf(stderr, "\ntraining options\n");
  fprintf(stderr, "      -p algorithm  : training algorithm (perceptron, mira, adagrad)\n");
  fprintf(stderr, "      -L            : use lemmas\n");
  fprintf(stderr, "      -W            : use full forms\n");
  fprintf(stderr, "      -B            : use basic features\n");
  fprintf(stderr, "      -F            : use first order features\n");
  fprintf(stderr, "      -G            : use grandchildren features\n");
  fprintf(stderr, "      -S            : use sibling features\n");
  fprintf(stderr, "      -U            : use subcat features\n");
  fprintf(stderr, "      -I int        : number of iterations for training\n");
  fprintf(stderr, "      -D int        : minimum dependency count\n");
  fprintf(stderr, "      -H int        : size of hash table\n");
  fprintf(stderr, "      -t            : store model as an array (more compact but slower to load)\n");
}

char * maca_graph_parser_GetVersion()
{
  return MACA_GRAPH_PARSER_VERSION;
}

char * maca_graph_parser_get_model_filename(char * cfg, int order)
{
    char * filename;
    char * path = maca_common_get_macaon_path(cfg);
    if(order == 2){
      filename = (char*)malloc(sizeof(char)*(strlen(path)+1+strlen(cfg)*2+5+strlen(MACA_GRAPH_PARSER_SECOND_MODEL_FILE_NAME)+1+4+1+1));
      /* sprintf(filename,"%s/%s/bin/%s_%s.bin",path,cfg,MACA_GRAPH_PARSER_SECOND_MODEL_FILE_NAME,cfg); */
      sprintf(filename,"%s/%s/bin/%s.bin",path,cfg,MACA_GRAPH_PARSER_SECOND_MODEL_FILE_NAME);
      return filename;
    }
    filename = (char*)malloc(sizeof(char)*(strlen(path)+1+strlen(cfg)*2+5+strlen(MACA_GRAPH_PARSER_FIRST_MODEL_FILE_NAME)+1+4+1+1));
    /* sprintf(filename,"%s/%s/bin/%s_%s.bin",path,cfg,MACA_GRAPH_PARSER_FIRST_MODEL_FILE_NAME,cfg); */
    sprintf(filename,"%s/%s/bin/%s.bin",path,cfg,MACA_GRAPH_PARSER_FIRST_MODEL_FILE_NAME);
    return filename;

}

char * maca_graph_parser_get_alphabet_filename(char * cfg, int order)
{
    char * filename;
    char * path = maca_common_get_macaon_path(cfg);
    if(order == 2){
      filename = (char*)malloc(sizeof(char)*(strlen(path)+1+strlen(cfg)*2+5+strlen(MACA_GRAPH_PARSER_SECOND_ALPHA_FILE_NAME)+1+5+1+1));
      /* sprintf(filename,"%s/%s/bin/%s_%s.alpha",path,cfg,MACA_GRAPH_PARSER_SECOND_ALPHA_FILE_NAME,cfg); */
      sprintf(filename,"%s/%s/bin/%s.alpha",path,cfg,MACA_GRAPH_PARSER_SECOND_ALPHA_FILE_NAME);
      return filename;
    }
    filename = (char*)malloc(sizeof(char)*(strlen(path)+1+strlen(cfg)*2+5+strlen(MACA_GRAPH_PARSER_FIRST_ALPHA_FILE_NAME)+1+5+1+1));
    /* sprintf(filename,"%s/%s/bin/%s_%s.alpha",path,cfg,MACA_GRAPH_PARSER_FIRST_ALPHA_FILE_NAME,cfg); */
    sprintf(filename,"%s/%s/bin/%s.alpha",path,cfg,MACA_GRAPH_PARSER_FIRST_ALPHA_FILE_NAME);
    return filename;
}

char * maca_graph_parser_get_dep_count_filename(char * cfg, int order)
{
    char * filename;
    char * path = maca_common_get_macaon_path(cfg);
    if(order == 2){
      filename = (char*)malloc(sizeof(char)*(strlen(path)+1+strlen(cfg)*2+5+strlen(MACA_GRAPH_PARSER_SECOND_DEP_COUNT_FILE_NAME)+1+10+1));
      /* sprintf(filename,"%s/%s/bin/%s_%s.dep_count",path,cfg,MACA_GRAPH_PARSER_SECOND_ALPHA_FILE_NAME,cfg); */
      sprintf(filename,"%s/%s/bin/%s.dep_count",path,cfg,MACA_GRAPH_PARSER_SECOND_ALPHA_FILE_NAME);
      return filename;
    }
    filename = (char*)malloc(sizeof(char)*(strlen(path)+1+strlen(cfg)*2+5+strlen(MACA_GRAPH_PARSER_FIRST_DEP_COUNT_FILE_NAME)+1+10+1));
    /* sprintf(filename,"%s/%s/bin/%s_%s.dep_count",path,cfg,MACA_GRAPH_PARSER_FIRST_ALPHA_FILE_NAME,cfg); */
    sprintf(filename,"%s/%s/bin/%s.dep_count",path,cfg,MACA_GRAPH_PARSER_FIRST_ALPHA_FILE_NAME);
    return filename;

}

void maca_graph_parser_print_verbose(maca_graph_parser_ctx * mtctx, int level, int type, char * message,...)
{
    //extern int verbose;
    va_list args;

    if(level <= mtctx->verbose_flag)
    {
	va_start(args,message);
        maca_print_vverbose(mtctx->module,level,type,NULL,message,&args);
        va_end(args);
    }   

}
