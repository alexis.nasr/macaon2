/*******************************************************************************
    Copyright (C) 2012 by Alexis Nasr <alexis.nasr@lif.univ-mrs.fr>
                          Ghasem Mirroshandel <ghasem.mirroshandel@lif.univ-mrs.fr>
    This file is part of maca_graph_parser.

    maca_tagger is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    maca_tagger is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with maca_tagger. If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#ifndef __MACA_GRAPH_PARSER_ALPHABET__
#define __MACA_GRAPH_PARSER_ALPHABET__

#include <stdio.h>
#include "hash.h"
#include "array.h"

typedef struct {
  char *name;
  int nb;
  hash_t *htable;
  array_t *array;
} maca_graph_parser_alphabet; 

#ifdef __cplusplus
extern "C"{
#endif

void maca_graph_parser_alphabet_print(FILE *filename, maca_graph_parser_alphabet *a);
void maca_graph_parser_alphabet_free(maca_graph_parser_alphabet *a);
maca_graph_parser_alphabet *maca_graph_parser_alphabet_new(char *name);
int maca_graph_parser_alphabet_add_symbol(maca_graph_parser_alphabet *a, char *symbol);
int maca_graph_parser_alphabet_get_code(maca_graph_parser_alphabet *a, char *symbol);
char * maca_graph_parser_alphabet_get_symbol(maca_graph_parser_alphabet *a, int code);
/* void maca_graph_parser_alphabet_print(char *filename, maca_graph_parser_alphabet *a); */
void maca_graph_parser_alphabet_print4(char *filename, maca_graph_parser_alphabet *a1, maca_graph_parser_alphabet *a2, maca_graph_parser_alphabet *a3, maca_graph_parser_alphabet *a4);
void maca_graph_parser_alphabet_print5(char *filename, maca_graph_parser_alphabet *a1, maca_graph_parser_alphabet *a2, maca_graph_parser_alphabet *a3, maca_graph_parser_alphabet *a4, maca_graph_parser_alphabet *a5);
maca_graph_parser_alphabet *maca_graph_parser_alphabet_load(char *filename);
int maca_graph_parser_alphabet_size(maca_graph_parser_alphabet *a);
maca_graph_parser_alphabet **maca_graph_parser_alphabet_load4(char *filename);
maca_graph_parser_alphabet **maca_graph_parser_alphabet_load5(char *filename);
#ifdef __cplusplus
}
#endif

#endif
