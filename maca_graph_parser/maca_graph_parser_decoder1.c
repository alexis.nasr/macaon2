/*******************************************************************************
    Copyright (C) 2012 by Alexis Nasr <alexis.nasr@lif.univ-mrs.fr>
                          Ghasem Mirroshandel <ghasem.mirroshandel@lif.univ-mrs.fr>
    This file is part of maca_graph_parser.

    maca_graph_parser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    maca_graph_parser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with maca_graph_parser. If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#include<stdlib.h>
#include<stdio.h>
#include<time.h>

#include"maca_constants.h"
#include"maca_msg.h"

#include"maca_graph_parser_decoder.h"
#include"maca_graph_parser_features.h"
#include"maca_graph_parser_feature_vector.h"
#include"maca_graph_parser.h"
#include"maca_graph_parser_feature_table.h"
#include "maca_graph_parser_dep_count_table.h"

/*-------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------*/

//Closed *CLOSED
//[MACA_MAX_LENGTH_SENTENCE] /* start */
//[MACA_MAX_LENGTH_SENTENCE] /* end */
//[2] /* position of the root (left or right) */
//;

//Open *OPEN
//[MACA_MAX_LENGTH_SENTENCE] /* start */
//[MACA_MAX_LENGTH_SENTENCE] /* end */
//[2] /* position of the root (left or right) */
//[NB_LABELS] /* label */
//;

/*-------------------------------------------------------------------------------------------*/
void maca_graph_parser_decoder1_decode(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s, maca_graph_parser_feature_table *feat_table)
{
  int labels_nb = ctx->labels_nb;
  int n = s->l;
  int start;
  int end;
  int dir; /* left or right */
  int label; /* label */
  int m; /* breakpoint (middle) */
  int m_argmax = 0;
  int label_argmax = 0;
  int span;
  float w;
  /* trial */
  int gov;
  int dep;
  /* common */
  int nb_cands;
  float score_max;
  /* fill OPEN: left and right closed */
  Closed *cand_LC;
  Closed *cand_RC;
  float score_cand_LC;
  float score_cand_RC;
  Closed *max_LC;
  Closed *max_RC;
  /* fill CLOSED: candidate open and closed */
  Closed *cand_C;
  Open *cand_O;
  float score_cand_C;
  float score_cand_O;
  Closed *max_C;
  Open *max_O;
  /* test: default edge label */
  /* int dft_label = maca_tags_get_code(ctx->cfg, "morpho", "fct", "__JOKER__"); */
  int dft_label = maca_alphabet_get_code(ctx->labels_alphabet, "__JOKER__");
  int length_class;
  feat_vector *fv_basic = NULL;
  feat_vector *fv_first = NULL;
  float pl_score;

  for(span = 1; span <= n; span++){
    for(start = 0; start+span < n; start++){
      end = start + span;
      if(ctx->verbose_flag > 1) fprintf(stderr, "start = %d end = %d\n",start,end);
      length_class =  maca_graph_parser_dep_count_table_compute_length_class(start,end);
      /* fill OPEN table*/
      for(dir=0; dir<2; dir++){
	gov = (dir == la)? end: start;
	dep = (dir == la)? start: end;
	
	for(label=0; label<labels_nb; label++){

	  /* filter with min_dep_count */
	  if((ctx->dep_count_table[s->pos[gov]][s->pos[dep]][label][length_class][dir] >= ctx->min_dep_count) ||
	     (label == dft_label)){
	    /* init */
	    nb_cands = 0;
	    score_max = MINF;
	    max_LC = NULL;
	    max_RC = NULL;
	    m_argmax = -1; /* for debug msgs */
	    
	    /* find best pair of CLOSED substructures */
	    for(m=start; m<end; m++){
	      /* if (start == 0 && m != 0) continue; */ /* mate */
	      
	      /* left closed */
	      cand_LC = ctx->CLOSED[start][m][ra];
	      if((cand_LC != NULL) || (m == start)){
		score_cand_LC = cand_LC ? cand_LC->score : 0;
	      } else
		continue;
	      
	      /* right closed */
	      cand_RC = ctx->CLOSED[m+1][end][la];
	      if((cand_RC != NULL) || (m == end-1)){
		score_cand_RC = cand_RC ? cand_RC->score : 0;
	      } else
		continue;
	      
	      /* only candidates reach this point */
	      nb_cands++;
	      
	      if (ctx->verbose_flag > 1) fprintf(stderr, "\t\tcand = CLOSED[%d][%d](%f) + CLOSED[%d][%d](%f)\n", 
						 start, m, score_cand_LC,
						 m+1, end, score_cand_RC);
	      
	      /* update max */
	      if(nb_cands == 1){
		/* first candidate */
		score_max = score_cand_LC + score_cand_RC;
		max_LC = cand_LC;
		max_RC = cand_RC;
		m_argmax = m;
	      } else {
		if (score_cand_LC + score_cand_RC > score_max){
		  score_max = score_cand_LC + score_cand_RC;
		  max_LC = cand_LC;
		  max_RC = cand_RC;
		  m_argmax = m; /* for debug msgs */
		}
	      } /* end update max */
	    } /* end find best pair of CLOSED substructures */
	    
	    /* create an OPEN if there is at least a candidate pair of CLOSED */
	    if(nb_cands > 0){
	      /* label */

	      if(ctx->store_in_feature_table){
		w = feat_table->lab[start][end][label][dir];
	      }
	      else{/*-- compute scores on the fly  --*/
		fv_first = maca_graph_parser_first_score(gov, dep, label, ctx, fv_first, &w); 
	      }
	      if(ctx->verbose_flag > 1){
		char bstart[128];
		char bend[128];
		maca_alphabet_get_symbol(ctx->words_alphabet,s->words[start], bstart, sizeof(bstart));
		maca_alphabet_get_symbol(ctx->words_alphabet,s->words[end], bend, sizeof(bend));
		fprintf(stderr, "\tscore(%s(%d) %s-%d-%s %s(%d)) = %f\n", 
			bstart, 
			start,
			(dir==la)?"<":"", 
			label, 
			(dir==la)?"":">",
		        bend, 
			end,
			w
			);
	      }
	    
	      /* create an OPEN from the pair of CLOSED */

	      if(ctx->store_in_feature_table){
		pl_score = feat_table->pl[start][end][dir];
	      }
	      else{
		/*-- compute scores on the fly --*/
		fv_basic = maca_graph_parser_basic_score(gov, dep, ctx, fv_basic, &pl_score);  
	      }
	      /* fprintf(stderr, "pl score = %f\n", pl_score); */
	      
	      ctx->OPEN[start][end][dir][label] = alloc_open(score_max + pl_score + w,
							start, end, label, dir,
							max_LC, max_RC);
	      if (ctx->verbose_flag > 1)
		fprintf(stderr, "\tOPEN[%d][%d][%d][%d](%f) = CLOSED[%d][%d][1], CLOSED[%d][%d][0]\n",
			start, end, dir, label, (score_max + pl_score + w), start, m_argmax, m_argmax+1, end);
	    } /* end check there is at least a candidate */

	  } /* end min_dep_count */
	} /* end for label */
      } /* end for dir */
      /* end fill OPEN table */
      

      /* fill CLOSED table */
      /* codes for ra and la are almost duplicates, but unrolled to:
	 - avoid having ternary operators on every line,
	 - and improve cache locality while accessing to OPEN[][],
	 thus saving CPU time.
       */

      /* RA */
      /* init */
      nb_cands = 0;
      score_max = MINF;
      max_C = NULL;
      max_O = NULL;
      m_argmax = -1; /* for DBG */ /* MM: should be start+1 ? */
      label_argmax = -1; /* for DBG */
      
      for(m=start+1; m<=end; m++){

	for(label=0; label<labels_nb; label++){
	  
	  cand_O = ctx->OPEN[start][m][ra][label];
	  if(cand_O != NULL)
	    score_cand_O = cand_O->score;
	  else
	    continue;

	  cand_C = ctx->CLOSED[m][end][ra];
	  if((cand_C != NULL) || (m == end))
	    score_cand_C = cand_C ? cand_C->score : 0;
	  else
	    continue;

	  /* only candidates reach this point */
	  nb_cands++;

	  if (ctx->verbose_flag > 1)
	    fprintf(stderr, "\t\tcand = OPEN[%d][%d][ra][%d](%f) + CLOSED[%d][%d][ra](%f)\n", 
		    start, m, label, score_cand_O,
		    m, end, score_cand_C);
	  
	  /* update max */
	  if(nb_cands == 1){
	    /* first candidate */
	    score_max = (score_cand_C + score_cand_O);
	    max_C = cand_C;
	    max_O = cand_O;
	    m_argmax = m; /* DBG */
	    label_argmax = label; /* DBG */
	  } else {
	    if((score_cand_C + score_cand_O) > score_max){ 
	      score_max = (score_cand_C + score_cand_O);
	      max_C = cand_C;
	      max_O = cand_O;
	      m_argmax = m; /* DBG */
	      label_argmax = label; /* DBG */
	    }
	  } /* end update max */
	} /* end for label */
      } /* end for m */ 

      /* create best closed */
      if(nb_cands > 0){
	ctx->CLOSED[start][end][ra] = alloc_closed(score_max, start, end, m_argmax, ra, 
					      max_C, max_O);
	if (ctx->verbose_flag > 1)
	  fprintf(stderr, "\tCLOSED[%d][%d][ra](%f) = OPEN[%d][%d][ra][%d], CLOSED[%d][%d][ra]\n",
		  start,end,score_max,start,m_argmax,label_argmax,m_argmax,end);
      }
      /* end RA */

      /* LA */
      /* init */
      nb_cands = 0;
      score_max = MINF;
      max_C = NULL;
      max_O = NULL;
      m_argmax = -1; /* DBG */ /* MM: should be start ? */
      label_argmax = -1; /* DBG */

      for(m=start; m<end; m++){

	for(label=0; label<labels_nb; label++){

	  cand_O = ctx->OPEN[m][end][la][label];
	  if(cand_O != NULL)
	    score_cand_O = cand_O->score;
	  else
	    continue;

	  cand_C = ctx->CLOSED[start][m][la];
	  if((cand_C != NULL) || (m == start))
	    score_cand_C = cand_C ? cand_C->score : 0;
	  else
	    continue;

	  /* only candidates reach this point */
	  nb_cands++;

	  if (ctx->verbose_flag > 1)
	    fprintf(stderr, "\t\tcand = CLOSED[%d][%d][la](%f) + OPEN[%d][%d][la][%d](%f)\n",
		    start, m, score_cand_C,
		    m, end, label, score_cand_O);

	  /* update max */
	  if(nb_cands == 1){
	    score_max = (score_cand_C + score_cand_O);
	    max_C = cand_C;
	    max_O = cand_O;
	    m_argmax = m; /* DBG */
	    label_argmax = label; /* DBG */
	  } else {
	    if ( (score_cand_C + score_cand_O) > score_max){
	      score_max = (score_cand_C + score_cand_O);
	      max_C = cand_C;
	      max_O = cand_O;
	      m_argmax = m; /* DBG */
	      label_argmax = label; /* DBG */
	    }
	  } /* end update max */

	} /* end for label */
      } /* end for m */

      /* create best closed */
      if(nb_cands > 0){
	ctx->CLOSED[start][end][la] = alloc_closed(score_max, start, end, m_argmax, la,
					      max_C, max_O);
	
	if (ctx->verbose_flag > 1)
	  fprintf(stderr, "\tCLOSED[%d][%d][la](%f) = CLOSED[%d][%d][la], OPEN[%d][%d][la][%d]\n",
		  start,end,score_max,start,m_argmax,m_argmax,end,label_argmax);
      }
      /* end LA */
      /* end fill CLOSED table */

    } /* end start */
  } /* end span */

  if(fv_basic){
    free_feat_vector(fv_basic);
    fv_basic = NULL;
  }
  if(fv_first){
    free_feat_vector(fv_first);
    fv_first = NULL;
  }
}

/*-------------------------------------------------------------------------------------------*/

void maca_graph_parser_decoder1_cleanup(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s)
{
  int labels_nb = ctx->labels_nb;
  int sentence_length = s->l;
  int start;
  int end;
  int dir; /* left or right */
  int label; /*label*/

  for(start = 0; start < sentence_length; start++){
    for(end=0; end < sentence_length; end++){
      for(dir=0; dir<2; dir++){
	/* closed */
	free(ctx->CLOSED[start][end][dir]);
	ctx->CLOSED[start][end][dir] = NULL;
	/* open */
	for(label=0; label<labels_nb; label++){
	  free(ctx->OPEN[start][end][dir][label]); 
	  ctx->OPEN[start][end][dir][label] = NULL;
	}
      }
    }
  }

  // faster implementation
  if(sentence_length > 0) {
      free(ctx->OPEN[0][0][0]);
      free(ctx->OPEN[0][0]);
      free(ctx->OPEN[0]);
      free(ctx->OPEN);
      free(ctx->CLOSED[0][0]);
      free(ctx->CLOSED[0]);
      free(ctx->CLOSED);
  }
  // reference implementation
  /*for(start = 0; start < sentence_length; start++) {
      for(end = 0; end < sentence_length; end++) {
          for(dir = 0; dir < 2; dir++) {
              free(ctx->OPEN[start][end][dir]);
          }
          free(ctx->OPEN[start][end]);
      }
      free(ctx->OPEN[start]);
  }
  free(ctx->OPEN);
  for(start = 0; start < sentence_length; start++) {
      for(end = 0; end < sentence_length; end++) {
          free(ctx->CLOSED[start][end]);
      }
      free(ctx->CLOSED[start]);
  }
  free(ctx->CLOSED);*/

}

/*-------------------------------------------------------------------------------------------*/
void maca_graph_parser_decoder1_init(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s)
{
  int labels_nb = ctx->labels_nb;
  int sentence_length = s->l;
  int start;
  int end;
  int dir; /* left or right */
  int label; /*label*/

  // faster implementation
  ctx->CLOSED = malloc(sizeof(Closed***) * sentence_length);
  ctx->OPEN = malloc(sizeof(Open****) * sentence_length);
  if(sentence_length > 0) {
      ctx->CLOSED[0] = malloc(sizeof(Closed**) * sentence_length * sentence_length);
      ctx->CLOSED[0][0] = malloc(sizeof(Closed*) * sentence_length * sentence_length * 2);
      for(start = 0; start < sentence_length; start++) {
          ctx->CLOSED[start] = ctx->CLOSED[0] + start * sentence_length;
          for(end = 0; end < sentence_length; end++) {
              ctx->CLOSED[start][end] = ctx->CLOSED[0][0] + ((start * sentence_length) + end) * 2;
          }
      }
      ctx->OPEN[0] = malloc(sizeof(Open***) * sentence_length * sentence_length);
      ctx->OPEN[0][0] = malloc(sizeof(Open**) * sentence_length * sentence_length * 2);
      ctx->OPEN[0][0][0] = malloc(sizeof(Open*) * sentence_length * sentence_length * 2 * ctx->labels_nb);
      for(start = 0; start < sentence_length; start++) {
          ctx->OPEN[start] = ctx->OPEN[0] + start * sentence_length;
          for(end = 0; end < sentence_length; end++) {
              ctx->OPEN[start][end] = ctx->OPEN[0][0] + ((start * sentence_length) + end) * 2;
              for(dir = 0; dir < 2; dir++) {
                  ctx->OPEN[start][end][dir] = ctx->OPEN[0][0][0] + ((((start * sentence_length) + end) * 2) + dir) * ctx->labels_nb;
              }
          }
      }
  }
  // reference implementation
  /*ctx->CLOSED = malloc(sizeof(Closed***) * sentence_length);
  for(start = 0; start < sentence_length; start++) {
      ctx->CLOSED[start] = malloc(sizeof(Closed**) * sentence_length);
      for(end = 0; end < sentence_length; end++) {
          ctx->CLOSED[start][end] = malloc(sizeof(Closed*) * 2);
      }
  }
  ctx->OPEN = malloc(sizeof(Open****) * sentence_length);
  for(start = 0; start < sentence_length; start++) {
      ctx->OPEN[start] = malloc(sizeof(Open***) * sentence_length);
      for(end = 0; end < sentence_length; end++) {
          ctx->OPEN[start][end] = malloc(sizeof(Open**) * 2);
          for(dir = 0; dir < 2; dir++) {
              ctx->OPEN[start][end][dir] = malloc(sizeof(Open*) * ctx->labels_nb);
          }
      }
  }*/

  for(start=0; start < sentence_length; start++){
    for(end=0; end < sentence_length; end++){
      for(dir=0; dir<2; dir++){
	/* closed */
	ctx->CLOSED[start][end][dir] = NULL;
	/* open */
	for(label=0; label<labels_nb; label++){
	  ctx->OPEN[start][end][dir][label] = NULL;
	}
      }
    }
  }
}


/*-------------------------------------------------------------------------------------------*/

void maca_graph_parser_decoder1_parse(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s, maca_graph_parser_feature_table *feat_table)
{
  Closed *bestSpan = NULL;

  maca_graph_parser_decoder1_decode(ctx, s, ctx->feature_table);

  bestSpan = ctx->CLOSED[0][s->l-1][ra];
  if(bestSpan != NULL){
    create_closed(bestSpan, s);
    s->score = bestSpan->score;
  }

}
