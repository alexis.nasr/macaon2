#include "maca_common.h"
#include "maca_graph_parser_sentence.h"
#include "maca_graph_parser_dep_count_table.h"


typedef struct {
  maca_graph_parser_sentence **ref;
  maca_graph_parser_sentence **hyp;
  int size;
  int capacity;
} hyp_ref_vector;

#ifdef __cplusplus
extern "C"{
#endif


hyp_ref_vector *allocate_hyp_ref_vector(int capacity);
void free_hyp_ref_vector(hyp_ref_vector *v);
void hyp_ref_vector_append(hyp_ref_vector *v, maca_graph_parser_sentence *ref, maca_graph_parser_sentence *hyp);

  //hyp_ref_vector *load_conll_corpus(maca_graph_parser_ctx *ctx);
void free_corpus(hyp_ref_vector *corpus);
  //void dump_conll_corpus(maca_graph_parser_ctx *ctx, hyp_ref_vector *corpus, FILE *f);

void maca_graph_parser_sentence_relabel_rare_deps(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s);
hyp_ref_vector *load_mcf_corpus(maca_graph_parser_ctx *ctx);
#ifdef __cplusplus
}
#endif
