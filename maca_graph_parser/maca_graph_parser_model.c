#include "maca_graph_parser_hash.h"
/* legacy includes from maca_graph_parser_hash.c */
/* TODO: clean up */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <math.h>

// for mmap()
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

#include "maca_graph_parser_model.h"


maca_graph_parser_model *maca_graph_parser_model_allocate(maca_graph_parser_ctx *ctx){
  /**
   * Allocate a model.
   */

  maca_graph_parser_model *m = malloc(sizeof(maca_graph_parser_model));
  if(m == NULL){
    maca_msg(ctx->module, MACA_ERROR);
    fprintf(stderr,"maca_graph_parser_model_allocate: memory allocation problem\n");
    exit(1);
  }
  return m;
}


void maca_graph_parser_model_init(maca_graph_parser_ctx *ctx, maca_graph_parser_model *model){
  /**
   * Initialize model.
   */

  /* maca_graph_parser_hash */
  /* model->feat_ht = creation_table(ctx->H, ctx->hash_fill_rate); */
  model->feat_ht = creation_table(ctx->H, 1);
}


void maca_graph_parser_model_free(maca_graph_parser_model *model){
  if(model == NULL)
    return;

  if(model->feat_array)
    feature_weight_table_free(model->feat_array);
  if(model->feat_ht)
    free_table(model->feat_ht);
  free(model);
}


void maca_graph_parser_model_print(maca_graph_parser_ctx *ctx){

}


/* persistence */
maca_graph_parser_model *maca_graph_parser_model_load(maca_graph_parser_ctx *ctx, char *model_file_name){
  /**
   * Load a model from a file.
   */

  maca_graph_parser_model *model = maca_graph_parser_model_allocate(ctx);
  
  /* open file */
  if(model_file_name == NULL){
    maca_msg(ctx->module, MACA_ERROR);
    fprintf(stderr, "maca_graph_parser_model_load: model file name is missing\n");
    exit(1);
  }
  FILE *model_file = fopen(model_file_name, "rb");
  if(model_file == NULL){
    fprintf(stderr, "cannot open file %s\n", model_file_name);
    exit(1);
  }
  
  /* read header */
  fread(&(model->is_hash_model), sizeof(int), 1, model_file); 
  /*   printf("hash model = %d\n", model->is_hash_model); */
  fread(&(model->min_dep_count), sizeof(int), 1, model_file); 
  /* printf("min dep count = %d\n", ctx->min_dep_count); */
  fread(&(model->use_lemmas), sizeof(int), 1, model_file); 
  /* printf("use lemmas = %d\n", ctx->use_lemmas); */
  fread(&(model->use_full_forms), sizeof(int), 1, model_file); 
  /* printf("use full forms = %d\n", ctx->use_full_forms); */
  fread(&(model->basic_features), sizeof(int), 1, model_file); 
  /* printf("basic features = %d\n", ctx->basic_features); */
  fread(&(model->first_features), sizeof(int), 1, model_file); 
  /* printf("first features = %d\n", ctx->first_features); */
  fread(&(model->grandchildren_features), sizeof(int), 1, model_file); 
  /* printf("grandchildren features = %d\n", ctx->grandchildren_features); */
  fread(&(model->sibling_features), sizeof(int), 1, model_file); 
  /* printf("sibling features = %d\n", ctx->sibling_features); */
  fread(&(model->subcat_features), sizeof(int), 1, model_file); 
  /* printf("sibling features = %d\n", ctx->sibling_features); */

  /* read content */
  if(model->is_hash_model)
    model->feat_ht = load_table(model_file);
  else{
    model->feat_array = load_feature_weight_table(model_file);
    model->feat_ht = feat_array2feat_hash(model->feat_array, ctx->hash_fill_rate);
    /*     ctx->hash_model = 1; */
    feature_weight_table_free(model->feat_array);
    model->feat_array = NULL;
  }

  fclose(model_file);
  
  return model;
}


void maca_graph_parser_model_dump(maca_graph_parser_ctx *ctx, maca_graph_parser_model *model, char *file_name, int produce_hash_model){
  /**
   * Dump model to file_name in hash table or array format.
   */

  FILE *model_file = fopen(file_name, "w");
  if(model_file == NULL){
    maca_msg(ctx->module, MACA_ERROR);
    fprintf(stderr, "cannot open file %s\n", ctx->model_file_name);
  }

  /*
    size_t cfg_strlen = strlen(ctx->cfg);
    fwrite(&cfg_strlen, sizeof(int), 1, model_file);
    fwrite(ctx->cfg, sizeof(char), cfg_strlen + 1, model_file);
  */
  fwrite(&(produce_hash_model), sizeof(int), 1, model_file); 
  fwrite(&(model->min_dep_count), sizeof(int), 1, model_file); 
  fwrite(&(model->use_lemmas), sizeof(int), 1, model_file); 
  fwrite(&(model->use_full_forms), sizeof(int), 1, model_file); 
  fwrite(&(model->basic_features), sizeof(int), 1, model_file); 
  fwrite(&(model->first_features), sizeof(int), 1, model_file);
  fwrite(&(model->grandchildren_features), sizeof(int), 1, model_file); 
  fwrite(&(model->sibling_features), sizeof(int), 1, model_file);
  fwrite(&(model->subcat_features), sizeof(int), 1, model_file);

  if(produce_hash_model)
    dump_table(model->feat_ht, model_file);
  else{
    model->feat_array = feat_hash2feat_array(model->feat_ht);
    dump_feature_weight_table(model->feat_array, model_file);
  } 

  fclose(model_file);
}


/* mmap */
maca_graph_parser_model *maca_graph_parser_model_mmap(maca_graph_parser_ctx *ctx, char *model_file_name){
  /**
   * Create a memory mapped model from model_file_name.
   */

    maca_graph_parser_model *model = maca_graph_parser_model_allocate(ctx);

    model->model_fd = open(model_file_name, O_RDONLY);
    if(model->model_fd == -1) {
        maca_msg(ctx->module, MACA_ERROR);
        fprintf(stderr, "could not open parser model \"%s\"\n", model_file_name);
        exit(1);
    }

    struct stat sb;
    if (fstat(model->model_fd, &sb) == -1) {
        maca_msg(ctx->module, MACA_ERROR);
        fprintf(stderr, "could not fstat parser model \"%s\"\n", model_file_name);
        exit(1);
    }
    model->mmap_length = sb.st_size;
    model->mmap_data = (const char*) mmap(NULL, model->mmap_length, PROT_READ, MAP_PRIVATE, model->model_fd, 0);
    if(model->mmap_data == MAP_FAILED) {
        perror("mmap");
        maca_msg(ctx->module, MACA_ERROR);
        fprintf(stderr, "mmap() failed on parser model \"%s\"\n", model_file_name);
        exit(1);
    }

    int field_id = 0;
    /* header */
    model->is_hash_model = ((int*)model->mmap_data)[field_id++];
    model->min_dep_count = ((int*)model->mmap_data)[field_id++];
    model->use_lemmas = ((int*)model->mmap_data)[field_id++];
    model->use_full_forms = ((int*)model->mmap_data)[field_id++];
    model->basic_features = ((int*)model->mmap_data)[field_id++];
    model->first_features = ((int*)model->mmap_data)[field_id++];
    model->grandchildren_features = ((int*)model->mmap_data)[field_id++];
    model->sibling_features = ((int*)model->mmap_data)[field_id++];
    /* content */
    if(model->is_hash_model) {
        //model->feat_ht = load_table(model_file);
        maca_graph_parser_hash *t = NULL;
        t = malloc(sizeof(maca_graph_parser_hash));
        t->taille = ((int*)model->mmap_data)[field_id++];
        t->nbelem = ((int*)model->mmap_data)[field_id++];
        t->table_clef = (feature_t*) (model->mmap_data + field_id * sizeof(int));
        t->params = (float*) (model->mmap_data + field_id * sizeof(int) + t->taille * sizeof(feature_t));
        t->total = NULL;
        model->feat_ht = t;
    } else {
        maca_msg(ctx->module, MACA_ERROR);
        fprintf(stderr, "feature weight table not supported in mmap'ed model \"%s\"\n", model_file_name);
    }
    return model;
}

void maca_graph_parser_model_munmap(maca_graph_parser_model *model) {
    if(model->mmap_data != MAP_FAILED) munmap((void*) model->mmap_data, model->mmap_length);
    if(model->model_fd != -1) close(model->model_fd);
}


/* scoring */
float score_feat_vector(feat_vector *fv, maca_graph_parser_model *model){
  /**
   * Score a feature vector against a model.
   */

  float sum = 0;

  if(fv == NULL)
    return sum;

  int i;
  for(i=0; i<fv->elt_nb; i++){
    float *w = recherche_hash(model->feat_ht, fv->array[i]);
    if(w)
      sum += *w;
  }

  return sum;
}


float score_feature_counter_array(feature_counter_array *a, maca_graph_parser_model *model){
  /**
   * Score feature counter array a against model.
   */

  float score = 0;

  int i;
  for(i=0; i < a->size; i++){
    feature_counter *c = a->array[i];
    if(c == NULL){
      continue;
    }

    int j;
    for(j=0; j < c->size; j++){
      int v = c->values[j];
      if(v != 0){
	float *w = recherche_hash(model->feat_ht, c->keys[j]);
	if(w != NULL){
	   /* printf("AN value = %d score = %f\n", v, score);  */
	  score += (v * (*w));
	}
      }
    }
  }

  return score;
}
