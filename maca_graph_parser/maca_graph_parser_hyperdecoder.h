/*******************************************************************************
    Copyright (C) 2012 by Alexis Nasr <alexis.nasr@lif.univ-mrs.fr>
                          Ghasem Mirroshandel <ghasem.mirroshandel@lif.univ-mrs.fr>
    This file is part of maca_graph_parser.

    maca_tagger is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    maca_tagger is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with maca_tagger. If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#ifndef __MACA_GRAPH_PARSER_DECODER_K__
#define __MACA_GRAPH_PARSER_DECODER_K__

#include "maca_common.h"
#include "maca_constants.h"
#include "maca_graph_parser.h"
#include "maca_graph_parser_sentence.h"

/* kbest */
#include "maca_graph_parser_heapq.h"
/* #include "maca_graph_parser_hypergraph.h" */ /* included in maca_graph_parser.h as a dirty hack */


typedef struct {
  int bs_i; /* rank of edge in the head vertex' backstar */
  int j[2]; /* */
} cand_id;

/* was: definition of vec_Dbp, moved to maca_graph_parser.h
   as a dirty hack */

#ifdef __cplusplus
extern "C"{
#endif


void maca_graph_parser_hyperdecoder_init(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s);
void maca_graph_parser_hyperdecoder_cleanup(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s);
void maca_graph_parser_hyperdecoder_parse(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s, maca_graph_parser_feature_table *feat_table);

void create_openBP(maca_graph_parser_ctx *ctx, DerivBP *d, maca_graph_parser_sentence *s, int ki);
void create_closedBP(maca_graph_parser_ctx *ctx, DerivBP *d, maca_graph_parser_sentence *s, int ki);

#ifdef __cplusplus
}
#endif


  
#endif
