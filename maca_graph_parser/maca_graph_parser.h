/*******************************************************************************
    Copyright (C) 2011 by XX <XX@lif.univ-mrs.fr>
    This file is part of maca_module.

    maca_MODULE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    maca_MODULE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with maca_MODULE. If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#ifndef __MACA_GRAPH_PARSER__
#define __MACA_GRAPH_PARSER__


#include <stdint.h>
#include <stdio.h>

#include "maca_alphabet_wrapper.h"
#include "maca_common.h"

#include "maca_graph_parser_hypergraph.h" /* dirty hack: we need def of Vertex for dynamically allocated kbest */

#include <math.h>
#ifndef INFINITY
#define INFINITY (float) (1.0f / 0.0f)
#endif

/*!< MODULE version */
#ifndef MACA_GRAPH_PARSER_VERSION
#define MACA_GRAPH_PARSER_VERSION "1.0.0"
#endif

/*!< MODULE NAME */
#define MACA_GRAPH_PARSER_NAME "maca_graph_parser"


#define feature_t uint64_t
#define _feat_family_nb 200




typedef struct 
{
  int taille;
  int nbelem;
  feature_t *table_clef;
  float *params;  
  float *total;  
} maca_graph_parser_hash;



/*typedef struct 
{
  int taille;
  int nbelem;
  int labels_nb;
  feature_t *table_clef;
  float ***params;  
  float ***total;  
} maca_graph_parser_hash;
*/
/* forward declaration of decoder structures */
struct open;
struct closed;
/*
typedef struct 
{
  feature_t feature;
  float weight;
} feature_weight;
*/
typedef struct 
{
  feature_t *features;
  float *weights;
  uint32_t size; /* was: size_t */
} maca_graph_parser_feature_weight_table;


typedef struct {
  /* header */
  int is_hash_model; /* type of model */
  int min_dep_count;
  int use_lemmas; /* features */
  int use_full_forms;
  int basic_features;
  int first_features;
  int grandchildren_features;
  int sibling_features;
  int subcat_features;
  /* content stored in a hash table or plain table */
  maca_graph_parser_hash *feat_ht; 
  maca_graph_parser_feature_weight_table *feat_array;
  /* mmap model */
  int model_fd;
  const char *mmap_data;
  uint64_t mmap_length;
  /* file name */
  char *file_name;
} maca_graph_parser_model;


typedef struct {
  int  field_nb;
  int  start[10];
  int  end[10];
  char type[10];
  int  value[10];
  int  length[10];
  int  label_start;
  int  label_end;
  int  direction_start;
  int  direction_end;
  int  hash_key_start;
  int  hash_key_end;
} templ;

typedef int *****maca_graph_parser_dep_count_table;

typedef struct  {
  int s_rel;   /*number of different syntactic relations */
  int s_pos;   /*number of different parts of speech */
  int s_word;  /*number of different fledged forms */
  int s_synt_feat;  /*number of different syntactic features */
  int s_type;  /* */
  int s_dir;   /* number of different dependency direction (left and right)*/
  int s_dist;  /* number of different dependency lengths */
  int s_feat;  /* */
  int s_child; /* */
  int type_start; /*first bit used to encode type in the binary representation of a feature */
  int type_end; /*first bit used to encode type in the binary representation of a feature */

  templ *tfdp;
  templ *tfdw;
  templ *tfdwp;
  templ *tfdpp;
  templ *tfdww;
  templ *tfdwpp;
  templ *tfdwwp;
  templ *tfdppp;
  templ *tfdpppp;
  templ *tfdwpwp;
  templ *tdppp;
  templ *tfddpp;
  templ *tfddppp;
  templ *tfddww;
  templ *tfddwp;
  templ *tfdsppp;
  templ *tfdspp;
  templ *tfdsww;
  templ *tfdswp;
  templ *tflpp;
  templ *tflppp;
  templ *tflpwp;

  templ* type2templ[_feat_family_nb];

}maca_graph_parser_templ_library;

/* kbest */
typedef struct {
  int k;
  int **gov; /* gov[i][ki] */
  int **label; /* label[i][ki] */
  float *score; /* score[ki] */
} maca_graph_parser_sentence_kbest;

/* should be in hyperdecoder.h */
typedef struct {
  int num; /* current size */
  int capacity; /* max size */
  DerivBP **elts; /* array of elements */
} vec_Dbp; /* vector of derivations with backpointers */

/* end kbest */

typedef struct {
  int l;
  /* arrays */
  int *words;
  int *lemmas;
  int *pos;
  int **morpho;
  int **synt_feats_array;
  int *synt_feats_nb;
  int *gov;
  int *label;
  /* */
  float score;
  void** word_adr;
  maca_graph_parser_sentence_kbest *kb;
}maca_graph_parser_sentence; 

typedef struct{
  int typesLen;
  int len;
  int order;
  /* first order features*/
  float ***pl;   /*[start][end][dir]*/
  float ****lab; /*[start][end][dir][label]*/
  /* second order features */
  float *****sib; /*[start][end][sib][dir][label] */
  float *****gra; /*[start][end][gra][dir][label] */
}maca_graph_parser_feature_table;


/* Structure qui contient les informations a conserver, ainsi que des données de controle du module */
typedef struct{
  char *module;		/*!< module name */
  char * cfg;		/*!< config/language selected */
  int verbose_flag;	/*!< verbose flag */
  maca_graph_parser_sentence *s;
  FILE* conll_file;
  /* input data */
  char *mcf_file_name;
  int sent_nb;

  /* alphabets */

  char *alphabet_file_name;
  
  maca_alphabet *words_alphabet;
  maca_alphabet *pos_alphabet;
  maca_alphabet *labels_alphabet;
  maca_alphabet *morpho_alphabet;
  maca_alphabet *synt_feats_alphabet;

  /* alphabet sizes */
  int labels_nb;
  int pos_nb;
  int words_nb;
  int morpho_nb;
  int synt_feats_nb;

  /* special values in alphabets */
  int w_start; 
  int w_end;
  int pos_start;
  int pos_end;
  int fct_joker;

  /* dep count */
  int min_dep_count;
  char *dep_count_table_file_name;
  maca_graph_parser_dep_count_table dep_count_table;

  /* model */
  char *model_file_name;
  maca_graph_parser_model *model;
  int produce_hash_model;
  int H;
  float hash_fill_rate;

  /* test: model2 */
  char *model2_file_name;
  maca_graph_parser_model *model2;

  /* feature extraction */
  maca_graph_parser_templ_library *e; /*!<pointer to the description of the feature families */
  int use_lemmas;
  int use_full_forms;
  int basic_features;
  int first_features;
  int grandchildren_features;
  int sibling_features;
  int subcat_features;
  /* mode */
  int mode;
  /* decoder */
  int order;
  int k;
  /* decoder2 */
  struct closed *****CLOSED2; /*[MACA_MAX_LENGTH_SENTENCE][MACA_MAX_LENGTH_SENTENCE][2][MACA_MAX_LENGTH_SENTENCE]*/;
  struct open *****OPEN2; /* [MACA_MAX_LENGTH_SENTENCE][MACA_MAX_LENGTH_SENTENCE][2][NB_LABELS] */
  /* decoder1 */
  struct closed ****CLOSED; /*[MACA_MAX_LENGTH_SENTENCE][MACA_MAX_LENGTH_SENTENCE][2]*/;
  struct open *****OPEN; /* [MACA_MAX_LENGTH_SENTENCE][MACA_MAX_LENGTH_SENTENCE][2][NB_LABELS] */

  /* kbest */
  Vertex ****CLOSEDK;
  Vertex *****OPENK;
  vec_Dbp ****CDERIV;
  vec_Dbp *****ODERIV;

  maca_graph_parser_feature_table *feature_table;
  /* training */
  int I;
  int algorithm; /* training algorithm */
  int max_sent_length;
  /* info */
  int print_ctx;
  /* output */
  char *file_name_out;
  FILE *file_out;
  
  /* store in feature_table the scores of the first and second order factors */
  int store_in_feature_table;

  /* mcf columns id */
  int mcf_index_id;
  int mcf_form_id;
  int mcf_lemma_id;
  int mcf_cpostag_id;
  int mcf_postag_id;
  int mcf_feats_id;
  int mcf_deprel_id;
  int mcf_head_id;
  int mcf_pdeprel_id;
  int mcf_phead_id;
  int mcf_subcat_id;
  
} maca_graph_parser_ctx;

typedef enum {PERCEPTRON_TRAINING, MIRA_TRAINING, ADAGRAD_TRAINING} TrainingAlgorithm;


#ifdef __cplusplus
extern "C"{
#endif


char * maca_graph_parser_get_model_filename(char * cfg, int order);
char * maca_graph_parser_get_alphabet_filename(char * cfg, int order);
char * maca_graph_parser_get_dep_count_filename(char * cfg, int order);
void maca_graph_parser_print_ctx(maca_graph_parser_ctx *ctx);


/** Initialize the default information structure 
 * \return a pointer on the structure maca_graph_parser_ctx
 */
maca_graph_parser_ctx * maca_graph_parser_InitCTX();

/** Initialize the information structure with the arguments
 * \param argc : number of arguments
 * \param argv : arguments (options)
 * \param mcctx : pointer on maca_common_ctx
 * \return a pointer on the structure maca_graph_parser_ctx
 */
maca_graph_parser_ctx * maca_graph_parser_LoadCTX(int argc, char ** argv);

/** free inner datastorage 
 * \param ctx : a maca_graph_parser data info structure pointer
 *
 * Remove a section and data in the maca_sentence hashtable.
 */
void maca_graph_parser_free_all(maca_graph_parser_ctx * ctx);

/** Print the graph_parser options
 */
void  maca_graph_parser_PrintHelpMessage();

/** Get the graph_parser version
 */
char * maca_tagger_GetVersion();

void maca_graph_parser_print_verbose(maca_graph_parser_ctx * mtctx, int level, int type, char * message,...);


void maca_graph_parser_init(maca_graph_parser_ctx * ctx);

#ifdef __cplusplus
}
#endif


  
/* CONSTANTS */
#define MACA_GRAPH_PARSER_ALPHABET_INVALID_CODE -1
/* #define NB_LABELS 70 */ /* maximum number of syntactic functions */
#define MINF -INFINITY
#define BasicFeatNb 500 // MACA_MAX_LENGTH_SENTENCE
#define FirstOrderFeatNb 500 // 33
#define GrandchildrenFeatNb 100 //32 
#define SiblingFeatNb 100 //32

/* parser modes */
#define DECODE_MODE 1
#define TRAIN_MODE 2
#define EVAL_MODE 3

/* directions for dependencies */
#define ra 1
#define la 0

/* features */
#define _f1 1
#define _f2 2
#define _f4 3
#define _f5 4
#define _f6 5
#define _f3 6
#define _f13 7
#define _f7 8
#define _f10 9
#define _f8 10
#define _f9 11
#define _f11 12
#define _f12 13
#define _f1l 14
#define _f2l 15
#define _f4l 16
#define _f5l 17
#define _f6l 18
#define _f3l 19
#define _f13l 20
#define _f7l 21
#define _f8l 22
#define _f10l 23
#define _f9l 24
#define _f11l 25
#define _f12l 26
#define _f14 27
#define _f15 28
#define _f16 29
#define _f17 30
#define _f18 31
#define _f19 32
#define _f20 33
#define _f39 34


#define _f21 35
#define _f22 36
#define _f23 37
#define _f24 38
#define _f25 39
#define _f26 40
#define _f27 41
#define _f28 42
#define _f29 43
#define _f24l 44
#define _f25l 45
#define _f26l 46
#define _f27l 47
#define _f28l 48
#define _f29l 49
#define _f42 50
#define _f43 51
#define _f44 52
#define _f45 53
#define _f46 54
#define _f47 55
#define _f48 56
#define _f49 57
#define _f50 58
#define _f51 59
#define _f52 60
#define _f53 61
#define _f54 62
#define _f55 63
#define _f56 64
#define _f57 65
#define _f74 66
#define _f30 67
#define _f31 68
#define _f32 69
#define _f33 70
#define _f34 71
#define _f35 72
#define _f36 73
#define _f37 74
#define _f38 75
#define _f33l 76
#define _f34l 77
#define _f35l 78
#define _f36l 79
#define _f37l 80
#define _f38l 81
#define _f58 82
#define _f59 83
#define _f60 84
#define _f61 85
#define _f62 86
#define _f63 87
#define _f64 88
#define _f65 89
#define _f66 90
#define _f67 91
#define _f68 92
#define _f69 93
#define _f70 94
#define _f71 95
#define _f72 96
#define _f73 97
#define _f75 98
#define _f76 99

/* subcat features */

#define _f77 100
#define _f78 101
#define _f79 102


/* (dependency) distance bins */
#define d1 1
#define d2 2
#define d3 3
#define d4 4
#define d5 5
#define d10 6
#define di0 7

#endif
