/*******************************************************************************
    Copyright (C) 2012 by Alexis Nasr <alexis.nasr@lif.univ-mrs.fr>
                          Ghasem Mirroshandel <ghasem.mirroshandel@lif.univ-mrs.fr>
    This file is part of maca_graph_parser.

    maca_tagger is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    maca_tagger is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with maca_tagger. If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#ifndef __MACA_GRAPH_PARSER_SENTENCE__
#define __MACA_GRAPH_PARSER_SENTENCE__

#include "maca_common.h"
#include "maca_mcf_wrapper.h"
#include "maca_constants.h"
//#include "maca_tags.h"
#include "maca_msg.h"
#include "maca_graph_parser.h"

#ifdef __cplusplus
extern "C"{
#endif


maca_graph_parser_sentence *maca_graph_parser_allocate_sentence(maca_graph_parser_ctx *ctx);
void maca_graph_parser_free_sentence(maca_graph_parser_sentence *s);
void maca_graph_parser_sentence_clear (maca_graph_parser_sentence *s);
void maca_graph_parser_sentence_add_word(maca_graph_parser_ctx * ctx, maca_graph_parser_sentence *s, void* adr, int word, int lemma, int pos, int gov, int label, int synt_feats_nb, int *synt_feats_array);
void maca_graph_parser_sentence_print_sentence(maca_graph_parser_ctx * ctx, maca_graph_parser_sentence *s);
maca_graph_parser_sentence *maca_graph_parser_duplicate(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *sent, maca_graph_parser_sentence *copy);
maca_graph_parser_sentence *maca_graph_parser_duplicate_sentence(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *sent, maca_graph_parser_sentence *copy);

void maca_graph_parser_load_sentence(maca_graph_parser_ctx * ctx, maca_graph_parser_sentence *s);
void maca_graph_parser_update_sentence(maca_graph_parser_ctx * ctx, maca_graph_parser_sentence *s);

/* kbest */
maca_graph_parser_sentence_kbest *maca_graph_parser_allocate_sentence_kbest(maca_graph_parser_ctx *ctx);
void maca_graph_parser_free_sentence_kbest(maca_graph_parser_sentence_kbest *kb);
void maca_graph_parser_reset_sentence_kbest(maca_graph_parser_sentence_kbest *kb);
maca_mcf_sentence *maca_graph_parser_read_mcf_sentence(maca_graph_parser_ctx *ctx, maca_mcf *format, maca_graph_parser_sentence *s);

void maca_graph_parser_sentence_fill_mcf_output(maca_mcf_sentence *mcf_sent, maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s);
#ifdef __cplusplus
}
#endif


#endif
