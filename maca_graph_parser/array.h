#ifndef __ARRAY__
#define __ARRAY__

#define ARRAY_TYPE char*

typedef struct {
    int num_elements;
    ARRAY_TYPE* data;
} array_t;

array_t* array_new();
void array_free(array_t* array);
ARRAY_TYPE array_get(array_t* array, int element);
void array_push(array_t* array, ARRAY_TYPE value);

#endif
