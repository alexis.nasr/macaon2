/*******************************************************************************
    Copyright (C) 2012 by Alexis Nasr <alexis.nasr@lif.univ-mrs.fr>
                          Ghasem Mirroshandel <ghasem.mirroshandel@lif.univ-mrs.fr>
    This file is part of maca_graph_parser.

    maca_tagger is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    maca_tagger is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with maca_tagger. If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#ifndef __MACA_GRAPH_PARSER_FEATURE_TABLE__
#define __MACA_GRAPH_PARSER_FEATURE_TABLE__

#include"maca_graph_parser_sentence.h"
#include"maca_graph_parser.h"
#include"maca_graph_parser_feature_vector.h"

#ifdef __cplusplus
extern "C"{
#endif


void maca_graph_parser_feature_table_allocator(maca_graph_parser_ctx *ctx);
void maca_graph_parser_feature_table_fill(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s, maca_graph_parser_feature_table *d);
void maca_graph_parser_feature_table_free(maca_graph_parser_ctx *ctx);

feat_vector *maca_graph_parser_basic_score(int gov, int dep, maca_graph_parser_ctx *ctx, feat_vector *fv_basic, float *score);
feat_vector *maca_graph_parser_first_score(int gov, int dep, int label, maca_graph_parser_ctx *ctx, feat_vector *fv_first, float *score);
feat_vector *maca_graph_parser_grandchildren_score(int gov, int dep, int gdep, int label, maca_graph_parser_ctx *ctx, feat_vector *fv, float *score);
feat_vector *maca_graph_parser_sibling_score(int gov, int dep, int sib, int label, maca_graph_parser_ctx *ctx, feat_vector *fv, float *score);
#ifdef __cplusplus
}
#endif

#endif
