#include <stdlib.h>

#include "maca_common.h"
#include "maca_constants.h"
#include "maca_msg.h"

char * maca_common_get_macaon_path()
{
    char * path;

    path = getenv(MACAON_PATH);
    if(path == NULL) path = MACAON_DIRECTORY;
    maca_print_verbose("maca_common",5,MACA_MESSAGE,"maca_common_get_macaon_path","get macaon data path : %s",path);
    return path;
}

int maca_verbose;
