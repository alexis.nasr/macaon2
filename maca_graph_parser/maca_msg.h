/***********************************************************************************
    Copyright (C) 2009-2012 by Jean-François Rey <jean-francois.rey@lif.univ-mrs.fr>
    This file is part of macaon.

    Macaon is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Macaon is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with macaon.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************************/

/**
 * \file maca_msg.h
 * \brief macaon log function
 * \author Jean-François REY
 * \version 2.5
 * \date 02 Aug 2011
 *
 */

#ifndef __MACA_MSG__
#define __MACA_MSG__

#include <stdio.h>
#include <stdarg.h>

#include "maca_constants.h"

#ifndef MACA_ERROR
#define MACA_ERROR 1
#define MACA_WARNING 2
#define MACA_MESSAGE 3
#endif

#define MACA_V_LOAD_XML "loading xml %s"
#define MACA_V_LOAD_CONFIG_XML "loading config xml file %s"
#define MACA_V_INIT "initialize data %s"
#define MACA_V_INIT_COMMON "initialize common options %s"
#define MACA_V_LEVEL "verbose level %i"
#define MACA_V_ON "verbose on %s"
#define MACA_V_FREE_MEM "free sentence %s %s section"
#define MACA_V_FREE_ALL "free all data from %s"
#define MACA_V_SENTENCE "processing sentence %s"
#define MACA_V_AUTOMATON "loading automaton %s"
#define MACA_V_UPDATE_AUTOMATON "update automaton %s"
#define MACA_V_FOUND_SECTION "found a <section> tag of type %s, ignoring it"
#define MACA_V_SECTION "processing section %s"
#define MACA_V_ADD_SECTION "add section %s"
#define MACA_V_ERROR_LOAD_SECTION "can't load %s section"
#define MACA_V_ADD_SEGS "add segments %s to %s section"
#define MACA_V_ADD_SEG "add segment %s to %s section"
#define MACA_V_ADD_XML_AUTOMATON "add xml automaton to %s section"

#ifdef __cplusplus
extern "C"{
#endif



/** Print type of message of a module
 * \param module : which module call this function
 * \param type : type of message, MACA_ERROR, MACA_WARNING or MACA_MSG
 *
 * Call this function before doing a fprintf on type output.
 */
void maca_msg(char *module, int type);

/** Print a message
 * \param module : which module call this function
 * \param type : type of message, MACA_ERROR, MACA_WARNING or MACA_MSG
 * \param function : in which function this maca_print_msg is call
 * \param message : format of the message to print (like fprintf)
 * \param ... : list of variable to print in message (like fprintf)
 */
void maca_print_msg(char * module, int type, const char * function, const char * message, ...);

/** Print verbose
 * \param module : which module call this function
 * \param level : verbose level to show
 * \param type : type of message, MACA_ERROR, MACA_WARNING or MACA_MSG
 * \param function : in which function this maca_print_msg is call
 * \param message : format of the message to print (like fprintf)
 * \param ... : list of variable to print in message (like fprintf)
 */
void maca_print_verbose(char * module, int level, int type, char * function, char * message, ...);

/** Print verbose
 * \param module : which module call this function
 * \param level : verbose level to show
 * \param type : type of message, MACA_ERROR, MACA_WARNING or MACA_MSG
 * \param function : in which function this maca_print_msg is call
 * \param message : format of the message to print (like fprintf)
 * \param va_list : list of variable to print in message
 */
void maca_print_vverbose(char * module, int level, int type, char * function, char * message, va_list *args);
#ifdef __cplusplus
}
#endif

#endif
