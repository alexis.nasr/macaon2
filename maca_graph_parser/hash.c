#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include"hash.h"

cell *cell_new(char *key, int val, cell *next)
{
  cell *c = (cell *)malloc(sizeof(cell));
  c->val = val;
  c->key = key;
  c->next = next;
  return c;
}

void cell_free(cell *c)
{
  if(c == NULL) return;
  cell_free(c->next);
  free(c->key);
  free(c);
}


hash_t *hash_new(int size)
{
  int i;
  hash_t *h = (hash_t *)malloc(sizeof(hash_t));
  h->size = size;
  h->nbelem = 0;
  h->array = (cell **)malloc(size * sizeof(cell *));
  for(i=0; i < size; i++)
    h->array[i] = NULL;
  return h;
}

void hash_free(hash_t *h)
{
  int i;
  for(i=0; i < h->size; i++)
    cell_free(h->array[i]);
  free(h);
}

int hash_func(char *key, int size)
{
  int i;
  int l = strlen(key);
  int val = key[0];
  for(i=1; i < l; i++)
    val = val + i *i * abs(key[i]);
  return val % size;
}

cell *hash_lookup(hash_t *h, char *key)
{
  int index = hash_func(key, h->size);
  cell *c;
  for(c=h->array[index]; c; c = c->next)
    if(!strcmp(key, c->key))
      return c;
  return NULL;
}

int hash_get_val(hash_t *h, char *key)
{
  int index = hash_func(key, h->size);
  cell *c;
  for(c=h->array[index]; c; c = c->next)
    if(!strcmp(key, c->key))
      return c->val;
  return HASH_INVALID_VAL;
}

void hash_add(hash_t *h, char *key, int val)
{
  int index;
  if(hash_lookup(h, key)) return;
  index = hash_func(key, h->size);
  h->array[index] = cell_new(key, val, h->array[index]);
  h->nbelem++;
}

int cell_nb(cell *c)
{
  if(c == NULL) return 0;
  return 1 + cell_nb(c->next);
}

void hash_stats(hash_t *h)
{
  int max = 0;
  int i,l;
  int *table;
  int nb;

  for(i=0; i < h->size; i++)
    if((l = cell_nb(h->array[i])) > max)
    max = l;
  nb = max + 1;
  table = (int *)malloc(nb * sizeof(int));
  for(i=0; i < nb; i++)
    table[i] = 0;
  for(i=0; i < h->size; i++)
    table[cell_nb(h->array[i])]++;
  
  for(i=0; i < nb; i++)
    printf("%d %d\n", i, table[i]);

  
}
