/*******************************************************************************
    Copyright (C) 2012 by Alexis Nasr <alexis.nasr@lif.univ-mrs.fr>
                          Ghasem Mirroshandel <ghasem.mirroshandel@lif.univ-mrs.fr>
                          Frederic Bechet <frederic.bechet@lif.univ-mrs.fr>
    This file is part of maca_graph_parser.

    maca_tagger is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    maca_tagger is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with maca_tagger. If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <math.h>

// for mmap()
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

#include "maca_graph_parser_hash.h"


/*----------------------------------------------------------------*/

#define CaseVide(indice,table) 		(table[indice]==VIDE)
#define SiEgal(valeur,indice,table)	(table[indice]==valeur)

/*----------------------------------------------------------------*/
#define TETA1   0.6180339887
//#define hash_func(clef,taille) (int)(fmodl((long double)(clef)* (long double) TETA1, (long double)1)*(taille))


#define hash_func(clef,taille) (int)((clef) % (feature_t)(taille))

/*int hash_func(feature_t clef, int taille)
{
  return (int)(clef % (feature_t)taille);
}*/


/*----------------------------------------------------------------*/

/*----------------------------------------------------------------*/

/* Recherche des nombres premiers */

/* renvoie le premier nombre premier superieur a un nombre donne en argument */

#define MaxPrem 100000

int prem_premier(int maxi )
{
  static int TablPrem[MaxPrem];
  int nbprem,n,i,limite,last;

  limite=(int)ceil(sqrt((double)maxi));
  last=1;
  nbprem=0;
  for (n=2;last<maxi;n++)
    {
      for(i=0;(i<nbprem)&&(n%TablPrem[i]!=0);i++);
      if (i==nbprem)
	{
	  if (nbprem==MaxPrem-1) { fprintf(stderr,"MaxPrem trop petit !!\n"); exit(0); }
	  last=n;
	  if (n<=limite) TablPrem[nbprem++]=n;
	}
    }

  if (last==maxi) last=prem_premier(maxi+1); /* pour le cas ou maxi est deja premier !! */
  /*printf("Le premier nombre premier superieur a %d est %d\n",maxi,last);*/
  return last;
}

/*----------------------------------------------------------------*/
/* low level functions */

static inline int lookup_node(maca_graph_parser_hash *t, feature_t clef){
  /**
   * Get the index where clef should be or should be put in t.
   */
  int len_t = t->taille;
  int indice = hash_func(clef,len_t);

  int essai;
  for(essai=1; (essai<=len_t) && (!CaseVide(indice,t->table_clef)); essai++){
    if(SiEgal(clef,indice,t->table_clef)){
      /* fprintf(stderr, "%d\n", essai); */
      return indice;
    }
    indice = (indice + 1) % len_t;
  }
  /* fprintf(stderr, "%d\n", essai); */

  return indice;
}


static inline int insert_node(maca_graph_parser_hash *t, int node_index, feature_t clef, float valeur, float valeur2){
  /**
   * Insert clef: (valeur, valeur2) in t at node_index.
   * Returns TRUE if clef did not already exist.
   */

  int already_exists = SiEgal(clef,node_index,t->table_clef);

  if(!already_exists){
    t->nbelem++;
    t->table_clef[node_index]=clef;
  }

  t->params[node_index]=valeur;
  t->total[node_index]=valeur2;
 
  return !already_exists;
}


/*----------------------------------------------------------------*/
/* high level functions */

int recherche_hash_index(maca_graph_parser_hash *t, feature_t clef){
  /**
   * Returns the index of the node where clef is in t, or -1 is clef is absent.
   */
  int node_index = lookup_node(t, clef);
  return (SiEgal(clef, node_index, t->table_clef))
    ? node_index
    : -1;
}


float *recherche_hash(maca_graph_parser_hash *t, feature_t clef){
  /**
   * Get the value associated to key clef in table table.
   *
   * This function is the hotspot of the parser (as of 2014-06).
   */

  /* basic version */
  /*
  int node_index = lookup_node(t, clef);
  if(!CaseVide(node_index,t->table_clef)){
    if(SiEgal(clef,node_index,t->table_clef)){
      return &(t->params[node_index]);
    }
    return NULL;
  }
  return NULL;
  */

  /* optimized version (manual inlining ++) */
  /* code adapted from lookup_node */
  int len_t = t->taille;
  int indice = hash_func(clef,len_t);

  int essai;
  for(essai=1; (essai<=len_t) && (!CaseVide(indice,t->table_clef)); essai++){
    if(SiEgal(clef,indice,t->table_clef)){
      //       fprintf(stderr, "%d\n", essai);
      return &(t->params[indice]);
    }
    indice = (indice + 1) % len_t;
  }
// fprintf(stderr, "%d\n", essai);
  return NULL;
}


int range_hash(maca_graph_parser_hash *t, feature_t clef, float valeur, float valeur2){
  /**
   * Insert clef: (valeur, valeur2) in t.
   * Returns -1 if t was already full, 0 if clef already existed
   * and 1 if the insertion succeeded.
   */

  int node_index = lookup_node(t, clef);

  if(!CaseVide(node_index,t->table_clef)){
    if(!SiEgal(clef,node_index,t->table_clef)){
      int len_t = t->taille;
      fprintf(stdout,"Hash table full (size = %d).\n", len_t);
      return -1;
    }
    return 0; /* key already in */
  }

  return insert_node(t, node_index, clef, valeur, valeur2);
}


/*----------------------------------------------------------------*/
maca_graph_parser_hash *load_table(FILE *f)
{
  maca_graph_parser_hash *t = malloc(sizeof(maca_graph_parser_hash));
  fread(&(t->taille), sizeof(int), 1, f);
  fread(&(t->nbelem), sizeof(int), 1, f);
  
  t->table_clef = malloc((size_t)t->taille * sizeof(feature_t));
  t->params = malloc((size_t)t->taille * sizeof(float));
  t->total = NULL;
  fread(t->table_clef, sizeof(feature_t), (size_t)t->taille, f);
  fread(t->params, sizeof(float), (size_t)t->taille, f);
  return t;
}

/*----------------------------------------------------------------*/

void dump_table(maca_graph_parser_hash *t, FILE *f)
{
  fwrite(&(t->taille), sizeof(int), 1, f);
  fwrite(&(t->nbelem), sizeof(int), 1, f);
  fwrite(t->table_clef, sizeof(feature_t), (size_t)t->taille, f);
  fwrite(t->params, sizeof(float), (size_t)t->taille, f);
}


/*----------------------------------------------------------------*/

maca_graph_parser_hash *creation_table(int nbelem, float coeff)
{
  int i;
  maca_graph_parser_hash *t = malloc(sizeof(maca_graph_parser_hash));

  t->nbelem = 0;
  /* t->taille = prem_premier((int)((float)nbelem/coeff)); */
  t->taille = (int)((double)nbelem/coeff); // cast to double: prevent non-portable "excess precision"

  t->params = malloc(sizeof(float) * (size_t)t->taille);
  t->total = malloc(sizeof(float) * (size_t)t->taille);
  t->table_clef = malloc(sizeof(feature_t) * (size_t)t->taille);

  if((t->params == NULL) || (t->total == NULL) || (t->table_clef == NULL)){
    fprintf(stderr, "memory allocation error!\n");
    exit(1);
  }

  for(i = 0; i < t->taille; i++) {
    t->table_clef[i]=VIDE;
    t->params[i]=0;
    t->total[i]=0;
  }
  return t;
}

void free_table(maca_graph_parser_hash *t)
{
  free(t->params);
  free(t->table_clef);

  if(t->total)
    free(t->total);

  free(t);
}


maca_graph_parser_feature_weight_table *feat_hash2feat_array(maca_graph_parser_hash *t)
{
  int i, j = 0;

  maca_graph_parser_feature_weight_table *table = malloc(sizeof(maca_graph_parser_feature_weight_table));
  table->size = 0;

  for(i = 0; i < t->taille; i++)
    if((t->table_clef[i] != VIDE) && (t->params[i] != 0))
      table->size++;

  table->features = malloc(table->size * sizeof(feature_t));
  table->weights = malloc(table->size * sizeof(float));
  for(i = 0; i < t->taille; i++){
    if((t->table_clef[i] != VIDE)  && (t->params[i] != 0)){
      table->features[j] = t->table_clef[i];
      table->weights[j] = t->params[i];
      j++;
    }
  }
  
  return table;
}


/*----------------------------------------------------------------*/
maca_graph_parser_feature_weight_table *load_feature_weight_table(FILE *f)
{
  maca_graph_parser_feature_weight_table *t = malloc(sizeof(maca_graph_parser_feature_weight_table));
  /* size */
  fread(&(t->size), sizeof(uint32_t), 1, f);
  /* features */
  t->features  = malloc(t->size * sizeof(feature_t));
  fread(t->features, sizeof(feature_t), t->size, f);
  /* weights */
  t->weights = malloc(t->size * sizeof(float));
  fread(t->weights, sizeof(float), t->size, f);

  return t;
}


void feature_weight_table_free(maca_graph_parser_feature_weight_table *t)
{
  free(t->features);
  free(t->weights);
  free(t);
}


void dump_feature_weight_table(maca_graph_parser_feature_weight_table *t, FILE *f)
{
  fwrite(&(t->size), sizeof(uint32_t), 1, f);
  fwrite(t->features, sizeof(feature_t), t->size, f);
  fwrite(t->weights, sizeof(float), t->size, f);
}

/*----------------------------------------------------------------*/


float *recherche_dicho(void *table, feature_t f)
{
  maca_graph_parser_feature_weight_table *t = table;
  int first = 0; /* Indice du premier élément du sous-tableau analysé */
  int last = (int)t->size - 1; /* Indice du dernier élément du sous-tableau analysé */
  int middle; /* Indice de l'élément du milieu du sous-tableau analysé */
  /* Tant qu'on a pas trouve l'élément recherché ou que le sous-tableau */
  /* contient plus de 1 élément */
  while(first <= last)
    {
      /* Calcul de la position de l'élément du milieu */
      middle=(first+last)/2;
      /* Si l'élément du milieu est l'élément recherché */

      if(t->features[middle] == f){ 
	return &(t->weights[middle]);
      }
      else
	{
	  /* Si la valeur recherchée est plus petite */
	  /* que la valeur du l'élément du milieu */
	  /* Alors on regarde le sous-tableau de gauche */
	  if(t->features[middle] > f) 
	    last = middle -1;
	  /* sinon on regarde le sous-tableau de droite*/
	  else first = middle +1;
	}
    }
  return NULL;
}


maca_graph_parser_hash *feat_array2feat_hash(maca_graph_parser_feature_weight_table *feat_array, float hash_fill_rate){

  /* printf("building hash table\n"); */
  maca_graph_parser_hash *feat_ht = creation_table(feat_array->size, hash_fill_rate);
  size_t i;
  for(i=0; i < feat_array->size; i++){
    range_hash(feat_ht, feat_array->features[i], feat_array->weights[i], 0);
  }
  /* printf("building hash table : done (%u elements)\n", i); */
  return feat_ht;
}



