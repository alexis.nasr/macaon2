#pragma once
#ifdef __cplusplus
#include <string>
#endif
extern "C"{
#include "maca_graph_parser.h"
}
#include<string>
#include<vector>

namespace macaon {
    struct ParsedWord {
        int id;
        std::string word;
        std::string lemma;
        std::string posTag;
        std::string dependencyLabel;
        int dependencyParent; // relative to word id
    };

    class Parser {
    private:
      bool loaded;
      maca_graph_parser_ctx *ctx;
    public:
      /* takes models trained for the macaon parser */

      Parser(
		 const char * cfg,		/*!< config/language selected */
		 int verbose_flag,	/*!< verbose flag */
		 const char *model_file_name,
		 const char *alphabet_file_name,
		 const char *dep_count_file_name,
		 int order);
      ~Parser();

      bool ProcessSentence(const std::vector<std::string> &words, 
			   const std::vector<std::string> &tags,
			   const std::vector<std::string> &lemmas,
			   std::vector<ParsedWord>& output);
      
      bool IsLoaded() { return loaded; }
    };

}

/* add C interface to generic parser */
extern "C" {
    macaon::Parser* Parser_new(char * cfg, int verbose_flag, char *model_file_name, char *alphabet_file_name, char *dep_count_file_name, int order);

    void Parser_free(macaon::Parser* parser);

    bool Parser_ProcessSentence(macaon::Parser* parser, int num_words, char** words, char** tags, char** lemmas, int* governors, const char** labels);
}

