/*******************************************************************************
    Copyright (C) 2012 by Alexis Nasr <alexis.nasr@lif.univ-mrs.fr>
                          Ghasem Mirroshandel <ghasem.mirroshandel@lif.univ-mrs.fr>
    This file is part of maca_graph_parser.

    maca_graph_parser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    maca_graph_parser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with maca_graph_parser. If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#include <string.h>
#include"maca_graph_parser_sentence.h"
#include"maca_graph_parser.h"
#include"maca_mcf_wrapper.h"


maca_graph_parser_sentence *maca_graph_parser_allocate_sentence(maca_graph_parser_ctx *ctx){
  /**
   * Allocate a maca_graph_parser_sentence.
   */

  maca_graph_parser_sentence *s = malloc(sizeof(maca_graph_parser_sentence));
  if(s == NULL){
    maca_msg(ctx->module, MACA_ERROR);
    fprintf(stderr,"maca_graph_parser_allocate_sentence: memory allocation problem\n");
    exit(1);
  }
  s->l = 0;

  /* dynamic allocation of fields */
  s->word_adr = malloc(MACA_MAX_LENGTH_SENTENCE * sizeof(void*));
  if(s->word_adr == NULL){
    maca_msg(ctx->module, MACA_ERROR);
    fprintf(stderr,"maca_graph_parser_allocate_sentence: memory allocation problem (2)\n");
    exit(1);
  }
  int i;
  for(i=0; i<MACA_MAX_LENGTH_SENTENCE; i++){
    s->word_adr[i] = NULL;
  }
  s->words = malloc(MACA_MAX_LENGTH_SENTENCE * sizeof(int));
  s->lemmas = malloc(MACA_MAX_LENGTH_SENTENCE * sizeof(int));
  s->pos = malloc(MACA_MAX_LENGTH_SENTENCE * sizeof(int));

  s->morpho = malloc(MACA_MAX_LENGTH_SENTENCE * sizeof(int *));
  for(i=0; i<MACA_MAX_LENGTH_SENTENCE; i++){
    s->morpho[i] = malloc(1 * sizeof(int));
    if(s->morpho[i] == NULL){
      maca_msg(ctx->module, MACA_ERROR);
      fprintf(stderr,"maca_graph_parser_allocate_sentence: memory allocation problem (3)\n");
      exit(1);
    }
  }
  s->gov = malloc(MACA_MAX_LENGTH_SENTENCE * sizeof(int));
  s->label = malloc(MACA_MAX_LENGTH_SENTENCE * sizeof(int));

  s->synt_feats_nb = malloc(MACA_MAX_LENGTH_SENTENCE * sizeof(int));

  s->synt_feats_array = malloc(MACA_MAX_LENGTH_SENTENCE * sizeof(int *));
  for(i=0; i<MACA_MAX_LENGTH_SENTENCE; i++){
    s->synt_feats_array[i] = NULL;
  }


  if((s->words == NULL) || (s->lemmas == NULL) || (s->pos == NULL) ||
     (s->morpho == NULL) || (s->gov == NULL) || (s->label == NULL)){
    maca_msg(ctx->module, MACA_ERROR);
    fprintf(stderr,"maca_graph_parser_allocate_sentence: memory allocation problem (4)\n");
    exit(1);
  }
  /* s->score = 0; */
  s->kb = NULL;

  /* add fake root */
  int root_w = (ctx->use_full_forms || ctx->use_lemmas) ? maca_alphabet_get_code(ctx->words_alphabet, "__START__") : -1;
  /* int root_pos = maca_tags_get_code(ctx->cfg, "morpho", "stype", "__START__"); */
  int root_pos = maca_alphabet_get_code(ctx->pos_alphabet, "__START__");
  maca_graph_parser_sentence_add_word(ctx, s, NULL, root_w, root_w, root_pos, 0, -1, 0, NULL);
  /* MM: gov 0, label -1: if these default values change, make sure they get backported to the call to the same function _sentence_add_word in the preceding function,
   to ensure consistency */

  return s;
}


void maca_graph_parser_free_sentence(maca_graph_parser_sentence *s){
  /**
   * Free a sentence
   */
  int i;

  if(s == NULL)
    return;

  if(s->word_adr != NULL){
    free(s->word_adr);
    s->word_adr = NULL;
  }
  if(s->words != NULL){
    free(s->words);
    s->words = NULL;
  }
  if(s->lemmas != NULL){
    free(s->lemmas);
    s->lemmas = NULL;
  }
  if(s->pos != NULL){
    free(s->pos);
    s->pos = NULL;
  }
  if(s->morpho != NULL){
    for(i=0; i < MACA_MAX_LENGTH_SENTENCE; i++){
      if(s->morpho[i] != NULL){
	free(s->morpho[i]);
	s->morpho[i] = NULL;
      }
    }
    free(s->morpho);
    s->morpho = NULL;
  }
  if(s->gov != NULL){
    free(s->gov);
    s->gov = NULL;
  }
  if(s->label != NULL){
    free(s->label);
    s->label = NULL;
  }

  if(s->synt_feats_nb != NULL){
    free(s->synt_feats_nb);
    s->synt_feats_nb = NULL;
  }


  if(s->synt_feats_array != NULL){
    for(i=0; i < MACA_MAX_LENGTH_SENTENCE; i++){
      if(s->synt_feats_array[i] != NULL){
	free(s->synt_feats_array[i]);
	s->synt_feats_array[i] = NULL;
      }
    }
    free(s->synt_feats_array);
    s->synt_feats_array = NULL;
  }



  if(s->kb != NULL){
    maca_graph_parser_free_sentence_kbest(s->kb);
    s->kb = NULL;
  }

  free(s);
}

void maca_graph_parser_sentence_clear (maca_graph_parser_sentence *s) {
  int k;
  
  for (k = 1; k < s->l; k++) {
    free(s->synt_feats_array[k]);
    s->synt_feats_array[k] = NULL;
    s->synt_feats_nb[k] = 0;
  }

  s->l = 1;
}

void maca_graph_parser_sentence_add_word(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s, void* adr, int word, int lemma, int pos, int gov, int label, int synt_feats_nb, int *synt_feats_array){
  /**
   * Add word to sentence.
   */
  int i;

  if(s->l >= MACA_MAX_LENGTH_SENTENCE){
    maca_msg(ctx->module, MACA_ERROR);
    fprintf(stderr, "sentence too long, change MACA_MAX_LENGTH_SENTENCE value\n");
    exit(1);
  }
  s->word_adr[s->l] = adr;
  s->words[s->l] = word;
  s->lemmas[s->l] = lemma;
  s->pos[s->l] = pos;
  s->gov[s->l] = gov;
  s->label[s->l] = label;
  s->synt_feats_nb[s->l] = synt_feats_nb;
  if(synt_feats_nb > 0){
    s->synt_feats_array[s->l] = malloc(synt_feats_nb * sizeof(int));
    for(i=0; i<synt_feats_nb; i++){
      s->synt_feats_array[s->l][i] = synt_feats_array[i];
    }
  }
  s->l++;
}


void maca_graph_parser_sentence_print_sentence(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s){
  /**
   * Print s.
   */

  int i;
  char word[128];
  char lemma[128];
  char pos[128];
  char label[128];


  for(i=1; i < s->l; i++){
    if (s->words[i] != -1) {
      maca_alphabet_get_symbol(ctx->words_alphabet, s->words[i], word, sizeof(word));
    } else {
      strcpy(word, "NA");
    }
    if (s->lemmas[i] != -1) {
      maca_alphabet_get_symbol(ctx->words_alphabet, s->lemmas[i], lemma, sizeof(lemma));
    } else {
      strcpy(lemma, "NA");
    }
    if (s->pos[i] != -1) {
      maca_alphabet_get_symbol(ctx->pos_alphabet, s->pos[i], pos, sizeof(pos));
    } else {
      strcpy(pos, "NA");
    }
    if (s->label[i] != -1) {
      maca_alphabet_get_symbol(ctx->labels_alphabet, s->label[i], label, sizeof(label));
    } else {
      strcpy(label, "NA");
    }
    printf("%d\t%s\t%s\t%s\t%d\t%s\n",
	   i,
	   word,
	   lemma,
	   pos,
	   s->gov[i],
	   label);
  }
  printf("\n");
}


maca_graph_parser_sentence *maca_graph_parser_duplicate(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *sent, maca_graph_parser_sentence *copy){
  /**
   * Duplicate sent to copy.
   */

  int i;

  if(sent == NULL) return NULL;

  if(copy == NULL){
    copy = maca_graph_parser_allocate_sentence(ctx);
  }
  
  copy->l = 0;

  for(i=0; i < sent->l; i++){
    copy->word_adr[i] = sent->word_adr[i];
    copy->words[i] = sent->words[i];
    copy->lemmas[i] = sent->lemmas[i];
    copy->pos[i] = sent->pos[i];
    copy->gov[i] = sent->gov[i];
    copy->label[i] = sent->label[i];
    copy->l++;
  } 
  /* copy score too (shouldn't we ?) */
  copy->score = sent->score;

  return copy;
}


maca_graph_parser_sentence *maca_graph_parser_duplicate_sentence(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *sent, maca_graph_parser_sentence *copy){
  /**
   * Duplicate sent to copy without the dependencies (gov and label)
   *
   * FIXME: merge into maca_graph_parser_duplicate()
   */

  int i,j;

  if(sent == NULL) return NULL;

  if(copy == NULL){
    copy = maca_graph_parser_allocate_sentence(ctx);
  }
  
  copy->l = 0;

  for(i=0; i < sent->l; i++){
    copy->word_adr[i] = sent->word_adr[i];
    copy->words[i] = sent->words[i];
    copy->lemmas[i] = sent->lemmas[i];
    copy->pos[i] = sent->pos[i];
    copy->gov[i] = 0; /* MM: ensure consistency with calls to _sentence_add_word in the functions above: gov=0, lab=-1 by default */
    copy->label[i] = -1;
    copy->synt_feats_nb[i] = sent->synt_feats_nb[i];
    if(copy->synt_feats_nb[i] > 0){
      copy->synt_feats_array[i] = malloc(copy->synt_feats_nb[i] * sizeof(int));
      for(j=0; j<copy->synt_feats_nb[i]; j++){
	copy->synt_feats_array[i][j] = sent->synt_feats_array[i][j];
      }
    }
    copy->l++;
  }
  /* reset score */
  copy->score = MINF;

  return copy;
}




/**
 * kbest
 */

maca_graph_parser_sentence_kbest *maca_graph_parser_allocate_sentence_kbest(maca_graph_parser_ctx *ctx){
  /**
   * Allocate a structure to store the k-best parses of a sentence.
   */
  
  maca_graph_parser_sentence_kbest *kb = malloc(sizeof(maca_graph_parser_sentence_kbest));
  if(kb == NULL){
    maca_msg(ctx->module, MACA_ERROR);
    fprintf(stderr,"maca_graph_parser_allocate_sentence_kbest: memory allocation problem\n");
    exit(1);    
  }

  /* fields */
  kb->k = ctx->k;
  kb->gov = malloc(MACA_MAX_LENGTH_SENTENCE * sizeof(int *));
  if(kb->gov == NULL){
    maca_msg(ctx->module, MACA_ERROR);
    fprintf(stderr,"maca_graph_parser_allocate_sentence_kbest: memory allocation problem (2)\n");
    exit(1);
  }
  kb->label = malloc(MACA_MAX_LENGTH_SENTENCE * sizeof(int *));
  if(kb->label == NULL){
    maca_msg(ctx->module, MACA_ERROR);
    fprintf(stderr,"maca_graph_parser_allocate_sentence_kbest: memory allocation problem (3)\n");
    exit(1);
  }

  int ki;
  int i;
  for(i=0; i<MACA_MAX_LENGTH_SENTENCE; i++){
    kb->gov[i] = malloc((kb->k) * sizeof(int));
    kb->label[i] = malloc((kb->k) * sizeof(int));
    for(ki=0; ki<kb->k; ki++){
      kb->gov[i][ki] = 0;
      kb->label[i][ki] = -1;
    }
  }
  kb->score = malloc((kb->k) * sizeof(float));
  for(ki=0; ki<kb->k; ki++){
    kb->score[ki] = MINF;
  }
  
  return kb;
}


void maca_graph_parser_free_sentence_kbest(maca_graph_parser_sentence_kbest *kb){
  /**
   * Free a sentence_kbest.
   */



  int i;

  if(kb == NULL)
    return;
  
  for(i=0; i<MACA_MAX_LENGTH_SENTENCE; i++){
    if(kb->gov[i] != NULL){
      free(kb->gov[i]);
      kb->gov[i] = NULL;
    }
    if(kb->label[i] != NULL){
      free(kb->label[i]);
      kb->label[i] = NULL;
    }
  }
  if(kb->score != NULL){
    free(kb->score);
    kb->score = NULL;
  }

  if(kb->gov != NULL){
    free(kb->gov);
    kb->gov = NULL;
  }
  if(kb->label != NULL){
    free(kb->label);
    kb->label = NULL;
  }
  free(kb);
}


void maca_graph_parser_reset_sentence_kbest(maca_graph_parser_sentence_kbest *kb){
  /**
   * Reset a sentence_kbest.
   */

  int i;
  int ki;

  for(i=1; i < MACA_MAX_LENGTH_SENTENCE; i++){
    for(ki=0; ki < kb->k; ki++){
      kb->gov[i][ki] = 0;
      kb->label[i][ki] = -1;
    }
  }
  for(ki=0; ki < kb->k; ki++){
    kb->score[ki] = MINF;
  }

}

maca_mcf_sentence *maca_graph_parser_read_mcf_sentence(maca_graph_parser_ctx *ctx, maca_mcf *format, maca_graph_parser_sentence *s)
{
  maca_mcf_sentence *mcf_sentence;
  int length;
  maca_mcf_word *mcf_word = NULL;
  int index, k;
  int code_postag, code_lemma, code_form, code_label, gov;
  int nb_synt_feats;
  int *synt_feats;
  char invalid_sentence;

  do {
    invalid_sentence = 0;
    do{
      mcf_sentence = maca_mcf_read_next_sentence(format);
      if(mcf_sentence == NULL) return NULL;
      length = maca_mcf_sentence_get_length(mcf_sentence);
      if(length > ctx->max_sent_length){
	maca_mcf_sentence_release(mcf_sentence);
	if(ctx->verbose_flag > 1){
	  maca_msg(ctx->module, MACA_ERROR);
	  fprintf(stderr, "sentence too long, skipping it\n");
	}      
      }
    }
    while(length > ctx->max_sent_length);

    for(index=1; index < length; index++){
      mcf_word = maca_mcf_sentence_get_word(mcf_sentence, index);

      code_form = -1;
      if(ctx->use_full_forms){
	if(maca_mcf_word_get_nb_values(mcf_word, ctx->mcf_form_id)){
	  code_form = maca_mcf_word_get_value_int(mcf_word, ctx->mcf_form_id, 0);
	}
	else{
	  if(ctx->verbose_flag > 1){
	    maca_msg(ctx->module, MACA_WARNING);
	    fprintf(stderr, "missing FORM in sentence, skipping it\n");
	    invalid_sentence = 1;
	  }      
	}
      }
    
      code_lemma = -1;
      if(ctx->use_lemmas){
	if(maca_mcf_word_get_nb_values(mcf_word, ctx->mcf_lemma_id)){
	  code_lemma = maca_mcf_word_get_value_int(mcf_word, ctx->mcf_lemma_id, 0);
	}
      }
    
      code_postag = -1;
      if (maca_mcf_word_get_nb_values(mcf_word, ctx->mcf_postag_id)) {
	code_postag = maca_mcf_word_get_value_int(mcf_word, ctx->mcf_postag_id, 0);
      } else {
	if(ctx->verbose_flag > 1){
	  maca_msg(ctx->module, MACA_WARNING);
	  fprintf(stderr, "missing POSTAG in sentence, skipping it\n");
	  invalid_sentence = 1;
	}
      }
    
      gov = -1;
      if (maca_mcf_word_get_nb_values(mcf_word, ctx->mcf_head_id)) {
	gov = maca_mcf_word_get_value_int(mcf_word, ctx->mcf_head_id, 0);
      } else {
	if(ctx->verbose_flag > 1){
	  maca_msg(ctx->module, MACA_WARNING);
	  fprintf(stderr, "missing HEAD in sentence, skipping it\n");
	  invalid_sentence = 1;
	}
      }
    
      code_label = -1;
      if (maca_mcf_word_get_nb_values(mcf_word, ctx->mcf_deprel_id)) {
	code_label = maca_mcf_word_get_value_int(mcf_word, ctx->mcf_deprel_id, 0);
      } else {
	if(ctx->verbose_flag > 1){
	  maca_msg(ctx->module, MACA_WARNING);
	  fprintf(stderr, "missing DEPREL in sentence, skipping it\n");
	  invalid_sentence = 1;
	}
      }

      synt_feats = NULL;
      nb_synt_feats = 0;
      if(ctx->subcat_features){
	nb_synt_feats = maca_mcf_word_get_nb_values(mcf_word, ctx->mcf_subcat_id);
	if (nb_synt_feats) {
	  synt_feats = malloc(nb_synt_feats * sizeof(int));
	  for (k = 0; k < nb_synt_feats; k++) {
	    synt_feats[k] = maca_mcf_word_get_value_int(mcf_word, ctx->mcf_subcat_id, k);
	  }
	}
      }

      /* Sentence is invalid, clear it and search for another one */
      if (invalid_sentence) {
	maca_graph_parser_sentence_clear(s);
	break;
      }
    
      maca_graph_parser_sentence_add_word(ctx, s, NULL, code_form, code_lemma, 
					  code_postag, gov, code_label, 
					  nb_synt_feats, synt_feats); 
    }
  } while (invalid_sentence);
  return mcf_sentence;
}

void maca_graph_parser_sentence_fill_mcf_output(maca_mcf_sentence *mcf_sent,
						maca_graph_parser_ctx *ctx,
						maca_graph_parser_sentence *s) {
  int k;
  maca_mcf_word *mcf_word;
  char buffer[128];
  for (k = 1; k < s->l; k++) {
    mcf_word = maca_mcf_sentence_get_word(mcf_sent, k);
    maca_mcf_word_clear_values(mcf_word, ctx->mcf_head_id);
    maca_mcf_word_add_value_int(mcf_word, ctx->mcf_head_id, s->gov[k]);
    maca_mcf_word_clear_values(mcf_word, ctx->mcf_deprel_id);
    maca_alphabet_get_symbol(ctx->labels_alphabet, s->label[k], buffer, sizeof(buffer));
    maca_mcf_word_add_value_alphabet(mcf_word, ctx->mcf_deprel_id, buffer, ctx->labels_alphabet);
  }
}
