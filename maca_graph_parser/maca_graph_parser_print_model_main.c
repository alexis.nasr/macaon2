#include"maca_graph_parser_hash.h"
#include"maca_graph_parser_model.h"
#include"maca_graph_parser_features.h"
#include"maca_alphabet_wrapper.h"
#include"maca_constants.h"

int main(int argc, char *argv[])
{
  int i,j;
  FILE *f = stdout;
  maca_graph_parser_ctx * ctx;
  /* int hval; */
  maca_alphabet_array *alpha_array;
  maca_graph_parser_model *model = NULL;

  ctx = maca_graph_parser_LoadCTX(argc,argv);


  /* alphabets */
  /* load alphabets */

  alpha_array = maca_alphabet_array_new_from_file(ctx->alphabet_file_name);
  ctx->words_alphabet = maca_alphabet_array_get_alphabet(alpha_array, MACA_ALPHABET_WORDS);
  ctx->words_nb = (ctx->words_alphabet != NULL) ? maca_alphabet_size(ctx->words_alphabet) : 0;
  ctx->labels_alphabet = maca_alphabet_array_get_alphabet(alpha_array, MACA_ALPHABET_LABELS);
  ctx->labels_nb = (ctx->labels_alphabet != NULL) ? maca_alphabet_size(ctx->labels_alphabet) : 0;
  ctx->pos_alphabet = maca_alphabet_array_get_alphabet(alpha_array, MACA_ALPHABET_POS);
  ctx->pos_nb = (ctx->pos_alphabet != NULL) ? maca_alphabet_size(ctx->pos_alphabet) : 0;
  ctx->morpho_alphabet = maca_alphabet_array_get_alphabet(alpha_array, MACA_ALPHABET_MORPHO);
  ctx->morpho_nb = (ctx->morpho_alphabet != NULL) ? maca_alphabet_size(ctx->morpho_alphabet) : 0;
  ctx->synt_feats_alphabet = maca_alphabet_array_get_alphabet(alpha_array, MACA_ALPHABET_SYNT_FEATS);
  ctx->synt_feats_nb = (ctx->synt_feats_alphabet != NULL) ? maca_alphabet_size(ctx->synt_feats_alphabet) : 0;

  /* template library allocator needs: words_nb, pos_nb, labels_nb */
  ctx->e = maca_graph_parser_templ_library_allocator(ctx);
    
  /* model */

  model = ctx->model = maca_graph_parser_model_load(ctx, ctx->model_file_name);

  /* model2 */
  if(ctx->model2_file_name != NULL){
    ctx->model2 = maca_graph_parser_model_load(ctx, ctx->model2_file_name);
  } else {
    ctx->model2 = NULL;
  }
  /* set active feature types for the decoder */
  ctx->min_dep_count = ctx->model->min_dep_count;
  ctx->use_lemmas = ctx->model->use_lemmas;
  ctx->use_full_forms = ctx->model->use_full_forms;
  ctx->basic_features = ctx->model->basic_features;
  ctx->first_features = ctx->model->first_features;
  ctx->grandchildren_features = ctx->model->grandchildren_features;
  ctx->sibling_features = ctx->model->sibling_features;
  ctx->subcat_features = ctx->model->subcat_features;


  fprintf(f, "is hash model \t = %d\n", model->is_hash_model);
  fprintf(f, "min dep count \t = %d\n", ctx->min_dep_count);
  fprintf(f, "use lemmas \t = %d\n", ctx->use_lemmas);
  fprintf(f, "use full forms \t = %d\n", ctx->use_full_forms);
  fprintf(f, "basic features \t = %d\n", ctx->basic_features);
  fprintf(f, "first order features  \t = %d\n", ctx->basic_features);
  fprintf(f, "grandchildre features \t = %d\n", ctx->grandchildren_features);
  fprintf(f, "sibling features \t = %d\n", ctx->sibling_features);
  fprintf(f, "subcat feautres \t = %d\n", ctx->subcat_features);

  fprintf(f, "features hash table size \t = %d\n", model->feat_ht->taille);
  fprintf(f, "nb of features \t = %d\n", model->feat_ht->nbelem);
  for(i=0, j=0; i < ctx->model->feat_ht->taille; i++){
      j++;
      if(ctx->model->feat_ht->table_clef[i] != VIDE){
	 fprintf(f, "%d\t%f\t", ++j, ctx->model->feat_ht->params[i]); 
/* 	hval = hash_func(ctx->model->feat_ht->table_clef[i],ctx->model->feat_ht->taille); */
/* 	fprintf(f, "%f h=%d", ctx->model->feat_ht->params[i],hval); */
	maca_graph_parser_print_feature(f, ctx, ctx->model->feat_ht->table_clef[i]);
	
	fprintf(f, "\n");
	/* fprintf(f, "%d %lld %f\n", ++j, ctx->model->feat_ht->table_clef[i], ctx->model->feat_ht->params[i]); */
	/* fprintf(f, "%d %lld %f type = %lld\n", ++j, feat_ht->table_clef[i], feat_ht->params[i], feature_get_type(feat_ht->table_clef[i])); */
      }
    }
  maca_graph_parser_model_free(ctx->model);
  return 0;
}
