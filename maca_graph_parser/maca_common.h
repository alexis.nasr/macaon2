#ifndef __MACA_COMMON__
#define __MACA_COMMON__


/** maca_common control data structure */
typedef struct {
    char * cfg;		/*!< Config name */
    
} maca_common_ctx;

maca_common_ctx* maca_init(int argc, char** argv);
char * maca_common_get_macaon_path();

extern int maca_verbose;

#endif
