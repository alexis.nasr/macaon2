/***************************************************************************
    Copyright (C) 2012 by Alexis Nasr <alexis.nasr@lif.univ-mrs.fr>
                          Ghasem Mirroshandel <ghasem.mirroshandel@lif.univ-mrs.fr>
    This file is part of maca_graph_parser.

    maca_graph_parser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    maca_graph_parser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with maca_graph_parser. If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

#include "maca_graph_parser_dep_count_table.h"
#include "maca_msg.h"

#include<stdio.h>
#include<stdlib.h>

#define MAX_LENGTH_CLASSES 10
#define LENGTH_CLASS_UNIT 4

int maca_graph_parser_dep_count_table_compute_length_class(int gov, int dep){
  int len;
  if(gov > dep){
    len = (gov - dep) / LENGTH_CLASS_UNIT;
    if (len < MAX_LENGTH_CLASSES) return len; else return MAX_LENGTH_CLASSES - 1;
  }
  else{
    len = (dep - gov) / LENGTH_CLASS_UNIT;
    if (len < MAX_LENGTH_CLASSES) return len; else return MAX_LENGTH_CLASSES - 1;
  }
}

maca_graph_parser_dep_count_table maca_graph_parser_dep_count_table_allocate(int pos_nb, int synt_labels_nb)
{
  maca_graph_parser_dep_count_table t;
  int gov, dep, label, length_class;

  t = malloc((size_t)pos_nb * sizeof(int ****));
  for(gov = 0; gov < pos_nb; gov++){
    t[gov] = malloc((size_t)pos_nb * sizeof(int ***));
    for(dep = 0; dep < pos_nb; dep++){
      t[gov][dep] = malloc((size_t)synt_labels_nb * sizeof(int **));
      for(label = 0; label < synt_labels_nb; label++){
	t[gov][dep][label] = malloc(MAX_LENGTH_CLASSES * sizeof (int *));
	for(length_class=0; length_class < MAX_LENGTH_CLASSES; length_class++){
	  t[gov][dep][label][length_class] = malloc(2 * sizeof(int));
	  t[gov][dep][label][length_class][0] = 0;
	  t[gov][dep][label][length_class][1] = 0;
	}      
      }
    }
  }
  return t;
}

void maca_graph_parser_dep_count_table_free(int pos_nb, int synt_labels_nb, maca_graph_parser_dep_count_table t)
{
  int gov, dep, label, length_class;
  
  for(gov = 0; gov < pos_nb; gov++){
    for(dep = 0; dep < pos_nb; dep++){
      for(label = 0; label < synt_labels_nb; label++){
	for(length_class=0; length_class < MAX_LENGTH_CLASSES; length_class++){
	  free(t[gov][dep][label][length_class]);
	}
	free(t[gov][dep][label]);
      }
      free(t[gov][dep]);
    }
    free(t[gov]);
  }
  free(t);
}

maca_graph_parser_dep_count_table maca_graph_parser_dep_count_table_read(maca_graph_parser_ctx * ctx, char *filename)
{
  FILE *f;
  maca_graph_parser_dep_count_table t;
  int gov, dep, label, dir, count, length_class;
  int fields_nb;
  t = maca_graph_parser_dep_count_table_allocate(ctx->pos_nb, ctx->labels_nb);
  f = fopen(filename, "rb");
  if(f == NULL){
    maca_msg(ctx->module, MACA_ERROR);
    fprintf(stderr, "cannot open file %s\n", filename);
    exit(1);
  }
  while((fields_nb = fscanf(f, "%d\t%d\t%d\t%d\t%d\t%d\n", &gov, &dep, &label, &length_class, &dir, &count)) != EOF){
    if(fields_nb != 6){
      maca_msg(ctx->module, MACA_ERROR);
      fprintf(stderr, "wrong number of fields in file %s (expected %d, found %d)\n", filename, 6, fields_nb);
      exit(1);
    }
    //fprintf(stderr, "%d %d %d %d %d %d\n", gov, dep, label, length_class, dir, count);
    t[gov][dep][label][length_class][dir] = count;
  }
  fclose(f);
  return t;
}

void maca_graph_parser_dep_count_table_print(maca_graph_parser_ctx * ctx, char *filename)
{
  FILE *f;
  maca_graph_parser_dep_count_table t = ctx->dep_count_table;
  int gov, dep, label, length_class;
  /* int dir, count; */
  
  if(filename == NULL) 
    f = stdout;
  else{
    f = fopen(filename, "w");
    if(f == NULL){
      maca_msg(ctx->module, MACA_ERROR);
      fprintf(stderr, "cannot open file %s\n", filename);
      exit(1);
    }
  }
  for(gov = 0; gov < ctx->pos_nb; gov++){
    for(dep = 0; dep < ctx->pos_nb; dep++){
      for(label = 0; label < ctx->labels_nb; label++){
	for(length_class=0; length_class < MAX_LENGTH_CLASSES; length_class++){
	  if(t[gov][dep][label][length_class][la])fprintf(f, "%d\t%d\t%d\t%d\t%d\t%d\n", gov, dep, label, length_class, la, t[gov][dep][label][length_class][la]); 
	  if(t[gov][dep][label][length_class][ra])fprintf(f, "%d\t%d\t%d\t%d\t%d\t%d\n", gov, dep, label, length_class, ra, t[gov][dep][label][length_class][ra]); 
	}
      }
    }
  }
  if(filename)
    fclose(f);
}

void maca_graph_parser_dep_count_table_update(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s)
{
  int i;
  int dir;
  maca_graph_parser_dep_count_table t = ctx->dep_count_table;
  int length_class;
  
  for(i=1; i < s->l; i++){
    dir = (s->gov[i] < i) ? ra : la;
    if((s->pos[i] >= 0) && (s->label[i] >= 0) && (s->pos[s->gov[i]] >= 0)){
      length_class = maca_graph_parser_dep_count_table_compute_length_class(s->gov[i], i);
      /* fprintf(stderr, "length class = %d\n", length_class); */
      t[s->pos[s->gov[i]]][s->pos[i]][s->label[i]][length_class][dir]++;
    }
  }
}
