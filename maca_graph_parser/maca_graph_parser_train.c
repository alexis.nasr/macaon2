/***************************************************************************
    Copyright (C) 2012 by Alexis Nasr <alexis.nasr@lif.univ-mrs.fr>
                          Ghasem Mirroshandel <ghasem.mirroshandel@lif.univ-mrs.fr>
    This file is part of maca_graph_parser.

    maca_graph_parser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    maca_graph_parser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with maca_graph_parser. If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

#include "maca_graph_parser_train.h"

#include "maca_graph_parser_sentence.h"
#include "maca_graph_parser_alphabet.h"
#include "maca_graph_parser_decoder.h"
#include "maca_graph_parser_metrics.h"
#include "maca_graph_parser_features.h"
#include "maca_graph_parser_hash.h"
#include "maca_graph_parser_model.h"
// #include <inttypes.h>


void update_weights(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *ref_s, maca_graph_parser_sentence *hyp_s, feature_counter_array *ref_feat_cnt_array, feature_counter_array *hyp_feat_cnt_array, int upd){
  /**
   * Update the weights in ctx->model->feat_ht.
   */

  /* check for "non-violation", i.e. score(hyp) < score(ref) */
  /* with the projective decoder, this can happen
     if ref is non-projective */
  /* TODO: check that it only happens in this case */

  float score_ref = score_feature_counter_array(ref_feat_cnt_array, ctx->model);
  float score_hyp = score_feature_counter_array(hyp_feat_cnt_array, ctx->model);

  if(score_hyp < score_ref){ 
    /* baseline strategy: skip update */
    return;
  }

  if(ctx->verbose_flag > 4){
    switch(ctx->algorithm) {
    case PERCEPTRON_TRAINING: fprintf(stderr, ">>> perceptron update\n"); break;
    case MIRA_TRAINING: fprintf(stderr, ">>> mira update\n"); break;
    case ADAGRAD_TRAINING: fprintf(stderr, ">>> adagrad update\n"); break;
    }
  }

  /* learning rate (= step size) */
  double alpha = 1.0; /* default value is 1 */
  /* ref feature counter - hyp feature counter */
  feature_counter_array *ref_minus_hyp = feature_counter_array_difference(ctx, ref_feat_cnt_array, hyp_feat_cnt_array);

  if(ctx->algorithm == MIRA_TRAINING) { /* MIRA adapts step size */
    double C_aggr = 1.0; /* aggressiveness parameter */

    float lam_dist = score_ref - score_hyp; /* lam_dist is <= 0 */
    /* cost of choosing a wrong hyp over the ref ; after update, the
       minimal margin between ref and hyp must be >= this cost */
    double err = maca_graph_parser_sentence_errors(ref_s, hyp_s) + 1; /* why + 1 ? */ /* err is > 0 */
    /* structured loss */
    float b = (float)err - lam_dist; /* b is > 0 */
    /* square norm of the distance vector */
    int dist = feature_counter_array_squared_norm(ref_minus_hyp);
    /* PA-I update: alpha = min(C_aggr, b/dist) */
    alpha = (dist == 0)? 0.0 : ((double) b / (double) dist); /* default was 1 */
    if(alpha > C_aggr) alpha = C_aggr;
    if(alpha < 0) alpha = 0.0; /* useless as at this point, b and dist are both guaranteed >= 0 */

    if(ctx->verbose_flag > 4) fprintf(stderr, "score ref = %f score hyp = %f diff = %f err = %lf dist = %d alpha = %lf\n", score_ref, score_hyp, lam_dist, err, dist, alpha);
  }
  
  /* update features */
  int i, j;
  feature_counter *fc;
  feature_t ft;
  int c;
  int index;
  double new_param;

  for(i=0; i < ref_minus_hyp->size; i++){
    fc = ref_minus_hyp->array[i];
    if(fc){
      for(j=0; j < fc->size; j++){
	c = fc->values[j]; /* feature count */
	/* update weight of features with non-zero counts */
	if(c != 0){
	  ft = fc->keys[j];
	  index = recherche_hash_index(ctx->model->feat_ht, ft);
	  /* printf("      key: %" PRIu64 "\n", f); */
	  if(index == -1){ /* init new feature */
	    if(ctx->algorithm == ADAGRAD_TRAINING) {
	      range_hash(ctx->model->feat_ht, ft, alpha, (c*c));
	    } else {
	      range_hash(ctx->model->feat_ht, ft, (alpha*c), (upd*alpha*c));
	    }
	  } else {
	    if(ctx->algorithm == ADAGRAD_TRAINING) {
	      ctx->model->feat_ht->total[index] += (c*c);
	      new_param = (double) ctx->model->feat_ht->params[index] + (alpha*c) / sqrt((double) ctx->model->feat_ht->total[index]);
	      if(!isnan(new_param)) ctx->model->feat_ht->params[index] = (float) new_param;
	    } else {
	      ctx->model->feat_ht->params[index] += (alpha*c); 
	      ctx->model->feat_ht->total[index] += (upd*alpha*c);
	    }
	  }
	  /* printf("      ref index %d: (params %f, total %f)\n", index, ctx->model->feat_ht->params[index], ctx->model->feat_ht->total[index]); */
	}
      }
    }
  }
 
  /* cleanup */
  if(ref_minus_hyp){
    free_feature_counter_array(ref_minus_hyp);
    ref_minus_hyp = NULL;
  }
}


void maca_graph_parser_train(maca_graph_parser_ctx *ctx, hyp_ref_vector *corpus){
  /**
   * Train a parsing model on corpus.
   */

  int i;
  int sent_id;
  int upd; /* update counter */
  int l;
  double e; /* error(ref, hyp) */
  double score = 0.0;

  maca_graph_parser_sentence *ref_s = NULL;
  maca_graph_parser_sentence *hyp_s = NULL;

  feature_counter_array *hyp_feat_cnt_array = NULL;
  feature_counter_array *ref_feat_cnt_array = NULL;
  
  if(corpus == NULL){
    maca_msg(ctx->module, MACA_ERROR);
    fprintf(stderr, "maca_graph_parser_train: corpus is NULL\n");
    exit(1);
  }

  upd = (ctx->I * corpus->size) + 1;

  for(i=0; i < ctx->I; i++){
    /* TODO: permute sentences in each iteration */
    for(sent_id=0; sent_id < corpus->size; sent_id++){
      ref_s = corpus->ref[sent_id];
      hyp_s = corpus->hyp[sent_id];

      /* decrease update counter */
      upd--;

      /* parse */
      maca_graph_parser_decoder_parse(ctx, hyp_s);

      /* if hyp != ref, update weights */
      e = maca_graph_parser_sentence_errors(ref_s, hyp_s);
      if (e > 0.0){
	ref_feat_cnt_array = extract_features_from_parse_fca(ctx, ref_s, ref_feat_cnt_array); 
	hyp_feat_cnt_array = extract_features_from_parse_fca(ctx, hyp_s, hyp_feat_cnt_array);
	
	update_weights(ctx, ref_s, hyp_s, ref_feat_cnt_array, hyp_feat_cnt_array, upd);
      }

      /* progress monitoring */
      score = maca_graph_parser_sentence_compute_las(ref_s, hyp_s, 0);
      printf("[%d][%d] %d %lf %d\n", i, sent_id, (ref_s->l - 1), score, ctx->model->feat_ht->nbelem);
    }
    /* TODO: average weights after each iteration */
  }
  
  /* final weight averaging */
  l = ctx->I * corpus->size;
  if(ctx->algorithm == PERCEPTRON_TRAINING || ctx->algorithm == MIRA_TRAINING) {
    for(i=0; i < ctx->model->feat_ht->taille; i++){
      ctx->model->feat_ht->params[i] = ctx->model->feat_ht->total[i] / l;
      /*    if(ctx->model->feat_ht->params[i] != 0)
	    fprintf(stderr, "feature weight = %f\n", ctx->model->feat_ht->params[i]);*/
    }
  }

  /* free used feature matrices */
  if(hyp_feat_cnt_array){
    free_feature_counter_array(hyp_feat_cnt_array);
    hyp_feat_cnt_array = NULL;
  }
  if(ref_feat_cnt_array){
    free_feature_counter_array(ref_feat_cnt_array);
    ref_feat_cnt_array = NULL;
  }
}
