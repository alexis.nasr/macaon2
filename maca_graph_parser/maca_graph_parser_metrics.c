#include "maca_graph_parser_metrics.h"


double maca_graph_parser_sentence_errors(maca_graph_parser_sentence *ref, maca_graph_parser_sentence *hyp){
  /**
   * Compute the number of errors in hyp wrt ref.
   */

  double correct = 0;
  int i;
  double x;

  /* start at 1 because 0 is the fake root */
  for(i=1; i < ref->l; i++){
    /* coarse cost function */
    /*
    if((ref->gov[i] == hyp->gov[i]) && (ref->label[i] == hyp->label[i]))
      correct += 1;
    */
    /* fine cost function */
    if(ref->gov[i] == hyp->gov[i]){
      correct += 0.5;
      if(ref->label[i] == hyp->label[i]){
	correct += 0.5;
      }
    }
  }
  x = ((double) ref->l - 1 - correct);
  return x;
}


double maca_graph_parser_sentence_compute_las(maca_graph_parser_sentence *ref, maca_graph_parser_sentence *hyp, int absolute){
  /**
   * Compute the Labelled Attachment Score of hyp wrt ref.
   *
   * Should not be used for English, where punctuation is
   * traditionnally ignored for scoring.
   */

  double result;
  int i;
  int correct = 0;

  for(i = 1; i < ref->l; i++){ /* start at 1 because 0 is the fake root */
    if((ref->gov[i] == hyp->gov[i]) && (ref->label[i] == hyp->label[i]))
      correct++;
  }

  if(absolute){
    result = correct;
  } else {
    result = ((double) correct) / ((double) (ref->l - 1)); /* l-1 to remove the fake root */
  }

  return result;
}


double maca_graph_parser_sentence_compute_uas(maca_graph_parser_sentence *ref, maca_graph_parser_sentence *hyp, int absolute){
  /**
   * Compute the Unlabelled Attachment Score of hyp wrt ref.
   *
   * Should not be used for English, where punctuation is
   * traditionnally ignored for scoring.
   */

  int i;
  int correct = 0;
  for(i = 1; i < ref->l; i++){
    if(ref->gov[i] == hyp->gov[i]) correct++;
    /* printf("%d\t%d\t%d\t%d\n", ref->gov[i], hyp->gov[i], ref->label[i], hyp->label[i]); */
  }
  if(absolute) 
    return correct;
  else
    return ((double) correct / (double) (ref->l - 1));
}


double maca_graph_parser_sentence_compute_las_oracle(maca_graph_parser_sentence *ref, maca_graph_parser_sentence *hyp, int k, int absolute){
  /**
   * Compute the oracle LAS as max_{i=0..k-1}(LAS(hyp[i])).
   *
   * FIXME: partial duplicate of compute_las
   */

  int ki;
  double cur_las;
  double max_las;
  /* almost duplicate of compute_las */
  int i;
  int correct;

  max_las = 0.0;
  for(ki=0; ki<k; ki++){
    /* cur_las = maca_graph_parser_sentence_compute_las(ref, hyp, ki, absolute); */

    /* instead of the preceding line, almost duplicate of compute_las */
    correct = 0;
    for(i = 1; i < ref->l; i++){ /* start at 1 because 0 is the fake root */
      if((ref->gov[i] == hyp->kb->gov[i][ki]) &&
	 (ref->label[i] == hyp->kb->label[i][ki]))
	correct++;
      /* printf("%d\t%d\t%d\t%d\n", ref->gov[i], hyp->gov[i], ref->label[i], hyp->label[i]); */
    }
    if(absolute) 
      cur_las = correct;
    else
      cur_las = ((double) correct / (double) (ref->l - 1)); /* l-1 to remove the fake root */
    /* end of almost duplicate */
    
    if (cur_las > max_las)
      max_las = cur_las;
  }

  return max_las;
}
