#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

// quick hack to resize the hash table
int main(int argc, char** argv) {
    if(argc != 3) {
        fprintf(stderr, "usage: %s <input-model> <output-model>\n", argv[0]);
        return 1;
    }
    FILE* input = fopen(argv[1], "r");
    FILE* output = fopen(argv[2], "w");
    int i;
    int value;
    for(i = 0; i < 8; i++) {
        fread(&value, sizeof(int), 1, input);
        fwrite(&value, sizeof(int), 1, output);
    }
    int nb_elements, size;
    fread(&size, sizeof(int), 1, input);
    fread(&nb_elements, sizeof(int), 1, input);
    fprintf(stderr, "size = %d\n", size);
    fprintf(stderr, "elements = %d\n", nb_elements);
    uint64_t* table = malloc(sizeof(uint64_t) * size);
    fread(table, size, sizeof(uint64_t), input);
    float* weights = malloc(sizeof(float) * size);
    fread(weights, size, sizeof(float), input); 

    int new_size = nb_elements * 3;
    fwrite(&new_size, sizeof(int), 1, output);
    fwrite(&nb_elements, sizeof(int), 1, output);

    uint64_t* table2 = calloc(sizeof(uint64_t), new_size);
    float* weights2 = calloc(sizeof(float), new_size);

    int num_collisions = 0;
    int max_collisions = 0;
    for(i = 0; i < size; i++) {
        if(table[i] != 0) {
            int hash = (int)(table[i] % (uint64_t)new_size);
            int offset;
            for(offset = 0; offset < new_size; offset++) {
                int location = (hash + offset) % new_size;
                if(table2[location] == 0) {
                    table2[location] = table[i];
                    weights2[location] = weights[i];
                    break;
                }
                num_collisions++;
            }
            if(offset > max_collisions) max_collisions = offset;
        }
    }
    fwrite(table2, sizeof(uint64_t), new_size, output);
    fwrite(weights2, sizeof(float), new_size, output);
    fprintf(stderr, "collisions = %f (max = %d)\n", 1.0 * num_collisions / nb_elements, max_collisions);

    fclose(input);
    fclose(output);

    // MM: free memory
    free(table2);
    table2 = NULL;
    free(weights);
    weights = NULL;
    free(weights2);
    weights2 = NULL;
    return 0;
}
