#include <iostream>
#include <string>
#include <vector>
#include <sstream>

#include "simple_parser.h"
using namespace macaon;

int main(int argc, char** argv)
{
    if(argc < 4) {
        std::cerr << "usage: " << argv[0] << " <cfg> <model.bin> <model.alpha> <model.dep_count>\n";
        std::cerr << "expects: one word, one lemma, one tag, per line, empty line between sentences\n";
        return 1;
    }
    Parser mp(argv[1], 0, argv[2], argv[3], argv[4], 1);

    std::vector<std::string> words;
    std::vector<std::string> lemmas;
    std::vector<std::string> tags;

    std::string line;
    while(std::getline(std::cin, line)) {
        std::cout << "|" << line << "|\n";
        if(line == "") {
            std::vector<ParsedWord> output;
            mp.ProcessSentence(words, tags, lemmas, output);
            std::cout << "size: " << output.size() << "\n";
            for(size_t i = 0; i < output.size(); i++){
                std::cout << i << " " << output[i].word << " " << output[i].posTag << " " << output[i].dependencyParent << " " << output[i].dependencyLabel  << "\n";
            }
            std::cout << "\n";
            words.clear();
            tags.clear();
            lemmas.clear();
        }
        std::stringstream reader(line);
        std::string word, lemma, tag;
        reader >> word >> lemma >> tag;
        words.push_back(word);
        lemmas.push_back(lemma);
        tags.push_back(tag);
    }
}
