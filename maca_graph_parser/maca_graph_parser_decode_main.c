/*******************************************************************************
    Copyright (C) 2012 by Alexis Nasr <alexis.nasr@lif.univ-mrs.fr>
                          Ghasem Mirroshandel <ghasem.mirroshandel@lif.univ-mrs.fr>
    This file is part of maca_graph_parser.

    maca_graph_parser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    maca_graph_parser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with maca_graph_parser. If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

#include<getopt.h>
#include<unistd.h>
#include<stdio.h>
#include<string.h>

#include "maca_common.h"
#include "maca_constants.h"
#include "maca_graph_parser.h"
#include "maca_graph_parser_conll2007_format.h"
#include "maca_graph_parser_decoder.h"


int main(int argc, char **argv)
{
  /* char c; */
  maca_graph_parser_ctx * ctx;
  maca_graph_parser_sentence *sentence;
  int sent_num;

  ctx = maca_graph_parser_LoadCTX(argc,argv);
  

  if(ctx->conll_file){
    sentence = maca_graph_parser_allocate_sentence(ctx);
    for(sentence = maca_graph_parser_read_conll_sentence(ctx, ctx->conll_file, sentence), sent_num = 0; 
	sentence && (sent_num < ctx->sent_nb); 
	sentence = maca_graph_parser_read_conll_sentence(ctx, ctx->conll_file, sentence), sent_num++){
      /* exit at empty sentence, should happen iff EOF */
      if(sentence->l == 1)
	break;
      /* skip too long sentences */
      if(sentence->l >= ctx->max_sent_length){
	continue;
      }

      maca_graph_parser_print_verbose(ctx, 2, MACA_MESSAGE, "parsing sentence");
      maca_graph_parser_decoder_parse(ctx, ctx->s);
      /* maca_graph_parser_update_sentence(ctx, ctx->s); */ /* TODO: backport modifs to maca_sentence if IO format is maca_xml */
      
      /* write parsed sentence to file_out (default to stdout) */
      maca_graph_parser_dump_conll_sentence(ctx, ctx->s, ctx->file_out);

      fprintf(stderr, "%d\n", sent_num);     
    }
    maca_graph_parser_free_sentence(sentence);
  } else {
      printf("You must provide a Conll file\n");
  }

  

  /* maca_graph_parser_add_stamp(maca_common_get_xml_root_node()); */
  maca_graph_parser_free_all(ctx);
  /* fermeture et libération memoire */
/*   maca_close(); */

  return 0;
}

