/*******************************************************************************
    Copyright (C) 2012 by Alexis Nasr <alexis.nasr@lif.univ-mrs.fr>
                          Ghasem Mirroshandel <ghasem.mirroshandel@lif.univ-mrs.fr>
    This file is part of maca_graph_parser.

    maca_graph_parser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    maca_graph_parser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with maca_graph_parser. If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

#include<getopt.h>
#include<unistd.h>
#include<stdio.h>
#include<string.h>

#include "maca_common.h"
#include "maca_constants.h"
#include "maca_graph_parser_model.h"
#include "maca_graph_parser_train.h"
#include "maca_graph_parser_features.h"

/*-------------------------------------------------------------------------------------------*/

int main(int argc, char **argv)
{

  /* sinon lecture des options dans la ligne de commande */
    maca_graph_parser_ctx* ctx;
    ctx = maca_graph_parser_LoadCTX(argc,argv);

  /* load training corpus */
  if(ctx->verbose_flag > 1){
    maca_msg(ctx->module, MACA_MESSAGE);
    fprintf(stderr, "loading training corpus\n");
  }
  hyp_ref_vector *corpus = load_mcf_corpus(ctx);
  fprintf(stderr, "Corpus size: %d\n", corpus->size);
  
  /* preprocessing: relabel rare dependencies */
  int sent_id = 0;
  for(sent_id=0; sent_id < corpus->size; sent_id++){
    maca_graph_parser_sentence_relabel_rare_deps(ctx, corpus->ref[sent_id]);
  }
  
  /* extractor_allocator() cannot be created earlier as it needs the
     complete alphabet, which is built during load_conll_corpus
     FIXME: properly (semi-)freeze the alphabet,
     cf. extractor_allocator()
  */
  ctx->e = maca_graph_parser_templ_library_allocator(ctx);
  /* dump model */
  maca_graph_parser_train(ctx, corpus);
  maca_graph_parser_model_dump(ctx, ctx->model, ctx->model_file_name, ctx->produce_hash_model);
  /* maca_graph_parser_alphabet_print(ctx->alphabet_file_name, ctx->alphabet); */
  maca_graph_parser_dep_count_table_print(ctx, ctx->dep_count_table_file_name);

  /* free corpus data */
  free_corpus(corpus);

  maca_graph_parser_free_all(ctx);
  
  return 0;
}

/*-------------------------------------------------------------------------------------------*/

