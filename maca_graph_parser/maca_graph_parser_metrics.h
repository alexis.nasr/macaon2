#include "maca_graph_parser.h"

#ifdef __cplusplus
extern "C"{
#endif


double maca_graph_parser_sentence_errors(maca_graph_parser_sentence *ref, maca_graph_parser_sentence *hyp);
double maca_graph_parser_sentence_compute_las(maca_graph_parser_sentence *ref, maca_graph_parser_sentence *hyp, int absolute);
double maca_graph_parser_sentence_compute_uas(maca_graph_parser_sentence *ref, maca_graph_parser_sentence *hyp, int absolute);
double maca_graph_parser_sentence_compute_las_oracle(maca_graph_parser_sentence *ref, maca_graph_parser_sentence *hyp, int k, int absolute);

  #ifdef __cplusplus
}
#endif
