#include "maca_graph_parser_hash.h"
#include "maca_graph_parser_features.h"
#include "maca_graph_parser_feature_vector.h"
#include "maca_graph_parser_feature_counter_array.h"


feature_counter_array *allocate_feature_counter_array(maca_graph_parser_ctx *ctx, int nb_words){
  /**
   * Allocate an array of feature counters.
   */
  
  feature_counter_array *a = malloc(sizeof(feature_counter_array));
  a->size = 4;
  int i;
  for(i=0; i < a->size; i++){
    a->array[i] = NULL;
  }

  a->nb_words = nb_words;

  if(ctx->basic_features){
    a->array[0] = a->basic_feature_counter = feature_counter_new(2 * nb_words * BasicFeatNb, 0.8);
  }
  if(ctx->first_features){
    a->array[1] = a->first_feature_counter = feature_counter_new(2 * nb_words * FirstOrderFeatNb, 0.8);
  }
  if(ctx->basic_features){
    a->array[2] = a->grandchildren_feature_counter = feature_counter_new(2 * nb_words * 2 * GrandchildrenFeatNb, 0.8);
  }
  if(ctx->basic_features){
    a->array[3] = a->sibling_feature_counter = feature_counter_new(2 * nb_words * SiblingFeatNb, 0.8);
  }

  return a;
}

void free_feature_counter_array(feature_counter_array *a){
  /**
   * Free a.
   */

  if(a == NULL)
    return;

  int i;
  for(i = 0; i < a->size; i++){
    if(a->array[i]){
      feature_counter_destroy(a->array[i]);
    }
  }
  free(a);
}


int feature_counter_array_squared_norm(feature_counter_array *a){
  /**
   * Return the squared norm of the feature counter array a.
   */

  int result = 0;

  int i;
  for(i=0; i < a->size; i++){
    if (a->array[i] == NULL)
      continue;
    
    result += feature_counter_squared_norm(a->array[i]);
  }
  
  return result;
}


feature_counter_array *feature_counter_array_difference(maca_graph_parser_ctx *ctx, feature_counter_array *a, feature_counter_array *b){
  /**
   * Return a new feature_counter_array with elements in a not in b.
   */

  feature_counter_array *result = allocate_feature_counter_array(ctx, a->nb_words);
  
  int i;
  for(i=0; i < a->size; i++){
    feature_counter *c_a = a->array[i];
    feature_counter *c_b = b->array[i];
    if(c_a == NULL)
      continue;

    feature_counter *c_res = result->array[i];
    feature_counter_update(c_res, c_a);
    feature_counter_subtract(c_res, c_b);
  }
  
  return result;
}


feature_counter_array *extract_features_from_parse_fca(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s, feature_counter_array *a){
  /**
   * Extract features from s and put them in an array of feature counters.
   *
   * Create a new array of feature counters if a is NULL.
   */

  if(a == NULL){
    a = allocate_feature_counter_array(ctx, (s->l - 1));
  } else { /* reset a */
    free_feature_counter_array(a);
    a = allocate_feature_counter_array(ctx, (s->l - 1));
  }

  feature_counter *c;
  feat_vector *v;
  int dep, gdep, sbl;
  int g;
  int i;

  if(ctx->basic_features){
    c = a->basic_feature_counter;
    v = NULL;
    for(dep=1; dep < s->l; dep++){
      v = basic(s, ctx, s->gov[dep], dep, v);
      feature_counter_update_vector(c, v);
    }
    free_feat_vector(v);
    v = NULL;
  }

  if(ctx->first_features){
    c = a->first_feature_counter;
    v = NULL;
    for(dep=1; dep < s->l; dep++){
      v = first(s, ctx, s->gov[dep], dep, s->label[dep], v);
      feature_counter_update_vector(c, v);
    }
    free_feat_vector(v);
    v = NULL;
  }
  
  if(ctx->sibling_features){
    c = a->sibling_feature_counter;
    v = NULL;
    for(dep=1; dep < s->l; dep++){
      // MM
      sbl = -1;
      g = s->gov[dep]; // governor
      /* wanted sibling: child of g in [g..dep] that is closest to dep */
      if (g < dep) { /* ra */
	for(i=dep-1; i > g; i--){
	  if(g == s->gov[i]){ // && (dep != i)
	    sbl = i;
	    break;
	  }
	}
      } else { /* la */
	for(i=dep+1; i < g; i++){
	  if(g == s->gov[i]){ // (dep != i) && 
	    sbl = i;
	    break;
	  }
	}	
      }
      /* sbl == -1 if no sibling */
      v = sibling(s, ctx, s->gov[dep], dep, sbl, s->label[dep], v);
      feature_counter_update_vector(c, v);
    }
    free_feat_vector(v);
    v = NULL;
  }

  if(ctx->grandchildren_features){
    c = a->grandchildren_feature_counter;
    v = NULL;
    for(dep=1; dep < s->l; dep++){
      // MM
      g = s->gov[dep];
      if (g < dep){ /* ra */
	/* cmi: inside [g;dep] */
	gdep = -1;
	for(i=dep-1; i > g; i--){
	  if(s->gov[i] == dep){
	    gdep = i;
	  }
	}
	v = grandchildren(s, ctx, g, dep, gdep, s->label[dep], v);
	feature_counter_update_vector(c, v);
	/* cmo: outside [g;dep] */
	gdep = -1;
	for(i=dep+1; i<s->l; i++){
	  if(s->gov[i] == dep){
	    gdep = i;
	  }
	}
	v = grandchildren(s, ctx, g, dep, gdep, s->label[dep], v);	
	feature_counter_update_vector(c, v);
      } else { /* la */
	/* cmi: inside [dep;g] */
	gdep = -1;
	for(i=dep+1; i < g; i++){
	  if(s->gov[i] == dep){
	    gdep = i;
	  }
	}
	v = grandchildren(s, ctx, g, dep, gdep, s->label[dep], v);
	feature_counter_update_vector(c, v);
	/* cmo: outside [dep;g] */
	gdep = -1;
	for(i=dep-1; i>0; i--){
	  if(s->gov[i] == dep){
	    gdep = i;
	  }
	}
	v = grandchildren(s, ctx, g, dep, gdep, s->label[dep], v);
	feature_counter_update_vector(c, v);
      }
    }
    free_feat_vector(v);
    v = NULL;
  }
  
  return a;
}
