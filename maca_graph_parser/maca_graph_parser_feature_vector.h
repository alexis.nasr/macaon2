/*******************************************************************************
    Copyright (C) 2012 by Alexis Nasr <alexis.nasr@lif.univ-mrs.fr>
                          Ghasem Mirroshandel <ghasem.mirroshandel@lif.univ-mrs.fr>
    This file is part of maca_graph_parser.

    maca_tagger is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    maca_tagger is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with maca_tagger. If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#ifndef __MACA_GRAPH_PARSER_FEATURE_VECTOR__
#define __MACA_GRAPH_PARSER_FEATURE_VECTOR__

#include "maca_common.h"
#include "maca_constants.h"
//#include "maca_tags.h"
#include "maca_msg.h"
#include "maca_graph_parser.h"

/* feature vector */
typedef struct {
  feature_t *array;
  int size;
  int elt_nb;
}feat_vector;


/* feature matrix */
typedef struct {
  feat_vector **array;
  int size;
  int vector_nb;
}feat_matrix;


#ifdef __cplusplus
extern "C"{
#endif

feat_vector *allocate_feat_vector(int size);
void free_feat_vector(feat_vector *v);
void print_feat_vector(maca_graph_parser_ctx *ctx, feat_vector *fv);



feat_matrix *allocate_feat_matrix(int lines, int columns);
void free_feat_matrix(feat_matrix *m);
void print_feat_matrix(maca_graph_parser_ctx *ctx, feat_matrix *fm);

#ifdef __cplusplus
}
#endif


  
#endif
