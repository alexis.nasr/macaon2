#include "maca_graph_parser_conll2007_format.h"

#include <errno.h>
#include <string.h>

maca_graph_parser_sentence *maca_graph_parser_read_conll_sentence(maca_graph_parser_ctx *ctx, FILE *f, maca_graph_parser_sentence *s){
  /**
   * Read into s a sentence from f in the CONLL 2007 format.
   *
   * Returns the next valid sentence in f.
   */

  int id;
  char id_text[MAX_STR];
  char form[MAX_STR];
  char lemma[MAX_STR];
  char cpostag[MAX_STR];
  char postag[MAX_STR];
  char feats[MAX_STR];
  unsigned gov;
  char gov_text[MAX_STR];
  char deprel[MAX_STR];
  char phead_str[MAX_STR]; /* phead_str is used to store syntactic features */
  char *synt_feats;
  char *synt_feat;
  char pdeprel[MAX_STR];
  char buff[MAX_LINE];
  unsigned nb_fields;
  int code_postag, code_lemma, code_form, code_label, code_synt_feat;
  int sentence_valid = 1;

  int synt_feats_array[100];
  int synt_feats_nb = 0;

  /* early termination */
  if(s == NULL){
    fprintf(stderr, "read_conll_sentence: s == NULL\n");
    return s;
  }

  /* reset sentence given as parameter */
  s->l = 1;

  if(feof(f)) return s;

  while(fgets(buff, MAX_LINE, f) != NULL){
    /* sentence separator: empty line */
    if(buff[0] == '\n'){
      if(s->l > 1){
	if(sentence_valid){
	  break;
	} else { /* invalid sentence: start reading next sentence instead */
	  s->l = 1;
	  sentence_valid = 1;
	  continue;
	}
      } else { /* skip extra empty lines */
	continue;
      }
    }

    /* skip too long sentence */
    if(s->l >= MACA_MAX_LENGTH_SENTENCE){
      if(ctx->verbose_flag > 1){
	maca_msg(ctx->module, MACA_ERROR);
	fprintf(stderr, "sentence too long, skipping it\n");
      }
      /* mark as invalid and continue reading */
      sentence_valid = 0;
      continue;
    }

    id_text[0] = form[0] = lemma[0] = cpostag[0] = feats[0] = gov_text[0] = deprel[0] = phead_str[0] = pdeprel[0] = '\0';
    id = gov = -1;
    /*1	Quatre	Quatre	D	det	-	2	det	0	0.000000	-1.000000	-1.000000	-1.000000*/
    /* nb_fields = sscanf(buff, "%d\t%s\t%s\t%s\t%s\t%s\t%d\t%s\t%d\t%s\t%lf%lf\t%lf", */
    nb_fields = sscanf(buff, "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s",
		       id_text, form, lemma, cpostag, postag, feats,
		       gov_text, deprel, phead_str, pdeprel);

    id = strtol(id_text, NULL, 10);
    if(errno == ERANGE) {
        fprintf(stderr, "WARNING: id is an invalid number in \"%s\"\n", buff);
    }
    gov = strtol(gov_text, NULL, 10);
    if(errno == ERANGE) {
        fprintf(stderr, "WARNING: gov is an invalid number in \"%s\"\n", buff);
    }

    if(nb_fields != 10) {
        fprintf(stderr, "WARNING: incorrect number of fields in \"%s\"\n", buff);
    }
    
    /* encode all fields as integers */
    code_form = maca_alphabet_add_symbol(ctx->words_alphabet, form);

    code_lemma = (strcmp(lemma, CONLL_UNK)) ? maca_alphabet_add_symbol(ctx->words_alphabet, lemma) : -1;

    /* code_postag = maca_tags_get_code(ctx->cfg, "morpho", "stype", postag); */

    
    /* get code of the postag. 
       If in train mode, new postags can be seen and must be added to the alphabet.
       in decode mode, a postag not present in the alphabet must raise a warning and the sentence is skipped */
    if(ctx->mode == TRAIN_MODE){
      code_postag = maca_alphabet_add_symbol(ctx->pos_alphabet, postag);
    }
    else{
      code_postag = maca_alphabet_get_code(ctx->pos_alphabet, postag);
      /* POS not in tagset: raise warning */
      if(code_postag ==  MACA_ALPHABET_INVALID_CODE){
	if(ctx->verbose_flag > 0){
	  maca_msg(ctx->module, MACA_WARNING);
	  fprintf(stderr,"pos %s unknown\n", postag);
	}
	/* POS not in tagset: mark sentence as invalid */
	sentence_valid = 0;
      }
    }

    /* get code of the synt_feat. 
       If in train mode, new synt_feats can be seen and must be added to the alphabet.
       in decode mode, a synt_feat not present in the alphabet must raise a warning and the sentence is skipped */

    synt_feats_nb = 0;
    
    if(strcmp(phead_str, "_")){
      synt_feats = strdup(phead_str);
      /* printf("synt feats = %s\n", synt_feats); */
      for(synt_feat = strtok (synt_feats, "|"); synt_feat; synt_feat = strtok (NULL, "|")){
	/* printf("%d synt feat = %s\n", synt_feats_nb, synt_feat); */
	if(ctx->mode == TRAIN_MODE){
	  code_synt_feat = maca_alphabet_add_symbol(ctx->synt_feats_alphabet, synt_feat);
	  synt_feats_array[synt_feats_nb++] = code_synt_feat;
	  
	}
	else{
	  code_synt_feat = maca_alphabet_get_code(ctx->synt_feats_alphabet, synt_feat);
	  /* synt_feat not in synt_feat set: raise warning */
	  if(code_synt_feat ==  MACA_ALPHABET_INVALID_CODE){
	    if(ctx->verbose_flag > 0){
	      maca_msg(ctx->module, MACA_WARNING);
	      fprintf(stderr,"synt_feat %s unknown\n", synt_feat);
	    }
	    /* synt_feat not in tagset: mark sentence as invalid */
	    sentence_valid = 0;
	  }
	  else{
	    synt_feats_array[synt_feats_nb++] = code_synt_feat;
	  }
	}
      }
      free(synt_feats);
    }


    /* code_label = (strcmp(deprel, CONLL_UNK))? maca_tags_get_code(ctx->cfg, "morpho", "fct", deprel) : -2; */
    code_label = (strcmp(deprel, CONLL_UNK))? maca_alphabet_add_symbol(ctx->labels_alphabet, deprel) : -2;


    /* label not in tagset: raise warning */
    if((code_label == -1) && (ctx->verbose_flag > 0)){
      maca_msg(ctx->module, MACA_WARNING);
      fprintf(stderr,"function %s unknown\n", deprel);
    }
    /* label not in tagset or unknown:
       if in training mode, mark sentence as invalid */
    if((code_label < 0) && (ctx->mode == TRAIN_MODE)){
      sentence_valid = 0;
    }

    /*      fprintf(stdout, "------------> id = %d\n", id);  
      fprintf(stdout, "------------> form = %s\t%d\n", form, code_form);  
      fprintf(stdout, "------------> postag = %s\t%d\n", postag, code_postag);  
      fprintf(stdout, "------------> lemma = %s\t%d\n", lemma, code_lemma);
      fprintf(stdout, "------------> cpostag = %s\t%d\n", cpostag, code_postag);
      fprintf(stdout, "------------> feats = %s\n", feats);
      fprintf(stdout, "------------> gov = %d\n", gov);
      fprintf(stdout, "------------> deprel = %s\t%d\n", deprel, code_label);
      fprintf(stdout, "------------> pdeprel = %s\n", pdeprel);
    */


    maca_graph_parser_sentence_add_word(ctx, s, NULL, code_form, code_lemma, code_postag, gov, code_label, synt_feats_nb, synt_feats_array); 
  }
  
  return s;
}


void maca_graph_parser_dump_conll_sentence_kbest(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s, FILE *f){
  int i; /* word index */
  int j; /* j-th best parse */
  int k = s->kb->k;
  int cw; /* return val for fprintf */
  char buffer[128];
  for(j=0; j < k; j++){
    for(i=1; i < s->l; i++){
      /* INDEX */
      cw = fprintf(f, "%d", i);
      if(cw < 0){
	fprintf(stderr, "dump_conll_sentence: problem while writing to file\n");
	exit(1);
      }
      /* FORM */
      if (s->words[i] != -1)
	maca_alphabet_get_symbol(ctx->words_alphabet, s->words[i], buffer, sizeof(buffer));
      else
	strcpy(buffer, "_");
      cw = fprintf(f, "\t%s", buffer);
      /* LEMMA */
      if (s->lemmas[i] != -1)
	maca_alphabet_get_symbol(ctx->words_alphabet, s->lemmas[i], buffer, sizeof(buffer));
      else
	strcpy(buffer, "_");
      cw = fprintf(f, "\t%s", buffer);
      /* CPOSTAG */
      if (s->pos[i] != -1)
	maca_alphabet_get_symbol(ctx->pos_alphabet, s->pos[i], buffer, sizeof(buffer));
      else
	strcpy(buffer, "_");
      cw = fprintf(f, "\t%s", buffer);
      /* POSTAG */
      if (s->pos[i] != -1)
	maca_alphabet_get_symbol(ctx->pos_alphabet, s->pos[i], buffer, sizeof(buffer));
      else
	strcpy(buffer, "_");
      cw = fprintf(f, "\t%s", buffer);
      /* TODO: w_morpho */
      cw = fprintf(f, "\t_");	
      /* HEAD */
      cw = fprintf(f, "\t%d", s->kb->gov[i][j]);
      /* DEPREL */
      /*      if(s->kb->gov[i][j] == 0) 
	fprintf(f,"\troot");
	else*/
      if (s->kb->label[i][j] != -1)
	maca_alphabet_get_symbol(ctx->pos_alphabet, s->kb->label[i][j], buffer, sizeof(buffer));
      else
	strcpy(buffer, "_");
      cw = fprintf(f, "\t%s", buffer);
      /* PHEAD */
      cw = fprintf(f, "\t_");
      /* PDEPREL */
      cw = fprintf(f, "\t_");
      cw = fprintf(f, "\n");
    }
    fprintf(f, "\n");
  }
  fprintf(f, "\n");
}

void maca_graph_parser_dump_conll_sentence_kbest_for_ghasem(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s, FILE *f){ 
/* void maca_graph_parser_dump_conll_sentence_kbest(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s, FILE *f){ */
  int i; /* word index */
  int j; /* j-th best parse */
  int k = s->kb->k;
  int cw; /* return val for fprintf */
  char buffer[128];

  fprintf(f, "#sentence\n");
  for(j=0; j < k; j++){
    fprintf(f, "#parse %d\n", j);
    fprintf(f, "#score %f\n", s->kb->score[j]);
    for(i=1; i < s->l; i++){
      /* INDEX */
      cw = fprintf(f, "%d", i);
      if(cw < 0){
	fprintf(stderr, "dump_conll_sentence: problem while writing to file\n");
	exit(1);
      }

      /* HEAD */
      cw = fprintf(f, " %d", s->kb->gov[i][j]);
      /* DEPREL */
      if(s->kb->gov[i][j] == 0) 
	fprintf(f," root");
      else
	if (s->kb->label[i][j] != -1)
	  maca_alphabet_get_symbol(ctx->pos_alphabet, s->kb->label[i][j], buffer, sizeof(buffer));
	else
	  strcpy(buffer, "_");
      cw = fprintf(f, "\t%s", buffer);
      cw = fprintf(f, "\n");
    }
    fprintf(f, "\n");
  }
  fprintf(f, "\n");
}

void maca_graph_parser_dump_conll_sentence(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s, FILE *f){
  /**
   * Dump sentence s to file f, in the CONLL 2007 format.
   *
   */

  int i = 0; // FIXME: was uninitialized before
  int cw; /* return val for fprintf */
  
  if(s == NULL)
    return;

  if(s->kb) {
    maca_graph_parser_dump_conll_sentence_kbest(ctx, s, f);
  /*maca_graph_parser_dump_conll_sentence_kbest_for_ghasem(ctx, ctx->s, ctx->file_out);*/
  } else {
    char word[128];
    char lemma[128];
    char pos[128];
    char label[128];

    if (s->words[i] != -1)
      maca_alphabet_get_symbol(ctx->words_alphabet, s->words[i], word, sizeof(word));
    else
      strcpy(word, "_");
    if (s->lemmas[i] != -1)
      maca_alphabet_get_symbol(ctx->words_alphabet, s->lemmas[i], lemma, sizeof(lemma));
    else
      strcpy(lemma, "_");
    if (s->pos[i] != -1)
      maca_alphabet_get_symbol(ctx->pos_alphabet, s->pos[i], pos, sizeof(pos));
    else
      strcpy(pos, "_");
    if (s->label[i] != -1)
      maca_alphabet_get_symbol(ctx->labels_alphabet, s->label[i], label, sizeof(label));
    else
      strcpy(label, "_");
    for(i=1; i < s->l; i++){
    /*
      w_morpho = (memcmp(s->morpho[i], {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, sizeof(s->morpho[i]))) ? MORPHO_FIELD_STRING : "_";
    */
      
      cw = fprintf(f, "%d\t%s\t%s\t%s\t%s\t%s\t%d\t%s\t%s\t%s\t\n",
		   i,
		   /* FORM */
		   word,
		   /* LEMMA */
		   lemma,
		   /* CPOSTAG */
		   pos,
		   /* POSTAG */
		   pos,
		   /* TODO: w_morpho */
		   "_", 
		   /* HEAD */
		   s->gov[i], 
		   /* DEPREL */
		   label,
		   /* PHEAD */
		   "_", 
		   /* PDEPREL */
		   "_" 
		   );
      if(cw < 0){
	fprintf(stderr, "dump_conll_sentence: problem while writing to file\n");
	exit(1);
      }
    }
    fprintf(f, "\n");
  }
  
}


/*-------------------------------------------------------------------------------------------*/
/*
void maca_graph_parser_load_conll_sentence(maca_graph_parser_ctx * ctx, sentence *conll_s, maca_graph_parser_sentence *maca_s)
{
  parse *p = conll_s->parses[0];
  int i;
  int form, lemma, postag, label;
  maca_s->l = 0;

    for(i=0; i<p->l; i++){
      postag = maca_tags_get_code(ctx->cfg, "morpho", "stype", p->words[i]->postag);
      lemma = maca_alphabet_add_symbol(ctx->words_alphabet, p->words[i]->lemma);
      form = maca_alphabet_add_symbol(ctx->words_alphabet, p->words[i]->form);


      label = maca_tags_get_code(ctx->cfg, "morpho", "fct", p->words[i]->deprel);
      maca_graph_parser_sentence_add_word(ctx, maca_s, NULL, form, lemma, postag, p->words[i]->head, label);
    }
}
*/

