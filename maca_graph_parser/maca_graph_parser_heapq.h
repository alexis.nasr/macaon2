typedef struct {
  int num;
  int max_size;
  float dft_value;
  float *values; 
  void **objects;
} heap;

#ifdef __cplusplus
extern "C"{
#endif

void heap_swap(heap *h, const int a, const int b);

// n-max
void heap_heapify_min(heap *h, int index);

void heap_build_min(heap *h);

void heap_extract_min(heap *h);

void heap_sort_nmax(heap *h);

void heap_insert_nmax(heap *h, float value, void *object);

// n-min
void heap_heapify_max(heap *h, int index);

void heap_build_max(heap *h);

void heap_extract_max(heap *h);

void heap_sort_nmin(heap *h);

void heap_insert_nmin(heap *h, float value, void *object);

// common
heap *heap_create(const int max_size, const float dft_value);

void heap_destroy(heap* h);

void *heap_get(heap *h, const int index);

float heap_get_value(heap *h, const int index);

int heap_size(heap *h);

void heap_clear(heap *h);

  #ifdef __cplusplus
}
#endif
