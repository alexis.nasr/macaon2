/*******************************************************************************
    Copyright (C) 2012 by Alexis Nasr <alexis.nasr@lif.univ-mrs.fr>
                          Ghasem Mirroshandel <ghasem.mirroshandel@lif.univ-mrs.fr>
    This file is part of maca_graph_parser.

    maca_graph_parser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    maca_graph_parser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with maca_graph_parser. If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#include"maca_graph_parser.h"
#include"maca_graph_parser_features.h"
#include"maca_graph_parser_feature_vector.h"


feat_vector *allocate_feat_vector(int size)
{
  feat_vector *v = malloc(sizeof(feat_vector));
  v->array = malloc((size_t)size * sizeof(feature_t));
  v->size = size;
  v->elt_nb = 0;
  return v;
}

void free_feat_vector(feat_vector *v)
{
  if(v){
    free(v->array);
    free(v);
  }
}


void print_feat_vector(maca_graph_parser_ctx *ctx, feat_vector *fv){
  /**
   * Print a feature vector on stderr (for debug).
   */

  if(fv == NULL)
    return;

  int i;
  for(i=0; i < fv->elt_nb; i++){
    maca_graph_parser_print_feature(stderr, ctx, fv->array[i]);
    fprintf(stderr, "\n");
  }
}


feat_matrix *allocate_feat_matrix(int lines, int columns)
{
  int i;
  feat_matrix *m = malloc(sizeof(feat_matrix));
  m->array = malloc((size_t)lines * sizeof(feat_vector*));
  for(i=0; i < lines; i++)
    m->array[i] = allocate_feat_vector(columns);
  m->size = lines;
  m->vector_nb = 0;
  return m;
}

void free_feat_matrix(feat_matrix *m)
{
  if(m){
    int i;
    for(i=0; i < m->size; i++){
      free_feat_vector(m->array[i]);
    }
    free(m->array);
    free(m);
  }
}


void print_feat_matrix(maca_graph_parser_ctx *ctx, feat_matrix *fm){
  /**
   * Print a feature matrix on stderr (for debug).
   */

  if(fm == NULL)
    return;

  int i;
  for(i=0; i < fm->vector_nb; i++){
    feat_vector *fv = fm->array[i];
    int j;
    for(j=0; j < fv->elt_nb; j++){
      maca_graph_parser_print_feature(stderr, ctx, fv->array[j]);
      fprintf(stderr, "\n");
    }
  }
}
