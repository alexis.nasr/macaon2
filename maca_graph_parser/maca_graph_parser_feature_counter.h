#include "maca_graph_parser.h"
#include "maca_graph_parser_feature_vector.h"


typedef struct {
  int size;
  int nb_elts;
  feature_t *keys;
  int *values;
} feature_counter;

typedef struct {
  feature_t key;
  int value;
} feature_count;

typedef struct {
  int size;
  int nb_elts;
  feature_count *counts;
} feature_count_vector;


#ifdef __cplusplus
extern "C"{
#endif



feature_counter *feature_counter_new(int nb_elts, float coeff);
void feature_counter_destroy(feature_counter *c);
void feature_counter_update_vector(feature_counter *c, feat_vector *v);
void feature_counter_subtract_vector(feature_counter *c, feat_vector *v);
void feature_counter_update(feature_counter *c, feature_counter *other);
void feature_counter_subtract(feature_counter *c, feature_counter *other);
int feature_counter_squared_norm(feature_counter *c);
feature_count_vector *feature_counter_items(feature_counter *c);

feature_count_vector *feature_count_vector_allocate(int size);
void feature_count_vector_free(feature_count_vector *v);
#ifdef __cplusplus
}
#endif
