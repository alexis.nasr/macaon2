/*******************************************************************************
    Copyright (C) 2012 by Alexis Nasr <alexis.nasr@lif.univ-mrs.fr>
                          Ghasem Mirroshandel <ghasem.mirroshandel@lif.univ-mrs.fr>
    This file is part of maca_graph_parser.

    maca_graph_parser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    maca_graph_parser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with maca_graph_parser. If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#include<stdlib.h>
#include<stdio.h>
#include<time.h>

#include"maca_constants.h"
#include"maca_msg.h"
//#include"maca_lex.h"

#include"maca_graph_parser_features.h"
#include"maca_graph_parser_feature_table.h"
#include"maca_graph_parser_hash.h"
#include "maca_graph_parser_model.h"
#include"maca_graph_parser.h"
/* kbest */
#include "maca_graph_parser_hyperdecoder.h"
/* end kbest */
#include "maca_graph_parser_dep_count_table.h"

/*-------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------*/

/**
 * Global state
 */

/* vertices */
/* CLOSED: start, end, direction of the attachment (left or right) */
/*
  Vertex *CLOSED
  [MACA_MAX_LENGTH_SENTENCE]
  [MACA_MAX_LENGTH_SENTENCE]
  [2]
  ;
*/
/* OPEN: start, end, direction of the attachment (left or right), label */
/*
  Vertex *OPEN
  [MACA_MAX_LENGTH_SENTENCE]
  [MACA_MAX_LENGTH_SENTENCE]
  [2]
  [NB_LABELS]
  ;
*/

/* k-best derivations with backpointers */
/*
  vec_Dbp *CDERIV
  [MACA_MAX_LENGTH_SENTENCE]
  [MACA_MAX_LENGTH_SENTENCE]
  [2]
  ;
*/

/*
  vec_Dbp *ODERIV
  [MACA_MAX_LENGTH_SENTENCE]
  [MACA_MAX_LENGTH_SENTENCE]
  [2]
  [NB_LABELS]
  ;
*/

/**
 * Util funcs
 */

cand_id *alloc_cand_id(int bs_i, int j[2]){
  /**
   * Allocate a candidate identifier
   */
  cand_id *res = malloc(sizeof(cand_id));
  if(res == NULL){
    fprintf(stderr, "memory allocation problem\n");
    exit(1);
  }
  res->bs_i = bs_i;
  res->j[0] = j[0];
  res->j[1] = j[1];
  return res;
}


void free_cand_id(cand_id *c){
  /**
   * Free a candidate ID
   */
  if(!c)
    return;
  free(c);
}


vec_Dbp *alloc_vec_Dbp(int capacity){
  /**
   * Allocate a vector of derivations with backpointers
   */

  vec_Dbp *res = malloc(sizeof(vec_Dbp));
  if(res == NULL){
    fprintf(stderr, "Mem prob\n");
    exit(1);
  }

  res->num = 0;
  res->capacity = capacity;
  res->elts = malloc(capacity * sizeof(DerivBP *));
  if(res->elts == NULL){
    fprintf(stderr, "Mem prob\n");
    exit(1);
  }
  
  return res;
}

void free_vec_Dbp(vec_Dbp *vd){
  /**
   * Free a vector of derivations with backpointers
   */
  if(!vd)
    return;

  vd->num = 0;
  vd->capacity = 0;
  free(vd->elts);
  free(vd);
}

void vec_Dbp_append(vec_Dbp *vd, DerivBP *dbp){
  /**
   * Append to a vector of derivations with backpointers
   */
  if(vd->num >= vd->capacity){
    fprintf(stderr, "Cannot append: Vector full\n");
    return;
  }
  vd->elts[vd->num] = dbp;
  vd->num += 1;
}

void reset_derivBP(DerivBP *dbp){
  /**
   * Reset a derivation with backpointers.
   */
  dbp->weight = MINF;
  dbp->e = NULL;
  dbp->j[0] = 0;
  dbp->j[1] = 0;
}

/* end utils */


/**
 * Decoder-specific helpers
 */

vec_Dbp *get_vec_Dbp(maca_graph_parser_ctx *ctx, Vertex *v){
  /**
   * Get the vector of k-best derivations (with backpointers) of v.
   */

  VertexSignature *vs = v->vsign;
  int start;
  int end;
  int dir;
  int label;
  vec_Dbp *res;

  if(vs->type == TYP_OPEN){
    start = vs->open.start;
    end = vs->open.end;
    dir = vs->open.dir;
    label = vs->open.label;
    res = ctx->ODERIV[start][end][dir][label];
  } else if(vs->type == TYP_CLOSED){
    start = vs->closed.start;
    end = vs->closed.end;
    dir = vs->closed.dir;
    res = ctx->CDERIV[start][end][dir];
  } else {
    fprintf(stderr, "not implemented yet\n");
    exit(1);
  }

  return res;
}


float eval_weight(maca_graph_parser_ctx *ctx, Hyperarc *e, int j[2],
		  maca_graph_parser_feature_table *feat_table){
  /**
   * Evaluate the weight of a potential derivation with backpointers.
   */
  float w_D0;
  float w_D1;
  VertexSignature *vs = e->head->vsign;
  SgOpen h;
  float res;

  /* the weight of a derivation is a function of the weights of
   * the subderivations
   */

  /* weight of the j[i]-th derivation for T_i ; 0 for closed of span 0 */
  w_D0 = (e->tail[0]) ? get_vec_Dbp(ctx, e->tail[0])->elts[j[0]]->weight : 0 ;
  w_D1 = (e->tail[1]) ? get_vec_Dbp(ctx, e->tail[1])->elts[j[1]]->weight : 0 ;

  if(vs->type == TYP_OPEN){
    /* w_open = w(subd0) + w(subd1) + w(head) */
    h = vs->open;
    /* here, w(head) = w_basic_feats(head) + w_first_order_feats(head) */
    res = (w_D0 + w_D1
	   + feat_table->pl[h.start][h.end][h.dir]
	   + feat_table->lab[h.start][h.end][h.label][h.dir]);
  } else {
    /* w_closed = w(subd0) + w(subd1) */
    res = w_D0 + w_D1;
  }

  return res;
}


/*-------------------------------------------------------------------------------------------*/
void maca_graph_parser_hyperdecoder_init(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s){
  /**
   * Initialize the decoder and build the corresponding hypergraph.
   *
   */
  int span, start, end, dir, label, gov, dep;
  /* ctx */
  int labels_nb = ctx->labels_nb;
  int k = ctx->k;
  /* sentence */
  int sentence_length = s->l; /* ctx->max_sent_length; */
  /* indices */
  int i;
  int k_i;
  VertexSignature *vs;
  int m;
  int j[2] = {0, 0};
  /* min dep count */
  /* int dep_count; */
  /* test: default edge label */
  /* int dft_label = maca_tags_get_code(ctx->cfg, "morpho", "fct", "__JOKER__"); */
  int dft_label = maca_alphabet_get_code(ctx->labels_alphabet, "__JOKER__");

  /* allocation of CLOSEDK, OPENK */
  ctx->CLOSEDK = malloc(sizeof(Vertex***) * sentence_length);
  ctx->OPENK = malloc(sizeof(Vertex****) * sentence_length);
  if(sentence_length > 0) {
      ctx->CLOSEDK[0] = malloc(sizeof(Vertex**) * sentence_length * sentence_length);
      ctx->CLOSEDK[0][0] = malloc(sizeof(Vertex*) * sentence_length * sentence_length * 2);
      for(start = 0; start < sentence_length; start++) {
          ctx->CLOSEDK[start] = ctx->CLOSEDK[0] + start * sentence_length;
          for(end = 0; end < sentence_length; end++) {
              ctx->CLOSEDK[start][end] = ctx->CLOSEDK[0][0] + ((start * sentence_length) + end) * 2;
          }
      }
      ctx->OPENK[0] = malloc(sizeof(Vertex***) * sentence_length * sentence_length);
      ctx->OPENK[0][0] = malloc(sizeof(Vertex**) * sentence_length * sentence_length * 2);
      ctx->OPENK[0][0][0] = malloc(sizeof(Vertex*) * sentence_length * sentence_length * 2 * ctx->labels_nb);
      for(start = 0; start < sentence_length; start++) {
          ctx->OPENK[start] = ctx->OPENK[0] + start * sentence_length;
          for(end = 0; end < sentence_length; end++) {
              ctx->OPENK[start][end] = ctx->OPENK[0][0] + ((start * sentence_length) + end) * 2;
              for(dir = 0; dir < 2; dir++) {
                  ctx->OPENK[start][end][dir] = ctx->OPENK[0][0][0] + ((((start * sentence_length) + end) * 2) + dir) * ctx->labels_nb;
              }
          }
      }
  }
  /* ensure CLOSEDK and OPENK are clean */
  for(start=0; start < sentence_length; start++){
    for(end=0; end < sentence_length; end++){
      for(dir=0; dir<2; dir++){
	/* closed */
	ctx->CLOSEDK[start][end][dir] = NULL;
	/* open */
	for(label=0; label<labels_nb; label++){
	  ctx->OPENK[start][end][dir][label] = NULL;
	}
      }
    }
  }

  /* allocation of CDERIV, ODERIV */
  ctx->CDERIV = malloc(sizeof(vec_Dbp***) * sentence_length);
  ctx->ODERIV = malloc(sizeof(vec_Dbp****) * sentence_length);
  if(sentence_length > 0) {
      ctx->CDERIV[0] = malloc(sizeof(vec_Dbp**) * sentence_length * sentence_length);
      ctx->CDERIV[0][0] = malloc(sizeof(vec_Dbp*) * sentence_length * sentence_length * 2);
      for(start = 0; start < sentence_length; start++) {
          ctx->CDERIV[start] = ctx->CDERIV[0] + start * sentence_length;
          for(end = 0; end < sentence_length; end++) {
              ctx->CDERIV[start][end] = ctx->CDERIV[0][0] + ((start * sentence_length) + end) * 2;
          }
      }
      ctx->ODERIV[0] = malloc(sizeof(vec_Dbp***) * sentence_length * sentence_length);
      ctx->ODERIV[0][0] = malloc(sizeof(vec_Dbp**) * sentence_length * sentence_length * 2);
      ctx->ODERIV[0][0][0] = malloc(sizeof(vec_Dbp*) * sentence_length * sentence_length * 2 * ctx->labels_nb);
      for(start = 0; start < sentence_length; start++) {
          ctx->ODERIV[start] = ctx->ODERIV[0] + start * sentence_length;
          for(end = 0; end < sentence_length; end++) {
              ctx->ODERIV[start][end] = ctx->ODERIV[0][0] + ((start * sentence_length) + end) * 2;
              for(dir = 0; dir < 2; dir++) {
                  ctx->ODERIV[start][end][dir] = ctx->ODERIV[0][0][0] + ((((start * sentence_length) + end) * 2) + dir) * ctx->labels_nb;
              }
          }
      }
  }
  /* ensure CLOSEDK and OPENK are clean */
  for(start=0; start < sentence_length; start++){
    for(end=0; end < sentence_length; end++){
      for(dir=0; dir<2; dir++){
	/* closed */
	ctx->CDERIV[start][end][dir] = NULL;
	/* open */
	for(label=0; label<labels_nb; label++){
	  ctx->ODERIV[start][end][dir][label] = NULL;
	}
      }
    }
  }
  

  /* allocation of the hypergraph */
  /* items in topological order */
  for(span=1; span < sentence_length; span++){
    /* fprintf(stderr, "span: %i\n", span); */

    /* low-level management of backstars */
    /* open */
    int bs_size_open = span; /* maximal size of backstar */
    vec_Vertex *tails_open = alloc_vec_Vertex(2*bs_size_open);
    /* closed */
    int bs_size_closed = span * labels_nb; /* maximal size of backstar */
    vec_Vertex *tails_closed = alloc_vec_Vertex(2*bs_size_closed);
    /* end low-level */
    int length_class;
    int dep_count;
    for(start=0; start+span < sentence_length; start++){
      end = start + span;
      length_class =  maca_graph_parser_dep_count_table_compute_length_class(start,end);

      /* create open items */
      for(dir=0; dir<2; dir++){
	gov = (dir == la) ? end : start;
	dep = (dir == la) ? start : end;

	for(label=0; label<labels_nb; label++){
	  /* min dep count filter */
	  /* TODO: try (unlabelled) length dictionary filter */
	  dep_count = ctx->dep_count_table[s->pos[gov]][s->pos[dep]][label][length_class][dir];
	  if((dep_count >= ctx->min_dep_count) ||
	     (label == dft_label)){
	    /* reset backstar */
	    tails_open->num = 0; /* tail vertices */
	    i = 0; /* actual size of backstar */
	    for(m=start; m<end; m++){
	      if(((ctx->CLOSEDK[start][m][ra] != NULL) || (m == start)) &&
		 ((ctx->CLOSEDK[m+1][end][la] != NULL) || (m+1 == end))){
		vec_Vertex_append(tails_open, ctx->CLOSEDK[start][m][ra]); /* start m */
		vec_Vertex_append(tails_open, ctx->CLOSEDK[m+1][end][la]); /* m+1 end */
		i++;
	      }
	    }
	    if(tails_open->num > 0){ /* non-empty backstar */
	      Vertex *v = alloc_vertex();
	      /* vertex signature */
	      vs = alloc_vertexSignature();
	      init_hyperopen(vs, start, end, label, dir);
	      init_vertex(v, tails_open, i, vs);
	      /* fprintf(stderr, "ctx->OPENK[%d][%d][%d][%d]\n", start, end, label, dir); */
	      ctx->OPENK[start][end][dir][label] = v;
	      /* derivations */
	      ctx->ODERIV[start][end][dir][label] = alloc_vec_Dbp(k);
	      for(k_i=0; k_i<k; k_i++){
		vec_Dbp_append(ctx->ODERIV[start][end][dir][label], alloc_derivBP(MINF, NULL, j));
	      }
	    }
	  }
	} /* end for label */
      } /* end for dir */
      
      /* create closed items */
      /* ra */
      /* reset backstar */
      tails_closed->num = 0; /* tail vertices */
      i = 0; /* actual size of backstar */
      for(m=start+1; m<=end; m++){
	for(label=0; label<labels_nb; label++){
	  if((ctx->OPENK[start][m][ra][label] != NULL) &&
	     ((ctx->CLOSEDK[m][end][ra] != NULL) || (m == end))){
	    vec_Vertex_append(tails_closed, ctx->OPENK[start][m][ra][label]);
	    vec_Vertex_append(tails_closed, ctx->CLOSEDK[m][end][ra]);
	    i++;
	  }
	}
      }
      if(tails_closed->num > 0){ /* backstar is non empty */
	Vertex *v = alloc_vertex();
	/* vertex signature */
	vs = alloc_vertexSignature();
	init_hyperclosed(vs, start, end, ra);
	init_vertex(v, tails_closed, i, vs);
	ctx->CLOSEDK[start][end][ra] = v;
	/* derivations */
	ctx->CDERIV[start][end][ra] = alloc_vec_Dbp(k);
	for(k_i=0; k_i<k; k_i++){
	  vec_Dbp_append(ctx->CDERIV[start][end][ra], alloc_derivBP(MINF, NULL, j));
	}
      }


      /* la */
      tails_closed->num = 0; /* tail vertices */
      i = 0; /* actual size of backstar */
      for(m=start; m<end; m++){
	for(label=0; label<labels_nb; label++){
	  if((ctx->OPENK[m][end][la][label] != NULL) &&
	     ((ctx->CLOSEDK[start][m][la] != NULL) || (m == start))){
	    vec_Vertex_append(tails_closed, ctx->OPENK[m][end][la][label]);
	    vec_Vertex_append(tails_closed, ctx->CLOSEDK[start][m][la]);
	    i++;
	  }
	}
      }
      if(tails_closed->num > 0){
	Vertex *v = alloc_vertex();
	/* vertex */
	vs = alloc_vertexSignature();
	init_hyperclosed(vs, start, end, la);
	init_vertex(v, tails_closed, i, vs);
	ctx->CLOSEDK[start][end][la] = v;
	/* derivations */
	ctx->CDERIV[start][end][la] = alloc_vec_Dbp(k);
	for(k_i=0; k_i<k; k_i++){
	  vec_Dbp_append(ctx->CDERIV[start][end][la], alloc_derivBP(MINF, NULL, j));
	}
      }
    } /* end for start */
    free_vec_Vertex(tails_open);
    free_vec_Vertex(tails_closed);	    
  }
}


void maca_graph_parser_hyperdecoder_cleanup(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s){
  /**
   * Cleanup the decoder and remove the corresponding hypergraph.
   *
   */
  /* global parameters */
  int labels_nb = ctx->labels_nb;
  int k = ctx->k;
  /* sentence */
  int sentence_length = s->l; /* ctx->max_sent_length; */
  /* indices */
  int span;
  int start;
  int end;
  int label; /*label*/
  int k_i;

  /* items in reverse topological order */
  for(span=sentence_length-1; span > 0; span--){
    for(start=0; start+span < sentence_length; start++){
      end = start + span;

      /* free closed items */
      /* ra */
      /* derivations */
      if(ctx->CDERIV[start][end][ra] != NULL){
	for(k_i=0; k_i<k; k_i++){ 
	  if(ctx->CDERIV[start][end][ra]->elts[k_i]){
	    free_derivBP(ctx->CDERIV[start][end][ra]->elts[k_i]);
	    ctx->CDERIV[start][end][ra]->elts[k_i] = NULL;
	  }
	}
	free_vec_Dbp(ctx->CDERIV[start][end][ra]);
	ctx->CDERIV[start][end][ra] = NULL;
      }
      /* vertex */
      if(ctx->CLOSEDK[start][end][ra] != NULL){
	free_vertex(ctx->CLOSEDK[start][end][ra]);
	ctx->CLOSEDK[start][end][ra] = NULL;
      }

      /* la */
      /* derivations */
      if(ctx->CDERIV[start][end][la] != NULL){
	for(k_i=0; k_i<k; k_i++){
	  if(ctx->CDERIV[start][end][la]->elts[k_i]){
	    free_derivBP(ctx->CDERIV[start][end][la]->elts[k_i]);
	    ctx->CDERIV[start][end][la]->elts[k_i] = NULL;
	  }
	}
	free_vec_Dbp(ctx->CDERIV[start][end][la]);
	ctx->CDERIV[start][end][la] = NULL;
      }
      /* vertex */
      if(ctx->CLOSEDK[start][end][la] != NULL){
	free_vertex(ctx->CLOSEDK[start][end][la]);
	ctx->CLOSEDK[start][end][la] = NULL;
      }

      /* free open items */
      for(label=0; label<labels_nb; label++){
	/* ra */
	/* derivations */
	if(ctx->ODERIV[start][end][ra][label] != NULL){
	  for(k_i=0; k_i<k; k_i++){
	    if(ctx->ODERIV[start][end][ra][label]->elts[k_i]){
	      free_derivBP(ctx->ODERIV[start][end][ra][label]->elts[k_i]);
	      ctx->ODERIV[start][end][ra][label]->elts[k_i] = NULL;
	    }
	  }
	  free_vec_Dbp(ctx->ODERIV[start][end][ra][label]);
	  ctx->ODERIV[start][end][ra][label] = NULL;
	}
	if(ctx->OPENK[start][end][ra][label] != NULL){
	  free_vertex(ctx->OPENK[start][end][ra][label]);
	  ctx->OPENK[start][end][ra][label] = NULL;
	}

	/* la */
	/* derivations */
	if(ctx->ODERIV[start][end][la][label] != NULL){
	  for(k_i=0; k_i<k; k_i++){
	    if(ctx->ODERIV[start][end][la][label]->elts[k_i]){
	      free_derivBP(ctx->ODERIV[start][end][la][label]->elts[k_i]);
	      ctx->ODERIV[start][end][la][label]->elts[k_i] = NULL;
	    }
	  }
	  free_vec_Dbp(ctx->ODERIV[start][end][la][label]);
	  ctx->ODERIV[start][end][la][label] = NULL;
	}
	free_vertex(ctx->OPENK[start][end][la][label]);
	ctx->OPENK[start][end][la][label] = NULL;
      }
    }
  }

  /* cleanup indexing arrays */
  if(sentence_length > 0){
    /* free CLOSEDK, OPENK */
    free(ctx->OPENK[0][0][0]);
    free(ctx->OPENK[0][0]);
    free(ctx->OPENK[0]);
    free(ctx->OPENK);
    free(ctx->CLOSEDK[0][0]);
    free(ctx->CLOSEDK[0]);
    free(ctx->CLOSEDK);
    /* free CDERIV, ODERIV */
    free(ctx->ODERIV[0][0][0]);
    free(ctx->ODERIV[0][0]);
    free(ctx->ODERIV[0]);
    free(ctx->ODERIV);
    free(ctx->CDERIV[0][0]);
    free(ctx->CDERIV[0]);
    free(ctx->CDERIV);
  }

}


/*--------------------------------------------------------------------------------------------*/
void get_candidates(maca_graph_parser_ctx *ctx, Vertex *v, int k,
		    heap *cand, cand_id ****seen_cand_ids,
		    maca_graph_parser_feature_table *feat_table){
  /**
   * Get the top k of 1-best derivations for v along each hyperarc.
   *
   * Parameters
   * ----------
   * cand: heap storing the result
   */

  if(v==NULL)
    return;

  /* DEBUG */
  /*
  printf("candidates for ");
  print_vertexSignature(v->vsign);
  printf(":\n");
  */
  /* DEBUG */

  /* 11: temp = set(<e,\bold{1}> for e \in BS(v)) */
  /* 12: cand[v] = top_k_elements(temp) */ /* (optional) prune away useless candidates */
  /* 13: heapify(cand[v]) */
  int j[2] = {0, 0}; /* 1-best vector in 0-based notation */

  int i;
  for(i=0; i<v->bs_size; i++){
    cand_id *c = seen_cand_ids[i][j[0]][j[1]] = alloc_cand_id(i, j);
    float w = eval_weight(ctx, v->bs[i], j, feat_table);

    /* DEBUG */
    /*
    printf("\t");
    if(v->bs[i]->tail[0] != NULL)
      print_vertexSignature(v->bs[i]->tail[0]->vsign);
    if((v->bs[i]->tail[0] != NULL) && 
       (v->bs[i]->tail[1] != NULL))
      printf(" + ");
    if(v->bs[i]->tail[1] != NULL)
      print_vertexSignature(v->bs[i]->tail[1]->vsign);
    printf(" : %f\n", w);
    */
    /* DEBUG */

    heap_insert_nmax(cand, w, c);
  }
}


void append_next(maca_graph_parser_ctx *ctx, heap *cand, DerivBP *p,
		 Vertex *v, cand_id ****seen_cand_ids,
		 maca_graph_parser_feature_table *feat_table){
  /**
   *
   * Parameters
   * ----------
   * cand: candidate set of derivations implemented as a priority queue
   *
   * p: array cell in which the next best derivation will be stored
   * (should be: vector of i-best derivations, i < k, to which the
   * next best derivation will be appended)
   *
   * seen_cand_ids: array of already seen cand ids, i.e. neighbours in the cube
   *
   */

  cand_id *c;
  float w;
  Hyperarc *e;
  int j[2];
  int i;
  int ip;
  int jp[2];
  vec_Dbp *vDbp_Ti;
  cand_id *cp;
  float wp;
  
  /* 9: <e,j> <- extract_min(cand) */
  heap_sort_nmax(cand);
  c = heap_get(cand, 0);
  w = heap_get_value(cand, 0);
  heap_extract_min(cand);
  /* c: (bs_i, j0, j1) */
  e = v->bs[c->bs_i];
  j[0] = c->j[0];
  j[1] = c->j[1];

  /* 10: append <e,j> to p */
  p->weight = w;
  p->e = e;
  p->j[0] = j[0];
  p->j[1] = j[1];

  /* 11: for i=1; i<|e|; i++ do */ /* add the |e| neighbours */
  /* 12:   j' = j + b^i */
  /* 13:   if (j'_i <= |^D(T_i(e))| and <e,j'> \notin cand then */
  /* 14:     insert(cand, <e,j'>) */ /* add to heap */

  /* 11 */
  for(i=0; i<2; i++){
    /* 12 */
    for(ip=0; ip<2; ip++){
      jp[ip] = (i == ip) ? j[i]+1 : j[i];
    }
    /* 13 */
    if(e->tail[i] == NULL){
      /* NULLs don't have 2nd-bests */
      continue;
    }
    vDbp_Ti = get_vec_Dbp(ctx, e->tail[i]);
    /* in the current implementation, the first test only guarantees
       we are not off limits (1) */
    if(jp[i] < vDbp_Ti->num){
      /* if the candidate has not already been used */
      if(seen_cand_ids[c->bs_i][jp[0]][jp[1]] == NULL){
	cp = seen_cand_ids[c->bs_i][jp[0]][jp[1]] = alloc_cand_id(c->bs_i, jp);
      
	/* (1): here is the real checking of j'_i <= |^D(T_i(e))| */
	if (vDbp_Ti->elts[jp[i]]->e != NULL){
	  /* 14 */
	  wp = eval_weight(ctx, e, jp, feat_table);
	  heap_insert_nmax(cand, wp, cp); /* add to heap */
	}
      }
    }
  }

}


void find_kbest(maca_graph_parser_ctx *ctx, Vertex *v, int k, maca_graph_parser_feature_table *feat_table){
  /**
   * Find the k best derivations for v.
   */

  int i;
  /* int j[2] = {-1, -1}; */
  int ja;
  int jb;
  heap *cand;
  cand_id ****seen_cand_ids; /* [bs_i][j0][j1] */
  vec_Dbp *vDbp_v;

  if(v == NULL)
    return;
  
  vDbp_v = get_vec_Dbp(ctx, v);
  /* alloc and init the array of already seen cand_ids */
  seen_cand_ids = malloc(sizeof(cand_id ***) * (v->bs_size));
  if(seen_cand_ids == NULL){
    fprintf(stderr, "Mem alloc error\n");
    exit(1);
  }
  if(v->bs_size > 0){
    seen_cand_ids[0] = malloc(sizeof(cand_id **) * (v->bs_size) * k);
    if(seen_cand_ids[0] == NULL){
      fprintf(stderr, "Mem alloc error\n");
      exit(1);
    }
    seen_cand_ids[0][0] = malloc(sizeof(cand_id *) * (v->bs_size) * k * k);
    if(seen_cand_ids[0][0] == NULL){
      fprintf(stderr, "Mem alloc error\n");
      exit(1);
    }
    for(i=0; i<v->bs_size; i++){
      seen_cand_ids[i] = seen_cand_ids[0] + i * k;
      for(ja=0; ja<k; ja++){
	seen_cand_ids[i][ja] = seen_cand_ids[0][0] + (i * k + ja) * k;
	for(jb=0; jb<k; jb++){
	  seen_cand_ids[i][ja][jb] = NULL;
	}
      }
    }
  }

  /* 6: get_candidates(v, k) */ /* initialize the heap */
  cand = heap_create(k, MINF);
  get_candidates(ctx, v, k, cand, seen_cand_ids, feat_table);

  /* 7: while | \bold{^D}(v) | < k and | cand[v] | > 0 do
     8:   append_next(cand[v], \bold{^D}(v)) */
  for(i=0; i<k; i++){
    if(heap_size(cand) <= 0)
      break;
    append_next(ctx, cand, vDbp_v->elts[i], v, seen_cand_ids, feat_table);
  }

  /* cleanup */
  if(seen_cand_ids){
    for(i=0; i<v->bs_size; i++){
      if(seen_cand_ids[i]){
	for(ja=0; ja<k; ja++){
	  if(seen_cand_ids[i][ja]){
	    for(jb=0; jb<k; jb++){
	      if(seen_cand_ids[i][ja][jb]){
		free_cand_id(seen_cand_ids[i][ja][jb]);
		seen_cand_ids[i][ja][jb] = NULL;
	      }
	    }
	  }
	}
      }
    }
    free(seen_cand_ids[0][0]);
    free(seen_cand_ids[0]);
    free(seen_cand_ids);
    seen_cand_ids = NULL;
  }
  heap_destroy(cand);

  /* FIXME: move this info msg where it belongs */
  /*
    if (ctx->verbose_flag > 4)
    fprintf(stderr, "\tctx->OPENK[%d][%d][%d][%d](%f) = ctx->CLOSEDK[%d][%d][1], ctx->CLOSEDK[%d][%d][0]\n",
    start, end, dir, label, (score_max + feat_table->pl[start][end][dir] + w),
    start, m_argmax, m_argmax+1, end);
  */
     
}


void find_all_kbest(int k,
		    maca_graph_parser_ctx *ctx,
		    maca_graph_parser_sentence *s,
		    maca_graph_parser_feature_table *feat_table){
  /**
   * Find the k best analyses for a sentence.
   */

  int sentence_length = s->l;
  int labels_nb = ctx->labels_nb;
  Vertex *head;

  /* sanitary check */
  if (k > ctx->k){
    fprintf(stderr, "ERR: k > ctx->k\n");
  }

  /* 2: for v \in V in topological order do :
     3:   find_kbest(v,k)
   */
  int span;
  for(span = 1; span < sentence_length; span++){
    int start;
    for(start = 0; start+span < sentence_length; start++){
      int end = start + span;
      if(ctx->verbose_flag > 4) fprintf(stderr, "start = %d end = %d\n",start,end);

      int dir;
      for(dir=0; dir<2; dir++){
	/* OPEN table */
	int label;
	for(label=0; label<labels_nb; label++){
	  head = ctx->OPENK[start][end][dir][label];
	  find_kbest(ctx, head, k, feat_table);
	}
	
	/* CLOSED table */
	head = ctx->CLOSEDK[start][end][dir];
	find_kbest(ctx, head, k, feat_table);
	
      } /* end for dir */
    } /* end for start */
  } /* end for start */
}


void maca_graph_parser_set_parse(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s, int ki){
  /**
   * Annotate s with its ki-th parse.
   *
   * FIXME: this is a dirty temporary hack.
   */
  int i;

  /* backport ki-th best from kb to s proper */
  for(i=1; i<s->l; i++){
    s->gov[i] = s->kb->gov[i][ki];
    s->label[i] = s->kb->label[i][ki];
  }
  s->score = s->kb->score[ki];
}


void maca_graph_parser_kbest_output(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s){
  /**
   * Output the kbest solutions found.
   */

  int ki;
  DerivBP *bestSpan = NULL;
  vec_Dbp *vd;

  /* ensure there is a kb to store the k-best parses */
  if(s->kb == NULL){
    s->kb = maca_graph_parser_allocate_sentence_kbest(ctx);
  }
  /* reset kb */
  maca_graph_parser_reset_sentence_kbest(s->kb);

  /* store the k-best parses from the forest into kb */
  vd = ctx->CDERIV[0][s->l-1][ra];
  if(vd){
    for(ki=0; (ki < vd->num) && (ki < ctx->k); ki++){
      bestSpan = vd->elts[ki];
      if(bestSpan->e != NULL){
	/* in the current implementation, e == NULL
	   iff there is no ki-th best solution */
	create_closedBP(ctx, bestSpan, s, ki);
	s->kb->score[ki] = bestSpan->weight;
      }
    }
  }

  /* backport 1-st best from kb to s proper */
  maca_graph_parser_set_parse(ctx, s, 0);
}


feature_counter_array *extract_features_from_kbest_parse_fca(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s, feature_counter_array *a, int ki){
  /**
   * Extract features from the ki-th best parse of s.
   *
   * Adapted from maca_graph_parser_feature_counter_array.c:extract_features_from_parse_fca().
   *
   * FIXME: find a way to share code with the original function.
   * FIXME: find a way to directly use the features used for decoding,
   * which are currently computed and forgotten in feature_table.
   */

  if(s->kb == NULL){
    return a; /* FIXME: raise an error */
  }

  if(a == NULL){
    a = allocate_feature_counter_array(ctx, (s->l - 1));
  } else { /* reset a */
    free_feature_counter_array(a);
    a = allocate_feature_counter_array(ctx, (s->l - 1));
  }

  feature_counter *c;
  feat_vector *v;
  int dep, gdep, sbl;
  int g;
  int i;

  if(ctx->basic_features){
    c = a->basic_feature_counter;
    v = NULL;
    for(dep=1; dep < s->l; dep++){
      v = basic(s, ctx, s->kb->gov[dep][ki], dep, v);
      feature_counter_update_vector(c, v);
    }
    free_feat_vector(v);
    v = NULL;
  }

  if(ctx->first_features){
    c = a->first_feature_counter;
    v = NULL;
    for(dep=1; dep < s->l; dep++){
      v = first(s, ctx, s->kb->gov[dep][ki], dep, s->kb->label[dep][ki], v);
      feature_counter_update_vector(c, v);
    }
    free_feat_vector(v);
    v = NULL;
  }

  if(ctx->sibling_features){
    c = a->sibling_feature_counter;
    v = NULL;
    for(dep=1; dep < s->l; dep++){
      // MM
      sbl = -1;
      g = s->kb->gov[dep][ki]; // governor
      /* wanted sibling: child of g in [g..dep] that is closest to dep */
      if (g < dep) { /* ra */
	for(i=dep-1; i > g; i--){
	  if(g == s->kb->gov[i][ki]){ // && (dep != i)
	    sbl = i;
	    break;
	  }
	}
      } else { /* la */
	for(i=dep+1; i < g; i++){
	  if(g == s->kb->gov[i][ki]){ // (dep != i) && 
	    sbl = i;
	    break;
	  }
	}	
      }
      /* sbl == -1 if no sibling */
      v = sibling(s, ctx, s->kb->gov[dep][ki], dep, sbl, s->kb->label[dep][ki], v);
      feature_counter_update_vector(c, v);
    }
    free_feat_vector(v);
    v = NULL;
  }

  if(ctx->grandchildren_features){
    c = a->grandchildren_feature_counter;
    v = NULL;
    for(dep=1; dep < s->l; dep++){
      // MM
      g = s->kb->gov[dep][ki];
      if (g < dep){ /* ra */
	/* cmi: inside [g;dep] */
	gdep = -1;
	for(i=dep-1; i > g; i--){
	  if(s->kb->gov[i][ki] == dep){
	    gdep = i;
	  }
	}
	v = grandchildren(s, ctx, g, dep, gdep, s->kb->label[dep][ki], v);
	feature_counter_update_vector(c, v);
	/* cmo: outside [g;dep] */
	gdep = -1;
	for(i=dep+1; i<s->l; i++){
	  if(s->kb->gov[i][ki] == dep){
	    gdep = i;
	  }
	}
	v = grandchildren(s, ctx, g, dep, gdep, s->kb->label[dep][ki], v);	
	feature_counter_update_vector(c, v);
      } else { /* la */
	/* cmi: inside [dep;g] */
	gdep = -1;
	for(i=dep+1; i < g; i++){
	  if(s->kb->gov[i][ki] == dep){
	    gdep = i;
	  }
	}
	v = grandchildren(s, ctx, g, dep, gdep, s->kb->label[dep][ki], v);
	feature_counter_update_vector(c, v);
	/* cmo: outside [dep;g] */
	gdep = -1;
	for(i=dep-1; i>0; i--){
	  if(s->kb->gov[i][ki] == dep){
	    gdep = i;
	  }
	}
	v = grandchildren(s, ctx, g, dep, gdep, s->kb->label[dep][ki], v);
	feature_counter_update_vector(c, v);
      }
    }
    free_feat_vector(v);
    v = NULL;
  }

  return a;
}


int maca_graph_parser_rescore_kbest(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s){
  /**
   * Rescore the k-best parses of s with higher-order factors.
   *
   * Currently uses second-order factors.
   *
   * FIXME: Currently returns the index of the best parse.
   */

  vec_Dbp *derivs;
  int ki;
  feature_counter_array *hyp_feature_counter_array;
  int nb_cands;
  float score_cand;
  float score_max;
  int index_max;
  
  /* init */
  nb_cands = 0;
  score_max = MINF;
  /* activate 2nd order features */
  int gra_feats = ctx->grandchildren_features;
  int sib_feats = ctx->sibling_features;
  ctx->grandchildren_features = 1;
  ctx->sibling_features = 1;
  /* end activate */
  hyp_feature_counter_array = allocate_feature_counter_array(ctx, (s->l - 1));

  /* */
  derivs = ctx->CDERIV[0][s->l-1][ra];
  if(derivs){
    for(ki=0; (ki < derivs->num) && (ki < ctx->k); ki++){
      if(derivs->elts[ki]->e != NULL){
	/* rescore */
	hyp_feature_counter_array = extract_features_from_kbest_parse_fca(ctx, s, hyp_feature_counter_array, ki);
	score_cand = score_feature_counter_array(hyp_feature_counter_array, ctx->model2);
	/* DEBUG */
	/* fprintf(stderr, "ki = %d: score = %f\n", ki, score_cand); */
	/* DEBUG */

	nb_cands++;

	if(nb_cands == 1){
	  index_max = ki;
	  score_max = score_cand;
	} else {
	  if(score_cand > score_max){
	    index_max = ki;
	    score_max = score_cand;
	  }
	}
      }
    } /* end for ki */
  }

  /* teardown feat counter */
  free_feature_counter_array(hyp_feature_counter_array);
  /* restore 2nd order features */
  ctx->grandchildren_features = gra_feats;
  ctx->sibling_features = sib_feats;
    
  return index_max;
}


void maca_graph_parser_hyperdecoder_parse(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s, maca_graph_parser_feature_table *feat_table){
  /**
   * Parse s with the hypergraph decoder.
   */

  find_all_kbest(ctx->k, ctx, s, ctx->feature_table);
  /* annotate the sentence with the k-best structures */
  maca_graph_parser_kbest_output(ctx, s);
  /* rescore kbest with higher-order factors */
  if(ctx->model2){
    int index_max = maca_graph_parser_rescore_kbest(ctx, s);
    maca_graph_parser_set_parse(ctx, s, index_max);
  }
}

/*-------------------------------------------------------------------------------------------*/

void create_closedBP(maca_graph_parser_ctx *ctx, DerivBP *d, maca_graph_parser_sentence *s, int ki){
  /**
   * Annotate sentence with the ki-th best parse.
   */

  /*
  if(c->dir == la){
    printf("create_closed [%d;(%d);<%d>] from closed [%d;<%d>] and open [%d;<%d>]\n",
	   c->start, c->breakpoint, c->end,
	   (c->d)? c->d->start : -1, (c->d)? c->d->end : -1,
	   (c->u)? c->u->start : -1, (c->u)? c->u->end : -1);
  } else {
    printf("create_closed [<%d>;(%d);%d] from open [<%d>;%d] and closed [<%d>;%d]\n",
	   c->start, c->breakpoint, c->end,
	   (c->u)? c->u->start : -1, (c->u)? c->u->end : -1,
	   (c->d)? c->d->start : -1, (c->d)? c->d->end : -1);
  }
  */
  int i;
  Vertex *Ti_e;
  DerivBP *DbpTi_e;

  if(d->e == NULL){
    fprintf(stderr, "hyperdecoder.c:closedBP(): weird but ignored at the moment\n");
    return;
  }
  /* recursive calls along each component */
  /* should be: one create_closedBP(), one create_openBP() */
  for(i=0; i<2; i++){
    Ti_e = d->e->tail[i];
    if(Ti_e){
      DbpTi_e = get_vec_Dbp(ctx, Ti_e)->elts[d->j[i]];
      if(Ti_e->vsign->type == TYP_CLOSED){
	create_closedBP(ctx, DbpTi_e, s, ki);
      } else if(Ti_e->vsign->type == TYP_OPEN){
	create_openBP(ctx, DbpTi_e, s, ki);
      } else {
	fprintf(stderr, "Not implemented yet\n");
      }
    }
  }
}

/*-------------------------------------------------------------------------------------------*/

void create_openBP(maca_graph_parser_ctx *ctx, DerivBP *d, maca_graph_parser_sentence *s, int ki){
  /**
   * Annotate sentence with the ki-th best parse.
   */

  /*
  if (o->dir == la){
    printf("create_open [%d;<%d>] from left closed [<%d>;%d] and right closed [%d;<%d>]\n",
	   o->start, o->end,
	   (o->left)? o->left->start : -1, (o->left)? o->left->end : -1,
	   (o->right)? o->right->start : -1, (o->right)? o->right->end : -1);
  } else {
    printf("create_open [<%d>;%d] from left closed [<%d;%d>] and right closed [<%d;%d>]\n",
	   o->start, o->end,
	   (o->left)? o->left->start : -1, (o->left)? o->left->end : -1,
	   (o->right)? o->right->start : -1, (o->right)? o->right->end : -1);
  }
  */
  SgOpen o;
  int gov;
  int dep;
  int i;
  Vertex *Ti_e;
  DerivBP *DbpTi_e;


  if(d->e == NULL){
    fprintf(stderr, "hyperdecoder.c:openBP(): weird but ignored at the moment\n");
    return;
  }
  /* update sentence */
  o = d->e->head->vsign->open;
  gov = (o.dir == la) ? o.end : o.start;
  dep = (o.dir == la) ? o.start : o.end;
  s->kb->gov[dep][ki] = gov;
  s->kb->label[dep][ki] = o.label;

  /* recursive calls along each component */
  /* should be: two create_closedBP() */
  for(i=0; i<2; i++){
    Ti_e = d->e->tail[i];
    if(Ti_e){
      DbpTi_e = get_vec_Dbp(ctx, Ti_e)->elts[d->j[i]];
      if(Ti_e->vsign->type == TYP_CLOSED){
	create_closedBP(ctx, DbpTi_e, s, ki);
      } else if(Ti_e->vsign->type == TYP_OPEN){
	create_openBP(ctx, DbpTi_e, s, ki);
      } else {
	fprintf(stderr, "Not implemented yet\n");
      }
    }
  }
}
