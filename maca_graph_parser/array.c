#include <stdlib.h>
#include "array.h"

array_t* array_new() {
  array_t* array = (array_t *)malloc(sizeof(array_t));
    array->num_elements = 0;
    array->data = NULL;
    return array;
}

void array_free(array_t* array) {
    if(array->data) free(array->data);
    free(array);
}

ARRAY_TYPE array_get(array_t* array, int element) {
    return array->data[element];
}

void array_push(array_t* array, ARRAY_TYPE value) {
  array->data = (ARRAY_TYPE *)realloc(array->data, sizeof(ARRAY_TYPE) * (array->num_elements + 1));
    array->data[array->num_elements] = value;
    array->num_elements++;
}

