/*******************************************************************************
    Copyright (C) 2012 by Alexis Nasr <alexis.nasr@lif.univ-mrs.fr>
                          Ghasem Mirroshandel <ghasem.mirroshandel@lif.univ-mrs.fr>
    This file is part of maca_graph_parser.

    maca_graph_parser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    maca_graph_parser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with maca_graph_parser. If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#include<stdlib.h>
#include<stdio.h>
#include<time.h>
#include"maca_constants.h"
#include"maca_msg.h"
#include"maca_graph_parser_decoder.h"
#include"maca_graph_parser_features.h"
#include"maca_graph_parser.h"
#include "maca_graph_parser_dep_count_table.h"
#include "maca_graph_parser_feature_table.h"


/*-------------------------------------------------------------------------------------------*/

//Closed *CLOSED2
//[MACA_MAX_LENGTH_SENTENCE] /* start */
//[MACA_MAX_LENGTH_SENTENCE] /* end */
//[2] /* position of the root (left or right)*/
//[MACA_MAX_LENGTH_SENTENCE] /* position of a son of the root  */
//;

//Open *OPEN2
//[MACA_MAX_LENGTH_SENTENCE] /* start */
//[MACA_MAX_LENGTH_SENTENCE] /* end */
//[2] /* position of the root (left or right) */
//[NB_LABELS] /* label */
//;
/*-------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------*/
void maca_graph_parser_decoder2_cleanup(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s)
{
  int labels_nb = ctx->labels_nb;
  int sentence_length = s->l;
  int start;
  int end;
  int dir; /* left or right */
  int label; /*label*/
  int m;

  for(start=0; start < sentence_length; start++){
    for(end=0; end < sentence_length; end++){
      for(dir=0; dir<2; dir++){
	/* closed */
	for(m=0; m<sentence_length; m++){
	  free(ctx->CLOSED2[start][end][dir][m]);
	  ctx->CLOSED2[start][end][dir][m] = NULL;
	}
	/* open */
	for(label=0; label<labels_nb; label++){
	  free(ctx->OPEN2[start][end][dir][label]); 
	  ctx->OPEN2[start][end][dir][label] = NULL;
	}
      }
    }
  }

  // faster implementation
  if(sentence_length > 0) {
      free(ctx->OPEN2[0][0][0]);
      free(ctx->OPEN2[0][0]);
      free(ctx->OPEN2[0]);
      free(ctx->OPEN2);
      free(ctx->CLOSED2[0][0][0]);
      free(ctx->CLOSED2[0][0]);
      free(ctx->CLOSED2[0]);
      free(ctx->CLOSED2);
  }
  // reference implementation
  /*for(start = 0; start < sentence_length; start++) {
      for(end = 0; end < sentence_length; end++) {
          for(dir = 0; dir < 2; dir++) {
              free(ctx->OPEN2[start][end][dir]);
          }
          free(ctx->OPEN2[start][end]);
      }
      free(ctx->OPEN2[start]);
  }
  free(ctx->OPEN2);
  for(start = 0; start < sentence_length; start++) {
      for(end = 0; end < sentence_length; end++) {
          for(dir = 0; dir < 2; dir++) {
              free(ctx->CLOSED2[start][end][dir]);
          }
          free(ctx->CLOSED2[start][end]);
      }
      free(ctx->CLOSED2[start]);
  }
  free(ctx->CLOSED2);*/
}

/*-------------------------------------------------------------------------------------------*/
void maca_graph_parser_decoder2_init(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s)
{
  int labels_nb = ctx->labels_nb;
  int sentence_length = s->l;
  int start;
  int end;
  int dir; /* left or right */
  int label; /*label*/
  int m; /* breakpoint */

  // faster implementation
  ctx->CLOSED2 = malloc(sizeof(Closed****) * sentence_length);
  ctx->OPEN2 = malloc(sizeof(Open****) * sentence_length);
  if(sentence_length > 0) {
      ctx->CLOSED2[0] = malloc(sizeof(Closed**) * sentence_length * sentence_length);
      ctx->CLOSED2[0][0] = malloc(sizeof(Closed*) * sentence_length * sentence_length * 2);
      ctx->CLOSED2[0][0][0] = malloc(sizeof(Closed*) * sentence_length * sentence_length * 2 * sentence_length);
      for(start = 0; start < sentence_length; start++) {
          ctx->CLOSED2[start] = ctx->CLOSED2[0] + start * sentence_length;
          for(end = 0; end < sentence_length; end++) {
              ctx->CLOSED2[start][end] = ctx->CLOSED2[0][0] + ((start * sentence_length) + end) * 2;
              for(dir = 0; dir < 2; dir++) {
                  ctx->CLOSED2[start][end][dir] = ctx->CLOSED2[0][0][0] + ((((start * sentence_length) + end) * 2) + dir) * sentence_length;
              }
          }
      }
      ctx->OPEN2[0] = malloc(sizeof(Open***) * sentence_length * sentence_length);
      ctx->OPEN2[0][0] = malloc(sizeof(Open**) * sentence_length * sentence_length * 2);
      ctx->OPEN2[0][0][0] = malloc(sizeof(Open*) * sentence_length * sentence_length * 2 * ctx->labels_nb);
      for(start = 0; start < sentence_length; start++) {
          ctx->OPEN2[start] = ctx->OPEN2[0] + start * sentence_length;
          for(end = 0; end < sentence_length; end++) {
              ctx->OPEN2[start][end] = ctx->OPEN2[0][0] + ((start * sentence_length) + end) * 2;
              for(dir = 0; dir < 2; dir++) {
                  ctx->OPEN2[start][end][dir] = ctx->OPEN2[0][0][0] + ((((start * sentence_length) + end) * 2) + dir) * ctx->labels_nb;
              }
          }
      }
  }
  // reference implementation
  /*ctx->CLOSED2 = malloc(sizeof(Closed****) * sentence_length);
  for(start = 0; start < sentence_length; start++) {
      ctx->CLOSED2[start] = malloc(sizeof(Closed***) * sentence_length);
      for(end = 0; end < sentence_length; end++) {
          ctx->CLOSED2[start][end] = malloc(sizeof(Closed**) * 2);
          for(dir = 0; dir < 2; dir++) {
              ctx->CLOSED2[start][end][dir] = malloc(sizeof(Closed*) * sentence_length);
          }
      }
  }
  ctx->OPEN2 = malloc(sizeof(Open****) * sentence_length);
  for(start = 0; start < sentence_length; start++) {
      ctx->OPEN2[start] = malloc(sizeof(Open***) * sentence_length);
      for(end = 0; end < sentence_length; end++) {
          ctx->OPEN2[start][end] = malloc(sizeof(Open**) * 2);
          for(dir = 0; dir < 2; dir++) {
              ctx->OPEN2[start][end][dir] = malloc(sizeof(Open*) * ctx->labels_nb);
          }
      }
  }*/

  for(start=0; start < sentence_length; start++){
    for(end=0; end < sentence_length; end++){
      for(dir=0; dir < 2; dir++){
	/* closed */
	for(m=0; m < sentence_length; m++){
	  ctx->CLOSED2[start][end][dir][m] = NULL;
	}
	/* open */
	for(label=0; label < labels_nb; label++){
	  ctx->OPEN2[start][end][dir][label] = NULL;
	}
      }
    }
  }

  /* create base substructures for dynamic programming: closed with span 0 */
  /* right attachments only for the fake root node at index 0 */
  ctx->CLOSED2[0][0][ra][0] = alloc_closed(0, 0, 0, 0, ra, NULL, NULL);
  /* left and right attachments for the other nodes */
  for(start = 1; start < sentence_length; start++){
    ctx->CLOSED2[start][start][la][start] = alloc_closed(0, start, start, start, la, NULL, NULL);
    ctx->CLOSED2[start][start][ra][start] = alloc_closed(0, start, start, start, ra, NULL, NULL);
  }
}

/*-------------------------------------------------------------------------------------------*/
void maca_graph_parser_decoder2_decode(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s, maca_graph_parser_feature_table *feat_table)
{
  int labels_nb = ctx->labels_nb;
  int n = s->l;
  int span;
  int start;
  int end;
  int label;
  int dir; /* left or right */
  int gov;
  int dep;
  int nb_cands;
  float score_max;
  Closed *max_LC;
  Closed *max_RC;
  int m_argmax;
  int argmax_i_left;
  int argmax_j_right;
  int m; /*breakpoint (middle)*/
  int nb_cands_cands;
  /* */
  Closed *cand_LC;
  float score_cand_LC;
  int i_LC;
  int i; /* position of grandchildren or sibling for left closed*/
  Closed *cand_cand_LC;
  float score_cand_cand_LC;
  /* */
  Closed *cand_RC;
  float score_cand_RC;
  int j_RC;
  int j; /* position of grandchildren or sibling for right closed*/
  Closed *cand_cand_RC;
  float score_cand_cand_RC;
  /* */
  float w;
  float w_first;
  float w_basic;
  /* */
  Closed *max_C;
  Open *max_O;
  /* int argmax_i; */
  int label_argmax;
  Open *cand_O;
  float score_cand_O;
  Closed *cand_C;
  float score_cand_C;
  float score_cand;
  /* test: default edge label */
  /* int dft_label = maca_tags_get_code(ctx->cfg, "morpho", "fct", "__JOKER__"); */
  int dft_label = maca_alphabet_get_code(ctx->labels_alphabet, "__JOKER__");
  int length_class;
  feat_vector *fv_basic = NULL;
  feat_vector *fv_first = NULL;
  feat_vector *fv_sibling = NULL;
  feat_vector *fv_grandchildren = NULL;

  for(span = 1; span < n; span++){
    for(start=0; start < n-span; start++){
      end = start + span;
      if (PRINT) printf("start = %d end = %d\n",start,end);
      length_class =  maca_graph_parser_dep_count_table_compute_length_class(start, end);
      /* fill OPEN2 table*/
      for(label=0; label<labels_nb; label++){
	if (PRINT) printf("\t\t\tlabel = %d\n",label);
	
	for(dir=0; dir<2; dir++){
	  gov = (dir == la)? end: start;
	  dep = (dir == la)? start: end;
	
	  if((ctx->dep_count_table[s->pos[gov]][s->pos[dep]][label][length_class][dir] >= ctx->min_dep_count) ||
	     (label == dft_label)){
	    /* init */
	    nb_cands = 0;
	    score_max = (float) MINF;
	    max_LC = NULL;
	    max_RC = NULL;
	    m_argmax = start;
	    argmax_i_left = start;
	    argmax_j_right = end;
	  
	    /* find best pair of CLOSED2 substructures */
	    for(m=start; m<end; m++){

	      /* left closed */
	      /* find the best candidate */
	      nb_cands_cands = 0;
	      cand_LC = NULL;
	      score_cand_LC = (float) MINF;
	      i_LC = start;
	      for(i=start; i <= m; i++){
		cand_cand_LC = ctx->CLOSED2[start][m][ra][i];
		if((cand_cand_LC != NULL) || (m == start)){
		  score_cand_cand_LC = cand_cand_LC ? cand_cand_LC->score : 0;

		  if(ctx->store_in_feature_table){
		    score_cand_cand_LC += (dir == la) ? feat_table->gra[start][end][i][dir][label] : feat_table->sib[start][end][i][dir][label];
		  }
		  else{
		    /*--compute scores on the fly ---*/
		    if(dir == la){
		      fv_grandchildren = maca_graph_parser_grandchildren_score(gov, dep, i, label, ctx, fv_grandchildren, &w); 
		      score_cand_cand_LC += w;
		    }
		    else{
		      fv_sibling = maca_graph_parser_sibling_score(gov, dep, i, label, ctx, fv_sibling, &w); 
		      score_cand_cand_LC += w;
		    }
		  }
		  
		  

		  nb_cands_cands++;

		  /* update cand LC */
		  if(nb_cands_cands == 1){
		    score_cand_LC = score_cand_cand_LC;
		    cand_LC = cand_cand_LC;
		    i_LC = i;
		  } else {
		    if(score_cand_cand_LC > score_cand_LC){
		      score_cand_LC = score_cand_cand_LC;
		      cand_LC = cand_cand_LC;
		      i_LC = i;
		    }
		  }
		}
	      }
	      /* for this m, no MLC => no OPEN2 */
	      if(nb_cands_cands == 0)
		continue;
		  
	      /* right closed */
	      /* find the best candidate */
	      nb_cands_cands = 0;
	      cand_RC = NULL;
	      score_cand_RC = (float) MINF;
	      j_RC = end;
	      for(j=m+1; j <= end; j++){
		cand_cand_RC = ctx->CLOSED2[m+1][end][la][j];
		if((cand_cand_RC != NULL) || (m+1 == end)){
		  score_cand_cand_RC = cand_cand_RC ? cand_cand_RC->score : 0;

		  if(ctx->store_in_feature_table){
		    score_cand_cand_RC += (dir == la) ? feat_table->sib[start][end][j][dir][label] : feat_table->gra[start][end][j][dir][label]; 
		  }
		  else {
		    /* --- compute scores on the fly */
		    if(dir == la){
		      fv_sibling = maca_graph_parser_sibling_score(start, end, j, label, ctx, fv_sibling, &w); 
		      score_cand_cand_RC += w;
		    }
		    else{
		      fv_grandchildren = maca_graph_parser_grandchildren_score(start, end, j, label, ctx, fv_grandchildren, &w); 
		      score_cand_cand_RC += w;
		    }
		  }

		  nb_cands_cands++;

		  /* update cand RC */
		  if(nb_cands_cands == 1){
		    score_cand_RC = score_cand_cand_RC;
		    cand_RC = cand_cand_RC;
		    j_RC = j;
		  } else {
		    if(score_cand_cand_RC > score_cand_RC){
		      score_cand_RC = score_cand_cand_RC;
		      cand_RC = cand_cand_RC;
		      j_RC = j;
		    }
		  }
		}
	      }
	      /* for this m, no MRC => no OPEN2 */
	      if(nb_cands_cands == 0)
		continue;

	      /* only candidates reach this point */
	      nb_cands++;

	      if (ctx->verbose_flag > 4) fprintf(stderr, "\t\tcand = CLOSED2[%d][%d](%f) + CLOSED2[%d][%d](%f)\n", 
						 start, m, score_cand_LC,
						 m+1, end, score_cand_RC);

	      /* update max */
	      if(nb_cands == 1){
		/* first candidate */
		score_max = score_cand_LC + score_cand_RC;
		max_LC = cand_LC;
		max_RC = cand_RC;
		m_argmax = m;
		argmax_i_left = i_LC;
		argmax_j_right = j_RC;
	      } else {
		if(score_cand_LC + score_cand_RC > score_max){
		  score_max = score_cand_LC + score_cand_RC;
		  max_LC = cand_LC;
		  max_RC = cand_RC;
		  m_argmax = m;
		  argmax_i_left = i_LC;
		  argmax_j_right = j_RC;
		}
	      }

	    } /* end for m */

	    /* create OPEN2 if there is at least a candidate */
	    if(nb_cands > 0){
	      /* score from basic and first order features */
		  if(ctx->store_in_feature_table){
		    w = feat_table->pl[start][end][dir] + feat_table->lab[start][end][label][dir];
		  }
		  else{
		    /* compute scores on the fly */
		    fv_first = maca_graph_parser_first_score(start, end, label, ctx, fv_first, &w_first);
		    fv_basic = maca_graph_parser_basic_score(gov, dep, ctx, fv_basic, &w_basic);  
		    w = w_first + w_basic;
		  }

	      if(ctx->verbose_flag > 4){
		char bstart[128];
		char bend[128];
		maca_alphabet_get_symbol(ctx->words_alphabet,s->words[start], bstart, sizeof(bstart));
		maca_alphabet_get_symbol(ctx->words_alphabet,s->words[end], bend, sizeof(bend));
		fprintf(stderr, "\tscore(%s(%d) %s-%d-%s %s(%d)) = %f\n", 
		        bstart, start,
			(dir==la)?"<":"", label, (dir==la)?"":">",
		        bend, end,
			w);
	      }

	      ctx->OPEN2[start][end][dir][label] = alloc_open(score_max + w,
							 start, end, label, dir,
							 max_LC, max_RC);

	      if (ctx->verbose_flag > 4)
		fprintf(stderr, "\tOPEN2[%d][%d][%d][%d](%f) = CLOSED2[%d][%d][1][%d], CLOSED2[%d][%d][0][%d]\n",
			start, end, dir, label, (score_max + w),
			start, m_argmax, argmax_i_left,
			m_argmax+1, end, argmax_j_right);
	    } /* end check there is at least one candidate */

	  } /* end filter min_dep_count */
	} /* end dir */
      } /* end label */
      /* end OPEN2 table */
      
      /* fill CLOSED2 table */
      int argmax_i;
      for(m=start; m<=end; m++){
	/* la */
	dir = la;
	if ((m < end) && (start != 0)){
	  /* init */
	  nb_cands = 0;
	  score_max = MINF;
	  max_C = NULL;
	  max_O = NULL;
	  argmax_i = start; // MM: unsure
	  label_argmax = -1;

	  for(label=0; label<labels_nb; label++){
	    for(i=m; i >= start; i--){

	      cand_O = ctx->OPEN2[m][end][dir][label];
	      if(cand_O != NULL)
		score_cand_O = cand_O->score;
	      else
		continue;

	      cand_C = ctx->CLOSED2[start][m][dir][i];
	      if((cand_C != NULL) || (m == start))
		score_cand_C = cand_C ? cand_C->score : 0;
	      else
		continue;

	      /* only candidates reach this point */
	      nb_cands++;

	      score_cand = score_cand_O + score_cand_C;
	      if(ctx->store_in_feature_table){
		score_cand += feat_table->gra[m][end][i][dir][label]; 
	      }
	      else{
		/* compute scores on the fly */
		fv_grandchildren = maca_graph_parser_grandchildren_score(m, end, i, label, ctx, fv_grandchildren, &w); 
		score_cand += w;
	      }


	      /* update max */
	      if(nb_cands == 1){
		score_max = score_cand;
		max_C = cand_C;
		max_O = cand_O;
		argmax_i = i;
		label_argmax = label;
	      } else {
		if(score_cand > score_max){
		  score_max = score_cand;
		  max_C = cand_C;
		  max_O = cand_O;
		  argmax_i = i;
		  label_argmax = label;
		}
	      }

	    }
	  } /* end for label */

	  /* create best closed2 */
	  if(nb_cands > 0){
	    ctx->CLOSED2[start][end][dir][m] = alloc_closed(score_max,
						       start, end, m, dir,
						       max_C, max_O);
	  
	    if (ctx->verbose_flag > 4)
	      fprintf(stderr, "\tCLOSED2[%d][%d][%d][%d](%f) = OPEN2[%d][%d][%d][%d], CLOSED2[%d][%d][%d][%d]\n",
		      start, end, dir, m, score_max,
		      m, end, dir, label_argmax,
		      start, m, dir, argmax_i);
	  }
	}
	/* end la */
	
	/* ra */
	dir = ra;
	if (m > start){
	  /* init */
	  nb_cands = 0;
	  score_max = MINF;
	  max_C = NULL;
	  max_O = NULL;
	  argmax_i = end; // MM: unsure
	  label_argmax = -1;

	  for(label=0; label<labels_nb; label++){
	    for(i = m; i <= end; i++){

	      cand_O = ctx->OPEN2[start][m][dir][label];
	      if(cand_O != NULL)
		score_cand_O = cand_O->score;
	      else
		continue;

	      cand_C = ctx->CLOSED2[m][end][dir][i];
	      if((cand_C != NULL) || (m == end))
		score_cand_C = cand_C ? cand_C->score : 0;
	      else
		continue;
	      
	      /* only candidates reach this point */
	      nb_cands++;
	      
	      score_cand = score_cand_O + score_cand_C;
	      if(ctx->store_in_feature_table){
		score_cand += feat_table->gra[start][m][i][dir][label]; 
	      }
	      else{
		/* compute scores on the fly */
		fv_grandchildren = maca_graph_parser_grandchildren_score(start, m, i, label, ctx, fv_grandchildren, &w); 
		score_cand += w;
	      }

	      /* update max */
	      if(nb_cands == 1){
		score_max = score_cand;
		max_C = cand_C;
		max_O = cand_O;
		argmax_i = i;
		label_argmax = label;
	      } else {
		if(score_cand > score_max){
		  score_max = score_cand;
		  max_C = cand_C;
		  max_O = cand_O;
		  argmax_i = i;
		  label_argmax = label;
		}
	      }
	      
	    }
	  } /* end for label */

	  /* create best closed2 */
	  if(nb_cands > 0){
	    ctx->CLOSED2[start][end][ra][m] = alloc_closed(score_max,
						      start, end, m, dir,
						      max_C, max_O);

	    if (ctx->verbose_flag > 4)
	      fprintf(stderr, "\tCLOSED2[%d][%d][%d][%d](%f) = OPEN2[%d][%d][%d][%d], CLOSED2[%d][%d][%d][%d]\n",
		      start, end, dir, m, score_max,
		      start, m, dir, label_argmax,
		      m, end, dir, argmax_i); 
	  }
	} /* end ra */

      } /* end m */
    } /* end start */
  } /* end span */
}

/*-------------------------------------------------------------------------------------------*/

void maca_graph_parser_decoder2_parse(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s, maca_graph_parser_feature_table *feat_table){
  Closed *bestSpan = NULL;
  float max = (float) MINF;
  float cand = (float) MINF;
  int nb_cands;
  int argmax;
  int i;
  
  maca_graph_parser_decoder2_decode(ctx, s, ctx->feature_table);

  /* find best structure for the sentence */
  nb_cands = 0;
  for(i=1; i < s->l; i++){

    if(ctx->CLOSED2[0][s->l-1][ra][i] != NULL){
      cand = ctx->CLOSED2[0][s->l-1][ra][i]->score;
      nb_cands++;

      /* update max */
      if(nb_cands == 1){
	max = cand;
	argmax = i;
      } else {
	if(cand > max){
	  max = cand;
	  argmax = i;
	}
      }
    }
  }

  if(nb_cands > 0){
    bestSpan = ctx->CLOSED2[0][s->l-1][ra][argmax];
    create_closed(bestSpan, s);
    s->score = bestSpan->score;
  }
}


  /*fill CLOSED2 table ALEXIS*/
  //	max = MINF;
  //	m_argmax = start;
  //	for(m=start; m<=end; m++){
  //	  for(label=0; label<L; label++){
  //	    cand = MINF;
  //	    if(dir == la){
  //	      if(m < end){
  //		cand = (OPEN2[m][end][dir][label]?  OPEN2[m][end][dir][label]->score : 0)
  //		  + (CLOSED2[start][m][dir]? CLOSED2[start][m][dir]->score: 0);
  //		if (PRINT) printf("\t\t\tcand = %f + %f\n",
  //		       (OPEN2[m][end][dir][label]?  OPEN2[m][end][dir][label]->score : 0),
  //		       (CLOSED2[start][m][dir]? CLOSED2[start][m][dir]->score: 0));
  //	      }
  //	    }
  //	    else{ /* dir == ra */
  //	      if(m > start){
  //		cand = (OPEN2[start][m][dir][label]? OPEN2[start][m][dir][label]->score: 0)
  //		  + (CLOSED2[m][end][dir]? CLOSED2[m][end][dir]->score : 0);
  //		if (PRINT) printf("\t\t\tcand = %f + %f\n",
  //		       (OPEN2[start][m][dir][label]? OPEN2[start][m][dir][label]->score: 0) ,
  //		       (CLOSED2[m][end][dir]? CLOSED2[m][end][dir]->score : 0));
  //	      }
  //	    }
  //	    if(max < cand){
  //	      max = cand;
  //	      m_argmax = m;
  //	      label_argmax = label;
  //	    }
  //	  }
  //	}
  //	if(dir == la){
  //	  CLOSED2[start][end][dir] = alloc_closed(max,
  //						  start,
  //						  end,
  //						  m_argmax,
  //						  dir,
  //						  CLOSED2[start][m_argmax][dir],
  //						  OPEN2[m_argmax][end][dir][label_argmax]);
  //
  //	  if (PRINT) printf("\t\t\t\tCLOSED2[%d][%d][%d](%f) = CLOSED2[%d][%d][%d], OPEN2[%d][%d][%d][%d]\n", start,end,dir,max,start,m_argmax,dir,m_argmax,end,dir,label_argmax);
  //	}
  //	else /* dir == ra */{
  //	  CLOSED2[start][end][dir] = alloc_closed(max,
  //						       start,
  //						       end,
  //						       m_argmax,
  //						       dir,
  //						       CLOSED2[m_argmax][end][dir],
  //						       OPEN2[start][m_argmax][dir][label_argmax]);
  //	  if (PRINT) printf("\t\t\t\tCLOSED2[%d][%d][%d](%f) = CLOSED2[%d][%d][%d], OPEN2[%d][%d][%d][%d]\n", start,end,dir,max,m_argmax,end,dir,start,m_argmax,dir,label_argmax);
  //	}
  //      }

  /*-------------------------------------------------------------------------------------------*/
//
//void parser(int n)
//{
//  int t; /*end*/
//  int s; /*start*/
//  int d; /*direction*/
//  int l; /*label*/
//  int m; /*breakpoint (middle)*/
//  int i; /* */
//  double MO; /*max open*/
//  double MLC; /*max left closed*/
//  double MRC; /*max right closed*/
//  double MC; /*max closed*/
//  double cand;
//  
//  for(t=0; t<n; t++){
//    for(s=t; s >=0; s--){
//      for(d=0; d<2; d++){
//	/* fill OPEN table*/
//	for(l=0; l<labels_nb; l++){
//	    MO = MINF;
//	    for(m=s; m<=t; m++){
//	      MLC = MINF;
//	      for(i=s; s<m; i++){
//		if(d==1) cand = CLOSED2[s][m][1][i] + SIB(s,t,i,0,l);
//		else cand = CLOSED2[s][m][1][i] + GRA(t,s,i,1,l);
//		if (cand > MLC) MLC=cand;
//	      }
//	      MRC = MINF;
//	      for(i=m; s<t; i++){
//		if(d==1) cand = CLOSED2[i][t][0][i] + GRA(s,t,i,0,l);
//		else cand = CLOSED2[m][t][0][i] + SIB(t,s,i,1,l);
//		if (cand > MRC) MRC=cand;
//	      }
//	      cand = MLC + MRC;
//	      if(MO < cand) MO = cand;
//	    }
//	    OPEN2[s][t][d][l]=MO;
//	}
//	
//	
//	/*fill CLOSED table*/
//	for(m=s; m<=t; m++){
//	  CLOSED2[s][t][d][m] = MINF;
//	  for(l=0; l<L; l++){
//	    if(d == 0){
//	      MO = OPEN2[m][t][d][l];
//	      MC = MINF;
//	      for(i=s; s<m; i++){
//		cand = CLOSED2[s][m][0][i] + GRA(t,m,i,1,l);
//		if (cand > MC) MC=cand;
//	      }
//	    }
//	    else{ /* d == 1*/
//	      MO = OPEN2[s][m][d][l];
//	      MC = MINF;
//	      for(i=m; s<t; i++){
//		  cand = CLOSED2[m][t][1][i] + GRA(s,m,i,0,l);
//		  if (cand > MC) MC=cand;
//	      }
//	    }
//	    cand = MO + MC;
//	    if(CLOSED2[s][t][d][m] < cand) CLOSED2[s][t][d][m] = cand;
//	  }
//	}
//      }
//    }
//  }
//}
  
