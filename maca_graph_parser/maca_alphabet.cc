#include "maca_alphabet.hh"
#include <sstream>

// int main(int argc, char **argv) {

//   if (argc - 1 != 1) {
//     std::cerr << "Need 1 arg\n";
//     exit(1);
//   }
  
//   std::cout << "This is a test executable!\n";

//   macaon::Alphabet alphabet("TEST");
  
//   alphabet.addSymbol("Orange");
//   int code2 = alphabet.addSymbol("Poire");

//   int code = alphabet.getCode("Orange");
//   std::cout << "Code of Orange: " << code << std::endl;
//   std::cout << "Symbol of " << code2 << " is " << alphabet.getSymbol(code2) << std::endl;
//   std::cout << "We can also access it like that: " << alphabet[1] << std::endl;

//   std::string file(argv[1]);
//   std::cout << "Loading alphabets from " << file << std::endl;
//   macaon::AlphabetArray alphaArray(file);
  
//   std::cout << "We've loaded " << alphaArray.size() << " alphabets\n";

//   alphaArray.addAlphabet(&alphabet);
  
//   if (alphabet == *(alphaArray.getAlphabet("TEST"))) {
//     std::cout << "They are equal!\n";
//   }

//   alphaArray.removeAlphabet("WORDS", true);

//   alphaArray.dump();
  
//   return 0;
// }

namespace macaon {
  Alphabet::Alphabet(const std::string name, bool loaded /*=false*/): name(name), loaded(loaded) {
    nb = 0;
    locked = false;
  }

  std::string Alphabet::getName() {
    return name;
  }

  int Alphabet::addSymbol(const std::string symbol) {
    int code;
    std::map<std::string,int>::iterator it = wordCodeMap.find(symbol);
    if (it == wordCodeMap.end()) {
      if (locked) {
        throw std::runtime_error("Tried to add new symbol but alphabet is locked!"); 
      }
      code = nb;
      wordCodeMap.insert(std::map<std::string,int>::value_type(symbol,code));
      codeWordArray.push_back(symbol);
      nb++;
    } else {
      code = it->second;
    }

    return code;
  }

  int Alphabet::getCode(const std::string &symbol) {
    std::map<std::string,int>::iterator it = wordCodeMap.find(symbol);
    if (it == wordCodeMap.end()) {
      throw std::runtime_error("Symbol does not exist in this alphabet!");
    }
    return it->second;
  }

  std::string &Alphabet::getSymbol(const int &code) {
    return codeWordArray.at(code);
  }

  int Alphabet::size() {
    return nb;
  }

  void Alphabet::lock() {
    locked = true;
  }

  void Alphabet::unlock() {
    locked = false;
  }

  bool Alphabet::isLoaded() {
    return loaded;
  }

  void Alphabet::dump(std::ostream &out) {
    out << "##" << name << "\n";
    for (std::vector<std::string>::size_type i = 0; i < codeWordArray.size(); i++) {
      out << codeWordArray[i] << "\n";
    }
  }

  bool Alphabet::operator ==(const Alphabet &a) {
    if (name != a.name)
      return false;
    if (nb != a.nb)
      return false;
    if (locked != a.locked)
      return false;
    if (loaded != a.loaded)
      return false;
    if (codeWordArray != a.codeWordArray)
      return false;
    return true;
  }
  
  bool Alphabet::operator !=(const Alphabet &a) {
    if (name != a.name)
      return true;
    if (nb != a.nb)
      return true;
    if (locked != a.locked)
      return true;
    if (loaded != a.loaded)
      return true;
    if (codeWordArray != a.codeWordArray)
      return true;
    return false;
  }

  int Alphabet::operator [](const std::string &s) {
    return getCode(s);
  }

  std::string &Alphabet::operator [](const int &i) {
    return getSymbol(i);
  }

  AlphabetArray::AlphabetArray() {
    nb = 0;
  }

  AlphabetArray::AlphabetArray(std::string filename) {
    nb = 0;
    load(filename);
  }

  // AlphabetArray::~AlphabetArray() {
  //   for (std::map<std::string, Alphabet*>::iterator it_map = alphabets.begin(); 
  // 	 it_map != alphabets.end();
  // 	 it_map++) {
  //     // if (createdAlphabets.find(it_map->first) == createdAlphabets.end())
  //     // 	continue;
      
  //     delete it_map->second;
  //   }
  // }

  bool AlphabetArray::addAlphabet(std::shared_ptr<Alphabet> alphabet) {
    auto res = alphabets.insert(std::map<std::string,std::shared_ptr<Alphabet>>
		     ::value_type(alphabet->getName(), alphabet));
    if (res.second) 
      nb++;
    return res.second;
  }

  std::shared_ptr<Alphabet> AlphabetArray::removeAlphabet(const std::string name) {
    std::map<std::string,std::shared_ptr<Alphabet>>::iterator it = alphabets.find(name);
    if (it == alphabets.end()) {
      return nullptr;
    }
    std::shared_ptr<Alphabet> a = it->second;

    alphabets.erase(name);
    //createdAlphabets.erase(name);
    nb--;

    return a;
  }

  std::shared_ptr<Alphabet> AlphabetArray::getAlphabet(const std::string name) {
    std::map<std::string, std::shared_ptr<Alphabet>>::iterator it = alphabets.find(name);
    if (it == alphabets.end()) {
      return nullptr;
    }
    return it->second;
  }

  bool AlphabetArray::has(const std::string name) {
    std::map<std::string, std::shared_ptr<Alphabet>>::iterator it = alphabets.find(name);
    if (it == alphabets.end())
      return false;
    return true;
  }

  int AlphabetArray::size() {
    return nb;
  }

  void AlphabetArray::load(std::string filename) {
    std::string symbol;
    std::shared_ptr<Alphabet> alphabet(nullptr);
    std::ifstream f(filename);
    if (!f.is_open()) {
      std::stringstream ss;
      ss << "Can't open file '" << filename << "' !";
      throw std::runtime_error(ss.str()); 
    }
    
    while (f >> symbol) {
      std::string alphaBegin = "##";
      if (symbol.compare(0, alphaBegin.length(), alphaBegin) == 0) {
	symbol.erase(0,2);
        alphabet = std::shared_ptr<Alphabet>(new Alphabet(symbol, true));
	addAlphabet(alphabet);
      } else {
	if (alphabet != NULL) {
	  alphabet->addSymbol(symbol);
	}
      }
    }
    f.close();
  }

  void AlphabetArray::dump(std::ostream &out) {
    std::map<std::string, std::shared_ptr<Alphabet>>::iterator it;
    for (it = alphabets.begin(); it != alphabets.end(); it++) {
      it->second->dump(out);
    }
  }

  void AlphabetArray::dump(std::string filename/*=""*/) {
    std::ofstream of;

    if (filename != "") {
      of.open(filename);
    }

    std::ostream &out = (filename != "") ? of : std::cout;

    if (filename != "" && !of.is_open()) {
      std::stringstream ss;
      ss << "Can't open file '" << filename << "' !";
      throw std::runtime_error(ss.str());
    }
    
    dump(out);
    
    if (filename != "") {
      of.close();
    }
  }

  std::shared_ptr<Alphabet> AlphabetArray::operator [](const std::string &name) {
    return getAlphabet(name);
  }
}
