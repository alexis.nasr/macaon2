#include "maca_alphabet.hh"
#include "maca_alphabet_wrapper.h"
#include <cstring>
#include <memory>

extern "C" {
  maca_alphabet* maca_alphabet_new(const char* name, char loaded) {
    std::shared_ptr<macaon::Alphabet> a(new macaon::Alphabet(name, (loaded != 0)));
    PtrPool<macaon::Alphabet>::getInstance().accept(a);

    return a.get();
  }

  void maca_alphabet_delete(maca_alphabet *a) {
    PtrPool<macaon::Alphabet>::getInstance().release(a);
  }

  void maca_alphabet_release(maca_alphabet *a) {
    maca_alphabet_delete(a);
  }

  void maca_alphabet_get_name(maca_alphabet *a, char* name, int size) {
    strncpy(name, a->getName().c_str(), size);
  }

  int maca_alphabet_add_symbol(maca_alphabet *a, const char* symbol) {
    try {
      return a->addSymbol(symbol);
    } catch (std::exception &e) {
      return -1;
    }
  }

  int maca_alphabet_get_code(maca_alphabet *a, const char* symbol) {
    try {
      return a->getCode(symbol);
    } catch (std::exception &e) {
      return -1;
    }
  }

  char maca_alphabet_get_symbol(maca_alphabet *a, int code, char* name, int size) {
    try {
      std::string sName = a->getSymbol(code);
      strncpy(name, sName.c_str(), size);
      return 1;
    } catch (std::exception &e) {
      return 0;
    }
  }

  int maca_alphabet_size(maca_alphabet *a) {
    return a->size();
  }

  void maca_alphabet_lock(maca_alphabet *a) {
    a->lock();
  }

  void maca_alphabet_unlock(maca_alphabet *a) {
    a->unlock();
  }

  char maca_alphabet_is_loaded(maca_alphabet *a) {
    return a->isLoaded();
  }


  maca_alphabet_array *maca_alphabet_array_new() {
    return new macaon::AlphabetArray();
  }

  maca_alphabet_array *maca_alphabet_array_new_from_file(const char* filename) {
    try {
      return new macaon::AlphabetArray(filename);
    } catch (std::exception &e) {
      return NULL;
    }
  }

  void maca_alphabet_array_delete(maca_alphabet_array *array) {
    delete array;
  }

  char maca_alphabet_array_add_alphabet(maca_alphabet_array *array, maca_alphabet *a) {
    std::shared_ptr<macaon::Alphabet> ptr = PtrPool<macaon::Alphabet>::getInstance().get(a);
    return array->addAlphabet(ptr);
  }

  maca_alphabet *maca_alphabet_array_remove_alphabet(maca_alphabet_array *array, const char* name,
						     char free_alphabet) {
    std::shared_ptr<macaon::Alphabet> a = array->removeAlphabet(name);
    if (free_alphabet) {
      return NULL;
    }
    
    return PtrPool<macaon::Alphabet>::getInstance().accept(a);
  }

  maca_alphabet *maca_alphabet_array_get_alphabet(maca_alphabet_array *array, const char* name) {
      std::shared_ptr<macaon::Alphabet> a = array->getAlphabet(name);
      return PtrPool<macaon::Alphabet>::getInstance().accept(a);
  }

  char maca_alphabet_array_has_alphabet(maca_alphabet_array *array, const char* name) {
    return array->has(name);
  }

  int maca_alphabet_array_size(maca_alphabet_array *array) {
    return array->size();
  }

  char maca_alphabet_array_load(maca_alphabet_array *array, const char* filename) {
    try {
      array->load(filename);
      return 1;
    } catch (std::exception &e) {
      return 0;
    }
  }

  char maca_alphabet_array_dump(maca_alphabet_array *array, const char* filename) {
    try {
      array->dump(filename);
      return 1;
    } catch (std::exception &e) {
      return 0;
    }
  }

}
