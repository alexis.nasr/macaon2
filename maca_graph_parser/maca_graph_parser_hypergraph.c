/**
 * Hypergraph.
 */

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>

#include "maca_graph_parser_hypergraph.h"


/**
 * Vertex signature
 */

VertexSignature *alloc_vertexSignature(){
  /**
   * Allocate a vertex signature.
   */

  VertexSignature *v = malloc(sizeof(VertexSignature));
  if(v == NULL){
    fprintf(stderr, "memory allocation problem\n");
    exit(1);
  }

  return v;
}

void free_vertexSignature(VertexSignature *v){
  /**
   *
   */
  if(!v)
    return;

  free(v);
}


void print_vertexSignature(VertexSignature *v){
  /**
   * Print a vertex signature to stdout.
   */
  if(v->type == TYP_OPEN){
    printf("OPEN[%d][%d][%d][%d]", v->open.start, v->open.end, v->open.dir, v->open.label);
  } else if (v->type == TYP_CLOSED){
    printf("CLOSED[%d][%d][%d]", v->closed.start, v->closed.end, v->closed.dir);
  } else if (v->type == TYP_CLOSED2){
    printf("CLOSED2[%d][%d][%d][%d]", v->closed2.start, v->closed2.end, v->closed2.dir, v->closed2.breakpoint);
  } else {
    printf("Not implemented yet\n");
  }
}


void init_hyperopen(VertexSignature *vs, int start, int end, int label, int dir){
  /**
   *
   */

  if(vs == NULL){
    fprintf(stderr, "Cannot set NULL vertex signature\n");
    exit(1);
  }
  vs->open.type = TYP_OPEN;
  vs->open.start = start;
  vs->open.end = end;
  vs->open.dir = dir;
  vs->open.label = label;
}


void init_hyperclosed(VertexSignature *vs, int start, int end, int dir){
  /**
   *
   */

  if(vs == NULL){
    fprintf(stderr, "Cannot set NULL vertex signature\n");
    exit(1);
  }
  vs->closed.type = TYP_CLOSED;
  vs->closed.start = start;
  vs->closed.end = end;
  vs->closed.dir = dir;
}


void init_hyperclosed2(VertexSignature *vs, int start, int end, int breakpoint, int dir){
  /**
   *
   */

  if(vs == NULL){
    fprintf(stderr, "Cannot set NULL vertex signature\n");
    exit(1);
  }
  vs->closed2.type = TYP_CLOSED2;
  vs->closed2.start = start;
  vs->closed2.end = end;
  vs->closed2.dir = dir;
  vs->closed2.breakpoint = breakpoint;
}


/**
 * Vertex
 */

Vertex *alloc_vertex(){
  /**
   * Allocate a vertex whose tail tails has size bs_size.
   */

  Vertex *v = malloc(sizeof(Vertex));
  if(v == NULL){
    fprintf(stderr, "memory allocation problem\n");
    exit(1);
  }
  /* vertex signature */
  v->vsign = NULL;
  /* backstar */
  v->bs = NULL;
  v->bs_size = 0;

  return v;
}


void init_vertex(Vertex *v, vec_Vertex *tails, size_t bs_size, VertexSignature *vs){
  /**
   * Init a vertex with a signature and a backstar.
   * TODO: s/vec_Vertex *tails, size_t bs_size/vec_Hyperarc *backstar
   */

  /* vertex signature */
  v->vsign = vs;
  /* backstar */
  v->bs = malloc(bs_size * sizeof(Hyperarc *));
  Vertex *t[2];
  int i;
  for(i=0; i<bs_size; i++){
    t[0] = tails->elts[2*i];
    t[1] = tails->elts[2*i+1];
    v->bs[i] = alloc_hyperarc(t, v);
  }
  v->bs_size = bs_size;  
}


void free_vertex(Vertex *v){
  /**
   *
   */
  int i;

  if(!v)
    return;
  
  free_vertexSignature(v->vsign);
  v->vsign = NULL;
  
  for(i=0; i<v->bs_size; i++){
    free_hyperarc(v->bs[i]);
    v->bs[i] = NULL;
  }
  free(v->bs);
  v->bs = NULL;

  free(v);
}


/**
 * Hyperarc stuff
 */

Hyperarc *alloc_hyperarc(Vertex *tail[2], Vertex *head){
  /**
   * Allocate an hyperarc.
   *
   * Parameters
   * ----------
   *
   */
  Hyperarc *e = malloc(sizeof(Hyperarc));
  if(e == NULL){
    fprintf(stderr, "memory allocation problem\n");
    exit(1);
  }
  e->tail[0] = tail[0];
  e->tail[1] = tail[1];
  e->head = head;
  return e;
}

void free_hyperarc(Hyperarc *e){
  /**
   *
   */
  if (!e)
    return;

  /* free e and NULLify its content */
  e->head = NULL;
  e->tail[0] = NULL;
  e->tail[1] = NULL;
  free(e);
}

/**
 * Derivation stuff
 */

Derivation *alloc_derivation(float weight, Hyperarc *e, Derivation *subd[2]){
  /**
   *
   */
  Derivation *d = malloc(sizeof(Derivation));
  if(d==NULL){
    fprintf(stderr, "memory allocation problem\n");
    exit(1);
  }
  d->weight = weight;
  d->e = e;
  d->subd[0] = subd[0];
  d->subd[1] = subd[1];
  return d;
}

void free_derivation(Derivation *d){
  /**
   *
   */
  if (!d)
    return;

  d->e = NULL;
  free(d);
}

DerivBP *alloc_derivBP(float weight, Hyperarc *e, int j[2]){
  /**
   * Allocate a derivation with backpointers.
   *
   * Parameters
   * ----------
   *
   */
  DerivBP *d = malloc(sizeof(DerivBP));
  if(d == NULL){
    fprintf(stderr, "memory allocation problem\n");
    exit(1);    
  }
  d->weight = weight;
  d->e = e;
  d->j[0] = j[0];
  d->j[1] = j[1];
  return d;
}

void free_derivBP(DerivBP *d){
  /**
   *
   */
  if (!d)
    return;

  d->e = NULL;
  free(d);
}


/**
 * Vector of vertices
 */

vec_Vertex *alloc_vec_Vertex(int capacity){
  /**
   * Allocate a vector of vertices.
   */
  vec_Vertex *res = malloc(sizeof(vec_Vertex));
  if(res == NULL){
    fprintf(stderr, "Mem prob\n");
    exit(1);
  }

  res->num = 0;
  res->capacity = capacity;
  res->elts = malloc(capacity * sizeof(Vertex *));
  if(res->elts == NULL){
    fprintf(stderr, "Mem prob\n");
    exit(1);
  }

  return res;
}


void free_vec_Vertex(vec_Vertex *vv){
  /**
   * Free a vector of vertices
   */
  if(!vv)
    return;

  free(vv->elts);
  free(vv);
}


void vec_Vertex_append(vec_Vertex *vv, Vertex *v){
  /**
   * Append to a vector of vertices
   */
  if(vv->num >= vv->capacity){
    fprintf(stderr, "Cannot append: Vector full\n");
    return;
  }
  vv->elts[vv->num] = v;
  vv->num += 1;
}
