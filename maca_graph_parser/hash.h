#ifndef __HASH__
#define __HASH__

#define HASH_INVALID_VAL -1

typedef struct _cell
{
  char *key;
  int val;
  struct _cell *next; 
} cell;

typedef struct
{
  int size;
  int nbelem;
  cell **array;
} hash_t;


cell *cell_new(char *key, int val, cell *next);
void cell_free(cell *c);

hash_t *hash_new(int size);
void hash_free(hash_t *h);
cell *hash_lookup(hash_t *h, char *key);
int hash_get_val(hash_t *h, char *key);
void hash_add(hash_t *h, char *key, int val);
void hash_stats(hash_t *h);


#endif
