/*******************************************************************************
    Copyright (C) 2012 by Alexis Nasr <alexis.nasr@lif.univ-mrs.fr>
                          Ghasem Mirroshandel <ghasem.mirroshandel@lif.univ-mrs.fr>
                          Jeremy Auguste <jeremy.auguste@etu.univ-amu.fr>
    This file is part of maca_graph_parser.

    maca_graph_parser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    maca_graph_parser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with maca_graph_parser. If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

#include<getopt.h>
#include<unistd.h>
#include<stdio.h>
#include<string.h>

#include "maca_common.h"
#include "maca_constants.h"
/* #include <maca_lex.h> */
#include "maca_graph_parser.h"
#include "maca_graph_parser_model.h"
#include "maca_graph_parser_sentence.h"
#include "maca_graph_parser_decoder.h"
#include "maca_graph_parser_metrics.h"
#include "maca_graph_parser_train.h"
/* #include "maca_graph_parser_conll2007_format.h" */
#include "maca_graph_parser_features.h"
#include "maca_graph_parser_feature_table.h"


void maca_graph_parser_decode_main(maca_graph_parser_ctx * ctx);
void maca_graph_parser_train_main(maca_graph_parser_ctx * ctx);


int main(int argc, char **argv)
{
  maca_graph_parser_ctx * ctx;

  /* no argument on command line */
  if(argc == 2 && !strcmp(argv[1], "-h"))
    {
      //maca_graph_parser_PrintHelpMessage(argv[0]);
      exit(0);
    }

  /* sinon lecture des options dans la ligne de commande */
  ctx = maca_graph_parser_LoadCTX(argc,argv);
 
  /* maca_graph_parser_init(ctx); */

  /* maca_lex_load_cfg(ctx->cfg); */


  if(ctx->mode == DECODE_MODE)
    maca_graph_parser_decode_main(ctx);
  else 
    if(ctx->mode == TRAIN_MODE)
      maca_graph_parser_train_main(ctx);

  maca_graph_parser_free_all(ctx);
  return 0;
}



void maca_graph_parser_decode_main(maca_graph_parser_ctx * ctx)
{
  maca_alphabet_array *alpha_array;
  /* int i; */
  int sent_num;
  /*maca_sentence * ms;*/

  /* model */
  //ctx->model = maca_graph_parser_model_mmap(ctx, ctx->model_file_name);
  ctx->model = maca_graph_parser_model_load(ctx, ctx->model_file_name);
  /* model2 */
  if(ctx->model2_file_name != NULL){
    ctx->model2 = maca_graph_parser_model_load(ctx, ctx->model2_file_name);
  } else {
    ctx->model2 = NULL;
  }
  /* set active feature types for the decoder */
  ctx->min_dep_count = ctx->model->min_dep_count;
  ctx->use_lemmas = ctx->model->use_lemmas;
  ctx->use_full_forms = ctx->model->use_full_forms;
  ctx->basic_features = ctx->model->basic_features;
  ctx->first_features = ctx->model->first_features;
  ctx->grandchildren_features = ctx->model->grandchildren_features;
  ctx->sibling_features = ctx->model->sibling_features;
  ctx->subcat_features = ctx->model->subcat_features;

  if(ctx->sibling_features || ctx->grandchildren_features) ctx->order = 2;

  /* alphabets */
  /* load alphabets */

  alpha_array = maca_alphabet_array_new_from_file(ctx->alphabet_file_name);
  if (alpha_array == NULL) {
    maca_msg(ctx->module, MACA_ERROR);
    fprintf(stderr, "couldn't open the alphabet file!\n");
    exit(1);
  }
  ctx->words_alphabet = maca_alphabet_array_get_alphabet(alpha_array, MACA_ALPHABET_WORDS);
  ctx->words_nb = (ctx->words_alphabet != NULL) ? maca_alphabet_size(ctx->words_alphabet) : 0;
  ctx->labels_alphabet = maca_alphabet_array_get_alphabet(alpha_array, MACA_ALPHABET_LABELS);
  ctx->labels_nb = (ctx->labels_alphabet != NULL) ? maca_alphabet_size(ctx->labels_alphabet) : 0;
  ctx->pos_alphabet = maca_alphabet_array_get_alphabet(alpha_array, MACA_ALPHABET_POS);
  ctx->pos_nb = (ctx->pos_alphabet != NULL) ? maca_alphabet_size(ctx->pos_alphabet) : 0;
  ctx->morpho_alphabet = maca_alphabet_array_get_alphabet(alpha_array, MACA_ALPHABET_MORPHO);
  ctx->morpho_nb = (ctx->morpho_alphabet != NULL) ? maca_alphabet_size(ctx->morpho_alphabet) : 0;
  ctx->synt_feats_alphabet = maca_alphabet_array_get_alphabet(alpha_array, MACA_ALPHABET_SYNT_FEATS);
  ctx->synt_feats_nb = (ctx->synt_feats_alphabet != NULL) ? maca_alphabet_size(ctx->synt_feats_alphabet) : 0;

  /* /\* store special values in ctx and check that every necessary alphabet is loaded *\/ */
  if (ctx->use_full_forms || ctx->use_lemmas) {
    if (ctx->words_alphabet == NULL) {
      maca_msg(ctx->module, MACA_ERROR);
      fprintf(stderr, "missing the '"MACA_ALPHABET_WORDS"' alphabet in the alphabet file\n");
      exit(1);
    }
    ctx->w_start = maca_alphabet_get_code(ctx->words_alphabet, "__START__");
    ctx->w_end = maca_alphabet_get_code(ctx->words_alphabet, "__END__");
  }
  
  if (ctx->pos_alphabet == NULL) {
    maca_msg(ctx->module, MACA_ERROR);
    fprintf(stderr, "missing the '"MACA_ALPHABET_POS"' alphabet in the alphabet file\n");
    exit(1);
  }
  ctx->pos_start = maca_alphabet_get_code(ctx->pos_alphabet, "__START__");
  ctx->pos_end = maca_alphabet_get_code(ctx->pos_alphabet, "__END__");

  if (ctx->labels_alphabet == NULL) {
    maca_msg(ctx->module, MACA_ERROR);
    fprintf(stderr, "missing the '"MACA_ALPHABET_LABELS"' alphabet in the alphabet file\n");
    exit(1);
  }
  ctx->fct_joker = maca_alphabet_get_code(ctx->labels_alphabet, "__JOKER__");

  if (ctx->subcat_features) {
    if (ctx->synt_feats_alphabet == NULL) {
      maca_msg(ctx->module, MACA_ERROR);
      fprintf(stderr, "missing the '"MACA_ALPHABET_SYNT_FEATS"' alphabet in the alphabet file\n");
      exit(1);
    }
  }
  /* end alphabets */



  /* template library allocator needs: words_nb, pos_nb, labels_nb */
  ctx->e = maca_graph_parser_templ_library_allocator(ctx);
    
  /* load dep_count_table */
  ctx->dep_count_table = maca_graph_parser_dep_count_table_read(ctx, ctx->dep_count_table_file_name);


  /* allocate feature table */
  if(ctx->store_in_feature_table){
    maca_graph_parser_feature_table_allocator(ctx);
  }
  
  ctx->s = maca_graph_parser_allocate_sentence(ctx);
  
  if(ctx->print_ctx) maca_graph_parser_print_ctx(ctx);  
  
  

  if(ctx->mcf_file_name){
    maca_mcf_sentence *mcf_sent;
    maca_mcf_column *column;
    maca_mcf *format = maca_mcf_new_with_alphabet_array(ctx->mcf_file_name, alpha_array);
    char buffer[128];
    
    /* INPUTS */
    /* full form */
    if(ctx->use_full_forms){
      ctx->mcf_form_id = maca_mcf_input(format, MACA_MCF_FORM);
      if(ctx->mcf_form_id == -1){
	maca_msg(ctx->module, MACA_ERROR);
	fprintf(stderr, "column FORM not found in the train file\n");
	exit(1);
      }
      /* check that the alphabet used is correct */
      column = maca_mcf_get_column_info(format, ctx->mcf_form_id);
      maca_mcf_column_get_type(column, buffer, sizeof(buffer));
      if (strcmp(buffer, MACA_ALPHABET_WORDS) != 0) {
	maca_msg(ctx->module, MACA_ERROR);
	fprintf(stderr, "column FORM is not using the "MACA_ALPHABET_WORDS" alphabet\n");
	exit(1);	
      }
    }

    /* lemmas */
    if (ctx->use_lemmas){
      ctx->mcf_lemma_id = maca_mcf_input(format, MACA_MCF_LEMMA);
      if (ctx->mcf_lemma_id == -1){
	maca_msg(ctx->module, MACA_ERROR);
	fprintf(stderr, "column LEMMA not found in the train file\n");
	exit(1);
      }
      /* check that the alphabet used is correct */
      column = maca_mcf_get_column_info(format, ctx->mcf_lemma_id);
      maca_mcf_column_get_type(column, buffer, sizeof(buffer));
      if (strcmp(buffer, MACA_ALPHABET_WORDS) != 0) {
	maca_msg(ctx->module, MACA_ERROR);
	fprintf(stderr, "column LEMMA is not using the "MACA_ALPHABET_WORDS" alphabet\n");
	exit(1);	
      }
    }

    /* postag */
    ctx->mcf_postag_id = maca_mcf_input(format, MACA_MCF_POSTAG);
    if (ctx->mcf_postag_id == -1){
      maca_msg(ctx->module, MACA_ERROR);
      fprintf(stderr, "column POSTAG not found in the train file\n");
      exit(1);
    }
    /* check that the alphabet used is correct */
    column = maca_mcf_get_column_info(format, ctx->mcf_postag_id);
    maca_mcf_column_get_type(column, buffer, sizeof(buffer));
    if (strcmp(buffer, MACA_ALPHABET_POS) != 0) {
      maca_msg(ctx->module, MACA_ERROR);
      fprintf(stderr, "column POSTAG is not using the "MACA_ALPHABET_POS" alphabet\n");
      exit(1);	
    }
    /* lock the postag alphabet */
    maca_alphabet_lock(ctx->pos_alphabet);
    
    /* subcat */
    if (ctx->subcat_features) {
      ctx->mcf_subcat_id = maca_mcf_input(format, MACA_MCF_SUBCAT);
      if (ctx->mcf_subcat_id == -1) {
	maca_msg(ctx->module, MACA_ERROR);
	fprintf(stderr, "column SUBCAT not found in the train file\n");
	exit(1);
      }
      /* check that the alphabet used is correct */
      column = maca_mcf_get_column_info(format, ctx->mcf_subcat_id);
      maca_mcf_column_get_type(column, buffer, sizeof(buffer));
      if (strcmp(buffer, MACA_ALPHABET_SYNT_FEATS) != 0) {
	maca_msg(ctx->module, MACA_ERROR);
	fprintf(stderr, "column SUBCAT is not using the "MACA_ALPHABET_SYNT_FEATS" alphabet\n");
	exit(1);	
      }
    }

    /* OUTPUTS */
    /* deprel */
    ctx->mcf_deprel_id = maca_mcf_output(format, MACA_MCF_DEPREL, MACA_ALPHABET_LABELS);
    if(ctx->mcf_deprel_id == -1){
      ctx->mcf_deprel_id = maca_mcf_input(format, MACA_MCF_DEPREL);
      if (ctx->mcf_deprel_id == -1) {
	maca_msg(ctx->module, MACA_ERROR);
	fprintf(stderr, "undefined error, talk to a developper about this issue\n");
	exit(1);
      }
      /* If we are in input mode, we need to check that the alphabet is the correct one */
      column = maca_mcf_get_column_info(format, ctx->mcf_deprel_id);
      maca_mcf_column_get_type(column, buffer, sizeof(buffer));
      if (strcmp(buffer, MACA_ALPHABET_LABELS) != 0) {
	maca_msg(ctx->module, MACA_ERROR);
	fprintf(stderr, "column DEPREL is not using the "MACA_ALPHABET_LABELS" alphabet\n");
	exit(1);	
      }
    }
    /* lock the deprel alphabet */
    maca_alphabet_lock(ctx->labels_alphabet);


    /* head */
    ctx->mcf_head_id = maca_mcf_output(format, MACA_MCF_HEAD, "INT");
    if(ctx->mcf_head_id == -1){
      ctx->mcf_head_id = maca_mcf_input(format, MACA_MCF_HEAD);
      if(ctx->mcf_head_id == -1){
	maca_msg(ctx->module, MACA_ERROR);
	fprintf(stderr, "undefined error, talk to a developper about this issue\n");
	exit(1);
      }
      /* check head is using integers */
      column = maca_mcf_get_column_info(format, ctx->mcf_head_id);
      maca_mcf_column_get_type(column, buffer, sizeof(buffer));
      if (strcmp(buffer, "INT") != 0) {
	maca_msg(ctx->module, MACA_ERROR);
	fprintf(stderr, "column HEAD is not using the INT type\n");
	exit(1);	
      }
    }

    maca_mcf_print_header(format, stdout);

    /* --- */
  
    for(mcf_sent = maca_graph_parser_read_mcf_sentence(ctx, format, ctx->s), sent_num = 0;
	mcf_sent && (sent_num < ctx->sent_nb); 
	mcf_sent = maca_graph_parser_read_mcf_sentence(ctx, format, ctx->s), sent_num++){
      
      maca_graph_parser_print_verbose(ctx, 2, MACA_MESSAGE, "parsing sentence");
      maca_graph_parser_decoder_parse(ctx, ctx->s);
      /* maca_graph_parser_update_sentence(ctx, ctx->s); */ /* TODO: backport modifs to maca_sentence if IO format is maca_xml */
      
      /* write parsed sentence to file_out (default to stdout) */
      //maca_graph_parser_dump_conll_sentence(ctx, ctx->s, ctx->file_out);

      maca_graph_parser_sentence_fill_mcf_output(mcf_sent, ctx, ctx->s);
      maca_mcf_sentence_print(mcf_sent, ctx->file_out);

      /* fprintf(stderr, "%d\n", sent_num);      */
      maca_mcf_sentence_release(mcf_sent);
      maca_graph_parser_sentence_clear(ctx->s);
    }
    if (mcf_sent != NULL) {
      maca_mcf_sentence_release(mcf_sent);
      mcf_sent = NULL;
    }
  }
  
  else{
    /*maca_load_data(1);
    ms = maca_common_get_iterator_sentences();
    for(ms;ms;ms=maca_next_sentence(ms)){ 
      maca_graph_parser_ProcessSentence(ms,ctx);
    }*/
  }
  
  /* maca_graph_parser_add_stamp(maca_common_get_xml_root_node());*/
  /* fermeture et libération memoire */
  /*maca_close(); */
}

void maca_graph_parser_train_main(maca_graph_parser_ctx *ctx)
{
  
  /* load training corpus and populate the alphabets */
  if(ctx->verbose_flag > 1){
    maca_msg(ctx->module, MACA_MESSAGE);
    fprintf(stderr, "loading training corpus\n");
  }
  hyp_ref_vector *corpus = load_mcf_corpus(ctx);
  fprintf(stderr, "Corpus size: %d\n", corpus->size);

  /* determine size of the populated alphabets */

  ctx->words_nb = (ctx->words_alphabet) ? maca_alphabet_size(ctx->words_alphabet) : 0;
  ctx->labels_nb = (ctx->labels_alphabet) ? maca_alphabet_size(ctx->labels_alphabet) : 0;
  ctx->pos_nb = (ctx->pos_alphabet) ? maca_alphabet_size(ctx->pos_alphabet) : 0;
  ctx->morpho_nb = (ctx->morpho_alphabet) ? maca_alphabet_size(ctx->morpho_alphabet) : 0;
  ctx->synt_feats_nb = (ctx->synt_feats_alphabet) ? maca_alphabet_size(ctx->synt_feats_alphabet) : 0;

  /* allocate dep_count table */
  ctx->dep_count_table = maca_graph_parser_dep_count_table_allocate(ctx->pos_nb, ctx->labels_nb);
  
  /* preprocessing: fill dep count table */
  int sent_id = 0;
  for(sent_id=0; sent_id < corpus->size; sent_id++){
    maca_graph_parser_dep_count_table_update(ctx, corpus->ref[sent_id]);
  }

  /* preprocessing: relabel rare dependencies */
  sent_id = 0;
  for(sent_id=0; sent_id < corpus->size; sent_id++){
    maca_graph_parser_sentence_relabel_rare_deps(ctx, corpus->ref[sent_id]);
  }
  
  /* init new model */
  ctx->model = maca_graph_parser_model_allocate(ctx);
  maca_graph_parser_model_init(ctx, ctx->model);
  /* set feature types for model */
  ctx->model->min_dep_count = ctx->min_dep_count;
  ctx->model->use_lemmas = ctx->use_lemmas;
  ctx->model->use_full_forms = ctx->use_full_forms;
  ctx->model->basic_features = ctx->basic_features;
  ctx->model->first_features = ctx->first_features;
  ctx->model->grandchildren_features = ctx->grandchildren_features;
  ctx->model->sibling_features = ctx->sibling_features;
  ctx->model->subcat_features = ctx->subcat_features;
  if(ctx->sibling_features || ctx->grandchildren_features) ctx->order = 2;  


  /* templ_library_allocator cannot be created earlier as it needs the
     complete alphabet, which is built during load_conll_corpus
     FIXME: properly (semi-)freeze the alphabet,
     cf. extractor_allocator()
  */
  ctx->e = maca_graph_parser_templ_library_allocator(ctx);

  /* allocate feature table */
  if(ctx->store_in_feature_table){
    maca_graph_parser_feature_table_allocator(ctx);
  }

  ctx->s = maca_graph_parser_allocate_sentence(ctx);

  if(ctx->print_ctx) maca_graph_parser_print_ctx(ctx);  
  
  maca_graph_parser_train(ctx, corpus);
  
  /* dump model and dep count table */
  maca_graph_parser_model_dump(ctx, ctx->model, ctx->model_file_name, ctx->produce_hash_model);
  maca_graph_parser_dep_count_table_print(ctx, ctx->dep_count_table_file_name);
  
  /* free corpus data */
  free_corpus(corpus);
}

