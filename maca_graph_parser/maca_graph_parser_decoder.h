/*******************************************************************************
    Copyright (C) 2012 by Alexis Nasr <alexis.nasr@lif.univ-mrs.fr>
                          Ghasem Mirroshandel <ghasem.mirroshandel@lif.univ-mrs.fr>
    This file is part of maca_graph_parser.

    maca_graph_parser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    maca_graph_parser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with maca_graph_parser. If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#ifndef __MACA_GRAPH_PARSER_DECODER__
#define __MACA_GRAPH_PARSER_DECODER__

#include"maca_graph_parser.h"

typedef struct open Open;
typedef struct closed Closed;

struct open{
  float score;
  int start;
  int end;
  int label;
  int dir;
  Closed *left;
  Closed *right;
};

struct closed{
  float score;
  int start;
  int end;
  int breakpoint;
  int dir;
  Closed *d;
  Open *u;
};

#define PRINT 0


#ifdef __cplusplus
extern "C"{
#endif


void create_open(Open *o, maca_graph_parser_sentence *s);
void create_closed(Closed *c, maca_graph_parser_sentence *s);
Open *alloc_open(float score, int start, int end, int label, int dir, Closed *left, Closed *right);
Closed *alloc_closed( float score, int start, int end, int breakpoint, int dir, Closed *d, Open *u);

void maca_graph_parser_decoder1_cleanup(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s);
void maca_graph_parser_decoder2_cleanup(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s);
void maca_graph_parser_decoder_cleanup(maca_graph_parser_ctx *ctx,  maca_graph_parser_sentence *s);

void maca_graph_parser_decoder1_init(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s);
void maca_graph_parser_decoder2_init(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s);
void maca_graph_parser_decoder_init(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s);

void maca_graph_parser_decoder1_parse(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s, maca_graph_parser_feature_table *feat_table);
void maca_graph_parser_decoder2_parse(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s, maca_graph_parser_feature_table *feat_table);
void maca_graph_parser_decoder_parse(maca_graph_parser_ctx *ctx,  maca_graph_parser_sentence *s);
#ifdef __cplusplus
}
#endif



#endif
