/***********************************************************************************
    Copyright (C) 2009-2012 by Jean-François Rey <jean-francois.rey@lif.univ-mrs.fr>
    This file is part of macaon.

    Macaon is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Macaon is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with macaon.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************************/

/**
 * \file maca_msg.c
 * \brief macaon log function implementation
 * \author Jean-François REY
 * \version 2.5
 * \date 02 Aug 2012
 *
 */

#include"maca_msg.h"


void maca_msg(char *module, int type)
{
  if(type == MACA_ERROR){
    fprintf(stderr, "[%s] ERROR : ", module);
  }
  else
  if(type == MACA_WARNING){
    fprintf(stderr, "[%s] WARNING : ", module);
  }
  else
  if(type == MACA_MESSAGE){
    fprintf(stderr, "[%s] MSG : ", module);
  }
}

void maca_print_msg(char * module, int type, const char * function, const char * message, ...)
{
	FILE * out;
	va_list args;

	switch(type){
	case MACA_ERROR : out = stderr;
		fprintf(stderr, "[%s] ERROR ", module);
		break;
	case MACA_WARNING : out = stderr;
		fprintf(stderr, "[%s] WARNING ", module);
		break;
	case MACA_MESSAGE : out = stderr;
		fprintf(stderr, "[%s] MSG ", module);
		break;
	default : out = stderr;
		fprintf(stderr, "[%s] DEFAULT MSG : ", module);
		break;
	}


	if(function) fprintf(out, "# %s # -> ", function);
	else fprintf(out,": ");

	va_start( args, message );
	vfprintf(out,message,args);
	va_end(args);

	fprintf(out,"\n");
}


void maca_print_verbose(char * module, int level, int type, char * function, char * message, ...)
{
//    extern int verbose;
    va_list args;
    int i;

    if(level <= maca_verbose || level == -1)
    {
	for(i=1;i<level;i++) fprintf(stderr,"    ");
	maca_msg(module,type);
        if(function != NULL) fprintf(stderr,"# %s # -> ",function);
        va_start(args,message);
        vfprintf(stderr,message,args);
        va_end(args);
        fprintf(stderr,"\n");
    }
}


void maca_print_vverbose(char * module, int level, int type,char * function, char * message, va_list * args)
{
    int i;

    for(i=1;i<level;i++) fprintf(stderr,"    ");
    maca_msg(module,type);
    if(function != NULL) fprintf(stderr,"# %s # -> ",function);
    vfprintf(stderr,message,*args);
    fprintf(stderr,"\n");
}
