/***********************************************************************************
    Copyright (C) 2009-2012 by Jean-Francois Rey <jean-francois.rey@lif.univ-mrs.fr>
    This file is part of macaon.

    Macaon is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Macaon is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with macaon.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************************/

/**
 * \file maca_constants.h
 * \brief Globals Variables declarations
 * \author Jean-François REY
 * \version 2.0
 * \date 02 Aug 2012
 *
 */

#ifndef __MACA_CONSTANTS__
#define __MACA_CONSTANTS__

extern int maca_verbose;

#define MACAON_PATH "MACAON_DIR"
#ifndef MACAON_DIRECTORY
#define MACAON_DIRECTORY "$HOME/macaon"
#endif

#define MACA_TAGS_FILE_NAME "maca_tags"
#define MACA_LEXIQUE_FILE_NAME "maca_lexique"

#define MACA_MORPHO_FILE_NAME "maca_morpho"
#define MACA_MORPHO_TRAITS_FILE_NAME "maca_morpho_traits"

#define MACA_TAGGER_EMISSION_PROB_FILE_NAME "maca_tagger_emission"
#define MACA_TAGGER_POS_NGRAM_FILE_NAME "maca_tagger_pos_ngram"

#define MACA_GUESSER_PROB_FILE_NAME "maca_tagger_guesser_prob"

#define MACA_CHUNKER_GRAMMAR_FILE_NAME "maca_chunker_grammar"

#define MACA_CRF_TAGGER_MODEL_FILE_NAME "crf_tagger_model"
#define MACA_CRF_TAGGER_LEXICON_FILE_NAME "crf_tagger_wordtag_lexicon"

#define MACA_GRAPH_PARSER_MODEL_FILE_NAME "maca_graph_parser_model1"
#define MACA_GRAPH_PARSER_DEP_COUNT_FILE_NAME "maca_graph_parser_model1"
#define MACA_GRAPH_PARSER_ALPHA_FILE_NAME "maca_graph_parser_model1"

#define MACA_GRAPH_PARSER_FIRST_MODEL_FILE_NAME "maca_graph_parser_model1"
#define MACA_GRAPH_PARSER_FIRST_DEP_COUNT_FILE_NAME "maca_graph_parser_model1"
#define MACA_GRAPH_PARSER_FIRST_ALPHA_FILE_NAME "maca_graph_parser_model1"

#define MACA_GRAPH_PARSER_SECOND_MODEL_FILE_NAME "maca_graph_parser_model2"
#define MACA_GRAPH_PARSER_SECOND_DEP_COUNT_FILE_NAME "maca_graph_parser_model2"
#define MACA_GRAPH_PARSER_SECOND_ALPHA_FILE_NAME "maca_graph_parser_model2"

#define MACA_DEFAULT_CFG "fr"

#define MACA_ENCODING "UTF-8" /**< Default file and characters encoding */

#define MACA_ERROR 1
#define MACA_WARNING 2
#define MACA_MESSAGE 3

#define DEFAULT_BUFFER_SIZE 4096

#define MACA_MAX_LENGTH_LINE 300
#define MACA_MAX_LENGTH_SENTENCE 400
#define MACA_MAX_LENGTH_WORD 100
#define MACA_MAX_LENGTH_POSTAG 50

/*#define MACA_START_SENTENCE <s>*/
#define MACA_END_SENTENCE "<EOS>"
#define MACA_TAILLE_TAB_DICO 50000
#define MACA_TYPE_PROBA float
#define MACA_LOGPROB_UNK -99
#define MACA_MINUS_INF (double) -100000
/*#define MACA_MINUS_INF - DBL_MAX*/
#define MACA_NULL_BACKP -2

#define MACA_INVALID_LEX_ID -1
#define MACA_INVALID_POS_TAG -1
#define MACA_INVALID_CHUNK_TAG -1
#define MACA_INVALID_LOGPROB 50
#define MACA_INVALID_CHUNK_TAG -1

#define MAX_LONG_LIGNE 100
#define MAX_LONG_PHRASE 2000
#define MAX_LONG_MOT 100
#define MAX_LONG_CAT 50
/*#define DEBUT_PHRASE <s>*/
#define FIN_PHRASE "<EOS>"
#define TAILLE_TAB_DICO 50000
#define TYPE_PROBA float
#define INVALID_CAT -1
#define MAX_LINE 1000
#define INVALID_LOGPROB 50
#define LOGPROB_UNK -99
#define MINUS_INF (double) -100000
/*#define MINUS_INF - DBL_MAX*/
#define NULL_BACKP -2
#define INVALID_LEFFF_CODE -1

#define MACA_NAMESPACE "maca"
#define MACA_HREF_NAMESPACE "http://macaon.lif.univ-mrs.fr/macaon/namespace/v1.0"
#define MACA_DTD_HREF "http://macaon.lif.univ-mrs.fr/uploads/macaon/documents/macaon_gen.dtd"
#define MACA_DTD_NAMESPACE_HREF "http://macaon.lif.univ-mrs.fr/uploads/macaon/documents/macaon_gen_namespace.dtd"

#define MACA_TOKENS_SECTION "prelex"	/**< Prelex section type */
#define MACA_WORDS_SECTION "lex"		/**< Lex section type */
#define MACA_POSS_SECTION "morpho"		/**< Morpho section type */
#define MACA_CHUNKS_SECTION "synt"		/**< Synt section type */

#define MACA_PRELEX_SECTION "prelex"	/**< Prelex section type */
#define MACA_LEX_SECTION "lex"			/**< Lex section type */
#define MACA_MORPHO_SECTION "morpho"	/**< Morpho section type */
#define MACA_SYNT_SECTION "synt"		/**< Synt section type */

#define MACA_TOKENS_TYPE "atome"		/**< Atome segment type */
#define MACA_WORDS_TYPE "ulex"			/**< Ulex segment type */
#define MACA_POSS_TYPE "cat"			/**< Cat segment type */
#define MACA_CHUNKS_TYPE "chunk"		/**< Chunk segment type */

#define MACA_ATOME_TYPE "atome"			/**< Atome segment type */
#define MACA_ULEX_TYPE "ulex"			/**< Ulex segment type */
#define MACA_CAT_TYPE "cat"				/**< Cat segment type */
#define MACA_CHUNK_TYPE "chunk"			/**< Chunk segment type */

#define MACAON_TAG "macaon"				/**< Macaon tag */
#define MACA_SENTENCE_TAG "sentence"	/**< Sentence tag */
#define MACA_SECTION_TAG "section"		/**< Section tag */
#define MACA_SEGS_TAG "segs"			/**< Segments tag */
#define MACA_SEG_TAG "seg"				/**< Segment tag */
#define MACA_SEQ_TAG "seq"				/**< Sequence tag */
#define MACA_ELT_TAG "elt"				/**< Element tag */
#define MACA_FSM_TAG "fsm"				/**< Fsm tag */


/* conventional names of the mcf colums*/

#define MACA_MCF_ID "ID"
#define MACA_MCF_FORM "FORM"
#define MACA_MCF_LEMMA "LEMMA"
#define MACA_MCF_CPOSTAG "CPOSTAG"
#define MACA_MCF_POSTAG "POSTAG"
#define MACA_MCF_FEATS "FEATS"
#define MACA_MCF_HEAD "HEAD"
#define MACA_MCF_DEPREL "DEPREL"
#define MACA_MCF_PHEAD "PHEAD"
#define MACA_MCF_PDEPREL "PDEPREL"
#define MACA_MCF_SUBCAT "SUBCAT"

#define MACA_ALPHABET_WORDS      "WORDS"
#define MACA_ALPHABET_POS        "POS"
#define MACA_ALPHABET_LABELS     "LABELS"
#define MACA_ALPHABET_MORPHO     "MORPHO"
#define MACA_ALPHABET_SYNT_FEATS "SYNT_FEATS"

#endif

