#include "maca_graph_parser_corpora.h"
#include <stdio.h>
#include <string.h>


hyp_ref_vector *allocate_hyp_ref_vector(int capacity){
  /**
   * Allocate a vector of hyp and ref.
   */

  int i;

  hyp_ref_vector *v = malloc(sizeof(hyp_ref_vector));
  if(v == NULL){
    fprintf(stderr, "allocate_hyp_ref_vector: memory alloc problem\n");
    exit(1);
  }

  v->ref = malloc(capacity * sizeof(maca_graph_parser_sentence *));
  if(v->ref == NULL){
    fprintf(stderr, "allocate_hyp_ref_vector: memory alloc problem for ref\n");
    exit(1);    
  }
  for(i = 0; i < capacity; i++){
    v->ref[i] = NULL;
  }

  v->hyp = malloc(capacity * sizeof(maca_graph_parser_sentence *));
  if(v->hyp == NULL){
    fprintf(stderr, "allocate_hyp_ref_vector: memory alloc problem for hyp\n");
    exit(1);    
  }
  for(i = 0; i < capacity; i++){
    v->hyp[i] = NULL;
  }

  v->capacity = capacity;
  v->size = 0;

  return v;
}


void free_hyp_ref_vector(hyp_ref_vector *v){
  /**
   * Free a vector of hyp ref.
   */
  
  int i;

  if(v == NULL)
    return;

  if(v->ref != NULL){
    for(i=0; i< v->size; i++){
      free(v->ref[i]); /* free sentence */
      v->ref[i] = NULL;
    }
    free(v->ref);
    v->ref = NULL;
  }
  
  if(v->hyp != NULL){
    for(i=0; i< v->size; i++){
      free(v->hyp[i]); /* free sentence */
      v->hyp[i] = NULL;
    }
    free(v->hyp);
    v->hyp = NULL;
  }

  v->capacity = 0;
  v->size = 0;

  free(v);
}


void hyp_ref_vector_append(hyp_ref_vector *v, maca_graph_parser_sentence *ref, maca_graph_parser_sentence *hyp){
  /**
   * Append a ref and hyp to vector.
   */

  if(v->size >= v->capacity){
    fprintf(stderr, "hyp_ref_vector full, please increase its capacity (current capacity: %d)\n", v->capacity);
    exit(1);
  }
  v->ref[v->size] = ref;
  v->hyp[v->size] = hyp;
  v->size += 1;
}


/* hyp_ref_vector *load_conll_corpus(maca_graph_parser_ctx *ctx){ */
/*   /\** */
/*    * Load a CONLL 2007 formatted corpus. */
/*    * */
/*    * sent_nb stores the number of sentences read */
/*    *\/ */

/*   int sent_nb; */
/*   hyp_ref_vector *v = allocate_hyp_ref_vector(ctx->sent_nb); */
/*   maca_graph_parser_sentence *ref_s = NULL; */
/*   maca_graph_parser_sentence *hyp_s = NULL; */

/*   sent_nb = 0; */
/*   while(sent_nb < ctx->sent_nb){ */
/*     /\* read (reference) sentence *\/ */
/*     ref_s = maca_graph_parser_allocate_sentence(ctx); */
/*     maca_graph_parser_read_conll_sentence(ctx, ctx->conll_file, ref_s); */
    
/*     if(ref_s->l == 1){ /\* should only happen at EOF *\/ */
/*       maca_graph_parser_free_sentence(ref_s); */
/*       if(ctx->verbose_flag > 4){ */
/* 	fprintf(stderr, "maca_graph_parser_corpora: breaking on empty sentence\n"); */
/*       }   */
/*       break; */
/*     }	 */

/*     /\* ignore sentences whose length exceeds the given limit *\/ */
/*     if(ref_s->l > ctx->max_sent_length){ */
/*       if(ctx->verbose_flag > 4){ */
/* 	fprintf(stderr, "maca_graph_parser_corpora: ignoring too long sentence\n"); */
/*       }   */
/*       maca_graph_parser_free_sentence(ref_s); */
/*       continue; */
/*     } */

/*     if(ctx->verbose_flag > 3){ */
/*       printf("------------------- %d -----------------------\n", sent_nb); */
/*     } */
/*     if(ctx->verbose_flag > 4){ */
/*       maca_graph_parser_sentence_print_sentence(ctx, ref_s); */
/*     } */

/*     /\* copy data to hypothesis sentence *\/ */
/*     hyp_s = maca_graph_parser_duplicate_sentence(ctx, ref_s, NULL); */
/*     /\* store in vector *\/ */
/*     hyp_ref_vector_append(v, ref_s, hyp_s); */
    
/*     /\* update dep_count_table *\/ */
/*     /\* maca_graph_parser_dep_count_table_update(ctx, ref_s); *\/ */

/*     sent_nb++; */
/*   } */

/*   if(ctx->verbose_flag > 1){ */
/*     maca_msg(ctx->module, MACA_MESSAGE); */
/*     fprintf(stderr, "%d sentences loaded\n", sent_nb); */
/*   } */

/*   return v; */
/* } */

hyp_ref_vector *load_mcf_corpus(maca_graph_parser_ctx *ctx){
  /**
   * Load an MCF formatted corpus.
   *
   */

  int sent_nb;
  hyp_ref_vector *v = allocate_hyp_ref_vector(ctx->sent_nb);
  maca_graph_parser_sentence *ref_s = NULL;
  maca_graph_parser_sentence *hyp_s = NULL;
  maca_mcf *format;
  maca_mcf_column *column;
  char buffer[128];
  maca_mcf_sentence *mcf_sent;

  format = maca_mcf_new(ctx->mcf_file_name);

  /* full form alphabet */
  if(ctx->use_full_forms){
    ctx->mcf_form_id = maca_mcf_input(format, MACA_MCF_FORM);
    if(ctx->mcf_form_id == -1){
      maca_msg(ctx->module, MACA_ERROR);
      fprintf(stderr, "column FORM not found in the train file\n");
      exit(1);
    }
    /* initialize word alphabet in ctx */
    column = maca_mcf_get_column_info(format, ctx->mcf_form_id);
    maca_mcf_column_get_type(column, buffer, sizeof(buffer));
    if (strcmp(buffer, MACA_ALPHABET_WORDS) != 0) {
      maca_msg(ctx->module, MACA_ERROR);
      fprintf(stderr, "column FORM is not using the "MACA_ALPHABET_WORDS" alphabet!\n");
      exit(1);
    }
    ctx->words_alphabet = maca_mcf_column_get_alphabet(column);

    /* add start and stop symbols for words */
    ctx->w_start = maca_alphabet_add_symbol(ctx->words_alphabet, "__START__");
    ctx->w_end = maca_alphabet_add_symbol(ctx->words_alphabet, "__END__");
  }

  /* lemmas alphabet */
  if(ctx->use_lemmas){
    ctx->mcf_lemma_id = maca_mcf_input(format, MACA_MCF_LEMMA);
    if(ctx->mcf_lemma_id == -1){
      maca_msg(ctx->module, MACA_ERROR);
      fprintf(stderr, "column LEMMA not found in the train file\n");
      exit(1);
    }
    /* initialize words alphabet in ctx */
    column = maca_mcf_get_column_info(format, ctx->mcf_lemma_id);
    maca_mcf_column_get_type(column, buffer, sizeof(buffer));
    if (strcmp(buffer, MACA_ALPHABET_WORDS) != 0) {
      maca_msg(ctx->module, MACA_ERROR);
      fprintf(stderr, "column LEMMA is not using the "MACA_ALPHABET_WORDS" alphabet!\n");
      exit(1);
    }
    if (!ctx->use_full_forms) {
      ctx->words_alphabet = maca_mcf_column_get_alphabet(column);
      /* add start and stop symbols for lemmas */
      ctx->w_start = maca_alphabet_add_symbol(ctx->words_alphabet, "__START__");
      ctx->w_end = maca_alphabet_add_symbol(ctx->words_alphabet, "__END__");
    }
  }

  /* postag alphabet */
  ctx->mcf_postag_id = maca_mcf_input(format, MACA_MCF_POSTAG);
  if(ctx->mcf_postag_id == -1){
    maca_msg(ctx->module, MACA_ERROR);
    fprintf(stderr, "column POSTAG not found in the train file\n");
    exit(1);
  }
  /* initialize postag alphabet in ctx */
  column = maca_mcf_get_column_info(format, ctx->mcf_postag_id);
  maca_mcf_column_get_type(column, buffer, sizeof(buffer));
  if (strcmp(buffer, MACA_ALPHABET_POS) != 0) {
    maca_msg(ctx->module, MACA_ERROR);
    fprintf(stderr, "column POSTAG is not using the "MACA_ALPHABET_POS" alphabet!\n");
    exit(1);
  }
  ctx->pos_alphabet = maca_mcf_column_get_alphabet(column);
  /* add start and stop symbols for part of speeches */
  ctx->pos_start = maca_alphabet_add_symbol(ctx->pos_alphabet, "__START__");
  ctx->pos_end = maca_alphabet_add_symbol(ctx->pos_alphabet, "__END__");


  /* deprel alphabet */
  ctx->mcf_deprel_id = maca_mcf_input(format, MACA_MCF_DEPREL);
  if(ctx->mcf_deprel_id == -1){
    maca_msg(ctx->module, MACA_ERROR);
    fprintf(stderr, "column DEPREL not found in the train file\n");
    exit(1);
  }
  /* initialize postag alphabet in ctx */
  column = maca_mcf_get_column_info(format, ctx->mcf_deprel_id);
  maca_mcf_column_get_type(column, buffer, sizeof(buffer));
  if (strcmp(buffer, MACA_ALPHABET_LABELS) != 0) {
    maca_msg(ctx->module, MACA_ERROR);
    fprintf(stderr, "column DEPREL is not using the "MACA_ALPHABET_LABELS" alphabet!\n");
    exit(1);
  }
  ctx->labels_alphabet = maca_mcf_column_get_alphabet(column);
  /* add joker in syntactic labels tagset */
  ctx->fct_joker = maca_alphabet_add_symbol(ctx->labels_alphabet, "__JOKER__");


  /* check that head (governor) column exists */
  ctx->mcf_head_id = maca_mcf_input(format, MACA_MCF_HEAD);
  if(ctx->mcf_head_id == -1){
    maca_msg(ctx->module, MACA_ERROR);
    fprintf(stderr, "column HEAD not found in the train file\n");
    exit(1);
  }
  maca_mcf_column_get_type(maca_mcf_get_column_info(format, ctx->mcf_head_id), buffer, sizeof(buffer));
  if(strcmp(buffer, "INT")){
    maca_msg(ctx->module, MACA_ERROR);
    fprintf(stderr, "column HEAD should be of type INT\n");
    exit(1);
  }
    
  /* subcat alphabet */
  if(ctx->subcat_features){
    ctx->mcf_subcat_id = maca_mcf_input(format, MACA_MCF_SUBCAT);
    if(ctx->mcf_subcat_id == -1){
      maca_msg(ctx->module, MACA_ERROR);
      fprintf(stderr, "column SUBCAT not found in the train file\n");
      exit(1);
    }
    /* initialize word alphabet in ctx */
    column = maca_mcf_get_column_info(format, ctx->mcf_subcat_id);
    maca_mcf_column_get_type(column, buffer, sizeof(buffer));
    if (strcmp(buffer, MACA_ALPHABET_SYNT_FEATS) != 0) {
      maca_msg(ctx->module, MACA_ERROR);
      fprintf(stderr, "column SUBCAT is not using the "MACA_ALPHABET_SYNT_FEATS" alphabet!\n");
      exit(1);
    }
    ctx->synt_feats_alphabet = maca_mcf_column_get_alphabet(column);
  }
  
  sent_nb = 0;
  while(sent_nb < ctx->sent_nb){
    /* read (reference) sentence */
    ref_s = maca_graph_parser_allocate_sentence(ctx);
    mcf_sent = maca_graph_parser_read_mcf_sentence(ctx, format, ref_s);
    maca_mcf_sentence_release(mcf_sent);
    
    if(ref_s->l == 1){ /* should only happen at EOF */
      maca_graph_parser_free_sentence(ref_s);
      if(ctx->verbose_flag > 4){
	fprintf(stderr, "maca_graph_parser_corpora: breaking on empty sentence\n");
      }  
      break;
    }	

    if(ctx->verbose_flag > 3){
      printf("------------------- %d -----------------------\n", sent_nb);
    }
    if(ctx->verbose_flag > 4){
      maca_graph_parser_sentence_print_sentence(ctx, ref_s);
    }

    /* copy data to hypothesis sentence */
    hyp_s = maca_graph_parser_duplicate_sentence(ctx, ref_s, NULL);
    /* store in vector */
    hyp_ref_vector_append(v, ref_s, hyp_s);
    
    /* update dep_count_table */
    /* maca_graph_parser_dep_count_table_update(ctx, ref_s); */

    sent_nb++;
  }

  if(ctx->verbose_flag > 1){
    maca_msg(ctx->module, MACA_MESSAGE);
    fprintf(stderr, "%d sentences loaded\n", sent_nb);
  }
  if (!maca_mcf_dump_alphabets(format, ctx->alphabet_file_name)) {
    maca_msg(ctx->module, MACA_ERROR);
    fprintf(stderr, "couldn't open the alphabet file!\n");
    exit(1);
  }
  maca_mcf_delete(format);
  return v;
}


void free_corpus(hyp_ref_vector *corpus){
  /**
   * Free a corpus.
   */
  
  free_hyp_ref_vector(corpus);
}


/* void dump_conll_corpus(maca_graph_parser_ctx *ctx, hyp_ref_vector *corpus, FILE *f){ */
/*   /\** */
/*    * Dump a CONLL 2007 formatted corpus to file f. */
/*    * */
/*    * The syntactic information dumped is the hypothesis parse tree. */
/*    * */
/*    *\/ */

/*   int sent_id = 0; */
/*   maca_graph_parser_sentence *s = NULL; */

/*   if(corpus == NULL) */
/*     return; */

/*   for(sent_id=0; sent_id < corpus->size; sent_id++){  */
/*     s = corpus->hyp[sent_id]; */
/*     maca_graph_parser_dump_conll_sentence(ctx, s, f); */
/*   } */
/* } */


void maca_graph_parser_sentence_relabel_rare_deps(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s){
  /**
   * Preprocess s to relabel rare dependencies as "__JOKER__".
   *
   * Updates ctx->dep_count_table accordingly.
   */

  int i;
  int dir, pos_gov, pos_dep, label, length_class;
  int joker_label = ctx->fct_joker;

  for(i=1; i < s->l; i++){
    dir = (i > s->gov[i])? ra : la;
    pos_gov = s->pos[s->gov[i]];
    pos_dep = s->pos[i];
    label = s->label[i];
    length_class =  maca_graph_parser_dep_count_table_compute_length_class(s->gov[i], i);

    if((pos_gov >= 0 && pos_dep >= 0 && label >= 0) &&
       (ctx->dep_count_table[pos_gov][pos_dep][label][length_class][dir] < ctx->min_dep_count)){
      /* relabel dep */
      s->label[i] = joker_label;
      /* update dep_count_table */
      ctx->dep_count_table[pos_gov][pos_dep][s->label[i]][length_class][dir]++;
      /* TODO: remove hapax from dep_count_table ? */
    }
  }
}
