/*******************************************************************************
    Copyright (C) 2012 by Alexis Nasr <alexis.nasr@lif.univ-mrs.fr>
                          Ghasem Mirroshandel <ghasem.mirroshandel@lif.univ-mrs.fr>
    This file is part of maca_graph_parser.

    maca_graph_parser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    maca_graph_parser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with maca_graph_parser. If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#include<stdlib.h>
#include<stdio.h>
#include<time.h>
#include <string.h>
#include"maca_constants.h"
#include"maca_msg.h"
#include"maca_graph_parser.h"
#include"maca_graph_parser_alphabet.h"

void maca_graph_parser_alphabet_free(maca_graph_parser_alphabet *a)
{
  array_free(a->array);
  hash_free(a->htable);
  free(a->name);
  free(a);
}

maca_graph_parser_alphabet *maca_graph_parser_alphabet_new(char *name)
{
  maca_graph_parser_alphabet *a = (maca_graph_parser_alphabet *)malloc(sizeof(maca_graph_parser_alphabet));
  if(a == NULL){
    fprintf(stderr, "memory allocation error\n");
    exit(1);
  }

  a->name = strdup(name);
  
  a->nb = 0;
  a->htable = hash_new(16);
  a->array = array_new();
  return a;
}


int maca_graph_parser_alphabet_add_symbol(maca_graph_parser_alphabet *a, char *symbol)
{
  int code;
  char *symbol_copy;
  cell* found;

  /* fprintf(stderr, " maca_graph_parser_alphabet_add_symbol(%s)\n", symbol); */

  found = hash_lookup(a->htable, symbol);
  if(found == NULL){
      code = a->nb;
      symbol_copy = strdup(symbol);
      hash_add(a->htable, symbol_copy, a->nb);
      array_push(a->array, symbol_copy);
      a->nb++;
  } else {
      code = found->val;
  }
  return code;
}

int maca_graph_parser_alphabet_get_code(maca_graph_parser_alphabet *a, char *symbol)
{
  cell *code;

  code = hash_lookup(a->htable, symbol);
  if(code == NULL)
    return MACA_GRAPH_PARSER_ALPHABET_INVALID_CODE;
  return code->val;
}


char * maca_graph_parser_alphabet_get_symbol(maca_graph_parser_alphabet *a, int code)
{
  return array_get(a->array, code);
}


void maca_graph_parser_alphabet_print4(char *filename, maca_graph_parser_alphabet *a1, maca_graph_parser_alphabet *a2, maca_graph_parser_alphabet *a3, maca_graph_parser_alphabet *a4)
{
  FILE *f;
  
  if(filename == NULL)
    f = stdout;
  else{
    f = fopen(filename, "w");
    if(f == NULL){
      fprintf(stderr, "cannot open file %s\n", filename);
      exit(1);
    }
  }

  if(a1) maca_graph_parser_alphabet_print(f, a1);
  if(a2) maca_graph_parser_alphabet_print(f, a2);
  if(a3) maca_graph_parser_alphabet_print(f, a3);
  if(a4) maca_graph_parser_alphabet_print(f, a4);

  if(filename)
    fclose(f);
}
void maca_graph_parser_alphabet_print5(char *filename, maca_graph_parser_alphabet *a1, maca_graph_parser_alphabet *a2, maca_graph_parser_alphabet *a3, maca_graph_parser_alphabet *a4, maca_graph_parser_alphabet *a5)
{
  FILE *f;
  
  if(filename == NULL)
    f = stdout;
  else{
    f = fopen(filename, "w");
    if(f == NULL){
      fprintf(stderr, "cannot open file %s\n", filename);
      exit(1);
    }
  }

  if(a1) maca_graph_parser_alphabet_print(f, a1);
  if(a2) maca_graph_parser_alphabet_print(f, a2);
  if(a3) maca_graph_parser_alphabet_print(f, a3);
  if(a4) maca_graph_parser_alphabet_print(f, a4);
  if(a5) maca_graph_parser_alphabet_print(f, a5);

  if(filename)
    fclose(f);
}

void maca_graph_parser_alphabet_print(FILE *f, maca_graph_parser_alphabet *a)
{
  int i;
  char *symbol;
  
  fprintf(f, "##%s\n", a->name);
  for(i=0; i < a->nb; i++){
    symbol = array_get(a->array,i);
    fprintf(f,"%s\n", symbol);
  }
}

maca_graph_parser_alphabet **maca_graph_parser_alphabet_load4(char *filename)
{
  FILE *f;
  int i = 0;
  char symbol[1000];
  maca_graph_parser_alphabet *a = NULL;
  maca_graph_parser_alphabet **alpha_array = (maca_graph_parser_alphabet **)malloc(4 * sizeof(maca_graph_parser_alphabet*));

  for(i=0; i < 4; i++)
    alpha_array[i] = NULL;

  f = fopen(filename, "rb");
  if(f == NULL){
    fprintf(stderr, "cannot open file %s\n", filename);
    exit(1);
  }
  i = 0;
  while(fscanf(f, "%s", symbol) != EOF){
    if((symbol[0] == '#') && (symbol[1] == '#'))
      alpha_array[i++] = a = maca_graph_parser_alphabet_new(symbol+2);
    else{
      if(a)
	maca_graph_parser_alphabet_add_symbol(a, symbol);
    }
  }
  fclose(f);
  return alpha_array;
}

maca_graph_parser_alphabet **maca_graph_parser_alphabet_load5(char *filename)
{
  FILE *f;
  int i = 0;
  char symbol[1000];
  maca_graph_parser_alphabet *a = NULL;
  maca_graph_parser_alphabet **alpha_array = (maca_graph_parser_alphabet **)malloc(5 * sizeof(maca_graph_parser_alphabet*));

  for(i=0; i < 5; i++)
    alpha_array[i] = NULL;

  f = fopen(filename, "rb");
  if(f == NULL){
    fprintf(stderr, "cannot open file %s\n", filename);
    exit(1);
  }
  i = 0;
  while(fscanf(f, "%s", symbol) != EOF){
    /*    printf("%s\n", symbol);*/
    if((symbol[0] == '#') && (symbol[1] == '#'))
      alpha_array[i++] = a = maca_graph_parser_alphabet_new(symbol+2);
    else{
      if(a)
	maca_graph_parser_alphabet_add_symbol(a, symbol);
    }
  }
  fclose(f);
  return alpha_array;
}

/*

    else{
    maca_msg("maca_graph_parser_alphabet", MACA_ERROR);
    fprintf(stderr, "error while loading alphabet file : %s\n", filename);
    fprintf(stderr, "alphabet files must begin with ##ALPHABET_NAME\n");
    exit(0);
  }
*/


maca_graph_parser_alphabet *maca_graph_parser_alphabet_load(char *filename)
{
  FILE *f;
  char symbol[1000];
  maca_graph_parser_alphabet *a = NULL;

  f = fopen(filename, "rb");
  if(f == NULL){
    fprintf(stderr, "cannot open file %s\n", filename);
    exit(1);
  }
  
  fscanf(f, "%s", symbol);
  if((symbol[0] == '#') && (symbol[1] == '#'))
    a = maca_graph_parser_alphabet_new(symbol+2);
  else{
    maca_msg("maca_graph_parser_alphabet", MACA_ERROR);
    fprintf(stderr, "error while loading alphabet file : %s\n", filename);
    fprintf(stderr, "alphabet files must begin with ##ALPHABET_NAME\n");
    exit(0);
  }

  while(fscanf(f, "%s", symbol) != EOF){
    /* printf("symbol = %s\n", symbol);  */
    maca_graph_parser_alphabet_add_symbol(a, symbol);
  }

  fclose(f);
  return a;
}

int maca_graph_parser_alphabet_size(maca_graph_parser_alphabet *a)
{
  return a->nb;
}
