/*******************************************************************************
    Copyright (C) 2012 by Alexis Nasr <alexis.nasr@lif.univ-mrs.fr>
                          Ghasem Mirroshandel <ghasem.mirroshandel@lif.univ-mrs.fr>
    This file is part of maca_graph_parser.

    maca_tagger is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    maca_tagger is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with maca_tagger. If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#ifndef __MACA_GRAPH_PARSER_FEATURES__
#define __MACA_GRAPH_PARSER_FEATURES__

#include"maca_graph_parser.h"
#include"maca_graph_parser_sentence.h"
#include "maca_graph_parser_feature_vector.h"

#ifdef __cplusplus
extern "C"{
#endif


feat_vector *first(maca_graph_parser_sentence *s, maca_graph_parser_ctx *ctx, int gov, int dep, int label, feat_vector *fv);
feat_vector *basic(maca_graph_parser_sentence *s, maca_graph_parser_ctx *ctx, int gov, int dep, feat_vector *fv);
feat_vector *grandchildren(maca_graph_parser_sentence *s, maca_graph_parser_ctx *ctx, int gov, int dep, int gdep, int label, feat_vector *fv);
feat_vector *sibling(maca_graph_parser_sentence *s, maca_graph_parser_ctx *ctx, int gov, int dep, int sblng, int label, feat_vector *fv);
feat_vector *subcat(maca_graph_parser_sentence *s, maca_graph_parser_ctx *ctx, int gov, int dep, int label, feat_vector *fv);

void maca_graph_parser_print_feature_bin(FILE *f, feature_t feat);
void maca_graph_parser_print_feature(FILE *f, maca_graph_parser_ctx *ctx, feature_t feat);

int maca_graph_parser_get_feature_type(feature_t feat, maca_graph_parser_templ_library *tl);
templ *maca_graph_parser_get_templ(feature_t feat, maca_graph_parser_templ_library *tl);
int maca_graph_parser_get_feature_label(feature_t feat, maca_graph_parser_templ_library *tl);
int maca_graph_parser_get_feature_direction(feature_t feat, maca_graph_parser_templ_library *tl);
feature_t maca_graph_parser_get_feature_hash_key(feature_t feat, maca_graph_parser_templ_library *tl);
void maca_graph_parser_decompose_feature(feature_t feat, int *direction, int *label, feature_t *hash_key, maca_graph_parser_templ_library *tl);

maca_graph_parser_templ_library *maca_graph_parser_templ_library_allocator(maca_graph_parser_ctx *ctx);
void maca_graph_parser_templ_library_free(maca_graph_parser_templ_library *tl);
#ifdef __cplusplus
}
#endif


#endif


