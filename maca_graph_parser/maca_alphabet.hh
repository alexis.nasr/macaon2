/*******************************************************************************
    Copyright (C) 2012 by Alexis Nasr <alexis.nasr@lif.univ-mrs.fr>
                          Jeremy Auguste <jeremy.auguste@etu.univ-amu.fr>
    This file is part of maca_common.

    maca_tagger is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    maca_tagger is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with maca_common. If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#ifndef __MACA_ALPHABET_H__
#define __MACA_ALPHABET_H__

#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <set>
#include <vector>
#include <stdexcept>
#include <memory>

#define MACA_ALPHABET_INVALID_CODE -1

namespace macaon {
  class Alphabet {
  private:
    std::string name;		/**< name of the alphabet */
    int nb;			/**< number of symbols in the alphabet */
    std::map<std::string, int> wordCodeMap; /**< map where keys are symbols and values their code */
    std::vector<std::string> codeWordArray; /**< vector where indexes are the symbols codes and values are the symbols */
    bool locked;		/**< informs if the alphabet is locked or not */
    bool loaded;		/**< informs if the alphabet has been loaded from a file */
    
  public:
    /** 
     * Constructor for Alphabet.
     * 
     * @param name   the name of the alphabet.
     * @param loaded true if alphabet is loaded from a file, false otherwise.
     */
    Alphabet(const std::string name, bool loaded=false);
    
    /** 
     * Gets the name of the alphabet.
     * 
     * 
     * @return the name of the alphabet.
     */
    std::string getName();
    
    /** 
     * Adds a symbol to the alphabet.
     * You can only add symbols if the alphabet is unlocked.
     * 
     * @param symbol the symbol to add to the alphabet.
     * 
     * @return a integer representation of the symbol.
     *
     * @throws runtime_error if the alphabet is locked.
     */
    int addSymbol(const std::string symbol);

    /** 
     * Gets the integer representation of a symbol.
     * 
     * @param symbol the symbol we are looking for.
     * 
     * @return the integer representation of the symbol.
     *
     * @throws runtime_error if the symbol does not exist.
     */
    int getCode(const std::string &symbol);

    /** 
     * Gets a symbol from its integer representation.
     * 
     * @param code the integer representation of a symbol.
     * 
     * @return the symbol.
     *
     * @throws out_of_range if the code is invalid.
     */
    std::string &getSymbol(const int &code);
    
    /** 
     * Gets the number of symbols of the alphabet.
     * 
     * 
     * @return the number of symbols.
     */
    int size();

    /** 
     * Locks the alphabet.
     * This means that you can't add any additional symbols to the alphabet.
     */
    void lock();

    /** 
     * Unlocks the alphabet.
     * 
     */
    void unlock();

    /** 
     * Returns if the alphabet has been loaded from a file.
     * 
     * 
     * @return true if loaded from a file, false otherwise.
     */
    bool isLoaded();

    /** 
     * Dumps the alphabet into an output stream.
     * 
     * @param out the output stream.
     */
    void dump(std::ostream &out);

    // Overloading ==
    bool operator ==(const Alphabet &a);
    
    // Overloading !=
    bool operator !=(const Alphabet &a);

    /** 
     * Same as getCode().
     * 
     * @return an integer representation. 
     */
    int operator [](const std::string &s);

    /** 
     * Same as getSymbol()
     * 
     * @return a symbol.
     */
    std::string &operator [](const int &i); 
  };
  
  class AlphabetArray {
  private:
    std::map<std::string, std::shared_ptr<Alphabet>> alphabets; /**< the stored alphabets */
    int nb;			/**< the number of alphabets */

  public:
    /** 
     * Constructor for AlphabetArray.
     * 
     */
    AlphabetArray();

    /** 
     * Constructor for AlphabetArray.
     * Also loads alphabets from the specified file.
     * 
     * @param filename the name of the file which contains alphabets.
     *
     * @throws runtime_error if we couldn't open the file.
     */
    AlphabetArray(std::string filename);

    /** 
     * Adds an alphabet to the array.
     * 
     * @param alphabet the alphabet to add.
     *
     * @return true if successful, false if an alphabet with the same name is already in the array.
     */
    bool addAlphabet(std::shared_ptr<Alphabet> alphabet);

    /** 
     * Removes an alphabet.
     * 
     * @param name the name of the alphabet to remove.
     * 
     * @return the removed alphabet, or nullptr if it doesn't exist.
     */
    std::shared_ptr<Alphabet> removeAlphabet(const std::string name);

    /** 
     * Gets an alphabet.
     * 
     * @param name the name of the alphabet to get.
     * 
     * @return the wanted alphabet, or nullptr if it doesn't exist.
     */
    std::shared_ptr<Alphabet> getAlphabet(const std::string name);
    
    /** 
     * Checks if an alphabet has the given name.
     * 
     * @param name the name of the potential alphabet.
     * 
     * @return true if an alphabet was found, false otherwise.
     */
    bool has(const std::string name);

    /** 
     * Get the number of alphabets.
     * 
     * 
     * @return the number of alphabets.
     */
    int size();
    
    /** 
     * Loads alphabets from a file and stores them in the array.
     * 
     * @param filename the name of the file which contains the alphabets.
     *
     * @throws runtime_error if we couldn't open the file.
     */
    void load(std::string filename);

    /** 
     * Dumps the alphabets in the output stream.
     * 
     * @param out the output stream.
     */
    void dump(std::ostream &out);

    /** 
     * Dumps the alphabets in the given filename.
     * 
     * @param filename the output filename.
     *
     * @throws runtime_error if we couldn't open the file.
     */
    void dump(std::string filename="");


    /** 
     * Same as getAlphabet()
     *
     * @return an alphabet. 
     */
    std::shared_ptr<Alphabet> operator [](const std::string &name);
  };
  
  template<class T>
  class PtrPool {
  public:
    static PtrPool<T>& getInstance() {
      static PtrPool<T> instance;
      return instance;
    }

    T *accept(std::shared_ptr<T> ptr) {
      objects[ptr.get()] = ptr;
      return ptr.get();
    }

    void release(T *a) {
      objects.erase(a);
    }

    std::shared_ptr<T> get(T *a) {
      auto it = objects.find(a);
      if (it == objects.end())
	return nullptr;
      return it->second;
    }

  private:
    std::map<T*,std::shared_ptr<T>> objects;

    PtrPool<T>() {}
    PtrPool<T>(PtrPool<T> const&) = delete;
    void operator=(PtrPool<T> const&) = delete;
  };
}

#endif /* __MACA_ALPHABET_H__ */
