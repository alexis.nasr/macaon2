/*******************************************************************************
    Copyright (C) 2012 by Alexis Nasr <alexis.nasr@lif.univ-mrs.fr>
                          Ghasem Mirroshandel <ghasem.mirroshandel@lif.univ-mrs.fr>
                          Frederic Bechet <frederic.bechet@lif.univ-mrs.fr>
    This file is part of maca_graph_parser.

    maca_tagger is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    maca_tagger is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with maca_tagger. If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/


#ifndef __MACA_GRAPH_PARSER_HASH__
#define __MACA_GRAPH_PARSER_HASH__

#include "maca_graph_parser.h"

#define VIDE	0


#ifdef __cplusplus
extern "C"{
#endif


/* hash table */
int hash_func(feature_t clef, int taille);
/* high level functions */
int recherche_hash_index(maca_graph_parser_hash *t, feature_t clef);
float *recherche_hash(maca_graph_parser_hash *t, feature_t clef);
int range_hash(maca_graph_parser_hash *t, feature_t clef, float valeur, float valeur2);


/* persistency */
maca_graph_parser_hash *load_table(FILE *f);
void dump_table(maca_graph_parser_hash *t, FILE *f);
/* init and free */
maca_graph_parser_hash *creation_table(int nbelem, float coeff);
void free_table(maca_graph_parser_hash *t);
/* feature_weight_table */
maca_graph_parser_feature_weight_table *load_feature_weight_table(FILE *f);
void dump_feature_weight_table(maca_graph_parser_feature_weight_table *t, FILE *f);
void feature_weight_table_free(maca_graph_parser_feature_weight_table *t);
float *recherche_dicho(void *t, feature_t f);
/* conversion */
maca_graph_parser_feature_weight_table *feat_hash2feat_array(maca_graph_parser_hash *t);
maca_graph_parser_hash *feat_array2feat_hash(maca_graph_parser_feature_weight_table *feat_array, float hash_fill_rate);

/* dead */
maca_graph_parser_feature_weight_table *maca_graph_parser_hash_compress(maca_graph_parser_hash *t);
#ifdef __cplusplus
}
#endif


#endif
