#include "maca_graph_parser_sentence.h"


#define CONLL_UNK "_"

#define MAX_STR 100
#define MAX_LINE 1000

#ifdef __cplusplus
extern "C"{
#endif


maca_graph_parser_sentence *maca_graph_parser_read_conll_sentence(maca_graph_parser_ctx *ctx, FILE *f, maca_graph_parser_sentence *s);
void maca_graph_parser_dump_conll_sentence(maca_graph_parser_ctx *ctx, maca_graph_parser_sentence *s, FILE *f);

#ifdef __cplusplus
}
#endif

  
/* void maca_graph_parser_load_conll_sentence(maca_graph_parser_ctx * ctx, sentence *conll_s, maca_graph_parser_sentence *maca_s); */
