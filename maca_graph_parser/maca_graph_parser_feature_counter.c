#include "maca_graph_parser_feature_counter.h"

#define hash_func(key,size) (int)((key) % (feature_t)(size))


feature_counter *feature_counter_new(int nb_elts, float coeff){
  /**
   * Create a feature counter of size (nb_elts / coeff).
   */
  
  feature_counter *c = malloc(sizeof(feature_counter));

  c->size = (int)((double) nb_elts / coeff);
  c->nb_elts = 0;
  c->keys = malloc(sizeof(feature_t) * (size_t) c->size);
  c->values = malloc(sizeof(int) * (size_t) c->size);
  
  if((c->keys == NULL) || (c->values == NULL)){
    fprintf(stderr, "feature_counter_new: mem alloc error\n");
    exit(1);
  }

  int i;
  for(i = 0; i < c->size; i++){
    c->keys[i] = 0;
    c->values[i] = 0;
  }
  return c;
}


void feature_counter_destroy(feature_counter *c){
  /**
   * Destroy feature counter.
   */
  if(c == NULL)
    return;

  free(c->keys);
  c->keys = NULL;
  free(c->values);
  c->values = NULL;
  free(c);
}


int feature_counter_get_index(feature_counter *c, feature_t f){
  /**
   * (private function) get the index where a feature should be.
   */
  
  int index = hash_func(f, c->size);
  /* open addressing, linear probing */
  int i;
  for(i=0; i < c->size; i++){
    /* new key */
    if(c->keys[index] == 0) return index;
    /* existing key */
    if(c->keys[index] == f) return index;
    /* collision */
    index = (index + 1) % (c->size);
  }
  /* no index found iff the feature_counter is full */
  /* TODO: dynamically extend the table */
  fprintf(stderr, "Feature counter full (size = %d)\n", c->size);
  exit(1);
}


void feature_counter_update_vector(feature_counter *c, feat_vector *v){
  /**
   * Add counts from elements in v.
   */
  
  if(v == NULL)
    return;

  int i;
  for(i = 0; i < v->elt_nb; i++){
    feature_t f = v->array[i];
    int index = feature_counter_get_index(c, f);
    /* insert new key */
    if(c->keys[index] == 0){
      c->keys[index] = f;
      c->nb_elts += 1;
    }
    /* increment value for new or existing key */
    if(c->keys[index] == f){
      c->values[index] += 1;
    }
  }
}


void feature_counter_subtract_vector(feature_counter *c, feat_vector *v){
  /**
   * Subtract counts from elements in v.
   */
  
  if(v == NULL)
    return;

  int i;
  for(i = 0; i < v->elt_nb; i++){
    feature_t f = v->array[i];
    int index = feature_counter_get_index(c, f);
    /* insert new key */
    if(c->keys[index] == 0){
      c->keys[index] = f;
      c->nb_elts += 1;
    }
    /* decrement value for new or existing key */
    if(c->keys[index] == f){
      c->values[index] -= 1;
    }
  }
}


void feature_counter_update(feature_counter *c, feature_counter *other){
  /**
   * Update the feature counter with feature counts from other.
   */

  if((c == NULL) || (other == NULL))
    return;
  
  int i;
  for(i=0; i < other->size; i++){
    if(other->keys[i] != 0){
      /* get feature count */
      feature_t f = other->keys[i];
      int count = other->values[i];
      /* get index in c */
      int index = feature_counter_get_index(c, f);
      /* add feature to c if necessary */
      if(c->keys[index] == 0){
	c->keys[index] = f;
	c->nb_elts += 1;
      }
      /* add count from other */
      c->values[index] += count;
    }
  }
}


void feature_counter_subtract(feature_counter *c, feature_counter *other){
  /**
   * Subtract from the feature counter feature counts from other.
   */

  if((c == NULL) || (other == NULL))
    return;
  
  int i;
  for(i=0; i < other->size; i++){
    if(other->keys[i] != 0){
      /* get feature count */
      feature_t f = other->keys[i];
      int count = other->values[i];
      /* get index in c */
      int index = feature_counter_get_index(c, f);
      /* add feature to c if necessary */
      if(c->keys[index] == 0){
	c->keys[index] = f;
	c->nb_elts += 1;
      }
      /* add count from other */
      c->values[index] -= count;
    }
  }
}


int feature_counter_squared_norm(feature_counter *c){
  /**
   * Compute the squared norm of c.
   */

  int result = 0;

  int i;
  for(i=0; i < c->size; i++){
    int v = c->values[i];
    result += (v * v);
  }

  return result;
}


feature_count_vector *feature_counter_items(feature_counter *c){
  /**
   * Get a vector of feature counts.
   */

  feature_count_vector *v = feature_count_vector_allocate(c->nb_elts);

  int i;
  for(i = 0; i < c->size; i++){
    if(c->keys[i] != 0){
      /* append to vector */
      v->counts[v->nb_elts].key = c->keys[i];
      v->counts[v->nb_elts].value = c->values[i];
      v->nb_elts += 1;
    }
  }

  return v;
}


/* feature count vector */
feature_count_vector *feature_count_vector_allocate(int size){
  feature_count_vector *v = malloc(sizeof(feature_count_vector));
  v->size = size;
  v->nb_elts = 0;
  v->counts = malloc(size * sizeof(feature_count));
  return v;
}


void feature_count_vector_free(feature_count_vector *v){
  if(v == NULL)
    return;
  
  free(v->counts);
  v->counts = NULL;
  free(v);
}
/* end feature count vector */
