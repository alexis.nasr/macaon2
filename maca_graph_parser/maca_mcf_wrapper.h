/*******************************************************************************
    Copyright (C) 2012 by Alexis Nasr <alexis.nasr@lif.univ-mrs.fr>
                          Ghasem Mirroshandel <ghasem.mirroshandel@lif.univ-mrs.fr>
                          Jeremy Auguste <jeremy.auguste@etu.univ-amu.fr>
    This file is part of maca_graph_parser.

    maca_tagger is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    maca_tagger is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with maca_tagger. If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#ifndef __MACA_MCF_WRAPPER_H__
#define __MACA_MCF_WRAPPER_H__

#include "maca_alphabet_wrapper.h"

#ifdef __cplusplus
extern "C" {
  namespace macaon {};
  using namespace macaon;
#else
#include <stdio.h>
#endif

  typedef struct McfWord maca_mcf_word;
  
  /**
   * Get the id of a word in a sentence.
   *
   * @param word  a word of a sentence.
   *
   * @return the id of the word.
   */
  int maca_mcf_word_get_id(maca_mcf_word *word);

  /**
   * Adds an integer value to the specified column.
   *
   * @param word   the word we are working on.
   * @param column the column in which we must insert our value.
   * @param value  an integer we want to insert.
   */
  void maca_mcf_word_add_value_int(maca_mcf_word *word, int column, int value);

  /**
   * Adds a float value to the specified column.
   *
   * @param word   the word we are working on.
   * @param column the column in which we must insert our value.
   * @param value  a float we want to insert.
   */
  void maca_mcf_word_add_value_float(maca_mcf_word *word, int column, float value);

  /**
   * Adds a string value to the specified column.
   *
   * @param word   the word we are working on.
   * @param column the column in which we must insert our value.
   * @param value  a string we want to insert.
   */
  void maca_mcf_word_add_value_string(maca_mcf_word *word, int column, char* value);

  /**
   * Adds a boolean value to the specified column.
   *
   * @param word   the word we are working on.
   * @param column the column in which we must insert our value.
   * @param value  a boolean we want to insert.
   */
  void maca_mcf_word_add_value_bool(maca_mcf_word *word, int column, char value);

  /**
   * Adds a symbol, represented by its integer value from the alphabet, 
   * to the specified column.
   *
   * @param word     the word we are working on.
   * @param column   the column in which we must insert our value.
   * @param symbol   a symbol we want to insert.
   * @param alphabet the alphabet of the symbol.
   *
   * @return 1 if successful, 0 if an error occured (alphabet locked and unknown symbol, etc.)
   */
  char maca_mcf_word_add_value_alphabet(maca_mcf_word *word, int column, char *symbol,
					maca_alphabet *alphabet);

  /**
   * Gets the number of values in the specified column.
   *
   * @param word   the word we are working on.
   * @param column the column we are checking.
   *
   * @return the number of values in the column.
   *
   * @note it's recommended to call this function before fetching the values.
   */
  int maca_mcf_word_get_nb_values(maca_mcf_word *word, int column);

  /**
   * Gets an integer value from the specified column and index.
   *
   * @param word   the word we want to get a walue from.
   * @param column the column we want to fetch from.
   * @param index  the index of the value in the vector of values.
   *
   * @return an integer value.
   *
   * @note if value was inserted using _add_value_alphabet, use this function to fetch.
   */
  int maca_mcf_word_get_value_int(maca_mcf_word *word, int column, int index);

  /**
   * Gets a float value from the specified column and index.
   *
   * @param word   the word we want to get a value from.
   * @param column the column we want to fetch from.
   * @param index  the index of the value in the vector of values.
   *
   * @return a float value.
   */
  float maca_mcf_word_get_value_float(maca_mcf_word *word, int column, int index);

  /**
   * Gets an integer value from the specified column and index.
   *
   * @param word   the word we want to get a value from.
   * @param column the column we want to fetch from.
   * @param index  the index of the value in the vector of values.
   * @param str    the buffer string to insert our value in.
   * @param size   the size of the buffer.
   */
  void maca_mcf_word_get_value_string(maca_mcf_word *word, int column, int index,
				      char *str, int size);

  /**
   * Gets a boolean value from the specified column and index.
   *
   * @param word   the word we want to get a value from.
   * @param column the column we want to fetch from.
   * @param index  the index of the value in the vector of values.
   *
   * @return a boolean value.
   */
  char maca_mcf_word_get_value_bool(maca_mcf_word *word, int column, int index);

  /** 
   * Clears all the values in the specified column of the specified word.
   * 
   * @param word the word we are clearing values from.
   * @param column the column to clear.
   */
  void maca_mcf_word_clear_values(maca_mcf_word *word, int column);
  /**
   * Prints the word (i.e its columns) on the desired output.
   *
   * @param word the word we want to print.
   * @param out  the desired output.
   */
  void maca_mcf_word_print(maca_mcf_word *word, FILE *out);

  typedef struct McfSentence maca_mcf_sentence;

  /**
   * Creates a new maca_mcf_sentence.
   *
   * @param nb_columns  the number of columns the sentence must have.
   *
   * @return a pointer to the new maca_mcf_sentence.
   *
   * @see maca_mcf_sentence_delete() 
   */
  maca_mcf_sentence *maca_mcf_sentence_new(int nb_columns);

  /**
   * Frees a maca_mcf_sentence.
   *
   * @param sentence  the sentence we are freeing.
   *
   * @see maca_mcf_sentence_new()
   */
  void maca_mcf_sentence_delete(maca_mcf_sentence *sentence);

  /**
   * Releases a previously obtained maca_mcf_sentence.
   *
   * @param sentence the sentence we are releasing.
   *
   * @see maca_mcf_read_next_sentence()
   * @see maca_mcf_sentences_get_sentence()
   */
  void maca_mcf_sentence_release(maca_mcf_sentence *sentence);

  /**
   * Creates a new word in the sentence.
   *
   * @param sentence  the sentence in which we are adding a new word.
   *
   * @return a pointer to the added word, do not try to free this!
   */
  maca_mcf_word *maca_mcf_sentence_add_word(maca_mcf_sentence *sentence);

  /**
   * Gets a word from a sentence at the specified index.
   *
   * @param sentence the sentence we are fetching from.
   * @param index    the index of the word.
   *
   * @return a pointer to the wanted word, do not try to free this!
   */
  maca_mcf_word *maca_mcf_sentence_get_word(maca_mcf_sentence *sentence, int index);

  /**
   * Clears the sentence of all its words.
   * 
   * @param sentence  the sentence we want to clear.
   */
  void maca_mcf_sentence_clear(maca_mcf_sentence *sentence);

  /**
   * Gets the length of a sentence.
   *
   * @param the sentence we want to get the length from.
   *
   * @return the length of a sentence.
   */
  int maca_mcf_sentence_get_length(maca_mcf_sentence *sentence);
  
  /**
   * Prints the sentence on the desired output.
   *
   * @param sentence the sentence we want to print.
   * @param out      the desired output.
   */
  void maca_mcf_sentence_print(maca_mcf_sentence *sentence, FILE *out);

  typedef struct McfSentences maca_mcf_sentences;

  /**
   * Creates a new maca_mcf_sentences.
   *
   * @return a pointer to the new maca_mcf_sentences.
   *
   * @see maca_mcf_sentences_delete()
   */
  maca_mcf_sentences *maca_mcf_sentences_new();

  /**
   * Frees a maca_mcf_sentences.
   *
   * @param sentences  the sentences we are freeing.
   *
   * @see maca_mcf_sentences_new()
   */
  void maca_mcf_sentences_delete(maca_mcf_sentences *sentences);

  /**
   * Releases a maca_mcf_sentences.
   *
   * @param sentences  the sentence we are releasing.
   *
   * @see maca_mcf_read_sentences()
   */
  void maca_mcf_sentences_release(maca_mcf_sentences *sentences);

  /**
   * Add a new sentence to the sentences.
   *
   * @param sentences the sentences we are adding the sentence into.
   * @param sentence  the sentence we are adding.
   */
  void maca_mcf_sentences_add_sentence(maca_mcf_sentences *sentences, maca_mcf_sentence *sentence);

  /**
   * Get a sentence from the sentences.
   *
   * @param sentences the sentences we are fetching from.
   * @param index     the index of a sentence.
   *
   * @return a pointer to a maca_mcf_sentence.
   *
   * @note you must release this pointer once you're finished with it!
   *
   * @see maca_mcf_sentence_release()
   */
  maca_mcf_sentence *maca_mcf_sentences_get_sentence(maca_mcf_sentences *sentences, int index);
  
  /**
   * Gets the number of sentences.
   *
   * @param sentences  the sentences we are working on.
   *
   * @return the number of sentences.
   */
  int maca_mcf_sentences_size(maca_mcf_sentences *sentences);


  /**
   * Prints all the sentences on the desired output.
   *
   * @param sentences the sentences to print.
   * @param out       the desired output.
   */
  void maca_mcf_sentences_print(maca_mcf_sentences *sentences, FILE *out);

  typedef struct McfColumn maca_mcf_column;

  /**
   * Gets the name of a column.
   *
   * @param column the column we are getting the name from.
   * @param name   the string buffer to store the name in.
   * @param size   the size of the buffer. 
   */
  void maca_mcf_column_get_name(maca_mcf_column *column, char *name, int size);

  /**
   * Gets the type of a column.
   *
   * @param column the column we are getting the type from.
   * @param type   the string buffer to store the type in.
   * @param size   the size of the buffer. 
   */
  void maca_mcf_column_get_type(maca_mcf_column *column, char *type, int size);

  /**
   * Gets the alphabet used by the column (if the type is an alphabet).
   *
   * @param column  the column we are getting the alphabet from.
   *
   * @return a pointer to the alphabet.
   *
   * @note you must release this pointer once you're finished with it!
   *
   * @see maca_alphabet_release()
   */
  maca_alphabet *maca_mcf_column_get_alphabet(maca_mcf_column *column);
  
  /**
   * Sets the alphabet of a column.
   *
   * @param column   the column we are updating.
   * @param alphabet the alphabet we are setting.
   */
  void maca_mcf_column_set_alphabet(maca_mcf_column *column, maca_alphabet *alphabet);

  typedef struct Mcf maca_mcf;

  /**
   * Creates a new maca_mcf.
   *
   * @param input_filename  the name of the file in the mcf format.
   *
   * @return a new maca_mcf.
   *
   * @see maca_mcf_new_with_alphabets()
   * @see maca_mcf_delete()
   */
  maca_mcf *maca_mcf_new(char *input_filename);

  /**
   * Creates a new maca_mcf, with filled alphabets.
   *
   * @param input_filename    the name of the file in the mcf format.
   * @param alphabet_filename the name of the file which contains all the alphabets.
   *
   * @return a new maca_mcf.
   * @see maca_mcf_new()
   * @see maca_mcf_delete()
   */
  maca_mcf *maca_mcf_new_with_alphabets(char *input_filename, char *alphabet_filename);

  /** 
   * Creates a new maca_mcf, with filled alphabets using alphabets from another alphabet array.
   * 
   * @param input_filename the name of the file in the mcf format.
   * @param array the alphabet array to take the alphabets from.
   * 
   * @return a new maca_mcf.
   *
   * @see maca_mcf_delete()
   */
  maca_mcf *maca_mcf_new_with_alphabet_array(char *input_filename, maca_alphabet_array *array);

  /**
   * Frees a maca_mcf.
   *
   * @param format the maca_mcf we are freeing.
   *
   * @see maca_mcf_new()
   * @see maca_mcf_new_with_alphabets()
   */
  void maca_mcf_delete(maca_mcf *format);
  
  /**
   * Notifies the format that a column will be used.
   *
   * @param format      the format of the mcf file.
   * @param column_name the name of the column.
   *
   * @return the id of column if it exists, -1 if it's an unknown column.
   */
  int maca_mcf_input(maca_mcf *format, char *column_name);

  /**
   * Adds an output column to the format.
   *
   * @param format      the format of the mcf file.
   * @param column_name the name of the new column.
   * @param column_type the type of the new column.
   *
   * @return the id of the new column, -1 if the column name already exists.
   *
   * @note in case the type is an alphabet, it will look for an alphabet with the
   *       same name. If it doesn't find one, it will create a new alphabet.
   */
  int maca_mcf_output(maca_mcf *format, char *column_name, char *column_type);
  
  /**
   * Gets a structure which stores information on the specified column.
   *
   * @param format the format of the mcf file.
   * @param id     the id of the column.
   *
   * @return a maca_mcf_column structure, or NULL if id is invalid.
   */
  maca_mcf_column *maca_mcf_get_column_info(maca_mcf *format, int id);

  /**
   * Gets a structure which stores information on the specified column.
   *
   * @param format the format of the mcf file.
   * @param name   the name of the column.
   *
   * @return a maca_mcf_column structure, or NULL if id is invalid.
   */
  maca_mcf_column *maca_mcf_get_column_info_by_name(maca_mcf *format, char *name);
  
  /**
   * Reads the next sentence in the mcf file.
   *
   * @param format  the format of the mcf file.
   *
   * @return a pointer to the read sentence, or NULL if none were found.
   *
   * @note you must release the pointer once you're finished with it!
   *
   * @see maca_mcf_sentence_release()
   */
  maca_mcf_sentence *maca_mcf_read_next_sentence(maca_mcf *format);

  /**
   * Reads all the sentences of the mcf file.
   *
   * @param format  the format of the mcf file.
   *
   * @return a pointer to the read sentences.
   *
   * @note you must release the pointer once you're finished with it!
   *
   * @see maca_mcf_sentences_release()
   */
  maca_mcf_sentences *maca_mcf_read_sentences(maca_mcf *format);

  /**
   * Prints the header into the desired output.
   *
   * @param format the format of the mcf file.
   * @param out    the desired output.
   */
  void maca_mcf_print_header(maca_mcf *format, FILE *out);

  /**
   * Dumps the alphabets into the specified file.
   *
   * @return 1 if successful, 0 otherwise.
   */
  char maca_mcf_dump_alphabets(maca_mcf *format, char *output_filename);

#ifdef __cplusplus
}
#endif
#endif /* __MACA_MCF_WRAPPER_H__ */
