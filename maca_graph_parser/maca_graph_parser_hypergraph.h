#ifndef __HYPERGRAPH__
#define __HYPERGRAPH__

#include <stdlib.h>
/**
 * Hypergraph
 */
#define TYP_OPEN 0
#define TYP_CLOSED 1
#define TYP_CLOSED2 2

typedef struct {
  int type; /* must not be changed */
  int start;
  int end;
  int dir;
  int label;
} SgOpen;

typedef struct {
  int type; /* must not be changed */
  int start;
  int end;
  int dir;
} SgClosed;

typedef struct {
  int type; /* must not be changed */
  int start;
  int end;
  int dir;
  int breakpoint;
} SgClosed2;

/* http://stackoverflow.com/a/18577481 */
/* http://www.sbin.org/doc/Xlib/chapt_21_app_E.html */
typedef union _VertexSignature {
  int type; /* must not be changed */
  SgOpen open;
  SgClosed closed;
  SgClosed2 closed2;
} VertexSignature;

typedef struct _Hyperarc Hyperarc;

typedef struct {
  VertexSignature *vsign; /* vertex signature */
  Hyperarc **bs; /* backstar */
  size_t bs_size; /* backstar size */
} Vertex;

struct _Hyperarc {
  Vertex *tail[2]; /* tail: vector of vertices; here: fixed arity = 2 */
  Vertex *head; /* head */
  /* weight function from R^{|T(e)|} to R */
  int ds_i; /* derivation index in the backstar of e's head vertex */ /* test */
};

/* derivation */
typedef struct Derivation {
  /* vertex v; */ /* == e.head */
  float weight; /* score */
  Hyperarc *e;
  struct Derivation *subd[2]; /* vector (here array) of subderivations, one for each vertex in tail */
} Derivation; 

/* derivation with backpointers */
typedef struct {
  /* vertex v; */ /* == e.head */
  float weight; /* score */
  Hyperarc *e;
  int j[2]; /* vector (here array) of subderivation indices, one for each vertex in tail */
} DerivBP; 

typedef struct {
  int num; /* current size */
  int capacity; /* max size */
  Vertex **elts; /* array of elements */
} vec_Vertex; /* vector of vertices */


#ifdef __cplusplus
extern "C"{
#endif


void get_derivBPs(Vertex *v, DerivBP **result);

VertexSignature *alloc_vertexSignature();
void free_vertexSignature(VertexSignature *v);
void print_vertexSignature(VertexSignature *v);
void init_hyperopen(VertexSignature *vs, int start, int end, int label, int dir);
void init_hyperclosed(VertexSignature *vs, int start, int end, int dir);
void init_hyperclosed2(VertexSignature *vs, int start, int end, int breakpoint, int dir);

Vertex *alloc_vertex();
void init_vertex(Vertex *v, vec_Vertex *tails, size_t bs_size, VertexSignature *vs);
void free_vertex(Vertex *v);

Hyperarc *alloc_hyperarc(Vertex *tail[2], Vertex *head);
void free_hyperarc(Hyperarc *e);

Derivation *alloc_derivation(float weight, Hyperarc *e, Derivation *subd[2]);
void free_derivation(Derivation *d);

DerivBP *alloc_derivBP(float weight, Hyperarc *e, int j[2]);
void free_derivBP(DerivBP *d);

vec_Vertex *alloc_vec_Vertex(int capacity);
void free_vec_Vertex(vec_Vertex *vv);
void vec_Vertex_append(vec_Vertex *vv, Vertex *v);

#ifdef __cplusplus
}
#endif

#endif
