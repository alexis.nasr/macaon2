/*******************************************************************************
    Copyright (C) 2012 by Alexis Nasr <alexis.nasr@lif.univ-mrs.fr>
                          Ghasem Mirroshandel <ghasem.mirroshandel@lif.univ-mrs.fr>
    This file is part of maca_graph_parser.

    maca_graph_parser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    maca_graph_parser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with maca_graph_parser. If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

#include<getopt.h>
#include<unistd.h>
#include<stdio.h>
#include<string.h>

#include "maca_common.h"
#include "maca_constants.h"
#include "maca_graph_parser.h"
#include "maca_graph_parser_sentence.h"
#include "maca_graph_parser_hash.h"
#include "maca_graph_parser_features.h"
#include "maca_graph_parser_decoder1.h"
#include "maca_graph_parser_conll2007_format.h"
#include "maca_graph_parser_metrics.h"
#include "maca_graph_parser_decoder.h"
#include "maca_graph_parser.h"

/*-------------------------------------------------------------------------------------------*/

int main(int argc, char **argv)
{
  maca_graph_parser_ctx * ctx;
  maca_graph_parser_sentence *ref = NULL; 
  maca_graph_parser_sentence *hyp = NULL;
  int sent_num;
  double sentence_las;
  int total_correct_dep = 0;
  int total_dep = 0;
  double corpus_las;
  /* kbest: oracle LAS */
  double sentence_las_oracle;
  int total_correct_dep_oracle = 0;
  double corpus_las_oracle;

  ctx = maca_graph_parser_LoadCTX(argc,argv);
  
  ref = maca_graph_parser_allocate_sentence(ctx);
  hyp = maca_graph_parser_allocate_sentence(ctx);
  FILE *conll_file = NULL; // TODO: fix
  if(conll_file){
    sent_num = 0; 
    while(1){
      if(sent_num >= ctx->sent_nb) break;

      ref = maca_graph_parser_read_conll_sentence(ctx, conll_file, ref); 
      if(ref == NULL) break;
      if(ref->l == 1) break;
      if(ref->l >= ctx->max_sent_length) continue;
	
      hyp = maca_graph_parser_duplicate_sentence(ctx, ref, hyp);
      maca_graph_parser_decoder_parse(ctx, hyp);

      /* metrics */
      sentence_las = maca_graph_parser_sentence_compute_las(ref, hyp, 0);
      total_correct_dep += maca_graph_parser_sentence_compute_las(ref, hyp, 1);
      total_dep += (hyp->l -1); /* MM: bugfix for compute_las: -1 to discount fake root */
      corpus_las = (double) total_correct_dep / (double) total_dep;
      fprintf(stderr, "%d\t%f\t%f", sent_num, sentence_las, corpus_las);
      if(ctx->k > 1){
	sentence_las_oracle = maca_graph_parser_sentence_compute_las_oracle(ref, hyp, ctx->k, 0);
	total_correct_dep_oracle += maca_graph_parser_sentence_compute_las_oracle(ref, hyp, ctx->k, 1);
	corpus_las_oracle = (double) total_correct_dep_oracle / (double) total_dep; 
	fprintf(stderr, "\t%f\t%f", sentence_las_oracle, corpus_las_oracle);
      }
      fprintf(stderr, "\n");

      sent_num++;
    }
    printf("LAS = %f\n", corpus_las);    
  }
  maca_graph_parser_free_sentence(hyp);
  maca_graph_parser_free_sentence(ref);

  maca_graph_parser_free_all(ctx);
  return 0;
}
