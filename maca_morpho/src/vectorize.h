#ifndef __VECTORIZE__
#define __VECTORIZE__

#include"dico.h"
#include"feat_model.h"
#include"feat_vec.h"

#define LOOKUP_MODE 1
#define ADD_MODE 2


feat_vec *form2fv(char *form, feat_vec *fv, feat_model *fm, dico *dico_features, int mode);

#endif
