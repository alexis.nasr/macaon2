#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"vectorize.h"

int get_feat_value(feat_model *fm, char *form, dico *dico_features, int feat_nb, int mode)
{
  feat_desc *fd = fm->array[feat_nb];
  int i;
  int feat_val;
  char str[10];

  /*  the name of the feature is built in fm->string and its value in the dictionnary (dico_features) is returned */
  fm->string[0] = '\0';
  for(i=0; i < fd->nbelem; i++){
    strcat(fm->string, fd->array[i]->name);
    feat_val = fd->array[i]->fct(form);
    sprintf(str, "%d", feat_val);
    strcat(fm->string, str);

    /* catenate_int(fm->string, feat_val); */
  }
  if(mode == LOOKUP_MODE){
    if(fm->string)
    return dico_string2int(dico_features, fm->string);
  } 
  return dico_add(dico_features, fm->string);
}


feat_vec *form2fv(char *form, feat_vec *fv, feat_model *fm, dico *dico_features, int mode)
{
  int i;
  feat_vec_empty(fv);
  for(i=0; i < fm->nbelem; i++)
    feat_vec_add(fv, get_feat_value(fm, form, dico_features, i, mode));
  return fv;
}
