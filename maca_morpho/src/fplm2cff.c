#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "maca_morpho_context.h"
#include "feat_model.h"
#include "feat_vec.h"
#include "dico.h"
#include "util.h"
#include "vectorize.h"

void decompose_feature_value(char *feature_value, char *feature, char *value)
{
  int i,j;
  int l = strlen(feature_value);
  int before = 1;
  for(i=0; (i < l) && (feature_value[i] != '='); i++){
    feature[i] = feature_value[i];
  }
  feature[i] = '\0';
  i++;
  for(j=0; i<l; i++, j++){
    value[j] = feature_value[i];
  }
  value[j] = '\0';
}


int main(int argc, char *argv[])
{
  context *ctx = context_read_options(argc, argv);
  if(ctx->help){
    context_general_help_message(ctx);
    context_language_help_message(ctx);
    context_fplm_help_message(ctx);
    context_maca_data_path_help_message(ctx);
    context_features_filename_help_message(ctx);
    context_features_model_help_message(ctx);
    exit(1);
  }
  feat_vec *fv = feat_vec_new(10);
  dico *dico_features = dico_new("dico_features", 1000);
  /*  feat_model *fm = feat_model_read(ctx->fm_filename, feat_lib_build(), ctx->verbose); */
  char form[100];
  char pos[100];
  char lemma[100];
  char morpho[100];
  FILE *F_fplm = NULL;
  char buffer[1000];
  char feature_value[100];
  char feature[100];
  char value[100];
  char *token;

  
  F_fplm = myfopen(ctx->fplm_filename, "r");

  
  while(fgets(buffer, 1000, F_fplm)){
    if(feof(F_fplm)) 
      break;
    //    printf("%s", buffer);
    buffer[strlen(buffer) - 1] = '\0';
    sscanf(buffer, "%[^\t]\t%[^\t]\t%[^\t]\t%[^\n]\n", form, pos, lemma, morpho);
    //printf("form = %s pos = %s lemma = %s morpho = %s\n", form, pos, lemma, morpho);
    token = strtok(morpho, "|");
    do{
      //printf("token = %s\n", token);
      decompose_feature_value(token, feature, value);
      //printf("feature = %s value = %s\n", feature, value);
    }while((token = strtok(NULL, "|")));
    
    
  }
  fclose(F_fplm);
}
    
    /*
  while(strcmp(form, "end")){
    fscanf(stdin, "%s", form);
    printf("form = %s\n", form);
    form2fv(form, fv, fm, dico_features, ADD_MODE);
    //void      feat_vec_print_string(feat_vec *fv, dico *dico_features);
    feat_vec_print(stdout, fv);
  }
  //dico_print_fh(stdout, dico_features);
  if(ctx->features_filename)
    dico_print(ctx->features_filename, dico_features);
    */

  
  
