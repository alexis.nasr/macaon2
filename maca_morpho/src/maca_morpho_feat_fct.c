#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"feat_lib.h"



int s1(void *input){return(input == NULL)? -1 : ((char *)input)[strlen((char *)input) - 1];}
int s2(void *input){return(input == NULL)? -1 : ((char *)input)[strlen((char *)input) - 2];}

feat_lib *feat_lib_build(void)
{
  feat_lib *fl = feat_lib_new();
  
  feat_lib_add(fl, 1,  (char *)"s1", s1);
  feat_lib_add(fl, 1,  (char *)"s2", s2);
  return fl;
}

