#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "maca_morpho_context.h"
#include "feat_model.h"
#include "feat_vec.h"
#include "dico.h"
#include "util.h"
#include "vectorize.h"
#include "feature_table.h"

void predict_help_message(context *ctx)
{
  context_general_help_message(ctx);
  context_language_help_message(ctx);
  context_fplm_help_message(ctx);
  context_maca_data_path_help_message(ctx);
  context_features_filename_help_message(ctx);    
  context_weights_matrix_filename_help_message(ctx);
  context_features_model_help_message(ctx);
  exit(1);
}


int main(int argc, char *argv[])
{
  context *ctx = context_read_options(argc, argv);
  if(ctx->help) predict_help_message(ctx);
  feature_table *cfw = feature_table_load(ctx->cfw_filename, ctx->verbose);
  feat_vec *fv = feat_vec_new(10);
  dico *dico_features = dico_read(ctx->features_filename, 0.5);
  feat_model *fm = feat_model_read(ctx->fm_filename, feat_lib_build(), ctx->verbose); 
  char form[100];
  int class;
  float max;
  
  
  while(strcmp(form, "end")){
    fscanf(stdin, "%s", form);
    printf("form = %s\n", form);
    form2fv(form, fv, fm, dico_features, LOOKUP_MODE);
    class = feature_table_argmax(fv, cfw, &max);
    feat_vec_print(stdout, fv);
    printf("class = %d\n", class);
    
  }

  if(ctx->features_filename)
    dico_print(ctx->features_filename, dico_features);


  
}
