#ifndef __MACA_MORPHO_CONTEXT__
#define __MACA_MORPHO_CONTEXT__

#include "mcd.h"
#include <stdlib.h>

#define DEFAULT_FPLM_FILENAME "fplm"



typedef struct {
  int   help;
  int   verbose;
  int   debug_mode;
  char *program_name;
  char *fplm_filename;
  char *language;
  char *maca_data_path;
  char *fm_filename;
  char *features_filename;
  char *cfw_filename;
} context;



context *context_new(void);
void     context_free(context *ctx);

context *context_read_options(int argc, char *argv[]);
void     context_general_help_message(context *ctx);
void     context_language_help_message(context *ctx);
void     context_fplm_help_message(context *ctx);
void     context_maca_data_path_help_message(context *ctx);
void     context_features_filename_help_message(context *ctx);
void     context_weights_matrix_filename_help_message(context *ctx);
void     context_features_model_help_message(context *ctx);
#endif
