set(SOURCES
  src/maca_morpho_feat_fct.c
  src/maca_morpho_context.c
  src/vectorize.c
)



#compiling library
include_directories(src)
add_library(maca_morpho STATIC ${SOURCES})
target_link_libraries(maca_morpho perceptron)
target_link_libraries(maca_morpho maca_common)


  
#compiling, linking and installing executables

add_executable(fplm2cff ./src/fplm2cff.c)
target_link_libraries(fplm2cff perceptron)
target_link_libraries(fplm2cff maca_common)
target_link_libraries(fplm2cff maca_morpho)
install (TARGETS fplm2cff DESTINATION bin)

add_executable(predict ./src/predict.c)
target_link_libraries(predict perceptron)
target_link_libraries(predict maca_common)
target_link_libraries(predict maca_morpho)
install (TARGETS predict DESTINATION bin)

