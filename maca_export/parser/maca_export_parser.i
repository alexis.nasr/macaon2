%module MacaonParser

%{
#include <maca_trans_parser_export.h>
%}



class MacaonTransParser {
    public:
	MacaonTransParser(char *lg, char *mcd);
	~MacaonTransParser();
	const char *parsemcf(const char *mcf);
};
