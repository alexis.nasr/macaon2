
//#ifdef __cplusplus
//extern "C"{
//#endif 

 #include "context.h"
 #include "config.h"

//#ifdef __cplusplus
//}
//#endif


class MacaonTransParser {
 public:
    /**
       creates instance of MacaonParser, assumes that the environment variable MACAON_DIR
       is defined
       @param lg language to be used (in the sense of sub-dir in MACAON_DIR)
       @param mcd the filename of the mcd definitions
     */
    MacaonTransParser(char *lg, char *mcd);
    ~MacaonTransParser();
    /** call dependency parser
	@param mcfString a string containing the sentence  to be analysed in mcf format
	(at least the columns form, pos, lemma must be present
	@return the parser output
     */
    const char *parsemcf(const char *mcfString);

    int initOK;

 private:
    /// keeps parser context 
    context *ctx;
    /// keeps last result (or NULL)
    char *resultstring;


    void set_linguistic_resources_filenames_parser(context *ctx);
    void print_word_buffer_fp(config *c, dico *dico_labels, mcd *mcd_struct, FILE *out);
    void simple_decoder_parser_arc_eager_str(context *ctx, const char *mcfString);
};




