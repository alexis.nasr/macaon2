//#include "maca_trans_parser/src/context.h"
//#include "maca_common/include/word.h"

//#ifdef __cplusplus
//extern "C"{
//#endif 

 #include "context.h"
 #include "../../maca_common/include/word.h"

//#ifdef __cplusplus
//}
//#endif


class MacaonTransTagger {
 public:
    /**
       creates instance, assumes that the environment variable MACAON_DIR
       is defined. One instance for each thread has to be created (this means loading resources for each thread).
       @param lg language to be used (in the sense of sub-dir in MACAON_DIR)
       @param mcd the filename of the mcd definitions
     */
    MacaonTransTagger(char *lg, char *mcd);

    ~MacaonTransTagger();

    /** call tagger
	@param mcfString a string containing the sentence to be analysed in mcf format.
	At least the column form must be present
	@return the parser output
     */
    const char *tagmcf(const char *mcfString);

    int initOK;

 private:
    /// keeps parser context 
    context *ctx;
    /// keeps last result (or NULL)
    char *resultstring;

    // import functions which are not available in libtransparse.a or are modified
    void decode_tagger_set_linguistic_resources_filenames(context *ctx);
    void simple_decoder_tagger(context *ctx, const char *mcf);
    void print_word(word *w, mcd *mcd_struct, dico *dico_pos, int postag, FILE *stream);
};



