#include <cstdio>
#include <cstring>


//#ifdef __cplusplus
//extern "C"{
//#endif 

 #include "config2feat_vec.h"
 #include "movement_tagger.h"
 #include "simple_decoder_tagger.h"

//#ifdef __cplusplus
//}
//#endif

#include "maca_trans_tagger_export.h"


MacaonTransTagger::MacaonTransTagger(char *lg, char *mcd) {
    char * argv[] = { (char *)"initParser",
		      (char *)"-L", lg,
		      (char *)"-C", mcd,
		      //(char *)"--debug",
		      //(char *)"-v",
	0
    };

    ctx = context_read_options(5, argv);

    decode_tagger_set_linguistic_resources_filenames(ctx);
    ctx->features_model = feat_model_read(ctx->features_model_filename, feat_lib_build(), ctx->verbose);
    ctx->vocabs = dico_vec_read(ctx->vocabs_filename, ctx->hash_ratio);
    mcd_link_to_dico(ctx->mcd_struct, ctx->vocabs, ctx->verbose);

    ctx->d_perceptron_features = dico_vec_get_dico(ctx->vocabs, (char *)"d_perceptron_features");
    resultstring = NULL;
}

MacaonTransTagger::~MacaonTransTagger() {
    context_free(ctx);
    if (resultstring != NULL) {
	free(resultstring);
	resultstring = NULL;
    }
}

const char *MacaonTransTagger::tagmcf(const char *mcfString) {
    if (resultstring != NULL) {
	free(resultstring);
	resultstring = NULL;
    }
    if(ctx->beam_width == 1)
	simple_decoder_tagger(ctx, mcfString);

    return resultstring;
}


/** taken as is from maca_trans_tagger.c */
void MacaonTransTagger::decode_tagger_set_linguistic_resources_filenames(context *ctx) {
    char absolute_filename[500];
  
    if(!ctx->perc_model_filename){
	strcpy(absolute_filename, ctx->maca_data_path);
	strcat(absolute_filename, DEFAULT_MODEL_TAGGER_FILENAME);
	ctx->perc_model_filename = strdup(absolute_filename);
    }

    if(!ctx->vocabs_filename){
	strcpy(absolute_filename, ctx->maca_data_path);
	strcat(absolute_filename, DEFAULT_VOCABS_TAGGER_FILENAME);
	ctx->vocabs_filename = strdup(absolute_filename);
    }

    /*  if(!ctx->mcd_filename){
	strcpy(absolute_filename, ctx->maca_data_path);
	strcat(absolute_filename, DEFAULT_MULTI_COL_DESC_TAGGER_FILENAME);
	ctx->mcd_filename = strdup(absolute_filename);
	}*/

    if(!ctx->features_model_filename){
	strcpy(absolute_filename, ctx->maca_data_path);
	strcat(absolute_filename, DEFAULT_FEATURES_MODEL_TAGGER_FILENAME);
	ctx->features_model_filename = strdup(absolute_filename);
    }

    if(!ctx->f2p_filename){
	strcpy(absolute_filename, ctx->maca_data_path);
	strcat(absolute_filename, DEFAULT_F2P_FILENAME);
	ctx->f2p_filename = strdup(absolute_filename);
	ctx->f2p = form2pos_read(ctx->f2p_filename);
    }

    if(ctx->verbose){
	fprintf(stderr, "perc_model_filename = %s\n", ctx->perc_model_filename);
	fprintf(stderr, "vocabs_filename = %s\n", ctx->vocabs_filename);
	fprintf(stderr, "mcd_filename = %s\n", ctx->mcd_filename);
	fprintf(stderr, "perc_features_model_filename = %s\n", ctx->features_model_filename);
	fprintf(stderr, "f2p_filename = %s\n", ctx->f2p_filename);
    }
}

/** taken from simple_decoder_tagger.c and modified to read from string and write to string */
void MacaonTransTagger::simple_decoder_tagger(context *ctx, const char *mcfString) {
    config *c;
    feat_vec *fv = feat_vec_new(feature_types_nb);
    //FILE *f = (ctx->input_filename)? myfopen(ctx->input_filename, "r") : stdin;
    FILE *f = fmemopen ((void *)mcfString, strlen(mcfString), "r");
    feature_table *ft =  feature_table_load(ctx->perc_model_filename, ctx->verbose);
    int postag;
    float max;
    word *b0;
    dico *dico_pos = dico_vec_get_dico(ctx->vocabs, (char *)"POS");

    c = config_new(f, ctx->mcd_struct, 5); 

    size_t size;
    if (resultstring != NULL) {
	free(resultstring);
	resultstring = NULL;
    }

    FILE *outstream = open_memstream (&resultstring, &size);
    while(!config_is_terminal(c)){
	if(ctx->f2p)
	    /* add_signature_to_words_in_word_buffer(c->bf, ctx->f2p, dico_pos); */
	    add_signature_to_words_in_word_buffer(c->bf, ctx->f2p); 

	b0 = word_buffer_b0(c->bf);
	postag = -1; //word_get_pos(b0);

	if(ctx->debug_mode){
	    fprintf(stderr, "***********************************\n");
	    config_print(stderr, c);
	}
    
	/* if postag is not specified in input it is predicted */
	if(postag == -1){
	    /* config_print(stdout, c); */
	    config2feat_vec_cff(ctx->features_model, c, ctx->d_perceptron_features, fv, LOOKUP_MODE);
      
	    /* feat_vec_print(stdout, fv); */
	    postag = feature_table_argmax(fv, ft, &max);
	    /* printf("postag = %d\n", postag); */

	    if(ctx->debug_mode){
		vcode *vcode_array = feature_table_get_vcode_array(fv, ft);
		for(int i=0; i < 3; i++){
		    fprintf(stderr, "%d\t", i);
		    fprintf(stderr, "%s\t%.4f\n", dico_int2string(dico_pos, vcode_array[i].class_code), vcode_array[i].score);
		}
		free(vcode_array);
	    }
	}

	print_word(b0, ctx->mcd_struct, dico_pos, postag, outstream);
    
	movement_tagger(c, postag);

    }
    fclose(outstream);
    /* config_print(stdout, c);  */
    feat_vec_free(fv);
    feature_table_free(ft);
    config_free(c); 
    fclose(f);
}

/** taken from simple_decoder_tagger.c and modified (paramater FILE *outstreal)
 */
void MacaonTransTagger::print_word(word *w, mcd *mcd_struct, dico *dico_pos, int postag, FILE *outstream) {
    char *buffer = NULL;
    char *token = NULL;
    int col_nb = 0;
    if(mcd_get_pos_col(mcd_struct) == -1){
	fprintf(outstream, "%s\t%s\n", w->input, dico_int2string(dico_pos, postag));
    }
    else{
	buffer = strdup(w->input);
	token = strtok(buffer, "\t");
	col_nb = 0;
	while(token){
	    if(col_nb != 0) fprintf(outstream, "\t");
	    if(col_nb == mcd_get_pos_col(mcd_struct))
		fprintf(outstream, "%s", dico_int2string(dico_pos, postag));
	    else
		word_print_col_n(outstream, w, col_nb);
	    col_nb++;
	    token = strtok(NULL, "\t");
	}
	if(col_nb <= mcd_get_pos_col(mcd_struct))
	    fprintf(outstream, "\t%s", dico_int2string(dico_pos, postag));
	fprintf(outstream, "\n");
	free(buffer);
    }
}
