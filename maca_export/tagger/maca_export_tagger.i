%module MacaonTagger

%{
#include <maca_trans_tagger_export.h>
%}

class MacaonTransTagger {
    public:
	MacaonTransTagger(char *lg, char *mcd);
	~MacaonTransTagger();
	const char *tagmcf(const char *mcf);
};
