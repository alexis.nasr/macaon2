%module MacaonLemmatizer

%{
#include <maca_lemmatizer_export.h>
%}

class MacaonTransLemmatizer {
    public:
	MacaonTransLemmatizer(char *lg, char *mcd);
	~MacaonTransLemmatizer();
	const char *lemmatizemcf(const char *mcf);
};

