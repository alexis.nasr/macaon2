//#ifdef __cplusplus
//extern "C"{
//#endif 

 #include "context.h"
 #include "../../../maca_trans_parser/src/config.h"
 #include "word.h"

//#ifdef __cplusplus
//}
//#endif


class MacaonTransLemmatizer {
 public:
    /**
       creates instance, assumes that the environment variable MACAON_DIR
       is defined. One instance for each thread has to be created (this means loading resources for each thread).
       @param lg language to be used (in the sense of sub-dir in MACAON_DIR)
       @param mcd the filename of the mcd definitions
     */
    MacaonTransLemmatizer(char *lg, char *mcd);

    ~MacaonTransLemmatizer();

    /** call lemmatizer
	@param mcfString a string containing the sentence  to be analysed in mcf format
	(at least the columns form, pos must be present
	@return the parser output
     */
    const char *lemmatizemcf(const char *mcfString);

    int initOK;

 private:
    /// keeps parser context 
    context *ctx;
    /// keeps last result (or NULL)
    char *resultstring;

    /// variables used during lemmatization
    char form_pos[1000];
    char lemma[200];
    char form[200];
    char pos[200];

    /// variables to stock data
    hash *form_pos_ht = NULL;
    char **lemma_array = NULL;
    int lemma_array_size;

    void maca_lemmatizer_set_linguistic_resources_filenames(context *ctx);
    char **read_fplm_file(char *fplm_filename, hash *form_pos_ht, int debug_mode, int *lemma_array_size);
    void print_word(word *w, mcd *mcd_struct, char *lemma, FILE *stream);
    char *lookup_lemma(char *form, char *pos, hash *form_pos_ht, char **lemma_array, int verbose);
};




