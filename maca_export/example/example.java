import lif.*;
/** example to use the macaon parser with java
    compile (in maca_data2)
     javac -cp ../macaon2/build/maca_export/tagger/macaontagger.jar:../macaon2/build/maca_export/lemmatizer/macaonlemmatizer.jar:../macaon2/build/maca_export/parser/macaonparser.jar ../macaon2/maca_export/example/example.java

    run
     java -cp ../macaon2/build/maca_export/tagger/macaontagger.jar:../macaon2/build/maca_export/lemmatizer/macaonlemmatizer.jar:../macaon2/build/maca_export/parser/macaonparser.jar:../macaon2/maca_export/example  -Djava.library.path=../macaon2/build/maca_export/tagger:../macaon2/build/maca_export/lemmatizer:../macaon2/build/maca_export/parser example

     make jar
      jar -cf ../macaon2/maca_export/example/macaon.jar -C ../macaon2/maca_export/example example.class


 */
public class example {
    MacaonTransTagger mt;
    MacaonTransLemmatizer ml;
    MacaonTransParser mp;

    
    public example(String lg, String mcd, boolean loadlibraries) {
	if (loadlibraries) {
	    System.loadLibrary("MacaonTransTaggerJava"); // use libMacaonTransTaggerJava.so
	    System.loadLibrary("MacaonTransLemmatizerJava");
	    System.loadLibrary("MacaonTransParserJava");
	}

	mt = new MacaonTransTagger(lg, mcd);
	ml = new MacaonTransLemmatizer(lg, mcd);
	mp = new MacaonTransParser(lg, mcd);
    }

    public void test1() {
	/*	StringBuilder mcf1 = new StringBuilder("La	D	le\n");
	mcf1.append("pose	N	pose\n");
	mcf1.append("d'	P	de\n");
	mcf1.append("un	D	un\n");
	mcf1.append("panneau	N	panneau\n");
	mcf1.append("stop	N	stop\n");
	mcf1.append("paraît	V	paraître\n");
	mcf1.append("être	V	être\n");
	mcf1.append("la	D	le\n");
	mcf1.append("formule	N	formule\n");
	mcf1.append("la	D	le\n");
	mcf1.append("mieux	ADV	mieux\n");
	mcf1.append("adaptée	A	adapté\n");
	mcf1.append("pour	P	pour\n");
	mcf1.append("assurer	V	assurer\n");
	mcf1.append("la	D	le\n");
	mcf1.append("sécurité	N	sécurité\n");
	mcf1.append("des	P+D	de\n");
	mcf1.append("usagers	N	usager\n");
	mcf1.append(".	PONCT	.\n");*/


	StringBuilder mcf1 = new StringBuilder("La\n");
	mcf1.append("pose\n");
	mcf1.append("d'\n");
	mcf1.append("un\n");
	mcf1.append("panneau\n");
	mcf1.append("stop\n");
	mcf1.append("paraît\n");
	mcf1.append("être\n");
	mcf1.append("la\n");
	mcf1.append("formule\n");
	mcf1.append("la\n");
	mcf1.append("mieux\n");
	mcf1.append("adaptée\n");
	mcf1.append("pour\n");
	mcf1.append("assurer\n");
	mcf1.append("la\n");
	mcf1.append("sécurité\n");
	mcf1.append("des\n");
	mcf1.append("usagers\n");
	mcf1.append(".\n");

	
	String tags = mt.tagmcf(mcf1.toString());
	System.out.println(tags);
	String lemmas = ml.lemmatizemcf(tags);
	System.out.println(lemmas);
	System.out.println(mp.parsemcf(lemmas));
	
    }

    public String tag(String text) {
	String t = text.replaceAll("([\\.,\\?;:!/])", " $1 ");

	String [] elems = t.split("[\\s]+");
	StringBuilder mcf1 = new StringBuilder();
	for (String elem : elems) {
	    mcf1.append(elem).append('\n');
	}
	//System.out.println(mcf1);
	String tags = mt.tagmcf(mcf1.toString());
	//System.out.println(tags);
	String lemmas = ml.lemmatizemcf(tags);

	return lemmas;
    }

    public static void main(String []args) {
	example ex = new example("jh-seq", "jh-seq/eval/wplgfs.mcd", true);
	ex.test1();
	//String res = ex.tag("la souris mange.");
	//System.out.println(res);
    }
}
