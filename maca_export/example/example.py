#!/usr/bin/python
# -*- coding: UTF-8 -*-


import os
currentdir = os.path.dirname(os.path.abspath(__file__))

import sys

sys.path.append(currentdir + "/../../build/maca_export/tagger")
sys.path.append(currentdir + "/../../build/maca_export/lemmatizer")
sys.path.append(currentdir + "/../../build/maca_export/parser")


import MacaonTagger
import MacaonLemmatizer
import MacaonParser

# for this example you should be in maca_data2
mt = MacaonTagger.MacaonTransTagger("jh-seq", "jh-seq/eval/wplgfs.mcd")
ml = MacaonLemmatizer.MacaonTransLemmatizer("jh-seq", "jh-seq/eval/wplgfs.mcd")
mp = MacaonParser.MacaonTransParser("jh-seq", "jh-seq/eval/wplgfs.mcd")

mcf="""La
grosse
souris
verte
a
mangé
le
bon
fromage
hier
soir"""

mcf0 ="""La	D
pose	N
d'	P
un	D
panneau	N
stop	N
paraît	V"""

mcf1 = """La	D	le
pose	N	pos
d'	P	de
un	D	un
panneau	N	panneau
stop	N	stop
paraît	V	paraître
être	V	être
la	D	le
formule	N	formule
la	D	le
mieux	ADV	mieux
adaptée	A	adapté
pour	P	pour
assurer	V	assurer
la	D	le
sécurité	N	sécurité
des	P+D	de
usagers	N	usager
.	PONCT	."""


mcf2 = """Une	D	un
réflexion	N	réflexion
commune	A	commun
est	V	être
menée	V	mener
avec	P	avec
les	D	le
enseignants	N	enseignant
et	C	et
les	D	le
délégués	N	délégué
de	P	de
parents	N	parent
d'	P	de
élèves	N	élève
,	PONCT	,
sous	P	sous
la	D	le
conduite	N	conduite
du	P+D	de
CAUE	N	CAUE
.	PONCT	."""

#print mp.parsemcf(mcf1)

#print mp.parsemcf(mcf2)
#print ml.lemmatizemcf(mcf0)

tags = mt.tagmcf(mcf)
print tags
lemmas = ml.lemmatizemcf(tags)
print lemmas
print mp.parsemcf(lemmas)

del mt
del ml
del mp


