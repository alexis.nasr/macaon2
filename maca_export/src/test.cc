#include <iostream>
#include <fstream>

#include "maca_lemmatizer_export.h"
#include "maca_trans_parser_export.h"
#include "maca_trans_tagger_export.h"

// test programme to be able using valgrind on the exported part of macaon
// run (in maca_data2)
/**
   ../macaon2/build/maca_export/src/testcxx  jh-seq jh-seq/eval/wplgfs.mcd ../macaon2/maca_export/src/test.mcf 
 */

using namespace std ;

int main(int argc, char *argv[]) {
    if (argc < 4) {
	cerr << "usage: " << argv[0] << " <langue> <mcd-file> <mcf-file>" << endl;
	return 1;
    }

    MacaonTransTagger *mt = new MacaonTransTagger(argv[1], argv[2]);
    MacaonTransLemmatizer *ml = new MacaonTransLemmatizer(argv[1], argv[2]);
    MacaonTransParser *mp = new MacaonTransParser(argv[1], argv[2]);

    ifstream ifp(argv[3]);
    string text;
    string line;
    while (!ifp.eof()) {
	getline(ifp, line);
	text += line + "\n";
    }

    const char *tags = mt->tagmcf(text.c_str());
    //cout << tags << endl;
    const char *lemmas = ml->lemmatizemcf(tags);
    const char *deps = mp->parsemcf(lemmas);
    //const char *deps = mp->parsemcf(text.c_str());
    cout << deps << endl;


    delete mt;
    delete ml;
    delete mp;
}
