#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<strings.h>
#include<math.h>
#include<getopt.h>
#include"conll_lib.h" 
#include"hash_str.h" 


void traite_au_revoir(conll_sentence *s, int pos)
{
  conll_word *au = conll_allocate_word(-1, (char *) "au", (char *) "au", (char *) "prep", (char *) "prep", (char *) "NULL", -1, (char *) "NULL");
  conll_word *revoir = conll_allocate_word(-1, (char *) "revoir", (char *) "revoir", (char *) "nc", (char *) "nc", (char *) "NULL", -1, (char *) "OBJ");
  conll_split_node_in_two(s, pos, au, revoir, pos, pos+1);

}  

void traite_ADV_que(conll_sentence *s, char *form, int pos)
{
  int i,j;
  char form_adv[30];
  char form_que[30];
  int l = strlen(form);


  for(i=0; (i<l) && (form[i] != '_'); i++){
    form_adv[i] = form[i];
  }
  form_adv[i] = '\0';
  i++;
  
  for(j=0; (i<l); i++, j++){
    form_que[j] = form[i];
  }
  form_que[j] = '\0';

  conll_word *que = conll_allocate_word(-1, form_que, (char *) "que", (char *) "CSU", (char *) "CSU", (char *) "NULL", -1, (char *) "NULL");
  conll_word *adv = conll_allocate_word(-1, form_adv, form_adv, (char *) "ADV", (char *) "ADV", (char *) "NULL", -1, (char *) "MORPH");

  conll_split_node_in_two(s, pos, que, adv, pos+1, pos);

}  

int chaine_possede_un_underscore(char *m)
{
  int i;
  int l = strlen(m);
  for(i=0; i < l; i++){
    if(m[i] == '_'){
      return 1;
    }
  }
    return 0;
}


int chaine_possede_un_plus(char *m)
{
  int i;
  int l = strlen(m);
  for(i=0; i < l; i++){
    if(m[i] == '+'){
      return 1;
    }
  }
    return 0;
}



int chaine_possede_un_moins(char *m)
{
  int i;
  int l = strlen(m);
  for(i=0; i < l; i++){
    if(m[i] == '-'){
      return 1;
    }
  }
    return 0;
}


int chaine_possede_un_plus_ou_un_moins(char *m)
{
  int i;
  int l = strlen(m);
  for(i=0; i < l; i++){
    if((m[i] == '-') || (m[i] == '+')){
      return 1;
    }
  }
    return 0;
}


void orfeo_traite_mots_composes(conll_sentence *s)
{
  unsigned i;
  conll_word *w;

  for(i=1; i<s->l; i++){
    w = s->words[i];
    if(chaine_possede_un_plus(w->form)){
      if(!strcmp(w->form, (char *) "au+revoir")) traite_au_revoir(s, i);
    }
    else if(chaine_possede_un_underscore(w->form)){
      if(!strcmp(w->form, (char *) "bien_que") || !strcmp(w->form, (char *) "bien_qu'") || !strcmp(w->form, (char *) "Bien_que") || !strcmp(w->form, (char *) "Bien_qu'")
	 || !strcmp(w->form, (char *) "ainsi_que") || !strcmp(w->form, (char *) "ainsi_qu'") || !strcmp(w->form, (char *) "Ainsi_que") || !strcmp(w->form, (char *) "Ainsi_qu'")
	 || !strcmp(w->form, (char *) "autant_que") || !strcmp(w->form, (char *) "autant_qu'") || !strcmp(w->form, (char *) "Autant_que") || !strcmp(w->form, (char *) "Autant_qu'")
	 || !strcmp(w->form, (char *) "alors_que") || !strcmp(w->form, (char *) "alors_qu'") || !strcmp(w->form, (char *) "Alors_que") || !strcmp(w->form, (char *) "Alors_qu'")
	 || !strcmp(w->form, (char *) "maintenant_que") || !strcmp(w->form, (char *) "mainenant_qu'") || !strcmp(w->form, (char *) "Maintenant_que") || !strcmp(w->form, (char *) "Mainenant_qu'")
	 || !strcmp(w->form, (char *) "encore_que") || !strcmp(w->form, (char *) "encore_qu'") || !strcmp(w->form, (char *) "Encore_que") || !strcmp(w->form, (char *) "Encore_qu'")
	 || !strcmp(w->form, (char *) "plus_que") || !strcmp(w->form, (char *) "plus_qu'") || !strcmp(w->form, (char *) "Plus_que") || !strcmp(w->form, (char *) "Plus_qu'")
	 || !strcmp(w->form, (char *) "tant_que") || !strcmp(w->form, (char *) "tant_qu'") || !strcmp(w->form, (char *) "Tant_que") || !strcmp(w->form, (char *) "Tant_qu'"))
	traite_ADV_que(s, w->form, i);
    }
  }
}
/*---------------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------*/

int chaine_est_un_chiffre(char *s)
{

  if(!strcmp(s, (char *) "et")) return 1;
  if(!strcmp(s, (char *) "zéro")) return 1;
  if(!strcmp(s, (char *) "un")) return 1;
  if(!strcmp(s, (char *) "deux")) return 1;
  if(!strcmp(s, (char *) "trois")) return 1;
  if(!strcmp(s, (char *) "quatre")) return 1;
  if(!strcmp(s, (char *) "cinq")) return 1;
  if(!strcmp(s, (char *) "six")) return 1;
  if(!strcmp(s, (char *) "sept")) return 1;
  if(!strcmp(s, (char *) "huit")) return 1;
  if(!strcmp(s, (char *) "neuf")) return 1;
  if(!strcmp(s, (char *) "dix")) return 1;
  if(!strcmp(s, (char *) "onze")) return 1;
  if(!strcmp(s, (char *) "douze")) return 1;
  if(!strcmp(s, (char *) "treize")) return 1;
  if(!strcmp(s, (char *) "quatorze")) return 1;
  if(!strcmp(s, (char *) "quinze")) return 1;
  if(!strcmp(s, (char *) "seize")) return 1;
  if(!strcmp(s, (char *) "vingt")) return 1;
  if(!strcmp(s, (char *) "vingts")) return 1;
  if(!strcmp(s, (char *) "trente")) return 1;
  if(!strcmp(s, (char *) "quarante")) return 1;
  if(!strcmp(s, (char *) "cinquante")) return 1;
  if(!strcmp(s, (char *) "soixante")) return 1;
  if(!strcmp(s, (char *) "cent")) return 1;
  if(!strcmp(s, (char *) "cents")) return 1;
  if(!strcmp(s, (char *) "mille")) return 1;
  if(!strcmp(s, (char *) "milles")) return 1;
  if(!strcmp(s, (char *) "million")) return 1;
  if(!strcmp(s, (char *) "millions")) return 1;
  if(!strcmp(s, (char *) "milliard")) return 1;
  if(!strcmp(s, (char *) "milliards")) return 1;
  return 0;
}


int chaine_est_un_chiffre_sauf_un(char *s)
{

  if(!strcmp(s, (char *) "zéro")) return 1;
  if(!strcmp(s, (char *) "deux")) return 1;
  if(!strcmp(s, (char *) "trois")) return 1;
  if(!strcmp(s, (char *) "quatre")) return 1;
  if(!strcmp(s, (char *) "cinq")) return 1;
  if(!strcmp(s, (char *) "six")) return 1;
  if(!strcmp(s, (char *) "sept")) return 1;
  if(!strcmp(s, (char *) "huit")) return 1;
  if(!strcmp(s, (char *) "neuf")) return 1;
  if(!strcmp(s, (char *) "dix")) return 1;
  if(!strcmp(s, (char *) "onze")) return 1;
  if(!strcmp(s, (char *) "douze")) return 1;
  if(!strcmp(s, (char *) "treize")) return 1;
  if(!strcmp(s, (char *) "quatorze")) return 1;
  if(!strcmp(s, (char *) "quinze")) return 1;
  if(!strcmp(s, (char *) "seize")) return 1;
  if(!strcmp(s, (char *) "vingt")) return 1;
  if(!strcmp(s, (char *) "trente")) return 1;
  if(!strcmp(s, (char *) "quarante")) return 1;
  if(!strcmp(s, (char *) "cinquante")) return 1;
  if(!strcmp(s, (char *) "soixante")) return 1;
  if(!strcmp(s, (char *) "cent")) return 1;
  if(!strcmp(s, (char *) "cents")) return 1;
  if(!strcmp(s, (char *) "mille")) return 1;
  if(!strcmp(s, (char *) "milles")) return 1;
  if(!strcmp(s, (char *) "million")) return 1;
  if(!strcmp(s, (char *) "millions")) return 1;
  if(!strcmp(s, (char *) "milliard")) return 1;
  if(!strcmp(s, (char *) "milliards")) return 1;
  return 0;
}



/*---------------------------------------------------------------------------------*/

int chaine_composee_de_digits(char *orig)
{
  int i;
  int l = strlen(orig);

  if(!strcmp(orig, (char *) ",")) return 0;

  for(i=0; i<l; i++)
    if(((orig[i] > '9') || (orig[i] < '0')) && (orig[i] != ','))
      return 0;
       
  return 1;
}
 

int chaine_est_un_nombre(char *orig)
{
  char *c, *s;
   /* printf("w = %s\n", s);  */

 
  if(chaine_composee_de_digits(orig)) return 1;
  if(chaine_est_un_chiffre_sauf_un(orig)) return 1;
  if(!chaine_possede_un_plus_ou_un_moins(orig)) return 0;
  if(!strcmp(orig, (char *) "-")) return 0;
  s = strdup(orig);
  for(c = strtok(s, (char *) "+-"); c; c = strtok(NULL, (char *) "+-")){
    if(!chaine_est_un_chiffre(c)){
      free(s);
      return 0;
    }
  }
  
  free(s);
  return 1;
}

/*---------------------------------------------------------------------------------*/

void orfeo_traite_nombres(conll_sentence *s)
{
  unsigned i;
  conll_word *w;

  for(i=1; i<s->l; i++){
    w = s->words[i];
    if(chaine_est_un_nombre(w->form)){
      /* printf("word = %s lemma = %s\n", w->form, w->lemma);  */
      strcpy(w->lemma, (char *) "NUM");
    }
  }
}


/*---------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------*/

void traite_amalgame_du(conll_sentence *s, int i)
{
  conll_word *w, *w2, *dep1;
  w = s->words[i];
  if((w->mother) && (w->daughters_nb > 0)){
    dep1 = w->daughters[0];
    strcpy(w->form, (char *) "de");
    strcpy(w->lemma, (char *) "de");
    w2 = conll_allocate_word(i, (char *) "le", (char *) "le", (char *) "DET", (char *) "DET", (char *) "NULL", 0, (char *) "SPEC");
    conll_add_word(s, w2, i+1, dep1);
  }
}

void traite_amalgame_des(conll_sentence *s, int i)
{
  conll_word *w, *w2, *dep1;
  w = s->words[i];
  if((w->mother) && (w->daughters_nb > 0)){
    dep1 = w->daughters[0];
    strcpy(w->form, (char *) "de");
    strcpy(w->lemma, (char *) "de");
    w2 = conll_allocate_word(i, (char *) "les", (char *) "le", (char *) "DET", (char *) "DET", (char *) "NULL", 0, (char *) "SPEC");
    conll_add_word(s, w2, i+1, dep1);
  }
}

void traite_amalgame_au(conll_sentence *s, int i)
{
  conll_word *w, *w2, *dep1;

  w = s->words[i];
  if((w->mother) && (w->daughters_nb > 0)){
    dep1 = w->daughters[0];
    strcpy(w->form, (char *) "à");
    strcpy(w->lemma, (char *) "à");
    w2 = conll_allocate_word(i, (char *) "le", (char *) "le", (char *) "DET", (char *) "DET", (char *) "NULL", 0, (char *) "SPEC");
    conll_add_word(s, w2, i+1, dep1);
  }
}

void traite_amalgame_aux(conll_sentence *s, int i)
{
  conll_word *w, *w2, *dep1;
  w = s->words[i];
  if((w->mother) && (w->daughters_nb > 0)){
    dep1 = w->daughters[0];
    strcpy(w->form, (char *) "à");
    strcpy(w->lemma, (char *) "à");
    w2 = conll_allocate_word(i, (char *) "les", (char *) "le", (char *) "DET", (char *) "DET", (char *) "NULL", 0, (char *) "SPEC");
    conll_add_word(s, w2, i+1, dep1);
  }
}

void traite_amalgame_auquel(conll_sentence *s, int i)
{
  conll_word *w, *w2, *dep1;
  w = s->words[i];
  if((w->mother) && (w->daughters_nb > 0)){
    dep1 = w->daughters[0];
    strcpy(w->form, (char *) "à");
    strcpy(w->lemma, (char *) "à");
    w2 = conll_allocate_word(i, (char *) "lequel", (char *) "lequel", (char *) "PRQ", (char *) "PRQ", (char *) "NULL", 0, (char *) "PRQ");
    conll_add_word(s, w2, i+1, dep1);
  }
}

void traite_amalgame_auxquels(conll_sentence *s, int i)
{
  conll_word *w, *w2, *dep1;
  w = s->words[i];
  if((w->mother) && (w->daughters_nb > 0)){
    dep1 = w->daughters[0];
    strcpy(w->form, (char *) "à");
    strcpy(w->lemma, (char *) "à");
    w2 = conll_allocate_word(i, (char *) "lesquels", (char *) "lequel", (char *) "PRQ", (char *) "PRQ", (char *) "NULL", 0, (char *) "PRQ");
    conll_add_word(s, w2, i+1, dep1);
  }
}

void traite_amalgame_auxquelles(conll_sentence *s, int i)
{
  conll_word *w, *w2, *dep1;
  w = s->words[i];
  if((w->mother) && (w->daughters_nb > 0)){
    dep1 = w->daughters[0];
    strcpy(w->form, (char *) "à");
    strcpy(w->lemma, (char *) "à");
    w2 = conll_allocate_word(i, (char *) "lesquelles", (char *) "lequel", (char *) "PRQ", (char *) "PRQ", (char *) "NULL", 0, (char *) "PRQ");
    conll_add_word(s, w2, i+1, dep1);
  }
}

void traite_amalgame_duquel(conll_sentence *s, int i)
{
  conll_word *w, *w2, *dep1;
  w = s->words[i];
  if((w->mother) && (w->daughters_nb > 0)){
    dep1 = w->daughters[0];
    strcpy(w->form, (char *) "de");
    strcpy(w->lemma, (char *) "de");
    w2 = conll_allocate_word(i, (char *) "lequel", (char *) "lequel", (char *) "PRQ", (char *) "PRQ", (char *) "NULL", 0, (char *) "PRQ");
    conll_add_word(s, w2, i+1, dep1);
  }
}

void traite_amalgame_desquels(conll_sentence *s, int i)
{
  conll_word *w, *w2, *dep1;
  w = s->words[i];
  if((w->mother) && (w->daughters_nb > 0)){
    dep1 = w->daughters[0];
    strcpy(w->form, (char *) "de");
    strcpy(w->lemma, (char *) "de");
    w2 = conll_allocate_word(i, (char *) "lesquels", (char *) "lequel", (char *) "PRQ", (char *) "PRQ", (char *) "NULL", 0, (char *) "PRQ");
    conll_add_word(s, w2, i+1, dep1);
  }
}

void traite_amalgame_desquelles(conll_sentence *s, int i)
{
  conll_word *w, *w2, *dep1;
  w = s->words[i];
  if((w->mother) && (w->daughters_nb > 0)){
    dep1 = w->daughters[0];
    strcpy(w->form, (char *) "de");
    strcpy(w->lemma, (char *) "de");
    w2 = conll_allocate_word(i, (char *) "lesquelles", (char *) "lequel", (char *) "PRQ", (char *) "PRQ", (char *) "NULL", 0, (char *) "PRQ");
    conll_add_word(s, w2, i+1, dep1);
  }
}

void orfeo_traite_amalgames(conll_sentence *s)
{
  unsigned i;
  conll_word *w;

  for(i=1; i<s->l; i++){
    /*        printf("************ l = %d\n", s->l);
	      printf("************ i = %d form = %s\n", i, w->form);*/
    w = s->words[i];
    if(!strcmp(w->form, (char *) "du")) traite_amalgame_du(s, i);
    else if(!strcmp(w->form, (char *) "des")) traite_amalgame_des(s, i);
    else if(!strcmp(w->form, (char *) "au")) traite_amalgame_au(s, i);
    else if(!strcmp(w->form, (char *) "aux")) traite_amalgame_aux(s, i);
    else if(!strcmp(w->form, (char *) "auquel")) traite_amalgame_auquel(s, i);
    else if(!strcmp(w->form, (char *) "auxquels")) traite_amalgame_auxquels(s, i);
    else if(!strcmp(w->form, (char *) "auxquelles")) traite_amalgame_auxquelles(s, i);
    else if(!strcmp(w->form, (char *) "duquel")) traite_amalgame_duquel(s, i);
    else if(!strcmp(w->form, (char *) "desquels")) traite_amalgame_desquels(s, i);
    else if(!strcmp(w->form, (char *) "desquelles")) traite_amalgame_desquelles(s, i);


  }
}










