#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "conll_lib.h"
#include "ftb_lib.h"

void ftb_change_root_head(conll_sentence *s, int new_head_index)
{
  unsigned i;
  conll_word *w;
  
  for(i=1; i<s->l; i++){
    w = s->words[i];
    if(!strcmp(w->deprel, "root")){
      w->head = new_head_index;
      break;
    }
  }
}

int ftb_get_root_index(conll_sentence *s)
{
  unsigned i;
  conll_word *w;
  
  for(i=1; i<s->l; i++){
    w = s->words[i];
    if(!strcmp(w->deprel, "root"))
      return i;
  }
  return -1;
}


int ftb_sentence_ends_with_poncts(conll_sentence *s)
{
  conll_word *w;
  
  w = s->words[s->l-1];
  if(!strcmp(w->postag, "PONCT") && !strcmp(w->feats, "s=s"))
    return 1;
  return 0;
}

int ftb_number_of_roots_in_sentence(conll_sentence *s)
{
  unsigned i;
  conll_word *w;
  int root_nb = 0;

  for(i=1; i<s->l; i++){
    w = s->words[i];
    if(!strcmp(w->deprel, "root"))
      root_nb++;
  }
  return root_nb;
}

int ftb_sentence_contains_missinghead(conll_sentence *s)
{
  unsigned i;
  conll_word *w;

  for(i=1; i<s->l; i++){
    w = s->words[i];
    if(!strcmp(w->deprel, "missinghead"))
      return 1;
  }
  return 0;
}


void ftb_change_form_and_lemma_of_numbers(conll_sentence *s)
{
  unsigned i;
  conll_word *w;

  for(i=1; i<s->l; i++){
    w = s->words[i];
    if(conll_is_num(w->form)){
      strcpy(w->form, "_NUM_");
      strcpy(w->lemma, "_NUM_");
    }
    
  }
}

void ftb_change_label_of_last_dep(conll_sentence *s)
{
  if(strcmp(s->words[s->l - 1]->deprel, "root"))
    strcpy(s->words[s->l - 1]->deprel, "eos");
}

/*---------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------*/

void ftb_retokenize_three_dots(conll_sentence *s)
{
  unsigned i;
  conll_word *w;
  unsigned l = s->l;
  for(i=1; i < l-2; i++){
    w = s->words[i];
    if(w){
      if(!strcmp(s->words[i]->form, ".") && !strcmp(s->words[i+1]->form, ".") && !strcmp(s->words[i+2]->form, ".")){
	strcpy(s->words[i]->form, "...");
	strcpy(s->words[i]->lemma, "...");
	conll_remove_word_rec(s, i+1);
	conll_remove_word_rec(s, i+2);
	/* fprintf(stderr, "retokenize ...\n"); */
      }
    }
  }
  conll_compact_sentence(s);
}

/*---------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------*/

void ftb_tokenize_dot(conll_sentence *s, char *gov_postag, char *dep_postag, char *label)
{
  unsigned i;
  conll_word *w;

  for(i=1; i<s->l; i++){
    w = s->words[i];
    /* printf("form = %s\n", w->form); */
    if((strlen(w->form) > 1)
       && (strcmp(w->form, "..."))
       && (w->form[strlen(w->form) - 1] == '.')){
      conll_word *abbrev = conll_copy_word(w);
      abbrev->form[strlen(abbrev->form) - 1] = '\0';
      strcpy(abbrev->postag, gov_postag); /*titre*/
      strcpy(abbrev->cpostag, gov_postag);
      if(w->lemma[strlen(w->lemma) - 1] == '.') abbrev->lemma[strlen(abbrev->lemma) - 1] = '\0';
      /* conll_word *dot = allocate_word(i, ".", ".", "poncts", "poncts", "NULL", -1, "abbrev"); */
      conll_word *dot = conll_allocate_word(i, (char *) ".", (char *) ".", dep_postag, dep_postag, (char *) "NULL", -1, label);

      conll_split_node_in_two(s, i, abbrev, dot, i, i+1);

    }

  }
}


void ftb_print_sentence_no_newline(conll_sentence *s)
{
  unsigned i;
  conll_word *w;

  if((s->l == 1) || (s->l == 0)) return;

  for(i=1; i<s->l; i++){
    w = s->words[i];
    fprintf(stdout, "%s", w->form);
    fprintf(stdout, "\t%s", w->postag);
    fprintf(stdout, "\t%s", w->feats);
    fprintf(stdout, "\t%s", w->lemma);
    fprintf(stdout, "\t%d", w->head);
    fprintf(stdout, "\t%s", w->deprel);
    if(i == s->l -  1) 
      fprintf(stdout, "\t1");
    else
      fprintf(stdout, "\t0");
    fprintf(stdout, "\n");
  }

}
