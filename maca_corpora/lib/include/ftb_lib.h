/*******************************************************************************
    Copyright (C) 2010 by Alexis Nasr <alexis.nasr@lif.univ-mrs.fr>
                      and Joseph Le Roux <joseph.le.roux@gmail.com>
    conll_lib is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    conll_lib is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with conll_lib. If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#ifndef __FTB_LIB__
#define __FTB_LIB__

#include <stdio.h>
#include <stdlib.h>

#include "conll_lib.h"
#include "ftb_lib.h"

int ftb_sentence_ends_with_poncts(conll_sentence *s);
int ftb_number_of_roots_in_sentence(conll_sentence *s);
int ftb_sentence_contains_missinghead(conll_sentence *s);
void ftb_change_form_and_lemma_of_numbers(conll_sentence *s);
void ftb_change_label_of_last_dep(conll_sentence *s);
void ftb_retokenize_three_dots(conll_sentence *s);
void ftb_tokenize_dot(conll_sentence *s, char *gov_postag, char *dep_postag, char *label);
void ftb_print_sentence_no_newline(conll_sentence *s);
int ftb_get_root_index(conll_sentence *s);
void ftb_change_root_head(conll_sentence *s, int new_head_index);
void ftb_compute_relative_index_of_heads(conll_sentence *s);
#endif
