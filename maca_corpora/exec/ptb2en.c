#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<strings.h>
#include<math.h>
#include<getopt.h>
#include"conll_lib.h" 
#include"hash_str.h" 

typedef struct options
{
  FILE * fd_parses;                    // parser output
  int verbose_level;
  int snum;
  char *filename;
  hash_str *h_pos;
  hash_str *h_fct;
} options;

void tokenize_dot_ptb(conll_sentence *s, char *dep_postag, char *label)
{
  unsigned i;
  conll_word *w;

  for(i=1; i<s->l; i++){
    w = s->words[i];
    /* printf("form = %s\n", w->form); */
    if((strlen(w->form) > 1)
       && (strcmp(w->form, "..."))
       && (w->form[strlen(w->form) - 1] == '.')){
      conll_word *abbrev = conll_copy_word(w);
      abbrev->form[strlen(abbrev->form) - 1] = '\0';
      //      strcpy(abbrev->postag, gov_postag); 
      // strcpy(abbrev->cpostag, gov_postag);
      if(w->lemma[strlen(w->lemma) - 1] == '.') abbrev->lemma[strlen(abbrev->lemma) - 1] = '\0';
      /* conll_word *dot = conll_allocate_word(i, ".", ".", "poncts", "poncts", "NULL", -1, "abbrev"); */
      conll_word *dot = conll_allocate_word(i, (char *) ".", (char *) ".", dep_postag, dep_postag, (char *) "_", -1, label);

      conll_split_node_in_two(s, i, abbrev, dot, i, i+1);

    }

  }
}

void change_pos_and_cpos_of_dot(conll_sentence *s, options *op)
{
  unsigned i;
  conll_word *w;

  for(i=1; i < s->l; i++){
    w = s->words[i];
    if(w){
      if(!strcmp(s->words[i]->form, ".")){
	strcpy(s->words[i]->postag, ".");
	strcpy(s->words[i]->cpostag, ".");
      }
    }
  }
}

void print_sentence_no_newline_en(conll_sentence *s)
{
  unsigned i;
  conll_word *w;

  if((s->l == 1) || (s->l == 0)) return;

  for(i=1; i<s->l; i++){
    w = s->words[i];
    /* fprintf(stdout, "%d", w->id); */
    /* fprintf(stdout, "\t%s", w->form); */
    fprintf(stdout, "%s", w->form);
    fprintf(stdout, "\t%s", w->postag);
    fprintf(stdout, "\t%s", w->postag);
    fprintf(stdout, "\t%s", w->lemma);
    /* fprintf(stdout, "\t%s", w->cpostag); */

    fprintf(stdout, "\t%d", w->head);

    /*    if(w->mother == NULL)
      fprintf(stdout, "\t0");
    else
    fprintf(stdout, "\t%d", w->mother->id - w->id);*/
    /*
    else{
      if(strcmp(w->deprel, "root"))
	fprintf(stdout, "\t%d", w->mother->id - w->id);
      else
	fprintf(stdout, "\t%d", w->mother->id - w->id);
        fprintf(stdout, "\t%d", 0);
	}*/
    fprintf(stdout, "\t%s", w->deprel);
    /* if(!strcmp(w->deprel, "eos")) */
    /* if(!strcmp(w->deprel, "ponct") && !strcmp(w->postag, "poncts")) */
     if(i == s->l -  1) 
       fprintf(stdout, "\t1");
    else
      fprintf(stdout, "\t0");
      

    fprintf(stdout, "\n");

    /* fprintf(stdout, "\t_\t_\n"); */
    
  }

}


/*---------------------------------------------------------------------------------*/

options op;

void print_options(options *op)
{
  fprintf(stderr, "file name = %s\n", op->filename);
  fprintf(stderr, "verbose level = %d\n", op->verbose_level);
  fprintf(stderr, "maximum number of sentences to process = %d\n", op->snum);
}

void reset_options(options * op)
{
  op->filename = NULL;
  op->fd_parses = NULL;
  op->verbose_level = 0;
  op->snum = 100000000;
  op->h_pos =  hash_str_new(100);

  hash_str_add(op->h_pos, strdup("ADJ"), strdup("adj"));
  hash_str_add(op->h_pos, strdup("ADJWH"), strdup("adj"));
  hash_str_add(op->h_pos, strdup("ADV"), strdup("adv"));
  hash_str_add(op->h_pos, strdup("ADVWH"), strdup("adv"));
  hash_str_add(op->h_pos, strdup("CC"), strdup("coo"));
  hash_str_add(op->h_pos, strdup("CLO"), strdup("clo"));
  hash_str_add(op->h_pos, strdup("CLR"), strdup("clr"));
  hash_str_add(op->h_pos, strdup("CLS"), strdup("cln"));
  hash_str_add(op->h_pos, strdup("CS"), strdup("csu"));
  hash_str_add(op->h_pos, strdup("DET"), strdup("det"));
  hash_str_add(op->h_pos, strdup("DETWH"), strdup("det"));
  hash_str_add(op->h_pos, strdup("ET"), strdup("etr"));
  hash_str_add(op->h_pos, strdup("I"), strdup("pres"));
  hash_str_add(op->h_pos, strdup("NC"), strdup("nc"));
  hash_str_add(op->h_pos, strdup("NPP"), strdup("np"));
  hash_str_add(op->h_pos, strdup("P"), strdup("prep"));
  hash_str_add(op->h_pos, strdup("P+D"), strdup("prep"));
  hash_str_add(op->h_pos, strdup("PONCT"), strdup(""));
  hash_str_add(op->h_pos, strdup("P+PRO"), strdup("prep"));
  hash_str_add(op->h_pos, strdup("PREF"), strdup("pref"));
  hash_str_add(op->h_pos, strdup("PRO"), strdup("pro"));
  hash_str_add(op->h_pos, strdup("PROREL"), strdup("prorel"));
  hash_str_add(op->h_pos, strdup("PROWH"), strdup("pri"));
  hash_str_add(op->h_pos, strdup("V"), strdup("v"));
  hash_str_add(op->h_pos, strdup("VIMP"), strdup("v"));
  hash_str_add(op->h_pos, strdup("VINF"), strdup("vinf"));
  hash_str_add(op->h_pos, strdup("VPP"), strdup("vppart"));
  hash_str_add(op->h_pos, strdup("VPR"), strdup("vprespart"));
  hash_str_add(op->h_pos, strdup("VS"), strdup("v"));

  op->h_fct =  hash_str_new(100);

  hash_str_add(op->h_fct, strdup("aff"), strdup("aff"));
  hash_str_add(op->h_fct, strdup("a_obj"), strdup("a_obj"));
  hash_str_add(op->h_fct, strdup("arg"), strdup("arg"));
  hash_str_add(op->h_fct, strdup("ato"), strdup("ato"));
  hash_str_add(op->h_fct, strdup("ats"), strdup("ats"));
  hash_str_add(op->h_fct, strdup("aux_caus"), strdup("aux_caus"));
  hash_str_add(op->h_fct, strdup("aux_pass"), strdup("aux_pass"));
  hash_str_add(op->h_fct, strdup("aux_tps"), strdup("aux_tps"));
  hash_str_add(op->h_fct, strdup("comp"), strdup("comp"));
  hash_str_add(op->h_fct, strdup("coord"), strdup("coord"));
  hash_str_add(op->h_fct, strdup("de_obj"), strdup("de_obj"));
  hash_str_add(op->h_fct, strdup("dep"), strdup("dep"));
  hash_str_add(op->h_fct, strdup("dep_coord"), strdup("dep_coord"));
  hash_str_add(op->h_fct, strdup("det"), strdup("det"));
  hash_str_add(op->h_fct, strdup("missinghead"), strdup("missinghead"));
  hash_str_add(op->h_fct, strdup("mod"), strdup("mod"));
  hash_str_add(op->h_fct, strdup("mod_rel"), strdup("mod_rel"));
  hash_str_add(op->h_fct, strdup("obj"), strdup("obj"));
  hash_str_add(op->h_fct, strdup("obj1"), strdup("obj"));
  hash_str_add(op->h_fct, strdup("p_obj"), strdup("p_obj"));
  hash_str_add(op->h_fct, strdup("ponct"), strdup("ponct"));
  hash_str_add(op->h_fct, strdup("root"), strdup("root"));
  hash_str_add(op->h_fct, strdup("suj"), strdup("suj"));
}

/*---------------------------------------------------------------------------------*/
void  print_help_message(char *program_name)
{
  fprintf(stderr, "%s usage: %s [options]\n", program_name, program_name);
  fprintf(stderr, "OPTIONS :\n");
  fprintf(stderr, "      -f <file>     : hypothesis conll file\n");
  fprintf(stderr, "      -n <int>      : process n sentences (default is 100 000 000)\n");
  fprintf(stderr, "      -v 1|2|3      : verbosity level\n");
  fprintf(stderr, "      -h            : print this message\n");
}

/*---------------------------------------------------------------------------------*/

void parse_options(int argc, char *argv[], options * op)
{
  char c;

  reset_options(op);

  if(argc ==1){
    print_help_message(argv[0]);
    exit(1);
  }
  
  while ((c = getopt (argc, argv, "hf:n:v:")) != -1)
    switch (c)
      {
      case 'h':
	print_help_message(argv[0]);
	exit(0);
      case 'f':
	op->filename = strdup(optarg);
	if((op->fd_parses = fopen(op->filename, "r")) == NULL){
	  fprintf(stderr, "cannot open hypothesis file %s : aborting\n", op->filename);
	  exit(1);
	}
	break;
      case 'n':
	op->snum = atoi(optarg);
	break;
      case 'v':
	op->verbose_level = atoi(optarg);
	break;
      }
  
  if (op->fd_parses == NULL){
    fprintf(stderr, "error : cannot open parse file: aborting\n");
    exit(1);
  }
}


/*---------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------*/
int main(int argc, char *argv[])
{
  conll_sentence *s = conll_allocate_sentence();
  int snum = 0;
  int res;
  
  parse_options(argc, argv, &op);
  print_options(&op); 
  for(res = conll_load_sentence(op.fd_parses, s); res && (snum < op.snum); res = conll_load_sentence(op.fd_parses, s)){
    s->num = snum;

    /* if(s->l > 200) continue; */
    //    if(!sentence_ends_with_poncts(s)) continue;
    //    if(number_of_roots_in_sentence(s) != 1) continue;
    //if(sentence_contains_missinghead(s)) continue;

    snum++;
    
     change_pos_and_cpos_of_dot(s, &op); 
    //change_form_and_lemma_of_numbers(s);
    //change_pos_fr(s, op.h_pos);
      /* change_label_of_last_dep(s);    */
    //retokenize_three_dots(s);
     tokenize_dot_ptb(s, (char *) ".", (char *)"ABBREV");
    conll_renumber_sentence(s);
    //    conll_compute_relative_index_of_heads(s);
    //    print_sentence_no_newline_en(s);
    conll_print_sentence(s);
    
  }
  fprintf(stderr, "\n");
  fclose(op.fd_parses);
  conll_free_sentence(s);
  return 0;
}
