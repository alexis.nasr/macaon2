%{
#include<stdio.h>
#include"maca_tokenizer_functions_for_lex.h"

extern int defait_amalgames;
/*extern int print_offset;
extern int print_token_length;*/
extern int defait_amalgames;
extern int offset;
extern int token_length;
extern char *token;



%}
separ [ \t\n]
nosepar [^ \t\n]

%option prefix="fr"
/*%option outfile="fr_lex.c"*/

/*%option noyywrap*/
%s state_defait_amalgames

%%
	if(defait_amalgames){
	BEGIN(state_defait_amalgames);
        }
#.*    {ECHO; printf("\n");}
\<[^\>]*\>   {maca_tokenizer_segment((char *)"", yytext);}
{separ}+     {maca_tokenizer_segment((char *)"", yytext);}
\.   {maca_tokenizer_segment((char *)".", yytext);}
\?   {maca_tokenizer_segment((char *)"?", yytext);}
\!   {maca_tokenizer_segment((char *)"!", yytext);}
,    {maca_tokenizer_segment((char *)",", yytext);}
:    {maca_tokenizer_segment((char *)":", yytext);}
;    {maca_tokenizer_segment((char *)";", yytext);}
…    {maca_tokenizer_segment((char *)"…", yytext);}
\)   {maca_tokenizer_segment((char *)")", yytext);}
»    {maca_tokenizer_segment((char *)"»", yytext);}
\(           {maca_tokenizer_segment((char *)"(", yytext);}
\"           {maca_tokenizer_segment((char *)"\"", yytext);}
«            {maca_tokenizer_segment((char *)"«", yytext);}

{nosepar}*'   {maca_tokenizer_segment((char *)yytext, yytext);}
{nosepar}*’   {maca_tokenizer_segment((char *)yytext, yytext);}

[0-9]+,[0-9]+ {maca_tokenizer_segment(yytext, yytext);}

-je      {maca_tokenizer_segment((char *)"-je", yytext);}
-tu      {maca_tokenizer_segment((char *)"-tu", yytext);}
-on      {maca_tokenizer_segment((char *)"-on", yytext);}
-ce      {maca_tokenizer_segment((char *)"-ce", yytext);}
-t-il    {maca_tokenizer_segment((char *)"-t-il", yytext);}
-il      {maca_tokenizer_segment((char *)"-il", yytext);}
-t-ils   {maca_tokenizer_segment((char *)"-t-ils", yytext);}
-ils     {maca_tokenizer_segment((char *)"-ils", yytext);}
-t-elle  {maca_tokenizer_segment((char *)"-t-elle", yytext);}
-elle    {maca_tokenizer_segment((char *)"-elle", yytext);}
-t-elles {maca_tokenizer_segment((char *)"-t-elles", yytext);}
-elles   {maca_tokenizer_segment((char *)"-elles", yytext);}
-là      {maca_tokenizer_segment((char *)"-là", yytext);}
.        {maca_tokenizer_add_char_to_token(yytext[0]);}

<state_defait_amalgames>{
" du " printf("\nde\nle\n");
" des " printf("\nde\nles\n");
" au " printf("\nà\nle\n");
" aux " printf("\nà\nles\n");
}
%%

int frwrap(void)
{
maca_tokenizer_segment((char *)"", (char *)"");
return 1;
}
