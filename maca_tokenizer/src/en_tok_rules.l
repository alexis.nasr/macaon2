%{
#include<stdio.h>
#include"maca_tokenizer_functions_for_lex.h"
extern int defait_amalgames;
extern int offset;
extern int token_length;
extern char *token;
%}

%option prefix="en"

/*%option noyywrap*/
%%

#.*    ECHO;
\<[^\>]*\> {maca_tokenizer_segment((char *)"", yytext);}
[ \t]+   {maca_tokenizer_segment((char *)"", yytext);}
[ ]*\.   {maca_tokenizer_segment((char *)".", yytext);}
[ ]*\?   {maca_tokenizer_segment((char *)"?", yytext);}
[ ]*\!   {maca_tokenizer_segment((char *)"!", yytext);}
[ ]*,    {maca_tokenizer_segment((char *)",", yytext);}
[ ]*:    {maca_tokenizer_segment((char *)":", yytext);}
[ ]*;    {maca_tokenizer_segment((char *)";", yytext);}
[ ]*…    {maca_tokenizer_segment((char *)"…", yytext);}
[ ]*\)   {maca_tokenizer_segment((char *)")", yytext);}
[ ]*»    {maca_tokenizer_segment((char *)"»", yytext);}
\(       {maca_tokenizer_segment((char *)"((", yytext);}
\"	 {maca_tokenizer_segment((char *)"\"", yytext);}
«	 {maca_tokenizer_segment((char *)"«", yytext);}

[0-9]+\.[0-9]+ {maca_tokenizer_segment(yytext, yytext);}

it's printf("it\nis\n");
isn't printf("is\nnot\n");
don’t printf("do\nnot");
doesn't printf("does\nnot");
doesn’t printf("does\nnot");
won't printf("will\nnot");
won’t printf("will\nnot");
cannot printf("can\nnot");
wanna printf("want\nto");
's      {maca_tokenizer_segment((char *)"'s", yytext);}
’s      {maca_tokenizer_segment((char *)"’s", yytext);}
\n+ printf("\n");

.        {maca_tokenizer_add_char_to_token(yytext[0]);}

%%

int enwrap(void)
{
maca_tokenizer_segment((char *)"", (char *)"");
return 1;
}
