#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"context.h"

int enlex(void);
int frlex(void);

int defait_amalgames = 0;
int print_offset = 0;
int print_token_length = 0;

int offset = 0;
int token_length = 0;
char token[10000];


void maca_tokenizer_help_message(context *ctx)
{
  context_general_help_message(ctx);
  fprintf(stderr, "INPUT\n");
  context_input_help_message(ctx);
  context_mcd_help_message(ctx);
  context_language_help_message(ctx);
}

void maca_tokenizer_check_options(context *ctx){
  if(ctx->help){
    maca_tokenizer_help_message(ctx);
    exit(1);
  }
}


int main(int argc, char* argv[])
{
  
  context *ctx;

  ctx = context_read_options(argc, argv);
  maca_tokenizer_check_options(ctx);

  print_offset = ctx->print_offset;
  print_token_length = ctx->print_token_length;
  
  if(!strcmp(ctx->language, "en"))
    enlex() ; 
  else
   frlex() ;  

       
  /* if(argc > 1) defait_amalgames = 1; */

  return 0;
}

