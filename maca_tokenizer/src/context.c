#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<unistd.h>
#include<getopt.h>
#include "context.h"
#include "util.h"



void context_free(context *ctx)
{
  if(ctx->program_name) free(ctx->program_name);
  if(ctx->input_filename) free(ctx->input_filename);
  if(ctx->output_filename) free(ctx->output_filename);
  if(ctx->language) free(ctx->language);
  if(ctx->maca_data_path) free(ctx->maca_data_path);
  free(ctx);
}

context *context_new(void)
{
  context *ctx = (context *)memalloc(sizeof(context));
  
  ctx->help = 0;
  ctx->verbose = 0;
  ctx->debug_mode = 0;
  ctx->program_name = NULL;
  ctx->mcd_filename = NULL;
  ctx->mcd_struct = NULL;
  ctx->language = strdup("fr");
  ctx->maca_data_path = NULL;
  ctx->input_filename = NULL;
  ctx->output_filename = NULL;
  ctx->print_offset = 0;
  ctx->print_token_length = 0;
  return ctx;
}

void context_input_help_message(context *ctx){
  fprintf(stderr, "\t-i --input  <file>  : input mcf file name\n");
}

void context_mcd_help_message(context *ctx){
  fprintf(stderr, "\t-C --mcd   <file> : multi column description file name\n");
}

void context_language_help_message(context *ctx){
  fprintf(stderr, "\t-L --language  : identifier of the language to use\n");
}

void context_print_offset_message(context *ctx){
  fprintf(stderr, "\t-p --print_offset  : print offset and token length\n");
}

void context_general_help_message(context *ctx)
{
    fprintf(stderr, "usage: %s [options]\n", ctx->program_name);
    fprintf(stderr, "Options:\n");
    fprintf(stderr, "\t-h --help             : print this message\n");
    fprintf(stderr, "\t-v --verbose          : activate verbose mode\n");
    fprintf(stderr, "\t-r --hratio   <float> : set the occupation ratio of hash tables (default is 0.5)\n");
    context_print_offset_message(ctx);
}



context *context_read_options(int argc, char *argv[])
{
  int c;
  int option_index = 0;
  context *ctx = context_new();

  ctx->program_name = strdup(argv[0]);

  static struct option long_options[9] =
    {
      {"help",                no_argument,       0, 'h'},
      {"verbose",             no_argument,       0, 'v'},
      {"debug",               no_argument,       0, 'd'},
      {"print_offset",        no_argument,       0, 'p'},
      {"input",               required_argument, 0, 'i'},
      {"output",              required_argument, 0, 'o'},
      {"mcd",                 required_argument, 0, 'C'}, 
      {"language",            required_argument, 0, 'L'},
      {"maca_data_path",      required_argument, 0, 'D'}
    };
  optind = 0;
  opterr = 0;
  
  while ((c = getopt_long (argc, argv, "hvdpi:o:C:L:D:", long_options, &option_index)) != -1){ 
    switch (c)
      {
      case 'd':
	ctx->debug_mode = 1;
	break;
      case 'h':
	ctx->help = 1;
	break;
      case 'v':
	ctx->verbose = 1;
	break;
      case 'i':
	ctx->input_filename = strdup(optarg);
	break;
      case 'o':
	ctx->output_filename = strdup(optarg);
	break;
      case 'C':
	ctx->mcd_filename = strdup(optarg);
	break;
      case 'L':
	ctx->language = strdup(optarg);
	break;
      case 'D':
	ctx->maca_data_path = strdup(optarg);
	break;
      case 'p':
	ctx->print_offset = 1;
	ctx->print_token_length = 1;
	break;
      }
  }

  if(ctx->mcd_filename)
    ctx->mcd_struct = mcd_read(ctx->mcd_filename, ctx->verbose);

  /*
  if(ctx->mcd_filename == NULL)
    ctx->mcd_struct = mcd_build_wplgf();
  */
  return ctx;
}

