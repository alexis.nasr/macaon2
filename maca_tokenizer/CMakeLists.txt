FLEX_TARGET(fr_tok_rules ./src/fr_tok_rules.l ${CMAKE_CURRENT_BINARY_DIR}/fr_lex.c)
FLEX_TARGET(en_tok_rules ./src/en_tok_rules.l ${CMAKE_CURRENT_BINARY_DIR}/en_lex.c)

set(SOURCES ./src/context.c
  ./src/maca_tokenizer_functions_for_lex.c
  ${FLEX_fr_tok_rules_OUTPUTS}
  ${FLEX_en_tok_rules_OUTPUTS})

##compiling library
include_directories(./src)

add_library(maca_tokenizer_lib STATIC ${SOURCES})

#compiling, linking and installing executables

include_directories(${CMAKE_CURRENT_BINARY_DIR})
add_executable(maca_tokenizer ./src/maca_tokenizer.c)
target_link_libraries(maca_tokenizer maca_tokenizer_lib maca_common)
install (TARGETS maca_tokenizer DESTINATION bin)
