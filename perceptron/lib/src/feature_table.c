#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>
#include"feature_table.h"
#include"util.h"

feature_table *feature_table_load(char *filename, int verbose)
{
  int i;
  feature_table *ft = NULL;
  int features_nb;
  int classes_nb;
  FILE *f = fopen(filename, "r");
  if(f == NULL){
    fprintf(stderr, "cannot open file %s\n", filename);
    exit(1);
  }
  fread(&features_nb, sizeof(int), 1, f);
  if(verbose)fprintf(stderr, "features_nb = %d\n", features_nb);
  fread(&classes_nb, sizeof(int), 1, f);
  if(verbose)fprintf(stderr, "classes_nb = %d\n", classes_nb);

  ft = (feature_table *)memalloc(sizeof(feature_table));
  ft->features_nb = features_nb;
  ft->classes_nb = classes_nb;
  ft->table = (float **)memalloc(features_nb * sizeof(float *));
  for(i=0; i< features_nb; i++){
    ft->table[i] = (float *)memalloc(classes_nb * sizeof(float));
    fread(ft->table[i], sizeof(float), classes_nb, f);
  }
  fclose(f);
  return ft;
}

void feature_table_dump(char *filename, feature_table *ft)
{
  int i;
  FILE *f;
  int features_nb = ft->features_nb;
  int classes_nb = ft->classes_nb;
  if(filename == NULL){
    f = stdout;
  }
  else{
    f= fopen(filename, "w");
    if(f == NULL){
      fprintf(stderr, "cannot open file %s\n", filename);
      exit(1);
    }
  }
  fwrite(&features_nb, sizeof(int), 1, f);
  fwrite(&classes_nb, sizeof(int), 1, f);
  for(i=0; i< ft->features_nb; i++)
    fwrite(ft->table[i], sizeof(float), ft->classes_nb, f);

  fclose(f);
}

void feature_table_print(char *filename, feature_table *ft)
{
  int i,j;
  FILE *f;
  if(filename == NULL){
    f = stdout;
  }
  else{
    f= fopen(filename, "w");
    if(f == NULL){
      fprintf(stderr, "cannot open file %s\n", filename);
      exit(1);
    }
  }
  fprintf(f, "features_nb = %d\n", ft->features_nb);
  fprintf(f, "classes_nb = %d\n", ft->classes_nb);
  for(i=0; i< ft->features_nb; i++){
    fprintf(f, "%d", i);
    for(j=0; j< ft->classes_nb;j++){
      fprintf(f, " %f", ft->table[i][j]);
    }
    fprintf(f, "\n");
  }
  fclose(f);
}

void feature_table_print_verbose(char *filename, feature_table *ft, dico *dico_features, dico *dico_classes)
{
  int i,j;
  FILE *f;
  if(filename == NULL){
    f = stdout;
  }
  else{
    f= fopen(filename, "w");
    if(f == NULL){
      fprintf(stderr, "cannot open file %s\n", filename);
      exit(1);
    }
  }
  fprintf(f, "features_nb = %d\n", ft->features_nb);
  fprintf(f, "classes_nb = %d\n", ft->classes_nb);
  for(i=0; i< ft->features_nb; i++){
    fprintf(f, "%s", dico_int2string(dico_features, i));
    for(j=0; j< ft->classes_nb;j++){
      if(ft->table[i][j] != 0)
      fprintf(f, " %s:%f", dico_int2string(dico_classes, j), ft->table[i][j]);
    }
    fprintf(f, "\n");
  }
  fclose(f);
}


feature_table *feature_table_new(int features_nb, int classes_nb)
{
  int i,j;
  feature_table *ft = (feature_table *)memalloc(sizeof(feature_table));
  ft->features_nb = features_nb;
  ft->classes_nb = classes_nb;
  ft->table = (float **)memalloc(features_nb * sizeof(float *));
  for(i=0; i< features_nb; i++){
    ft->table[i] = (float *)memalloc(classes_nb * sizeof(float));
    for(j=0; j< classes_nb; j++)
	ft->table[i][j] = 0;
	/* ft->table[i][j] = (j== 0)? 0 : 1; */
  }
  return ft;
}

void feature_table_free(feature_table *ft)
{
  int i;

  for(i=0; i < ft->features_nb; i++)
    if(ft->table[i]) free(ft->table[i]);
  free(ft->table);
  free(ft);
}

float feature_table_diff_scores(feat_vec *fv, feature_table *ft)
{
  float *classes_score = (float *)memalloc(ft->classes_nb * sizeof(float));
  float first = 0;
  float second = 0;
  int cla;
  int classes_nb = ft->classes_nb;
  
  feature_table_scores(fv, ft, classes_score);

  first = classes_score[0];
  for(cla=1; cla < classes_nb; cla++)
    if(classes_score[cla] > first){
      second = first;
      first = classes_score[cla];
    }
  return (first - second);
}

float feature_table_argmax_1_2(feat_vec *fv, feature_table *ft, int *argmax1, float *max1, int *argmax2, float *max2)
{
  float *classes_score = (float *)memalloc(ft->classes_nb * sizeof(float));
  int cla;
  int classes_nb = ft->classes_nb;
  
  feature_table_scores(fv, ft, classes_score);

  *max1 = classes_score[0];
  *argmax1 = 0;
  for(cla=1; cla < classes_nb; cla++)
    if(classes_score[cla] > *max1){
      *max1 = classes_score[cla];
      *argmax1 = cla;
    }

  if(*argmax1 != 0){
    *argmax2 = 0;
    *max2 = classes_score[*argmax2];
  }
  else{
    *argmax2 = 1;
    *max2 = classes_score[*argmax2];
  }

  for(cla=0; cla < classes_nb; cla++)
    if((cla != *argmax1) && (classes_score[cla] > *max2)){
      *max2 = classes_score[cla];
      *argmax2 = cla;
    }
  
  /* printf("max1 = %f argmax1 = %d max2 = %f argmax2 = %d\n", *max1, *argmax1, *max2, *argmax2); */

  return (*max1 - *max2);
}


float feature_table_entropy(feat_vec *fv, feature_table *ft)
{
  float *classes_score = (float *)memalloc(ft->classes_nb * sizeof(float));
  int cla;
  int classes_nb = ft->classes_nb;
  float min;
  float sum = 0;
  float entropy = 0;
  float proba;
  
  feature_table_scores(fv, ft, classes_score);

  min = classes_score[0];
  for(cla=1; cla < classes_nb; cla++)
    if(classes_score[cla] < min)
      min = classes_score[cla];

  /* printf("min = %f\n", min); */
  
  for(cla=0; cla < classes_nb; cla++){
    classes_score[cla] -= min;
    sum += classes_score[cla];
  }

  /* printf("sum = %f\n", sum); */
  
  for(cla=0; cla < classes_nb; cla++){
    proba = classes_score[cla] / sum;
    /* printf("proba = %f entropy = %f\n", proba, entropy); */
    if(proba != 0.0)
      entropy -= proba * log(proba);
  }
  free(classes_score);
  return entropy;
}



int feature_table_argmax(feat_vec *fv, feature_table *ft, float *max)
{
  float *classes_score = (float *)memalloc(ft->classes_nb * sizeof(float));
  int cla, argmax;
  int classes_nb = ft->classes_nb;
  int feat;

  /* printf("feat tabl argmax classes nb = %d\n", classes_nb); */
    
  for(cla=0; cla < classes_nb; cla++) classes_score[cla] = 0;
  
  for(feat=0; feat < fv->nb; feat++){
    for(cla=0; cla < classes_nb; cla++){
      if((fv->t[feat] != -1) && (fv->t[feat] < ft->features_nb)){
      /* if(fv->t[feat] != -1){ */
	/* printf("feat score = %f\n", ft->table[fv->t[feat]][cla]); */
	classes_score[cla] += ft->table[fv->t[feat]][cla];
      }
    }
  }
  
  argmax = 0;
  *max = classes_score[0];
      /* printf("max = %f argmax = %d\n", *max, argmax); */
  for(cla=1; cla < classes_nb; cla++){
    /* printf("candidat %d = %f\n", cla, classes_score[cla]); */
    if(classes_score[cla] > *max){
      /* printf("max = %f argmax = %d\n", *max, argmax); */
      *max = classes_score[cla];
      argmax = cla;
    }
  } 
  free(classes_score);
  return argmax;
}

/* fill an array (classes_score) with the scores of the different classes */
/* for the feature vector fv */

void feature_table_scores(feat_vec *fv, feature_table *ft, float *classes_score)
{
  int cla;
  int classes_nb = ft->classes_nb;
  int feat;
  /* float sum = 0; */
  float min = 0;

  for(cla=0; cla < classes_nb; cla++) classes_score[cla] = 0;
  
  for(cla=0; cla < classes_nb; cla++){
    for(feat=0; feat < fv->nb; feat++){
      if(fv->t[feat] != -1){
	classes_score[cla] += ft->table[fv->t[feat]][cla];
      }
    }
    if(classes_score[cla] < min)
      min = classes_score[cla];
  }

 
  /* normalize */
  
  /*  for(class=0; class < classes_nb; class++){
    classes_score[class] -= min;
    sum += classes_score[class];
  }

  for(class=0; class < classes_nb; class++)
  classes_score[class] = log(classes_score[class] / sum);*/
}

int comparator (void const *pa, void const *pb){
  vcode const *a = (vcode *)pa, *b = (vcode *)pb;
	float ret = a->score - b->score;
	if(ret > 0) return -1;
	if(ret < 0) return 1;
	return 0;
}

/* takes as input a feature vector (fv) and a weight table (ft) 
   and returns an array of vcode which encodes, for every class
   the weight of fv with respect to  this class */

vcode* feature_table_get_vcode_array(feat_vec *fv, feature_table* ft)
{
  int classes_nb = ft->classes_nb, feat, cla;
  vcode* table = (vcode *)memalloc((classes_nb+1) * sizeof(vcode));
  
  for(cla = 0; cla < classes_nb; cla++){
    table[cla].score = 0;
    table[cla].class_code = cla;
    for(feat = 0; feat < fv->nb;feat++){
      if(fv->t[feat] >= ft->features_nb) continue;
      if(fv->t[feat] == -1) continue;
      table[cla].score +=
	ft->table[fv->t[feat]][cla];
    }
  }
  table[classes_nb].class_code = -1;
  table[classes_nb].score = 0;
  
  qsort(table, classes_nb, sizeof(vcode), comparator);
  return table;
}
