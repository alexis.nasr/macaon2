#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<unistd.h>
#include<getopt.h>
#include "perceptron_context.h"
#include "util.h"

void perceptron_context_free(perceptron_context *ctx)
{
  if(ctx->program_name)            free(ctx->program_name);
  if(ctx->cff_filename)            free(ctx->cff_filename);
  if(ctx->perc_model_filename)     free(ctx->perc_model_filename);

  free(ctx);
}

perceptron_context *perceptron_context_new(void)
{
  perceptron_context *ctx = (perceptron_context *)memalloc(sizeof(perceptron_context));

  ctx->help = 0;
  ctx->verbose = 0;
  ctx->program_name = NULL;
  ctx->perc_model_filename = NULL;
  ctx->cff_filename = NULL;
  ctx->iteration_nb = 4;

  return ctx;
}

void perceptron_context_help_message(perceptron_context *ctx)
{
    fprintf(stderr, "usage: %s [options]\n", ctx->program_name);
    fprintf(stderr, "Options:\n");
    fprintf(stderr, "\t-h --help                 : print this message\n");
    fprintf(stderr, "\t-v --verbose              : activate verbose mode\n");
    fprintf(stderr, "\t-n --iter           <int> : number of iterations  (default is 4)\n");
    fprintf(stderr, "\t-c --cff           <file> : CFF format file name\n");
    fprintf(stderr, "\t-m --model         <file> : model file name\n");
}

perceptron_context *perceptron_context_read_options(int argc, char *argv[])
{
  int c;
  int option_index = 0;
  perceptron_context *ctx = perceptron_context_new();

  ctx->program_name = strdup(argv[0]);

  static struct option long_options[5] =
    {
      {"help",                no_argument,       0, 'h'},
      {"verbose",             no_argument,       0, 'v'},
      {"model",               required_argument, 0, 'm'}, 
      {"iter",                required_argument, 0, 'n'},
      {"cff",                 required_argument, 0, 'c'}
    };
  optind = 0;
  opterr = 0;
  
  
  while ((c = getopt_long (argc, argv, "hvm:n:c:", long_options, &option_index)) != -1){ 
    switch (c)
      {
      case 'h':
	ctx->help = 1;
	break;
      case 'v':
	ctx->verbose = 1;
	break;
      case 'm':
	ctx->perc_model_filename = strdup(optarg);
	break;
      case 'n':
	ctx->iteration_nb = atoi(optarg);
	break;
      case 'c':
	ctx->cff_filename = strdup(optarg);
	break;
      }
  }

  return ctx;
}


