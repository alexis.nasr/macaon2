#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"util.h"


int *cff_max_value_per_column(char *cff_filename, int n)
{
  char buffer[10000]; /* ugly */
  char *token;
  int i;
  int col;
  int *max_array = (int *)memalloc(n * sizeof(int));
  for(i = 0; i < n; i++){
    max_array[i] = 0;
  }
  FILE *f = myfopen(cff_filename, "r");

  while(fgets(buffer, 10000, f)){
    token = strtok(buffer, "\t");
    col = 0;
    while(token){
      if(max_array[col] < atoi(token)){
	max_array[col] = atoi(token);
      }
      token = strtok(NULL , "\t");
      col++;
    }
  }
  fclose(f);
  return max_array;
}

int cff_look_for_number_of_columns(char *cff_filename)
{
  int nb_col = 0;
  FILE *f = myfopen(cff_filename, "r");
  char buffer[10000]; /* ugly */
  char *token;
  
  fgets(buffer, 10000, f);
  token = strtok(buffer, "\t");
  while(token){
    nb_col++;
    token = strtok(NULL , "\t");
  }
  
  fclose(f);
  return nb_col;
}


void look_for_number_of_features_and_classes(char *filename, int *max_feat, int *max_class)
{
  char buffer[10000];
  FILE *f = fopen(filename, "r");
  char *token;
  int nb;

  *max_feat = 0;
  *max_class = 0;
  if(f == NULL)
    return;
  while(fgets(buffer, 10000, f)){
    buffer[strlen(buffer) - 1] = '\0';
    token = strtok(buffer, "\t");
    nb = atoi(token);
    if(nb > *max_class) *max_class = nb;
    while((token = strtok(NULL, "\t"))){
      nb = atoi(token);
      if(nb > *max_feat) *max_feat = nb;

    }
  }

  *max_feat = *max_feat + 1;
  *max_class = *max_class + 1;
  fclose(f);
}

int look_for_number_of_examples(char *filename)
{
  char buffer[10000];
  FILE *f = fopen(filename, "r");
  int number = 0;

  while(fgets(buffer, 10000, f))
    number ++;
  fclose(f);
  return number;
}
  
int look_for_number_of_features(char *filename)
{
  char buffer[10000];
  FILE *f = fopen(filename, "r");
  char *token;
  int nb;
  int max_feat = 0;

  while(fgets(buffer, 10000, f)){
    buffer[strlen(buffer) - 1] = '\0';
    token = strtok(buffer, "\t");
    while((token = strtok(NULL, "\t"))){
      nb = atoi(token);
      if(nb > max_feat) max_feat = nb;
    }
  }
  max_feat = max_feat + 1;
  fclose(f);
  return max_feat;
}


int *count_occ_of_features(char *filename, int *n_feat)
{
  char buffer[10000];
  FILE *f = fopen(filename, "r");
  char *token;
  int nb;
  int *occ_table = NULL;
  
  *n_feat = look_for_number_of_features(filename);
  occ_table = (int *)memalloc(*n_feat * sizeof(int));
  while(fgets(buffer, 10000, f)){
    buffer[strlen(buffer) - 1] = '\0';
    token = strtok(buffer, "\t");
    while((token = strtok(NULL, "\t"))){
      nb = atoi(token);
      occ_table[nb]++;

    }
  }
  fclose(f);
  return occ_table;
}

