#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"feature_table.h"
#include"cf_file.h"
#include"util.h"

void perceptron_avg(char *filename, feature_table *ft, int n_iter)
{
  char buffer[10000];
  int class_ref;
  int classes_nb = ft->classes_nb;
  FILE *f;
  int cla;
  float max;
  int argmax;
  int feat;
  int epoch;
  int i,j;
  float *classes_score = (float *)memalloc(ft->classes_nb * sizeof(float));
  /* feat_vec *fv = feat_vec_new(feature_types_nb); */
  feat_vec *fv = feat_vec_new(1);
  char *token;
  feature_table *ft_sum = feature_table_new(ft->features_nb, ft->classes_nb);
  int counter = 1; 

  for(epoch = 0; epoch < n_iter; epoch++){
    fprintf(stderr, "[%d]", epoch + 1);
    f = fopen(filename, "r");
    i = 0;
    while(fgets(buffer, 10000, f)){
      buffer[strlen(buffer) - 1] = '\0';
      feat_vec_empty(fv);
      class_ref = atoi(strtok(buffer, "\t"));
      while((token = strtok(NULL, "\t"))){
	/* printf("token = %s\n", token); */
	feat_vec_add(fv, atoi(token));
      }


      for(cla=0; cla < classes_nb; cla++)	
	classes_score[cla] = 0;
      
      for(feat=0; feat < fv->nb; feat++)
	for(cla=0; cla < classes_nb; cla++)
	  if(fv->t[feat] != -1)
	    classes_score[cla] += ft->table[fv->t[feat]][cla];

      argmax = 0;
      max = classes_score[0];
      for(cla=1; cla < classes_nb; cla++){
	if(classes_score[cla] > max){
	  max = classes_score[cla];
	  argmax = cla;
	}
      }
      
      if(argmax != class_ref){
	/* fprintf(stderr, "[%d][%d] wrong\n", epoch, i);   */
	for(feat=0; feat < fv->nb; feat++){
	  if(fv->t[feat] != -1){
	  ft->table[fv->t[feat]][argmax]--;
	  ft->table[fv->t[feat]][class_ref]++;

	  ft_sum->table[fv->t[feat]][argmax] -= counter;
	  ft_sum->table[fv->t[feat]][class_ref] += counter;
	}
	}
      }
      else{
	/* fprintf(stderr, "[%d][%d] right\n", epoch, i);   */
      }
      i++;
      /*      if(i%10000 == 0)
	      fprintf(stderr, "[%d][%d]\n", epoch, i); */
      counter++;
    }

    fclose(f);
  }
  fprintf(stderr, "\n"); 

  for(i=0; i< ft->features_nb; i++)
    for(j=0; j< ft->classes_nb; j++)
       ft->table[i][j] -= 1/(float)counter * ft_sum->table[i][j]; 
  
  free(classes_score);
  feat_vec_free(fv);
  /*  feature_table_free(ft_sum);*/
}

void perceptron(char *filename, feature_table *ft, int n_iter)
{
  char buffer[10000];
  int class_ref;
  int classes_nb = ft->classes_nb;
  FILE *f;
  int cla;
  float max;
  int argmax;
  int feat;
  int epoch;
  int i;
  float *classes_score = (float *)memalloc(ft->classes_nb * sizeof(float));
  /* feat_vec *fv = feat_vec_new(feature_types_nb); */
  feat_vec *fv = feat_vec_new(1);
  char *token;

  for(epoch = 0; epoch < n_iter; epoch++){
    fprintf(stderr, "[%d]", epoch);
    f = fopen(filename, "r");
    i = 0;
    while(fgets(buffer, 10000, f)){
      buffer[strlen(buffer) - 1] = '\0';
      feat_vec_empty(fv);
      class_ref = atoi(strtok(buffer, "\t"));
      while((token = strtok(NULL, "\t"))){
	/* printf("token = %s\n", token); */
	feat_vec_add(fv, atoi(token));
      }
      
      for(cla=0; cla < classes_nb; cla++)	
	classes_score[cla] = 0;

      for(feat=0; feat < fv->nb; feat++)
	for(cla=0; cla < classes_nb; cla++)
	  classes_score[cla] += ft->table[fv->t[feat]][cla];

      argmax = 0;
      max = classes_score[0];
      for(cla=1; cla < classes_nb; cla++){
	if(classes_score[cla] > max){
	  max = classes_score[cla];
	  argmax = cla;
	}
      }
      
      if(argmax != class_ref){
	/* fprintf(stderr, "[%d][%d] wrong\n", epoch, i);   */
	for(feat=0; feat < fv->nb; feat++){
	  ft->table[fv->t[feat]][argmax]--;
	  ft->table[fv->t[feat]][class_ref]++;
	}	
      }
      else{
	/* fprintf(stderr, "[%d][%d] right\n", epoch, i);   */
      }
      i++;
      /*      if(i%10000 == 0)
	      fprintf(stderr, "[%d][%d]\n", epoch, i); */
    }

    fclose(f);
  }
    fprintf(stderr, "\n"); 
  free(classes_score);
  feat_vec_free(fv);
}
