#include<stdio.h>
#include<stdlib.h>
#include"feat_vec.h"
#include"util.h"
/* #include "feat_types.h" */
/* #include "feat_model.h" */

void feat_vec_concat(feat_vec *fv1, feat_vec *fv2)
{
  int i;
  for(i=0; i < fv2->nb; i++)
    if(fv2->t[i] != -1)
      feat_vec_add(fv1, fv2->t[i]);
}


feat_vec *feat_vec_new(int size)
{
  feat_vec *fv = (feat_vec *)memalloc(sizeof(feat_vec));
  fv->size = size;
  fv->nb = 0;
  fv->t = (int *)memalloc(size * sizeof(int));
  return fv;
}

void feat_vec_free(feat_vec *fv)
{
  if(fv){
    if(fv->t)
      free(fv->t);
    free(fv);
  }
}

feat_vec *feat_vec_copy(feat_vec *fv)
{
  feat_vec *copy = feat_vec_new(fv->size);
  int i;
  for(i=0; i < fv->nb; i++){
    feat_vec_add(copy, fv->t[i]);
  }
  return copy;
}

int feat_vec_add(feat_vec *fv, int feat)
{
  if(fv->nb == fv->size -1){
    fv->size = 2 * (fv->size + 1);
    fv->t = (int *)realloc(fv->t, fv->size * sizeof(int));
  }
  fv->t[fv->nb] = feat;
  fv->nb++;
  return fv->nb;
}

void feat_vec_empty(feat_vec *fv)
{
  fv->nb = 0;
}

void feat_vec_print_string(feat_vec *fv, dico *dico_features)
{
  int i;
  for(i=0; i < fv->nb; i++){
    if(fv->t[i] != -1)
      printf("\t%s", dico_int2string(dico_features, fv->t[i]));
    else
      printf("\tNULL");
  }
  printf("\n");
}

void feat_vec_print(FILE *f, feat_vec *fv)
{
  int i;
  for(i=0; i < fv->nb; i++)
    fprintf(f, "\t%d", fv->t[i]);
  fprintf(f, "\n");
}

