#ifndef __PERCEPTRON_CONTEXT__
#define __PERCEPTRON_CONTEXT__

#include "dico_vec.h"
#include<stdlib.h>

typedef struct {
  int help;
  int verbose;
  char *program_name;
  char *cff_filename;
  char *perc_model_filename;
  int iteration_nb;
} perceptron_context;

perceptron_context *perceptron_context_new(void);
void perceptron_context_free(perceptron_context *ctx);
perceptron_context *perceptron_context_read_options(int argc, char *argv[]);
void perceptron_context_help_message(perceptron_context *ctx);


#endif
