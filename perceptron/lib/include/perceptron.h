#ifndef __PERCEPTRON__
#define __PERCEPTRON__
void perceptron(char *filename, feature_table *ft, int n_iter);
void perceptron_avg(char *filename, feature_table *ft, int n_iter);
#endif
