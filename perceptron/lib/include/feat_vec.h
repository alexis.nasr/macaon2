#ifndef __FEAT_VEC__
#define __FEAT_VEC__

typedef struct {
  int size;
  int nb;
  int *t;
} feat_vec;

/* #include "feat_model.h" */
#include "dico.h"
/*#include "dico_vec.h"*/
/*#include "word_emb.h"*/
#include "mcd.h"

void      feat_vec_concat(feat_vec *fv1, feat_vec *fv2);
feat_vec *feat_vec_copy(feat_vec *fv);
feat_vec *feat_vec_new(int size);
void      feat_vec_free(feat_vec *fv);
int       feat_vec_add(feat_vec *fv, int feat);
void      feat_vec_empty(feat_vec *fv);
void      feat_vec_print_string(feat_vec *fv, dico *dico_features);
void      feat_vec_print(FILE *f, feat_vec *fv);
/* void feat_vec_print_dnn(FILE *f, feat_vec *fv, feat_model *fm, mcd *m);  */
/* void feat_vec_fill_input_array_dnn(fann_type *input_array, feat_vec *fv, feat_model *fm, mcd *m); */
/* void feat_vec_fill_input_array_dnn(float *input_array, feat_vec *fv, feat_model *fm, mcd *m); */
#endif
