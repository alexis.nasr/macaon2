#ifndef __FEATURE_TABLE__
#define __FEATURE_TABLE__
#include"dico.h"
#include"feat_vec.h"

typedef struct 
{
  int features_nb;
  int classes_nb;
  float **table;
} feature_table;


typedef struct {
  int class_code;
  float score;
} vcode;

feature_table *feature_table_load(char *filename, int verbose);
void           feature_table_dump(char *filename, feature_table *ft);
feature_table *feature_table_new(int features_nb, int classes_nb);
void           feature_table_print(char *filename, feature_table *ft);
void           feature_table_print_verbose(char *filename, feature_table *ft, dico *dico_features, dico *dico_classes);
int            feature_table_argmax(feat_vec *fv, feature_table *ft, float *max);
float          feature_table_entropy(feat_vec *fv, feature_table *ft);
float          feature_table_diff_scores(feat_vec *fv, feature_table *ft);
float          feature_table_argmax_1_2(feat_vec *fv, feature_table *ft, int *argmax1, float *max1, int *argmax2, float *max2);
void           feature_table_free(feature_table *ft);
void           feature_table_scores(feat_vec *fv, feature_table *ft, float *classes_score);
vcode         *feature_table_get_vcode_array(feat_vec *fv, feature_table* ft);
#endif
