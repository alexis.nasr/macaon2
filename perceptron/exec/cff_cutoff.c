#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<getopt.h>
#include"feature_table.h"
#include"dico.h"
#include"util.h"
#include"perceptron.h"
#include"perceptron_context.h"
#include"cf_file.h"

typedef struct {
  int help;
  int verbose;
  char *program_name;
  char *cff_filename;
  int cutoff;
  char *vocabs_filename;
  dico_vec *vocabs;
  float hash_ratio;
  dico *d_perceptron_features;

} cff_cutoff_context;

cff_cutoff_context *cff_cutoff_context_new(void)
{
  cff_cutoff_context *ctx = (cff_cutoff_context *)memalloc(sizeof(cff_cutoff_context));

  ctx->help = 0;
  ctx->verbose = 0;
  ctx->program_name = NULL;
  ctx->vocabs_filename = NULL;
  ctx->cff_filename = NULL;
  ctx->cutoff = 1;
  ctx->hash_ratio = 0.5;
  ctx->vocabs = NULL;
  ctx->d_perceptron_features = NULL;
  return ctx;
}

void cff_cutoff_context_free(cff_cutoff_context *ctx)
{
  if(ctx->program_name)            free(ctx->program_name);
  if(ctx->cff_filename)            free(ctx->cff_filename);
  free(ctx);
}


void cff_cutoff_help_message2(cff_cutoff_context *ctx)
{
    fprintf(stderr, "usage: %s [options]\n", ctx->program_name);
    fprintf(stderr, "Options:\n");
    fprintf(stderr, "\t-h --help                 : print this message\n");
    fprintf(stderr, "\t-v --verbose              : activate verbose mode\n");
    fprintf(stderr, "\t-i --input         <file> : cff file name\n");
    fprintf(stderr, "\t-V --vocabs        <file> : vocabs filename\n");
    fprintf(stderr, "\t-c --cutoff         <int> : threshold (features appearing less than the threshold are ignored\n");
}

cff_cutoff_context *cff_cutoff_read_options(int argc, char *argv[])
{
  int c;
  int option_index = 0;
  cff_cutoff_context *ctx = cff_cutoff_context_new();

  ctx->program_name = strdup(argv[0]);

  static struct option long_options[5] =
    {
      {"help",                no_argument,       0, 'h'},
      {"verbose",             no_argument,       0, 'v'},
      {"input",               required_argument, 0, 'i'}, 
      {"vocabs",              required_argument, 0, 'V'}, 
      {"cutoff",              required_argument, 0, 'c'}
    };
  optind = 0;
  opterr = 0;
  
  
  while ((c = getopt_long (argc, argv, "hvi:V:c:", long_options, &option_index)) != -1){ 
    switch (c)
      {
      case 'h':
	ctx->help = 1;
	break;
      case 'v':
	ctx->verbose = 1;
	break;
      case 'V':
	ctx->vocabs_filename = strdup(optarg);
	break;
      case 'c':
	ctx->cutoff = atoi(optarg);
	break;
      case 'i':
	ctx->cff_filename = strdup(optarg);
	break;
      }
  }

  return ctx;
}

void cff_cutoff_check_options(cff_cutoff_context *ctx)
{
  if(ctx->help
     || !ctx->vocabs_filename
     || !ctx->cff_filename
     ){
    cff_cutoff_help_message2(ctx);
    exit(1);
  }
}

int main(int argc, char *argv[])
{
  char buffer[10000];
  FILE *f;
  char *token;
  int n_feat = 0;
  int *occ_table = NULL;
  int occ_removed = 0;
  int feat_removed = 0;
  int i;
  int f_occ = 0;
  int *old2new;
  int feat;

  dico *old_d_feat;
  dico *new_d_feat;
  cff_cutoff_context *ctx;

  ctx = cff_cutoff_read_options(argc, argv);
  cff_cutoff_check_options(ctx);

  ctx->vocabs = dico_vec_read(ctx->vocabs_filename, ctx->hash_ratio);
  ctx->d_perceptron_features = dico_vec_get_dico(ctx->vocabs, (char *)"d_perceptron_features");

  old_d_feat = ctx->d_perceptron_features;
  new_d_feat = dico_new((char *)"d_perceptron_features", old_d_feat->nbelem * ctx->hash_ratio);
  occ_table = count_occ_of_features(ctx->cff_filename, &n_feat);
  f = myfopen(ctx->cff_filename, "r");
  old2new = (int *)memalloc(n_feat * sizeof(int));

  for(i=0; i < n_feat; i++)
    if(occ_table[i] < ctx->cutoff)
      old2new[i] = -1;
    else
      old2new[i] = dico_add(new_d_feat, dico_int2string(old_d_feat, i));

  feat_removed = old_d_feat->nbelem - new_d_feat->nbelem;

  while(fgets(buffer, 10000, f)){
    buffer[strlen(buffer) - 1] = '\0';
    token = strtok(buffer, "\t");
    printf("%s", token);
    while((token = strtok(NULL, "\t"))){
      f_occ++;
      feat = atoi(token);
       printf("\t%d", old2new[feat]); 
       if(old2new[feat] == -1)       occ_removed++; 
       /*       if(old2new[feat] != -1)
	printf("\t%d", old2new[feat]);
      else
      occ_removed++;*/
    }
    printf("\n");
  }
  fclose(f);


  fprintf(stderr, "total number of features    : %d\n", n_feat);
  fprintf(stderr, "threshold                   : %d\n", ctx->cutoff);
  fprintf(stderr, "after thresholding          : %d\n", n_feat - feat_removed);
  fprintf(stderr, "ratio                       : %.3f\n\n", (float)(n_feat - feat_removed) / n_feat);

  /*  fprintf(stderr, "total number of feature occurrences : %d\n", f_occ);
  fprintf(stderr, "atfer thresholding                  : %d\n", f_occ - occ_removed);
  fprintf(stderr, "ratio                               : %.3f\n", (float)(f_occ - occ_removed) / f_occ);*/


  dico_vec_replace_dico(ctx->vocabs, old_d_feat, new_d_feat);
  dico_vec_print(ctx->vocabs_filename, ctx->vocabs);

  dico_free(new_d_feat);
  free(old2new);
  cff_cutoff_context_free(ctx);
  return 0;
}
