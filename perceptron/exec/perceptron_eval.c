#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<getopt.h>
#include"util.h"
#include"feature_table.h"
#include"perceptron.h"
#include"perceptron_context.h"

void eval_classifier_help_message(perceptron_context *ctx)
{
  perceptron_context_help_message(ctx);
  /* fprintf(stderr, "INPUT\n"); */
  /* perceptron_context_model_help_message(ctx); */
  /* perceptron_context_cff_help_message(ctx); */
}

void eval_classifier_check_options(perceptron_context *ctx)
{
  if(!ctx->cff_filename
     || !ctx->perc_model_filename
     || ctx->help
     ){
    eval_classifier_help_message(ctx);
    exit(1);
  }
}

int main(int argc, char *argv[])
{
  int class_ref, class_hyp;
  char buffer[10000];
  feature_table *ft;
  FILE *f;
  int i = 0;
  int err_nb = 0;
  feat_vec *fv = feat_vec_new(2);
  char *token;
  float accuracy;
  perceptron_context *ctx;
  float score;

  ctx = perceptron_context_read_options(argc, argv);
  eval_classifier_check_options(ctx);

  ft = feature_table_load(ctx->perc_model_filename, ctx->verbose);
  f = myfopen(ctx->cff_filename, "r");
  i = 0;
  while(fgets(buffer, 10000, f)){
    buffer[strlen(buffer) - 1] = '\0';
    feat_vec_empty(fv);
    class_ref = atoi(strtok(buffer, "\t"));
    while((token = strtok(NULL, "\t"))){
      /* printf("token = %s\n", token); */
      feat_vec_add(fv, atoi(token));
    }
    
    class_hyp =  feature_table_argmax(fv, ft, &score);
    
    if(class_hyp != class_ref){
      printf("REF = %d HYP = %d\n", class_ref, class_hyp); 
      err_nb++;
    }
    else{
      /* fprintf(stderr, "[%d][%d] right\n", epoch, i);   */
    }
    i++;
    accuracy = 1 - (float) err_nb / (float) i;
  }
    printf("accuracy = %f\n", accuracy);
  fclose(f);
  return 0;
}
