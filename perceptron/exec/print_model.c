#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"feature_table.h"
#include"dico.h"
#include"context.h"

void print_model_help_message(context *ctx)
{
  context_general_help_message(ctx);
  fprintf(stderr, "INPUT\n");
  context_d_features_help_message(ctx);
  context_d_classes_help_message(ctx);
  context_model_help_message(ctx);
}

void print_model_check_options(context *ctx){
  if(!ctx->perc_model_filename
     || !ctx->dico_features_filename
     || !ctx->dico_classes_filename
     || ctx->help
     ){
    print_model_help_message(ctx);
    exit(1);
  }
}

int main(int argc, char *argv[])
{  
  feature_table *ft = NULL;
  dico *dico_features = NULL;
  dico *dico_classes = NULL;
  context *ctx;

  ctx = context_read_options(argc, argv);
  print_model_check_options(ctx);

  ft = feature_table_load(ctx->perc_model_filename);
  dico_features = dico_read(ctx->dico_features_filename, ctx->hash_ratio);
  dico_classes = dico_read(ctx->dico_classes_filename, ctx->hash_ratio);

  feature_table_print_verbose(NULL, ft, dico_features, dico_classes);
  return 0;
}
