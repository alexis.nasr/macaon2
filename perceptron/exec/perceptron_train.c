#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<getopt.h>
#include"feature_table.h"
#include"dico.h"
#include"perceptron.h"
#include"perceptron_context.h"
#include"cf_file.h"

void train_help_message(perceptron_context *ctx)
{
  perceptron_context_help_message(ctx);
}

void train_check_options(perceptron_context *ctx)
{
  if(!ctx->cff_filename
     || !ctx->perc_model_filename
     || ctx->help
     ){
    train_help_message(ctx);
    exit(1);
  }
}

int main(int argc, char *argv[])
{
  int nb_feat, nb_class;
  feature_table *ft;
  perceptron_context *ctx;

  ctx = perceptron_context_read_options(argc, argv);
  train_check_options(ctx);

  look_for_number_of_features_and_classes(ctx->cff_filename, &nb_feat, &nb_class);
  if(nb_class > 1){
    ft = feature_table_new(nb_feat, nb_class);
    fprintf(stderr, "table allocated (%d x %d)\n", nb_feat, nb_class); 
    perceptron_avg(ctx->cff_filename, ft, ctx->iteration_nb);
    feature_table_dump(ctx->perc_model_filename, ft);
  }
  perceptron_context_free(ctx);

  return 0;
}
