#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include"dico.h"
#include"util.h"


/* return 1 if form contains at least one space character */
int form_is_complex(char *form)
{
  int i;
  int l = strlen(form);
  for(i=0; i < l; i++)
    if(form[i] == ' ')
      return 1;
  return 0;
}

dico *decompose_mwe_in_fplm_file(char *fplm_filename, FILE *output_file, int debug_mode)
{
  char form[1000];
  char pos[1000];
  char lemma[1000];  
  char morpho[1000];
  char buffer[10000];
  FILE *f= myfopen(fplm_filename, "r");
  int fields_nb;
  char token[1000];
  int l;
  int i, j;
  dico *d_tokens = dico_new((char *)"TOKENS", 100000);
  int token_code;  
  while(fgets(buffer, 10000, f)){
    fields_nb = sscanf(buffer, "%[^\t]\t%s\t%[^\t]\t%s\n", form, pos, lemma, morpho);
    if(fields_nb != 4){
      if(debug_mode){
	fprintf(stderr, "form = %s pos = %s lemma = %s\n", form, pos, lemma); 
	fprintf(stderr, "incorrect fplm entry, skipping it\n");
      }
      continue;
    }
    if(form_is_complex(form)){
      /* fprintf(stdout, "form = %s pos = %s lemma = %s\n", form, pos, lemma);    */
      /* fprintf(stdout, "%s\n", form);    */
      l = strlen(form);
      j = 0;
      for(i=0; i <= l; i++){
	if((form[i] != ' ') && (i < l)){
	  token[j++] = form[i];
	}
	else{
	  token[j] = '\0';
	  token_code = dico_add(d_tokens, token);
	  /* fprintf(output_file, "token = %s code = %d\n", token, token_code); */
	  fprintf(output_file, "%d", token_code);
	  if(i != l)
	    fprintf(output_file, " ");
	  j = 0;
	}
      }
      fprintf(output_file, "\n");
    }
  }
  return d_tokens;
}
  
int main(int argc, char *argv[])
{

  dico *d_tokens;

  d_tokens = decompose_mwe_in_fplm_file(argv[1], stdout, 1);
  dico_print((char *)"d_tokens.dico", d_tokens);
  dico_free(d_tokens);
}
