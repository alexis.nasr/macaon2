#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include"trie.h"
#include"dico.h"
#include"util.h"
#include"context.h"


void maca_lexer_help_message(context *ctx)
{
  context_general_help_message(ctx);
  fprintf(stderr, "INPUT\n");
  context_input_help_message(ctx);
  context_mcd_help_message(ctx);
  context_language_help_message(ctx);
  context_maca_data_path_help_message(ctx);
  context_form_column_help_message(ctx);
  context_mwe_token_separator_help_message(ctx);
  context_mwe_filename_help_message(ctx);
  context_vocab_help_message(ctx);
}

void maca_lexer_check_options(context *ctx){
  if(ctx->help){
    maca_lexer_help_message(ctx);
    exit(1);
  }
}

int look_for_accept_state_in_path(trie *mwe_trie, int *states_array, int path_index)
{
  int i;
  for(i=path_index - 1; i >= 0; i--){
    if(mwe_trie->states[states_array[i]]->is_accept) return i;
  }
  return -1;
}

void print_states_array(char *buffer, context *ctx, trie *mwe_trie, dico *d_mwe_tokens, int *states_array, int *symbols_array, int path_index, float *start_array, float *end_array, int orfeo, char *spkr)
{
  int i;
  if(path_index == 0) return;
  int accept_state_index =  look_for_accept_state_in_path(mwe_trie, states_array, path_index);
  /* all tokens in path s.t. 0 <= token_index <= accept_state_index form an mwe */
  for(i=0; i <= accept_state_index; i++){ 
    if(ctx->paste){
      if(i > 0) printf("%s", ctx->mwe_tokens_separator); 
      printf("%s", dico_int2string(d_mwe_tokens, symbols_array[i])); 
    }
    else{
      if(i==0) printf("%s\t1\n", dico_int2string(d_mwe_tokens, symbols_array[i]));
      else printf("%s\t0\n", dico_int2string(d_mwe_tokens, symbols_array[i]));
    }
  }
  if(i>0 && orfeo){
    printf("\t%f\t%f\t%s", start_array[0], end_array[accept_state_index], spkr);
  }
      

  if(ctx->paste)
    if(accept_state_index != -1) printf("\n"); 
  /* all tokens in path s.t. accept_state_index < token_index < path_index do not form an mwe, they are just printed */
  for(i = accept_state_index + 1; i < path_index; i++){
    if(ctx->paste){
      if(orfeo){
	printf("%s\t%f\t%f\t%s\n", dico_int2string(d_mwe_tokens, symbols_array[i]), start_array[i], end_array[i], spkr);
      }
      else{
	printf("%s\n", dico_int2string(d_mwe_tokens, symbols_array[i]));
      }
    }
    else
      printf("%s\t1\n", dico_int2string(d_mwe_tokens, symbols_array[i]));
  }
}



int main(int argc, char *argv[])
{
  char buffer[10000];
  int form_code;
  context *ctx;
  /* int form_column; */
  FILE *f = NULL;
  trie *mwe_trie;
  dico *d_mwe_tokens = NULL;
  int states_array[100]; /* an array in which we store the states we have traversed in the trie */
  int symbols_array[100];
  float start_array[100];
  float end_array[100];
  int path_index = 0;
  int next_state;
  int orfeo = 0;
  char form[1000];
  float start;
  float end;
  char spkr[1000];
  ctx = context_read_options(argc, argv);
   maca_lexer_check_options(ctx);
   
   /*  
  if(ctx->form_column != -1)
    form_column = ctx->form_column;
  else
    form_column = ctx->mcd_struct->wf2col[MCD_WF_FORM];
   */
  if(ctx->input_filename == NULL)
    f = stdin;
  else
    f = myfopen(ctx->input_filename, "r");

  if(ctx->verbose) fprintf(stderr, "reading mwe list from file : %s\n", ctx->mwe_filename);
  mwe_trie = trie_build_from_collection(ctx->mwe_filename);
  
  if(ctx->verbose) fprintf(stderr, "reading mwe tokens vocabulary from file : %s\n", ctx->mwe_tokens_dico_filename);
  d_mwe_tokens = dico_read(ctx->mwe_tokens_dico_filename, 0.5);

  /* trie_print(stdout, mwe_trie); */
  
  while(fgets(buffer, 10000, f)){
    /* look for a valid form */
    if((buffer[0] == '\n') || (buffer[0] == ' ') || (buffer[0] == '\t')){
      printf("\n");
      continue;
    }
    buffer[strlen(buffer)-1] = '\0';

    if(orfeo){
      sscanf(buffer,"%s\t%f\t%f\t%s", form, &start, &end, spkr);
      /* look for code of form read */
      form_code = dico_string2int(d_mwe_tokens, form);
      
    }
    else{
      /* look for code of form read */
      form_code = dico_string2int(d_mwe_tokens, buffer);
    }

    if(form_code == -1){
    /* if form has no code, it cannot be part of a mwe, print the potential mwe discovered so far */
      print_states_array(buffer, ctx, mwe_trie, d_mwe_tokens, states_array, symbols_array, path_index, start_array, end_array, orfeo, spkr);
      path_index = 0;
      /* print the current form */
      if(ctx->paste){
	if(orfeo)
	  printf("%s\t%f\t%f\t%s\n", form, start, end, spkr);
	else
	  printf("%s\n", buffer);
      }
      else
	printf("%s\t1\n", buffer);
      continue;
    }
    
    /* look for the next state in the trie */
    next_state = trie_destination_state(mwe_trie, (path_index == 0) ? 0: states_array[path_index - 1], form_code);

    if(next_state != 0){
      /* the path is growing */
      symbols_array[path_index] = form_code;
      if(orfeo){
	start_array[path_index] = start;
	end_array[path_index] = end;
      }
      states_array[path_index] = next_state;
      path_index++;
      continue;
    }
    /* print the potential mwe discovered so far */
    print_states_array(buffer, ctx, mwe_trie, d_mwe_tokens, states_array, symbols_array, path_index, start_array, end_array, orfeo, spkr);

    if(path_index != 0)
      /* if there was a path that aborted, see if there is a valid transition from state 0 with form */
      next_state = trie_destination_state(mwe_trie, 0, form_code);
    
    path_index = 0;
    if(next_state){
      /* such a transition exists */
      symbols_array[path_index] = form_code;
      if(orfeo){
	start_array[path_index] = start;
	end_array[path_index] = end;
      }
      states_array[path_index] = next_state;
      path_index++;
      continue;
    }
    
    /* such a transition does not exist, just print the form */
    if(ctx->paste){
      if(orfeo)
	printf("%s\t%f\t%f\t%s\n", form, start, end, spkr);
      else
	printf("%s\n", buffer);
    }
    else
      printf("%s\t1\n", buffer);
  }
  
  if(path_index != 0){
    /* there is something in states array */
    /* print the potential mwe discovered so far */
    print_states_array(buffer, ctx, mwe_trie, d_mwe_tokens, states_array, symbols_array, path_index, start_array, end_array, orfeo, spkr);
    path_index = 0;
  }
  
  return 0;
}

