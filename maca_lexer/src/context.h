#ifndef __MACA_LEXER_CONTEXT__
#define __MACA_LEXER_CONTEXT__

#include "mcd.h"
#include <stdlib.h>

#define DEFAULT_MWE_TOKENS_DICO_FILENAME "d_tokens.dico"
#define DEFAULT_MWE_FILENAME "mwe"

typedef struct {
  int help;
  int verbose;
  int debug_mode;
  char *program_name;
  char *language;
  char *maca_data_path;
  char *mcd_filename;
  mcd *mcd_struct;
  int form_column;
  char *input_filename;
  char *output_filename;
  char *mwe_filename;
  char *mwe_tokens_dico_filename;
  char *mwe_tokens_separator;
  int paste;
} context;

context *context_new(void);
void context_free(context *ctx);

context *context_read_options(int argc, char *argv[]);
void context_general_help_message(context *ctx);
void context_conll_help_message(context *ctx);
void context_language_help_message(context *ctx);
void context_maca_data_path_help_message(context *ctx);
void context_mcd_help_message(context *ctx);
void context_form_column_help_message(context *ctx);
void context_pos_column_help_message(context *ctx);

void context_input_help_message(context *ctx);
void context_mwe_token_separator_help_message(context *ctx);
void context_mwe_filename_help_message(context *ctx);
void context_vocab_help_message(context *ctx);

#endif
